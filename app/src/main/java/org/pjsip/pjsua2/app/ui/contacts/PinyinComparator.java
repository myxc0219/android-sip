package org.pjsip.pjsua2.app.ui.contacts;

import org.pjsip.pjsua2.app.model.PersonBean;

import java.util.Comparator;

public class PinyinComparator implements Comparator<PersonBean> {
    public int compare(PersonBean o1, PersonBean o2) {
        if (o1.getSortLetters().equals("@")
                || o2.getSortLetters().equals("#")) {
            return -1;
        } else if (o1.getSortLetters().equals("#")
                || o2.getSortLetters().equals("@")) {
            return 1;
        } else {
            return o1.getSortLetters().compareTo(o2.getSortLetters());
        }
    }
}

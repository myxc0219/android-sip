package org.pjsip.pjsua2.app.global;

import android.content.Context;
import android.os.Build;
import android.os.Looper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import org.pjsip.pjsua2.app.R;

public class GlobalNotify {

    static WindowManager systemService;
    static View layoutView;

    public static void showSysnotify(Context context)
    {
        systemService = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutView = inflater.inflate(R.layout.custom_notify_toast, null);

        Button closebutton = (Button) layoutView.findViewById(R.id.notify_button);
        closebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                systemService.removeView(layoutView);
            }
        });

        WindowManager.LayoutParams params = new WindowManager.LayoutParams();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            params.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
        }
        //这个地方一定要这样设置，不然要么是布局之外不能接受事件，要么是布局里面和返回按键接收不到事件。
        params.flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
        params.gravity = Gravity.TOP | Gravity.LEFT;
        // 设置窗口宽度和高度
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        params.x = 50;
        Looper.prepare();
        systemService.addView(layoutView,params);
        Looper.loop();
    }

    //全局弹出消息的绑定函数(已弃用)
    public static void closeSysnotify(Context context)
    {
        systemService = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutView = inflater.inflate(R.layout.custom_notify_toast, null);
        Looper.prepare();
        systemService.removeViewImmediate(layoutView);
        Looper.loop();
    }

}

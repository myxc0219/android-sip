package org.pjsip.pjsua2.app.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Get or set system properties by reflection.
 */
public class SystemPropertiesUtils {
    private static Method sysPropGet;
    private static Method sysPropSet;
    private static Method sysPropGetInt;

    private SystemPropertiesUtils() {
    }

    static {
        try {
            Class<?> S = Class.forName("android.os.SystemProperties");
            Method M[] = S.getMethods();
            for (Method m : M) {
                String n = m.getName();
                if (n.equals("get")) {
                    sysPropGet = m;
                } else if (n.equals("set")) {
                    sysPropSet = m;
                } else if (n.equals("getInt")) {
                    sysPropGetInt = m;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String get(String key, String def) {
        try {
            return (String) sysPropGet.invoke(null, key, def);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }

    public static void set(String key, String def) {
        try {
            sysPropSet.invoke(null, key, def);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static int getInt(String key, int def) {
        try {
            return (int) sysPropGetInt.invoke(null, key, def);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }
}

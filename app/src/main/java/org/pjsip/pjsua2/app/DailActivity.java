package org.pjsip.pjsua2.app;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.Call;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.app.adapter.IntercomAdapter;
import org.pjsip.pjsua2.app.adapter.MySpinnerAdapter;
import org.pjsip.pjsua2.app.dialog.ImNotifyDialog;
import org.pjsip.pjsua2.app.dialog.LineChoseDialog;
import org.pjsip.pjsua2.app.dialog.MbcForbidCallDialog;
import org.pjsip.pjsua2.app.dialog.MsgNotifyDialog;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.Channel;
import org.pjsip.pjsua2.app.model.IntercomUser;
import org.pjsip.pjsua2.app.model.UserInfo;
import org.pjsip.pjsua2.pjsip_inv_state;
import org.pjsip.pjsua2.pjsip_role_e;
import org.pjsip.pjsua2.pjsip_status_code;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import pl.droidsonroids.gif.GifImageView;

public class DailActivity extends BaseActivity
        implements Handler.Callback
{
    public static Handler handler_;
    private final Handler handler = new Handler(this);

    private Integer click_btn_index = -1;

    //private static MyCall interCall;
    //private static CallInfo interCallInfo;
    private static CallInfo lastCallInfo;

    public boolean currentdail=false;
    //在拨打外线电话的时候后续会需要点击按钮的功能
    public MainActivity.MyCallWithState current_dailout;


    //集成intercom界面功能
    //private static MyCall intercomCall;
    //private static CallInfo intercomCallInfo;
    private ListView buddyListView;
    private IntercomAdapter buddyListAdapter;
    private ArrayList<Map<String, String>> buddyList = new ArrayList<>();
    private int buddyListSelectedIdx = -1;
    private Spinner channelSpinner;
    private List<IntercomUser> intercomUserList = new ArrayList<>();

    @Override
    protected void onStart() {

        if(GlobalVariable.isDL1129) {
            Button innercomBtn = findViewById(R.id.buttonMA6);
            innercomBtn.setVisibility(View.GONE);
        }
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.ct_title_home));
            imgBtn1.setImageDrawable(getDrawable(R.drawable.ct_title_history));
            imgBtn2.setImageDrawable(getDrawable(R.drawable.ct_title_contacts));
            imgBtn3.setImageDrawable(getDrawable(R.drawable.ct_title_setting));
            //imgBtn4.setImageDrawable(getDrawable(R.drawable.ct_title_dail));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.gs_title_home));
            imgBtn1.setImageDrawable(getDrawable(R.drawable.gs_title_history));
            imgBtn2.setImageDrawable(getDrawable(R.drawable.gs_title_contacts));
            imgBtn3.setImageDrawable(getDrawable(R.drawable.gs_title_setting));
            //imgBtn4.setImageDrawable(getDrawable(R.drawable.ct_title_dail));
        }
        super.onStart();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        if(GlobalVariable.viewmodel == 1){
            setContentView(R.layout.activity_dial);
        }
        else if(GlobalVariable.viewmodel == 2){
            setContentView(R.layout.activity_dial_ct);
        }
        else if(GlobalVariable.viewmodel == 3){
            setContentView(R.layout.activity_dial_gs);
        }

        handler_ = handler;



        //默认添加两个0（外线电话要3个0不过拨错会有语音提示）//已在标准界面添加
        if(GlobalVariable.setdailvalue) {
            EditText dailtext = (EditText) findViewById(R.id.dialNumberText);
            dailtext.setText(GlobalVariable.dailvalue);
            GlobalVariable.setdailvalue = false;
        }


//        LinearLayout callinglayout = findViewById(R.id.callingPanel);
//        LinearLayout diallayout = findViewById(R.id.dialPanel);
//        callinglayout.setVisibility(View.GONE);
//        diallayout.setVisibility(View.VISIBLE);


        ImageButton backSpacebutton =  findViewById(R.id.dailBackSpace);
        backSpacebutton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
                dailtext.setText("");
                return true;
            }
        });

        //初始化intercom列表（更新版本的intercom列表会随着频道选择的改变而改变），
        // 会从数据库中重新查找，需要新增接口但是尚未实现
        //buddyList添加非自己的所有对象
//        List<String> uriList = new ArrayList<>();
//        for(ProgramDirector director:GlobalVariable.pdrList)
//        {
//            uriList.add("<sip:" + director.sipid + "@" + MainActivity.SIPIP + ">");
//        }
//        uriList.add(MainActivity.zhuboUri);
//        uriList.remove(MainActivity.localUri);
//
//        for(String uri:uriList)
//        {
//            buddyList.add(putData(uri, "SSS"));
//        }

        //处理内通列表（仅耦合器模式）
        if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3){
            //处理左侧呼出部分的显示
            String[] from = { "uri", "username","sipid","character" };
            int[] to = { R.id.text1, R.id.text2,R.id.text3,R.id.text4 };
            buddyListAdapter = new IntercomAdapter(
                    this, buddyList,
                    R.layout.intercom_list_item,
                    from, to);

            buddyListView  = (ListView) findViewById(R.id.intercomListView);
            buddyListView.setAdapter(buddyListAdapter);
            buddyListView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                final View view,
                                                int position, long id)
                        {
                            view.setSelected(true);
                            buddyListSelectedIdx = position;
                        }
                    }
            );

            buddyListView.setOnItemLongClickListener(
                    new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                            try {
    //                            LayoutInflater mInflater = LayoutInflater.from(DailActivity.this);
    //                            View convertView = mInflater.inflate(R.layout.intercom_list_item,parent, false);
                                TextView uriTextView = view.findViewById(R.id.text1);
                                TextView nameView = view.findViewById(R.id.text2);
                                TextView characterView = view.findViewById(R.id.text4);

                                String buddy_uri = uriTextView.getText().toString();
                                String toname = nameView.getText().toString();
                                String character = characterView.getText().toString();


                                //20210522
    //                            MyCall call = new MyCall(MainActivity.account, -1);
    //                            CallOpParam prm = new CallOpParam(true);
    //                            try {
    //                                call.makeCall(buddy_uri, prm);
    //                            } catch (Exception e) {
    //                                call.delete();
    //                            }
                                //intercomCall = call;
                                //需要通过主函数回调获得相关通话状态
    //                            MainActivity.currentCall = call;

                                //应该改成直接转到拨号界面

                                //在呼出之前要先把未保持的电话保持
                                if(GlobalVariable.charactername.equals("director")){
                                    Message m2 = Message.obtain(MainActivity.handler_,1013,null);
                                    m2.sendToTarget();
                                }

                                //拆分uri内容判断是否是跨栏目内通
                                //20210715先使用测试手段都先用跨栏目方式处理
                                if(false){
                                    if(GlobalVariable.charactername.equals("director")){
                                        GlobalVariable.innerrolefrom = "pdr";
                                    }
                                    else{
                                        GlobalVariable.innerrolefrom = "mbc";
                                    }
                                    if(character.equals("pdr")){
                                        GlobalVariable.innerroleto = "pdr";
                                    }
                                    else{
                                        GlobalVariable.innerroleto = "mbc";
                                    }

                                    String tokamsrv = buddy_uri.split("@")[1].replace(">","");
                                    String msg_to_send =
                                            "{ \"msgType\": \"INNER2 MAKE CALL\"" +
                                                    ", \"fromsipid\": \""+ GlobalVariable.localUri+ "\""
                                                    +", \"tosipid\": \""+ buddy_uri+ "\""
                                                    + ", \"frompgm\": \""+ GlobalVariable.channelname + "\""
                                                    + ", \"topgm\": \""+ GlobalVariable.channelname + "\""
                                                    + ", \"fromname\": \""+ GlobalVariable.username + "\""
                                                    + ", \"toname\": \""+ toname + "\""
                                                    + ", \"fromrole\": \""+ GlobalVariable.innerrolefrom + "\""
                                                    + ", \"torole\": \""+ GlobalVariable.innerroleto + "\""
                                                    + ", \"msg\": \"" + "\""
                                                    + ", \"tokamsrv\": \"" +tokamsrv+ "\""
                                                    +" }";
                                    if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);
                                }else{
                                    if(GlobalVariable.charactername.equals("director")){
                                        GlobalVariable.innerrolefrom = "pdr";
                                        if(buddy_uri.equals(GlobalVariable.zhuboUri)){
                                            GlobalVariable.innerroleto = "mbc";
                                        }
                                        else{
                                            GlobalVariable.innerroleto = "pdr";
                                        }

                                    }
                                    else{
                                        GlobalVariable.innerrolefrom = "mbc";
                                        //主播是不可能两个的
                                        GlobalVariable.innerroleto = "pdr";

                                    }

                                    String msg_to_send =
                                            "{ \"msgType\": \"INNER MAKE CALL\"" +
                                                    ", \"fromsipid\": \""+ GlobalVariable.localUri+ "\""
                                                    +", \"tosipid\": \""+ buddy_uri+ "\""
                                                    + ", \"frompgm\": \""+ GlobalVariable.channelname + "\""
                                                    + ", \"topgm\": \""+ GlobalVariable.channelname + "\""
                                                    + ", \"fromname\": \""+ GlobalVariable.username + "\""
                                                    + ", \"toname\": \""+ toname + "\""
                                                    + ", \"fromrole\": \""+ GlobalVariable.innerrolefrom + "\""
                                                    + ", \"torole\": \""+ GlobalVariable.innerroleto + "\""
                                                    + ", \"msg\": \"" + "\""
                                                    +" }";
                                    if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);
                                }

                                GlobalVariable.innerstart = true;
                                GlobalVariable.innercallfrom = GlobalVariable.localUri;
                                GlobalVariable.innercallto = buddy_uri;
                                GlobalVariable.innerchannelfrom = GlobalVariable.channelname;
                                GlobalVariable.innerchannelto = GlobalVariable.channelname;
                                GlobalVariable.innernamefrom = GlobalVariable.username;
                                GlobalVariable.innernameto = toname;

                                //showCallActivity();//20210601在确定回了inner make call再显示

    //                            LinearLayout callinglayout = findViewById(R.id.callingPanel);
    //                            LinearLayout diallayout = findViewById(R.id.dialPanel);
    //                            callinglayout.setVisibility(View.VISIBLE);
    //                            diallayout.setVisibility(View.GONE);

    //                            buddyListView.setEnabled(false);



                                return true;
                            }catch (Exception e){
                                LogClient.generate("【操作错误】"+e.getMessage());
                                e.printStackTrace();
                            }
                            return false;
                        }
                    }
            );

            channelSpinner =  (Spinner) findViewById(R.id.spinnerChannel);
            //获得频道
            List<String> chnameList = new ArrayList();
            String[] channelArray ;
            for(Channel ch : GlobalVariable.channelList)
            {
                String name = "";
                if(ch.getName().equals("-"))
                    name = "";
                else
                    name = ch.getName();
                chnameList.add("   "+ch.getParentname()+" "+name);
            }
            channelArray = chnameList.toArray(new String[chnameList.size()]);
            //String[] channelArray = {"频道1","频道2","频道三"};
            ArrayAdapter<String> spAdapter = new MySpinnerAdapter(this,channelArray);
            channelSpinner.setAdapter(spAdapter);
            channelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Spinner spinner = (Spinner) parent;
                    String pro = (String) spinner.getItemAtPosition(position);
                    //根据选择的频道名称或者postion找到对应的channelID，开启线程访问数据库
                    // 找到pgmid, 获得SIPSRV用于拼接注册地址
                    // 找directbroadcastingroom表中所有的pgmid对应的pdrid和mbcid，
                    // 再根据结果去主播和导播表查找id对应的SIPID 得到访问结果后返回
                    //getSipIPList
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                Long id = -1L;
                                for(Channel ch : GlobalVariable.channelList){
                                    String name = "";
                                    if(ch.getName().equals("-"))
                                        name = "";
                                    else
                                        name = ch.getName();
                                    String temp = "   "+ch.getParentname()+" "+name;

                                    if((temp.equals(pro)))
                                        id = ch.getId();
                                }
                                HttpRequest request = new HttpRequest();
                                String rs = request.postJson(
                                        "http://"+GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getSipIPListByChannel",
                                        "{\"type\":\"get config\", \"channelid\":\"" + id + "\"}"
                                );
                                JSONObject rs_json = new JSONObject(rs);

                                JSONArray rs_json_data = rs_json.getJSONArray("data");
                                intercomUserList = JSON.parseArray(rs_json_data.toString(), IntercomUser.class);
                                Message m2 = Message.obtain(DailActivity.handler_,1003,null);
                                m2.sendToTarget();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        //2021/11/23解除屏蔽
//        if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3){
//            if(GlobalVariable.sipmodel != 0){
//                ImageButton dailbtn = findViewById(R.id.dailMakeCall);
//                ImageButton dailwith0btn = findViewById(R.id.dailMakeCallWith0);
//                dailbtn.setEnabled(false);
//                dailwith0btn.setEnabled(false);
//                dailbtn.setBackgroundResource(R.color.transparent);
//                dailwith0btn.setBackgroundResource(R.color.transparent);
//            }
//        }


    }

    @Override
    protected void onResume() {
        if(GlobalVariable.autocallout){
            dailOut(null);
            GlobalVariable.autocallout = false;
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler_ = null;
    }

    @Override
    public boolean handleMessage(Message m) {

        if (m.what == MainActivity.MSG_TYPE.CALL_STATE) {

            lastCallInfo = (CallInfo) m.obj;
            updateCallState(lastCallInfo);

        }

        if(m.what == 1001)
        {
//            List<String> uriList = new ArrayList<>();
//            for(ProgramDirector director:GlobalVariable.pdrList)
//            {
//                uriList.add("<sip:" + director.sipid + "@" + MainActivity.SIPIP + ">");
//            }
//            uriList.add(MainActivity.zhuboUri);
//            uriList.remove(MainActivity.localUri);
//
//            for(String uri:uriList)
//            {
//                buddyList.add(putData(uri, "SSS"));
//            }
        }
        if(m.what == 1002) {
            if(GlobalVariable.setdailvalue) {
                EditText dailtext = (EditText) findViewById(R.id.dialNumberText);
                dailtext.setText(GlobalVariable.dailvalue);
                GlobalVariable.setdailvalue = false;
            }
        }
        if(m.what == 10021) {
            if(GlobalVariable.setdailvalue) {
                EditText dailtext = (EditText) findViewById(R.id.dialNumberText);
                dailtext.setText(GlobalVariable.dailvalue);
                GlobalVariable.setdailvalue = false;
            }
            if(GlobalVariable.autocallout){
                dailOut(null);
                GlobalVariable.autocallout=false;
            }
        }
        if(m.what == 1003){
            buddyList.clear();
            buddyListAdapter.notifyDataSetChanged();
            for(IntercomUser usr : intercomUserList)
            {
                String uri = "<sip:" + usr.getSipid() + "@" + usr.getKamsrv() + ">";
                if(!uri.equals(MainActivity.localUri)){
                    buddyList.add(putData(uri, usr.getUsername(),usr.getSipid(),usr.getCharacter()));
                    Log.e("添加的内部通话角色为",usr.getSipid()+usr.getCharacter());
                }

            }
            Log.e("buddyList",buddyList.size()+"");
            //buddyListAdapter.notify();
            buddyListAdapter.notifyDataSetChanged();
        }
        else if(m.what == 1005){
            if(m.obj!=null && GlobalVariable.prm!=null){
                //2021-04-09提醒内容更改为自定义格式
                //OnInstantMessageParam prm = (OnInstantMessageParam) m.obj;
//                OnInstantMessageParam prm = GlobalVariable.prm;
//
//                String msgContent = "";
//                String username = "";
//                String template = "";
//                int type = -1;
//                try {
//                    JSONObject msg_json = new JSONObject(prm.getMsgBody());
//                    msgContent = msg_json.get("msg").toString();
//                    username = msg_json.get("username").toString();
//                    template = msg_json.get("template").toString();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(this,msgContent,username,template).create();

                    ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(
                            this,
                            GlobalVariable.im_msg,
                            GlobalVariable.im_username,
                            GlobalVariable.im_template).create();
                    imNotifyDialog.show();
                    imNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                    imNotifyDialog.getWindow().setLayout(500, 300);

            }

        }
        if(m.what == 1010)
        {
            if(GlobalVariable.viewmodel == 1){
                TextView timeView = (TextView) findViewById(R.id.mainTime);
                if (timeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    timeView.setText(dateString.replace(" ", "         "));
                }

                Button ImButton = findViewById(R.id.buttonMA6);
                if(GlobalVariable.IMRedTitle)
                {
                    if(GlobalVariable.IMRedShowed)
                    {
                        ImButton.setTextColor(Color.WHITE);
                        GlobalVariable.IMRedShowed = false;
                    }
                    else
                    {
                        ImButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.IMRedShowed = true;
                    }
                }
                else{
                    ImButton.setTextColor(Color.WHITE);
                }
                /**有未接听电话首页图标闪烁**/
                Button PhoneButton = findViewById(R.id.buttonMA3);
                if(GlobalVariable.PhoneRedTitle){
                    if(GlobalVariable.PhoneRedShowed1){
                        PhoneButton.setTextColor(Color.WHITE);
                        GlobalVariable.PhoneRedShowed1 = false;
                    }else{
                        PhoneButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.PhoneRedShowed1 = true;
                    }
                }else{
                    PhoneButton.setTextColor(Color.WHITE);
                }

                //刷新状态图标
                ImageView mainState1 = (ImageView) findViewById(R.id.MainState1);
                ImageView mainState2 = (ImageView) findViewById(R.id.MainState2);
                ImageView mainState3 = (ImageView) findViewById(R.id.MainState3);

                if(GlobalVariable.callring)
                {
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.allowring));
                }
                else{
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.disablering));
                }

                if(GlobalVariable.whitelistmodel)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.whitelistmodel));
                }
                else if(GlobalVariable.allowblacklist)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.allowblack));
                }
                else{
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.disableblack));
                }

                if(!GlobalVariable.online)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall == 1)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.busy));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall==2) {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.linebusy));
                    mainState3.setColorFilter(Color.TRANSPARENT);
                }
                else {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.allowin));
                    mainState3.setColorFilter(Color.WHITE);
                }

                TextView mainChannel = findViewById(R.id.mainChannel);
                TextView mainSipID = findViewById(R.id.mainSipID);
                mainChannel.setText(GlobalVariable.channelname);
                mainSipID.setText(GlobalVariable.user_sipid);
            }
            else if(GlobalVariable.viewmodel == 2) {

                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);

                Typeface typeface_fitcan = Typeface.createFromAsset(this.getAssets(), "fonts/BGOTHM.TTF");
                ct_title_username.setTypeface(typeface_fitcan);
                ct_title_name.setTypeface(typeface_fitcan);

                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_pdr));
                }
            }
            else if(GlobalVariable.viewmodel == 3){
                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);
                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_pdr));
                }
            }
        }
        else if(m.what == 1011) {
            if(GlobalVariable.viewmodel == 1){
                ImageView titleImage = findViewById(R.id.titleImage);
                if(String.valueOf(m.obj).equals("success")){
                    titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo));
                }
                else{
                    titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo_red));
                }
            }
            else if(GlobalVariable.viewmodel == 2){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(m.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.ct_gray));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }
            else if(GlobalVariable.viewmodel == 3){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(m.obj).equals("success")) {
                    tvchannel.setTextColor(Color.WHITE);
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }
        }
        else if(m.what == 1014){
            String msg_content = String.valueOf(m.obj);
            MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"系统状态已更改\n"+msg_content).create();
            msgNotifyDialog.show();
            if(GlobalVariable.viewmodel == 1)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            msgNotifyDialog.getWindow().setLayout(500, 400);
            final Timer t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    msgNotifyDialog.dismiss();
                    t.cancel();
                }
            }, 2000);
        }
        else if(m.what == 1015){
            showMainActivity(null);
        }
        else if(m.what == 1016){
            //处理耦合器模式线路选择拨号
            String contacturi = String.valueOf(m.obj);
            Integer prefix_id = GlobalVariable.ctNumberMap.get(contacturi);
            String prefix = "0"+prefix_id;
            EditText dailtext = findViewById(R.id.dialNumberText);
            String phonenumber = "";
            if(click_btn_index == 1){
                phonenumber = prefix+ dailtext.getText().toString();
            }
            else if(click_btn_index == 2){
                phonenumber = prefix+"0"+ dailtext.getText().toString();
            }
            String daoboUri = "";
            String zhuboUri = "";
            if(GlobalVariable.charactername.equals("director"))
            {
                daoboUri = MainActivity.localUri;
            }
            else
            {
                zhuboUri = MainActivity.zhuboUri;
            }
            phonenumber = phonenumber.replace("#","A");
            String msg_to_send =
                    "{ \"msgType\": \"DAIL OUT CALL\", " +
                            "\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
                            "\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
                            "\"Pstn\": \""+ phonenumber+ "\", " +
                            "\"contacturi\": \""+ contacturi+ "\"" +
                            " }";
            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

            List<String> numberlist = new ArrayList<>();
            numberlist.add("<sip:"+phonenumber+"@"+GlobalVariable.EUSERVER+">");
            numberlist.add(contacturi);
            Message m2 = Message.obtain(MainActivity.handler_,1008,numberlist);
            m2.sendToTarget();

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
        /**话筒摘机/挂机逻辑处理**/
		else if(m.what == 2000) {
            //挂机逻辑处理
            if(MainActivity.allCallinList.size()==0){
                //回到主页面
                showMainActivity(null);
            }
            else if(MainActivity.allCallinList.size()>0){
                Message m2 = Message.obtain(MainActivity.handler_,2001,null);
                m2.sendToTarget();
            }
        }
		else if(m.what == 2001){
		    //摘机逻辑处理
            if(MainActivity.allCallinList.size()==0){
                dailOut(null);
            }
            else if(MainActivity.allCallinList.size()>0){
                //不操作
                //有新来电直接切到首页变成首页逻辑处理
            }
        }
        return true;
    }

    private void updateCallState(CallInfo ci) {
        GifImageView gifView = (GifImageView)findViewById(R.id.intercomGif);
        ImageView gifNoneView = (ImageView)findViewById(R.id.intercomGifNone);
        TextView tvPeer  = (TextView) findViewById(R.id.intercomPeer);
        TextView tvState = (TextView) findViewById(R.id.intercomState);
        String call_state = "";

        if (ci == null) {
            gifView.setVisibility(View.GONE);
            gifNoneView.setVisibility(View.VISIBLE);

            tvState.setText("电话挂断");
            return;
        }

        if (ci.getRole() == pjsip_role_e.PJSIP_ROLE_UAC) {
            //buttonAccept.setVisibility(View.GONE);
        }

        if (ci.getState() <
                pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED)
        {
            if (ci.getRole() == pjsip_role_e.PJSIP_ROLE_UAS) {
                call_state = "来电";
                /* Default button texts are already 'Accept' & 'Reject' */
            } else {
                //buttonHangup.setText("取消");
                call_state = ci.getStateText();
                //自己拨出电话播出成功后
            }
        }
        else if (ci.getState() >=
                pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED)
        {
            //buttonAccept.setVisibility(View.GONE);
            call_state = ci.getStateText();
            if(call_state!=null && call_state.equals("CONFIRMED")) {
                call_state = "已接通";
            }
            if (ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED) {
                //buttonHangup.setText("挂断");
                gifView.setVisibility(View.VISIBLE);
                gifNoneView.setVisibility(View.GONE);

            } else if (ci.getState() ==
                    pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED)
            {
                //buttonHangup.setText("确认");
                gifView.setVisibility(View.GONE);
                gifNoneView.setVisibility(View.VISIBLE);
                call_state = "电话挂断: " + ci.getLastReason();
//                int id = ci.getId();
//                Log.e("测试CALLID--CALLINFO",id+"");
//
//                id = MainActivity.currentCall.getId();
//                Log.e("测试CALLID--CURRENT",id+"");

                int id = ci.getId();
                Log.e("测试CALLID--CALLINFO",id+"");
                Call call = Call.lookup(id);
                CallOpParam prm = new CallOpParam();
                prm.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
                try {
                    if(call!=null) {
                        //call.hangup(prm);
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                if(call!=null){
                    call.delete();
                }


                //MainActivity.currentCall.delete();
            }
        }
        //20210514不再显示remoteuri显示频道和用户名

        String showname = "";
        String rm_uri = ci.getRemoteUri();
        showname = rm_uri;
        String[] rm_uri_split = rm_uri.split("@");
        String sipid = rm_uri_split[0].replace("<sip:","");


        for(UserInfo info : GlobalVariable.userinfoList)
        {
            if(info.getSipid()!=null && info.getSipid().equals(sipid));
            showname = info.getChannelname()+" "+info.getUsername();
        }
        tvPeer.setText(showname);
        //tvPeer.setText(ci.getRemoteUri());
        tvState.setText(call_state);
    }

    public void clickNumber(View view)
    {

//        if(current_dailout!=null){
//            String msg_to_send =
//                    "{ \"msgType\": \"DAIL OUT CALL\", \"DaoBoCallUri\": \""
//                            + current_dailout.daobocalluri
//                            + "\", \"ZhuBoCallUri\": \""
//                            + MainActivity.zhuboUri
//                            + "\", \"Pstn\": \""
//                            + current_dailout.pstn
//                            + "\", \"OutCallUri\": \""
//                            + current_dailout.outcalluri
//                            + "\" }";
////            Intent intent_ack = new Intent(this, JWebSocketClientService.class);
////            intent_ack.putExtra("msg_to_send", msg_to_send);
////            startService(intent_ack);
//            MainActivity.wsControl.sendMessage(msg_to_send);
//        }
//        else
//        {
            //根据Button的Text决定输入值
            Button clickbutton = (Button)view;
            String number = (String) clickbutton.getText();
            EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
            dailtext.setText(dailtext.getText().toString()+number);
//        }
    }
    public void subNumber(View view)
    {

        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        String old_value = dailtext.getText().toString();
        if(old_value.length()>0)
        {
            dailtext.setText(old_value.substring(0,old_value.length() - 1));
        }
    }

    public void dailOut(View view)
    {
        if(GlobalVariable.charactername.equals("broadcaster")&&(GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2 || GlobalVariable.sipmodel == 3))
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(DailActivity.this,"请在导播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }
        if(GlobalVariable.charactername.equals("director")&& GlobalVariable.sipmodel == 0)
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(DailActivity.this,"请在主播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }

        click_btn_index = 1;

        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        String temp = String.valueOf(dailtext.getText());
        if(temp.equals("")||temp.equals("null")){
            if(view!=null)
                view.setEnabled(true);
            return;
        }
        //不再使用本地拨号的机制
//        MyCall call = new MyCall(MainActivity.account, -1);
//        CallOpParam prm = new CallOpParam(true);
//        try {
//            EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
//            String phonenumber = dailtext.getText().toString();
//            call.makeCall("<sip:"+phonenumber+"@"+GlobalVariable.SIPSERVER+">", prm);
//        } catch (Exception e) {
//            call.delete();
//            return;
//        }
//        interCall = call;
//
//        LinearLayout callinglayout = findViewById(R.id.callingPanel);
//        LinearLayout diallayout = findViewById(R.id.dialPanel);
//        callinglayout.setVisibility(View.VISIBLE);
//        diallayout.setVisibility(View.GONE);

        if(GlobalVariable.isDaLian){
            if(GlobalVariable.diallinemodel == 1){
                //只有一个有效线路的时候让用户直接就呼出
                ArrayList<Map<String, String>> lineList = new ArrayList<>();
                int line = -1;
                for(int i = 1;i<7;i++){
                    //当前线是否是注册线路
                    boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);
                    //当前线是否被已有电话占用
                    boolean line_occupy = false;
//                    try {
//                        int id_index = GlobalVariable.ctIdMap.get(i);
//                        MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//                        line_occupy = true;
//                    }catch (Exception e){
//                        e.printStackTrace();
//                        line_occupy = false;
//                    }
                    if(GlobalVariable.isDaLian){
                        //判断外线号码和当前号码相同判断线路被占用
                        for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                            if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                                line_occupy = true;
                            }
                        }
                    }
                    if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
                        line_occupy = true;
                    }

                    if(line_exist && !line_occupy){
                        line = i;
                        HashMap<String, String> item = new HashMap<String, String>();
                        item.put("linename", "线路"+line);
                        item.put("linenumber", GlobalVariable.ctNumberMapRevert.get(line));
                        lineList.add(item);
                    }
                }

                if(lineList.size() == 1){
                    Map<String,String> item = null;
                    try{
                        item = lineList.get(0);
                    }
                    catch (Exception e){}
                    if(item!=null) {
                        String contacturi = item.get("linenumber");
                        Message m2 = Message.obtain(DailActivity.handler_, 1016, contacturi);
                        m2.sendToTarget();
                    }
                }
                else{
                    //弹出子窗口选择空闲线路呼出，如果无有效空闲线路，直接返回
                    LineChoseDialog lineChoseDialog = new LineChoseDialog.Builder(DailActivity.this).create();
                    lineChoseDialog.show();
                    lineChoseDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                    lineChoseDialog.getWindow().setLayout(500, 400);
                }
            }
        }
        else if(GlobalVariable.viewmodel == 2){
            //拨号之前预选线路，确定当前有几路电话，确定预配置电话总路数
            //确定是否有空闲的可外呼线路
            //确定是固定呼出线路模式还是自动分配线路模式
            //如无空闲线路提示当前无空闲线路并返回
            if(GlobalVariable.diallinemodel == 1){
                //弹出子窗口选择空闲线路呼出，如果无有效空闲线路，直接返回
                LineChoseDialog lineChoseDialog = new LineChoseDialog.Builder(DailActivity.this).create();
                lineChoseDialog.show();
                lineChoseDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                lineChoseDialog.getWindow().setLayout(500, 400);
            }
            else if(GlobalVariable.diallinemodel == 2){
                //固定线路是哪一个也得配置
                //功能待定

            }
        }
        else {
            view.setEnabled(false);
            String phonenumber = "00" + dailtext.getText().toString();
            String daoboUri = "";
            String zhuboUri = "";
            if (GlobalVariable.charactername.equals("director")) {
                daoboUri = MainActivity.localUri;
            } else {
                zhuboUri = MainActivity.zhuboUri;
            }
            phonenumber = phonenumber.replace("#","A");
            String msg_to_send =
                    "{ \"msgType\": \"DAIL OUT CALL\", " +
                            "\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
                            "\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
                            "\"Pstn\": \""+ phonenumber+ "\", " +
                            "\"contacturi\": \""+ "0000"+ "\"" +
                            " }";
            if (MainActivity.wsControl != null) MainActivity.wsControl.sendMessage(msg_to_send);

            List<String> numberlist = new ArrayList<>();
            numberlist.add("<sip:"+phonenumber+"@"+GlobalVariable.EUSERVER+">");
            numberlist.add("0000");
            Message m2 = Message.obtain(MainActivity.handler_, 1008, numberlist);
            m2.sendToTarget();

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }
    }
    public void dailOutWith0(View view)
    {
        if(GlobalVariable.charactername.equals("broadcaster")&&(GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2 || GlobalVariable.sipmodel == 3))
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(DailActivity.this,"请在导播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }
        if(GlobalVariable.charactername.equals("director")&& GlobalVariable.sipmodel == 0)
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(DailActivity.this,"请在主播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }

        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);

        String temp = String.valueOf(dailtext.getText());
        if(temp.equals("")||temp.equals("null")){
            view.setEnabled(true);
            return;
        }
        click_btn_index = 2;
        if(GlobalVariable.isDaLian){
            if(GlobalVariable.diallinemodel == 1){
                //弹出子窗口选择空闲线路呼出，如果无有效空闲线路，直接返回
                LineChoseDialog lineChoseDialog = new LineChoseDialog.Builder(DailActivity.this).create();
                lineChoseDialog.show();
                lineChoseDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                lineChoseDialog.getWindow().setLayout(500, 400);
            }
        }
        else if(GlobalVariable.viewmodel == 2){
            //拨号之前预选线路，确定当前有几路电话，确定预配置电话总路数
            //确定是否有空闲的可外呼线路
            //确定是固定呼出线路模式还是自动分配线路模式
            //如无空闲线路提示当前无空闲线路并返回
            if(GlobalVariable.diallinemodel == 1){
                //弹出子窗口选择空闲线路呼出，如果无有效空闲线路，直接返回
                LineChoseDialog lineChoseDialog = new LineChoseDialog.Builder(DailActivity.this).create();
                lineChoseDialog.show();
                lineChoseDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                lineChoseDialog.getWindow().setLayout(500, 400);
            }
            else if(GlobalVariable.diallinemodel == 2){
                //固定线路是哪一个也得配置
                //功能待定
            }
        }
        else{
            view.setEnabled(false);
            String phonenumber = "0"+"00"+ dailtext.getText().toString();
            String daoboUri = "";
            String zhuboUri = "";
            if(GlobalVariable.charactername.equals("director"))
            {
                daoboUri = MainActivity.localUri;
            }
            else
            {
                zhuboUri = MainActivity.zhuboUri;
            }
            phonenumber = phonenumber.replace("#","A");
            String msg_to_send =
                    "{ \"msgType\": \"DAIL OUT CALL\", " +
                            "\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
                            "\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
                            "\"Pstn\": \""+ phonenumber+ "\", " +
                            "\"contacturi\": \""+ "0000"+ "\"" +
                            " }";
            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

            List<String> numberlist = new ArrayList<>();
            numberlist.add("<sip:"+phonenumber+"@"+GlobalVariable.EUSERVER+">");
            numberlist.add("0000");
            Message m2 = Message.obtain(MainActivity.handler_, 1008, numberlist);
            m2.sendToTarget();

            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }

    }

    public void dailOutWith1(View view)
    {
        //该函数用于测试拨打SIP内线号码如9001，需配合修改kamailio的cfg配置使用
        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);

        String[] temp = String.valueOf(dailtext.getText()).replace("*",".").split("#");

        //String temp = String.valueOf(dailtext.getText());
        if(temp[0].equals("")||temp[0].equals("null")){
            view.setEnabled(true);
            return;
        }
        click_btn_index = 2;

        String phonenumber = temp[0];
        String daoboUri = "";
        String zhuboUri = "";
        if(GlobalVariable.charactername.equals("director"))
        {
            daoboUri = MainActivity.localUri;
        }
        else
        {
            zhuboUri = MainActivity.zhuboUri;
        }
        String msg_to_send =
                "{ \"msgType\": \"DAIL OUT CALL\", " +
                        "\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
                        "\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
                        "\"Pstn\": \""+ phonenumber+ "\", " +
                        "\"contacturi\": \""+ phonenumber+ "\", " +
                        "\"server\": \""+ temp[1]+ "\"" +
                        " }";
        if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

        List<String> numberlist = new ArrayList<>();
        numberlist.add("<sip:"+phonenumber+"@"+temp[1]+">");
        numberlist.add(phonenumber);
        Message m2 = Message.obtain(MainActivity.handler_, 1008, numberlist);
        m2.sendToTarget();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);


    }

    public void dailHangup(View view)
    {
//        Button dailbutton = (Button)findViewById(R.id.dailMakeCall);
//        dailbutton.setEnabled(true);

        //buddyListView.setEnabled(true);

//        LinearLayout callinglayout = findViewById(R.id.callingPanel);
//        LinearLayout diallayout = findViewById(R.id.dialPanel);
//
//        callinglayout.setVisibility(View.GONE);
//        diallayout.setVisibility(View.VISIBLE);

//        ImageButton dialbutton = findViewById(R.id.dailMakeCall);
//        dialbutton.setEnabled(true);

//        if(MainActivity.currentCall!=null)
//        {
//            CallOpParam prm = new CallOpParam();
//            prm.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
//            try {
//                int id = MainActivity.currentCall.getId();
//                MainActivity.currentCall.hangup(prm);
//
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//        }

    }

    public void dailHide(View view){

        if(MainActivity.currentCall!=null)
        {//在退出界面前先挂断电话
            dailHangup(null);
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);//可以跳过中间栈直接实现两个界面间的切换
        startActivity(intent);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {//主界面特有的按键映射
        int keycode = event.getKeyCode();
        if(event.getAction()== KeyEvent.ACTION_DOWN)
        {
            switch(keycode)
            {
                case KeyEvent.KEYCODE_0:
                    sendClickMessage("0");
                    break;
                case KeyEvent.KEYCODE_1:
                    sendClickMessage("1");
                    break;
                case KeyEvent.KEYCODE_2:
                    sendClickMessage("2");
                    break;
                case KeyEvent.KEYCODE_3:
                    sendClickMessage("3");
                    break;
                case KeyEvent.KEYCODE_4:
                    sendClickMessage("4");
                    break;
                case KeyEvent.KEYCODE_5:
                    sendClickMessage("5");
                    break;
                case KeyEvent.KEYCODE_6:
                    sendClickMessage("6");
                    break;
                case KeyEvent.KEYCODE_7:
                    sendClickMessage("7");
                    break;
                case KeyEvent.KEYCODE_8:
                    sendClickMessage("8");
                    break;
                case KeyEvent.KEYCODE_9:
                    sendClickMessage("9");
                    break;
                case KeyEvent.KEYCODE_STAR:
                    sendClickMessage("*");
                    break;
                case KeyEvent.KEYCODE_POUND:
                    sendClickMessage("#");
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void sendClickMessage(String number){
        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        dailtext.setText(dailtext.getText().toString()+number);

    }

    private void showCallActivity()
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, CallActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//可以跳过中间栈直接实现两个界面间的切换
        startActivity(intent);
    }

    public void showMainActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_home_disabled));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_home_disabled));
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showHistoryActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_history_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_history_disable));
        }
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showContactsActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_contacts_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_contacts_disable));
        }
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showCommunicateActivity(View view)
    {
        Intent intent = new Intent(this, CommunicateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showSettingActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_setting_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_setting_disable));
        }
        Intent intent = new Intent(this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showIntercomActivity(View view)
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, IntercomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        //setContentView(layoutIntercom);
    }
    public void showDailActivity(View view){

    }

    private HashMap<String, String> putData(String uri, String username, String sipid,String character)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("uri", uri);
        item.put("username", username);
        item.put("sipid", sipid);
        item.put("character",character==null?"pdr":character);//20210709追加用户角色
        return item;
    }


}

package org.pjsip.pjsua2.app;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpRequest {
    private static final MediaType JSON=MediaType.parse("application/json; charset=utf-8");
    private String url;
    private String json;
    private String token;
    private String param;

    public String postJson(String Url, String Json) {
        url = Url;
        json = Json;
        //申明给服务端传递一个json串
        //创建一个OkHttpClient对象
        OkHttpClient okHttpClient = new OkHttpClient();
        //创建一个RequestBody(参数1：数据类型 参数2传递的json串)
        RequestBody requestBody = RequestBody.create(JSON, json);
        //创建一个请求对象
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        //发送请求获取响应
        try {
            Response response=okHttpClient.newCall(request).execute();
            //判断请求是否成功
            if(response.isSuccessful()){
                //打印服务端返回结果
                //Log.i("HttpRequest",response.body().string());
                return response.body().string();
            }
        } catch (IOException e) {
            //2021-09-23屏蔽
            //e.printStackTrace();
            return "error";
        }
        return null;
    }

    public String postJsonWithToken(String Url, String Param,String Token)
    {
        url = Url;
        param = Param;
        token = Token;
        OkHttpClient okHttpClient = new OkHttpClient();
        //创建一个请求对象
        Request request = new Request.Builder().url(url+param).addHeader("x-auth-token",token).post(null).build();
        try {
            Response response=okHttpClient.newCall(request).execute();
            //判断请求是否成功
            if(response.isSuccessful()){
                //打印服务端返回结果
                //Log.i("HttpRequest",response.body().string());
                return response.body().string();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "error";
        }
        return null;
    }


}

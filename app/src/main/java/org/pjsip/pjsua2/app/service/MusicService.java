package org.pjsip.pjsua2.app.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class MusicService extends Service {
    //private String path = String.valueOf(R.raw.ringcall);//"http://"+ GlobalVariable.EUSERVER +":8080/record/ringcall.mp3";
    private String path = "http://"+ GlobalVariable.EUSERVER +":8080/record/ringcall.mp3";

    //Uri setDataSourceuri = Uri.parse("android.resource://org.pjsip.pjsua2/"+R.raw.ringcall);

    private MediaPlayer player;

    public MusicService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        //当执行完了onCreate后，就会执行onBind把操作歌曲的方法返回
        return new MyBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //这里只执行一次，用于准备播放器
        //player = new MediaPlayer();
        player=MediaPlayer.create(this, R.raw.ringtone);
        try {
            //player.setDataSource(path);
            //异步准备
            player.prepareAsync();

            //添加准备好的监听
            player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //如果准备好了，就会进行这个方法
                    //mp.start();
//                    try {
//                        mp.prepare();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                }
            });
        }
//        catch (IOException e) {
//            e.printStackTrace();
//        }
        catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("服务", "准备播放音乐");

        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer arg0) {
                player.start();
                player.setLooping(true);

            }
        });

    }

    //该方法包含关于歌曲的操作
    public class MyBinder extends Binder {

        //判断是否处于播放状态
        public boolean isPlaying(){
            try {
                return player.isPlaying();
            }catch(Exception e){
                return false;
            }
        }

        //播放或暂停歌曲
        public void play() {
//            if(!player.isPlaying()) {
            try {
                //player.prepareAsync();
                player.start();
                //player.setLooping(true);
                Log.e("服务", "播放音乐");
            }catch (Exception e)
            {
                e.printStackTrace();
            }
//            }
        }

        public void pause() {
//            if(!player.isPlaying()) {
            try {
                player.pause();
                Log.e("服务", "停止音乐");
            }catch (Exception e)
            {
                e.printStackTrace();
            }
                //           }
        }

        //返回歌曲的长度，单位为毫秒
        public int getDuration(){
            return player.getDuration();
        }

        //返回歌曲目前的进度，单位为毫秒
        public int getCurrenPostion(){
            return player.getCurrentPosition();
        }

        //设置歌曲播放的进度，单位为毫秒
        public void seekTo(int mesc){
            player.seekTo(mesc);
        }
    }

}
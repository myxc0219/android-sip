package org.pjsip.pjsua2.app.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import java.util.ArrayList;

import vdroid.api.external.ExtraInfo;

public class IntentUtils {
    public static void openDialer(Context context) {
        Intent intent = new Intent();
        Uri uri = Uri.parse("sip:");
        intent.setAction("vdroid.intent.action.DIAL");
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void dialNumber(Context context, String number, int sip) {
        Intent intent = new Intent();
        Uri uri = Uri.parse("sip:" + number + "@" + sip);
        intent.setAction("vdroid.intent.action.DIAL");
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void call(Context context, String number, int sip, boolean video) {
        Intent intent = new Intent();
        Uri uri = Uri.parse("sip:" + number + "@" + sip);
        intent.setAction("vdroid.intent.action.CALL");
        intent.setData(uri);
        intent.putExtra("isVideo", video);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void answer(Context context, int callID) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.ANSWER");
        intent.putExtra("call_id", callID);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void callEnd(Context context, int callID) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CALL_END");
        intent.putExtra("call_id", callID);
        context.sendBroadcast(intent);
    }

    public static void updateCallNumber(Context context, int callID, String number, String name, String iconUrl, String company, String jobTitle, String location, String group) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CALL_NUMBER_UPDATE");
        Bundle bundle = new Bundle();
        bundle.putInt("call_id", callID);
        bundle.putString("number", number);
        bundle.putString("name", name);
        bundle.putString("iconUrl", iconUrl);
        bundle.putString("company", company);
        bundle.putString("jobTitle", jobTitle);
        bundle.putString("location", location);
        bundle.putString("group", group);
        intent.putExtra("call_update", bundle);
        context.sendBroadcast(intent);
    }

    public static void updateAudioDevice(Context context, int device) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.AUDIO_DEVICE_UPDATE");
        intent.putExtra("device", device);
        context.sendBroadcast(intent);
    }

    public static void updateLED(Context context, int type, int cmd) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.LED_UPDATE");
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putInt("cmd", cmd);
        intent.putExtra("led", bundle);
        context.sendBroadcast(intent);
    }

    public static void requestConfig(Context context, int id, ArrayList<String> key) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CONFIG_REQUEST");
        Bundle bundle = new Bundle();
        bundle.putInt("id", id);
        bundle.putStringArrayList("key", key);
        intent.putExtra("config", bundle);
        context.sendBroadcast(intent);
    }

    public static void updateConfig(Context context, ArrayList<String> key) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CONFIG_UPDATE");
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("key", key);
        intent.putExtra("config", bundle);
        context.sendBroadcast(intent);
    }

    public static void watchConfig(Context context, ArrayList<String> key) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CONFIG_WATCHER");
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("key", key);
        intent.putExtra("config", bundle);
        context.sendBroadcast(intent);
    }

    public static void requestLineState(Context context, int type) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.LINE_STATE_REQUEST");
        intent.putExtra("type", type);
        context.sendBroadcast(intent);
    }

    public static void openContacts(Context context) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CONTACTS");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void updateCallLog(Context context, ArrayList<ExtraInfo> list) {
        Intent intent = new Intent();
        intent.setAction("vdroid.intent.action.CALL_LOG_UPDATE");
        intent.putParcelableArrayListExtra("call_log", list);
        context.sendBroadcast(intent);
    }

    public static void sendTo(Context context, String number) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        String uriString = "smsto:"+number;
        Uri uri = Uri.parse(uriString);
        intent.setData(uri);
        intent.putExtra("line", 1);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}

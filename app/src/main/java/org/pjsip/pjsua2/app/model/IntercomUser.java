package org.pjsip.pjsua2.app.model;

public class IntercomUser {

    private String username;
    private Long userid;
    private String kamsrv;
    private String sipid;
    private String character;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getKamsrv() {
        return kamsrv;
    }

    public void setKamsrv(String kamsrv) {
        this.kamsrv = kamsrv;
    }

    public String getSipid() {
        return sipid;
    }

    public void setSipid(String sipid) {
        this.sipid = sipid;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }
}

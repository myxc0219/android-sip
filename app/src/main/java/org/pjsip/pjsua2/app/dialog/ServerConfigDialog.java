package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.pjsip.pjsua2.app.LoginActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class ServerConfigDialog extends Dialog {

    private ServerConfigDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;


        private ServerConfigDialog mDialog;

        private Button mSaveButton;
        private EditText mServerIpText;


        public Builder(Context context) {
            mContext = context;
            mDialog = new ServerConfigDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_server_config, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mServerIpText = mLayout.findViewById(R.id.serverIpText);
            mServerIpText.setText(GlobalVariable.SIPSERVER);
            mSaveButton = mLayout.findViewById(R.id.serverIpSave);
        }


        public ServerConfigDialog create() {


            mSaveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GlobalVariable.SIPSERVER = String.valueOf(mServerIpText.getText());

                    SharedPreferences sharedPreferencesx = mContext.getSharedPreferences("userconfig", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferencesx.edit();//获取编辑器
                    editor.putString("sipserver",GlobalVariable.SIPSERVER);
                    editor.commit();//提交修改

                    Message m2 = Message.obtain(LoginActivity.handler_,1002,null);
                    m2.sendToTarget();
                    mDialog.dismiss();
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }
}

package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.model.CallRecord;
import org.pjsip.pjsua2.app.util.EditTextUtils;

import java.util.Date;

public class HistoryDialog extends Dialog {

    private HistoryDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private Button mbtnHISConfirm;

        private View.OnClickListener mButtonClickListener;

        private HistoryDialog mDialog;

        private CallRecord callRecord;


        private TextView mtextStarttime;
        private TextView mtextDuration;
        private TextView mtextName;
        private TextView mtextPstn;
        private TextView mtextPstnmark;
        private TextView mtextContent;
        private TextView mtextPdrmark;
        private TextView mtextMbcmark;



        private TextView mtvHisStartTime;
        private TextView mtvHisDuration;
        private TextView mtvHisName;
        private TextView mtvHisPstn;
        private TextView mtvHisPstnmark;
        private TextView mtvHisContent;
        private TextView mtvHisPdrmark;
        private TextView mtvHisMbcmark;





        public Builder(Context context, CallRecord record) {
            mContext = context;
            callRecord = record;
            mDialog = new HistoryDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_history_detail, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mbtnHISConfirm = mLayout.findViewById(R.id.btnHISConfirm);

            mtextStarttime = (TextView)mLayout.findViewById(R.id.textHISStarttime);
            mtextDuration = (TextView)mLayout.findViewById(R.id.textHISDuration);
            mtextName = (TextView)mLayout.findViewById(R.id.textHISName);
            mtextPstn = (TextView)mLayout.findViewById(R.id.textHISPstn);
            mtextPstnmark = (TextView)mLayout.findViewById(R.id.textHISPstnmark);
            mtextContent = (TextView)mLayout.findViewById(R.id.textHISContent);
            mtextPdrmark = (TextView)mLayout.findViewById(R.id.textHISPdrmark);
            mtextMbcmark = (TextView)mLayout.findViewById(R.id.textHISMbcmark);

            mtvHisStartTime = (TextView)mLayout.findViewById(R.id.tvHisStartTime);
            mtvHisDuration = (TextView)mLayout.findViewById(R.id.tvHisDuration);
            mtvHisName = (TextView)mLayout.findViewById(R.id.tvHisName);
            mtvHisPstn = (TextView)mLayout.findViewById(R.id.tvHisPstn);
            mtvHisPstnmark = (TextView)mLayout.findViewById(R.id.tvHisPstnmark);
            mtvHisContent = (TextView)mLayout.findViewById(R.id.tvHisContent);
            mtvHisPdrmark = (TextView)mLayout.findViewById(R.id.tvHisPdrmark);
            mtvHisMbcmark = (TextView)mLayout.findViewById(R.id.tvHisMbcmark);

            try {
                mtextStarttime.setText(record.starttime==null?"00:00": GlobalVariable.hm_df.format(GlobalVariable.sdf.parse(record.starttime)));
            }catch (Exception e){}

            //展示录音时长
            String duration = GlobalVariable.getRingDuring(record.recordpath);
            if(duration == null)
            {
                duration = "00:00";
            }
            else{
                duration = GlobalVariable.ms_df.format(new Date(Long.parseLong(duration)));
            }
            mtextDuration.setText(duration);
            //需要根据pstn查找用户姓名，如果没有用户名再显示空
            if(GlobalVariable.pstn_name_KV.get(record.callingnumber)!=null)
                mtextName.setText(GlobalVariable.pstn_name_KV.get(record.callingnumber));
            mtextPstn.setText(record.callingnumber==null?"":record.callingnumber);
            mtextPstnmark.setText(record.pstnmark==null?"":record.pstnmark);
            mtextContent.setText(record.content==null?"":record.content);
            mtextPdrmark.setText(record.pdrmark==null?"":record.pdrmark);
            mtextMbcmark.setText(record.mbcmark==null?"":record.mbcmark);

            if(false){//if(GlobalVariable.viewmodel == 2){
                mtextStarttime.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextDuration.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextName.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextPstn.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextPstnmark.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextContent.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextPdrmark.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtextMbcmark.setTextColor(mContext.getResources().getColor(R.color.ct_gray));

                mtextStarttime.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextDuration.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextName.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextPstn.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextPstnmark.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextContent.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextPdrmark.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                mtextMbcmark.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));

                mtvHisStartTime.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisDuration.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisName.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisPstn.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisPstnmark.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisContent.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisPdrmark.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvHisMbcmark.setTextColor(mContext.getResources().getColor(R.color.ct_gray));

                mbtnHISConfirm.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mbtnHISConfirm.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            }
        }


//        /**
//         * 设置按钮文字和监听
//         */
//        public Builder setButton(@NonNull String text, View.OnClickListener listener) {
//            mButton.setText(text);
//            mButtonClickListener = listener;
//            return this;
//        }

        public HistoryDialog create() {



            mbtnHISConfirm.setOnClickListener(view -> {
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });
            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }


    }

    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), this.getContext());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}
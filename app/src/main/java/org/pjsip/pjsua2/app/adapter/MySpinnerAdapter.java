package org.pjsip.pjsua2.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class MySpinnerAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private String [] mStringArray;
    public MySpinnerAdapter(Context context, String[] stringArray) {

        super(context, android.R.layout.simple_spinner_item, stringArray);
        mContext = context;
        mStringArray = stringArray;
    }





    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        //修改Spinner展开后的字体颜色
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent,false);
        }

        convertView.setBackground(mContext.getDrawable(R.color.transparent));
        //此处text1是Spinner默认的用来显示文字的TextView
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);

        tv.setText(mStringArray[position]);
        tv.setTextSize(28f);
        if(GlobalVariable.viewmodel == 1)
        {
            tv.setTextColor(Color.WHITE);
            tv.setBackgroundColor(convertView.getResources().getColor(R.color.translucentblack));
        }
        else if(GlobalVariable.viewmodel == 2){
            tv.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
            tv.setBackgroundColor(convertView.getResources().getColor(R.color.translucent));
        }
        else if(GlobalVariable.viewmodel == 3){
            tv.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
            tv.setBackgroundColor(convertView.getResources().getColor(R.color.translucent));
        }

        return convertView;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 修改Spinner选择后结果的字体颜色
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
        }

        //此处text1是Spinner默认的用来显示文字的TextView
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(mStringArray[position]);

        if(GlobalVariable.viewmodel == 1){
            tv.setTextSize(28f);
            tv.setTextColor(Color.WHITE);
        }
        else if(GlobalVariable.viewmodel == 2){
            tv.setTextSize(24f);
            //tv.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
            tv.setTextColor(Color.WHITE);
        }
        else if(GlobalVariable.viewmodel == 3){
            tv.setTextSize(24f);
            tv.setTextColor(Color.WHITE);
        }

        return convertView;
    }

}

package org.pjsip.pjsua2.app.model;

public class CallRecord {
    public String callid;
    public Integer type;
    public String callingnumber;
    public String callednumber;
    public String starttime;
    public String endtime;
    public String callduration;
    public Long channelid;
    public Long columnid;
    public Long director;
    public Long broadcaster;
    public String content;
    public Integer state;
    public String recordpath;

    public String pstnmark;
    public String pdrmark;
    public String mbcmark;

    public Integer operatetype;

}

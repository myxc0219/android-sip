package org.pjsip.pjsua2.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListPopupWindow;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.OnInstantMessageParam;
import org.pjsip.pjsua2.SendInstantMessageParam;
import org.pjsip.pjsua2.app.adapter.ChattingAdapter;
import org.pjsip.pjsua2.app.adapter.IntercomAdapter;
import org.pjsip.pjsua2.app.adapter.MySpinnerAdapter;
import org.pjsip.pjsua2.app.dialog.MsgNotifyDialog;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.Channel;
import org.pjsip.pjsua2.app.model.ChatMessage;
import org.pjsip.pjsua2.app.model.IntercomUser;
import org.pjsip.pjsua2.app.model.ProgramDirector;
import org.pjsip.pjsua2.app.spinner.MySpinner;
import org.pjsip.pjsua2.app.util.EditTextUtils;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class CommunicateActivity extends BaseActivity implements Handler.Callback {

    //对话List使用//////////////////////////
    protected static final String TAG = "MainActivity";
    private ChattingAdapter chatHistoryAdapter;
    private static List<ChatMessage> messages = GlobalVariable.messages;

    private ListView chatHistoryLv;
    private Button sendBtn;
    private EditText textEditor;

    private Spinner template;
    private MySpinner content;

    private ArrayAdapter<String> _BAdapter;
    private String[] contentTextArray;

//    private String[] typeArray;
//    private Map<String,String[]> contentarrayMap ;

    TextView textViewContent;
    private ListPopupWindow popupWindow;
    private List<String> strings;


    ////////////////////////////////////////

    public static Handler handler_;
    private final Handler handler = new Handler(this);
    public static Boolean configcomplete = false;

    //集成intercom界面功能
    //private static MyCall intercomCall;
    //private static CallInfo intercomCallInfo;
    private ListView buddyListView;
    private IntercomAdapter buddyListAdapter;
    private ArrayList<Map<String, String>> buddyList = new ArrayList<>();
    private int buddyListSelectedIdx = -1;
    private Spinner channelSpinner;
    private List<IntercomUser> intercomUserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        handler_ = handler;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communicate);
//        try {
//
//            List<TemplateType> typeList = JSON.parseArray(typearray.toString(), TemplateType.class);
//
//            typeArray = new String[typeList.size()];
//            contentarrayMap = new HashMap<>();
//            int index = 0;
//            for(TemplateType type : typeList)
//            {
//                typeArray[index] = type.getTypename();
//
//                JSONArray contentarray = rs_json_data.getJSONArray(type.getTypeno());
//                List<TemplateContent> contentList = JSON.parseArray(contentarray.toString(), TemplateContent.class);
//
//                String[] contenttextArray = new String[contentList.size()];
//                int text_index = 0;
//                for(TemplateContent content : contentList)
//                {
//
//                    contenttextArray[text_index] = content.getTextcontent();
//                    text_index++;
//                }
//                contentarrayMap.put(String.valueOf(index),contenttextArray);
//                index++;
//            }
//
//            //configcomplete = true;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        while(true){
//            if(configcomplete)
//                break;
//        }


        List<String> uriList = new ArrayList<>();
        for(ProgramDirector director:GlobalVariable.pdrList)
        {
            uriList.add("sip:" + director.sipid + "@" + GlobalVariable.EUSERVER + "");
        }
        uriList.add(MainActivity.zhuboUri.replace("<","").replace(">",""));

        String local_uri = MainActivity.localUri.replace("<","").replace(">","");
        Iterator<String> it1 = uriList.iterator();
        while(it1.hasNext()) {
            if(local_uri.equals(it1.next())) {
                it1.remove();
            }
        }

        //获得已注册的buddyList的Uri
        if(MainActivity.account.buddyList!=null) {
            for (MyBuddy buddy : MainActivity.account.buddyList) {
                String uri = buddy.cfg.getUri();

                Iterator<String> it2 = uriList.iterator();
                while (it2.hasNext()) {
                    if (uri.equals(it2.next())) {
                        it2.remove();
                    }
                }
                uriList.remove(uri);
            }
        }
        for(String uri:uriList)
        {
            Message m2 = Message.obtain(MainActivity.handler_,1004,uri);
            m2.sendToTarget();
        }

        chatHistoryLv = (ListView) findViewById(R.id.chatting_history_lv);
        setAdapterForThis();
        sendBtn = (Button) findViewById(R.id.send_button);
        textEditor = (EditText) findViewById(R.id.text_editor);

        textEditor.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence text, int start, int before, int count) {

                //text  输入框中改变后的字符串信息
                //start 输入框中改变后的字符串的起始位置
                //before 输入框中改变前的字符串的位置 默认为0
                //count 输入框中改变后的一共输入字符串的数量

                //这个方法是在Text改变过程中触发调用的，它的意思就是说在原有的文本text中，从start开始的count个字符
                //替换长度为before的旧文本，注意这里没有将要之类的字眼，也就是说一句执行了替换动作
            }

            @Override
            public void beforeTextChanged(CharSequence text, int start, int count,int after) {
                //text  输入框中改变前的字符串信息
                //start 输入框中改变前的字符串的起始位置
                //count 输入框中改变前后的字符串改变数量一般为0
                //after 输入框中改变后的字符串与起始位置的偏移量

                //这个方法是在Text改变之前被调用，它的意思就是说在原有的文本text中，从start开始的count个字符
                //将会被一个新的长度为after的文本替换，注意这里是将被替换，还没有被替换
            }

            @Override
            public void afterTextChanged(Editable edit) {
                //edit  输入结束呈现在输入框中的信息
                GlobalVariable.imMessage = String.valueOf(edit);
            }
        });

        sendBtn.setOnClickListener(l);

        //2021-03-15先屏蔽提示按钮按正常操作
//        NotificationButton buttonNotify = (NotificationButton) findViewById(R.id.buttonMA6);
//        buttonNotify.setNotificationNumber(7);

        template = findViewById(R.id.spinnerTemplate);
        content = findViewById(R.id.spinnerContent);

//        CharSequence[] strings = typeArray;
//        ArrayAdapter<CharSequence> _Adapter = new ArrayAdapter<CharSequence>(
//                this,
//                android.R.layout.simple_spinner_item,
//                0,
//                Arrays.asList(strings));
        ArrayAdapter<String> _Adapter = new MySpinnerAdapter(this,GlobalVariable.typeArray);

        //_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        template.setAdapter(_Adapter);

        // 点击省份对应城市数据绑定
        template.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Spinner spinner = (Spinner) parent;
                String pro = (String) spinner.getItemAtPosition(position);
                // 默认显示城市
//                ArrayAdapter<CharSequence> _BAdapter = new ArrayAdapter<CharSequence>(
//                        CommunicateActivity.this,
//                        android.R.layout.simple_spinner_item,
//                        0,
//                        Arrays.asList(contentarrayMap.get(""+position))
//                );


//                _BAdapter = new MyArrayListAdapter(
//                                CommunicateActivity.this,android.R.layout.simple_list_item_1,
//                                GlobalVariable.contentarrayMap.get(""+position)==null?Arrays.asList(new String[]{"","",""}):Arrays.asList(GlobalVariable.contentarrayMap.get(""+position))
//                        );
//                _BAdapter = new ArrayAdapter(
//                        CommunicateActivity.this,android.R.layout.simple_list_item_1,
//                        GlobalVariable.contentarrayMap.get(""+position)==null?Arrays.asList(new String[]{"","",""}):Arrays.asList(GlobalVariable.contentarrayMap.get(""+position))
//                );

//                _BAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                _BAdapter = new MySpinnerAdapter(
                        CommunicateActivity.this,
                        GlobalVariable.contentarrayMap.get(""+position)==null?new String[]{"","",""}:GlobalVariable.contentarrayMap.get(""+position)
                );
                content.setAdapter(_BAdapter);
//                contentTextArray = GlobalVariable.contentarrayMap.get(""+position)==null?new String[]{"","",""}:GlobalVariable.contentarrayMap.get(""+position);
//                popupWindow.setAdapter(_BAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        content.setSelection(0, true);
//        content.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Spinner spinner = (Spinner) parent;
//                String pro = (String) spinner.getItemAtPosition(position);
//                //textEditor = findViewById(R.id.text_editor);
//                textEditor.setText(pro);
//
//            }
//        });
        content.setOnItemClicked(new MySpinner.CustomSpinnerListener() {
            @Override
            public void onItemClicked(MySpinner view, AdapterView<?> parent, int position, long id) {
                //Spinner spinner = (Spinner) parent;
                String pro = (String) view.getItemAtPosition(position);
                //textEditor = findViewById(R.id.text_editor);
                textEditor.setText(pro);
            }
        });
        content.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

//                Log.e("触发位置",""+position);
//                Spinner spinner = (Spinner) parent;
//                try {
//                    Field field = AdapterView.class.getDeclaredField("mOldSelectedPosition");
//                    field.setAccessible(true);  //设置mOldSelectedPosition可访问
//                    field.setInt(spinner, AdapterView.INVALID_POSITION); //设置mOldSelectedPosition的值
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
//                String pro = (String) spinner.getItemAtPosition(position);
//                if(GlobalVariable.firstcontentselect){
//                    GlobalVariable.firstcontentselect = false;
//                }
//                else {
//                    //textEditor = findViewById(R.id.text_editor);
//                    textEditor.setText(pro);
//                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //不再使用content转使用textview来处理点击问题


        //导致点击后再次点击清空输入框的问题，于20210525屏蔽
//        EditText editText = findViewById(R.id.text_editor);
//        editText.setOnClickListener(v -> {
//            //已经被选中再次点击也可以弹出
//            editText.setText("");
//        });



        //处理左侧呼出部分的显示
        String[] from = { "uri", "username","sipid","character" };
        int[] to = { R.id.text1, R.id.text2,R.id.text3,R.id.text4 };
        buddyListAdapter = new IntercomAdapter(
                this, buddyList,
                R.layout.intercom_list_item,
                from, to);

        buddyListView  = (ListView) findViewById(R.id.intercomListView);
        buddyListView.setAdapter(buddyListAdapter);
        buddyListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            final View view,
                                            int position, long id)
                    {
                        view.setSelected(true);
                        buddyListSelectedIdx = position;
                    }
                }
        );

        buddyListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
//                            LayoutInflater mInflater = LayoutInflater.from(DailActivity.this);
//                            View convertView = mInflater.inflate(R.layout.intercom_list_item,parent, false);
                            TextView uriTextView = view.findViewById(R.id.text1);
                            TextView nameView = view.findViewById(R.id.text2);

                            String buddy_uri = uriTextView.getText().toString();
                            String toname = nameView.getText().toString();


                            //20210522
//                            MyCall call = new MyCall(MainActivity.account, -1);
//                            CallOpParam prm = new CallOpParam(true);
//                            try {
//                                call.makeCall(buddy_uri, prm);
//                            } catch (Exception e) {
//                                call.delete();
//                            }
                            //intercomCall = call;
                            //需要通过主函数回调获得相关通话状态
//                            MainActivity.currentCall = call;

                            //应该改成直接转到拨号界面

                            //在呼出之前要先把未保持的电话保持
                            if(GlobalVariable.charactername.equals("director")){
                                Message m2 = Message.obtain(MainActivity.handler_,1013,null);
                                m2.sendToTarget();
                            }
                            //20210709判断角色是导播是主播的方式未定，目前使用的排除方法在同一个用户组下是有效的

                            if(GlobalVariable.charactername.equals("director")){
                                GlobalVariable.innerrolefrom = "pdr";
                                if(buddy_uri.equals(GlobalVariable.zhuboUri)){
                                    GlobalVariable.innerroleto = "mbc";
                                }
                                else{
                                    GlobalVariable.innerroleto = "pdr";
                                }

                            }
                            else{
                                GlobalVariable.innerrolefrom = "mbc";
                                //主播是不可能两个的
                                GlobalVariable.innerroleto = "pdr";

                            }
                            String msg_to_send =
                                    "{ \"msgType\": \"INNER MAKE CALL\"" +
                                            ", \"fromsipid\": \""+ GlobalVariable.localUri+ "\""
                                            +", \"tosipid\": \""+ buddy_uri+ "\""
                                            + ", \"frompgm\": \""+ GlobalVariable.channelname + "\""
                                            + ", \"topgm\": \""+ GlobalVariable.channelname + "\""
                                            + ", \"fromname\": \""+ GlobalVariable.username + "\""
                                            + ", \"toname\": \""+ toname + "\""
                                            + ", \"fromrole\": \""+ GlobalVariable.innerrolefrom + "\""
                                            + ", \"torole\": \""+ GlobalVariable.innerroleto + "\""
                                            + ", \"msg\": \"" + "\""
                                            +" }";
                            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                            GlobalVariable.innerstart = true;
                            GlobalVariable.innercallfrom = GlobalVariable.localUri;
                            GlobalVariable.innercallto = buddy_uri;
                            GlobalVariable.innerchannelfrom = GlobalVariable.channelname;
                            GlobalVariable.innerchannelto = GlobalVariable.channelname;
                            GlobalVariable.innernamefrom = GlobalVariable.username;
                            GlobalVariable.innernameto = toname;

                            //showCallActivity();//20210601在确定回了inner make call再显示

//                            LinearLayout callinglayout = findViewById(R.id.callingPanel);
//                            LinearLayout diallayout = findViewById(R.id.dialPanel);
//                            callinglayout.setVisibility(View.VISIBLE);
//                            diallayout.setVisibility(View.GONE);

//                            buddyListView.setEnabled(false);



                            return true;
                        }catch (Exception e){
                            LogClient.generate("【操作错误】"+e.getMessage());
                            e.printStackTrace();
                        }
                        return false;
                    }
                }
        );

        channelSpinner =  (Spinner) findViewById(R.id.spinnerChannel);
        //获得频道
        List<String> chnameList = new ArrayList();
        String[] channelArray ;
        for(Channel ch : GlobalVariable.channelList)
        {
            String name = "";
            if(ch.getName().equals("-"))
                name = "";
            else
                name = ch.getName();
            chnameList.add("   "+ch.getParentname()+" "+name);
        }
        channelArray = chnameList.toArray(new String[chnameList.size()]);
        //String[] channelArray = {"频道1","频道2","频道三"};
        ArrayAdapter<String> spAdapter = new MySpinnerAdapter(this,channelArray);
        channelSpinner.setAdapter(spAdapter);
        channelSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Spinner spinner = (Spinner) parent;
                String pro = (String) spinner.getItemAtPosition(position);
                //根据选择的频道名称或者postion找到对应的channelID，开启线程访问数据库
                // 找到pgmid, 获得SIPSRV用于拼接注册地址
                // 找directbroadcastingroom表中所有的pgmid对应的pdrid和mbcid，
                // 再根据结果去主播和导播表查找id对应的SIPID 得到访问结果后返回
                //getSipIPList
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            Long id = -1L;
                            for(Channel ch : GlobalVariable.channelList){
                                String name = "";
                                if(ch.getName().equals("-"))
                                    name = "";
                                else
                                    name = ch.getName();
                                String temp = "   "+ch.getParentname()+" "+name;

                                if((temp.equals(pro)))
                                    id = ch.getId();
                            }
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    "http://"+GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getSipIPListByChannel",
                                    "{\"type\":\"get config\", \"channelid\":\"" + id + "\"}"
                            );
                            JSONObject rs_json = new JSONObject(rs);

                            JSONArray rs_json_data = rs_json.getJSONArray("data");
                            intercomUserList = JSON.parseArray(rs_json_data.toString(), IntercomUser.class);
                            Message m2 = Message.obtain(CommunicateActivity.handler_,1002,null);
                            m2.sendToTarget();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        //不同角色不同背景
        LinearLayout layoutBkg = findViewById(R.id.communicateBkg);
        if(GlobalVariable.charactername.equals("director")){
            layoutBkg.setBackground(getDrawable(R.drawable.communicate_background_3));
        }
        else{
            layoutBkg.setBackground(getDrawable(R.drawable.communicate_background_2));
        }
        //MainActivity跳转到当前不会到底部，原因未知
        //更新到聊天内容底部
        if(chatHistoryLv!=null)
            chatHistoryLv.setSelection(chatHistoryLv.getBottom());

        ImageButton imageButtonTitle = findViewById(R.id.buttonICON2);
        if(GlobalVariable.charactername.equals("broadcaster")) {
            //主播，仅导播模式屏蔽
            if (GlobalVariable.sipmodel == 3){
                imageButtonTitle.setBackgroundResource(R.color.transparent);
                imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                imageButtonTitle.setEnabled(false);
            }
            if (GlobalVariable.sipmodel == 1||GlobalVariable.sipmodel == 2){
                imageButtonTitle.setBackgroundResource(R.color.transparent);
                imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                imageButtonTitle.setEnabled(false);
            }
        }
        else{
            if (GlobalVariable.sipmodel == 0){
                imageButtonTitle.setBackgroundResource(R.color.transparent);
                imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                imageButtonTitle.setEnabled(false);
            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {

        if(GlobalVariable.isDL1129) {
            Button innercomBtn = findViewById(R.id.buttonMA6);
            innercomBtn.setVisibility(View.GONE);
        }
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        //Log.e("临时IM消息",GlobalVariable.imMessage);
        textEditor.setText(GlobalVariable.imMessage);
        GlobalVariable.firstcontentselect = true;

        //更新到聊天内容底部
        chatHistoryLv = (ListView) findViewById(R.id.chatting_history_lv);
        setAdapterForThis();
        if(chatHistoryLv!=null) {
            Log.e("FIND","Message数量"+messages.size());
            Log.e("FIND","底部位置"+chatHistoryLv.getBottom());
            chatHistoryLv.setSelection(messages.size());
        }
        else
            Log.e("ERROR","没有找到");



//        textViewContent= (TextView) findViewById(R.id.textviewContent);
//        textViewContent.setTextColor(getColor(R.color.white));
//        popupWindow = new ListPopupWindow(this);
//        //popupWindow.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,strings));
//        _BAdapter = new MySpinnerAdapter(CommunicateActivity.this,new String[]{"","",""});
//
//        //_BAdapter = new MyArrayListAdapter(this,android.R.layout.simple_list_item_1, Arrays.asList(new String[]{"1","2","3"}));
//        //_BAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, Arrays.asList(new String[]{"1","2","3"}));
//        popupWindow.setAdapter(_BAdapter);
//        popupWindow.setBackgroundDrawable(getDrawable(R.color.translucentblack));
//        contentTextArray = new String[]{"1","2","3"};
//
////        int maxWidth = 0;
////        for (String car : contentTextArray) {
////            TextView textView = (TextView) View.inflate(this, android.R.layout.simple_dropdown_item_1line, null);
////            textView.setText(car);
////            textView.measure(0, 0);
////            maxWidth = maxWidth > textView.getMeasuredWidth() ? maxWidth : textView.getMeasuredWidth();
////        }
//
//        popupWindow.setAnchorView(textViewContent);
//        popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
//        popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
//        popupWindow.setModal(true);
//        popupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int position, long id) {
//
//                textViewContent.setText(contentTextArray[position]);
//                textEditor.setText(textViewContent.getText().toString());
//                popupWindow.dismiss();
//                Toast.makeText(CommunicateActivity.this,""+textViewContent.getText().toString(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//        textViewContent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popupWindow.show();
//                popupWindow.setWidth(LinearLayout.LayoutParams.WRAP_CONTENT);
//                popupWindow.setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);
//                popupWindow.setModal(true);
//                popupWindow.setBackgroundDrawable(getDrawable(R.color.translucentblack));
//            }
//        });



        super.onStart();
    }

    @Override
    public boolean handleMessage(Message msg) {

        if(msg.what == 1001)
        {
            OnInstantMessageParam prm = (OnInstantMessageParam) msg.obj;

//            System.out.println("======== Incoming pager ======== ");
//            System.out.println("From     : " + prm.getFromUri());
//            System.out.println("To       : " + prm.getToUri());
//            System.out.println("Contact  : " + prm.getContactUri());
//            System.out.println("Mimetype : " + prm.getContentType());
//            System.out.println("Body     : " + prm.getMsgBody());
            String massagecontent = prm.getMsgBody();
            String from = prm.getFromUri();
            messages.add(
                    new ChatMessage(
                            ChatMessage.MESSAGE_FROM,
                            "内容是"+massagecontent,
                            GlobalVariable.hm_df.format(new Date()),
                            (String)template.getSelectedItem(),
                            GlobalVariable.username
                    )
            );
            chatHistoryAdapter.notifyDataSetChanged();
            chatHistoryLv.setSelection(chatHistoryLv.getBottom());
            return true;
        }
        if(msg.what == 1002){
            buddyList.clear();
            for(IntercomUser usr : intercomUserList)
            {
                String uri = "<sip:" + usr.getSipid() + "@" + usr.getKamsrv() + ">";
                if(!uri.equals(MainActivity.localUri))
                    buddyList.add(putData(uri, usr.getUsername(),usr.getSipid(),usr.getCharacter()));
            }
            buddyListAdapter.notifyDataSetChanged();
        }
        else if(msg.what == 1005)
        {
            //在当前页面不作处理,直接进行chatHistoryAdapter的刷新
            chatHistoryAdapter.notifyDataSetChanged();
            chatHistoryLv.setSelection(chatHistoryLv.getBottom());
//            new  AlertDialog.Builder(getApplicationContext())
//                    .setTitle("提示" )
//                    .setMessage("有新消息提醒" )
//                    .setPositiveButton("确定" ,  null )
//                    .show();
        }
        else if(msg.what == 1010)
        {
            TextView timeView = (TextView) findViewById(R.id.mainTime);
            if (timeView != null) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String dateString = sdf.format(date);
                timeView.setText(dateString.replace(" ", "         "));
            }
            //刷新状态图标
            ImageView mainState1 = (ImageView) findViewById(R.id.MainState1);
            ImageView mainState2 = (ImageView) findViewById(R.id.MainState2);
            ImageView mainState3 = (ImageView) findViewById(R.id.MainState3);

            Button ImButton = findViewById(R.id.buttonMA6);
            if(GlobalVariable.IMRedTitle)
            {
                if(GlobalVariable.IMRedShowed)
                {
                    ImButton.setTextColor(Color.WHITE);
                    GlobalVariable.IMRedShowed = false;
                }
                else
                {
                    ImButton.setTextColor(Color.RED);
                    GlobalVariable.IMRedShowed = true;
                }
            }
            else{
                ImButton.setTextColor(Color.WHITE);
            }
            /**有未接听电话首页图标闪烁**/
            Button PhoneButton = findViewById(R.id.buttonMA3);
            if(GlobalVariable.PhoneRedTitle){
                if(GlobalVariable.PhoneRedShowed6){
                    PhoneButton.setTextColor(Color.WHITE);
                    GlobalVariable.PhoneRedShowed6 = false;
                }else{
                    PhoneButton.setTextColor(getResources().getColor(R.color.notify_blue));
                    GlobalVariable.PhoneRedShowed6 = true;
                }
            }else{
                PhoneButton.setTextColor(Color.WHITE);
            }

            if(GlobalVariable.callring)
            {
                mainState1.setImageDrawable(getResources().getDrawable(R.drawable.allowring));
            }
            else{
                mainState1.setImageDrawable(getResources().getDrawable(R.drawable.disablering));
            }

            if(GlobalVariable.whitelistmodel)
            {
                mainState2.setImageDrawable(getResources().getDrawable(R.drawable.whitelistmodel));
            }
            else if(GlobalVariable.allowblacklist)
            {
                mainState2.setImageDrawable(getResources().getDrawable(R.drawable.allowblack));
            }
            else{
                mainState2.setImageDrawable(getResources().getDrawable(R.drawable.disableblack));
            }

            if(!GlobalVariable.online)
            {
                mainState3.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                mainState3.setColorFilter(Color.RED);
            }
            else if(GlobalVariable.busyall == 1)
            {
                mainState3.setImageDrawable(getResources().getDrawable(R.drawable.busy));
                mainState3.setColorFilter(Color.RED);
            }
            else if(GlobalVariable.busyall==2) {
                mainState3.setImageDrawable(getResources().getDrawable(R.drawable.linebusy));
                mainState3.setColorFilter(Color.TRANSPARENT);
            }
            else {
                mainState3.setImageDrawable(getResources().getDrawable(R.drawable.allowin));
                mainState3.setColorFilter(Color.WHITE);
            }

            TextView mainChannel = findViewById(R.id.mainChannel);
            TextView mainSipID = findViewById(R.id.mainSipID);
            mainChannel.setText(GlobalVariable.channelname);
            mainSipID.setText(GlobalVariable.user_sipid);
        }
        else if(msg.what == 1011){
            ImageView titleImage = findViewById(R.id.titleImage);
            if(String.valueOf(msg.obj).equals("success")){titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo));}
            else{titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo_red));}
        }
        else if(msg.what == 1014){
            String msg_content = String.valueOf(msg.obj);
            MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"系统状态已更改\n"+msg_content).create();
            msgNotifyDialog.show();
            if(GlobalVariable.viewmodel == 1)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            msgNotifyDialog.getWindow().setLayout(500, 400);
            final Timer t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    msgNotifyDialog.dismiss();
                    t.cancel();
                }
            }, 2000);
        }
        else if(msg.what == 1015){
            showMainActivity(null);
        }
        return false;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler_ = null;
    }

    // 设置adapter
    private void setAdapterForThis() {
        initMessages();
        chatHistoryAdapter = new ChattingAdapter(this, messages);
        chatHistoryLv.setAdapter(chatHistoryAdapter);
    }

    // 为listView添加数据
    private void initMessages() {
//        messages.add(new ChatMessage(ChatMessage.MESSAGE_FROM, "hello"));
//        messages.add(new ChatMessage(ChatMessage.MESSAGE_TO, "hello"));

    }

    private View.OnClickListener l = new View.OnClickListener() {

        public void onClick(View v) {

            if (v.getId() == sendBtn.getId()) {
                String str = textEditor.getText().toString();
                String sendStr;
                if (str != null
                        && (sendStr = str.trim().replaceAll("\r", "").replaceAll("\t", "").replaceAll("\n", "")
                        .replaceAll("\f", "")) != "") {
                    sendMessage(sendStr);
                }
                try {
                    Field field = AdapterView.class.getDeclaredField("mOldSelectedPosition");
                    field.setAccessible(true);  //设置mOldSelectedPosition可访问
                    field.setInt(content, AdapterView.INVALID_POSITION); //设置mOldSelectedPosition的值
                } catch (Exception e) {
                    e.printStackTrace();
                }
                textEditor.setText("");

            }
        }
        // 模拟发送消息
        private void sendMessage(String sendStr) {
            messages.add(
                    new ChatMessage(
                            ChatMessage.MESSAGE_TO,
                            sendStr,
                            GlobalVariable.hm_df.format(new Date()),
                            (String)template.getSelectedItem(),
                            GlobalVariable.username
                    )
            );
            chatHistoryAdapter.notifyDataSetChanged();
            chatHistoryLv.setSelection(chatHistoryLv.getBottom());

            List<MyBuddy> buddyList = MainActivity.app.accList.get(0).buddyList;
            for (MyBuddy buddy:buddyList) {
                SendInstantMessageParam param = new SendInstantMessageParam();


                String jsonMessage = "{ \"username\": \""+GlobalVariable.username +"\", "+
                        "\"template\": \""
                        + (String)template.getSelectedItem()//(String) spinner.getItemAtPosition(position);
                        + "\", \"date\": \""
                        + GlobalVariable.hm_df.format(new Date())
                        + "\", \"msg\": \""
                        + sendStr
                        + "\", \"type\": \""
                        + 0//0:顶端字变红 1:弹框提醒
                        + "\" }";

                param.setContent(jsonMessage);


                //param.setContent(sendStr);
                //param.setContentType("");
                try {
                    buddy.sendInstantMessage(param);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public void sendMessageTest(View view)
    {
        List<MyBuddy> buddyList = MainActivity.app.accList.get(0).buddyList;

        String str = textEditor.getText().toString();
        String sendStr;
        if (str != null
                && (sendStr = str.trim().replaceAll("\r", "").replaceAll("\t", "").replaceAll("\n", "")
                .replaceAll("\f", "")) != "")
        {
            //本地显示
            messages.add(
                    new ChatMessage(
                            ChatMessage.MESSAGE_TO,
                            sendStr,
                            GlobalVariable.hm_df.format(new Date()),
                            (String)template.getSelectedItem(),
                            GlobalVariable.username
                    )
            );
            chatHistoryAdapter.notifyDataSetChanged();
            chatHistoryLv.setSelection(chatHistoryLv.getBottom());
            //发送IM消息
            for (MyBuddy buddy:buddyList) {
                SendInstantMessageParam param = new SendInstantMessageParam();
                String jsonMessage = "{ \"username\": \""+GlobalVariable.username +"\", "+
                        "\"template\": \""
                        + (String)template.getSelectedItem()//(String) spinner.getItemAtPosition(position);
                        + "\", \"date\": \""
                        + GlobalVariable.hm_df.format(new Date())
                        + "\", \"msg\": \""
                        + sendStr
                        + "\", \"type\": \""
                        + 1//0:顶端字变红 1:弹框提醒
                        + "\" }";

                param.setContent(jsonMessage);
                //param.setContentType("");
                try {
                    buddy.sendInstantMessage(param);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            Field field = AdapterView.class.getDeclaredField("mOldSelectedPosition");
            field.setAccessible(true);  //设置mOldSelectedPosition可访问
            field.setInt(content, AdapterView.INVALID_POSITION); //设置mOldSelectedPosition的值
        } catch (Exception e) {
            e.printStackTrace();
        }
        textEditor.setText("");
    }

    public void clickNumber(View view)
    {
        //根据Button的Text决定输入值
        Button clickbutton = (Button)view;
        String number = (String) clickbutton.getText();
        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        dailtext.setText(dailtext.getText().toString()+number);
    }
    public void subNumber(View view)
    {

        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        String old_value = dailtext.getText().toString();
        if(old_value.length()>0)
        {
            dailtext.setText(old_value.substring(0,old_value.length() - 1));
        }
    }
    public void clearNumber(View view)
    {

        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        dailtext.setText("");

    }
    public void CommunicateDail(View view)
    {
        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        if(dailtext!=null)
        {
            GlobalVariable.setdailvalue = true;
            GlobalVariable.dailvalue = String.valueOf(dailtext.getText());

            if(DailActivity.handler_!=null){
                Message m2 = Message.obtain(DailActivity.handler_,1002,null);
                m2.sendToTarget();
            }

            Intent intent = new Intent(this, DailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivity(intent);
        }

    }
    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), CommunicateActivity.this);
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    private HashMap<String, String> putData(String uri, String username, String sipid, String character)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("uri", uri);
        item.put("username", username);
        item.put("sipid", sipid);
        item.put("character",character==null?"pdr":character);//20210709追加用户角色
        return item;
    }

    private void showCallActivity()
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, CallActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//可以跳过中间栈直接实现两个界面间的切换
        startActivity(intent);
    }

    public void showDailActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, DailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void showMainActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showHistoryActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showContactsActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showCommunicateActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        Intent intent = new Intent(this, CommunicateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showSettingActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        Intent intent = new Intent(this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showIntercomActivity(View view)
    {
        //去除全局的红色提醒标记
        GlobalVariable.IMRedTitle = false;
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, IntercomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        //setContentView(layoutIntercom);
    }

}

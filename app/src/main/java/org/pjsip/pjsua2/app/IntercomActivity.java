package org.pjsip.pjsua2.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.annotation.NonNull;

import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.model.ProgramDirector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IntercomActivity extends Activity implements Handler.Callback {
    public static Handler handler_;
    private final Handler handler = new Handler(this);

    private static MyCall interCall;
    private static CallInfo interCallInfo;
    private ListView buddyListView;
    private SimpleAdapter buddyListAdapter;
    private ArrayList<Map<String, String>> buddyList = new ArrayList<>();
    private int buddyListSelectedIdx = -1;

    public void dial(View view){
        //根据选择的ListViewItem,创建Call

        HashMap<String, String> item = (HashMap<String, String>) buddyListView.
                getItemAtPosition(buddyListSelectedIdx);
        String buddy_uri = item.get("uri");

        MyCall call = new MyCall(MainActivity.account, -1);
        CallOpParam prm = new CallOpParam(true);
        try {
            call.makeCall(buddy_uri, prm);
        } catch (Exception e) {
            call.delete();
            return;
        }
        interCall = call;
    }

    public void hide(View view){
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);//可以跳过中间栈直接实现两个界面间的切换
        startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        handler_ = handler;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intercom);

        //buddyList添加非自己的所有对象
        List<String> uriList = new ArrayList<>();
        for(ProgramDirector director:GlobalVariable.pdrList)
        {
            uriList.add("<sip:" + director.sipid + "@" + GlobalVariable.EUSERVER + ">");
        }
        uriList.add(MainActivity.zhuboUri);
        uriList.remove(MainActivity.localUri);

        for(String uri:uriList)
        {
            buddyList.add(putData(uri, ""));
        }


        String[] from = { "uri", "status" };
        int[] to = { android.R.id.text1, android.R.id.text2 };
        buddyListAdapter = new SimpleAdapter(
                this, buddyList,
                android.R.layout.simple_list_item_2,
                from, to);

        buddyListView  = (ListView) findViewById(R.id.buddyListView);
        buddyListView.setAdapter(buddyListAdapter);
        buddyListView.setOnItemClickListener(
            new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent,
                            final View view,
                            int position, long id)
                {
                    view.setSelected(true);
                    buddyListSelectedIdx = position;
                }
            }
        );
    }
    private HashMap<String, String> putData(String uri, String status)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("uri", uri);
        item.put("status", status);
        return item;
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {
        return false;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler_ = null;
    }
}
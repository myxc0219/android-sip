package org.pjsip.pjsua2.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.pjsip.pjsua2.app.R;

import java.util.List;

public class MyArrayListAdapter extends ArrayAdapter {
    public MyArrayListAdapter(@NonNull Context context, int resource) {
        super(context, resource);
    }

    public MyArrayListAdapter(@NonNull Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public MyArrayListAdapter(@NonNull Context context, int resource, @NonNull Object[] objects) {
        super(context, resource, objects);
    }

    public MyArrayListAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull Object[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public MyArrayListAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
    }

    public MyArrayListAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(super.getContext());
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent,false);
        }

        convertView.setBackground(super.getContext().getDrawable(R.color.transparent));
        //此处text1是Spinner默认的用来显示文字的TextView
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setBackgroundColor(convertView.getResources().getColor(R.color.translucentblack));
        tv.setText(String.valueOf(super.getItem(position)));
        tv.setTextSize(28f);
        tv.setTextColor(Color.WHITE);

        return super.getDropDownView(position, convertView, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(super.getContext());
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        }

        //此处text1是Spinner默认的用来显示文字的TextView
        TextView tv = (TextView) convertView.findViewById(android.R.id.text1);
        tv.setText(String.valueOf(super.getItem(position)));
        tv.setTextSize(28f);
        tv.setTextColor(Color.WHITE);

        return super.getView(position, convertView, parent);
    }
}

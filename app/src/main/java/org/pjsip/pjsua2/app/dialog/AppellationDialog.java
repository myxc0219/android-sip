package org.pjsip.pjsua2.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.util.EditTextUtils;

import java.util.ArrayList;
import java.util.List;


public class AppellationDialog extends Dialog {

    private AppellationDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;


//        private ImageView mIcon;
//        private TextView mTitle;
//        private TextView mMessage;
//        private Button mButton;

        private Button mbtnAppellationConfirm;
        private Button mbtnAppellationCancel;

        private EditText mAppellationText;

        private View.OnClickListener mButtonClickListener;

        private AppellationDialog mDialog;


        private List<Button> aplBtnList = new ArrayList<>();


        public Builder(Context context) {
            mContext = context;
            mDialog = new AppellationDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_add_appellation, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));


            mbtnAppellationConfirm = mLayout.findViewById(R.id.btnAppellationConfirm);
            mbtnAppellationCancel = mLayout.findViewById(R.id.btnAppellationCancel);
            mAppellationText = mLayout.findViewById(R.id.appellationText);

            aplBtnList.add(mLayout.findViewById(R.id.textApl1));
            aplBtnList.add(mLayout.findViewById(R.id.textApl2));
            aplBtnList.add(mLayout.findViewById(R.id.textApl3));
            aplBtnList.add(mLayout.findViewById(R.id.textApl4));
            aplBtnList.add(mLayout.findViewById(R.id.textApl5));
            aplBtnList.add(mLayout.findViewById(R.id.textApl6));
            aplBtnList.add(mLayout.findViewById(R.id.textApl7));
            aplBtnList.add(mLayout.findViewById(R.id.textApl8));
            aplBtnList.add(mLayout.findViewById(R.id.textApl9));
            aplBtnList.add(mLayout.findViewById(R.id.textApl10));
            aplBtnList.add(mLayout.findViewById(R.id.textApl11));
            aplBtnList.add(mLayout.findViewById(R.id.textApl12));

            aplBtnList.add(mLayout.findViewById(R.id.textAplt1));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt2));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt3));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt4));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt5));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt6));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt7));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt8));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt9));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt10));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt11));
            aplBtnList.add(mLayout.findViewById(R.id.textAplt12));

        }

//        /**
//         * 设置按钮文字和监听
//         */
//        public Builder setButton(@NonNull String text, View.OnClickListener listener) {
//            mButton.setText(text);
//            mButtonClickListener = listener;
//            return this;
//        }

        public AppellationDialog create() {

            for(Button button:aplBtnList){
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Button textBtn = (Button) v;
                        EditText editText = mLayout.findViewById(R.id.appellationText);
                        editText.setText(String.valueOf(editText.getText()) + String.valueOf(textBtn.getText()));
                    }
                });
            }


            mbtnAppellationConfirm.setOnClickListener(view -> {
                EditText editText = ((Activity)mContext).findViewById(R.id.editTextName);
                editText.setText(mAppellationText.getText());
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });


            mbtnAppellationCancel.setOnClickListener(view -> {
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });
            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }


    }

    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), this.getContext());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}

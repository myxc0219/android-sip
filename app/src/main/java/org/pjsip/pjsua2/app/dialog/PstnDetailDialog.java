package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.adapter.MySpinnerAdapter;
import org.pjsip.pjsua2.app.util.EditTextUtils;

public class PstnDetailDialog extends Dialog {

    private PstnDetailDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private Button mbtnPstnConfirm;
        private Button mbtnPstnCancel;

        private View.OnClickListener mButtonClickListener;

        private PstnDetailDialog mDialog;

        private String[] provinces;

        private MainActivity.MyCallWithState mcallWithState;

        private Spinner province;
        private Spinner city;
        private EditText meditPstnAddress;
        private EditText meditPstnAge;
        private EditText meditPstnLevel;


        private Button mbtnPstnWhiteT;
        private Button mbtnPstnWhiteF;
        private Button mbtnPstnBlackT;
        private Button mbtnPstnBlackF;
        private Button mbtnPstnContactT;
        private Button mbtnPstnContactF;
        private Button mbtnPstnExpertT;
        private Button mbtnPstnExpertF;

        private CheckBox mCheckContactTodo;

        private boolean isWhitelist = false;
        private boolean isBlacklist = false;

        private boolean isContact = false;
        private boolean isExpert = false;

        private boolean isTodolist = false;

        public Builder(Context context, MainActivity.MyCallWithState callWithState) {
            mContext = context;

            mcallWithState = callWithState;

            isWhitelist = mcallWithState.iswhitelist==1?true:false;
            isBlacklist = mcallWithState.isblacklist==1?true:false;
            isContact = mcallWithState.iscontact==1?true:false;
            isExpert = mcallWithState.isexpert==1?true:false;

            //使用mark作为待解决项标志位
            isTodolist = mcallWithState.remark.equals("0")?false:true;

            mDialog = new PstnDetailDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_pstn_detail, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mCheckContactTodo = mLayout.findViewById(R.id.check_ContactTodo);
            mCheckContactTodo.setChecked(isTodolist);

            mbtnPstnConfirm = mLayout.findViewById(R.id.btnPstnConfirm);
            mbtnPstnCancel = mLayout.findViewById(R.id.btnPstnCancel);
            provinces = mContext.getResources().getStringArray(R.array.province);

            province = mLayout.findViewById(R.id.editPstnProvince);

            city = mLayout.findViewById(R.id.editPstnCity);

            meditPstnAddress = mLayout.findViewById(R.id.editPstnAddress);
            meditPstnAge = mLayout.findViewById(R.id.editPstnAge);
            meditPstnLevel = mLayout.findViewById(R.id.editPstnLevel);

            mbtnPstnWhiteT = mLayout.findViewById(R.id.btnPstnWhiteT);
            mbtnPstnWhiteF = mLayout.findViewById(R.id.btnPstnWhiteF);
            mbtnPstnBlackT = mLayout.findViewById(R.id.btnPstnBlackT);
            mbtnPstnBlackF = mLayout.findViewById(R.id.btnPstnBlackF);
            mbtnPstnContactT = mLayout.findViewById(R.id.btnPstnContactT);
            mbtnPstnContactF = mLayout.findViewById(R.id.btnPstnContactF);
            mbtnPstnExpertT = mLayout.findViewById(R.id.btnPstnExpertT);
            mbtnPstnExpertF = mLayout.findViewById(R.id.btnPstnExpertF);

            //在获得按钮后对按钮状态进行初始化
            int colorblue = mContext.getResources().getColor(R.color.blue);
            int colorwhite = mContext.getResources().getColor(R.color.white);

            mbtnPstnWhiteF.setTextColor(isWhitelist?colorwhite:colorblue);
            mbtnPstnWhiteT.setTextColor(isWhitelist?colorblue:colorwhite);
            mbtnPstnWhiteF.setBackgroundResource(isWhitelist?R.drawable.button_selector_thin:R.drawable.button_selected);
            mbtnPstnWhiteT.setBackgroundResource(isWhitelist?R.drawable.button_selected:R.drawable.button_selector_thin);

            mbtnPstnBlackF.setTextColor(isBlacklist?colorwhite:colorblue);
            mbtnPstnBlackT.setTextColor(isBlacklist?colorblue:colorwhite);
            mbtnPstnBlackF.setBackgroundResource(isBlacklist?R.drawable.button_selector_thin:R.drawable.button_selected);
            mbtnPstnBlackT.setBackgroundResource(isBlacklist?R.drawable.button_selected:R.drawable.button_selector_thin);

            mbtnPstnContactF.setTextColor(isContact?colorwhite:colorblue);
            mbtnPstnContactT.setTextColor(isContact?colorblue:colorwhite);
            mbtnPstnContactF.setBackgroundResource(isContact?R.drawable.button_selector_thin:R.drawable.button_selected);
            mbtnPstnContactT.setBackgroundResource(isContact?R.drawable.button_selected:R.drawable.button_selector_thin);

            mbtnPstnExpertF.setTextColor(isExpert?colorwhite:colorblue);
            mbtnPstnExpertT.setTextColor(isExpert?colorblue:colorwhite);
            mbtnPstnExpertF.setBackgroundResource(isExpert?R.drawable.button_selector_thin:R.drawable.button_selected);
            mbtnPstnExpertT.setBackgroundResource(isExpert?R.drawable.button_selected:R.drawable.button_selector_thin);





//            ArrayAdapter<CharSequence> _Adapter = ArrayAdapter.createFromResource(
//                    mContext, R.array.province,
//                    android.R.layout.simple_spinner_item);
            String [] mStringArray = mContext.getResources().getStringArray(R.array.province);
            ArrayAdapter<String> _Adapter = new MySpinnerAdapter(mContext,mStringArray);
            //_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            province.setAdapter(_Adapter);

            // 点击省份对应城市数据绑定
            province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    Spinner spinner = (Spinner) parent;
                    String pro = (String) spinner.getItemAtPosition(position);
                    // 默认显示城市
//                    ArrayAdapter<CharSequence> _BAdapter = ArrayAdapter
//                            .createFromResource(mContext,
//                                    R.array.北京,
//                                    android.R.layout.simple_spinner_item);
                    String [] mStringArray = mContext.getResources().getStringArray(R.array.北京);
                    ArrayAdapter<String> _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    // 点击省市显示相应城市
                    if (pro.equals("北京")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.北京);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);
                    } else if (pro.equals("天津")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.天津);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("河北")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.河北);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("山西")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.山西);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("内蒙古")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.内蒙古);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("辽宁")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.辽宁);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("吉林")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.吉林);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("黑龙江")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.黑龙江);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("上海")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.上海);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("江苏")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.江苏);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("浙江")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.浙江);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("安徽")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.安徽);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("福建")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.福建);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("江西")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.江西);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("山东")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.山东);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("河南")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.河南);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("湖北")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.湖北);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("湖南")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.湖南);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("广东")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.广东);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("广西")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.广西);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("海南")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.海南);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("重庆")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.重庆);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("四川")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.四川);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("贵州")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.贵州);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("云南")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.云南);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("西藏")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.西藏);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("陕西")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.陕西);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("甘肃")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.甘肃);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("青海")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.青海);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("宁夏")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.宁夏);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("新疆")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.新疆);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("台湾")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.台湾);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("香港")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.香港);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);

                    } else if (pro.equals("澳门")) {
                        mStringArray = mContext.getResources().getStringArray(R.array.澳门);
                        _BAdapter = new MySpinnerAdapter(mContext,mStringArray);
                    }

                    _BAdapter
                            .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    city.setAdapter(_BAdapter);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

//        /**
//         * 设置按钮文字和监听
//         */
//        public Builder setButton(@NonNull String text, View.OnClickListener listener) {
//            mButton.setText(text);
//            mButtonClickListener = listener;
//            return this;
//        }

        public PstnDetailDialog create() {
            int colorblue = mContext.getResources().getColor(R.color.blue);
            int colorwhite = mContext.getResources().getColor(R.color.white);

            mbtnPstnWhiteT.setOnClickListener(view -> {
                isWhitelist = !isWhitelist;
                mbtnPstnWhiteF.setTextColor(isWhitelist?colorwhite:colorblue);
                mbtnPstnWhiteT.setTextColor(isWhitelist?colorblue:colorwhite);
                mbtnPstnWhiteF.setBackgroundResource(isWhitelist?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnWhiteT.setBackgroundResource(isWhitelist?R.drawable.button_selected:R.drawable.button_selector_thin);
            });
            mbtnPstnWhiteF.setOnClickListener(view -> {
                isWhitelist = !isWhitelist;
                mbtnPstnWhiteF.setTextColor(isWhitelist?colorwhite:colorblue);
                mbtnPstnWhiteT.setTextColor(isWhitelist?colorblue:colorwhite);
                mbtnPstnWhiteF.setBackgroundResource(isWhitelist?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnWhiteT.setBackgroundResource(isWhitelist?R.drawable.button_selected:R.drawable.button_selector_thin);
            });

            mbtnPstnBlackT.setOnClickListener(view -> {
                isBlacklist = !isBlacklist;
                mbtnPstnBlackF.setTextColor(isBlacklist?colorwhite:colorblue);
                mbtnPstnBlackT.setTextColor(isBlacklist?colorblue:colorwhite);
                mbtnPstnBlackF.setBackgroundResource(isBlacklist?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnBlackT.setBackgroundResource(isBlacklist?R.drawable.button_selected:R.drawable.button_selector_thin);
            });
            mbtnPstnBlackF.setOnClickListener(view -> {
                isBlacklist = !isBlacklist;
                mbtnPstnBlackF.setTextColor(isBlacklist?colorwhite:colorblue);
                mbtnPstnBlackT.setTextColor(isBlacklist?colorblue:colorwhite);
                mbtnPstnBlackF.setBackgroundResource(isBlacklist?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnBlackT.setBackgroundResource(isBlacklist?R.drawable.button_selected:R.drawable.button_selector_thin);
            });
            mbtnPstnContactT.setOnClickListener(view -> {
                isContact = !isContact;
                mbtnPstnContactF.setTextColor(isContact?colorwhite:colorblue);
                mbtnPstnContactT.setTextColor(isContact?colorblue:colorwhite);
                mbtnPstnContactF.setBackgroundResource(isContact?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnContactT.setBackgroundResource(isContact?R.drawable.button_selected:R.drawable.button_selector_thin);
            });
            mbtnPstnContactF.setOnClickListener(view -> {
                isContact = !isContact;
                mbtnPstnContactF.setTextColor(isContact?colorwhite:colorblue);
                mbtnPstnContactT.setTextColor(isContact?colorblue:colorwhite);
                mbtnPstnContactF.setBackgroundResource(isContact?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnContactT.setBackgroundResource(isContact?R.drawable.button_selected:R.drawable.button_selector_thin);
            });
            mbtnPstnExpertT.setOnClickListener(view -> {
                isExpert = !isExpert;
                mbtnPstnExpertF.setTextColor(isExpert?colorwhite:colorblue);
                mbtnPstnExpertT.setTextColor(isExpert?colorblue:colorwhite);
                mbtnPstnExpertF.setBackgroundResource(isExpert?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnExpertT.setBackgroundResource(isExpert?R.drawable.button_selected:R.drawable.button_selector_thin);
            });
            mbtnPstnExpertF.setOnClickListener(view -> {
                isExpert = !isExpert;
                mbtnPstnExpertF.setTextColor(isExpert?colorwhite:colorblue);
                mbtnPstnExpertT.setTextColor(isExpert?colorblue:colorwhite);
                mbtnPstnExpertF.setBackgroundResource(isExpert?R.drawable.button_selector_thin:R.drawable.button_selected);
                mbtnPstnExpertT.setBackgroundResource(isExpert?R.drawable.button_selected:R.drawable.button_selector_thin);
            });

            mCheckContactTodo.setOnCheckedChangeListener((buttonView, isChecked) -> {
                isTodolist = isChecked;
                Log.e("TODOLIST","按键状态改变为"+(isChecked?"选中":"未选中"));
            });

            mbtnPstnCancel.setOnClickListener(view -> {
                mDialog.dismiss();
            });
            mbtnPstnConfirm.setOnClickListener(view -> {

                //mButtonClickListener.onClick(view);
                mcallWithState.province = String.valueOf(province.getSelectedItem());
                mcallWithState.city = String.valueOf(province.getSelectedItem());
                mcallWithState.address = String.valueOf(meditPstnAddress.getText());

                mcallWithState.age = Integer.parseInt(String.valueOf(meditPstnAge.getText()).equals("")?"0":String.valueOf(meditPstnAge.getText()));
                mcallWithState.level = String.valueOf(meditPstnLevel.getText());

                mcallWithState.iswhitelist = isWhitelist?1:0;
                mcallWithState.isblacklist = isBlacklist?1:0;
                mcallWithState.iscontact = isContact?1:0;
                mcallWithState.isexpert = isExpert?1:0;

                mcallWithState.remark = isTodolist?"1":"0";

                //发送消息让主页面更新配置
                Message m2 = Message.obtain(MainActivity.handler_,1012,null);
                m2.sendToTarget();
//                if(GlobalVariable.charactername.equals("director"))
//                {
//                    //setCallRecordInfo(currentCallWithState);
//
//                }
//                else{
//                    //setCallRecordInfoMbc(currentCallWithState);
//                }
                mDialog.dismiss();

            });
            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }


    }

    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), this.getContext());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}


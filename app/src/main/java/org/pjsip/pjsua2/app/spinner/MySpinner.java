package org.pjsip.pjsua2.app.spinner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.widget.AppCompatSpinner;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 增加只有选择后才触发事件的接口
 */
public class MySpinner extends AppCompatSpinner {
    private CustomSpinnerListener onCustomItemClicked;


    public MySpinner(Context context) {
        super(context);
    }

    public MySpinner(Context context, int mode) {
        super(context, mode);
    }

    public MySpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    {
        setSpinnerClick();
    }

    public void setOnItemClicked(CustomSpinnerListener onItemClicked) {
        this.onCustomItemClicked = onItemClicked;
    }

    private void setSpinnerClick() {

        try {
            Field mPopupField = this.getClass().getSuperclass().getDeclaredField("mPopup");
            mPopupField.setAccessible(true);
            //DropdownPopup
            final Object dialogPopup = mPopupField.get(this);
            final Class<?> dropdownPopupClass = dialogPopup.getClass();
            Method setOnItemClickListenerMethod = dropdownPopupClass.getMethod("setOnItemClickListener", AdapterView.OnItemClickListener.class);
            setOnItemClickListenerMethod.invoke(dialogPopup, new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MySpinner.this.setSelection(position);
                    if (onCustomItemClicked!=null) {
                        onCustomItemClicked.onItemClicked(MySpinner.this,parent,position,id);
                    }
                    try {
                        dropdownPopupClass.getMethod("dismiss").invoke(dialogPopup);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }

    public interface CustomSpinnerListener{
        public void  onItemClicked(MySpinner view,AdapterView<?> parent, int position, long id);
    }
}
//import android.content.Context;
//import android.content.res.Resources;
//import android.util.AttributeSet;
//import android.view.View;
//import android.widget.AdapterView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.appcompat.widget.AppCompatSpinner;
//
//public class MySpinner extends AppCompatSpinner {
//    private AdapterView.OnItemSelectedListener onItemSelectedListenerBackUp;
//
//    public MySpinner(@NonNull Context context) {
//        super(context);
//    }
//
//    public MySpinner(@NonNull Context context, int mode) {
//        super(context, mode);
//    }
//
//    public MySpinner(@NonNull Context context, @Nullable AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public MySpinner(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
//        super(context, attrs, defStyleAttr);
//    }
//
//    public MySpinner(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int mode) {
//        super(context, attrs, defStyleAttr, mode);
//    }
//
//    public MySpinner(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int mode, Resources.Theme popupTheme) {
//        super(context, attrs, defStyleAttr, mode, popupTheme);
//    }
//
//    public void setSelectedWithOutLinkage(int position) {
//        super.setOnItemSelectedListener(null);
//        setSelection(position);
//        setOnItemSelectedListener(this.onItemSelectedListenerBackUp);
//    }
//
//    @Override
//    public void setOnItemSelectedListener(@Nullable AdapterView.OnItemSelectedListener listener) {
//        super.setOnItemSelectedListener(cloneListener(listener));
//        this.onItemSelectedListenerBackUp = cloneListener(listener);
//    }
//
//    private AdapterView.OnItemSelectedListener cloneListener(final AdapterView.OnItemSelectedListener listener) {
//        return new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (listener != null) {
//                    listener.onItemSelected(parent, view, position, id);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//                if (listener != null) {
//                    listener.onNothingSelected(parent);
//                }
//            }
//        };
//    }
//}

package org.pjsip.pjsua2.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.adapter.NameAdapter;
import org.pjsip.pjsua2.app.model.PersonBean;
import org.pjsip.pjsua2.app.ui.contacts.PinyinComparator;
import org.pjsip.pjsua2.app.ui.contacts.SideBar;
import org.pjsip.pjsua2.app.util.PinyinUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NameSelectDialog extends Dialog {

    private NameSelectDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;


//        private ImageView mIcon;
//        private TextView mTitle;
//        private TextView mMessage;
//        private Button mButton;

        private Button mNameSelectManButton;
        private Button mNameSelectWomanButton;
        private Button mNameSelectCancelButton;

        private View.OnClickListener mButtonClickListener;

        private NameSelectDialog mDialog;

        private NameAdapter nameAdapter;
        private ListView listView;
        private List<PersonBean> data;
        private SideBar sidebar;
        private TextView dialog;
        private ListView firstNameListView;
        private int firstNameListSelectedIdx = -1;

        public Builder(Context context) {
            mContext = context;
            mDialog = new NameSelectDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_add_firstname, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

//            mIcon = mLayout.findViewById(R.id.dialog_icon);
//            mTitle = mLayout.findViewById(R.id.dialog_title);
//            mMessage = mLayout.findViewById(R.id.dialog_message);
//            mButton = mLayout.findViewById(R.id.dialog_button);

            mNameSelectManButton = mLayout.findViewById(R.id.nameSelectMan);
            mNameSelectWomanButton = mLayout.findViewById(R.id.nameSelectWoman);
            mNameSelectCancelButton = mLayout.findViewById(R.id.nameSelectCancel);

            firstNameListView = (ListView) mLayout.findViewById(R.id.listViewFirstName);
            firstNameListView.setOnItemClickListener(
                    new AdapterView.OnItemClickListener()
                    {
                        @Override
                        public void onItemClick(AdapterView<?> parent,
                                                final View view,
                                                int position, long id)
                        {
                            try {
                                view.setSelected(true);
                                firstNameListSelectedIdx = position;
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                    }
            );


            sidebar = (SideBar) mLayout.findViewById(R.id.sidebar);
            listView = (ListView) mLayout.findViewById(R.id.listViewFirstName);
            dialog = (TextView) mLayout.findViewById(R.id.dialog);
            sidebar.setTextView(dialog);
            // 设置字母导航触摸监听
            sidebar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

                @Override
                public void onTouchingLetterChanged(String s) {
                    // TODO Auto-generated method stub
                    // 该字母首次出现的位置
                    int position = nameAdapter.getPositionForSelection(s.charAt(0));

                    if (position != -1) {
                        listView.setSelection(position);
                    }
                }
            });
            data = getData(mLayout.getResources().getStringArray(R.array.firstnames));
            //data = getData(pstnlist);
            // 数据在放在adapter之前需要排序
            Collections.sort(data, new PinyinComparator());
            nameAdapter = new NameAdapter(context, data);
            listView.setAdapter(nameAdapter);
        }

        private List<PersonBean> getData(String[] data) {

            //String[] data
            List<PersonBean> listarray = new ArrayList<PersonBean>();
            for (int i = 0; i < data.length; i++) {
                //for(Pstn pstn : pstnList){

                String pinyin = PinyinUtils.getPingYin(String.valueOf(data[i]));
                String Fpinyin = "#";
                if(!pinyin.equals("")){
                    Fpinyin = pinyin.substring(0, 1).toUpperCase();
                }
                PersonBean person = new PersonBean();
                person.setName(data[i]);
                person.setPinYin(pinyin);

                // 正则表达式，判断首字母是否是英文字母
                if (Fpinyin.matches("[A-Z]")) {
                    person.setSortLetters(Fpinyin);
                } else {
                    person.setSortLetters("#");
                }
                listarray.add(person);
            }
            return listarray;
        }


//        /**
//         * 设置按钮文字和监听
//         */
//        public Builder setButton(@NonNull String text, View.OnClickListener listener) {
//            mButton.setText(text);
//            mButtonClickListener = listener;
//            return this;
//        }

        public NameSelectDialog create() {


            mNameSelectManButton.setOnClickListener(view -> {
                EditText editText = ((Activity)mContext).findViewById(R.id.editTextName);
                if(data.get(firstNameListSelectedIdx)!=null) {
                    editText.setText(data.get(firstNameListSelectedIdx).getName()+"先生");
                }
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });

            mNameSelectWomanButton.setOnClickListener(view -> {
                EditText editText = ((Activity)mContext).findViewById(R.id.editTextName);
                if(data.get(firstNameListSelectedIdx)!=null) {
                    editText.setText(data.get(firstNameListSelectedIdx).getName()+"女士");
                }
                mDialog.dismiss();
                //.onClick(view);
            });

            mNameSelectCancelButton.setOnClickListener(view -> {
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });
            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }
}

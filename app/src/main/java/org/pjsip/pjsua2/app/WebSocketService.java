//package org.pjsip.pjsua2.app;
//
//import android.app.Service;
//import android.content.Intent;
//import android.os.Binder;
//import android.os.IBinder;
//import android.util.Log;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.pjsip.pjsua2.app.global.GlobalVariable;
//
//import java.util.concurrent.TimeUnit;
//
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.Response;
//import okhttp3.WebSocket;
//import okhttp3.WebSocketListener;
//import okio.ByteString;
//
////import kotlin.concurrent.thread;
//
//public class WebSocketService extends Service {
//    private String ws_url = "ws://"+ GlobalVariable.EUSERVER +":1333";
//    private WebSocket webSocket = null;
//    private OkHttpClient client = new OkHttpClient().newBuilder().connectTimeout(60000, TimeUnit.MILLISECONDS)
//            .readTimeout(60000, TimeUnit.MILLISECONDS)
//            .build();
//    private Request request = new Request.Builder().url(ws_url).build();
//
//    private Boolean online = false;
//
//
//    class WSListener extends WebSocketListener {
//        void reconnect() {
//            webSocket = client.newWebSocket(request, new WSListener());
//        }
//        @Override
//        public void onClosed(@NotNull WebSocket webSocket, int code, @NotNull String reason) {
//            super.onClosed(webSocket, code, reason);
//            Log.d("WebSocketService", "WebSocketService on closed!!!!!!!!!!!!!!");
//            online = false;
//        }
//
//        @Override
//        public void onFailure(@NotNull WebSocket webSocket, @NotNull Throwable t, @Nullable Response response) {
//            super.onFailure(webSocket, t, response);
//            online = false;
//            if(client == null)
//            {
//                client = new OkHttpClient().newBuilder().connectTimeout(60000, TimeUnit.MILLISECONDS)
//                        .readTimeout(60000, TimeUnit.MILLISECONDS)
//                        .build();
//            }
//            client.dispatcher().cancelAll();
//            client.connectionPool().evictAll();
//            Intent intent = new Intent("cn.fitcan.action.HANDLE_WS_MESSAGE");
//            intent.putExtra("msg","disconnected");
//            intent.setPackage(getPackageName());
//            sendBroadcast(intent);
//            reconnect();
//        }
//
//        @Override
//        public void onMessage(@NotNull WebSocket webSocket, @NotNull String text) {
//            super.onMessage(webSocket, text);
//            Log.d("WebSocketService", text);
//            handleMessage(text);
//        }
//
//        @Override
//        public void onMessage(@NotNull WebSocket webSocket, @NotNull ByteString bytes) {
//            super.onMessage(webSocket, bytes);
//            Log.d("WebSocketService", "byte string..................");
//        }
//
//        @Override
//        public void onOpen(@NotNull WebSocket webSocket, @NotNull Response response) {
//            super.onOpen(webSocket, response);
//            online = true;
//            Intent intent = new Intent("cn.fitcan.action.HANDLE_WS_MESSAGE");
//            intent.putExtra("msg", "connected");
//            intent.setPackage(getPackageName());
//            sendBroadcast(intent);
//        }
//    }
//
//
//    void handleMessage(String msg) {
//        Intent intent = new Intent("cn.fitcan.action.HANDLE_WS_MESSAGE");
//        intent.putExtra("msg", msg);
//        intent.setPackage(getPackageName());
//        sendBroadcast(intent);
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        Log.d("WebSocketService", "WebSocketService created!!!!!!!!!!!!!!");
//    }
//
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.d("WebSocetService", "websocket service started!!!");
//
//        String msg_to_send = String.valueOf(intent.getExtras().get("msg_to_send"));
//        Log.d("消息内容", msg_to_send);
//        if(msg_to_send.equals("no")) {
////            Log.d("AAAAAAA", "gogogogogogo");
////            new Thread(
////                    () -> webSocket = client.newWebSocket(request, new WSListener())
////            ).start();
//
//        }else {
//            if(webSocket!=null)
//                webSocket.send(msg_to_send);
//            else
//                new Thread(
//                        () -> {
//                            webSocket = client.newWebSocket(request, new WSListener());
//                            webSocket.send(msg_to_send);
//                        }
//                ).start();
//        }
//
//        return super.onStartCommand(intent, flags, startId);
//    }
//
//    @Override
//    public void onDestroy() {
//        if(webSocket!=null)
//        {
//            webSocket.close(WsStatus.CODE.NORMAL_CLOSE, WsStatus.TIP.NORMAL_CLOSE);
//
//        }
//        client.dispatcher().cancelAll();
//        client.connectionPool().evictAll();
//        //client = null;
//        Intent intent = new Intent("cn.fitcan.action.HANDLE_WS_MESSAGE");
//        intent.putExtra("msg","disconnected");
//        intent.setPackage(getPackageName());
//        sendBroadcast(intent);
//        super.onDestroy();
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {
//            return new WebSocketService.MyBinder();
//    }
//
//    class WsStatus {
//
//        public final static int CONNECTED = 1;
//        public final static int CONNECTING = 0;
//        public final static int RECONNECT = 2;
//        public final static int DISCONNECTED = -1;
//
//        class CODE {
//
//            public final static int NORMAL_CLOSE = 1000;
//            public final static int ABNORMAL_CLOSE = 1001;
//        }
//
//        class TIP {
//
//            public final static String NORMAL_CLOSE = "normal close";
//            public final static String ABNORMAL_CLOSE = "abnormal close";
//        }
//    }
//
//    public class MyBinder extends Binder {
//        public void transMessage(){
//
//        }
//    }
//}
//
//
//

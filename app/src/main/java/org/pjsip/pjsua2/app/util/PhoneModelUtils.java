package org.pjsip.pjsua2.app.util;

import android.util.Log;

public class PhoneModelUtils {
    private static String TAG = "PhoneModelUtils";

    public static String getPhoneBoard() {
        String phoneBoard = SystemPropertiesUtils.get("ro.product.vendor.board", "X7A");
        Log.i(TAG, "take phoneModel =" + phoneBoard);
        return phoneBoard;
    }

    public static String getPhoneModel() {
        String phoneModel = SystemPropertiesUtils.get("ro.product.vendor.model", "X7A");
        Log.i(TAG, "getPhoneSoftwareModel take phoneModel =" + phoneModel);
        return phoneModel;
    }
}

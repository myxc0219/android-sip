package org.pjsip.pjsua2.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ThemedSpinnerAdapter;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.app.adapter.SortAdapter;
import org.pjsip.pjsua2.app.dialog.ContactAddDialog;
import org.pjsip.pjsua2.app.dialog.ImNotifyDialog;
import org.pjsip.pjsua2.app.dialog.MbcForbidCallDialog;
import org.pjsip.pjsua2.app.dialog.MsgNotifyDialog;
import org.pjsip.pjsua2.app.dialog.MyAlertDialog;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.PersonBean;
import org.pjsip.pjsua2.app.model.Pstn;
import org.pjsip.pjsua2.app.ui.contacts.PinyinComparator;
import org.pjsip.pjsua2.app.ui.contacts.SideBar;
import org.pjsip.pjsua2.app.util.PinyinUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ContactsActivity extends BaseActivity implements Handler.Callback {
    public static Handler handler_;
    private final Handler handler = new Handler(this);

    private List<Pstn> pstnlist = new ArrayList<>();
    ArrayList<Map<String, String>> contactList;
    //private SimpleAdapter contactListAdapter;

    //表示当前显示列表的种类
    private int currentlist_state = -1;

    //处理删除id
    private Long deleteid = 0L;
    private String deletenumber = "";

    //检索排序相关
    private SortAdapter sortadapter;
    private ListView listView;
    private List<PersonBean> data;
    private SideBar sidebar;
    private TextView dialog;

    private ListView contactListView;
    private int contactListSelectedIdx = -1;

    private int isexpert=-1;

    private List<PersonBean> getData(List<Pstn> pstnList) {

        //String[] data
        List<PersonBean> listarray = new ArrayList<PersonBean>();
        //for (int i = 0; i < data.length; i++) {
        for(Pstn pstn : pstnList){

            String pinyin = PinyinUtils.getPingYin(String.valueOf(pstn.name));
            String Fpinyin = "#";
            if(!pinyin.equals("")){
                Fpinyin = pinyin.substring(0, 1).toUpperCase();
            }

            PersonBean person = new PersonBean();
            person.setName(pstn.name);
            person.setPinYin(pinyin);
            person.setAge(pstn.age);
            person.setPhone(pstn.pstn == null?pstn.callingnumber:pstn.pstn);
            person.setIsblacklist(pstn.isblacklist);

            // 正则表达式，判断首字母是否是英文字母
            if (Fpinyin.matches("[A-Z]")) {

                person.setSortLetters(Fpinyin);
            } else {

                person.setSortLetters("#");
            }

            listarray.add(person);
        }
        return listarray;
    }

    private void init() {
        // TODO Auto-generated method stub
        sidebar = (SideBar) findViewById(R.id.sidebar);
        listView = (ListView) findViewById(R.id.listViewContact);
        dialog = (TextView) findViewById(R.id.dialog);
        sidebar.setTextView(dialog);
        // 设置字母导航触摸监听
        sidebar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                // TODO Auto-generated method stub
                // 该字母首次出现的位置
                int position = sortadapter.getPositionForSelection(s.charAt(0));

                if (position != -1) {
                    listView.setSelection(position);
                }
            }
        });
        //data = getData(getResources().getStringArray(R.array.listpersons));
        data = getData(pstnlist);
        // 数据在放在adapter之前需要排序
        Collections.sort(data, new PinyinComparator());
        sortadapter = new SortAdapter(this, data,listView);
        listView.setAdapter(sortadapter);
    }

    public void showDailActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleDail);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_dail_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleDail);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_dail_disable));
        }
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, DailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void showMainActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_home_disabled));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_home_disabled));
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showHistoryActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_history_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_history_disable));
        }
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showContactsActivity(View view)
    {
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showCommunicateActivity(View view)
    {
        Intent intent = new Intent(this, CommunicateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showSettingActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_setting_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_setting_disable));
        }
        Intent intent = new Intent(this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showIntercomActivity(View view)
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, IntercomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        //setContentView(layoutIntercom);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler_ = handler;
        if(GlobalVariable.viewmodel == 1) {
            setContentView(R.layout.activity_contacts);
        }
        else if(GlobalVariable.viewmodel == 2){
            setContentView(R.layout.activity_contacts_ct);
        }
        else if(GlobalVariable.viewmodel == 3){
            setContentView(R.layout.activity_contacts_gs);
        }

        contactList = new ArrayList<Map<String, String>>();

//        String[] callin_from = { "name", "type" };
//        int[] callin_to = { R.id.textContacts1, R.id.textContacts2 };
//        contactListAdapter = new SimpleAdapter(
//                this, contactList,
//                R.layout.contacts_list_item,
//                callin_from, callin_to);

        contactListView = (ListView) findViewById(R.id.listViewContact);
        //先使用
        //contactListView.setAdapter(contactListAdapter);
        contactListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            final View view,
                                            int position, long id)
                    {
                        try {
                            sortadapter = (SortAdapter) listView.getAdapter();


                            view.setSelected(true);
                            contactListSelectedIdx = position;
                            for(PersonBean per :data){
                                per.setSelected(false);
                            }
                            PersonBean person = sortadapter.persons.get(position);
                            person.setSelected(true);

//                            sortadapter = new SortAdapter(ContactsActivity.this, data,listView);
//                            listView.setAdapter(sortadapter);
//                            sortadapter.notifyDataSetChanged();

                            sortadapter.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
        );
        contactListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            return true;
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        return false;
                    }
                }
        );

        init();
        //无导播模式显示拨出，其他均隐藏

        if(GlobalVariable.viewmodel == 1){
            ImageButton imageButtonDail = findViewById(R.id.contactsDail);
            ImageButton imageButtonTitle = findViewById(R.id.buttonICON2);
            if(GlobalVariable.charactername.equals("broadcaster")) {
                if (GlobalVariable.sipmodel == 3) {
                    imageButtonDail.setEnabled(false);
                    imageButtonDail.setBackgroundResource(R.color.black);
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
                //主播，单/多导播模式屏蔽
                if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){
                    imageButtonDail.setEnabled(false);
                    imageButtonDail.setBackgroundResource(R.color.black);
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
            }
            else{
                if (GlobalVariable.sipmodel == 0) {
                    imageButtonDail.setEnabled(false);
                    imageButtonDail.setBackgroundResource(R.color.black);
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
            }
        }
        else if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3){
            ImageButton imageButtonDail = findViewById(R.id.contactsDail);
            //ImageButton imageButtonTitle = findViewById(R.id.ctTitleDail);
            if(GlobalVariable.charactername.equals("broadcaster")) {
                if (GlobalVariable.sipmodel == 3) {
                    imageButtonDail.setEnabled(false);
                    imageButtonDail.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonDail.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
                }
                if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) {
                    imageButtonDail.setEnabled(false);
                    imageButtonDail.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonDail.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
                }
            }
            else{
                if (GlobalVariable.sipmodel == 0) {
                    imageButtonDail.setEnabled(false);
                    imageButtonDail.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonDail.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
                }
            }
        }

    }

    @Override
    protected void onStart() {

        if(GlobalVariable.isDL1129) {
            Button innercomBtn = findViewById(R.id.buttonMA6);
            innercomBtn.setVisibility(View.GONE);

            Button whitelistBtn = findViewById(R.id.ContactsType4);
            whitelistBtn.setVisibility(View.GONE);

            Button blacklistBtn = findViewById(R.id.ContactsType5);
            blacklistBtn.setVisibility(View.GONE);
        }

        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.ct_title_home));
            imgBtn1.setImageDrawable(getDrawable(R.drawable.ct_title_history));
            //imgBtn2.setImageDrawable(getDrawable(R.drawable.ct_title_contacts));
            imgBtn3.setImageDrawable(getDrawable(R.drawable.ct_title_setting));
            imgBtn4.setImageDrawable(getDrawable(R.drawable.ct_title_dail));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.gs_title_home));
            imgBtn1.setImageDrawable(getDrawable(R.drawable.gs_title_history));
            //imgBtn2.setImageDrawable(getDrawable(R.drawable.gs_title_contacts));
            imgBtn3.setImageDrawable(getDrawable(R.drawable.gs_title_setting));
            imgBtn4.setImageDrawable(getDrawable(R.drawable.gs_title_dail));
        }

        super.onStart();
//        Button ImButton = findViewById(R.id.buttonMA6);
//        if(GlobalVariable.IMRedTitle)
//            ImButton.setTextColor(Color.RED);
//        else
//            ImButton.setTextColor(Color.WHITE);

        new Thread(){
            @Override
            public void run() {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getPstnList",
                            "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                    "\"character\": \""+GlobalVariable.charactername+"\"," +
                                    "\"isexpert\": \"-1\"," +
                                    "\"iscontact\": \"1\"" +
                                    "}"
                    );
                    JSONObject rs_json = new JSONObject(rs);
                    JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                    pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                    data = getData(pstnlist);
                    // 数据在放在adapter之前需要排序
                    Collections.sort(data, new PinyinComparator());

//                    contactList.clear();
//                    for(Pstn pstn :pstnlist)
//                    {
//                        //"state", "callingnumber","starttime","type"
//                        contactList.add(putData(pstn.name.toString(),pstn.type.toString()));
//                    }
                    //获得解析数据后更新页面
                    Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                    m2.sendToTarget();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {
        if(msg.what == 1001){
            //contactListAdapter.notifyDataSetChanged();
            sortadapter = new SortAdapter(this, data,listView);
            listView.setAdapter(sortadapter);
            sortadapter.notifyDataSetChanged();
        }
        else if(msg.what == 1002){
            new Thread(){
                @Override
                public void run() {
                    try {
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getPstnList",
                                "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                        "\"character\": \""+GlobalVariable.charactername+"\"," +
                                        "\"isexpert\": \"-1\"," +
                                        "\"iscontact\": \"1\"" +
                                        "}"
                        );
                        JSONObject rs_json = new JSONObject(rs);
                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                        data = getData(pstnlist);
                        // 数据在放在adapter之前需要排序
                        Collections.sort(data, new PinyinComparator());
                        //获得解析数据后更新页面
                        Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                        m2.sendToTarget();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
        else if(msg.what == 1005){
            if(msg.obj!=null && GlobalVariable.prm!=null){
                //2021-04-09提醒内容更改为自定义格式
                //OnInstantMessageParam prm = (OnInstantMessageParam) msg.obj;
//                OnInstantMessageParam prm = GlobalVariable.prm;
//
//                String msgContent = "";
//                String username = "";
//                String template = "";
//                int type = -1;
//                try {
//                    JSONObject msg_json = new JSONObject(prm.getMsgBody());
//                    msgContent = msg_json.get("msg").toString();
//                    username = msg_json.get("username").toString();
//                    template = msg_json.get("template").toString();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(this,msgContent,username,template).create();
                ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(
                        this,
                        GlobalVariable.im_msg,
                        GlobalVariable.im_username,
                        GlobalVariable.im_template).create();
                imNotifyDialog.show();
                imNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                imNotifyDialog.getWindow().setLayout(500, 300);
            }
        }
        else if(msg.what == 1010){

            if(GlobalVariable.viewmodel == 1) {
                TextView timeView = (TextView) findViewById(R.id.mainTime);
                if (timeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    timeView.setText(dateString.replace(" ", "         "));
                }
                //刷新状态图标
                ImageView mainState1 = (ImageView) findViewById(R.id.MainState1);
                ImageView mainState2 = (ImageView) findViewById(R.id.MainState2);
                ImageView mainState3 = (ImageView) findViewById(R.id.MainState3);

                Button ImButton = findViewById(R.id.buttonMA6);
                if(GlobalVariable.IMRedTitle)
                {
                    if(GlobalVariable.IMRedShowed)
                    {
                        ImButton.setTextColor(Color.WHITE);
                        GlobalVariable.IMRedShowed = false;
                    }
                    else
                    {
                        ImButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.IMRedShowed = true;
                    }
                }
                else{
                    ImButton.setTextColor(Color.WHITE);
                }

                /**有未接听电话首页图标闪烁**/
                Button PhoneButton = findViewById(R.id.buttonMA3);
                if(GlobalVariable.PhoneRedTitle){
                    if(GlobalVariable.PhoneRedShowed5){
                        PhoneButton.setTextColor(Color.WHITE);
                        GlobalVariable.PhoneRedShowed5 = false;
                    }else{
                        PhoneButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.PhoneRedShowed5 = true;
                    }
                }else{
                    PhoneButton.setTextColor(Color.WHITE);
                }

                if(GlobalVariable.callring)
                {
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.allowring));
                }
                else{
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.disablering));
                }

                if(GlobalVariable.whitelistmodel)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.whitelistmodel));
                }
                else if(GlobalVariable.allowblacklist)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.allowblack));
                }
                else{
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.disableblack));
                }

                if(!GlobalVariable.online)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall == 1)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.busy));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall==2) {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.linebusy));
                    mainState3.setColorFilter(Color.TRANSPARENT);
                }
                else {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.allowin));
                    mainState3.setColorFilter(Color.WHITE);
                }

                TextView mainChannel = findViewById(R.id.mainChannel);
                TextView mainSipID = findViewById(R.id.mainSipID);
                mainChannel.setText(GlobalVariable.channelname);
                mainSipID.setText(GlobalVariable.user_sipid);
            }

            else if(GlobalVariable.viewmodel == 2) {
                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);

                Typeface typeface_fitcan = Typeface.createFromAsset(this.getAssets(), "fonts/BGOTHM.TTF");
                ct_title_username.setTypeface(typeface_fitcan);
                ct_title_name.setTypeface(typeface_fitcan);
                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_pdr));
                }
            }
            else if(GlobalVariable.viewmodel == 3) {
                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);
                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_pdr));
                }
            }
        }
        else if(msg.what == 1011){
            if(GlobalVariable.viewmodel == 1){
                ImageView titleImage = findViewById(R.id.titleImage);
                if(String.valueOf(msg.obj).equals("success")){titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo));}
                else{titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo_red));}
            }
            else if(GlobalVariable.viewmodel == 2){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(msg.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }
            else if(GlobalVariable.viewmodel == 3){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(msg.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }

        }
        else if(msg.what == 1014){
            String msg_content = String.valueOf(msg.obj);
            MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"系统状态已更改\n"+msg_content).create();
            msgNotifyDialog.show();
            if(GlobalVariable.viewmodel == 1)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            msgNotifyDialog.getWindow().setLayout(500, 400);
            final Timer t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    msgNotifyDialog.dismiss();
                    t.cancel();
                }
            }, 2000);
        }
        else if(msg.what == 1015){
            showMainActivity(null);
        }
        else if(msg.what == 1016){
            try{
                if(contactListSelectedIdx!=-1) {
                    PersonBean person = data.get(contactListSelectedIdx);
                    if(person!=null) {
                        deleteid = 0L;
                        deletenumber = "";
                        for(Pstn pstn:pstnlist){
                            if(person.getPhone().equals(pstn.pstn)){
                                deleteid = pstn.id;
                                deletenumber = pstn.pstn;
                            }
                            else if(person.getPhone().equals(pstn.callingnumber)){
                                deleteid = pstn.id;
                                deletenumber = pstn.callingnumber;
                            }
                        }
                        if(currentlist_state == -1 || currentlist_state == 1 || currentlist_state == 2){
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        //先删除应该要删除的项
                                        HttpRequest requestA = new HttpRequest();
                                        String rsA = requestA.postJson(
                                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deletePstnByAndroid",
                                                "{\"id\":\"" + deleteid + "\"}"
                                        );
                                        Log.e("删除通讯录日志",rsA+deleteid);
                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getPstnList",
                                                "{\"userid\":\"" + GlobalVariable.userid + "\", " +
                                                        "\"character\": \"" + GlobalVariable.charactername + "\"," +
                                                        "\"isexpert\": \"-1\"," +
                                                        "\"iscontact\": \"1\"" +
                                                        "}"
                                        );
                                        JSONObject rs_json = new JSONObject(rs);
                                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                                        data = getData(pstnlist);
                                        // 数据在放在adapter之前需要排序
                                        Collections.sort(data, new PinyinComparator());
                                        //更新页面
                                        Message m2 = Message.obtain(ContactsActivity.handler_, 1001, null);
                                        m2.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }
                        else if(currentlist_state == 3){
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        //先删除应该要删除的项
                                        HttpRequest requestA = new HttpRequest();
                                        String rsA = requestA.postJson(
                                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deleteWhiteBlackByAndroid",
                                                "{\"id\":\"" + deleteid + "\"" +
                                                        ",\"type\":\"white\""+
                                                        "}"
                                        );
                                        Log.e("删除白名单日志",rsA+deleteid);

                                        fullEditWhiteOrBlackState(deletenumber,true,false);


                                        try {
                                            HttpRequest request = new HttpRequest();
                                            String rs = request.postJson(
                                                    GlobalVariable.SEHTTPServer + "/listchange",
                                                    "{\"code\":" + 200 + ","
                                                            +"\"operate\": \""+"delete"+ "\","
                                                            +"\"phonenumber\": \""+ deletenumber + "\","
                                                            +"\"type\": \"whitelist\","
                                                            +"\"iswhitelist\": 0,"
                                                            +"\"isblacklist\": 0"
                                                            + "}"
                                            );
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }



                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getWhiteList",
                                                "{}"
                                        );
                                        JSONObject rs_json = new JSONObject(rs);
                                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                                        for(Pstn pstn:pstnlist){
                                            pstn.name = " ";
                                        }
                                        data = getData(pstnlist);
                                        // 数据在放在adapter之前需要排序
                                        Collections.sort(data, new PinyinComparator());
                                        //获得解析数据后更新页面
                                        Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                                        m2.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }
                        else if(currentlist_state == 4){
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        //先删除应该要删除的项
                                        HttpRequest requestA = new HttpRequest();
                                        String rsA = requestA.postJson(
                                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deleteWhiteBlackByAndroid",
                                                "{\"id\":\"" + deleteid + "\"" +
                                                        ",\"type\":\"black\""+
                                                        "}"
                                        );
                                        Log.e("删除黑名单日志",rsA+deleteid);

                                        fullEditWhiteOrBlackState(deletenumber,false,true);

                                        try {
                                            HttpRequest request = new HttpRequest();
                                            String rs = request.postJson(
                                                    GlobalVariable.SEHTTPServer + "/listchange",
                                                    "{\"code\":" + 200 + ","
                                                            +"\"operate\": \""+"delete"+ "\","
                                                            +"\"phonenumber\": \""+ deletenumber + "\","
                                                            +"\"type\": \"blacklist\","
                                                            +"\"iswhitelist\": 0,"
                                                            +"\"isblacklist\": 0"
                                                            + "}"
                                            );
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getWhiteList",
                                                "{}"
                                        );
                                        JSONObject rs_json = new JSONObject(rs);
                                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                                        for(Pstn pstn:pstnlist){
                                            pstn.name = " ";
                                        }
                                        data = getData(pstnlist);
                                        // 数据在放在adapter之前需要排序
                                        Collections.sort(data, new PinyinComparator());
                                        //获得解析数据后更新页面
                                        Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                                        m2.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }

                    }
                }
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        else if(msg.what == 2000){
            //挂机逻辑处理
            showMainActivity(null);
        }
        else if(msg.what == 2001){
            //摘机逻辑处理
            ImageButton imgBtn = findViewById(R.id.contactsDail);
            if(imgBtn.isEnabled()) {
                ContactsDail(null);
            }
        }
        return true;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler_ = null;
    }

    private HashMap<String, String> putData(String name, String type)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("type", type);
        return item;
    }

    public void getCallRecordInfo(MainActivity.MyCallWithState callWithState, String finalPstn, String uid){
        HttpRequest request = new HttpRequest();
        String rs = request.postJson(
                "Http://"+GlobalVariable.SIPSERVER+":8080/smart/contactWithSipServer/getPstnByPstn",
                "{\"type\":\"post pstn info\", " +
                        "\"pstn\":\""+ finalPstn +"\", " +
                        "\"callid\":\""+ uid +"\"}"
        );

        try {
            org.json.JSONObject rs_json = new org.json.JSONObject(rs);
            JSONArray rs_jsonarray = rs_json.getJSONObject("data").getJSONArray("pstns");
            if(rs_jsonarray.length()>0)
            {
                org.json.JSONObject rs_json_pstn = rs_jsonarray.getJSONObject(0);
                callWithState.pstn = rs_json_pstn.get("pstn")==null?"":rs_json_pstn.get("pstn").toString();
                callWithState.name = rs_json_pstn.get("name")==null?"":rs_json_pstn.get("name").toString();
                callWithState.age =  rs_json_pstn.get("age")==null? 0:(Integer)rs_json_pstn.get("age");
                callWithState.male = rs_json_pstn.get("male")==null? 0:(Integer) rs_json_pstn.get("male");
                callWithState.from = rs_json_pstn.get("frm")==null?"":rs_json_pstn.get("frm").toString();
                callWithState.iswhitelist = String.valueOf(rs_json_pstn.get("iswhitelist")).equals("1")?1:0;
                callWithState.isblacklist = String.valueOf(rs_json_pstn.get("isblacklist")).equals("1")?1:0;
                callWithState.iscontact = String.valueOf(rs_json_pstn.get("iscontact")).equals("1")?1:0;
                callWithState.isexpert = String.valueOf(rs_json_pstn.get("isexpert")).equals("1")?1:0;
            }


            org.json.JSONObject rs_json_callrecord = rs_json.getJSONObject("data").getJSONObject("callrecord");
            if(rs_json_callrecord!=null)
            {
                callWithState.content = rs_json_callrecord.get("content")==null?"":rs_json_callrecord.get("content").toString();
                callWithState.pstnmark = rs_json_callrecord.get("pstnmark")==null?"":rs_json_callrecord.get("pstnmark").toString();
                callWithState.pdrmark = rs_json_callrecord.get("pdrmark")==null?"":rs_json_callrecord.get("pdrmark").toString();
                callWithState.mbcmark = rs_json_callrecord.get("mbcmark")==null?"":rs_json_callrecord.get("mbcmark").toString();
            }
        } catch (Exception e) {
            LogClient.generate("【CallRecord获取转换错误】"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void fullEditWhiteOrBlackState(String pstn_number,Boolean white_edit,Boolean black_edit){
        MainActivity.MyCallWithState callws = new MainActivity.MyCallWithState();
        callws.pstn = pstn_number;
        callws.uid = "1";
        new Thread() {
            @Override
            public void run() {
                getCallRecordInfo(callws, callws.pstn, callws.uid);
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoHis",
                        "{\"type\":\"post pstn info\", " +
                                "\"pstn\":\"" + callws.pstn + "\", " +
                                "\"callid\":\"" + callws.uid + "\", " +
                                "\"name\":\"" + ((callws.name==null||callws.name.equals(""))?"":callws.name) + "\", " +
                                "\"iswhitelist\":\"" + (white_edit?0:(callws.iswhitelist==null?0:callws.iswhitelist)) + "\", " +
                                "\"isblacklist\":\"" + (black_edit?0:(callws.isblacklist==null?0:callws.isblacklist)) + "\", " +
                                "\"iscontact\":\"" + (callws.iscontact==null?0:callws.iscontact) + "\", " +
                                "\"isexpert\":\"" + (callws.isexpert==null?0:callws.isexpert) + "\"" +
                                "}"
                );
            }
        }.start();
    }

    public void ContactsAdd(View view){

            ContactAddDialog contactAddDialog = new ContactAddDialog.Builder(ContactsActivity.this).create();
            contactAddDialog.show();
            if(GlobalVariable.viewmodel == 1)
                contactAddDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                contactAddDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                contactAddDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            contactAddDialog.getWindow().setLayout(700, 500);

    }

    public void ContactsDeleteOld(View view){
        if(data.size()>0){
            new  AlertDialog.Builder(this)
                    .setTitle("确认" )
                    .setMessage("确定删除吗？" )
                    .setPositiveButton("是" ,  new DialogInterface.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try{
                                if(contactListSelectedIdx!=-1) {
                                    PersonBean person = data.get(contactListSelectedIdx);
                                    if(person!=null) {
                                        deleteid = 0L;
                                        for(Pstn pstn:pstnlist){
                                            Log.e("当前通讯录人员",""+pstn.id);
                                            Log.e("比较Phone和callingnumber",person.getPhone()+" VS "+pstn.pstn);
                                            if(person.getPhone().equals(pstn.pstn)){
                                                deleteid = pstn.id;
                                            }
                                        }
                                        new Thread() {
                                            @Override
                                            public void run() {
                                                try {
                                                    //先删除应该要删除的项
                                                    HttpRequest requestA = new HttpRequest();
                                                    String rsA = requestA.postJson(
                                                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deletePstnByAndroid",
                                                            "{\"id\":\"" + deleteid + "\"}"
                                                    );
                                                    Log.e("删除通讯录日志",rsA+deleteid);
                                                    HttpRequest request = new HttpRequest();
                                                    String rs = request.postJson(
                                                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getPstnList",
                                                            "{\"userid\":\"" + GlobalVariable.userid + "\", " +
                                                                    "\"character\": \"" + GlobalVariable.charactername + "\"," +
                                                                    "\"isexpert\": \"-1\"," +
                                                                    "\"iscontact\": \"1\"" +
                                                                    "}"
                                                    );
                                                    JSONObject rs_json = new JSONObject(rs);
                                                    JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                                                    pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                                                    data = getData(pstnlist);
                                                    // 数据在放在adapter之前需要排序
                                                    Collections.sort(data, new PinyinComparator());
                                                    //更新页面
                                                    Message m2 = Message.obtain(ContactsActivity.handler_, 1001, null);
                                                    m2.sendToTarget();
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }.start();
                                    }
                                }
                            }
                            catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                    }).setNegativeButton("否" ,   new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();
        }

    }

    public void ContactsDelete(View view){
        if(data.size()>0){
            MyAlertDialog myAlertDialog = new MyAlertDialog.Builder(this,"确认删除?").create();
            myAlertDialog.show();
            if(GlobalVariable.viewmodel == 1)
                myAlertDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                myAlertDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                myAlertDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            myAlertDialog.getWindow().setLayout(500, 250);
        }

    }

    //联系人根据姓名
    public void ContactsDail(View view)
    {
        if(GlobalVariable.charactername.equals("broadcaster")&&(GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2 || GlobalVariable.sipmodel == 3))
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(ContactsActivity.this,"请在导播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }
        if(GlobalVariable.charactername.equals("director")&&GlobalVariable.sipmodel == 0)
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(ContactsActivity.this,"请在主播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }

        if(data.size()>0) {
            try {
                if (contactListSelectedIdx != -1) {
                    PersonBean person = data.get(contactListSelectedIdx);
                    //Pstn pstn = pstnlist.get(contactListSelectedIdx);
                    if (person != null) {
                        if(GlobalVariable.viewmodel == 1){
                            GlobalVariable.setdailvalue = true;
                            GlobalVariable.dailvalue = person.getPhone();

                            if (DailActivity.handler_ != null) {
                                Message m2 = Message.obtain(DailActivity.handler_, 1002, null);
                                m2.sendToTarget();
                            }

                            Intent intent = new Intent(this, DailActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        }
                        else if(GlobalVariable.viewmodel == 2) {
                            if (MainActivity.handler_ != null) {
                                Message m2 = Message.obtain(MainActivity.handler_, 1020, person.getPhone());
                                m2.sendToTarget();
                            }
                            Intent intent = new Intent(this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        }
                        else if(GlobalVariable.viewmodel == 3) {
                            if (MainActivity.handler_ != null) {
                                Message m2 = Message.obtain(MainActivity.handler_, 1020, person.getPhone());
                                m2.sendToTarget();
                            }
                            Intent intent = new Intent(this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            startActivity(intent);
                        }
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }

    }

    public void showContactsDetail(View view)
    {
        Pstn pstn = null;
        Integer position = (Integer) view.getTag();
        if(pstnlist!=null)
        {
            try {
                pstn = pstnlist.get(position);
            }catch (Exception e){

            }
        }
        if(pstn!=null)
        {
            dlgContactsDetail(pstn);
        }
    }
    private void dlgContactsDetail(Pstn pstn)
    {

        LayoutInflater li = LayoutInflater.from(ContactsActivity.this);
        View detailview = li.inflate(R.layout.dlg_pstn_detail, null);

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setView(detailview);
        adb.setTitle("详细信息");
        adb.setCancelable(false);
        adb.setPositiveButton("确认",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {

                    }
                }
        );
        adb.setNegativeButton("取消",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                }
        );



//        TextView textPstnmark = (TextView)detailview.findViewById(R.id.textHISPstnmark);
//        TextView textContent = (TextView)detailview.findViewById(R.id.textHISContent);
//        TextView textPdrmark = (TextView)detailview.findViewById(R.id.textHISPdrmark);
//        TextView textMbcmark = (TextView)detailview.findViewById(R.id.textHISMbcmark);
//
//        textPstnmark.setText(record.pstnmark==null?"":record.pstnmark);
//        textContent.setText(record.content==null?"":record.content);
//        textPdrmark.setText(record.pdrmark==null?"":record.pdrmark);
//        textMbcmark.setText(record.mbcmark==null?"":record.mbcmark);
//


        AlertDialog ad = adb.create();
        ad.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
        ad.show();
    }

    public void refreshContactsList(View view)
    {
        listView.clearChoices();

        Integer state = Integer.parseInt(view.getTag().toString());
        currentlist_state = state;

        if(GlobalVariable.viewmodel == 1) {
            Button contactsType1 = findViewById(R.id.ContactsType1);
            Button contactsType2 = findViewById(R.id.ContactsType2);
            Button contactsType3 = findViewById(R.id.ContactsType3);
            Button contactsType4 = findViewById(R.id.ContactsType4);
            Button contactsType5 = findViewById(R.id.ContactsType5);
            if (state == -1) {
                isexpert = -1;
                contactsType1.setBackgroundResource(R.drawable.button_selector_title_thin);
                contactsType2.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType3.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType4.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType5.setBackgroundResource(R.drawable.button_selector_transparent);
            } else if (state == 1) {
                isexpert = 0;
                contactsType1.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType2.setBackgroundResource(R.drawable.button_selector_title_thin);
                contactsType3.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType4.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType5.setBackgroundResource(R.drawable.button_selector_transparent);
            } else if (state == 2) {
                isexpert = 1;
                contactsType1.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType2.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType3.setBackgroundResource(R.drawable.button_selector_title_thin);
                contactsType4.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType5.setBackgroundResource(R.drawable.button_selector_transparent);
            } else if (state == 3) {
                //白名单模式
                contactsType1.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType2.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType3.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType4.setBackgroundResource(R.drawable.button_selector_title_thin);
                contactsType5.setBackgroundResource(R.drawable.button_selector_transparent);
            } else if (state == 4) {
                //黑名单模式
                contactsType1.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType2.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType3.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType4.setBackgroundResource(R.drawable.button_selector_transparent);
                contactsType5.setBackgroundResource(R.drawable.button_selector_title_thin);
            }
        }
        else if(GlobalVariable.viewmodel == 2){
            if (state == -1) {
                isexpert = -1;
            } else if (state == 1) {
                isexpert = 0;
            } else if (state == 2) {
                isexpert = 1;
            } else if (state == 3) {
                //白名单模式
            } else if (state == 4) {
                //黑名单模式
            }
        }
        else if(GlobalVariable.viewmodel == 3){
            if (state == -1) {
                isexpert = -1;
            } else if (state == 1) {
                isexpert = 0;
            } else if (state == 2) {
                isexpert = 1;
            } else if (state == 3) {
                //白名单模式
            } else if (state == 4) {
                //黑名单模式
            }
        }
        if( state == -1 || state == 1 || state == 2 ){//获得通讯录
            new Thread(){
                @Override
                public void run() {
                    try {
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getPstnList",
                                "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                        "\"character\": \""+GlobalVariable.charactername+"\"," +
                                        "\"isexpert\": \""+isexpert+"\"," +
                                        "\"iscontact\": \"1\"" +
                                        "}"
                        );
                        JSONObject rs_json = new JSONObject(rs);
                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                        data = getData(pstnlist);
                        // 数据在放在adapter之前需要排序
                        Collections.sort(data, new PinyinComparator());
                        //获得解析数据后更新页面
                        Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                        m2.sendToTarget();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
        if( state == 3 ){//获得白名单
            new Thread(){
                @Override
                public void run() {
                    try {
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getWhiteList",
                                "{}"
                        );
                        JSONObject rs_json = new JSONObject(rs);
                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                        for(Pstn pstn:pstnlist){
                            pstn.name = " ";
                        }
                        data = getData(pstnlist);
                        // 数据在放在adapter之前需要排序
                        Collections.sort(data, new PinyinComparator());
                        //获得解析数据后更新页面
                        Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                        m2.sendToTarget();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
        if( state == 4 ){//获得黑名单
            new Thread(){
                @Override
                public void run() {
                    try {
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getBlackList",
                                "{}"
                        );
                        JSONObject rs_json = new JSONObject(rs);
                        JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                        data = getData(pstnlist);
                        for(Pstn pstn:pstnlist){
                            pstn.name = " ";
                        }
                        // 数据在放在adapter之前需要排序
                        Collections.sort(data, new PinyinComparator());
                        //获得解析数据后更新页面
                        Message m2 = Message.obtain(ContactsActivity.handler_,1001,null);
                        m2.sendToTarget();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }

    }
}

    @RequiresApi(api = Build.VERSION_CODES.M)
    class ContactAdapter extends BaseAdapter implements Filterable, ThemedSpinnerAdapter {
        private final LayoutInflater mInflater;

        private int[] mTo;
        private String[] mFrom;
        private android.widget.SimpleAdapter.ViewBinder mViewBinder;

        private List<? extends Map<String, ?>> mData;

        private int mResource;
        private int mDropDownResource;

        /** Layout inflater used for {@link #getDropDownView(int, View, ViewGroup)}. */
        private LayoutInflater mDropDownInflater;

        private org.pjsip.pjsua2.app.ContactAdapter.SimpleFilter mFilter;
        private ArrayList<Map<String, ?>> mUnfilteredData;

        private ListView mListView;

        /**
         * Constructor
         *
         * @param context The context where the View associated with this SimpleAdapter is running
         * @param data A List of Maps. Each entry in the List corresponds to one row in the list. The
         *        Maps contain the data for each row, and should include all the entries specified in
         *        "from"
         * @param resource Resource identifier of a view layout that defines the views for this list
         *        item. The layout file should include at least those named views defined in "to"
         * @param from A list of column names that will be added to the Map associated with each
         *        item.
         * @param to The views that should display column in the "from" parameter. These should all be
         *        TextViews. The first N views in this list are given the values of the first N columns
         *        in the from parameter.
         */
        public ContactAdapter(Context context, List<? extends Map<String, ?>> data,
                              @LayoutRes int resource, String[] from, @IdRes int[] to,ListView listview) {
            mData = data;
            mResource = mDropDownResource = resource;
            mFrom = from;
            mTo = to;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            mListView = listview;
        }

        /**
         * @see android.widget.Adapter#getCount()
         */
        public int getCount() {
            return mData.size();
        }

        /**
         * @see android.widget.Adapter#getItem(int)
         */
        public Object getItem(int position) {
            return mData.get(position);
        }

        /**
         * @see android.widget.Adapter#getItemId(int)
         */
        public long getItemId(int position) {
            return position;
        }

        /**
         * @see android.widget.Adapter#getView(int, View, ViewGroup)
         */
        public View getView(int position, View convertView, ViewGroup parent) {

            return createViewFromResource(mInflater, position, convertView, parent, mResource);
        }

        private View createViewFromResource(LayoutInflater inflater, int position, View convertView,
                                            ViewGroup parent, int resource) {
            View v;

            boolean currentselected = false;
            int selectposition = mListView.getCheckedItemPosition();
            if(selectposition==position)
                currentselected = true;


            if (convertView == null) {
                v = inflater.inflate(resource, parent, false);
            } else {
                v = convertView;
            }
            ImageButton detailbutton = (ImageButton) v.findViewById(R.id.button_HISDetail);
            detailbutton.setTag(position);

            bindView(position, v);

            return v;
        }

        /**
         * <p>Sets the layout resource to create the drop down views.</p>
         *
         * @param resource the layout resource defining the drop down views
         * @see #getDropDownView(int, android.view.View, android.view.ViewGroup)
         */
        public void setDropDownViewResource(int resource) {
            mDropDownResource = resource;
        }

        /**
         * Sets the {@link android.content.res.Resources.Theme} against which drop-down views are
         * inflated.
         * <p>
         * By default, drop-down views are inflated against the theme of the
         * {@link Context} passed to the adapter's constructor.
         *
         * @param theme the theme against which to inflate drop-down views or
         *              {@code null} to use the theme from the adapter's context
         * @see #getDropDownView(int, View, ViewGroup)
         */
        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        public void setDropDownViewTheme(Resources.Theme theme) {
            if (theme == null) {
                mDropDownInflater = null;
            } else if (theme == mInflater.getContext().getTheme()) {
                mDropDownInflater = mInflater;
            } else {
                final Context context = new ContextThemeWrapper(mInflater.getContext(), theme);
                mDropDownInflater = LayoutInflater.from(context);
            }
        }

        @Override
        public Resources.Theme getDropDownViewTheme() {
            return mDropDownInflater == null ? null : mDropDownInflater.getContext().getTheme();
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            final LayoutInflater inflater = mDropDownInflater == null ? mInflater : mDropDownInflater;
            return createViewFromResource(inflater, position, convertView, parent, mDropDownResource);
        }

        private void bindView(int position, View view) {
            final Map dataSet = mData.get(position);
            if (dataSet == null) {
                return;
            }

            final android.widget.SimpleAdapter.ViewBinder binder = mViewBinder;
            final String[] from = mFrom;
            final int[] to = mTo;
            final int count = to.length;

            for (int i = 0; i < count; i++) {
                final View v = view.findViewById(to[i]);
                if (v != null) {
                    final Object data = dataSet.get(from[i]);
                    String text = data == null ? "" : data.toString();
                    if (text == null) {
                        text = "";
                    }

                    boolean bound = false;
                    if (binder != null) {
                        bound = binder.setViewValue(v, data, text);
                    }

                    if (!bound) {
                        if (v instanceof Checkable) {
                            if (data instanceof Boolean) {
                                ((Checkable) v).setChecked((Boolean) data);
                            } else if (v instanceof TextView) {
                                // Note: keep the instanceof TextView check at the bottom of these
                                // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                                setViewText((TextView) v, text);
                            } else {
                                throw new IllegalStateException(v.getClass().getName() +
                                        " should be bound to a Boolean, not a " +
                                        (data == null ? "<unknown type>" : data.getClass()));
                            }
                        } else if (v instanceof TextView) {
                            // Note: keep the instanceof TextView check at the bottom of these
                            // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                            setViewText((TextView) v, text);
                        } else if (v instanceof ImageView) {
                            if (data instanceof Integer) {
                                setViewImage((ImageView) v, (Integer) data);
                            } else {
                                setViewImage((ImageView) v, text);
                            }
                        } else {
                            throw new IllegalStateException(v.getClass().getName() + " is not a " +
                                    " view that can be bounds by this SimpleAdapter");
                        }
                    }
                }
            }
        }

        /**
         * Returns the {@link android.widget.SimpleAdapter.ViewBinder} used to bind data to views.
         *
         * @return a ViewBinder or null if the binder does not exist
         *
         * @see #setViewBinder(android.widget.SimpleAdapter.ViewBinder)
         */
        public android.widget.SimpleAdapter.ViewBinder getViewBinder() {
            return mViewBinder;
        }

        /**
         * Sets the binder used to bind data to views.
         *
         * @param viewBinder the binder used to bind data to views, can be null to
         *        remove the existing binder
         *
         * @see #getViewBinder()
         */
        public void setViewBinder(android.widget.SimpleAdapter.ViewBinder viewBinder) {
            mViewBinder = viewBinder;
        }

        /**
         * Called by bindView() to set the image for an ImageView but only if
         * there is no existing ViewBinder or if the existing ViewBinder cannot
         * handle binding to an ImageView.
         *
         * This method is called instead of {@link #setViewImage(ImageView, String)}
         * if the supplied data is an int or Integer.
         *
         * @param v ImageView to receive an image
         * @param value the value retrieved from the data set
         *
         * @see #setViewImage(ImageView, String)
         */
        public void setViewImage(ImageView v, int value) {
            v.setImageResource(value);
        }

        /**
         * Called by bindView() to set the image for an ImageView but only if
         * there is no existing ViewBinder or if the existing ViewBinder cannot
         * handle binding to an ImageView.
         *
         * By default, the value will be treated as an image resource. If the
         * value cannot be used as an image resource, the value is used as an
         * image Uri.
         *
         * This method is called instead of {@link #setViewImage(ImageView, int)}
         * if the supplied data is not an int or Integer.
         *
         * @param v ImageView to receive an image
         * @param value the value retrieved from the data set
         *
         * @see #setViewImage(ImageView, int)
         */
        public void setViewImage(ImageView v, String value) {
            try {
                v.setImageResource(Integer.parseInt(value));
            } catch (NumberFormatException nfe) {
                v.setImageURI(Uri.parse(value));
            }
        }

        /**
         * Called by bindView() to set the text for a TextView but only if
         * there is no existing ViewBinder or if the existing ViewBinder cannot
         * handle binding to a TextView.
         *
         * @param v TextView to receive text
         * @param text the text to be set for the TextView
         */
        public void setViewText(TextView v, String text) {
            v.setText(text);
        }

        public Filter getFilter() {
            if (mFilter == null) {
                mFilter = new org.pjsip.pjsua2.app.ContactAdapter.SimpleFilter();
            }
            return mFilter;
        }

        /**
         * This class can be used by external clients of SimpleAdapter to bind
         * values to views.
         *
         * You should use this class to bind values to views that are not
         * directly supported by SimpleAdapter or to change the way binding
         * occurs for views supported by SimpleAdapter.
         *
         * @see android.widget.SimpleAdapter#setViewImage(ImageView, int)
         * @see android.widget.SimpleAdapter#setViewImage(ImageView, String)
         * @see android.widget.SimpleAdapter#setViewText(TextView, String)
         */
        public static interface ViewBinder {
            /**
             * Binds the specified data to the specified view.
             *
             * When binding is handled by this ViewBinder, this method must return true.
             * If this method returns false, SimpleAdapter will attempts to handle
             * the binding on its own.
             *
             * @param view the view to bind the data to
             * @param data the data to bind to the view
             * @param textRepresentation a safe String representation of the supplied data:
             *        it is either the result of data.toString() or an empty String but it
             *        is never null
             *
             * @return true if the data was bound to the view, false otherwise
             */
            boolean setViewValue(View view, Object data, String textRepresentation);
        }

        /**
         * <p>An array filters constrains the content of the array adapter with
         * a prefix. Each item that does not start with the supplied prefix
         * is removed from the list.</p>
         */
        private class SimpleFilter extends Filter {

            @Override
            protected FilterResults performFiltering(CharSequence prefix) {
                FilterResults results = new FilterResults();

                if (mUnfilteredData == null) {
                    mUnfilteredData = new ArrayList<Map<String, ?>>(mData);
                }

                if (prefix == null || prefix.length() == 0) {
                    ArrayList<Map<String, ?>> list = mUnfilteredData;
                    results.values = list;
                    results.count = list.size();
                } else {
                    String prefixString = prefix.toString().toLowerCase();

                    ArrayList<Map<String, ?>> unfilteredValues = mUnfilteredData;
                    int count = unfilteredValues.size();

                    ArrayList<Map<String, ?>> newValues = new ArrayList<Map<String, ?>>(count);

                    for (int i = 0; i < count; i++) {
                        Map<String, ?> h = unfilteredValues.get(i);
                        if (h != null) {

                            int len = mTo.length;

                            for (int j=0; j<len; j++) {
                                String str =  (String)h.get(mFrom[j]);

                                String[] words = str.split(" ");
                                int wordCount = words.length;

                                for (int k = 0; k < wordCount; k++) {
                                    String word = words[k];

                                    if (word.toLowerCase().startsWith(prefixString)) {
                                        newValues.add(h);
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    results.values = newValues;
                    results.count = newValues.size();
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                //noinspection unchecked
                mData = (List<Map<String, ?>>) results.values;
                if (results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        }

}

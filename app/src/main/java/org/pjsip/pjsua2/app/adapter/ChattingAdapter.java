package org.pjsip.pjsua2.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.model.ChatMessage;

import java.util.List;

public class ChattingAdapter extends BaseAdapter {
    protected static final String TAG = "ChattingAdapter";
    private Context context;

    private List<ChatMessage> chatMessages;

    public ChattingAdapter(Context context, List<ChatMessage> messages) {
        super();
        this.context = context;
        this.chatMessages = messages;

    }

    public int getCount() {
        return chatMessages.size();
    }

    public Object getItem(int position) {
        return chatMessages.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        ChatMessage message = chatMessages.get(position);
        if (convertView == null || (holder = (ViewHolder) convertView.getTag()).flag != message.getDirection()) {

            holder = new ViewHolder();
            if (message.getDirection() == ChatMessage.MESSAGE_FROM) {
                holder.flag = ChatMessage.MESSAGE_FROM;

                convertView = LayoutInflater.from(context).inflate(R.layout.chatting_item_from, null);
                ImageView img_view = convertView.findViewById(R.id.chatting_user_img);
                if(GlobalVariable.charactername.equals("director"))
                {
                   img_view.setImageDrawable(convertView.getResources().getDrawable(R.drawable.chat_mbc));
                }
                else{
                    img_view.setImageDrawable(convertView.getResources().getDrawable(R.drawable.chat_pdr));
                }



            } else {
                holder.flag = ChatMessage.MESSAGE_TO;
                convertView = LayoutInflater.from(context).inflate(R.layout.chatting_item_to, null);
                ImageView img_view = convertView.findViewById(R.id.chatting_user_img);
                if(GlobalVariable.charactername.equals("director"))
                {
                    img_view.setImageDrawable(convertView.getResources().getDrawable(R.drawable.chat_pdr));
                }
                else{
                    img_view.setImageDrawable(convertView.getResources().getDrawable(R.drawable.chat_mbc));
                }
            }

            holder.text = (TextView) convertView.findViewById(R.id.chatting_content_itv);
            holder.timetext =  (TextView) convertView.findViewById(R.id.chatting_time_tv);
            holder.templatetext = (TextView) convertView.findViewById(R.id.chatting_template_tv);
            holder.usertext = (TextView) convertView.findViewById(R.id.chatting_user_tv);
            convertView.setTag(holder);
        }
        holder.text.setText(message.getContent());
        holder.timetext.setText(message.getTime());
        holder.templatetext.setText(message.getTemplatename());
        holder.usertext.setText(message.getUsername());


        return convertView;
    }
    //优化listview的Adapter
    static class ViewHolder {
        TextView text;
        TextView timetext;
        TextView usertext;
        TextView templatetext;
        int flag;
    }

}
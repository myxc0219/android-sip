package org.pjsip.pjsua2.app.model;

public class Channel {
    private Long parentid;
    private Long pgmid;
    private Long id;
    private String parentname;
    private String name;

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public Long getPgmid() {
        return pgmid;
    }

    public void setPgmid(Long pgmid) {
        this.pgmid = pgmid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getParentname() {
        return parentname;
    }

    public void setParentname(String parentname) {
        this.parentname = parentname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

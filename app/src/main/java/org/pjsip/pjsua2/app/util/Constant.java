package org.pjsip.pjsua2.app.util;

import android.os.Environment;


public class Constant {
    public static final String TMP_DIRPATH = Environment.getExternalStorageDirectory().getPath() + "/democasei-tmp/";
    public static final String AUDIO_MUTE = "vendor.audio.mute";
    public static final String PHONE_MODEL_X7A = "X7A";
    public static final String PHONE_MODEL_I56A = "i56A";
    public static final String PHONE_MODEL_J7A = "J7A";
    public static final String PHONE_MODEL_F600S = "F600S";
    public static final String PHONE_MODEL_VS32 = "VS32";
    public static final String RING_FILE_PATH = "/system/media/audio/ringtones/Happy_Technology_Logo.ogg";
}

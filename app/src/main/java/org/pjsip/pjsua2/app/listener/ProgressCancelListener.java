package org.pjsip.pjsua2.app.listener;

public interface ProgressCancelListener {
    void onCancelProgress();
}

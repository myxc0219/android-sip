package org.pjsip.pjsua2.app.util;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMediaManager {
    private static String TAG = "CommonMediaManager";

    public static final int AUDIO_DEVICE_NONE = 0;
    public static final int AUDIO_DEVICE_SPEAKER = 1;
    public static final int AUDIO_DEVICE_HANDSET = 2;
    public static final int AUDIO_DEVICE_HEADSET = 3;

    public static final int TONE_UNKNOWN = 0;
    public static final int TONE_DIAL = 1;
    public static final int TONE_BUSY = 2;
    public static final int TONE_CONGESTION = 3;
    public static final int TONE_RADIO_ACK = 4;
    public static final int TONE_RADIO_NOTAVAIL = 5;
    public static final int TONE_ERROR = 6;
    public static final int TONE_CALL_WAITING = 7;
    public static final int TONE_RINGTONE = 8;
    public static final int TONE_BEEP = 9;
    public static final int TONE_ACK = 10;
    public static final int TONE_NACK = 11;
    public static final int TONE_MAX = TONE_NACK;

    private static final int MESSAGE_START_VOICE_LOOPBACK = 0;
    private static final int MESSAGE_STOP_RECORDING = 1;
    private static final int MESSAGE_PLAY_TONE = 2;
    private static final int MESSAGE_PLAY_RING = 3;
    private static final int MESSAGE_STOP_PLAY = 4;
    private TimerTask mTimerTask;
    private Timer mTimer = new Timer(true);
    private String mSpeakerLeftString;
    private String mSpeakerRightString;
    private String mDac1LeftString;
    private String mDac1RightString;
    private String mCaptureLeftString;
    private String mCaptureRightString;

    private static final HashMap<Integer, Integer> mToneMap = new HashMap<Integer, Integer>();

    static {
        // Map STK tone ids to the system tone ids.
        mToneMap.put(TONE_DIAL, ToneGenerator.TONE_SUP_DIAL);
        mToneMap.put(TONE_BUSY, ToneGenerator.TONE_SUP_BUSY);
        mToneMap.put(TONE_CONGESTION, ToneGenerator.TONE_SUP_CONGESTION);
        mToneMap.put(TONE_RADIO_ACK, ToneGenerator.TONE_SUP_RADIO_ACK);
        mToneMap.put(TONE_RADIO_NOTAVAIL, ToneGenerator.TONE_SUP_RADIO_NOTAVAIL);
        mToneMap.put(TONE_ERROR, ToneGenerator.TONE_SUP_ERROR);
        mToneMap.put(TONE_CALL_WAITING, ToneGenerator.TONE_SUP_CALL_WAITING);
        mToneMap.put(TONE_RINGTONE, ToneGenerator.TONE_SUP_RINGTONE);
        mToneMap.put(TONE_BEEP, ToneGenerator.TONE_PROP_BEEP);
        mToneMap.put(TONE_ACK, ToneGenerator.TONE_PROP_ACK);
        mToneMap.put(TONE_NACK, ToneGenerator.TONE_PROP_NACK);
    }

    private Context mContext = null;
    private AudioManager mAudioManager = null;
    private HandlerThread mMediaThread;
    private MediaHandler mMediaHandler;
    private boolean mToneUseSystemStream = true;
    private int volumeValue;

    private static CommonMediaManager sInstance;

    public static CommonMediaManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new CommonMediaManager(context);
            sInstance.getSpeakerVoiceLoopCaptureVolumeBefore();
            sInstance.getSpeakerVoiceLoopPlayVolumeBefore();
        }
        return sInstance;
    }

    public static void release() {
        Log.i(TAG, "static void release");
        if (sInstance != null) {
            sInstance.destroy();
            sInstance = null;
        }
    }

    private CommonMediaManager(Context context) {
        Log.i(TAG, "MediaManager");
        mContext = context;
        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mMediaThread = new HandlerThread("MediaHandler");
        mMediaThread.start();
        mMediaHandler = new MediaHandler(mMediaThread.getLooper());
        volumeValue = SystemPropertiesUtils.getInt("ro.config.ring_vol_steps", 7);
    }

    public void setVoiceAudioDevice(int audioDevice) {
        Log.i(TAG, "setVoiceAudioDevice: audioDevice=" + getAudioDeviceString(audioDevice));

        switch (audioDevice) {
            case AUDIO_DEVICE_SPEAKER:
                AudioManagerUtils.setSpeakerphoneOn(mAudioManager, true);
                break;
            case AUDIO_DEVICE_HANDSET:
                AudioManagerUtils.setHandsetOn(mAudioManager, true);
                break;
            case AUDIO_DEVICE_HEADSET:
                if (AudioManagerUtils.isWiredHeadsetOn(mAudioManager, false)) {
                    AudioManagerUtils.setHeadsetOn(mAudioManager, true);
                } else if (AudioManagerUtils.isUsbHeadsetConnected(mAudioManager, false)) {
                    AudioManagerUtils.setUsbHeadsetOn(mAudioManager, true);
                } else {
                    AudioManagerUtils.setEhsHeadsetOn(mAudioManager, true);
                }
                break;
            default:
                mAudioManager.setSpeakerphoneOn(false);
                break;
        }
    }

    public List<String> separatedString(String str) {
        String rep = "\\d+";
        Pattern p = Pattern.compile(rep);
        Matcher matcher = p.matcher(str);
        List<String> list = new ArrayList<>();
        while (matcher.find()) {
            list.add(matcher.group(0));
        }
        return list;
    }

    public void getSpeakerVoiceLoopPlayVolumeBefore() {
        String speakerString = Utils.processSystemCmd("tinymix 3");
        if (!TextUtils.isEmpty(speakerString)) {
            if (speakerString.length() > 24) {
                List<String> list = separatedString(speakerString);
                for (int i = 0; i < list.size(); i++) {
                    Log.d(TAG, "speaker list(i) =" + list.get(i));
                }
                if (list.size() > 2) {
                    if (!TextUtils.isEmpty(list.get(0))) {
                        mSpeakerLeftString = list.get(0);
                        Log.d(TAG, "mSpeakerLeftString =" + mSpeakerLeftString);
                    }
                    if (!TextUtils.isEmpty(list.get(1))) {
                        mSpeakerRightString = list.get(1);
                        Log.d(TAG, "mSpeakerRightString =" + mSpeakerRightString);
                    }

                }
            }
        }
        String dac1String = Utils.processSystemCmd("tinymix 10");
        if (!TextUtils.isEmpty(dac1String)) {
            if (dac1String.length() > 21) {
                List<String> list = separatedString(dac1String);
                for (int i = 0; i < list.size(); i++) {
                    Log.d(TAG, "dac1 list(i) =" + list.get(i));
                }
                if (list.size() > 3) {
                    if (!TextUtils.isEmpty(list.get(1))) {
                        mDac1LeftString = list.get(1);
                        Log.d(TAG, "mDac1LeftString =" + mDac1LeftString);
                    }
                    if (!TextUtils.isEmpty(list.get(2))) {
                        mDac1RightString = list.get(2);
                        Log.d(TAG, "mDac1RightString =" + mDac1RightString);
                    }

                }
            }
        }
    }

    public void getSpeakerVoiceLoopCaptureVolumeBefore() {
        Utils.processSystemCmd("tinymix 17");
        String captureString = Utils.processSystemCmd("tinymix 10");
        if (!TextUtils.isEmpty(captureString)) {
            if (captureString.length() > 19) {
                List<String> list = separatedString(captureString);
                for (int i = 0; i < list.size(); i++) {
                    Log.d(TAG, "capture list(i) =" + list.get(i));
                }
                if (list.size() > 3) {
                    if (!TextUtils.isEmpty(list.get(1))) {
                        mCaptureLeftString = list.get(1);
                        Log.d(TAG, "mCaptureLeftString =" + mCaptureLeftString);
                    }
                    if (!TextUtils.isEmpty(list.get(2))) {
                        mCaptureRightString = list.get(2);
                        Log.d(TAG, "mCaptureRightString =" + mCaptureRightString);
                    }
                }
            }
        }
    }

    public static void setSpeakerVoiceLoopPlayVolume() {
        Log.i(TAG, "setSpeakerVoiceLoopPlayVolume");
        String phoneBoard = PhoneModelUtils.getPhoneBoard();
        if (!TextUtils.isEmpty(phoneBoard)) {
            if (phoneBoard.equals(Constant.PHONE_MODEL_J7A)) {
                Utils.processSystemCmd("tinymix \"DAC1 Playback Volume\" 165\n" +
                        "tinymix \"Speaker Playback Volume\" 28\n");
            } else if (phoneBoard.equals(Constant.PHONE_MODEL_I56A)) {
                Utils.processSystemCmd("tinymix \"DAC1 Playback Volume\" 175\n" +
                        "tinymix \"Speaker Playback Volume\" 30\n");
            } else if (phoneBoard.equals(Constant.PHONE_MODEL_X7A)) {
                Utils.processSystemCmd("tinymix \"DAC1 Playback Volume\" 175\n" +
                        "tinymix \"Speaker Playback Volume\" 31\n");
            } else if (phoneBoard.equals(Constant.PHONE_MODEL_VS32)) {
                Utils.processSystemCmd("tinymix \"DAC1 Playback Volume\" 155\n" +
                        "tinymix \"Speaker Playback Volume\" 28\n");
            }
        }
    }

    public void setSpeakerVoiceLoopCaptureVolume() {
        Log.i(TAG, "setSpeakerVoiceLoopCaptureVolume");
        String phoneBoard = PhoneModelUtils.getPhoneBoard();
        if (!TextUtils.isEmpty(phoneBoard)) {
            if (phoneBoard.equals(Constant.PHONE_MODEL_J7A)) {
                Utils.processSystemCmd("tinymix \"ADC Capture Volume\" 56\n");
            } else if (phoneBoard.equals(Constant.PHONE_MODEL_I56A)) {
                Utils.processSystemCmd("tinymix \"ADC Capture Volume\" 68\n");
            } else if (phoneBoard.equals(Constant.PHONE_MODEL_X7A)) {
                Utils.processSystemCmd("tinymix \"ADC Capture Volume\" 77\n");
            } else if (phoneBoard.equals(Constant.PHONE_MODEL_VS32)) {
                Utils.processSystemCmd("tinymix \"ADC Capture Volume\" 70\n");
            }
        }
    }

    public void recoverPlayAndCaptureVolume() {
        Log.i(TAG, "mDac1LeftString =" + mDac1LeftString + ", mDac1RightString =" + mDac1RightString);
        if (!TextUtils.isEmpty(mDac1LeftString) && !TextUtils.isEmpty(mDac1RightString)) {
            String recoveDAC1 = Utils.processSystemCmd("tinymix \"DAC1 Playback Volume\" " + mDac1LeftString + " " + mDac1RightString);
            Log.i(TAG, "recoveDAC1 =" + recoveDAC1);
        }
        Log.i(TAG, "mSpeakerLeftString =" + mSpeakerLeftString + ", mSpeakerRightString =" + mSpeakerRightString);
        if (!TextUtils.isEmpty(mSpeakerLeftString) && !TextUtils.isEmpty(mSpeakerRightString)) {
            String recoveSpeaker = Utils.processSystemCmd("tinymix \"Speaker Playback Volume\" " + mSpeakerLeftString + " " + mSpeakerRightString);
            Log.i(TAG, "recoveSpeaker =" + recoveSpeaker);
        }
        Log.i(TAG, "mCaptureLeftString =" + mCaptureLeftString + ", mCaptureRightString =" + mCaptureRightString);
        if (!TextUtils.isEmpty(mCaptureLeftString) && !TextUtils.isEmpty(mCaptureRightString)) {
            String recoveADC = Utils.processSystemCmd("tinymix \"ADC Capture Volume\" " + mCaptureLeftString + " " + mCaptureRightString);
            Log.i(TAG, "recoveADC =" + recoveADC);
        }
    }

    public void stopPlay() {
        Log.i(TAG, "stopPlay");

        if (mMediaHandler != null) {
            Message msg = mMediaHandler.obtainMessage(MESSAGE_STOP_PLAY);
            mMediaHandler.sendMessage(msg);
        }
    }

    public void startVoiceLoop(int audioDevice) {
        Log.i(TAG, "startVoiceLoop: audioDevice=" + getAudioDeviceString(audioDevice));

        if (mMediaHandler != null) {
            Message msg = mMediaHandler.obtainMessage(MESSAGE_START_VOICE_LOOPBACK);
            msg.arg1 = audioDevice;
            mMediaHandler.sendMessage(msg);
        }
    }

    public void stopVoiceLoop() {
        Log.i(TAG, "stopVoiceLoop");

        if (mMediaHandler != null) {
            mMediaHandler.stopVoiceLoop();
            mMediaHandler.stopPlay();
        }
    }

    private void destroy() {
        Log.i(TAG, "destroy");

        setVoiceAudioDevice(AUDIO_DEVICE_NONE);
        recoverPlayAndCaptureVolume();

        if (mMediaHandler != null) {
            mMediaHandler.stopVoiceLoop();
            mMediaHandler.stopPlay();
            mMediaHandler.release();
            mMediaHandler.removeCallbacksAndMessages(null);
            mMediaHandler = null;
        }

        if (mMediaThread != null) {
            mMediaThread.quit();
            mMediaHandler = null;
        }

    }

    public String getAudioDeviceString(int audioDevice) {
        String value = "Unknown";

        switch (audioDevice) {
            case AUDIO_DEVICE_SPEAKER:
                value = "Speaker";
                break;
            case AUDIO_DEVICE_HANDSET:
                value = "Handset";
                break;
            case AUDIO_DEVICE_HEADSET:
                value = "Headset";
                break;
            case AUDIO_DEVICE_NONE:
                value = "None";
                break;
            default:
                break;
        }
        return value;
    }

    private class MediaHandler extends Handler {
        private static final int RATE_IN_HZ = 32000;//32000
        private ToneGenerator mToneGenerator = null;
        private Ringtone mRingTone = null;
        private int mRecordBuffer = 0;
        private int mTrackBuffer = 0;
        private AudioRecord mAudioRecord;
        private AudioTrack mAudioTrack;
        private int mAudioDevice;
        private boolean mStarted = false;
        private boolean mRecordStarted = false;
        private boolean mTrackStarted = false;
        private File mPCMFile;
        private Looper mLooper;

        public MediaHandler(Looper looper) {
            super(looper);
            mLooper = looper;

            mToneGenerator = new ToneGenerator(mToneUseSystemStream ? AudioManager.STREAM_SYSTEM : AudioManager.STREAM_VOICE_CALL, 100);

            final Uri defaultRingtoneUri = Uri.parse("file://" + Constant.RING_FILE_PATH);
            if (defaultRingtoneUri != null) {
                mRingTone = RingtoneManager.getRingtone(mContext, defaultRingtoneUri);
                mRingTone.setStreamType(AudioManager.STREAM_RING);
            }

        }

        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage: begin msg=" + msg);
            switch (msg.what) {
                case MESSAGE_START_VOICE_LOOPBACK:
                    startVoiceLoop(msg.arg1);
                    break;
                case MESSAGE_STOP_RECORDING:
                    stopRecording();
                    break;
                case MESSAGE_STOP_PLAY:
                    stopPlay();
                    break;
                default:
                    break;
            }
        }

        public void stopVoiceLoop() {
            Log.i(TAG, "stopVoiceLoop");

            mStarted = false;

            stopRecording();
            stopPlaying();
        }

        private void stopPlay() {
            Log.i(TAG, "stopPlay");

            if (mToneGenerator != null) {
                mToneGenerator.stopTone();
            }

            if (mRingTone != null) {
                mRingTone.stop();
            }

            mAudioDevice = AUDIO_DEVICE_NONE;

            setVoiceAudioDevice(AUDIO_DEVICE_NONE);
            mAudioManager.setMode(AudioManager.MODE_NORMAL);
        }

        private void release() {
            Log.i(TAG, "release");

            if (mToneGenerator != null) {
                mToneGenerator.release();
                mToneGenerator = null;
            }
            if (mRingTone != null) {
                mRingTone = null;
            }

            releaseRecorder();
            releasePlayer();
            mAudioManager.setMode(AudioManager.MODE_NORMAL);
            stopTimer();
        }

        private void startVoiceLoop(int audioDevice) {
            Log.i(TAG, "startVoiceLoop: audioDevice=" + getAudioDeviceString(audioDevice));

            mAudioDevice = audioDevice;

            mPCMFile = Utils.getFile("voice_loop_data");

            createRecorder();
            createPlayer();

            setVoiceAudioDevice(mAudioDevice);
            mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
            if (AUDIO_DEVICE_SPEAKER == audioDevice) {
                mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, 6, 0);
                //mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            } else {
                mAudioManager.setStreamVolume(AudioManager.STREAM_VOICE_CALL, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL), 0);
            }
            mStarted = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (mStarted) {
                        readAndPlayPcmFile(mPCMFile);
                    }
                }
            }).start();
        }

        private void createRecorder() {
            Log.i(TAG, "createRecorder");

            if (mAudioRecord == null) {
                mRecordBuffer = AudioRecord.getMinBufferSize(RATE_IN_HZ,
                        AudioFormat.CHANNEL_IN_STEREO,
                        AudioFormat.ENCODING_PCM_16BIT);

                mAudioRecord = new AudioRecord(MediaRecorder.AudioSource.VOICE_COMMUNICATION,
                        RATE_IN_HZ, AudioFormat.CHANNEL_IN_STEREO,
                        AudioFormat.ENCODING_PCM_16BIT,
                        mRecordBuffer * 10);
            }
        }

        private void startRecording() {
            Log.i(TAG, "startRecording =" + mAudioRecord);

            if (mAudioRecord != null) {
                mAudioRecord.startRecording();
                mRecordStarted = true;
            }
        }

        private void stopRecording() {
            Log.i(TAG, "stopRecording");

            if (mAudioRecord != null) {
                if (AudioRecord.RECORDSTATE_RECORDING == mAudioRecord.getRecordingState()) {
                    mAudioRecord.stop();
                }
                mRecordStarted = false;
            }
        }

        private void releaseRecorder() {
            Log.i(TAG, "releaseRecorder");

            if (mAudioRecord != null) {
                mAudioRecord.release();
                mAudioRecord = null;
            }
        }

        private void createPlayer() {
            Log.i(TAG, "createPlayer");
            if (mAudioTrack == null) {
                mTrackBuffer = AudioTrack.getMinBufferSize(RATE_IN_HZ,
                        AudioFormat.CHANNEL_OUT_STEREO,
                        AudioFormat.ENCODING_PCM_16BIT);

                mAudioTrack = new AudioTrack(AudioManager.STREAM_VOICE_CALL,
                        RATE_IN_HZ, AudioFormat.CHANNEL_OUT_STEREO,
                        AudioFormat.ENCODING_PCM_16BIT, mTrackBuffer,
                        AudioTrack.MODE_STREAM);
            }
        }

        private void startPlaying() {
            Log.i(TAG, "startPlaying =" + mAudioTrack);

            if (mAudioTrack != null) {
                mAudioTrack.play();
                mTrackStarted = true;
            }
        }

        private void stopPlaying() {
            Log.i(TAG, "stopPlaying");
            if (mAudioTrack != null) {
                if (AudioTrack.PLAYSTATE_PLAYING == mAudioTrack.getPlayState()) {
                    mAudioTrack.stop();
                }
                mTrackStarted = false;
            }
        }

        private void releasePlayer() {
            Log.i(TAG, "releasePlayer");

            if (mAudioTrack != null) {
                mAudioTrack.release();
                mAudioTrack = null;
            }
        }

        private void startTimer() {
            if (mTimerTask == null) {
                mTimerTask = new TimerTask() {
                    @Override
                    public void run() {
                        startPlaying();
                    }
                };
            }

            if (mTimer == null) {
                mTimer = new Timer();
            }
            if (mTimer != null && mTimerTask != null) {
                mTimer.schedule(mTimerTask, 1000);
            }
        }

        private void stopTimer() {
            Log.i(TAG, "stopTimer");
            if (mTimer != null) {
                mTimer.cancel();
                mTimer = null;
            }
            if (mTimerTask != null) {
                mTimerTask.cancel();
                mTimerTask = null;
            }
        }

        private int readAndPlayPcmFile(File file) {
            Log.i(TAG, "playPcmFile");

            if (file != null) {
                final byte[] buffer = new byte[mRecordBuffer];
                startRecording();
                Log.d(TAG, "readAndPlayPcmFile mTimer =" + mTimer + ", mTimerTask =" + mTimerTask);
                startTimer();

                FileOutputStream outputStream = null;
                int mAudioRecordSize = 0;
                int totalSize = 0;
                boolean updateSpeakerPlayVolume = false;
                boolean updateSpeakerCaptureVolume = false;

                while (mAudioRecord != null && mRecordStarted && (mAudioRecordSize = mAudioRecord.read(buffer, 0, mRecordBuffer)) != -1) {
                    if (!updateSpeakerCaptureVolume) {
                        updateSpeakerCaptureVolume = true;
                        if (AUDIO_DEVICE_SPEAKER == mAudioDevice) {
                            setSpeakerVoiceLoopCaptureVolume();
                        }
                    }
                    try {
                        if (outputStream == null) {
                            outputStream = new FileOutputStream(file.getAbsoluteFile());
                        }
                        //Log.i(TAG, "recordPcmFile size=" + size + " totalSize=" + totalSize);
                        outputStream.write(buffer);
                        outputStream.flush();
                        totalSize += mAudioRecordSize;

                        //mAudioRecordSize = mAudioRecord.read(buffer, 0, mRecordBuffer);
                        byte[] tmpBuffer = new byte[mAudioRecordSize];
                        tmpBuffer = buffer.clone();
                        if (AUDIO_DEVICE_SPEAKER == mAudioDevice) {
                            try {
                                Thread.sleep(20);//20ms OK
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        if (mAudioTrack != null) {
                            if (AudioTrack.PLAYSTATE_PLAYING == mAudioTrack.getPlayState()) {
                                if (!updateSpeakerPlayVolume) {
                                    updateSpeakerPlayVolume = true;
                                    if (AUDIO_DEVICE_SPEAKER == mAudioDevice) {
                                        setSpeakerVoiceLoopPlayVolume();
                                    }
                                }
                            }
                            if (tmpBuffer != null) {
                                if (tmpBuffer.length > 0) {
                                    mAudioTrack.write(tmpBuffer, 0, tmpBuffer.length);
                                }
                            }
                        }

                    } catch (Exception e) {
                        Log.e(TAG, "createFile pcm error =" + e);
                    }
                }
                try {
                    stopPlaying();
                    stopRecording();
                } catch (IllegalStateException ise) {
                    Log.e(TAG, "IllegalStateException ise =" + ise.toString());
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (Exception e) {
                        Log.e(TAG, "outputStream close =" + e.toString());
                    }
                }
                return 0;
            }
            return -1;
        }
    }
}
package org.pjsip.pjsua2.app;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ThemedSpinnerAdapter;

import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.app.dialog.HistoryDialog;
import org.pjsip.pjsua2.app.dialog.ImNotifyDialog;
import org.pjsip.pjsua2.app.dialog.MbcForbidCallDialog;
import org.pjsip.pjsua2.app.dialog.MsgNotifyDialog;
import org.pjsip.pjsua2.app.dialog.PstnDetailDialogHis;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.CallRecord;
import org.pjsip.pjsua2.app.model.Pstn;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class HistoryActivity extends BaseActivity implements Handler.Callback {
    public static Handler handler_;
    private final Handler handler = new Handler(this);
    private List<CallRecord> callrecordlist = new ArrayList<>();
    ArrayList<Map<String, String>> historyList;
    private ContentAdapter historyListAdapter;

    public ListView historyListView;
    private int historyListSelectedIdx = -1;

    private Integer current_callid = -1;

    private MediaPlayer mPlayer;
    private boolean isplay = false;
    private ImageButton main_ib_button;
    private SeekBar main_seekBar;
    private TextView mediaTimeView;
    private Mythred mythred;
    private String currentRecordPath;

    public int currentpage = 1;
    public int currentstate = -1;
    public int currentoperatetype = -1;

    public List<Pstn> pstnlist;
    //public static Map<String,String> pstn_name_KV = new HashMap<>();

    private MainActivity.MyCallWithState callws;

    private static boolean historyListLock = false;


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        if(GlobalVariable.viewmodel == 1) {
            setContentView(R.layout.activity_history);
        }
        else if(GlobalVariable.viewmodel == 2){
            setContentView(R.layout.activity_history_ct);
        }
        else if(GlobalVariable.viewmodel ==3){
            setContentView(R.layout.activity_history_gs);
        }
        handler_ = handler;

        historyList = new ArrayList<Map<String, String>>();

        String[] callin_from = { "state", "callingnumber","starttime","type","duration","truenumber","line" };
        int[] callin_to = { R.id.textHIS1, R.id.textHIS2, R.id.textHIS3, R.id.textHIS4,R.id.textHIS5,R.id.textHIS6,R.id.textHIS7 };


        historyListView = (ListView) findViewById(R.id.listViewHistory);
        historyListAdapter = new ContentAdapter(
                this, historyList,
                R.layout.history_list_item,
                callin_from, callin_to,historyListView);
        historyListView.setAdapter(historyListAdapter);
        historyListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            final View view,
                                            int position, long id)
                    {
                        try {
                            view.setSelected(true);
                            historyListSelectedIdx = position;
                            historyListAdapter.notifyDataSetChanged();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }
        );
        historyListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
//                            new  AlertDialog.Builder(mainActivityContext)
//                                    .setTitle("确认" )
//                                    .setMessage("确定吗？" )
//                                    .setPositiveButton("是" ,  null )
//                                    .setNegativeButton("否" , null)
//                                    .show();
                            return true;
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        return false;
                    }
                }
        );

        historyListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                //判断滚动是否停止
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    //判断界面上显示的最后一项item的position，是否等于item的总个数减1（item的position从0开始），如果是，说明滑动到了底部。
                    if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
                        /**
                         * 如果程序运行到这里，说明滑动到了底部，下面显示加载效果，通过线程加载数据
                         */
                        new Thread() {

                            public void run() {
                                try {
                                    currentpage++;
                                    HttpRequest request = new HttpRequest();
                                    String rs = request.postJson(
                                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getHistoryByUser",
                                            "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                                    "\"page\": \""+currentpage+"\", " +
                                                    "\"pageSize\": \"8\", " +
                                                    "\"pgmid\": \""+GlobalVariable.pgmid+"\", " +
                                                    "\"state\": \""+currentstate+"\", " +
                                                    "\"operatetype\": \""+currentoperatetype+"\", " +
                                                    "\"character\": \""+GlobalVariable.charactername+"\"}"
                                    );
                                    JSONObject rs_json = new JSONObject(rs);
                                    JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
                                    List<CallRecord> tempcallrecordlist = JSON.parseArray(rs_json_callrecordlist.toString(),CallRecord.class);
                                    //historyList.clear();

                                    //MediaPlayer durationplayer = new MediaPlayer();
                                    //ContentResolver contentResolver = getContentResolver();
                                    for(CallRecord record :tempcallrecordlist)
                                    {
                                        callrecordlist.add(record);
                                        //Cursor cursor = contentResolver.query(Uri.parse(record.recordpath), null, null,null, null);
                                        //long duration_long = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
                                        //String duration = GlobalVariable.ms_df.format(new Date(duration_long));
                                        String duration = getRingDuring(record.recordpath);
                                        if(duration == null)
                                        {
                                            duration = "00:00";
                                        }
                                        else{
                                            long ms = Long.parseLong(duration) - TimeZone.getDefault().getRawOffset();
                                            duration = GlobalVariable.hms_df.format(ms);
                                        }
                                        //String duration = "12:34";
                                        //"state", "callingnumber","starttime","type"
                                        if(record.operatetype == 1){
                                            //record.callingnumber = record.callingnumber.replaceFirst("00","");
                                            record.callingnumber = record.callingnumber.substring(2);
                                        };
                                        String line = "线路";
                                        line += GlobalVariable.ctNumberMap.get(record.callednumber)!=null?GlobalVariable.ctNumberMap.get(record.callednumber):"";

                                        historyList.add(
                                            putData(
                                                record.callingnumber,
                                                line,
                                                String.valueOf(record.operatetype),
                                                record.state.toString(),
                                                GlobalVariable.pstn_name_KV.get(record.callingnumber)==null?record.callingnumber:GlobalVariable.pstn_name_KV.get(record.callingnumber),
                                                record.starttime,
                                                record.type.toString(),
                                                record.callid,
                                                duration
                                            )
                                        );

                                    }
                                    //获得解析数据后更新页面(当数据较多时等待统一刷新很慢)
                                    Message m2 = Message.obtain(HistoryActivity.handler_,1001,null);
                                    m2.sendToTarget();

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }.start();

                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {
                // TODO Auto-generated method stub
            }
        });

        main_ib_button = (ImageButton) findViewById(R.id.main_ib_button);
        main_seekBar = (SeekBar) findViewById(R.id.main_seekBar);
        mediaTimeView = (TextView) findViewById(R.id.MediaTimeView);
        //给进度条设置监听(以次来实现快进功能)
        main_seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //首先获取seekbar拖动后的位置
                int progress=main_seekBar.getProgress();
                //跳转到某个位置播放
                if(mPlayer!=null)
                    mPlayer.seekTo(progress);

            }
        });

        //无导播模式显示拨出，其他均隐藏

        if(GlobalVariable.viewmodel == 1) {
            ImageButton imageButtonDail = findViewById(R.id.hisDail);
            ImageButton imageButtonTitle = findViewById(R.id.buttonICON2);
            if (GlobalVariable.charactername.equals("broadcaster")) {
                if (GlobalVariable.sipmodel == 3) {
                    imageButtonDail.setVisibility(View.GONE);
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
                //主播，单/多导播模式屏蔽
                if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){
                    imageButtonDail.setVisibility(View.GONE);
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
            } else {
                if (GlobalVariable.sipmodel == 0) {
                    imageButtonDail.setVisibility(View.GONE);
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
            }
        }
        else if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3){
            ImageButton imageButtonDail = findViewById(R.id.hisDail);
            //ImageButton imageButtonTitle = findViewById(R.id.ctTitleDail);
            if (GlobalVariable.charactername.equals("broadcaster")) {
                if (GlobalVariable.sipmodel == 3) {
                    imageButtonDail.setVisibility(View.GONE);
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
                }
            } else {
                if (GlobalVariable.sipmodel == 0) {
                    imageButtonDail.setVisibility(View.GONE);
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
                }
            }
        }
    }

    @Override
    protected void onStart() {
        if(GlobalVariable.isDL1129) {
            Button innercomBtn = findViewById(R.id.buttonMA6);
            innercomBtn.setVisibility(View.GONE);
        }
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.ct_title_home));
            //imgBtn1.setImageDrawable(getDrawable(R.drawable.ct_title_history));
            imgBtn2.setImageDrawable(getDrawable(R.drawable.ct_title_contacts));
            imgBtn3.setImageDrawable(getDrawable(R.drawable.ct_title_setting));
            imgBtn4.setImageDrawable(getDrawable(R.drawable.ct_title_dail));
        }
        super.onStart();

        currentstate = -1;

//        Button ImButton = findViewById(R.id.buttonMA6);
//        if(GlobalVariable.IMRedTitle)
//            ImButton.setTextColor(Color.RED);
//        else
//            ImButton.setTextColor(Color.WHITE);

        //每次界面从后台调起展示给用户时，都需要重新从数据库获取数据展示给用户
        //获得的记录为Json格式，赋予本地临时保存
        //是否分页查询，记录是否可在话机删除等功能待定
        new Thread(){
            @Override
            public void run() {
                try {
                    //处理姓名键值对
                    HttpRequest requestKV = new HttpRequest();
                    String rsKV = requestKV.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getPstnList",
                            "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                    "\"character\": \""+GlobalVariable.charactername+"\"," +
                                    "\"isexpert\": \"-1\"," +
                                    "\"iscontact\": \"0\"" +
                                    "}"
                    );
                    JSONObject rs_jsonKV = new JSONObject(rsKV);
                    JSONArray rs_json_contactslist = rs_jsonKV.getJSONArray("data");
                    pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

                    for(Pstn pstn :pstnlist){
                        if(pstn.name!=null&&pstn.name!="")
                        {
                            GlobalVariable.pstn_name_KV.put(pstn.pstn,pstn.name);
                        }
                    }

                    currentpage= 1;
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getHistoryByUser",
                            "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                    "\"state\": \"-1\", " +
                                    "\"operatetype\": \"-1\", " +
                                    "\"page\": \""+currentpage+"\", " +
                                    "\"pageSize\": \"8\", " +
                                    "\"pgmid\": \""+GlobalVariable.pgmid+"\", " +
                                    "\"character\": \""+GlobalVariable.charactername+"\"}"
                    );
                    JSONObject rs_json = new JSONObject(rs);
                    JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
                    callrecordlist = JSON.parseArray(rs_json_callrecordlist.toString(),CallRecord.class);
                    historyList.clear();

                    //MediaPlayer durationplayer = new MediaPlayer();
                    //ContentResolver contentResolver = getContentResolver();
                    for(CallRecord record :callrecordlist)
                    {
                        //Cursor cursor = contentResolver.query(Uri.parse(record.recordpath), null, null,null, null);
                        //long duration_long = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
                        //String duration = GlobalVariable.ms_df.format(new Date(duration_long));
                        String duration = getRingDuring(record.recordpath);
                        if(duration == null)
                        {
                            duration = "00:00";
                        }
                        else{
                            long ms = Long.parseLong(duration) - TimeZone.getDefault().getRawOffset();
                            duration = GlobalVariable.hms_df.format(ms);
                        }
                        //String duration = "12:34";
                        //"state", "callingnumber","starttime","type"
                        if(record.operatetype == 1){
                            //record.callingnumber = record.callingnumber.replaceFirst("00","");
                            record.callingnumber = record.callingnumber.substring(2);
                        };
                        String line = "线路";
                        line += GlobalVariable.ctNumberMap.get(record.callednumber)!=null?GlobalVariable.ctNumberMap.get(record.callednumber):"";

                        historyList.add(
                            putData(
                                record.callingnumber,
                                line,
                                String.valueOf(record.operatetype),
                                record.state.toString(),
                                GlobalVariable.pstn_name_KV.get(record.callingnumber)==null?record.callingnumber:GlobalVariable.pstn_name_KV.get(record.callingnumber),
                                record.starttime,
                                record.type.toString(),
                                record.callid,
                                duration
                            )
                        );

                    }
                    //获得解析数据后更新页面(当数据较多时等待统一刷新很慢)
                    Message m2 = Message.obtain(HistoryActivity.handler_,1001,null);
                    m2.sendToTarget();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                try {

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {

        if(msg.what == 1001)
        {
            //
            historyListLock = false;
            historyListAdapter.setDataList(historyList);
            //historyListAdapter.notifyDataSetChanged();
            updateViewByConfig();
        }
        if(msg.what == 1002)
        {
            String text = (String) msg.obj;
            mediaTimeView.setText(text);
        }
        else if(msg.what == 1005)
        {
            if(msg.obj!=null  && GlobalVariable.prm!=null){
                //2021-04-09提醒内容更改为自定义格式
                //OnInstantMessageParam prm = (OnInstantMessageParam) msg.obj;
//                OnInstantMessageParam prm = GlobalVariable.prm;
//                String msgContent = "";
//                String username = "";
//                String template = "";
//                int type = -1;
//                try {
//                    JSONObject msg_json = new JSONObject(prm.getMsgBody());
//                    msgContent = msg_json.get("msg").toString();
//                    username = msg_json.get("username").toString();
//                    template = msg_json.get("template").toString();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(this,msgContent,username,template).create();

                    ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(
                            this,
                            GlobalVariable.im_msg,
                            GlobalVariable.im_username,
                            GlobalVariable.im_template).create();
                    imNotifyDialog.show();
                    imNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                    imNotifyDialog.getWindow().setLayout(500, 300);

            }
        }
        else if(msg.what == 1010)
        {
            if(GlobalVariable.viewmodel == 1){
                TextView timeView = (TextView) findViewById(R.id.mainTime);
                if (timeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    timeView.setText(dateString.replace(" ", "         "));
                }
                //刷新状态图标
                ImageView mainState1 = (ImageView) findViewById(R.id.MainState1);
                ImageView mainState2 = (ImageView) findViewById(R.id.MainState2);
                ImageView mainState3 = (ImageView) findViewById(R.id.MainState3);

                Button ImButton = findViewById(R.id.buttonMA6);
                if(GlobalVariable.IMRedTitle)
                {
                    if(GlobalVariable.IMRedShowed)
                    {
                        ImButton.setTextColor(Color.WHITE);
                        GlobalVariable.IMRedShowed = false;
                    }
                    else
                    {
                        ImButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.IMRedShowed = true;
                    }
                }
                else{
                    ImButton.setTextColor(Color.WHITE);
                }

                /**有未接听电话首页图标闪烁**/
                Button PhoneButton = findViewById(R.id.buttonMA3);
                if(GlobalVariable.PhoneRedTitle){
                    if(GlobalVariable.PhoneRedShowed4){
                        PhoneButton.setTextColor(Color.WHITE);
                        GlobalVariable.PhoneRedShowed4 = false;
                    }else{
                        PhoneButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.PhoneRedShowed4 = true;
                    }
                }else{
                    PhoneButton.setTextColor(Color.WHITE);
                }

                if(GlobalVariable.callring)
                {
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.allowring));
                }
                else{
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.disablering));
                }

                if(GlobalVariable.whitelistmodel)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.whitelistmodel));
                }
                else if(GlobalVariable.allowblacklist)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.allowblack));
                }
                else{
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.disableblack));
                }

                if(!GlobalVariable.online)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall == 1)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.busy));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall==2) {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.linebusy));
                    mainState3.setColorFilter(Color.TRANSPARENT);
                }
                else {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.allowin));
                    mainState3.setColorFilter(Color.WHITE);
                }

                TextView mainChannel = findViewById(R.id.mainChannel);
                TextView mainSipID = findViewById(R.id.mainSipID);
                mainChannel.setText(GlobalVariable.channelname);
                mainSipID.setText(GlobalVariable.user_sipid);
            }
            else if(GlobalVariable.viewmodel == 2) {

                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);

                Typeface typeface_fitcan = Typeface.createFromAsset(this.getAssets(), "fonts/BGOTHM.TTF");
                ct_title_username.setTypeface(typeface_fitcan);
                ct_title_name.setTypeface(typeface_fitcan);
                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_pdr));
                }
            }
            else if(GlobalVariable.viewmodel == 3){
                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);
                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_pdr));
                }
            }

        }
        else if(msg.what == 1011) {
            if(GlobalVariable.viewmodel == 1){
                ImageView titleImage = findViewById(R.id.titleImage);
                if(titleImage!=null)
                {
                    if(String.valueOf(msg.obj).equals("success")){
                        titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo));
                    }
                    else{
                        titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo_red));
                    }
                }
            }
            else if(GlobalVariable.viewmodel == 2){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(msg.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }
            else if(GlobalVariable.viewmodel == 3){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(msg.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }

        }
        //处理电话号码后期保存的问题
        else if(msg.what == 1012){

            if(callws!=null){
                new Thread() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void run() {
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoHis",
                                "{\"type\":\"post pstn info\", " +
                                        "\"pstn\":\"" + callws.pstn + "\", " +
                                        "\"callid\":\"" + callws.uid + "\", " +
                                        "\"name\":\"" + ((callws.name==null||callws.name.equals(""))?"":callws.name) + "\", " +
                                        "\"iswhitelist\":\"" + (callws.iswhitelist==null?0:callws.iswhitelist) + "\", " +
                                        "\"isblacklist\":\"" + (callws.isblacklist==null?0:callws.isblacklist) + "\", " +
                                        "\"iscontact\":\"" + (callws.iscontact==null?0:callws.iscontact) + "\", " +
                                        "\"isexpert\":\"" + (callws.isexpert==null?0:callws.isexpert) + "\"" +
                                        "}"
                        );
                        //更新数据后发送黑白名单变更消息给纬视部分处理
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    HttpRequest request = new HttpRequest();
                                    String rs = request.postJson(
                                            GlobalVariable.SEHTTPServer + "/listchange",
                                            "{\"code\":" + 200 + ","
                                                    +"\"operate\": \""+"edit"+ "\","
                                                    +"\"phonenumber\": \""+ callws.pstn + "\","
                                                    +"\"type\": \"all\","
                                                    +"\"iswhitelist\": "+ (callws.iswhitelist==null?0:callws.iswhitelist)+ ","
                                                    +"\"isblacklist\": "+ (callws.isblacklist==null?0:callws.isblacklist)
                                                    + "}"
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();


                        //需要重新加载界面数据
                        if(callws!=null)
                        {
                            currentstate = -1;
                            //每次界面从后台调起展示给用户时，都需要重新从数据库获取数据展示给用户
                            //获得的记录为Json格式，赋予本地临时保存
                            //是否分页查询，记录是否可在话机删除等功能待定
                            new Thread(){
                                @Override
                                public void run() {
                                    try {
                                        //处理姓名键值对
                                        HttpRequest requestKV = new HttpRequest();
                                        String rsKV = requestKV.postJson(
                                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getPstnList",
                                                "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                                        "\"character\": \""+GlobalVariable.charactername+"\"," +
                                                        "\"isexpert\": \"-1\"," +
                                                        "\"iscontact\": \"0\"" +
                                                        "}"
                                        );
                                        JSONObject rs_jsonKV = new JSONObject(rsKV);
                                        JSONArray rs_json_contactslist = rs_jsonKV.getJSONArray("data");
                                        pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);
                                        for(Pstn pstn :pstnlist){
                                            if(pstn.name!=null&&pstn.name!="")
                                            {
                                                GlobalVariable.pstn_name_KV.put(pstn.pstn,pstn.name);
                                            }
                                        }
                                        currentpage= 1;
                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getHistoryByUser",
                                                "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                                        "\"state\": \"-1\", " +
                                                        "\"operatetype\": \"-1\", " +
                                                        "\"page\": \""+currentpage+"\", " +
                                                        "\"pageSize\": \"8\", " +
                                                        "\"pgmid\": \""+GlobalVariable.pgmid+"\", " +
                                                        "\"character\": \""+GlobalVariable.charactername+"\"}"
                                        );
                                        JSONObject rs_json = new JSONObject(rs);
                                        JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
                                        callrecordlist = JSON.parseArray(rs_json_callrecordlist.toString(),CallRecord.class);
                                        historyList.clear();

                                        for(CallRecord record :callrecordlist)
                                        {
                                            String duration = getRingDuring(record.recordpath);
                                            if(duration == null){duration = "00:00";}
                                            else{
                                                long ms = Long.parseLong(duration) - TimeZone.getDefault().getRawOffset();
                                                duration = GlobalVariable.hms_df.format(ms);
                                            }

                                            if(record.operatetype == 1){
                                                //record.callingnumber = record.callingnumber.replaceFirst("00","");
                                                record.callingnumber = record.callingnumber.substring(2);
                                            };
                                            String line = "线路";
                                            line += GlobalVariable.ctNumberMap.get(record.callednumber)!=null?GlobalVariable.ctNumberMap.get(record.callednumber):"";


                                            historyList.add(
                                                    putData(
                                                            record.callingnumber,
                                                            line,
                                                            String.valueOf(record.operatetype),
                                                            record.state.toString(),
                                                            GlobalVariable.pstn_name_KV.get(record.callingnumber)==null?record.callingnumber:GlobalVariable.pstn_name_KV.get(record.callingnumber),
                                                            record.starttime,
                                                            record.type.toString(),
                                                            record.callid,
                                                            duration
                                                    )
                                            );
                                        }
                                        //获得解析数据后更新页面(当数据较多时等待统一刷新很慢)
                                        Message m2 = Message.obtain(HistoryActivity.handler_,1001,null);
                                        m2.sendToTarget();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }
                    }
                }.start();
            }
        }
        else if(msg.what == 1013){
            PstnDetailDialogHis pstnDetailDialogHis = new PstnDetailDialogHis.Builder(HistoryActivity.this,callws).create();
            pstnDetailDialogHis.show();
            if(GlobalVariable.viewmodel == 1)
                pstnDetailDialogHis.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                pstnDetailDialogHis.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                pstnDetailDialogHis.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            pstnDetailDialogHis.getWindow().setLayout(700, 550);
        }
        else if(msg.what == 1014){
            String msg_content = String.valueOf(msg.obj);
            MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"系统状态已更改\n"+msg_content+"更改").create();
            msgNotifyDialog.show();
            if(GlobalVariable.viewmodel == 1)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            msgNotifyDialog.getWindow().setLayout(500, 400);

            final Timer t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    msgNotifyDialog.dismiss();
                    t.cancel();
                }
            }, 2000);
        }
        else if(msg.what == 1015){
            showMainActivity(null);
        }
        else if(msg.what == 2000){
            //挂机逻辑处理
            showMainActivity(null);
        }
        else if(msg.what == 2001){
            //摘机逻辑处理
            switchPagePause();
            ImageButton imgBtn = findViewById(R.id.hisDail);
            if(imgBtn.isEnabled()) {
                HistoryDail(null);
            }
        }
        return true;
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler_ = null;
    }

    public void updateViewByConfig() {

    }
    //分类获取callrecordlist
    //state 类型
    //  未接        //0   默认状态
    //  已接        //1   导播或者主播接听电话会置这个状态
    //  挂断        //2   导播或者主播将电话挂断会置这个状态
    //  语音留言     //3
    // 黑名单被拒绝   //4
    //  拨出        //10  初始状态也是结束状态
    public void refreshCallRecordList(View view)
    {
        if(historyListLock){
            return;
        }
        historyListLock = true;
        historyListView.clearChoices();
        historyListAdapter.clearDataList();

        Integer state = Integer.parseInt(view.getTag().toString());
        if(GlobalVariable.viewmodel == 1) {
            Button hisType1 = findViewById(R.id.HisType1);
            Button hisType2 = findViewById(R.id.HisType2);
            Button hisType3 = findViewById(R.id.HisType3);
            Button hisType4 = findViewById(R.id.HisType4);
            Button hisType5 = findViewById(R.id.HisType5);

            if(state == -1)
            {
                currentoperatetype = -1;
                hisType1.setBackgroundResource(R.drawable.button_selector_title_thin);
                hisType2.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType3.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType4.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType5.setBackgroundResource(R.drawable.button_selector_transparent);
            }
            else if(state == 0)
            {
                currentoperatetype = 0;
                hisType1.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType2.setBackgroundResource(R.drawable.button_selector_title_thin);
                hisType3.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType4.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType5.setBackgroundResource(R.drawable.button_selector_transparent);
            }
            else if(state == 1)
            {
                currentoperatetype = 0;
                hisType1.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType2.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType3.setBackgroundResource(R.drawable.button_selector_title_thin);
                hisType4.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType5.setBackgroundResource(R.drawable.button_selector_transparent);
            }
            else if(state == 2)
            {
                currentoperatetype = -0;
                hisType1.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType2.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType3.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType4.setBackgroundResource(R.drawable.button_selector_title_thin);
                hisType5.setBackgroundResource(R.drawable.button_selector_transparent);
            }
            else if(state == 10)
            {
                state = -1;
                currentoperatetype = 1;
                hisType1.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType2.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType3.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType4.setBackgroundResource(R.drawable.button_selector_transparent);
                hisType5.setBackgroundResource(R.drawable.button_selector_title_thin);
            }
        }
        else if(GlobalVariable.viewmodel == 2){
            if(state == -1){currentoperatetype = -1;}
            else if(state == 0){currentoperatetype = 0;}
            else if(state == 1){currentoperatetype = 0;}
            else if(state == 2){currentoperatetype = -0;}
            else if(state == 10){state = -1;currentoperatetype = 1;}
        }
        else if(GlobalVariable.viewmodel == 3){
            if(state == -1){currentoperatetype = -1;}
            else if(state == 0){currentoperatetype = 0;}
            else if(state == 1){currentoperatetype = 0;}
            else if(state == 2){currentoperatetype = -0;}
            else if(state == 10){state = -1;currentoperatetype = 1;}
        }
        currentstate = state;
        currentpage = 1;
        new Thread(){
            @Override
            public void run() {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getHistoryByUser",
                            "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                    "\"page\": \"1\", " +
                                    "\"pageSize\": \"8\", " +
                                    "\"pgmid\": \""+GlobalVariable.pgmid+"\", " +
                                    "\"state\": \""+currentstate+"\", " +
                                    "\"operatetype\": \""+currentoperatetype+"\", " +
                                    "\"character\": \""+GlobalVariable.charactername+"\"}"
                    );
                    JSONObject rs_json = new JSONObject(rs);
                    JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
                    callrecordlist = JSON.parseArray(rs_json_callrecordlist.toString(),CallRecord.class);
                    historyList.clear();

                    //MediaPlayer durationplayer = new MediaPlayer();
                    ContentResolver contentResolver = getContentResolver();
                    for(CallRecord record :callrecordlist)
                    {
//                        Cursor cursor = contentResolver.query(Uri.parse(record.recordpath), null, null,null, null);
//                        long duration_long = cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));
//                        String duration = GlobalVariable.ms_df.format(new Date(duration_long));
                        String duration = getRingDuring(record.recordpath);
                        if(duration == null)
                        {
                            duration = "00:00";
                        }
                        else{
                            long ms = Long.parseLong(duration) - TimeZone.getDefault().getRawOffset();
                            duration = GlobalVariable.hms_df.format(ms);
                        }
                        //"state", "callingnumber","starttime","type"



                        if(record.operatetype == 1){
                            //record.callingnumber = record.callingnumber.replaceFirst("00","");
                            record.callingnumber = record.callingnumber.substring(2);
                        };

                        String line = "线路";
                        line += GlobalVariable.ctNumberMap.get(record.callednumber)!=null?GlobalVariable.ctNumberMap.get(record.callednumber):"";


                        historyList.add(
                            putData(
                                record.callingnumber,   //如果是呼出的话去两个0
                                line,
                                String.valueOf(record.operatetype),
                                record.state.toString(),
                                GlobalVariable.pstn_name_KV.get(record.callingnumber)==null?record.callingnumber:GlobalVariable.pstn_name_KV.get(record.callingnumber),
                                record.starttime,
                                record.type.toString(),
                                record.callid,duration
                            )
                        );

                        //设置一个临时的MediaPlayer

                    }
                    //获得解析数据后更新页面
                    Message m2 = Message.obtain(HistoryActivity.handler_,1001,null);
                    m2.sendToTarget();
                } catch (Exception e) {
                    //即使执行失败也解除互斥锁
                    Message m2 = Message.obtain(HistoryActivity.handler_,1001,null);
                    m2.sendToTarget();
                    e.printStackTrace();
                }
            }
        }.start();
    }


    //"state", "callingnumber","starttime","type"
    private HashMap<String, String> putData(String truenumber,String line,String operatetype,String state, String callingnumber, String starttime, String type,String callid,String duration)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("truenumber", truenumber);
        item.put("line", line);
        item.put("operatetype", operatetype);
        item.put("state", state);
        item.put("callingnumber", callingnumber);
        item.put("starttime", starttime);
        item.put("type", type);
        item.put("callid",callid);
        item.put("duration",duration);
        return item;
    }

    private void switchPagePause(){
//        if(mPlayer.isPlaying()){
//            isplay=false;
//            mPlayer.pause();
//            //改变图标（实现播放）
//            main_ib_button.setImageResource(android.R.drawable.ic_media_play);
//            //当停止播放时线程也停止了(这样也可以减少占用的内存)
//            mythred=null;
//        }


        if(mPlayer!=null && mPlayer.isPlaying()) {
            playOrPause(null);
        }


    }

    public void showDailActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleDail);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_dail_disable));
        }
        switchPagePause();
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, DailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showIntercomActivity(View view)
    {
        switchPagePause();
        Intent intent = new Intent(this, IntercomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        //setContentView(layoutIntercom);
    }

    public void showMainActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_home_disabled));
        }
        switchPagePause();
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showHistoryActivity(View view)
    {
        switchPagePause();
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showContactsActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_contacts_disable));
        }
        switchPagePause();
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showCommunicateActivity(View view)
    {
        switchPagePause();
        Intent intent = new Intent(this, CommunicateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showSettingActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_setting_disable));
        }
        switchPagePause();
        Intent intent = new Intent(this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void playOrPause(View view)
    {
        if(currentRecordPath!=null&&!currentRecordPath.equals("")) {
            setMediaPlayer(currentRecordPath);
        }
    }
    public void changeMediaSpeed(View view)
    {
        //先暂停，如果是暂停或者是未开始状态，不处理

        boolean isPlaying = false;
        if(mPlayer!=null && mPlayer.isPlaying()){
            isPlaying = true;
            isplay=false;
            mPlayer.pause();
            //改变图标（实现播放）
            main_ib_button.setImageResource(android.R.drawable.ic_media_play);
            //当停止播放时线程也停止了(这样也可以减少占用的内存)
            mythred=null;
        }

        Button button = (Button) view;
        if(button.getText().equals("1X"))
        {
             button.setText("1.5X");
             GlobalVariable.mplayerSpeed = 1.5F;
        }
        else if(button.getText().equals("1.5X"))
        {
            button.setText("2X");
            GlobalVariable.mplayerSpeed = 2F;
        }
        else if(button.getText().equals("2X"))
        {
            button.setText("0.5X");
            GlobalVariable.mplayerSpeed = 0.5F;
        }
        else if(button.getText().equals("0.5X"))
        {
            button.setText("1X");
            GlobalVariable.mplayerSpeed = 1F;
        }

        //后播放，如果原来没有播放也不播放
        if(mPlayer!=null && isPlaying){
            isplay=true;
            mPlayer.start();
            mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(GlobalVariable.mplayerSpeed));
            //改变图标（实现暂停）
            main_ib_button.setImageResource(android.R.drawable.ic_media_pause);
            mythred = new Mythred();
            mythred.start();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setMediaPlayer(String recordUri)
    {
        if(mPlayer==null) {
            isplay = true;
            mPlayer = new MediaPlayer();
            //设置音频流的类型
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            //设置音源
            try {
                //这是你电脑中tomcat中webapps中的ROOT里面的音乐
                mPlayer.setDataSource(this, Uri.parse(recordUri));
                //异步准备
                mPlayer.prepareAsync();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mPlayer.start();
                    mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(GlobalVariable.mplayerSpeed));
                    //改变图标（实现暂停）
                    main_ib_button.setImageResource(android.R.drawable.ic_media_pause);
                    //获得音乐总时长
                    int lengthoftime = mPlayer.getDuration();
                    main_seekBar.setMax(lengthoftime);
                    //开启线程\
                    mythred = new Mythred();
                    mythred.start();
                    mPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                        @Override
                        public void onBufferingUpdate(MediaPlayer mp, int percent) {
                            //得到缓冲值
                            int secendProssed= mPlayer.getDuration()/100*percent;
                            //设置第二进度
                            main_seekBar.setSecondaryProgress(secendProssed);

                        }
                    });


                }
            });

//            if (mPlayer == null) {
//                return;
//            }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                // API 23 （6.0）以上 ，通过设置Speed改变音乐的播放速率
//                if (mPlayer.isPlaying()) {
//                    // 判断是否正在播放，未播放时，要在设置Speed后，暂停音乐播放
//                    mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(GlobalVariable.mplayerSpeed));
//                } else {
//                    mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(GlobalVariable.mplayerSpeed));
//                    mPlayer.pause();
//                }
//            } else {
//                // 在Android6.0以前，需要另想办法处理，后续查到好的方法再补充
//            }
        }
        else if(mPlayer.isPlaying()){
            isplay=false;
            mPlayer.pause();
            //改变图标（实现播放）
            main_ib_button.setImageResource(android.R.drawable.ic_media_play);
            //当停止播放时线程也停止了(这样也可以减少占用的内存)
            mythred=null;

        }else {
            isplay=true;
            mPlayer.start();
            mPlayer.setPlaybackParams(mPlayer.getPlaybackParams().setSpeed(GlobalVariable.mplayerSpeed));
            //改变图标（实现暂停）
            main_ib_button.setImageResource(android.R.drawable.ic_media_pause);
            mythred = new Mythred();
            mythred.start();
        }
    }

    class Mythred extends Thread{
        @Override
        public void run() {
            super.run();
            boolean keep = true;
            while(main_seekBar.getProgress()<=main_seekBar.getMax()&&keep){
                //设置进度条的进度
                //得到当前音乐的播放位置
                if(mPlayer!=null&&main_seekBar!=null) {
                    int currentPosition = mPlayer.getCurrentPosition();
                    //Log.i("test", "currentPosition" + currentPosition);
//                    int musicTime = currentPosition / 1000;
//                    String  show1 = musicTime / 60 + ":" + musicTime % 60;
//
//                    musicTime = mPlayer.getDuration() / 1000;
//                    String  show2 = musicTime / 60 + ":" + musicTime % 60;

                    SimpleDateFormat hsdf = new SimpleDateFormat("HH:mm:ss");
                    Long ms_current = Long.parseLong(String.valueOf(currentPosition)) - TimeZone.getDefault().getRawOffset();
                    Long ms_total = Long.parseLong(String.valueOf(mPlayer.getDuration())) - TimeZone.getDefault().getRawOffset();
                    Date currentDate = new Date(currentPosition);
                    Date totalDate = new Date(mPlayer.getDuration());
                    if(HistoryActivity.handler_!=null) {
                        Message m2 = Message.obtain(HistoryActivity.handler_, 1002, hsdf.format(ms_current) + "/" + hsdf.format(ms_total));
                        m2.sendToTarget();
                    }
                    else{
                        keep = false;
                    }

                    main_seekBar.setProgress(currentPosition);
                }
                //让进度条每一秒向前移动
                SystemClock.sleep(1000);
                if (!isplay){
                    break;
                }
            }

        }
    }

    private void dlgHistoryDetail(CallRecord record)
    {
        LayoutInflater li = LayoutInflater.from(HistoryActivity.this);
        View detailview = li.inflate(R.layout.dlg_history_detail, null);

        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setView(detailview);

        TextView textPstnmark = (TextView)detailview.findViewById(R.id.textHISPstnmark);
        TextView textContent = (TextView)detailview.findViewById(R.id.textHISContent);
        TextView textPdrmark = (TextView)detailview.findViewById(R.id.textHISPdrmark);
        TextView textMbcmark = (TextView)detailview.findViewById(R.id.textHISMbcmark);

        textPstnmark.setText(record.pstnmark==null?"":record.pstnmark);
        textContent.setText(record.content==null?"":record.content);
        textPdrmark.setText(record.pdrmark==null?"":record.pdrmark);
        textMbcmark.setText(record.mbcmark==null?"":record.mbcmark);

        adb.setTitle("详细信息");
        adb.setCancelable(false);
        adb.setPositiveButton("确认",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id)
                    {

                    }
                }
        );
        adb.setNegativeButton("取消",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                }
        );

        AlertDialog ad = adb.create();
        //ad.getButton(AlertDialog.BUTTON_POSITIVE).setVisibility(View.GONE);
        ad.show();
    }

    public void HistoryPstnAdd(View view)//需要在通话记录中，将用户保存到通讯录或者黑白名单
    {
        try{
            if(historyListSelectedIdx!=-1){
                CallRecord callRecord = callrecordlist.get(historyListSelectedIdx);
                if(callRecord!=null)
                {
                    //还是要单开一个线程找pstn对应的数据库信息

                    callws = new MainActivity.MyCallWithState();
                    callws.pstn = callRecord.callingnumber;
                    callws.uid = callRecord.callid;
                    new Thread() {
                        @Override
                        public void run() {
                            getCallRecordInfo(callws, callws.pstn, callws.uid);
                            Message m2 = Message.obtain(HistoryActivity.handler_,1013,null);
                            m2.sendToTarget();
                        }
                    }.start();
                    //callws.name = GlobalVariable.pstn_name_KV.get(callRecord.callingnumber)==null?"":GlobalVariable.pstn_name_KV.get(callRecord.callingnumber);

                }

            }
        }catch(Exception e){}

    }
    public void HistoryPstnDetail(View view){
        try {
            if (historyListSelectedIdx != -1) {
                CallRecord record = callrecordlist.get(historyListSelectedIdx);
                if (record != null) {
                    //旧界面弃用，采用新的界面
                    //dlgHistoryDetail(record);
                    HistoryDialog historyDialog = new HistoryDialog.Builder(this, record).create();
                    historyDialog.show();
                    if (GlobalVariable.viewmodel == 1)
                        historyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                    else if (GlobalVariable.viewmodel == 2)
                        historyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
                    else if (GlobalVariable.viewmodel == 3)
                        historyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
                    historyDialog.getWindow().setLayout(1000, 600);
                }
            }
        }catch (Exception e){}
    }


    public void getCallRecordInfo(MainActivity.MyCallWithState callWithState, String finalPstn, String uid){
        HttpRequest request = new HttpRequest();
        String rs = request.postJson(
                "Http://"+GlobalVariable.SIPSERVER+":8080/smart/contactWithSipServer/getPstnByPstn",
                "{\"type\":\"post pstn info\", " +
                        "\"pstn\":\""+ finalPstn +"\", " +
                        "\"callid\":\""+ uid +"\"}"
        );

        try {
            JSONObject rs_json = new JSONObject(rs);
            JSONArray rs_jsonarray = rs_json.getJSONObject("data").getJSONArray("pstns");
            if(rs_jsonarray.length()>0)
            {
                JSONObject rs_json_pstn = rs_jsonarray.getJSONObject(0);
                callWithState.pstn = rs_json_pstn.get("pstn")==null?"":rs_json_pstn.get("pstn").toString();
                callWithState.name = rs_json_pstn.get("name")==null?"":rs_json_pstn.get("name").toString();
                callWithState.age =  rs_json_pstn.get("age")==null? 0:(Integer)rs_json_pstn.get("age");
                callWithState.male = rs_json_pstn.get("male")==null? 0:(Integer) rs_json_pstn.get("male");
                callWithState.from = rs_json_pstn.get("frm")==null?"":rs_json_pstn.get("frm").toString();
                callWithState.iswhitelist = String.valueOf(rs_json_pstn.get("iswhitelist")).equals("1")?1:0;
                callWithState.isblacklist = String.valueOf(rs_json_pstn.get("isblacklist")).equals("1")?1:0;
                callWithState.iscontact = String.valueOf(rs_json_pstn.get("iscontact")).equals("1")?1:0;
                callWithState.isexpert = String.valueOf(rs_json_pstn.get("isexpert")).equals("1")?1:0;
            }


            JSONObject rs_json_callrecord = rs_json.getJSONObject("data").getJSONObject("callrecord");
            if(rs_json_callrecord!=null)
            {
                callWithState.content = rs_json_callrecord.get("content")==null?"":rs_json_callrecord.get("content").toString();
                callWithState.pstnmark = rs_json_callrecord.get("pstnmark")==null?"":rs_json_callrecord.get("pstnmark").toString();
                callWithState.pdrmark = rs_json_callrecord.get("pdrmark")==null?"":rs_json_callrecord.get("pdrmark").toString();
                callWithState.mbcmark = rs_json_callrecord.get("mbcmark")==null?"":rs_json_callrecord.get("mbcmark").toString();
            }
        } catch (Exception e) {
            LogClient.generate("【CallRecord获取转换错误】"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void HistoryDail(View view)
    {
        if(GlobalVariable.charactername.equals("broadcaster")&&(GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2 || GlobalVariable.sipmodel == 3))
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(HistoryActivity.this,"请在导播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }
        if(GlobalVariable.charactername.equals("director")&&GlobalVariable.sipmodel == 0)
        {
            MbcForbidCallDialog mbcForbidCallDialog = new MbcForbidCallDialog.Builder(HistoryActivity.this,"请在主播端外呼电话").create();
            mbcForbidCallDialog.show();
            if(GlobalVariable.viewmodel == 1)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                mbcForbidCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            mbcForbidCallDialog.getWindow().setLayout(500, 400);
            return;
        }
        try {
            if (historyListSelectedIdx != -1) {
                CallRecord callRecord = callrecordlist.get(historyListSelectedIdx);
                if (callRecord != null) {
                    if (GlobalVariable.viewmodel == 1) {
                        GlobalVariable.setdailvalue = true;
                        GlobalVariable.dailvalue = callRecord.callingnumber;

                        if (DailActivity.handler_ != null) {
                            Message m2 = Message.obtain(DailActivity.handler_, 1002, null);
                            m2.sendToTarget();
                        }

                        Intent intent = new Intent(this, DailActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    } else if (GlobalVariable.viewmodel == 2) {
                        if (MainActivity.handler_ != null) {
                            Message m2 = Message.obtain(MainActivity.handler_, 1020, callRecord.callingnumber);
                            m2.sendToTarget();
                        }

                        Intent intent = new Intent(this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    } else if (GlobalVariable.viewmodel == 3) {
                        if (MainActivity.handler_ != null) {
                            Message m2 = Message.obtain(MainActivity.handler_, 1020, callRecord.callingnumber);
                            m2.sendToTarget();
                        }

                        Intent intent = new Intent(this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }
                }
            }
        }catch (Exception e){}

    }

    public void hisShowHistoryDetail(View view)
    {
        CallRecord record = null;
        Integer position = (Integer) view.getTag();
        if(callrecordlist!=null)
        {
            try {
                record = callrecordlist.get(position);
            }catch (Exception e){

            }
        }
        if(record!=null)
        {
            //旧界面弃用，采用新的界面
            //dlgHistoryDetail(record);
            HistoryDialog historyDialog = new HistoryDialog.Builder(this,record).create();

            historyDialog.show();
            if(GlobalVariable.viewmodel == 1)
                historyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                historyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                historyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            historyDialog.getWindow().setLayout(1000, 600);
        }

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void playHistoryRecord(View view)
    {
        CallRecord record = null;
        Integer position = (Integer) view.getTag();
//        historyListView.setSelection(position);
//        historyListSelectedIdx = position;
        historyListView.setItemChecked(position, true);
        historyListSelectedIdx = position;
        historyListAdapter.notifyDataSetChanged();
        if(callrecordlist!=null)
        {
            try {
                record = callrecordlist.get(position);
            }catch (Exception e){

            }
        }
        if(record!=null)
        {
            currentRecordPath = record.recordpath;
            //先进行状态清空再重新进行初始化
            isplay=false;
            if(mPlayer!=null) {
                mPlayer.pause();
            }
            mPlayer = null;

            //改变图标（实现播放）
            main_ib_button.setImageResource(android.R.drawable.ic_media_play);
            //当停止播放时线程也停止了(这样也可以减少占用的内存)
            mythred=null;
            setMediaPlayer(record.recordpath);
        }
    }

    public void muteCall(View view) {

    }

    public static String getRingDuring(String mUri){
        String duration=null;
        android.media.MediaMetadataRetriever mmr = new android.media.MediaMetadataRetriever();
        try { if (mUri != null) { HashMap<String, String> headers=null;
            if (headers == null) { headers = new HashMap<String, String>();
                headers.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-CN; MW-KW-001 Build/JRO03C) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 UCBrowser/1.0.0.001 U4/0.8.0 Mobile Safari/533.1"); }
            mmr.setDataSource(mUri, headers); }
            duration = mmr.extractMetadata(android.media.MediaMetadataRetriever.METADATA_KEY_DURATION); }
        catch (Exception ex) { }
        finally { mmr.release(); }
        Log.e("ryan","duration "+duration);
        return duration;
    }
}

@RequiresApi(api = Build.VERSION_CODES.M)
class ContentAdapter extends BaseAdapter implements Filterable, ThemedSpinnerAdapter {
    private final LayoutInflater mInflater;

    private int[] mTo;
    private String[] mFrom;
    private android.widget.SimpleAdapter.ViewBinder mViewBinder;

    private List<? extends Map<String, ?>> mData;

    private int mResource;
    private int mDropDownResource;

    /** Layout inflater used for {@link #getDropDownView(int, View, ViewGroup)}. */
    private LayoutInflater mDropDownInflater;

    private SimpleFilter mFilter;
    private ArrayList<Map<String, ?>> mUnfilteredData;

    private ListView mListView;


    public void setDataList(ArrayList<? extends Map<String, ?>> list) {
        if (list != null) {
            mData = (ArrayList<? extends Map<String, ?>>) list.clone();
            notifyDataSetChanged();
        }
    }

    public void clearDataList() {
        if (mData != null) {
            mData.clear();
        }
        notifyDataSetChanged();
    }


    /**
     * Constructor
     *
     * @param context The context where the View associated with this SimpleAdapter is running
     * @param data A List of Maps. Each entry in the List corresponds to one row in the list. The
     *        Maps contain the data for each row, and should include all the entries specified in
     *        "from"
     * @param resource Resource identifier of a view layout that defines the views for this list
     *        item. The layout file should include at least those named views defined in "to"
     * @param from A list of column names that will be added to the Map associated with each
     *        item.
     * @param to The views that should display column in the "from" parameter. These should all be
     *        TextViews. The first N views in this list are given the values of the first N columns
     *        in the from parameter.
     */
    public ContentAdapter(Context context, List<? extends Map<String, ?>> data,
                         @LayoutRes int resource, String[] from, @IdRes int[] to,ListView listview) {
        mData = data;
        mResource = mDropDownResource = resource;
        mFrom = from;
        mTo = to;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mListView = listview;
    }

    /**
     * @see android.widget.Adapter#getCount()
     */
    public int getCount() {
        return mData.size();
    }

    /**
     * @see android.widget.Adapter#getItem(int)
     */
    public Object getItem(int position) {
        return mData.get(position);
    }

    /**
     * @see android.widget.Adapter#getItemId(int)
     */
    public long getItemId(int position) {
        return position;
    }

    /**
     * @see android.widget.Adapter#getView(int, View, ViewGroup)
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        return createViewFromResource(mInflater, position, convertView, parent, mResource);
    }

    private View createViewFromResource(LayoutInflater inflater, int position, View convertView,
                                        ViewGroup parent, int resource) {
        View v;
        if (convertView == null) {
            v = inflater.inflate(resource, parent, false);
        } else {
            v = convertView;
        }

        boolean currentselected = false;
        int selectposition = mListView.getCheckedItemPosition();
        if(selectposition==position)
            currentselected = true;


        ImageButton detailbutton = (ImageButton) v.findViewById(R.id.button_HISDetail);
        detailbutton.setTag(position);
        ImageButton playbutton = (ImageButton) v.findViewById(R.id.button_HISPlay);
        playbutton.setTag(position);

        if(GlobalVariable.viewmodel == 1) {
            detailbutton.setColorFilter(v.getResources().getColor(R.color.transparent));
            playbutton.setColorFilter(v.getResources().getColor(R.color.transparent));
        }

        else if(GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2){
            detailbutton.setColorFilter(v.getResources().getColor(R.color.transparent));
            playbutton.setColorFilter(v.getResources().getColor(R.color.transparent));
        }
        else if(false){//else if(GlobalVariable.viewmodel == 2){
            detailbutton.setColorFilter(v.getResources().getColor(R.color.ct_gray));
            playbutton.setColorFilter(v.getResources().getColor(R.color.ct_gray));
        }

        ImageView stateview = (ImageView) v.findViewById(R.id.imageHIS1);
        ImageButton playButton = v.findViewById(R.id.button_HISPlay);

        TextView pstnTextView = v.findViewById(R.id.textHIS2);
        TextView lineTextView = v.findViewById(R.id.textHIS7);
        TextView ctTextView = v.findViewById(R.id.textHIS3);
        TextView timeTextView = v.findViewById(R.id.textHIS5);
        TextView numberTextView = v.findViewById(R.id.textHIS6);
        TextView playTextView = v.findViewById(R.id.tv_HISPlay);

        if(currentselected){
            pstnTextView.setTextColor(v.getResources().getColor(R.color.select_blue));
            lineTextView.setTextColor(v.getResources().getColor(R.color.select_blue));
            ctTextView.setTextColor(v.getResources().getColor(R.color.select_blue));
            timeTextView.setTextColor(v.getResources().getColor(R.color.select_blue));
            numberTextView.setTextColor(v.getResources().getColor(R.color.select_blue));

            pstnTextView.setTextSize(25);
            lineTextView.setTextSize(25);
            ctTextView.setTextSize(25);
            timeTextView.setTextSize(25);
            numberTextView.setTextSize(25);

        }
        else{
            if(GlobalVariable.viewmodel == 1) {
                pstnTextView.setTextColor(Color.WHITE);
                lineTextView.setTextColor(Color.WHITE);
                ctTextView.setTextColor(Color.WHITE);
                timeTextView.setTextColor(Color.WHITE);
                numberTextView.setTextColor(Color.WHITE);
            }
            else if(false){//else if(GlobalVariable.viewmodel == 2){
                pstnTextView.setTextColor(v.getResources().getColor(R.color.ct_gray));
                lineTextView.setTextColor(v.getResources().getColor(R.color.ct_gray));
                ctTextView.setTextColor(v.getResources().getColor(R.color.ct_gray));
                timeTextView.setTextColor(v.getResources().getColor(R.color.ct_gray));
                numberTextView.setTextColor(v.getResources().getColor(R.color.ct_gray));
            }
            else if(GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2){
                pstnTextView.setTextColor(Color.WHITE);
                lineTextView.setTextColor(Color.WHITE);
                ctTextView.setTextColor(Color.WHITE);
                timeTextView.setTextColor(Color.WHITE);
                numberTextView.setTextColor(Color.WHITE);
            }

            pstnTextView.setTextSize(24);
            lineTextView.setTextSize(24);
            ctTextView.setTextSize(24);
            timeTextView.setTextSize(24);
            numberTextView.setTextSize(24);
        }

        if(mData.get(position).get("duration").equals("00:00"))
        {
            timeTextView.setTextColor(Color.TRANSPARENT);
            playButton.setVisibility(View.GONE);
            playTextView.setVisibility(View.VISIBLE);
        }
        else{
            if(GlobalVariable.viewmodel == 1) {
                timeTextView.setTextColor(Color.WHITE);
            }
            else if(false) {//else if(GlobalVariable.viewmodel == 2) {
                timeTextView.setTextColor(v.getResources().getColor(R.color.ct_gray));
            }
            else if(GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2){
                timeTextView.setTextColor(Color.WHITE);
            }
            playButton.setVisibility(View.VISIBLE);
            playTextView.setVisibility(View.GONE);

        }

        //根据状态判断显示图标
        //state 类型
        //  未接        //0   默认状态
        //  已接        //1   导播或者主播接听电话会置这个状态
        //  挂断        //2   导播或者主播将电话挂断会置这个状态
        //  语音留言     //3
        // 黑名单被拒绝   //4
        //  拨出        //10  初始状态也是结束状态(弃用)

        if(GlobalVariable.viewmodel == 1){
            if(mData.get(position).get("state").equals("0"))
            {
                stateview.setImageResource(R.drawable.phonestate5);
            }
            else if(mData.get(position).get("state").equals("1"))
            {
                stateview.setImageResource(R.drawable.phonestate3);
            }
            else if(mData.get(position).get("state").equals("2"))
            {
                stateview.setImageResource(R.drawable.phonestate1);
            }
    //        else if(mData.get(position).get("state").equals("3"))
    //        {
    //
    //        }
    //        else if(mData.get(position).get("state").equals("4"))
    //        {
    //            stateview.setImageResource(R.drawable.phonestate1);
    //        }

            if(mData.get(position).get("operatetype").equals("1"))
            {
                stateview.setImageResource(R.drawable.phonestate2);
            }
        }
        else if(false){//else if(GlobalVariable.viewmodel == 2){
            if(mData.get(position).get("state").equals("0"))
            {
                stateview.setImageResource(R.drawable.ct_phonestate5);
            }
            else if(mData.get(position).get("state").equals("1"))
            {
                stateview.setImageResource(R.drawable.ct_phonestate3);
            }
            else if(mData.get(position).get("state").equals("2"))
            {
                stateview.setImageResource(R.drawable.ct_phonestate1);
            }
            if(mData.get(position).get("operatetype").equals("1"))
            {
                stateview.setImageResource(R.drawable.ct_phonestate2);
            }
        }
        else if(GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2){
            if(mData.get(position).get("state").equals("0"))
            {
                stateview.setImageResource(R.drawable.gs_phonestate5);
            }
            else if(mData.get(position).get("state").equals("1"))
            {
                stateview.setImageResource(R.drawable.gs_phonestate3);
            }
            else if(mData.get(position).get("state").equals("2"))
            {
                stateview.setImageResource(R.drawable.gs_phonestate1);
            }
            if(mData.get(position).get("operatetype").equals("1"))
            {
                stateview.setImageResource(R.drawable.gs_phonestate2);
            }
        }




        bindView(position, v);

        return v;
    }

    /**
     * <p>Sets the layout resource to create the drop down views.</p>
     *
     * @param resource the layout resource defining the drop down views
     * @see #getDropDownView(int, android.view.View, android.view.ViewGroup)
     */
    public void setDropDownViewResource(int resource) {
        mDropDownResource = resource;
    }

    /**
     * Sets the {@link android.content.res.Resources.Theme} against which drop-down views are
     * inflated.
     * <p>
     * By default, drop-down views are inflated against the theme of the
     * {@link Context} passed to the adapter's constructor.
     *
     * @param theme the theme against which to inflate drop-down views or
     *              {@code null} to use the theme from the adapter's context
     * @see #getDropDownView(int, View, ViewGroup)
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void setDropDownViewTheme(Resources.Theme theme) {
        if (theme == null) {
            mDropDownInflater = null;
        } else if (theme == mInflater.getContext().getTheme()) {
            mDropDownInflater = mInflater;
        } else {
            final Context context = new ContextThemeWrapper(mInflater.getContext(), theme);
            mDropDownInflater = LayoutInflater.from(context);
        }
    }

    @Override
    public Resources.Theme getDropDownViewTheme() {
        return mDropDownInflater == null ? null : mDropDownInflater.getContext().getTheme();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = mDropDownInflater == null ? mInflater : mDropDownInflater;
        return createViewFromResource(inflater, position, convertView, parent, mDropDownResource);
    }

    private void bindView(int position, View view) {
        final Map dataSet = mData.get(position);
        if (dataSet == null) {
            return;
        }

        final android.widget.SimpleAdapter.ViewBinder binder = mViewBinder;
        final String[] from = mFrom;
        final int[] to = mTo;
        final int count = to.length;

        for (int i = 0; i < count; i++) {
            final View v = view.findViewById(to[i]);
            if (v != null) {
                final Object data = dataSet.get(from[i]);
                String text = data == null ? "" : data.toString();
                if (text == null) {
                    text = "";
                }

                boolean bound = false;
                if (binder != null) {
                    bound = binder.setViewValue(v, data, text);
                }

                if (!bound) {
                    if (v instanceof Checkable) {
                        if (data instanceof Boolean) {
                            ((Checkable) v).setChecked((Boolean) data);
                        } else if (v instanceof TextView) {
                            // Note: keep the instanceof TextView check at the bottom of these
                            // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                            setViewText((TextView) v, text);
                        } else {
                            throw new IllegalStateException(v.getClass().getName() +
                                    " should be bound to a Boolean, not a " +
                                    (data == null ? "<unknown type>" : data.getClass()));
                        }
                    } else if (v instanceof TextView) {
                        // Note: keep the instanceof TextView check at the bottom of these
                        // ifs since a lot of views are TextViews (e.g. CheckBoxes).
                        setViewText((TextView) v, text);
                    } else if (v instanceof ImageView) {
                        if (data instanceof Integer) {
                            setViewImage((ImageView) v, (Integer) data);
                        } else {
                            setViewImage((ImageView) v, text);
                        }
                    } else {
                        throw new IllegalStateException(v.getClass().getName() + " is not a " +
                                " view that can be bounds by this SimpleAdapter");
                    }
                }
            }
        }
    }

    /**
     * Returns the {@link android.widget.SimpleAdapter.ViewBinder} used to bind data to views.
     *
     * @return a ViewBinder or null if the binder does not exist
     *
     * @see #setViewBinder(android.widget.SimpleAdapter.ViewBinder)
     */
    public android.widget.SimpleAdapter.ViewBinder getViewBinder() {
        return mViewBinder;
    }

    /**
     * Sets the binder used to bind data to views.
     *
     * @param viewBinder the binder used to bind data to views, can be null to
     *        remove the existing binder
     *
     * @see #getViewBinder()
     */
    public void setViewBinder(android.widget.SimpleAdapter.ViewBinder viewBinder) {
        mViewBinder = viewBinder;
    }

    /**
     * Called by bindView() to set the image for an ImageView but only if
     * there is no existing ViewBinder or if the existing ViewBinder cannot
     * handle binding to an ImageView.
     *
     * This method is called instead of {@link #setViewImage(ImageView, String)}
     * if the supplied data is an int or Integer.
     *
     * @param v ImageView to receive an image
     * @param value the value retrieved from the data set
     *
     * @see #setViewImage(ImageView, String)
     */
    public void setViewImage(ImageView v, int value) {
        v.setImageResource(value);
    }

    /**
     * Called by bindView() to set the image for an ImageView but only if
     * there is no existing ViewBinder or if the existing ViewBinder cannot
     * handle binding to an ImageView.
     *
     * By default, the value will be treated as an image resource. If the
     * value cannot be used as an image resource, the value is used as an
     * image Uri.
     *
     * This method is called instead of {@link #setViewImage(ImageView, int)}
     * if the supplied data is not an int or Integer.
     *
     * @param v ImageView to receive an image
     * @param value the value retrieved from the data set
     *
     * @see #setViewImage(ImageView, int)
     */
    public void setViewImage(ImageView v, String value) {
        try {
            v.setImageResource(Integer.parseInt(value));
        } catch (NumberFormatException nfe) {
            v.setImageURI(Uri.parse(value));
        }
    }

    /**
     * Called by bindView() to set the text for a TextView but only if
     * there is no existing ViewBinder or if the existing ViewBinder cannot
     * handle binding to a TextView.
     *
     * @param v TextView to receive text
     * @param text the text to be set for the TextView
     */
    public void setViewText(TextView v, String text) {
        v.setText(text);
    }

    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new SimpleFilter();
        }
        return mFilter;
    }

    /**
     * This class can be used by external clients of SimpleAdapter to bind
     * values to views.
     *
     * You should use this class to bind values to views that are not
     * directly supported by SimpleAdapter or to change the way binding
     * occurs for views supported by SimpleAdapter.
     *
     * @see android.widget.SimpleAdapter#setViewImage(ImageView, int)
     * @see android.widget.SimpleAdapter#setViewImage(ImageView, String)
     * @see android.widget.SimpleAdapter#setViewText(TextView, String)
     */
    public static interface ViewBinder {
        /**
         * Binds the specified data to the specified view.
         *
         * When binding is handled by this ViewBinder, this method must return true.
         * If this method returns false, SimpleAdapter will attempts to handle
         * the binding on its own.
         *
         * @param view the view to bind the data to
         * @param data the data to bind to the view
         * @param textRepresentation a safe String representation of the supplied data:
         *        it is either the result of data.toString() or an empty String but it
         *        is never null
         *
         * @return true if the data was bound to the view, false otherwise
         */
        boolean setViewValue(View view, Object data, String textRepresentation);
    }

    /**
     * <p>An array filters constrains the content of the array adapter with
     * a prefix. Each item that does not start with the supplied prefix
     * is removed from the list.</p>
     */
    private class SimpleFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence prefix) {
            FilterResults results = new FilterResults();

            if (mUnfilteredData == null) {
                mUnfilteredData = new ArrayList<Map<String, ?>>(mData);
            }

            if (prefix == null || prefix.length() == 0) {
                ArrayList<Map<String, ?>> list = mUnfilteredData;
                results.values = list;
                results.count = list.size();
            } else {
                String prefixString = prefix.toString().toLowerCase();

                ArrayList<Map<String, ?>> unfilteredValues = mUnfilteredData;
                int count = unfilteredValues.size();

                ArrayList<Map<String, ?>> newValues = new ArrayList<Map<String, ?>>(count);

                for (int i = 0; i < count; i++) {
                    Map<String, ?> h = unfilteredValues.get(i);
                    if (h != null) {

                        int len = mTo.length;

                        for (int j=0; j<len; j++) {
                            String str =  (String)h.get(mFrom[j]);

                            String[] words = str.split(" ");
                            int wordCount = words.length;

                            for (int k = 0; k < wordCount; k++) {
                                String word = words[k];

                                if (word.toLowerCase().startsWith(prefixString)) {
                                    newValues.add(h);
                                    break;
                                }
                            }
                        }
                    }
                }

                results.values = newValues;
                results.count = newValues.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            mData = (List<Map<String, ?>>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.pjsip.pjsua2.app.ContactsActivity;
import org.pjsip.pjsua2.app.HttpRequest;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class ContactAddDialog extends Dialog {

    private static Button mConfirmButton;
    private static Button mCancelButton;
    private static EditText mAddPstnName;
    private static EditText mAddPstnNumber;

    private Button btnIsExpertT;
    private Button btnIsExpertF;

    private boolean pstn_isexpert = false;

    private ContactAddDialog(Context context, int themeResId) {
        super(context, themeResId);
        imContext = context;
    }

    private Context imContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {


        mConfirmButton = findViewById(R.id.btnPstnConfirm);
        mCancelButton = findViewById(R.id.btnPstnCancel);

        mAddPstnName = findViewById(R.id.addPstnName);
        mAddPstnNumber = findViewById(R.id.addPstnNumber);

        btnIsExpertT = findViewById(R.id.btnIsExpertT);
        btnIsExpertF = findViewById(R.id.btnIsExpertF);
        btnIsExpertT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {pstn_isexpert = !pstn_isexpert;updateViewByConfig();}
        });
        btnIsExpertF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {pstn_isexpert = !pstn_isexpert;updateViewByConfig();}
        });

        mConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pstn_name = String.valueOf(mAddPstnName.getText());
                String pstn_number = String.valueOf(mAddPstnNumber.getText());
                Log.e("添加通讯录日志",pstn_name + " --- " + pstn_number);
                if(!pstn_name.equals("")&&!pstn_number.equals("")){
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                HttpRequest requestA = new HttpRequest();
                                String rsA = requestA.postJson(
                                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/addPstnByAndroid",
                                        "{\"name\":\"" + pstn_name + "\"," +
                                                "\"pstn\":\"" + pstn_number + "\","+
                                                "\"isexpert\":\"" + (pstn_isexpert?1:0) + "\""+
                                                "}"
                                );
                                Log.e("添加通讯录日志",rsA);

                                //更新页面
                                Message m2 = Message.obtain(ContactsActivity.handler_, 1002, null);
                                m2.sendToTarget();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.start();
                }
                dismiss();
            }
        });

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        updateViewByConfig();
        super.onStart();
    }

    public void updateViewByConfig(){
        //设置背景颜色
        int colorblue = imContext.getResources().getColor(R.color.blue);
        int colorwhite = imContext.getResources().getColor(R.color.white);
        int colorgray = imContext.getResources().getColor(R.color.ct_gray);

        if(GlobalVariable.viewmodel == 1  || GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2) {
            btnIsExpertT.setTextColor(pstn_isexpert ? colorblue : colorwhite);
            btnIsExpertF.setTextColor(pstn_isexpert ? colorwhite : colorblue);
            btnIsExpertT.setBackgroundResource(pstn_isexpert ? R.drawable.button_selected : R.drawable.button_selector_thin);
            btnIsExpertF.setBackgroundResource(pstn_isexpert ? R.drawable.button_selector_thin : R.drawable.button_selected);
        }
        else if(false){//else if(GlobalVariable.viewmodel == 2){
            btnIsExpertT.setTextColor(pstn_isexpert ? colorwhite : colorgray);
            btnIsExpertF.setTextColor(pstn_isexpert ? colorgray : colorwhite);
            btnIsExpertT.setBackgroundResource(pstn_isexpert ? R.drawable.button_selected_ctgray : R.drawable.button_selector_ctgray);
            btnIsExpertF.setBackgroundResource(pstn_isexpert ? R.drawable.button_selector_ctgray : R.drawable.button_selected_ctgray);
        }
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;


        private static ContactAddDialog mDialog;

        private static TextView mtvAddPstnName;
        private static TextView mtvAddPstnNumber;
        private static TextView mtvAddPstnExpert;

        private static Button mbtnPstnConfirm;
        private static Button mbtnPstnCancel;

        private static EditText maddPstnName;
        private static EditText maddPstnNumber;

        public Builder(Context context) {
            mContext = context;
            mDialog = new ContactAddDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_add_contact, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            if(false){//GlobalVariable.viewmodel == 2
                mtvAddPstnName = mLayout.findViewById(R.id.tvAddPstnName);
                mtvAddPstnNumber = mLayout.findViewById(R.id.tvAddPstnNumber);
                mtvAddPstnExpert = mLayout.findViewById(R.id.tvAddPstnExpert);
                mbtnPstnConfirm = mLayout.findViewById(R.id.btnPstnConfirm);
                mbtnPstnCancel = mLayout.findViewById(R.id.btnPstnCancel);
                maddPstnName = mLayout.findViewById(R.id.addPstnName);
                maddPstnNumber = mLayout.findViewById(R.id.addPstnNumber);


                mtvAddPstnName.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvAddPstnNumber.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mtvAddPstnExpert.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mbtnPstnConfirm.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mbtnPstnConfirm.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
                mbtnPstnCancel.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mbtnPstnCancel.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));

                maddPstnName.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                maddPstnName.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));
                maddPstnNumber.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                maddPstnNumber.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_ctgray));

            }

        }




        public ContactAddDialog create() {

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }
}

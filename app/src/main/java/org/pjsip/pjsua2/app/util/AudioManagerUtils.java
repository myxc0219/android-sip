package org.pjsip.pjsua2.app.util;

import android.media.AudioManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AudioManagerUtils {
    private static Method setSpeakerphoneOn;
    private static Method setHandsetOn;
    private static Method setHeadsetOn;
    private static Method setUsbHeadsetOn;
    private static Method setEhsHeadsetOn;
    private static Method isWiredHeadsetOn;
    private static Method isUsbHeadsetConnected;

    private AudioManagerUtils() {
    }

    static {
        try {
            Class<?> cls = Class.forName("android.media.AudioManager");
            Method M[] = cls.getMethods();
            for (Method m : M) {
                String n = m.getName();
                if (n.equals("setSpeakerphoneOn")) {
                    setSpeakerphoneOn = m;
                } else if (n.equals("setHandsetOn")) {
                    setHandsetOn = m;
                } else if (n.equals("setHeadsetOn")) {
                    setHeadsetOn = m;
                } else if (n.equals("setUsbHeadsetOn")) {
                    setUsbHeadsetOn = m;
                } else if (n.equals("setEhsHeadsetOn")) {
                    setEhsHeadsetOn = m;
                } else if (n.equals("isWiredHeadsetOn")) {
                    isWiredHeadsetOn = m;
                } else if (n.equals("isUsbHeadsetConnected")) {
                    isUsbHeadsetConnected = m;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setSpeakerphoneOn(AudioManager manager, boolean on) {
        try {
            setSpeakerphoneOn.invoke(manager, on);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void setHandsetOn(AudioManager manager, boolean on) {
        try {
            setHandsetOn.invoke(manager, on);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void setHeadsetOn(AudioManager manager, boolean on) {
        try {
            setHeadsetOn.invoke(manager, on);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void setUsbHeadsetOn(AudioManager manager, boolean on) {
        try {
            setUsbHeadsetOn.invoke(manager, on);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static void setEhsHeadsetOn(AudioManager manager, boolean on) {
        try {
            setEhsHeadsetOn.invoke(manager, on);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public static boolean isWiredHeadsetOn(AudioManager manager, boolean def) {
        try {
            return (boolean) isWiredHeadsetOn.invoke(manager);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }

    public static boolean isUsbHeadsetConnected(AudioManager manager, boolean def) {
        try {
            return (boolean) isUsbHeadsetConnected.invoke(manager);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return def;
    }
}

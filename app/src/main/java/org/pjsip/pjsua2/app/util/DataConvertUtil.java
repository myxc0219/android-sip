package org.pjsip.pjsua2.app.util;

import java.util.HashMap;

public class DataConvertUtil {

    public static HashMap<String, String> putDataHISDlg(String starttime, String duration, String content)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("starttime", starttime);
        item.put("duration", duration);
        item.put("content", content);
        return item;
    }

    public static HashMap<String, String> putData(String uri, String status)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("uri", uri);
        item.put("status", status);
        return item;
    }

    public static HashMap<String, String> putData(String name, String time,String location, String pstn)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("time", time);
        item.put("location", location);
        item.put("pstn", pstn);
        item.put("firsttime", time);
        return item;
    }

    public static HashMap<String, String> putData(String name, String time,String location, String pstn,String tailnum,String pstnmark,String content,String id,String uid,String callednumber)
    {
        HashMap<String, String> item = new HashMap<String, String>();
        item.put("name", name);
        item.put("time", time);
        item.put("location", location);
        item.put("pstn", pstn);
        item.put("tailnum", tailnum);
        item.put("firsttime", time);
        item.put("firsttimeshow", time.substring(11));
        item.put("accepttime", "00:00:00");
        item.put("pstnmark", pstnmark);
        item.put("content", content);
        item.put("statetime", "00:00:00");
        item.put("id", id);
        item.put("uid",uid);
        item.put("callednumber",callednumber);
        return item;
    }

    public static String getConfigString(String key)
    {
        //1.姓名  2.年龄   3.等级  4.地址   5.分组   6.性别   7.黑名单  8.通讯录
        switch(key)
        {
            case "0":
                return "0";
            case "1":
                return "姓名";
            case "2":
                return "年龄";
            case "3":
                return "等级";
            case "4":
                return "地址";
            case "5":
                return "分组";
            case "6":
                return "性别";
            case "7":
                return "黑名单";
            case "8":
                return "通讯录";
            default:
                return key;
        }
    }
}

package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class MsgNotifyDialog extends Dialog {

    private MsgNotifyDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private MsgNotifyDialog mDialog;
        private LinearLayout mLineBkg;
        private TextView mTitle;
        private TextView mContent;
        private Button mConfirmButton;


        public Builder(Context context,String content) {
            mContext = context;
            mDialog = new MsgNotifyDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_msg_notify, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mContent = mLayout.findViewById(R.id.msgContent);
            mContent.setText(content);
            mConfirmButton = mLayout.findViewById(R.id.msgConfirmButton);

            //if(GlobalVariable.viewmodel == 2){
            if(false){
                mLineBkg = mLayout.findViewById(R.id.msgNotifyLineBkg);
                mLineBkg.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_thin_ctgray));
                mTitle = mLayout.findViewById(R.id.msgNotifyTitle);
                mTitle.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mContent.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mConfirmButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector));
                mConfirmButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
            }
        }
        public MsgNotifyDialog create() {
            mConfirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog

            return mDialog;
        }
    }

    @Override
    public void dismiss() {
        GlobalVariable.sysconfig_notify = false;
        Log.e("当前进入了","弹框关闭阶段");
        super.dismiss();
    }
}
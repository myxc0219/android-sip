package org.pjsip.pjsua2.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.app.dialog.LoginLoadDialog;
import org.pjsip.pjsua2.app.dialog.ServerConfigDialog;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.Channel;
import org.pjsip.pjsua2.app.model.ProgramDirector;
import org.pjsip.pjsua2.app.model.TemplateContent;
import org.pjsip.pjsua2.app.model.TemplateType;
import org.pjsip.pjsua2.app.model.UserInfo;
import org.pjsip.pjsua2.app.util.CommonMediaManager;
import org.pjsip.pjsua2.app.util.CommonPlatformManager;
import org.pjsip.pjsua2.app.util.EditTextUtils;
import org.pjsip.pjsua2.app.util.IpUtils;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.observers.DisposableObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class LoginActivity  extends Activity implements Handler.Callback {

    // Allowlist two apps.
    private static final String KIOSK_PACKAGE = "org.pjsip.pjsua2.app";
    private static final String[] APP_PACKAGES = {KIOSK_PACKAGE};

    private static final String TAG = "LoginActivity";
    private static boolean onLogingSurface = true;
    public static Handler handler_;
    private final Handler handler = new Handler(this);
    //private String SIPSERVER;
    private String name;
    private String passwd;
    private String localip;
    private LoginLoadDialog loginLoadDialog;

    private String initial_name;
    private String initial_password;

    private String autologin;
    private String initial_autologin;

    private final CompositeDisposable disposables = new CompositeDisposable();

    Observable<String> sampleObservable() {
//        return Observable.defer(new Supplier<ObservableSource<? extends String>>() {
//            @Override
//            public ObservableSource<? extends String> get() throws Throwable {
//                // Do some long running operation
//                //SystemClock.sleep(5000);
//                return Observable.just("A", "B", "C", "D", "E");
//            }
//        });

      return  Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> emitter) throws Throwable {

                boolean bl_restartsrv = false;
                if(name!=initial_name||passwd!=initial_password)
                    bl_restartsrv = true;
                //try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson("http://" + GlobalVariable.SIPSERVER + ":8080/smart/auth/pc/v1/login?account=" + name + "&passwd=" + passwd, "{}");
                    JSONObject rs_json_token = new JSONObject(rs);
                    Log.e("登录完解析authtoken","22222222");
                    if (rs_json_token.get("code").toString().equals("0000")) {
                        Log.e("SETALLCONFIG","333333");
                        GlobalVariable.authToken = rs_json_token.getJSONObject("data").get("authToken").toString();
                        SharedPreferences sharedPreferencesx = getSharedPreferences("userconfig", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferencesx.edit();//获取编辑器
                        editor.putString("name", name);
                        editor.putString("passwd", passwd);
                        editor.putString("autologin",autologin);
                        editor.putString("sipserver", GlobalVariable.SIPSERVER);
                        editor.commit();//提交修改
                        //请求结束以后立即就需要更新配置了
                        //请求语音网关ip
                        HttpRequest requestVoServer = new HttpRequest();
                        String rsVoServer = requestVoServer.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/shellExcute/getVoicegatewayIP",
                                "{\"type\":\"get voserver\", \"authtoken\":\"" + GlobalVariable.authToken + "\"}"
                        );
                        JSONObject rs_json_voserver = new JSONObject(rsVoServer);
                        GlobalVariable.VOSEVER = String.valueOf(rs_json_voserver.get("data"));//userinfo
                        Log.e("语音网关ip获得",""+GlobalVariable.VOSEVER);
                        //请求web服务器相关本地配置
                        HttpRequest requestWeb = new HttpRequest();
                        String rsWeb = requestWeb.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getSipConfigByToken",
                                "{\"type\":\"get config\", \"authtoken\":\"" + GlobalVariable.authToken + "\"}"
                        );
                        JSONObject rs_json = new JSONObject(rsWeb);
                        JSONObject rs_json_userinfo = rs_json.getJSONObject("data").getJSONObject("userinfo");//userinfo
                        JSONObject rs_json_channel = rs_json.getJSONObject("data").getJSONObject("channel");
                        JSONObject rs_json_server = rs_json.getJSONObject("data").getJSONObject("program");
                        JSONObject rs_json_zhubo = rs_json.getJSONObject("data").getJSONObject("zhubo");
                        JSONObject rs_json_config = rs_json.getJSONObject("data").getJSONObject("config");
                        JSONArray rs_json_pdrlist = rs_json.getJSONObject("data").getJSONArray("pdrlist");
                        //耦合器模式线路对应关系
                        JSONObject rs_json_linemapping = rs_json.getJSONObject("data").getJSONObject("linemapping");

                        //20210803隐藏禁止呼出线路
                        String type1 = String.valueOf(rs_json_linemapping.get("type1"));
                        String type2 = String.valueOf(rs_json_linemapping.get("type2"));
                        String type3 = String.valueOf(rs_json_linemapping.get("type3"));
                        String type4 = String.valueOf(rs_json_linemapping.get("type4"));
                        String type5 = String.valueOf(rs_json_linemapping.get("type5"));
                        String type6 = String.valueOf(rs_json_linemapping.get("type6"));

                        //20210718用户绑定
                        GlobalVariable.user_tableid = String.valueOf(rs_json_userinfo.get("id"));

                        String userid1 = String.valueOf(rs_json_linemapping.get("userid1"));
                        String userid2 = String.valueOf(rs_json_linemapping.get("userid2"));
                        String userid3 = String.valueOf(rs_json_linemapping.get("userid3"));
                        String userid4 = String.valueOf(rs_json_linemapping.get("userid4"));
                        String userid5 = String.valueOf(rs_json_linemapping.get("userid5"));
                        String userid6 = String.valueOf(rs_json_linemapping.get("userid6"));

                        //20210618测试耦合器号码位置绑定
                        String line1 = String.valueOf(rs_json_linemapping.get("line1"));
                        String line2 = String.valueOf(rs_json_linemapping.get("line2"));
                        String line3 = String.valueOf(rs_json_linemapping.get("line3"));
                        String line4 = String.valueOf(rs_json_linemapping.get("line4"));
                        String line5 = String.valueOf(rs_json_linemapping.get("line5"));
                        String line6 = String.valueOf(rs_json_linemapping.get("line6"));

                        if(!line1.equals("")&&!line1.equals("null") && type1.equals("1")){
                            GlobalVariable.getCtNumberMapRevertForbid.put(1, line1);
                        }
                        if(!line2.equals("")&&!line2.equals("null") && type2.equals("1")){
                            GlobalVariable.getCtNumberMapRevertForbid.put(2, line2);
                        }
                        if(!line3.equals("")&&!line3.equals("null") && type3.equals("1")){
                            GlobalVariable.getCtNumberMapRevertForbid.put(3, line3);
                        }
                        if(!line4.equals("")&&!line4.equals("null") && type4.equals("1")){
                            GlobalVariable.getCtNumberMapRevertForbid.put(4, line4);
                        }
                        if(!line5.equals("")&&!line5.equals("null") && type5.equals("1")){
                            GlobalVariable.getCtNumberMapRevertForbid.put(5, line5);
                        }
                        if(!line6.equals("")&&!line6.equals("null") && type6.equals("1")){
                            GlobalVariable.getCtNumberMapRevertForbid.put(6, line6);
                        }

                        if(!line1.equals("")&&!line1.equals("null")){
                            GlobalVariable.ctNumberMapAll.put(line1, 1);
                            GlobalVariable.ctNumberMapRevertAll.put(1, line1);
                        }
                        if(!line2.equals("")&&!line2.equals("null")){
                            GlobalVariable.ctNumberMapAll.put(line2, 2);
                            GlobalVariable.ctNumberMapRevertAll.put(2, line2);
                        }
                        if(!line3.equals("")&&!line3.equals("null")){
                            GlobalVariable.ctNumberMapAll.put(line3, 3);
                            GlobalVariable.ctNumberMapRevertAll.put(3, line3);
                        }
                        if(!line4.equals("")&&!line4.equals("null")){
                            GlobalVariable.ctNumberMapAll.put(line4, 4);
                            GlobalVariable.ctNumberMapRevertAll.put(4, line4);
                        }
                        if(!line5.equals("")&&!line5.equals("null")){
                            GlobalVariable.ctNumberMapAll.put(line5, 5);
                            GlobalVariable.ctNumberMapRevertAll.put(5, line5);
                        }
                        if(!line6.equals("")&&!line6.equals("null")){
                            GlobalVariable.ctNumberMapAll.put(line6, 6);
                            GlobalVariable.ctNumberMapRevertAll.put(6, line6);
                        }



                        if(!line1.equals("")&&!line1.equals("null") && (userid1.equals("0")||userid1.equals(GlobalVariable.user_tableid))){
                            GlobalVariable.ctNumberMap.put(line1,1);
                            GlobalVariable.ctNumberMapRevert.put(1, line1);
                        }
                        if(!line2.equals("")&&!line2.equals("null") && (userid2.equals("0")||userid2.equals(GlobalVariable.user_tableid))){
                            GlobalVariable.ctNumberMap.put(line2,2);
                            GlobalVariable.ctNumberMapRevert.put(2, line2);
                        }
                        if(!line3.equals("")&&!line3.equals("null") && (userid3.equals("0")||userid3.equals(GlobalVariable.user_tableid))){
                            GlobalVariable.ctNumberMap.put(line3,3);
                            GlobalVariable.ctNumberMapRevert.put(3, line3);
                        }
                        if(!line4.equals("")&&!line4.equals("null") && (userid4.equals("0")||userid4.equals(GlobalVariable.user_tableid))){
                            GlobalVariable.ctNumberMap.put(line4,4);
                            GlobalVariable.ctNumberMapRevert.put(4, line4);
                        }
                        if(!line5.equals("")&&!line5.equals("null") && (userid5.equals("0")||userid5.equals(GlobalVariable.user_tableid))){
                            GlobalVariable.ctNumberMap.put(line5,5);
                            GlobalVariable.ctNumberMapRevert.put(5, line5);
                        }
                        if(!line6.equals("")&&!line6.equals("null") && (userid6.equals("0")||userid6.equals(GlobalVariable.user_tableid))){
                            GlobalVariable.ctNumberMap.put(line6,6);
                            GlobalVariable.ctNumberMapRevert.put(6, line6);
                        }

                        if(GlobalVariable.liteModel){
                            GlobalVariable.ctNumberMap = GlobalVariable.ctNumberMapAll;
                            GlobalVariable.ctNumberMapRevert = GlobalVariable.ctNumberMapRevertAll;
                        }

                        //20210630添加分路置忙功能
                        String busy1 = String.valueOf(rs_json_linemapping.get("busy1"));
                        String busy2 = String.valueOf(rs_json_linemapping.get("busy2"));
                        String busy3 = String.valueOf(rs_json_linemapping.get("busy3"));
                        String busy4 = String.valueOf(rs_json_linemapping.get("busy4"));
                        String busy5 = String.valueOf(rs_json_linemapping.get("busy5"));
                        String busy6 = String.valueOf(rs_json_linemapping.get("busy6"));
                        if(!busy1.equals("")&&!busy1.equals("null")){
                            if ((Integer.parseInt(busy1) == 1)) { GlobalVariable.linebusy1 = true;} else {GlobalVariable.linebusy1 = false;}
                        }
                        if(!busy2.equals("")&&!busy2.equals("null")){
                            if ((Integer.parseInt(busy2) == 1)) { GlobalVariable.linebusy2 = true;} else {GlobalVariable.linebusy2 = false;}
                        }
                        if(!busy3.equals("")&&!busy3.equals("null")){
                            if ((Integer.parseInt(busy3) == 1)) { GlobalVariable.linebusy3 = true;} else {GlobalVariable.linebusy3 = false;}
                        }
                        if(!busy4.equals("")&&!busy4.equals("null")){
                            if ((Integer.parseInt(busy4) == 1)) { GlobalVariable.linebusy4 = true;} else {GlobalVariable.linebusy4 = false;}
                        }
                        if(!busy5.equals("")&&!busy5.equals("null")){
                            if ((Integer.parseInt(busy5) == 1)) { GlobalVariable.linebusy5 = true;} else {GlobalVariable.linebusy5 = false;}
                        }
                        if(!busy6.equals("")&&!busy6.equals("null")){
                            if ((Integer.parseInt(busy6) == 1)) { GlobalVariable.linebusy6 = true;} else {GlobalVariable.linebusy6 = false;}
                        }

                        GlobalVariable.pgmid = rs_json_server.get("pgmid").toString();
                        GlobalVariable.username = rs_json_userinfo.get("userName").toString();
                        GlobalVariable.channelid = Long.parseLong(rs_json_channel.get("id").toString());
                        GlobalVariable.channelname = rs_json_channel.get("name").toString().equals("-") ? rs_json_channel.get("parentname").toString() : rs_json_channel.get("name").toString();
                        GlobalVariable.EUSERVER = rs_json_server.get("kamsrv").toString();
                        GlobalVariable.sipUri = "<sip:" + rs_json_server.get("sipid").toString() + "@" + rs_json_server.get("kamsrv").toString() + ">";
                        GlobalVariable.sipUriParam = "sip:" + rs_json_server.get("sipid").toString() + "@" + rs_json_server.get("kamsrv").toString();
                        //				//获得页面配置
                        //				JSONObject rs_json_viewconfig = rs_json.getJSONObject("data").getJSONObject("viewconfig");
                        //				//发送消息通知主线程处理界面绘制
                        //				Message m2 = Message.obtain(MainActivity.handler_,1002,rs_json_viewconfig);
                        //				m2.sendToTarget();
                        GlobalVariable.zhuboUri = "<sip:" + rs_json_zhubo.get("sipid").toString() + "@" + GlobalVariable.EUSERVER + ">";
                        GlobalVariable.mbcid = Long.parseLong(rs_json_zhubo.get("mbcid").toString());

                        if (rs_json_config.get("$ref").toString().equals("$.data.zhubo")) {
                            GlobalVariable.user_sipid = rs_json_zhubo.get("sipid").toString();
                            GlobalVariable.userid = Long.parseLong(rs_json_zhubo.get("mbcid").toString());
                            GlobalVariable.charactername = "broadcaster";
                            GlobalVariable.sipid = GlobalVariable.user_sipid;
                        } else//if(rs_json_config.get("$ref").toString().equals("$.data.daobo"))
                        {
                            JSONObject rs_json_daobo = rs_json.getJSONObject("data").getJSONObject("daobo");
                            GlobalVariable.user_sipid = rs_json_daobo.get("sipid").toString();
                            GlobalVariable.userid = Long.parseLong(rs_json_daobo.get("pdrid").toString());
                            GlobalVariable.charactername = "director";
                            GlobalVariable.sipid = GlobalVariable.user_sipid;
                        }
                        GlobalVariable.pdrList = JSON.parseArray(rs_json_pdrlist.toString(), ProgramDirector.class);
                        GlobalVariable.localUri = "<sip:" + GlobalVariable.user_sipid + "@" + GlobalVariable.EUSERVER + ">";
                        GlobalVariable.localUriParam = "sip:" + GlobalVariable.user_sipid + "@" + GlobalVariable.EUSERVER;//+ ">";//2021-03-04多了右尖括号
                        Log.e("SIP配置结束","AAAAA");
                        HttpRequest requestX = new HttpRequest();
                        String rsX = requestX.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateLocalUri",
                                "{\"character\":\"" + GlobalVariable.charactername + "\"," +
                                        "\"localip\":\"" + GlobalVariable.localip + "\"," +
                                        "\"pgmid\":\"" + GlobalVariable.pgmid + "\"," +
                                        "\"userid\":\"" + GlobalVariable.userid + "\"}"
                        );
                        HttpRequest request1 = new HttpRequest();
                        String rs1 = request1.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateConfigJson",
                                "{\"pgmid\":\"" + GlobalVariable.pgmid + "\"}"
                        );
                        Log.e("更新配置文件结束","BBBBB");
                        //在启动时就加载一次配置
                        HttpRequest requestConfig = new HttpRequest();
                        String rsConfig = requestConfig.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getSysConfig",
                                "{\"userid\":\"" + GlobalVariable.userid + "\"," +
                                        "\"pgmid\":\"" + GlobalVariable.pgmid + "\" }"
                        );
                        Log.e("getSysConfig","BBBBB");
                        JSONObject rs_json_sysconfig = new JSONObject(rsConfig);
                        JSONObject rs_json_userstate = rs_json_sysconfig.getJSONObject("data").getJSONObject("userstate");
                        JSONObject rs_json_configcontent = rs_json_sysconfig.getJSONObject("data").getJSONObject("config");

                        GlobalVariable.online = rs_json_userstate.get("online").toString().equals("0") ? false : true;

                        GlobalVariable.callring = rs_json_userstate.get("callring").toString().equals("0") ? false : true;

                        GlobalVariable.communicatering = rs_json_userstate.get("communicatering").toString().equals("0") ? false : true;

                        GlobalVariable.busystate = rs_json_userstate.get("communicatering").toString().equals("0") ? false : true;

                        GlobalVariable.callnum = Integer.parseInt(rs_json_userstate.get("callnum").toString());

                        GlobalVariable.waitnum = Integer.parseInt(rs_json_userstate.get("waitnum").toString());

                        GlobalVariable.allowblacklist = rs_json_configcontent.get("allowblacklist").toString().equals("0") ? false : true;

                        GlobalVariable.whitelistmodel = rs_json_configcontent.get("whitelistmodel").toString().equals("0") ? false : true;

                        GlobalVariable.busyall = Integer.parseInt(rs_json_configcontent.get("busyall").toString());

                        GlobalVariable.autoswitch = rs_json_configcontent.get("autoswitch").toString().equals("0") ? false : true;
                        GlobalVariable.sipmodel = Integer.parseInt(rs_json_configcontent.get("sipmodel").toString());
                        GlobalVariable.viewmodel = Integer.parseInt(rs_json_configcontent.get("viewmodel").toString());
                        GlobalVariable.timeout_milliseconds = 10000 * Integer.parseInt(String.valueOf(rs_json_configcontent.get("msgwait")==null?"30":rs_json_configcontent.get("msgwait")));

                        GlobalVariable.SEHTTPServer = String.valueOf(rs_json_configcontent.get("httpreflect"));
                        GlobalVariable.livelock = String.valueOf(rs_json_configcontent.get("livelock")).equals("0") ? false : true;



                        //20210426热更新直接改成重启服务
                        //				JSONObject rs_json1 = new JSONObject(rs1);
                        //				if(rs_json1.get("code").toString().equals("0000"))
                        //				{
                        //					//通知SIPSERVER对配置进行热更新
                        //					String msg_to_send = "{ \"msgType\": \"UPDATE CONFIG\", \"config\": \"" + "/usr/sipprj/bin/"+GlobalVariable.pgmid + ".json" + "\""+" }";
                        //					Intent intent_ack = new Intent(MainActivity.this, JWebSocketClientService.class);
                        //					intent_ack.putExtra("msg_to_send", msg_to_send);
                        //					startService(intent_ack);
                        HttpRequest requestRS = new HttpRequest();
                        //String authToken = "null";
                        //sipmodel = 1;//默认的模式是单导播模式，模式代码1：单导播模式，0：无导播模式 2：多导播模式,3:仅导播模式
                        if (bl_restartsrv) {

                                String rsRS = requestRS.postJson(
                                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/shellExcute/restartsipsubsrvm",
                                        "{\"path1\":\"/usr/sipprj/bin/sipsrv.json\"," +
                                                "\"path2\":\"\"," +
                                                "\"model\":\""+ GlobalVariable.sipmodel+"\"" +
                                                "}"
                                );

                        }
                        Log.e("restartsipsubsrvs","555555");
                        HttpRequest requestIntercom = new HttpRequest();
                        String rsIntercom = requestIntercom.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getAllSipMap",
                                "{\"type\":\"get config\"}"
                        );
                        JSONObject rs_json_Intercom = new JSONObject(rsIntercom);

                        JSONArray rs_json_itcarray = rs_json_Intercom.getJSONArray("data");
                        GlobalVariable.userinfoList = JSON.parseArray(rs_json_itcarray.toString(), UserInfo.class);
                        Log.e("getAllSipMap","66666");

                        HttpRequest requestTp = new HttpRequest();
                        String rsTp = requestTp.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getTemplateWithContent",
                                "{\"type\":\"get config\", \"authtoken\":\"" + GlobalVariable.authToken + "\"}"
                        );
                        JSONObject rs_json_tp = new JSONObject(rsTp);

                        JSONObject rs_json_data = rs_json_tp.getJSONObject("data");
                        JSONArray typearray = rs_json_data.getJSONArray("typelist");
                        GlobalVariable.typeList = JSON.parseArray(typearray.toString(), TemplateType.class);
                        GlobalVariable.typeArray = new String[GlobalVariable.typeList.size()];

                        int index = 0;
                        for (TemplateType type : GlobalVariable.typeList) {
                            GlobalVariable.typeArray[index] = type.getTypename();
                            JSONArray contentarray = rs_json_data.getJSONArray(type.getTypeno());
                            List<TemplateContent> contentList = JSON.parseArray(contentarray.toString(), TemplateContent.class);

                            String[] contenttextArray = new String[contentList.size()];
                            int text_index = 0;
                            for (TemplateContent content : contentList) {
                                contenttextArray[text_index] = content.getTextcontent();
                                text_index++;
                            }
                            GlobalVariable.contentarrayMap.put(String.valueOf(index), contenttextArray);
                            index++;
                        }

                        HttpRequest requestCH = new HttpRequest();
                        String rsCH = requestTp.postJson(
                                "http://" + GlobalVariable.SIPSERVER + ":8080/smart/dicsearch/pc/v1/getChannelList",
                                "{\"type\":\"get config\", \"authtoken\":\"" + GlobalVariable.authToken + "\"}"
                        );
                        Log.e("SIPSERVER",GlobalVariable.SIPSERVER);
                        Log.e("频道列表",rsCH);
                        JSONObject rs_json_ch = new JSONObject(rsCH);
                        JSONArray channelarray = rs_json_ch.getJSONArray("data");
                        GlobalVariable.channelList = JSON.parseArray(channelarray.toString(), Channel.class);

                        //在获得频道列表后将当前频道移到第一位
                        int replaceidx = -1;
                        for(Channel ch : GlobalVariable.channelList){
                            if(ch.getId() == GlobalVariable.channelid){
                                replaceidx = GlobalVariable.channelList.indexOf(ch);
                            }
                        }
                        if(replaceidx!=-1&&GlobalVariable.channelList.size()>0) {
                            Collections.swap(GlobalVariable.channelList, 0, replaceidx);
                        }



                    } else {
                        LogClient.generate("【用户未成功登录】");
                        throw new Exception("【用户未成功登录】");
                    }
//                }catch (Exception e){
//                    LogClient.generate("【配置获取错误】"+e.getMessage());
//                    e.printStackTrace();
//                }
//                emitter.onNext("A");
//                emitter.onNext("B");
                emitter.onComplete();
            }
        });
    }

    public void showMainActivity(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showHistoryActivity(View view)
    {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showContactsActivity(View view)
    {
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showCommunicateActivity(View view)
    {
        Intent intent = new Intent(this, CommunicateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showSettingActivity(View view)
    {
        Intent intent = new Intent(this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void showDailActivity(View view)
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, DailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), LoginActivity.this);
            }
        }
        return super.dispatchTouchEvent(ev);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        File[] folders = new File[3];
//        Observable.fromArray(folders)
//                .flatMap(new Func1<File, ObservableSource<File>>() {
//                    @Override
//                    public ObservableSource<File> call(File file) {
//                        return Observable.fromArray(file.listFiles());
//                    }
//                })
//                .filter(new Func1<File, Boolean>() {
//                    @Override
//                    public Boolean call(File file) {
//                        return file.getName().endsWith(".png");
//                    }
//                })
//                .map(new Func1<File, Bitmap>() {
//                    @Override
//                    public Bitmap call(File file) {
//                        return getBitmapFromFile(file);
//                    }
//                })
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<Bitmap>() {
//                    @Override
//                    public void call(Bitmap bitmap) {
//                        imageCollectorView.addImage(bitmap);
//                    }
//                });
//        //标准的RXJava3的链路调用创建方式
//        Observable<String> o = Observable.create(s->{
//                Log.i(TAG,"开始访问网络");
//                s.onNext("1");
//                s.onNext("2");
//                s.onNext("3");
//                s.onComplete();
//        });
//        Observable<String> o1 = o.flatMap(
//                new Function<String, ObservableSource<? extends String>>() {
//                    @Override
//                    public ObservableSource<? extends String> apply(String s) throws Throwable {
//                        return Observable.just(s).map(item->item+"+1").subscribeOn(Schedulers.io());
//                    }
//                }
//        );

        GlobalVariable.login_success = false;
        @SuppressLint("WrongConstant")
        Object mPlatformManager  = getBaseContext().getSystemService("platform");

        handler_ = handler;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_ct);

        new Thread() {
            @Override
            public void run() {
                LogClient.generate("登录界面初始化完成");
            }
        }.start();

        CommonMediaManager mediaManager = CommonMediaManager.getInstance(getBaseContext());
        CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
        mediaManager.setVoiceAudioDevice(1);
        flashManager.flashSpeaker();

        localip = IpUtils.getLocalIpAddress();
        GlobalVariable.localip = IpUtils.getLocalIpAddress();

        //读取配置文件中的数据并赋值给控件
        SharedPreferences sharedPreferences = getSharedPreferences("userconfig", Context.MODE_PRIVATE);
        name = sharedPreferences.getString("name", "");
        passwd = sharedPreferences.getString("passwd", "");
        autologin = sharedPreferences.getString("autologin", "");
        initial_name = name;
        initial_password = passwd;
        initial_autologin = autologin;

        GlobalVariable.SIPSERVER = sharedPreferences.getString("sipserver", "192.168.1.234");
//        if(GlobalVariable.isDaLian){
//            GlobalVariable.SIPSERVER = "192.168.1.234";
//            ImageButton loginSetting = (ImageButton)findViewById(R.id.loginSetting);
//            loginSetting.setVisibility(View.GONE);
//        }
        EditText textName = (EditText)findViewById(R.id.loginName);
        EditText textPasswd = (EditText)findViewById(R.id.loginPasswd);
        TextView textLocalip = (TextView)findViewById(R.id.loginLocalip);
        textName.setText(name);
        textPasswd.setText(passwd);
        textLocalip.setText(localip);

        TextView textServerip = (TextView)findViewById(R.id.loginServerip);
        textServerip.setText(GlobalVariable.SIPSERVER);

        CheckBox checkboxAutologin  = (CheckBox)findViewById(R.id.checkbox_autologin);
        if(autologin.equals("true")){
            checkboxAutologin.setChecked(true);
        }
        else{
            checkboxAutologin.setChecked(false);
        }
        checkboxAutologin.setOnCheckedChangeListener(
            new CompoundButton.OnCheckedChangeListener() {
                 @Override
                 public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                     if(isChecked){
                         autologin = "true";
                     }
                     else{
                         autologin = "false";
                     }
                 }
             }
        );

        new Thread() {
            @Override
            public void run() {
                try {
                    while(true) {
                        currentThread().sleep(30000);
                        if (autologin.equals("true") && !GlobalVariable.login_success) {
                            Message m2 = Message.obtain(LoginActivity.handler_,1004,null);
                            m2.sendToTarget();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();



        new Thread() {
            @Override
            public void run() {
                try {
                    while(true){
                        try {
                            //Date nowdate = new Date();
                            //处理左上角显示系统时间
                            Message m2 = Message.obtain(LoginActivity.handler_,1001,null);
                            m2.sendToTarget();
                            Thread.sleep(1000);
                        }catch (Exception e){
//                            Log.i("aaa", "error:" + e.getMessage());
//                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    LogClient.generate(e.getMessage());
                    e.printStackTrace();
                }
            }
        }.start();

        textPasswd.addTextChangedListener(
                new TextWatcher() {
                  @Override
                  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                  }
                  @Override
                  public void onTextChanged(CharSequence text, int start, int before, int count) {
                  }
                  @Override
                  public void afterTextChanged(Editable s) {
                      passwd = textPasswd.getText().toString();
                  }
              }
         );
        textName.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }
                    @Override
                    public void onTextChanged(CharSequence text, int start, int before, int count) {
                    }
                    @Override
                    public void afterTextChanged(Editable s) {
                        name = textName.getText().toString();
                    }
                }
        );

        //调整系统音量至80%
//        AudioManager audioManager;
//        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
//        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,  audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
    }

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
	protected void onStart() {
        Context context = this.getApplicationContext();//.getContext();

        try {
            DevicePolicyManager dpm =
                    (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
            //ComponentName adminName = getComponentName();
            ComponentName adminName = new ComponentName(this, AdminReceiver.class);

            DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            boolean isAdminActive = devicePolicyManager.isAdminActive(adminName);
            if (!isAdminActive) {//这一句一定要有...
                Intent intent = new Intent();
                //指定动作
                intent.setAction(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                //指定给那个组件授权
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminName);
                startActivity(intent);
            }
            dpm.setLockTaskPackages(adminName, APP_PACKAGES);
            // Set an option to turn on lock task mode when starting the activity.
            ActivityOptions options = ActivityOptions.makeBasic();
            options.setLockTaskEnabled(true);

            // Start our kiosk app's main activity with our lock task mode option.
//        PackageManager packageManager = context.getPackageManager();
//        Intent launchIntent = packageManager.getLaunchIntentForPackage(KIOSK_PACKAGE);
//        if (launchIntent != null) {
//            context.startActivity(launchIntent, options.toBundle());
//        }
            if (dpm.isLockTaskPermitted(this.getPackageName())) {
                startLockTask();
            } else {
                Log.e("锁屏失败", "没有权限");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        //定义AudioManager
        SharedPreferences sharedPreferences = getSharedPreferences("volumeconfig", Context.MODE_PRIVATE);
        int callvolume = Integer.parseInt(sharedPreferences.getString("callvolume", "90"));
        int musicvolume = Integer.parseInt(sharedPreferences.getString("musicvolume", "90"));

        AudioManager mgr = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        mgr.setStreamVolume(AudioManager.STREAM_VOICE_CALL,callvolume,AudioManager.FLAG_SHOW_UI);
        mgr.setStreamVolume(AudioManager.STREAM_MUSIC,musicvolume, AudioManager.FLAG_SHOW_UI);

//        // 调高音量
//        mgr.adjustStreamVolume(AudioManager.STREAM_MUSIC,AudioManager.ADJUST_RAISE, AudioManager.FLAG_SHOW_UI);
//        // 调低音量
//        mgr.adjustStreamVolume(AudioManager.STREAM_MUSIC,AudioManager.ADJUST_LOWER, AudioManager.FLAG_SHOW_UI);



//        new Thread(new Runnable() {
//            @RequiresApi(api = Build.VERSION_CODES.N)
//            @Override
//            public void run() {
//                while (onLogingSurface) {
//                    try {
//                        Thread.sleep(1000);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    //Log.e("线程在响应",(new Date()).toString()+"");
//                    ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//                    List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
//                    if (!tasks.isEmpty()) {
//                        ComponentName topActivity = tasks.get(0).topActivity;
//                        if (!topActivity.getPackageName().equals(getPackageName())) {
//                            Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//                            startActivity(intent);
//                        }
//                    }
//                }
//            }
//        }).start();
        super.onStart();
    }

    @Override
    public boolean handleMessage(@NonNull Message m) {
        if(m.what == 1001)
        {
			TextView timeView = (TextView)findViewById(R.id.loginTime);
			if(timeView!=null) {
                Date date = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String dateString = sdf.format(date);
                timeView.setText(dateString);
            }

            /**监听服务状态,在服务未启动时主图标显示红色**/
//            new Thread() {
//                @Override
//                public void run() {
//                    String result = "";
//                    boolean net_error = false;
//                    try {
//                        HttpRequest request1 = new HttpRequest();
//                        String rs1 = request1.postJson("http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/chkproc", "{\"path1\":\"sipsubsrv_m\",\"path2\":\"\"}");
//                        JSONObject rs_json1 = new JSONObject(rs1);
//                        if(String.valueOf(rs_json1.getJSONArray("data").get(0)).equals("0"))
//                        {
//                            result +="sip服务未启动";
//                            net_error = true;
//                        }
//                        HttpRequest request2 = new HttpRequest();
//                        String rs2 = request2.postJson("http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/chkproc", "{\"path1\":\"kamailio\",\"path2\":\"\"}");
//                        JSONObject rs_json2 = new JSONObject(rs2);
//                        if(String.valueOf(rs_json2.getJSONArray("data").get(0)).equals("0"))
//                        {
//                            result +="kamalio服务未启动";
//                            net_error = true;
//                        }
//                        HttpRequest request3 = new HttpRequest();
//                        String rs3 = request3.postJson("http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/chkproc", "{\"path1\":\"sipcomm\",\"path2\":\"\"}");
//                        JSONObject rs_json3 = new JSONObject(rs3);
//                        if(String.valueOf(rs_json3.getJSONArray("data").get(0)).equals("0"))
//                        {
//                            result +="串口服务未启动";
//                            net_error = true;
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        result = "网络连接异常";
//                        net_error = true;
//                    }
//                    if(net_error)
//                    {
//                        if( LoginActivity.handler_!=null) {
//                            Message m2 = Message.obtain(LoginActivity.handler_, 1011, result);
//                            m2.sendToTarget();
//                        }
//                    }
//                    else{
//                        if( LoginActivity.handler_!=null){
//                            Message m2 = Message.obtain(LoginActivity.handler_,1011,"success");
//                            m2.sendToTarget();
//                        }
//
//                    }
//                }
//            }.start();


            return true;
        }
        else if(m.what == 1002)
        {
            TextView textServerip = (TextView)findViewById(R.id.loginServerip);
            textServerip.setText(GlobalVariable.SIPSERVER);
        }
        else if(m.what == 1003)
        {

        }
        else if(m.what == 1004){
            login(null);
        }
        else if(m.what == 1011){
            ImageButton loginSetting = findViewById(R.id.loginSetting);
            if(String.valueOf(m.obj).equals("success")){
                loginSetting.setColorFilter(getResources().getColor(R.color.white));
            }
            else{
                loginSetting.setColorFilter(getResources().getColor(R.color.im_red));
            }
        }

        return false;

    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        if(loginLoadDialog!=null)
            loginLoadDialog.dismiss();
        handler_ = null;
    }

//    @Override
//    public boolean dispatchKeyEvent(KeyEvent event) {
//        int keycode = event.getKeyCode();
//        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
//            if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
//                //this.exitApp();
//            }
//            return true;
//        }
//        else {
//            return super.dispatchKeyEvent(event);
//        }
//    }

    public void muteCall(View view)
    {

    }

    public void showServerConfig(View view){
        ServerConfigDialog serverConfigDialog = new ServerConfigDialog.Builder(LoginActivity.this).create();
        serverConfigDialog.show();
        serverConfigDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
        serverConfigDialog.getWindow().setLayout(500, 300);
    }

    public void login(View view)  {
        Log.e("开始LOGINGIF","000000");
        loginLoadDialog = new LoginLoadDialog.Builder(LoginActivity.this).create();
        loginLoadDialog.show();
        loginLoadDialog.getWindow().setDimAmount(0f);
        loginLoadDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        loginLoadDialog.getWindow().setLayout(1024, 600);
        Log.e("开始LOGIN","111111");
        disposables.add(sampleObservable()
                // Run on a background thread
                .subscribeOn(Schedulers.io())
                // Be notified on the main thread
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<String>() {
                    @Override public void onComplete() {
                        Log.d(TAG, "onComplete()");
                        //加载完配置后注意根据配置决定界面按钮显示
                        //服务请求完毕后通知主界面


                        //发送HTTP请求通知登录成功
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    String character = "";
                                    if (GlobalVariable.charactername.equals("director"))
                                        character = "daobo";
                                    else
                                        character = "zhubo";
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
                                    HttpRequest request = new HttpRequest();
                                    String rs = request.postJson(
                                            GlobalVariable.SEHTTPServer + "/login",
                                            "{" +
                                                    "\"" + character + "\":\"" + name + "\"," +
                                                    "\"time\":\"" + sdf.format(new Date()) + "\"," +
                                                    "\"code\":" + 200 + "}"
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();

                        onLogingSurface = false;
                        finish();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }

                    @Override public void onError(Throwable e) {
                        Log.e(TAG, "onError()", e);
                        LogClient.generate("【登录错误】"+e.getMessage());

                        loginLoadDialog.dismiss();
                        AlertDialog.Builder builder  = new AlertDialog.Builder(LoginActivity.this);
                        builder.setTitle("确认" ) ;
                        builder.setMessage("登录失败" ) ;
                        builder.setPositiveButton("是" ,  null );
                        builder.show();
                    }

                    @Override public void onNext(String string) {
                        Log.d(TAG, "onNext(" + string + ")");
                    }
                }));

    }

}

package org.pjsip.pjsua2.app;


import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.AudioMedia;
import org.pjsip.pjsua2.app.controller.ActivityController;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.model.CallRecord;
import org.pjsip.pjsua2.app.util.CommonMediaManager;
import org.pjsip.pjsua2.app.util.CommonPlatformManager;
import org.pjsip.pjsua2.app.util.MyTimerUtils;

import java.util.List;

public class BaseActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityController.addActivity(this);
        ActivityController.setCurrentActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityController.removeActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyTimerUtils.getInstance(this).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyTimerUtils.getInstance(this).cancel();
    }

    @Override
    public void onUserInteraction() {
        MyTimerUtils.getInstance(this).cancel();
        MyTimerUtils.getInstance(this).start();
        super.onUserInteraction();
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        Log.e("KEYCODE", String.valueOf(event.getKeyCode()));
        int keycode = event.getKeyCode();
        AudioManager mAudioManager = (AudioManager)getBaseContext().getSystemService(Context.AUDIO_SERVICE);

        CommonMediaManager mediaManager = CommonMediaManager.getInstance(getBaseContext());
        CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keycode) {
                case 24://音量加
                    int callvolume_asc =  mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
                    int musicvolume_asc = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    SharedPreferences sharedPreferencesx_asc = getSharedPreferences("volumeconfig", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor_asc = sharedPreferencesx_asc.edit();//获取编辑器
                    editor_asc.putString("callvolume", callvolume_asc+"");
                    editor_asc.putString("musicvolume", musicvolume_asc+"");
                    editor_asc.commit();//提交修改
                    Log.e("当前电话音量",""+callvolume_asc);
                    Log.e("当前媒体音量",""+musicvolume_asc);
                    if(callvolume_asc>7){
                        try {
                            AudioMedia play_med = MainActivity.onlycall.getAudioMedia(-1);
                            long rxlevel = play_med.getRxLevel();
                            Log.e("rxlevel",""+rxlevel);
                            play_med.adjustRxLevel((float) 0.5);
                            long rxlevel_new = play_med.getRxLevel();
                            Log.e("rxlevel_new",""+rxlevel_new);

                            long txlevel = play_med.getTxLevel();
                            Log.e("txlevel",""+txlevel);
                            play_med.adjustTxLevel((float) 0.5);
                            long txlevel_new = play_med.getTxLevel();
                            Log.e("txlevel_new",""+txlevel_new);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case 25://音量减
                    int callvolume =  mAudioManager.getStreamVolume(AudioManager.STREAM_VOICE_CALL);
                    int musicvolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                    SharedPreferences sharedPreferencesx = getSharedPreferences("volumeconfig", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferencesx.edit();//获取编辑器
                    editor.putString("callvolume", callvolume+"");
                    editor.putString("musicvolume", musicvolume+"");
                    editor.commit();//提交修改
                    break;
                case 91://打开/关闭麦克风
                    if(GlobalVariable.macrophone)
                    {
                        mAudioManager.setMicrophoneMute(false);
                        GlobalVariable.macrophone = false;
                        flashManager.stopflashMute();
                    }
                    else {
                        mAudioManager.setMicrophoneMute(true);
                        GlobalVariable.macrophone = true;
                        flashManager.flashMute();
                    }
                    break;
                case 400://免提
                    //final String[] items = {"Default", "Speaker", "Headset", "Handset"};
                    //    public static final int AUDIO_DEVICE_NONE = 0;
                    //    public static final int AUDIO_DEVICE_SPEAKER = 1;
                    //    public static final int AUDIO_DEVICE_HANDSET = 2;
                    //    public static final int AUDIO_DEVICE_HEADSET = 3;

                    mediaManager.setVoiceAudioDevice(GlobalVariable.audiodevice);
                    if (GlobalVariable.audiodevice == 1) {//切换到话筒
                        flashManager.flashSpeaker();
                        //AudioManagerUtils.setHandsetOn(mAudioManager,true);
                        GlobalVariable.audiodevice = 2;

                    } else {//切换到扬声器
                        //AudioManagerUtils.setSpeakerphoneOn(mAudioManager,true);
                        flashManager.stopflashSpeaker();
                        GlobalVariable.audiodevice = 1;

                    }
                    break;
                case 402://耳机模式
                    if(GlobalVariable.headset)
                    {
                        mediaManager.setVoiceAudioDevice(1);
                        GlobalVariable.headset = false;
                        flashManager.stopflashHeadset();
                    }
                    else {
                        mediaManager.setVoiceAudioDevice(3);
                        GlobalVariable.headset = true;
                        flashManager.flashHeadset();
                    }
                    break;
                case 403:
                    break;
                case 404:
                    final Activity activity = ActivityController.getCurrentActivity();
                    ActivityManager am = (ActivityManager) activity.getApplicationContext().getSystemService(activity.ACTIVITY_SERVICE);
                    ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.MainActivity")) {
                        //判断主界面上当前按钮的状态是否允许操作
                        //允许的话对当前的电话进行保持和取消保持
                    }
                    break;
                case 405://切换到信息界面
                    if(GlobalVariable.isDaLian){
                        new Thread(){
                            @Override
                            public void run() {
                            try {
                                HttpRequest request = new HttpRequest();
                                String rs = request.postJson(
                                        "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getHistoryByUser",
                                        "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                                "\"page\": \"1\", " +
                                                "\"pageSize\": \"1\", " +
                                                "\"pgmid\": \""+GlobalVariable.pgmid+"\", " +
                                                "\"state\": \""+10+"\", " +
                                                "\"operatetype\": \""+1+"\", " +
                                                "\"character\": \""+GlobalVariable.charactername+"\"}"
                                );
                                JSONObject rs_json = new JSONObject(rs);
                                JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
                                List<CallRecord> callrecordlist = JSON.parseArray(rs_json_callrecordlist.toString(), CallRecord.class);
                                if(callrecordlist.size()>0){
                                    //发送消息给MainActivity进行拨号动作
                                    GlobalVariable.setdailvalue = true;
                                    GlobalVariable.autocallout = true;
                                    GlobalVariable.dailvalue = callrecordlist.get(0).callingnumber.substring(2);//外呼的电话号码都要去掉前两位

                                    if(DailActivity.handler_!=null){
                                        Message m2 = Message.obtain(DailActivity.handler_,10021,null);
                                        m2.sendToTarget();
                                    }
                                    if(MainActivity.handler_!=null){
                                        Message m2 = Message.obtain(MainActivity.handler_,1019,null);
                                        m2.sendToTarget();
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            }
                        }.start();
                    }
                    else {
                        Intent intent = new Intent(this, CommunicateActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startActivity(intent);
                    }

                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Log.e("操作日志KeyCode",keyCode+"");
        CommonMediaManager mediaManager = CommonMediaManager.getInstance(getBaseContext());
        CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
        if(keyCode == 401){

            if ((System.currentTimeMillis() - GlobalVariable.pressTime) > 1000) {
                GlobalVariable.pressTime = System.currentTimeMillis();
                //Log.e("操作日志","拿起话筒");
                //1是扬声器，2是话筒
                mediaManager.setVoiceAudioDevice(2);
                flashManager.stopflashSpeaker();

                if(GlobalVariable.isDL1018){

                }
                else {
                    //这边是放下话筒
                    //处理挂断电话逻辑
                    final Activity activity = ActivityController.getCurrentActivity();
                    ActivityManager am = (ActivityManager) activity.getApplicationContext().getSystemService(activity.ACTIVITY_SERVICE);
                    ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                    //当前是在MainActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.MainActivity")) {
                        Message m2 = Message.obtain(MainActivity.handler_, 2001, null);
                        m2.sendToTarget();
                    }
                    //当前是在DailActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.DailActivity")) {
                        Message m2 = Message.obtain(DailActivity.handler_, 2001, null);
                        m2.sendToTarget();
                    }
                    //当前是在HistoryActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.HistoryActivity")) {
                        Message m2 = Message.obtain(HistoryActivity.handler_, 2001, null);
                        m2.sendToTarget();
                    }
                    //当前是在ContactsActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.ContactsActivity")) {
                        Message m2 = Message.obtain(ContactsActivity.handler_, 2001, null);
                        m2.sendToTarget();
                    }
                }
            } else {
                GlobalVariable.pressTime = System.currentTimeMillis();
                if(GlobalVariable.isDL1018){

                }
                else {
                    Toast.makeText(this, "操作太过频繁，稍后再试", Toast.LENGTH_SHORT).show();
                }
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        //Log.e("操作日志KeyCode",keyCode+"");
        CommonMediaManager mediaManager = CommonMediaManager.getInstance(getBaseContext());
        CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
        if(keyCode == 401){
            if ((System.currentTimeMillis() - GlobalVariable.pressTime) > 500) {
                GlobalVariable.pressTime = System.currentTimeMillis();
                //Log.e("操作日志","放下话筒");
                //1是扬声器，2是话筒
                mediaManager.setVoiceAudioDevice(1);
                flashManager.flashSpeaker();

                //这边是拿起话筒
                //处理接听电话逻辑
                if(GlobalVariable.isDL1018){

                }
                else{
                    final Activity activity = ActivityController.getCurrentActivity();
                    ActivityManager am = (ActivityManager) activity.getApplicationContext().getSystemService(activity.ACTIVITY_SERVICE);
                    ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                    //当前是在MainActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.MainActivity")) {
                        Message m2 = Message.obtain(MainActivity.handler_, 2000, null);
                        m2.sendToTarget();
                    }
                    //当前是在DailActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.DailActivity")) {
                        Message m2 = Message.obtain(DailActivity.handler_, 2000, null);
                        m2.sendToTarget();
                    }
                    //当前是在HistoryActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.HistoryActivity")) {
                        Message m2 = Message.obtain(HistoryActivity.handler_, 2000, null);
                        m2.sendToTarget();
                    }
                    //当前是在ContactsActivity
                    if (cn.getClassName().equals("org.pjsip.pjsua2.app.ContactsActivity")) {
                        Message m2 = Message.obtain(ContactsActivity.handler_, 2000, null);
                        m2.sendToTarget();
                    }
                }
            }
            else{
                GlobalVariable.pressTime = System.currentTimeMillis();
                if(GlobalVariable.isDL1018){

                }
                else {
                    Toast.makeText(this, "操作太过频繁，稍后再试", Toast.LENGTH_SHORT).show();
                }
            }




        }

        return super.onKeyUp(keyCode, event);
    }
}


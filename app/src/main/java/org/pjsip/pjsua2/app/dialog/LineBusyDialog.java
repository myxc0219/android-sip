package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.pjsip.pjsua2.app.HttpRequest;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.SettingActivity;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class LineBusyDialog extends Dialog {

    private LineBusyDialog(Context context, int themeResId) {
        super(context, themeResId);
        imContext = context;
    }

    private Context imContext;
    private static TextView tvLineBusy1;
    private static TextView tvLineBusy2;
    private static TextView tvLineBusy3;
    private static TextView tvLineBusy4;
    private static TextView tvLineBusy5;
    private static TextView tvLineBusy6;

    private Button btnLineBusyConfirm;
    private Button btnLineBusyCancel;
    private Button btnLineBusy1T;
    private Button btnLineBusy1F;
    private Button btnLineBusy2T;
    private Button btnLineBusy2F;
    private Button btnLineBusy3T;
    private Button btnLineBusy3F;
    private Button btnLineBusy4T;
    private Button btnLineBusy4F;
    private Button btnLineBusy5T;
    private Button btnLineBusy5F;
    private Button btnLineBusy6T;
    private Button btnLineBusy6F;

    private boolean linebusy1 = false;
    private boolean linebusy2 = false;
    private boolean linebusy3 = false;
    private boolean linebusy4 = false;
    private boolean linebusy5 = false;
    private boolean linebusy6 = false;

    private static TextView ttLineBusy1;
    private static TextView ttLineBusy2;
    private static TextView ttLineBusy3;
    private static TextView ttLineBusy4;
    private static TextView ttLineBusy5;
    private static TextView ttLineBusy6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        GlobalVariable.linebusy_change = false;
        tvLineBusy1 = findViewById(R.id.tvLineBusy1);
        tvLineBusy2 = findViewById(R.id.tvLineBusy2);
        tvLineBusy3 = findViewById(R.id.tvLineBusy3);
        tvLineBusy4 = findViewById(R.id.tvLineBusy4);
        tvLineBusy5 = findViewById(R.id.tvLineBusy5);
        tvLineBusy6 = findViewById(R.id.tvLineBusy6);
        btnLineBusyConfirm = findViewById(R.id.btnLineBusyConfirm);
        btnLineBusyCancel = findViewById(R.id.btnLineBusyCancel);
        btnLineBusy1T = findViewById(R.id.btnLineBusy1T);
        btnLineBusy1F = findViewById(R.id.btnLineBusy1F);
        btnLineBusy2T = findViewById(R.id.btnLineBusy2T);
        btnLineBusy2F = findViewById(R.id.btnLineBusy2F);
        btnLineBusy3T = findViewById(R.id.btnLineBusy3T);
        btnLineBusy3F = findViewById(R.id.btnLineBusy3F);
        btnLineBusy4T = findViewById(R.id.btnLineBusy4T);
        btnLineBusy4F = findViewById(R.id.btnLineBusy4F);
        btnLineBusy5T = findViewById(R.id.btnLineBusy5T);
        btnLineBusy5F = findViewById(R.id.btnLineBusy5F);
        btnLineBusy6T = findViewById(R.id.btnLineBusy6T);
        btnLineBusy6F = findViewById(R.id.btnLineBusy6F);

        ttLineBusy1 = findViewById(R.id.titleLineBusy1);
        ttLineBusy2 = findViewById(R.id.titleLineBusy2);
        ttLineBusy3 = findViewById(R.id.titleLineBusy3);
        ttLineBusy4 = findViewById(R.id.titleLineBusy4);
        ttLineBusy5 = findViewById(R.id.titleLineBusy5);
        ttLineBusy6 = findViewById(R.id.titleLineBusy6);

        if(false){//if(GlobalVariable.viewmodel == 2){
            btnLineBusyConfirm.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            btnLineBusyCancel.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            btnLineBusyConfirm.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
            btnLineBusyCancel.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));

            tvLineBusy1.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            tvLineBusy2.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            tvLineBusy3.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            tvLineBusy4.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            tvLineBusy5.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            tvLineBusy6.setTextColor(imContext.getResources().getColor(R.color.ct_gray));

            ttLineBusy1.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            ttLineBusy2.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            ttLineBusy3.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            ttLineBusy4.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            ttLineBusy5.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
            ttLineBusy6.setTextColor(imContext.getResources().getColor(R.color.ct_gray));
        }



        btnLineBusy1T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy1 = !linebusy1;updateViewByConfig();}
        });
        btnLineBusy1F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy1 = !linebusy1;updateViewByConfig();}
        });
        btnLineBusy2T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy2 = !linebusy2;updateViewByConfig();}
        });
        btnLineBusy2F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy2 = !linebusy2;updateViewByConfig();}
        });
        btnLineBusy3T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy3 = !linebusy3;updateViewByConfig();}
        });
        btnLineBusy3F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy3 = !linebusy3;updateViewByConfig();}
        });
        btnLineBusy4T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy4 = !linebusy4;updateViewByConfig();}
        });
        btnLineBusy4F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy4 = !linebusy4;updateViewByConfig();}
        });
        btnLineBusy5T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy5 = !linebusy5;updateViewByConfig();}
        });
        btnLineBusy5F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy5 = !linebusy5;updateViewByConfig();}
        });
        btnLineBusy6T.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy6 = !linebusy6;updateViewByConfig();}
        });
        btnLineBusy6F.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {linebusy6 = !linebusy6;updateViewByConfig();}
        });

        btnLineBusyCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        btnLineBusyConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //判断配置的线路是不是都是否
                boolean allnotbusy = true;

                if(String.valueOf(tvLineBusy1.getText()).equals("")){}//跳过
                else if(linebusy1) {allnotbusy=false;}
                if(String.valueOf(tvLineBusy2.getText()).equals("")){}//跳过
                else if(linebusy2) {allnotbusy=false;}
                if(String.valueOf(tvLineBusy3.getText()).equals("")){}//跳过
                else if(linebusy3) {allnotbusy=false;}
                if(String.valueOf(tvLineBusy4.getText()).equals("")){}//跳过
                else if(linebusy4) {allnotbusy=false;}
                if(String.valueOf(tvLineBusy5.getText()).equals("")){}//跳过
                else if(linebusy5) {allnotbusy=false;}
                if(String.valueOf(tvLineBusy6.getText()).equals("")){}//跳过
                else if(linebusy6) {allnotbusy=false;}

                if(allnotbusy) {
                    //GlobalVariable.linebusyall = true;
                    //发送消息通知setting页面切换回“否”
                    Message m2 = Message.obtain(SettingActivity.handler_, 1004, null);
                    m2.sendToTarget();
                }

                new Thread(){
                    @Override
                    public void run() {
                        try {
                            if(
                                    GlobalVariable.linebusy1 != linebusy1
                                    || GlobalVariable.linebusy2 != linebusy2
                                    || GlobalVariable.linebusy3 != linebusy3
                                    || GlobalVariable.linebusy4 != linebusy4
                                    || GlobalVariable.linebusy5 != linebusy5
                                    || GlobalVariable.linebusy6 != linebusy6
                            ) {
                                GlobalVariable.linebusy_change = true;
                            }
                            GlobalVariable.linebusy1 = linebusy1;
                            GlobalVariable.linebusy2 = linebusy2;
                            GlobalVariable.linebusy3 = linebusy3;
                            GlobalVariable.linebusy4 = linebusy4;
                            GlobalVariable.linebusy5 = linebusy5;
                            GlobalVariable.linebusy6 = linebusy6;

                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    "http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/updateLineMapping",
                                    "{\"busy1\":\"" + (linebusy1?1:0) + "\"," +
                                            "\"busy2\":\"" + (linebusy2?1:0) + "\"," +
                                            "\"busy3\":\"" + (linebusy3?1:0) + "\"," +
                                            "\"busy4\":\"" + (linebusy4?1:0) + "\"," +
                                            "\"busy5\":\"" + (linebusy5?1:0) + "\"," +
                                            "\"busy6\":\"" + (linebusy6?1:0) + "\"}"
                            );
//                            String lineparam = "";
//                            for(int i=1;i<7;i++){
//                                //如果当路置忙的情况添加到
//                                if(GlobalVariable.ctNumberMapRevertAll.get(i)!=null){
//                                    if(i==1&&GlobalVariable.linebusy1){
//                                        lineparam += ",{\"line\":\""+"01"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//                                    }
//                                    if(i==2&&GlobalVariable.linebusy2){
//                                        lineparam += ",{\"line\":\""+"02"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//                                    }
//                                    if(i==3&&GlobalVariable.linebusy3){
//                                        lineparam += ",{\"line\":\""+"03"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//                                    }
//                                    if(i==4&&GlobalVariable.linebusy4){
//                                        lineparam += ",{\"line\":\""+"04"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//                                    }
//                                    if(i==5&&GlobalVariable.linebusy5){
//                                        lineparam += ",{\"line\":\""+"05"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//                                    }
//                                    if(i==6&&GlobalVariable.linebusy6){
//                                        lineparam += ",{\"line\":\""+"06"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//                                    }
//                                }
//                            }
//                            String msg_to_send = "{ \"msgType\": \"ADD BUSY LINE\", \"BusyLines\": ["+(lineparam.equals("")?"":lineparam.substring(1))+ "] }";
//                            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                        }catch (Exception e){}
                    }
                }.start();
                dismiss();
            }
        });

        tvLineBusy1.setText(GlobalVariable.ctNumberMapRevert.get(1));
        tvLineBusy2.setText(GlobalVariable.ctNumberMapRevert.get(2));
        tvLineBusy3.setText(GlobalVariable.ctNumberMapRevert.get(3));
        tvLineBusy4.setText(GlobalVariable.ctNumberMapRevert.get(4));
        tvLineBusy5.setText(GlobalVariable.ctNumberMapRevert.get(5));
        tvLineBusy6.setText(GlobalVariable.ctNumberMapRevert.get(6));

        linebusy1 = GlobalVariable.linebusy1;
        linebusy2 = GlobalVariable.linebusy2;
        linebusy3 = GlobalVariable.linebusy3;
        linebusy4 = GlobalVariable.linebusy4;
        linebusy5 = GlobalVariable.linebusy5;
        linebusy6 = GlobalVariable.linebusy6;

        updateViewByConfig();
        super.onStart();
    }

    public void updateViewByConfig(){

        //设置背景颜色
        int colorblue = imContext.getResources().getColor(R.color.blue);
        int colorwhite = imContext.getResources().getColor(R.color.white);
        int colorgray = imContext.getResources().getColor(R.color.ct_gray);


        if(GlobalVariable.viewmodel == 1 || GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2){
            btnLineBusy1T.setTextColor(linebusy1?colorblue:colorwhite);
            btnLineBusy1F.setTextColor(linebusy1?colorwhite:colorblue);
            btnLineBusy1T.setBackgroundResource(linebusy1?R.drawable.button_selected:R.drawable.button_selector_thin);
            btnLineBusy1F.setBackgroundResource(linebusy1?R.drawable.button_selector_thin:R.drawable.button_selected);

            btnLineBusy2T.setTextColor(linebusy2?colorblue:colorwhite);
            btnLineBusy2F.setTextColor(linebusy2?colorwhite:colorblue);
            btnLineBusy2T.setBackgroundResource(linebusy2?R.drawable.button_selected:R.drawable.button_selector_thin);
            btnLineBusy2F.setBackgroundResource(linebusy2?R.drawable.button_selector_thin:R.drawable.button_selected);

            btnLineBusy3T.setTextColor(linebusy3?colorblue:colorwhite);
            btnLineBusy3F.setTextColor(linebusy3?colorwhite:colorblue);
            btnLineBusy3T.setBackgroundResource(linebusy3?R.drawable.button_selected:R.drawable.button_selector_thin);
            btnLineBusy3F.setBackgroundResource(linebusy3?R.drawable.button_selector_thin:R.drawable.button_selected);

            btnLineBusy4T.setTextColor(linebusy4?colorblue:colorwhite);
            btnLineBusy4F.setTextColor(linebusy4?colorwhite:colorblue);
            btnLineBusy4T.setBackgroundResource(linebusy4?R.drawable.button_selected:R.drawable.button_selector_thin);
            btnLineBusy4F.setBackgroundResource(linebusy4?R.drawable.button_selector_thin:R.drawable.button_selected);

            btnLineBusy5T.setTextColor(linebusy5?colorblue:colorwhite);
            btnLineBusy5F.setTextColor(linebusy5?colorwhite:colorblue);
            btnLineBusy5T.setBackgroundResource(linebusy5?R.drawable.button_selected:R.drawable.button_selector_thin);
            btnLineBusy5F.setBackgroundResource(linebusy5?R.drawable.button_selector_thin:R.drawable.button_selected);

            btnLineBusy6T.setTextColor(linebusy6?colorblue:colorwhite);
            btnLineBusy6F.setTextColor(linebusy6?colorwhite:colorblue);
            btnLineBusy6T.setBackgroundResource(linebusy6?R.drawable.button_selected:R.drawable.button_selector_thin);
            btnLineBusy6F.setBackgroundResource(linebusy6?R.drawable.button_selector_thin:R.drawable.button_selected);
        }
        else if(false){//else if(GlobalVariable.viewmodel == 2){
            btnLineBusy1T.setTextColor(linebusy1?colorwhite:colorgray);
            btnLineBusy1F.setTextColor(linebusy1?colorgray:colorwhite);
            btnLineBusy1T.setBackgroundResource(linebusy1?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            btnLineBusy1F.setBackgroundResource(linebusy1?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            btnLineBusy2T.setTextColor(linebusy2?colorwhite:colorgray);
            btnLineBusy2F.setTextColor(linebusy2?colorgray:colorwhite);
            btnLineBusy2T.setBackgroundResource(linebusy2?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            btnLineBusy2F.setBackgroundResource(linebusy2?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            btnLineBusy3T.setTextColor(linebusy3?colorwhite:colorgray);
            btnLineBusy3F.setTextColor(linebusy3?colorgray:colorwhite);
            btnLineBusy3T.setBackgroundResource(linebusy3?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            btnLineBusy3F.setBackgroundResource(linebusy3?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            btnLineBusy4T.setTextColor(linebusy4?colorwhite:colorgray);
            btnLineBusy4F.setTextColor(linebusy4?colorgray:colorwhite);
            btnLineBusy4T.setBackgroundResource(linebusy4?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            btnLineBusy4F.setBackgroundResource(linebusy4?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            btnLineBusy5T.setTextColor(linebusy5?colorwhite:colorgray);
            btnLineBusy5F.setTextColor(linebusy5?colorgray:colorwhite);
            btnLineBusy5T.setBackgroundResource(linebusy5?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            btnLineBusy5F.setBackgroundResource(linebusy5?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            btnLineBusy6T.setTextColor(linebusy6?colorwhite:colorgray);
            btnLineBusy6F.setTextColor(linebusy6?colorgray:colorwhite);
            btnLineBusy6T.setBackgroundResource(linebusy6?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            btnLineBusy6F.setBackgroundResource(linebusy6?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);
        }


    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private static LineBusyDialog mDialog;
        private static TextView tvLineBusy1;
        private static TextView tvLineBusy2;
        private static TextView tvLineBusy3;
        private static TextView tvLineBusy4;
        private static TextView tvLineBusy5;
        private static TextView tvLineBusy6;
        private static Button btnLineBusyConfirm;

        private static Button btnLineBusy1T;
        private static Button btnLineBusy1F;
        private static Button btnLineBusy2T;
        private static Button btnLineBusy2F;
        private static Button btnLineBusy3T;
        private static Button btnLineBusy3F;
        private static Button btnLineBusy4T;
        private static Button btnLineBusy4F;
        private static Button btnLineBusy5T;
        private static Button btnLineBusy5F;
        private static Button btnLineBusy6T;
        private static Button btnLineBusy6F;

        public Builder(Context context) {
            mContext = context;
            mDialog = new LineBusyDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_linebusy_config, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

        }


        public LineBusyDialog create() {

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }
}


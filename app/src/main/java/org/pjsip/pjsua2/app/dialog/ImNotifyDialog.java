package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.pjsip.pjsua2.app.CommunicateActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class ImNotifyDialog extends Dialog {

    private ImNotifyDialog(Context context, int themeResId) {
        super(context, themeResId);
        imContext = context;
    }

    private Context imContext;
    private static TextView imtextViewTemp;
    private static TextView imtextViewUser;
    private static TextView imtextViewMsg;
    private static Button imConfirmButton;
    private static Button imReplayButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        imtextViewTemp = findViewById(R.id.imNoifyTemp);
        imtextViewUser = findViewById(R.id.imNoifyUser);
        imtextViewMsg = findViewById(R.id.imNoifyMsg);
        imtextViewTemp.setText(GlobalVariable.im_template);
        imtextViewUser.setText(GlobalVariable.im_username);
        imtextViewMsg.setText(GlobalVariable.im_msg);

        imConfirmButton = findViewById(R.id.imConfirmButton);
        imReplayButton = findViewById(R.id.imReplyButton);
        imConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        imReplayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                Intent intent = new Intent(imContext, CommunicateActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                imContext.startActivity(intent);
            }
        });

        super.onStart();
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;


        private static ImNotifyDialog mDialog;
        private static Button mConfirmButton;
        private static Button mReplayButton;
        private static TextView mtextViewTemp;
        private static TextView mtextViewUser;
        private static TextView mtextViewMsg;


        public Builder(Context context,String msg,String username,String template) {
            mContext = context;
            mDialog = new ImNotifyDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_im_notify, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mtextViewTemp = mLayout.findViewById(R.id.imNoifyTemp);
            mtextViewUser = mLayout.findViewById(R.id.imNoifyUser);
            mtextViewMsg = mLayout.findViewById(R.id.imNoifyMsg);

            mConfirmButton = mLayout.findViewById(R.id.imConfirmButton);
            mReplayButton = mLayout.findViewById(R.id.imReplyButton);


            mtextViewTemp.setText(template);
            mtextViewUser.setText(username);
            mtextViewMsg.setText(msg);

        }


        public ImNotifyDialog create() {
            mConfirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });

            mReplayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                    Intent intent = new Intent(mContext, CommunicateActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mContext.startActivity(intent);
                }
            });


            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }
}

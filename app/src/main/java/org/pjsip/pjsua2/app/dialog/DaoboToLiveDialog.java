package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

public class DaoboToLiveDialog extends Dialog {

    private DaoboToLiveDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;


        private DaoboToLiveDialog mDialog;

        private Button mSaveButton;
        private Button mCancelButton;
        private EditText mAdminPwdText;

        private TextView mtvDialogTitle;


        public Builder(Context context) {
            mContext = context;
            mDialog = new DaoboToLiveDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_force_exit, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mAdminPwdText = mLayout.findViewById(R.id.AdminPwdText);
            mSaveButton = mLayout.findViewById(R.id.serverIpSave);
            mCancelButton = mLayout.findViewById(R.id.serverIpCancel);
            mtvDialogTitle = mLayout.findViewById(R.id.tvDialogTitle);
            mtvDialogTitle.setText("导播切入直播密码");

            if(false){//if(GlobalVariable.viewmodel == 2){
                mSaveButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mSaveButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mCancelButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mCancelButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mtvDialogTitle.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mAdminPwdText.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
            }

        }


        public DaoboToLiveDialog create() {

            mSaveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(String.valueOf(mAdminPwdText.getText()).equals("123")){
                        if(GlobalVariable.daobo_living){
                            //切出直播间
                            Message m2 = Message.obtain(MainActivity.handler_,1022,null);
                            m2.sendToTarget();
                        }
                        else {
                            //切入直播间
                            Message m2 = Message.obtain(MainActivity.handler_,1021,null);
                            m2.sendToTarget();
                        }

                    }
                    mDialog.dismiss();
                }
            });

            mCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }
}



package org.pjsip.pjsua2.app.httpapi;

import android.os.Message;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yanzhenjie.andserver.annotation.PostMapping;
import com.yanzhenjie.andserver.annotation.RequestBody;
import com.yanzhenjie.andserver.annotation.RestController;

import org.json.JSONArray;
import org.pjsip.pjsua2.app.HttpRequest;
import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.SettingActivity;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.CallInformation;
import org.pjsip.pjsua2.app.model.CallOperate;
import org.pjsip.pjsua2.app.model.LinebusyConfig;
import org.pjsip.pjsua2.app.model.Pstn;
import org.pjsip.pjsua2.app.model.SipConfig;
import org.pjsip.pjsua2.app.util.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class HttpController {

    public static int busyall = 0;
//    @PostMapping("/user/login")
//    String login(@RequestParam("account") String account, @RequestParam("password") String password) {
//        if("123".equals(account) && "123".equals(password)) {
//            return "Login successful.";
//        } else {
//            return "Login failed.";
//        }
//    }
    @PostMapping("/query-calls")
    String querycalls(@RequestBody String jsonString) {
        List<CallInformation> callInformationList = new ArrayList<>();
        for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
            CallInformation callInformation = new CallInformation();
            if(callws.dailout){
                callInformation.outcall = callws.pstn.substring(2);
            }
            else{
                callInformation.outcall = callws.pstn;
            }
            callInformation.contact = callws.callednumber;
            callInformation.daobo = GlobalVariable.username;
            callInformation.recordpath = "http://"+GlobalVariable.SIPSERVER+":8080/record/"+callws.uid+".wav";
            callInformation.callintime = callws.callintime;
            if(callws.type == MainActivity.CALL_TYPE.DIALINIT){
                callInformation.status = "UNKNOWN";
            }
            else if(callws.type == MainActivity.CALL_TYPE.NEWINIT){
                callInformation.status = "CALLING";
            }
            else if(callws.type == MainActivity.CALL_TYPE.DAOBOACPT){
                callInformation.status = "DAOBO_ANSWER";
            }
            else if(callws.type == MainActivity.CALL_TYPE.DAOBOHOLD){
                callInformation.status = "DAOBO_HOLD";
            }
            else if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO){
                callInformation.status = "DAOBO_TO_ZHUBO";
            }
            else if(callws.type == MainActivity.CALL_TYPE.ZHUBOACPT){
                callInformation.status = "ZHUBO_ANSWER";
            }
            else if(callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD){
                callInformation.status = "ZHUBO_HOLD";
            }
            else if(callws.type == MainActivity.CALL_TYPE.WAITDAOBO){
                callInformation.status = "ZHUBO_TO_DAOBO";
            }
            else if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                callInformation.status = "LIVING";
            }
            else if(callws.type == MainActivity.CALL_TYPE.LIVEHOLD){
                callInformation.status = "LIVING_HOLD";
            }
            callInformationList.add(callInformation);
        }
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("calls",callInformationList);
        resultMap.put("code",200);
        return JsonUtils.toJson(resultMap);
    }
    @PostMapping("/query-configs")
    String queryconfigs(@RequestBody String jsonString) {
        SipConfig sipConfig = new SipConfig();
        sipConfig.sipmodel = GlobalVariable.sipmodel.toString();
        sipConfig.ip = GlobalVariable.localip;
        sipConfig.role = GlobalVariable.charactername.equals("director")?"daobo":"zhubo";
        sipConfig.sipid = GlobalVariable.sipid;
        sipConfig.setbusy = GlobalVariable.busyall == 0?false:true;
        sipConfig.lines =
                GlobalVariable.ctNumberMapRevert.get(1) + ","
                +GlobalVariable.ctNumberMapRevert.get(2) + ","
                +GlobalVariable.ctNumberMapRevert.get(3) + ","
                +GlobalVariable.ctNumberMapRevert.get(4) + ","
                +GlobalVariable.ctNumberMapRevert.get(5) + ","
                +GlobalVariable.ctNumberMapRevert.get(6);
        sipConfig.linebusy = (GlobalVariable.linebusy1?"true":"false") + ","
                            +(GlobalVariable.linebusy2?"true":"false") + ","
                            +(GlobalVariable.linebusy3?"true":"false") + ","
                            +(GlobalVariable.linebusy4?"true":"false") + ","
                            +(GlobalVariable.linebusy5?"true":"false") + ","
                            +(GlobalVariable.linebusy6?"true":"false") ;

        sipConfig.dialforbid = (GlobalVariable.getCtNumberMapRevertForbid.get(1)==null?"true":"false") + ","
                            +(GlobalVariable.getCtNumberMapRevertForbid.get(2)==null?"true":"false") + ","
                            +(GlobalVariable.getCtNumberMapRevertForbid.get(3)==null?"true":"false") + ","
                            +(GlobalVariable.getCtNumberMapRevertForbid.get(4)==null?"true":"false") + ","
                            +(GlobalVariable.getCtNumberMapRevertForbid.get(5)==null?"true":"false") + ","
                            +(GlobalVariable.getCtNumberMapRevertForbid.get(6)==null?"true":"false") ;

        sipConfig.busymodel = GlobalVariable.busyall;
        sipConfig.pgmname = GlobalVariable.channelname;
        sipConfig.code = 200;
        return JsonUtils.toJson(sipConfig);
    }
    @PostMapping("/daobo-answer")
    String daoboanswer(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director")){
            if(GlobalVariable.sipmodel == 0){
                //无导播模式
                callOperate.code = 403;
            }
            else if(GlobalVariable.sipmodel == 1){
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                                || callws.type == MainActivity.CALL_TYPE.DAOBOHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3000,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            else if(GlobalVariable.sipmodel == 2){
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                                || callws.type == MainActivity.CALL_TYPE.DAOBOHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3000,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            else if(GlobalVariable.sipmodel == 3){
                //仅导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                                || callws.type == MainActivity.CALL_TYPE.DAOBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3000,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
        }
        else{
            callOperate.code = 403;
        }

        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/daobo-hangup")
    String daobohangup(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        Message m = Message.obtain(MainActivity.handler_, 3001, callws.uid);
                        m.sendToTarget();
                        callOperate.code = 200;
                        return JsonUtils.toJson(callOperate);
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        Message m = Message.obtain(MainActivity.handler_, 3001, callws.uid);
                        m.sendToTarget();
                        callOperate.code = 200;
                        return JsonUtils.toJson(callOperate);
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        Message m = Message.obtain(MainActivity.handler_, 3001, callws.uid);
                        m.sendToTarget();
                        callOperate.code = 200;
                        return JsonUtils.toJson(callOperate);
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
        }
        else{
            callOperate.code = 403;
        }
        new Thread() {
            @Override
            public void run() {
                try {
                    String call_status = "ENDCALL";
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            GlobalVariable.SEHTTPServer + "/calltypechange",
                            "{" +
                                    "\"recordpath\":\"\"," +
                                    "\"outcall\":\"" + outcall + "\"," +
                                    "\"contact\":\""  + "\"," +
                                    "\"status\":\"" + call_status + "\"," +
                                    "\"callintime\":\"\"," +
                                    "\"code\":" + 200 + "}"
                    );
                } catch (Exception e) {
                    //e.printStackTrace();
                    Log.e("SEHTTPServer","指令异常");
                }
            }
        }.start();
        callOperate.code = 200;
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/daobo-hold")
    String daobohold(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                callOperate.code = 403;
            }
            else if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3002, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            else if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3002, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            else if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3002, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
        }
        else {
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/daobo-to-zhubo")
    String daobotozhubo(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3003, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3003, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else {
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/daobo-cancel-switch")
    String daobocancelswitch(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;

        if(GlobalVariable.charactername.equals("director")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3004, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                for (MainActivity.MyCallWithState callws : MainActivity.allCallinList) {
                    String pstn = callws.dailout ? callws.pstn.substring(2) : callws.pstn;
                    if (pstn.equals(outcall)) {//外线号码一致
                        if (callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                        ) {
                            Message m = Message.obtain(MainActivity.handler_, 3004, callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        } else {
                            callOperate.code = 403;
                        }
                    } else {
                        callOperate.code = 403;
                    }
                }
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else {
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/daobo-make-call")
    String daobomakecall(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        String contact = jsonObject.get("contact").toString();
        List<String> CallParam = new ArrayList<>();
        CallParam.add(outcall);
        CallParam.add(contact);
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;

        boolean contact_correct = false;
        int line = -1;
        for(int i = 1;i<7;i++){
            //当前线是否是注册线路
            boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);
            //当前线是否被已有电话占用
            boolean line_occupy = false;
//            try {
//                int id_index = GlobalVariable.ctIdMap.get(i);
//                MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//                line_occupy = true;
//            }catch (Exception e){
//                e.printStackTrace();
//                line_occupy = false;
//            }
            if(GlobalVariable.isDaLian){
                //判断外线号码和当前号码相同判断线路被占用
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                        line_occupy = true;
                    }
                }
            }
            if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
                line_occupy = true;
            }
            if(line_exist && !line_occupy){
                line = i;
                if(contact.equals(GlobalVariable.ctNumberMapRevert.get(line))){
                    contact_correct = true;
                }
            }
        }

        if(!contact_correct){
            callOperate.code = 403;
        }
        else if(GlobalVariable.charactername.equals("director")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                Message m = Message.obtain(MainActivity.handler_, 3005, CallParam);
                m.sendToTarget();
                callOperate.code = 200;
                return JsonUtils.toJson(callOperate);
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                Message m = Message.obtain(MainActivity.handler_, 3005, CallParam);
                m.sendToTarget();
                callOperate.code = 200;
                return JsonUtils.toJson(callOperate);
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                Message m = Message.obtain(MainActivity.handler_, 3005, CallParam);
                m.sendToTarget();
                callOperate.code = 200;
                return JsonUtils.toJson(callOperate);
            }
        }
        else {
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/zhubo-answer")
    String zhuboanswer(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("broadcaster")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3006,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3006,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3006,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else {
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/zhubo-hold")
    String zhubohold(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("broadcaster")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.ZHUBOACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3007,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else {
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/zhubo-to-daobo")
    String zhubotodaobo(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director"))
        {
            //兼容导播控制主播（无主播盒）
            if(GlobalVariable.sipmodel == 1) {
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3008,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 2) {
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3008,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
        }
        else if(GlobalVariable.charactername.equals("broadcaster")) {
            if(GlobalVariable.sipmodel == 0){
                //无导播模式
                callOperate.code = 403;
            }
            if(GlobalVariable.sipmodel == 1) {
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3008,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 2) {
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.LIVEACPT
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3008,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else{
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/to-living")
    String toliving(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director")){
            if(GlobalVariable.sipmodel == 0){
                //无导播模式
                callOperate.code = 403;
            }
            if(GlobalVariable.sipmodel == 1){
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 2){
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                                || callws.type == MainActivity.CALL_TYPE.WAITDAOBO
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 3){
                //仅导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.DAOBOACPT
                                || callws.type == MainActivity.CALL_TYPE.DAOBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
        }
        else if(GlobalVariable.charactername.equals("broadcaster")){
            if(GlobalVariable.sipmodel == 0){
                //无导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.NEWINIT
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOACPT
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 1){
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 2){
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.WAITZHUBO
                                || callws.type == MainActivity.CALL_TYPE.ZHUBOHOLD
                                || callws.type == MainActivity.CALL_TYPE.LIVEHOLD
                        ){
                            Message m = Message.obtain(MainActivity.handler_,3009,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 3){
                //仅导播模式
                callOperate.code = 403;
            }
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/living-hold")
    String livinghold(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;
        if(GlobalVariable.charactername.equals("director")){
            if(GlobalVariable.sipmodel == 0){
                //无导播模式
                //callOperate.code = 403;
                //兼容导播同时控制主播（无主播盒）
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 1){
                //单导播模式
                //callOperate.code = 403;
                //兼容导播同时控制主播（无主播盒）
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 2){
                //多导播模式
                //callOperate.code = 403;
                //兼容导播同时控制主播（无主播盒）
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 3){
                //仅导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
        }
        else if(GlobalVariable.charactername.equals("broadcaster")){
            if(GlobalVariable.sipmodel == 0){
                //无导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 1){
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 2){
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        if(callws.type == MainActivity.CALL_TYPE.LIVEACPT){
                            Message m = Message.obtain(MainActivity.handler_,3010,callws.uid);
                            m.sendToTarget();
                            callOperate.code = 200;
                            return JsonUtils.toJson(callOperate);
                        }
                        else{
                            callOperate.code = 403;
                        }
                    }
                }
            }
            if(GlobalVariable.sipmodel == 3){
                //仅导播模式
                callOperate.code = 403;
            }
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/zhubo-makecall")
    String zhubomakecall(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        String contact = jsonObject.get("contact").toString();
        List<String> CallParam = new ArrayList<>();
        CallParam.add(outcall);
        CallParam.add(contact);
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;

        boolean contact_correct = false;
        int line = -1;
        for(int i = 1;i<7;i++){
            //当前线是否是注册线路
            boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);
            //当前线是否被已有电话占用
            boolean line_occupy = false;
//            try {
//                int id_index = GlobalVariable.ctIdMap.get(i);
//                MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//                line_occupy = true;
//            }catch (Exception e){
//                e.printStackTrace();
//                line_occupy = false;
//            }
            if(GlobalVariable.isDaLian){
                //判断外线号码和当前号码相同判断线路被占用
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                        line_occupy = true;
                    }
                }
            }
            if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
                line_occupy = true;
            }
            if(line_exist && !line_occupy){
                line = i;
                if(contact.equals(GlobalVariable.ctNumberMapRevert.get(line))){
                    contact_correct = true;
                }
            }
        }

        if(!contact_correct){
            callOperate.code = 403;
        }
        if(GlobalVariable.charactername.equals("broadcaster")) {
            if (GlobalVariable.sipmodel == 0) {
                //无导播模式
                Message m = Message.obtain(MainActivity.handler_, 3005, CallParam);
                m.sendToTarget();
                callOperate.code = 200;
                return JsonUtils.toJson(callOperate);
            }
            if (GlobalVariable.sipmodel == 1) {
                //单导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 2) {
                //多导播模式
                callOperate.code = 403;
            }
            if (GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else{
            callOperate.code = 403;
        }
        return JsonUtils.toJson(callOperate);
    }
    @PostMapping("/zhubo-hangup")
    String zhubohangup(@RequestBody String jsonString){
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String outcall = jsonObject.get("outcall").toString();
        CallOperate callOperate = new CallOperate();
        callOperate.outcall = outcall;
        callOperate.code = 403;

        if(GlobalVariable.charactername.equals("broadcaster") || GlobalVariable.charactername.equals("director")) {
            if(GlobalVariable.sipmodel == 0) {
                //无导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        Message m = Message.obtain(MainActivity.handler_, 3012, callws.uid);
                        m.sendToTarget();
                        callOperate.code = 200;
                        return JsonUtils.toJson(callOperate);
                    }
                    else {
                        callOperate.code = 403;
                    }
                }
            }
            else if(GlobalVariable.sipmodel == 1){
                //单导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        Message m = Message.obtain(MainActivity.handler_, 3012, callws.uid);
                        m.sendToTarget();
                        callOperate.code = 200;
                        return JsonUtils.toJson(callOperate);
                    }
                    else {
                        callOperate.code = 403;
                    }
                }
            }
            else if(GlobalVariable.sipmodel == 2){
                //多导播模式
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    String pstn = callws.dailout?callws.pstn.substring(2):callws.pstn;
                    if(pstn.equals(outcall)){//外线号码一致
                        Message m = Message.obtain(MainActivity.handler_, 3012, callws.uid);
                        m.sendToTarget();
                        callOperate.code = 200;
                        return JsonUtils.toJson(callOperate);
                    }
                    else {
                        callOperate.code = 403;
                    }
                }
            }
            else if(GlobalVariable.sipmodel == 3) {
                //仅导播模式
                callOperate.code = 403;
            }
        }
        else{
            callOperate.code = 403;
        }
        new Thread() {
            @Override
            public void run() {
                try {
                    String call_status = "ENDCALL";
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            GlobalVariable.SEHTTPServer + "/calltypechange",
                            "{" +
                                    "\"recordpath\":\"\"," +
                                    "\"outcall\":\"" + outcall + "\"," +
                                    "\"contact\":\""  + "\"," +
                                    "\"status\":\"" + call_status + "\"," +
                                    "\"callintime\":\"\"," +
                                    "\"code\":" + 200 + "}"
                    );
                } catch (Exception e) {
                    //e.printStackTrace();
                    Log.e("SEHTTPServer","指令异常");
                }
            }
        }.start();
        callOperate.code = 200;
        return JsonUtils.toJson(callOperate);
    }

    //20220330 新增设置控制协议（全部置忙,线路置忙,全部取消置忙,线路取消置忙）
    @PostMapping("/linebusy")
    String linebusy(@RequestBody String jsonString){
        new Thread() {
            @Override
            public void run() {
                try {
                    //20230222修正控制协议和纬视逻辑一致，当传入参数中全为置忙或全为不置忙时，本机也切换到全忙/取消
                    JSONObject jsonObject =JSONObject.parseObject(jsonString);
                    String line1 = jsonObject.get("line1").toString();
                    String line2 = jsonObject.get("line2").toString();
                    String line3 = jsonObject.get("line3").toString();
                    String line4 = jsonObject.get("line4").toString();
                    String line5 = jsonObject.get("line5").toString();
                    String line6 = jsonObject.get("line6").toString();

                    int linebusy1 = -1;
                    int linebusy2 = -1;
                    int linebusy3 = -1;
                    int linebusy4 = -1;
                    int linebusy5 = -1;
                    int linebusy6 = -1;

                    //设置线路数
                    //busyall = 0;不置忙   busyall = 1;全部置忙    busyall = 2;分路置忙
                    if(line1.equals("true")){linebusy1 = 1;}else if(line1.equals("false")){linebusy1 = 0;}
                    if(line2.equals("true")){linebusy2 = 1;}else if(line2.equals("false")){linebusy2 = 0;}
                    if(line3.equals("true")){linebusy3 = 1;}else if(line3.equals("false")){linebusy3 = 0;}
                    if(line4.equals("true")){linebusy4 = 1;}else if(line4.equals("false")){linebusy4 = 0;}
                    if(line5.equals("true")){linebusy5 = 1;}else if(line5.equals("false")){linebusy5 = 0;}
                    if(line6.equals("true")){linebusy6 = 1;}else if(line6.equals("false")){linebusy6 = 0;}

                    boolean all_zero = false;
                    boolean all_one = false;

                    if(linebusy1 == 1) {
                        //设置了第一路，设置的是置忙，看其他所有的线路不为空的是否是置忙，是，则置全忙
                        if( (GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2)==null)
                                && (GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3)==null)
                                && (GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4)==null)
                                && (GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5)==null)
                                && (GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6)==null)
                                && linebusy2 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ){ all_one = true; }
                    }
                    else if(linebusy1 == 0) {
                        if ((!GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2) == null)
                                && (!GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3) == null)
                                && (!GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4) == null)
                                && (!GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5) == null)
                                && (!GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6) == null)
                                && linebusy2 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ) { all_zero = true; }
                    }

                    if(linebusy2 == 1) {
                        //设置了第一路，设置的是置忙，看其他所有的线路不为空的是否是置忙，是，则置全忙
                        if( (GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1)==null)
                                && (GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3)==null)
                                && (GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4)==null)
                                && (GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5)==null)
                                && (GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6)==null)
                                && linebusy1 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ){ all_one = true; }
                    }
                    else if(linebusy2 == 0) {
                        if ((!GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1) == null)
                                && (!GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3) == null)
                                && (!GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4) == null)
                                && (!GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5) == null)
                                && (!GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6) == null)
                                && linebusy1 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ) { all_zero = true; }
                    }

                    if(linebusy3 == 1) {
                        //设置了第一路，设置的是置忙，看其他所有的线路不为空的是否是置忙，是，则置全忙
                        if( (GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2)==null)
                                && (GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1)==null)
                                && (GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4)==null)
                                && (GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5)==null)
                                && (GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6)==null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy4 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ){ all_one = true; }
                    }
                    else if(linebusy3 == 0) {
                        if ((!GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2) == null)
                                && (!GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1) == null)
                                && (!GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4) == null)
                                && (!GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5) == null)
                                && (!GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6) == null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy4 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ) { all_zero = true; }
                    }

                    if(linebusy4 == 1) {
                        //设置了第一路，设置的是置忙，看其他所有的线路不为空的是否是置忙，是，则置全忙
                        if( (GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2)==null)
                                && (GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3)==null)
                                && (GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1)==null)
                                && (GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5)==null)
                                && (GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6)==null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy3 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ){ all_one = true; }
                    }
                    else if(linebusy4 == 0) {
                        if ((!GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2) == null)
                                && (!GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3) == null)
                                && (!GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1) == null)
                                && (!GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5) == null)
                                && (!GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6) == null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy3 == -1 && linebusy5 == -1 && linebusy6 == -1
                        ) { all_zero = true; }
                    }

                    if(linebusy5 == 1) {
                        //设置了第一路，设置的是置忙，看其他所有的线路不为空的是否是置忙，是，则置全忙
                        if( (GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2)==null)
                                && (GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3)==null)
                                && (GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4)==null)
                                && (GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1)==null)
                                && (GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6)==null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy6 == -1
                        ){ all_one = true; }
                    }
                    else if(linebusy5 == 0) {
                        if ((!GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2) == null)
                                && (!GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3) == null)
                                && (!GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(2) == null)
                                && (!GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1) == null)
                                && (!GlobalVariable.linebusy6 || GlobalVariable.ctNumberMapRevert.get(6) == null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy6 == -1
                        ) { all_zero = true; }
                    }

                    if(linebusy6 == 1) {
                        //设置了第一路，设置的是置忙，看其他所有的线路不为空的是否是置忙，是，则置全忙
                        if( (GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2)==null)
                                && (GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3)==null)
                                && (GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4)==null)
                                && (GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5)==null)
                                && (GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1)==null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy5 == -1
                        ){ all_one = true; }
                    }
                    else if(linebusy6 == 0) {
                        if ((!GlobalVariable.linebusy2 || GlobalVariable.ctNumberMapRevert.get(2) == null)
                                && (!GlobalVariable.linebusy3 || GlobalVariable.ctNumberMapRevert.get(3) == null)
                                && (!GlobalVariable.linebusy4 || GlobalVariable.ctNumberMapRevert.get(4) == null)
                                && (!GlobalVariable.linebusy5 || GlobalVariable.ctNumberMapRevert.get(5) == null)
                                && (!GlobalVariable.linebusy1 || GlobalVariable.ctNumberMapRevert.get(1) == null)
                                && linebusy1 == -1 && linebusy2 == -1 && linebusy3 == -1 && linebusy4 == -1 && linebusy5 == -1
                        ) { all_zero = true; }
                    }

                    if(linebusy1 == 1 && linebusy2 == 1 && linebusy3 == 1 && linebusy4 == 1 && linebusy5 == 1 && linebusy6 == 1){
                        all_one = true; all_zero = false;
                    }
                    else if(linebusy1 == 0 && linebusy2 == 0 && linebusy3 == 0 && linebusy4 == 0 && linebusy5 == 0 && linebusy6 == 0){
                        all_zero = true; all_one = false;
                    }
                    if(line1.equals("true")){GlobalVariable.linebusy1 = true;}else if(line1.equals("false")){GlobalVariable.linebusy1 = false;}
                    if(line2.equals("true")){GlobalVariable.linebusy2 = true;}else if(line2.equals("false")){GlobalVariable.linebusy2 = false;}
                    if(line3.equals("true")){GlobalVariable.linebusy3 = true;}else if(line3.equals("false")){GlobalVariable.linebusy3 = false;}
                    if(line4.equals("true")){GlobalVariable.linebusy4 = true;}else if(line4.equals("false")){GlobalVariable.linebusy4 = false;}
                    if(line5.equals("true")){GlobalVariable.linebusy5 = true;}else if(line5.equals("false")){GlobalVariable.linebusy5 = false;}
                    if(line6.equals("true")){GlobalVariable.linebusy6 = true;}else if(line6.equals("false")){GlobalVariable.linebusy6 = false;}

                    busyall = 2;
                    if(all_zero) {
                        busyall = 0;
                        GlobalVariable.linebusy1 = GlobalVariable.linebusy2
                            = GlobalVariable.linebusy3 = GlobalVariable.linebusy4
                            = GlobalVariable.linebusy5 = GlobalVariable.linebusy6 = false;
                    }

                    if(all_one) {
                        busyall = 1;
                        GlobalVariable.linebusy1 = GlobalVariable.linebusy2
                                = GlobalVariable.linebusy3 = GlobalVariable.linebusy4
                                = GlobalVariable.linebusy5 = GlobalVariable.linebusy6 = true;
                    }

//                    if(linebusy1 && linebusy2 && linebusy3 && linebusy4 && linebusy5 && linebusy6)
//                    {
//                        busyall = 1;
//                    }
//                    boolean allnotbusy = false;
//                    if(!linebusy1 && !linebusy2 && !linebusy3 && !linebusy4 && !linebusy5 && !linebusy6 )
//                    {
//                        allnotbusy = true;
//                        busyall = 0;
//                    }
//                    if(allnotbusy) {
                    if(all_zero) {
                        //GlobalVariable.linebusyall = true;
                        //发送消息通知setting页面切换回“否”
                        if(SettingActivity.handler_!=null) {
                            Message m2 = Message.obtain(SettingActivity.handler_, 1004, null);
                            m2.sendToTarget();
                        }
                    }
                    GlobalVariable.send_to_other_content = "";
                    if((GlobalVariable.busyall!=busyall || GlobalVariable.linebusy_change )&& busyall == 2)
                    {
                        GlobalVariable.send_to_other_content = "[分路置忙]\r\n";
                        new Thread() {
                            @Override
                            public void run() {
                                try {
                                    HttpRequest request = new HttpRequest();
                                    String rs = request.postJson(
                                            GlobalVariable.SEHTTPServer + "/setbusy",
                                            "{\"code\":" + 200 + "}"
                                    );
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    }
                    else if(GlobalVariable.busyall!=busyall)
                    {
                        if(busyall == 1) {
                            GlobalVariable.send_to_other_content = "[置忙]\r\n";
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                GlobalVariable.SEHTTPServer + "/setbusy",
                                                "{\"code\":" + 200 + "}"
                                        );
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }
                        if(busyall == 0){
                            GlobalVariable.send_to_other_content ="[取消置忙]\r\n";
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                GlobalVariable.SEHTTPServer + "/unsetbusy",
                                                "{\"code\":" + 200 + "}"
                                        );
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();
                        }
                    }
                    if(!GlobalVariable.send_to_other_content.equals(""))
                        GlobalVariable.send_to_other_config = true;
                    else
                        GlobalVariable.send_to_other_config = false;

                    //为参数设置临时值
                    String busy_value1 = "";
                    String busy_value2 = "";
                    String busy_value3 = "";
                    String busy_value4 = "";
                    String busy_value5 = "";
                    String busy_value6 = "";
                    if(!line1.equals("")){busy_value1 = (linebusy1 == 1?"1":"0");}
                    if(!line2.equals("")){busy_value2 = (linebusy2 == 1?"1":"0");}
                    if(!line3.equals("")){busy_value3 = (linebusy3 == 1?"1":"0");}
                    if(!line4.equals("")){busy_value4 = (linebusy4 == 1?"1":"0");}
                    if(!line5.equals("")){busy_value5 = (linebusy5 == 1?"1":"0");}
                    if(!line6.equals("")){busy_value6 = (linebusy6 == 1?"1":"0");}

                    GlobalVariable.busyall = busyall;

                    if(busyall == 0)
                    {
                        busy_value1 = busy_value2 = busy_value3 = busy_value4 = busy_value5 = busy_value6 = "0";
                    }
                    else if(busyall == 1)
                    {
                        busy_value1 = busy_value2 = busy_value3 = busy_value4 = busy_value5 = busy_value6 = "1";
                    }

                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/updateLineMapping",
                            "{" +
                                    "\"busy1\":\"" + busy_value1 + "\"," +
                                    "\"busy2\":\"" + busy_value2 + "\"," +
                                    "\"busy3\":\"" + busy_value3 + "\"," +
                                    "\"busy4\":\"" + busy_value4 + "\"," +
                                    "\"busy5\":\"" + busy_value5 + "\"," +
                                    "\"busy6\":\"" + busy_value6 + "\"}"
                    );

                    //禁止在此修改未传入的linebusy变量(20230222修改，强制转换状态 在三种主模式下切换)



                    HttpRequest requestOut = new HttpRequest();
                    String rsOut = requestOut.postJson(
                            GlobalVariable.SEHTTPServer + "/linebusy",
                            "{\"code\":" + 200 + ","
                                    +"\"linebusy\": \""
                                    +(GlobalVariable.linebusy1?"true":"false") + ","
                                    +(GlobalVariable.linebusy2?"true":"false") + ","
                                    +(GlobalVariable.linebusy3?"true":"false") + ","
                                    +(GlobalVariable.linebusy4?"true":"false") + ","
                                    +(GlobalVariable.linebusy5?"true":"false") + ","
                                    +(GlobalVariable.linebusy6?"true":"false") + "\","
                                    +"\"busymodel\": "+ GlobalVariable.busyall
                                    + "}"
                    );

                    HttpRequest request1 = new HttpRequest();
                    String rs1 = request1.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/updateSysConfig",
                            "{\"userid\":\""+GlobalVariable.userid+"\"," +
                                    "\"pgmid\":\"" + GlobalVariable.pgmid+"\","+
                                    "\"busystate\": " + (GlobalVariable.busystate?'1':'0') +","+
                                    "\"callnum\": " + (GlobalVariable.callnum==null?0:GlobalVariable.callnum) +","+
                                    "\"waitnum\": " + (GlobalVariable.waitnum==null? 0:GlobalVariable.waitnum) +","+
                                    "\"online\": " + (GlobalVariable.online?'1':'0') +","+
                                    "\"callring\": " +(GlobalVariable.callring?'1':'0') +","+
                                    "\"communicatering\": " +(GlobalVariable.communicatering?'1':'0')+","+
                                    "\"allowblacklist\": " +(GlobalVariable.allowblacklist?'1':'0')+","+
                                    "\"whitelistmodel\": " +(GlobalVariable.whitelistmodel?'1':'0')+","+
                                    "\"busyall\": " +(GlobalVariable.busyall)+","+
                                    "\"viewmodel\": " +(GlobalVariable.viewmodel)+","+//特殊情况
                                    "\"sipmodel\": " +GlobalVariable.sipmodel+","+
                                    "\"autoswitch\": " +(GlobalVariable.autoswitch?'1':'0')+","+
                                    "}"
                    );

                    if(SettingActivity.handler_!=null){
                        Message m2 = Message.obtain(SettingActivity.handler_,1002,null);
                        m2.sendToTarget();
                    }
                    if(GlobalVariable.send_to_other_config) {
                        String msg_to_send = "{ " +
                                "\"msgType\": \"CHANGE SYS CONFIG\"," +
                                "\"config\": \"" + GlobalVariable.send_to_other_content + "\"," +
                                "\"state\": \"1\"," +
                                "\"local\": \"\"" +
                                "}";
                        if (MainActivity.wsControl != null)
                            MainActivity.wsControl.sendMessage(msg_to_send);
                    }

                } catch (Exception e) {e.printStackTrace();}
            }
        }.start();

        LinebusyConfig linebusyConfig = new LinebusyConfig();
        linebusyConfig.code = 200;

        return JsonUtils.toJson(linebusyConfig);
    }

    @PostMapping("/linebusyall")
    String linebusyall(@RequestBody String jsonString){
        new Thread() {
            @Override
            public void run() {
                try {
                    JSONObject jsonObject =JSONObject.parseObject(jsonString);
                    String busy_order = jsonObject.get("busyall").toString();
                    if(busy_order.equals("false")){
                        busyall = 0;
                        GlobalVariable.linebusy1 = GlobalVariable.linebusy2
                                = GlobalVariable.linebusy3 = GlobalVariable.linebusy4
                                = GlobalVariable.linebusy5 = GlobalVariable.linebusy6 = false;
                        //发送消息通知setting页面切换回“否”
                        if(SettingActivity.handler_!=null) {
                            Message m2 = Message.obtain(SettingActivity.handler_, 1004, null);
                            m2.sendToTarget();
                        }

                    }else if(busy_order.equals("true")){
                        busyall = 1;
                        GlobalVariable.linebusy1 = GlobalVariable.linebusy2
                                = GlobalVariable.linebusy3 = GlobalVariable.linebusy4
                                = GlobalVariable.linebusy5 = GlobalVariable.linebusy6 = true;
                        if(SettingActivity.handler_!=null) {
                            Message m2 = Message.obtain(SettingActivity.handler_, 1004, null);
                            m2.sendToTarget();
                        }
                    }

                    if(GlobalVariable.busyall!=busyall)
                    {
                        if(busyall == 1) {
                            GlobalVariable.send_to_other_content = "[置忙]\r\n";
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                GlobalVariable.SEHTTPServer + "/setbusy",
                                                "{\"code\":" + 200 + "}"
                                        );
                                    } catch (Exception e) {e.printStackTrace();}
                                }
                            }.start();
                        }
                        if(busyall == 0){
                            GlobalVariable.send_to_other_content ="[取消置忙]\r\n";
                            new Thread() {
                                @Override
                                public void run() {
                                    try {
                                        HttpRequest request = new HttpRequest();
                                        String rs = request.postJson(
                                                GlobalVariable.SEHTTPServer + "/unsetbusy",
                                                "{\"code\":" + 200 + "}"
                                        );
                                    } catch (Exception e) {e.printStackTrace();}
                                }
                            }.start();
                        }

                        if(!GlobalVariable.send_to_other_content.equals(""))
                            GlobalVariable.send_to_other_config = true;
                        else
                            GlobalVariable.send_to_other_config = false;

                        //为参数设置临时值
                        String busy_value1 = "";
                        String busy_value2 = "";
                        String busy_value3 = "";
                        String busy_value4 = "";
                        String busy_value5 = "";
                        String busy_value6 = "";

                        GlobalVariable.busyall = busyall;

                        if(busyall == 0)
                        {
                            busy_value1 = busy_value2 = busy_value3 = busy_value4 = busy_value5 = busy_value6 = "0";
                        }
                        else if(busyall == 1)
                        {
                            busy_value1 = busy_value2 = busy_value3 = busy_value4 = busy_value5 = busy_value6 = "1";
                        }

                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/updateLineMapping",
                                "{" +
                                        "\"busy1\":\"" + busy_value1 + "\"," +
                                        "\"busy2\":\"" + busy_value2 + "\"," +
                                        "\"busy3\":\"" + busy_value3 + "\"," +
                                        "\"busy4\":\"" + busy_value4 + "\"," +
                                        "\"busy5\":\"" + busy_value5 + "\"," +
                                        "\"busy6\":\"" + busy_value6 + "\"}"
                        );

                        //禁止在此修改未传入的linebusy变量(20230222修改，强制转换状态 在三种主模式下切换)
                        HttpRequest requestOut = new HttpRequest();
                        String rsOut = requestOut.postJson(
                                GlobalVariable.SEHTTPServer + "/linebusy",
                                "{\"code\":" + 200 + ","
                                        +"\"linebusy\": \""
                                        +(GlobalVariable.linebusy1?"true":"false") + ","
                                        +(GlobalVariable.linebusy2?"true":"false") + ","
                                        +(GlobalVariable.linebusy3?"true":"false") + ","
                                        +(GlobalVariable.linebusy4?"true":"false") + ","
                                        +(GlobalVariable.linebusy5?"true":"false") + ","
                                        +(GlobalVariable.linebusy6?"true":"false") + "\","
                                        +"\"busymodel\": "+ GlobalVariable.busyall
                                        + "}"
                        );

                        HttpRequest request1 = new HttpRequest();
                        String rs1 = request1.postJson(
                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/updateSysConfig",
                                "{\"userid\":\""+GlobalVariable.userid+"\"," +
                                        "\"pgmid\":\"" + GlobalVariable.pgmid+"\","+
                                        "\"busystate\": " + (GlobalVariable.busystate?'1':'0') +","+
                                        "\"callnum\": " + (GlobalVariable.callnum==null?0:GlobalVariable.callnum) +","+
                                        "\"waitnum\": " + (GlobalVariable.waitnum==null? 0:GlobalVariable.waitnum) +","+
                                        "\"online\": " + (GlobalVariable.online?'1':'0') +","+
                                        "\"callring\": " +(GlobalVariable.callring?'1':'0') +","+
                                        "\"communicatering\": " +(GlobalVariable.communicatering?'1':'0')+","+
                                        "\"allowblacklist\": " +(GlobalVariable.allowblacklist?'1':'0')+","+
                                        "\"whitelistmodel\": " +(GlobalVariable.whitelistmodel?'1':'0')+","+
                                        "\"busyall\": " +(GlobalVariable.busyall)+","+
                                        "\"viewmodel\": " +(GlobalVariable.viewmodel)+","+//特殊情况
                                        "\"sipmodel\": " +GlobalVariable.sipmodel+","+
                                        "\"autoswitch\": " +(GlobalVariable.autoswitch?'1':'0')+","+
                                        "}"
                        );

                        if(SettingActivity.handler_!=null){
                            Message m2 = Message.obtain(SettingActivity.handler_,1002,null);
                            m2.sendToTarget();
                        }
                        if(GlobalVariable.send_to_other_config) {
                            String msg_to_send = "{ " +
                                    "\"msgType\": \"CHANGE SYS CONFIG\"," +
                                    "\"config\": \"" + GlobalVariable.send_to_other_content + "\"," +
                                    "\"state\": \"1\"," +
                                    "\"local\": \"\"" +
                                    "}";
                            if (MainActivity.wsControl != null)
                                MainActivity.wsControl.sendMessage(msg_to_send);
                        }
                    }
                }catch (Exception e) {e.printStackTrace();}
            }
        }.start();
        LinebusyConfig linebusyConfig = new LinebusyConfig();
        linebusyConfig.code = 200;
        return JsonUtils.toJson(linebusyConfig);
    }

    public void getCallRecordInfo(MainActivity.MyCallWithState callWithState, String finalPstn, String uid){
        HttpRequest request = new HttpRequest();
        String rs = request.postJson(
                "Http://"+GlobalVariable.SIPSERVER+":8080/smart/contactWithSipServer/getPstnByPstn",
                "{\"type\":\"post pstn info\", " +
                        "\"pstn\":\""+ finalPstn +"\", " +
                        "\"callid\":\""+ uid +"\"}"
        );

        try {
            org.json.JSONObject rs_json = new org.json.JSONObject(rs);
            JSONArray rs_jsonarray = rs_json.getJSONObject("data").getJSONArray("pstns");
            if(rs_jsonarray.length()>0)
            {
                org.json.JSONObject rs_json_pstn = rs_jsonarray.getJSONObject(0);
                callWithState.pstn = rs_json_pstn.get("pstn")==null?"":rs_json_pstn.get("pstn").toString();
                callWithState.name = rs_json_pstn.get("name")==null?"":rs_json_pstn.get("name").toString();
                callWithState.age =  rs_json_pstn.get("age")==null? 0:(Integer)rs_json_pstn.get("age");
                callWithState.male = rs_json_pstn.get("male")==null? 0:(Integer) rs_json_pstn.get("male");
                callWithState.from = rs_json_pstn.get("frm")==null?"":rs_json_pstn.get("frm").toString();
                callWithState.iswhitelist = String.valueOf(rs_json_pstn.get("iswhitelist")).equals("1")?1:0;
                callWithState.isblacklist = String.valueOf(rs_json_pstn.get("isblacklist")).equals("1")?1:0;
                callWithState.iscontact = String.valueOf(rs_json_pstn.get("iscontact")).equals("1")?1:0;
                callWithState.isexpert = String.valueOf(rs_json_pstn.get("isexpert")).equals("1")?1:0;
            }


            org.json.JSONObject rs_json_callrecord = rs_json.getJSONObject("data").getJSONObject("callrecord");
            if(rs_json_callrecord!=null)
            {
                callWithState.content = rs_json_callrecord.get("content")==null?"":rs_json_callrecord.get("content").toString();
                callWithState.pstnmark = rs_json_callrecord.get("pstnmark")==null?"":rs_json_callrecord.get("pstnmark").toString();
                callWithState.pdrmark = rs_json_callrecord.get("pdrmark")==null?"":rs_json_callrecord.get("pdrmark").toString();
                callWithState.mbcmark = rs_json_callrecord.get("mbcmark")==null?"":rs_json_callrecord.get("mbcmark").toString();
            }
        } catch (Exception e) {
            LogClient.generate("【CallRecord获取转换错误】"+e.getMessage());
            e.printStackTrace();
        }
    }

    public void fullEditWhiteOrBlackState(String pstn_number,Boolean white_edit,Boolean black_edit){
        MainActivity.MyCallWithState callws = new MainActivity.MyCallWithState();
        callws.pstn = pstn_number;
        callws.uid = "1";
        new Thread() {
            @Override
            public void run() {
                getCallRecordInfo(callws, callws.pstn, callws.uid);
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoHis",
                        "{\"type\":\"post pstn info\", " +
                                "\"pstn\":\"" + callws.pstn + "\", " +
                                "\"callid\":\"" + callws.uid + "\", " +
                                "\"name\":\"" + ((callws.name==null||callws.name.equals(""))?"":callws.name) + "\", " +
                                "\"iswhitelist\":\"" + (white_edit?0:(callws.iswhitelist==null?0:callws.iswhitelist)) + "\", " +
                                "\"isblacklist\":\"" + (black_edit?0:(callws.isblacklist==null?0:callws.isblacklist)) + "\", " +
                                "\"iscontact\":\"" + (callws.iscontact==null?0:callws.iscontact) + "\", " +
                                "\"isexpert\":\"" + (callws.isexpert==null?0:callws.isexpert) + "\"" +
                                "}"
                );
            }
        }.start();
    }

    @PostMapping("/listoperate")
    String listoperate(@RequestBody String jsonString) {
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String type = jsonObject.get("type").toString();
        String operate = jsonObject.get("operate").toString();

        String oldnumber = jsonObject.get("oldnumber").toString();
        String newnumber = jsonObject.get("newnumber").toString();


        //三种操作，增，删，改
        if(operate.equals("add"))
        {
            ///smart/blackList/pc/v1/add,参数:{"callingnumber":"18010001003","content":""}
            if(type.equals("blacklist")) {
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/addWhiteBlackByAndroid",
                        "{\"phonenumber\":\"" + newnumber + "\"" +
                                ",\"type\":\"black\"" +
                                "}"
                );
            }
            else if(type.equals("whitelist")){
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/addWhiteBlackByAndroid",
                        "{\"phonenumber\":\"" + newnumber + "\"" +
                                ",\"type\":\"white\"" +
                                "}"
                );
            }

        }
        else if(operate.equals("remove"))
        {
            if(type.equals("blacklist")) {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getBlackList",
                            "{}"
                    );
                    org.json.JSONObject rs_json = new org.json.JSONObject(rs);
                    JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                    List<Pstn> pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

                    for (Pstn pstn : pstnlist) {
                        if (newnumber.equals(pstn.callingnumber)) {
                            //将此号码移除
                            Long deleteid = pstn.id;
                            HttpRequest requestA = new HttpRequest();
                            String rsA = requestA.postJson(
                                    "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deleteWhiteBlackByAndroid",
                                    "{\"id\":\"" + deleteid + "\"" +
                                            ",\"type\":\"black\""+
                                            "}"
                            );

                            fullEditWhiteOrBlackState(newnumber,false,true);
                        }
                    }
                } catch (Exception e) {}
            }
            else if(type.equals("whitelist"))
            {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getWhiteList",
                            "{}"
                    );
                    org.json.JSONObject rs_json = new org.json.JSONObject(rs);
                    JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                    List<Pstn> pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

                    for (Pstn pstn : pstnlist) {
                        if (newnumber.equals(pstn.callingnumber)) {
                            //将此号码移除
                            Long deleteid = pstn.id;
                            HttpRequest requestA = new HttpRequest();
                            String rsA = requestA.postJson(
                                    "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deleteWhiteBlackByAndroid",
                                    "{\"id\":\"" + deleteid + "\"" +
                                            ",\"type\":\"white\""+
                                            "}"
                            );
                            fullEditWhiteOrBlackState(newnumber,true,false);
                        }
                    }
                } catch (Exception e) {
                    Map<String,Object> resultMap = new HashMap<>();
                    resultMap.put("code",400);
                    return JsonUtils.toJson(resultMap);
                }
            }
        }
        else if(operate.equals("edit"))
        {
            if(type.equals("blacklist")) {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getBlackList",
                            "{}"
                    );
                    org.json.JSONObject rs_json = new org.json.JSONObject(rs);
                    JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                    List<Pstn> pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

                    for (Pstn pstn : pstnlist) {
                        if (oldnumber.equals(pstn.callingnumber)) {
                            //将此号码移除
                            Long deleteid = pstn.id;
                            HttpRequest requestA = new HttpRequest();
                            String rsA = requestA.postJson(
                                    "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deleteWhiteBlackByAndroid",
                                    "{\"id\":\"" + deleteid + "\"" +
                                            ",\"type\":\"black\""+
                                            "}"
                            );
                            fullEditWhiteOrBlackState(oldnumber,false,true);
                        }
                    }
                    //添加新号码
                    HttpRequest requestAdd = new HttpRequest();
                    String rsAdd = requestAdd.postJson(
                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/addWhiteBlackByAndroid",
                            "{\"phonenumber\":\"" + newnumber + "\"" +
                                    ",\"type\":\"black\"" +
                                    "}"
                    );
                } catch (Exception e) {}
            }
            else if(type.equals("whitelist"))
            {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getWhiteList",
                            "{}"
                    );
                    org.json.JSONObject rs_json = new org.json.JSONObject(rs);
                    JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                    List<Pstn> pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

                    for (Pstn pstn : pstnlist) {
                        if (oldnumber.equals(pstn.callingnumber)) {
                            //将此号码移除
                            Long deleteid = pstn.id;
                            HttpRequest requestA = new HttpRequest();
                            String rsA = requestA.postJson(
                                    "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/deleteWhiteBlackByAndroid",
                                    "{\"id\":\"" + deleteid + "\"" +
                                            ",\"type\":\"white\""+
                                            "}"
                            );
                            fullEditWhiteOrBlackState(oldnumber,false,true);
                        }
                    }

                    //添加新号码
                    HttpRequest requestAdd = new HttpRequest();
                    String rsAdd = requestAdd.postJson(
                            "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/addWhiteBlackByAndroid",
                            "{\"phonenumber\":\"" + newnumber + "\"" +
                                    ",\"type\":\"white\"" +
                                    "}"
                    );

                } catch (Exception e) {
                    Map<String,Object> resultMap = new HashMap<>();
                    resultMap.put("code",400);
                    return JsonUtils.toJson(resultMap);
                }
            }
        }

        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("code",200);
        return JsonUtils.toJson(resultMap);
    }

    //获得黑白名单
    @PostMapping("/listseek")
    String listseek(@RequestBody String jsonString){
        JSONObject jsonObject =JSONObject.parseObject(jsonString);
        String type = jsonObject.get("type").toString();
        List<Pstn> pstnlist = new ArrayList<>();

        if(type.equals("blacklist"))
        {
            try {
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getBlackList",
                        "{}"
                );
                org.json.JSONObject rs_json = new org.json.JSONObject(rs);
                JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

            } catch (Exception e) {
                Map<String,Object> resultMap = new HashMap<>();
                resultMap.put("code",400);
                return JsonUtils.toJson(resultMap);
            }
        }
        else if(type.equals("whitelist"))
        {
            try {
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getWhiteList",
                        "{}"
                );
                org.json.JSONObject rs_json = new org.json.JSONObject(rs);
                JSONArray rs_json_contactslist = rs_json.getJSONArray("data");
                pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

            } catch (Exception e) {
                Map<String,Object> resultMap = new HashMap<>();
                resultMap.put("code",400);
                return JsonUtils.toJson(resultMap);
            }
        }



        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("list",pstnlist);
        resultMap.put("code",200);
        return JsonUtils.toJson(resultMap);
    }
    //获得主机电源状态
    @PostMapping("/querypower")
    String querypower(@RequestBody String jsonString) {

        //一般的处理流程
        //用户发送请求消息到话机，话机转发给网页后台http接口，
        //在接口中发送命令给端口1444的本地websocket 串口命令转发程序sipcomm
        //sipcomm将命令通过串口发送到设备前控制面板fpga
        //fpga回复串口消息，串口程序转发到网页后台/getInstruction接口
        ///getInstruction里面转发给本地1333端口，发送给SIP主程序
        //SIP主程序转发给各个关联的话机
        //话机的websocket处理中处理返回消息，
        //主动发送http消息给第一步的发送请求消息的用户服务器

        String powerstate = "";
        try {
            HttpRequest request = new HttpRequest();
            String rs = request.postJson(
                    "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getPowerState",
                    "{}"
            );
            //org.json.JSONObject rs_json = new org.json.JSONObject(rs);
            //powerstate = rs_json.getJSONObject("data").getString("power");
        } catch (Exception e) {
            Map<String,Object> resultMap = new HashMap<>();
            resultMap.put("code",400);
            return JsonUtils.toJson(resultMap);
        }

        Map<String,Object> resultMap = new HashMap<>();
        //resultMap.put("power",powerstate);
        resultMap.put("code",200);
        return JsonUtils.toJson(resultMap);
    }
}

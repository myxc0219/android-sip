package org.pjsip.pjsua2.app.model;

public class Pstn {
    public Long id;

    public String pstn;

    public String name;

    public Integer male;

    public Integer age;

    public String from;

    public Integer isblacklist;

    public String remark;

    public Integer type = 1;

    public String callingnumber;
}

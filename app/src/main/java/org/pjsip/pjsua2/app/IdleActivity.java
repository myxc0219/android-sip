package org.pjsip.pjsua2.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class IdleActivity extends Activity {


    private TimerTask task;
    private Timer timer;

    private int marginX = 0;
    private int marginY = 0;

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if(marginY == 240&&marginX == 680)
            {
                marginX = 0;
                marginY = 0;
            }
            else if(marginX == 680)
            {
                marginX = 0;
                marginY += 20;
            }
            LinearLayout gifLayout = findViewById(R.id.idleGIF);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(marginX,marginY,0,0);
            gifLayout.setLayoutParams(params);
            marginX += 10;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idle);


        task = new TimerTask() {
            @Override
            public void run() {
                //handler.sendEmptyMessage(1);
                Message m = Message.obtain(handler, 1000, null );
                m.sendToTarget();
            }
        };

        timer = new Timer();
        //timer.schedule(task, 1000);
        timer.schedule(task, new Date(),1000);


//        Date now = new Date();
//        SimpleDateFormat dateFormat = new SimpleDateFormat(
//                "yyyy-MM-dd");// 可以方便地修改日期格式
//        String hehe = dateFormat.format(now);
//        Timer timer = new Timer(true);
//        Random rand = new Random();
//        timer.schedule(task,strToDateLong(hehe+" 2:"+rand.nextInt(60) + 1+":"+rand.nextInt(60) + 1));

//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }).start();
    }

    @Override
    protected void onDestroy() {

        if(task!=null)
        {
            task.cancel();
        }
        if(timer!=null)
        {
            timer.cancel();
        }
        super.onDestroy();
    }

    public void exitIdle(View view)
    {
        if(task!=null)
        {
            task.cancel();
        }
        if(timer!=null)
        {
            timer.cancel();
        }
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);//可以跳过中间栈直接实现两个界面间的切换
        startActivity(intent);
    }


    @SuppressLint("SimpleDateFormat")
    public static Date strToDateLong(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ParsePosition pos = new ParsePosition(0);
        Date strtodate = formatter.parse(strDate, pos);
        return strtodate;
    }

}
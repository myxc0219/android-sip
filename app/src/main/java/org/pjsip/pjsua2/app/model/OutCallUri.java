package org.pjsip.pjsua2.app.model;

public class OutCallUri {
    private String outcalluri;

    private Integer type;

    private String uid;

    private Integer state;

    private String callednumber;

    public String getCallednumber() {
        return callednumber;
    }

    public void setCallednumber(String callednumber) {
        this.callednumber = callednumber;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getOutcalluri() {
        return outcalluri;
    }

    public void setOutcalluri(String outcalluri) {
        this.outcalluri = outcalluri;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}

package org.pjsip.pjsua2.app.util;

import android.content.Context;
import android.os.CountDownTimer;

public class MyTimerUtils extends CountDownTimer {

    public static MyTimerUtils mInstance;

    private Context context;

    //private  static long Future = 2 * 60 * 1000;
    private  static long Future = 3000 * 1000;

    private static long Interval = 1000;

    public static MyTimerUtils getInstance(Context context){
        //单例
        if (mInstance == null){
            synchronized (MyTimerUtils.class){
                if (mInstance == null) {
                    mInstance = new MyTimerUtils(Future,Interval,context);
                }
            }
        }
        return mInstance;

    }

    private MyTimerUtils(long millisInFuture, long countDownInterval, Context context) {
        super(Future, Interval);
        this.context = context;
    }

    @Override
    public void onTick(long time) {
        //TODO 每隔countDownInterval执行
    }

    @Override
    public void onFinish() {
        //TODO 计时完成
        //20210419去除屏保功能
        //context.startActivity(new Intent(context, IdleActivity.class));

        //Log.e("aaaa", "jishi wancheng");
    }


}




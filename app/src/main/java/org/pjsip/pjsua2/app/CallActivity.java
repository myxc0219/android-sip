/* $Id: CallActivity.java 5138 2015-07-30 06:23:35Z ming $ */
/*
 * Copyright (C) 2013 Teluu Inc. (http://www.teluu.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.pjsip.pjsua2.app;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.Call;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.VideoPreviewOpParam;
import org.pjsip.pjsua2.VideoWindowHandle;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.UserInfo;
import org.pjsip.pjsua2.app.service.MusicService;
import org.pjsip.pjsua2.app.util.CommonPlatformManager;
import org.pjsip.pjsua2.pjmedia_orient;
import org.pjsip.pjsua2.pjsip_inv_state;
import org.pjsip.pjsua2.pjsip_role_e;
import org.pjsip.pjsua2.pjsip_status_code;

import pl.droidsonroids.gif.GifImageView;

class VideoPreviewHandler implements SurfaceHolder.Callback
{
    public boolean videoPreviewActive = false;
        
    public void updateVideoPreview(SurfaceHolder holder) 
    {
	if (MainActivity.currentCall != null &&
	    MainActivity.currentCall.vidWin != null &&
	    MainActivity.currentCall.vidPrev != null)
	{	
	    if (videoPreviewActive) {
		VideoWindowHandle vidWH = new VideoWindowHandle();
		vidWH.getHandle().setWindow(holder.getSurface());
		VideoPreviewOpParam vidPrevParam = new VideoPreviewOpParam();
		vidPrevParam.setWindow(vidWH);		
		try {
		    MainActivity.currentCall.vidPrev.start(vidPrevParam);
		} catch (Exception e) {
		    System.out.println(e);
		}
	    } else {
		try {
		    MainActivity.currentCall.vidPrev.stop();
		} catch (Exception e) {
		    System.out.println(e);
		}	
	    }
	}
    }    
    
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
	updateVideoPreview(holder);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) 
    {
	
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) 
    {
	try {
	    MainActivity.currentCall.vidPrev.stop();
	} catch (Exception e) {
	    System.out.println(e);
	}
    }    
}

public class CallActivity extends BaseActivity
			  implements Handler.Callback, SurfaceHolder.Callback
{

    public static Handler handler_;
    private static VideoPreviewHandler previewHandler = 
	    					      new VideoPreviewHandler();

    private final Handler handler = new Handler(this);
    private static CallInfo lastCallInfo;

	public static CallActivity instance=null;

	private MyConnection conn;
	private MusicService.MyBinder musicControl;
	private class MyConnection implements ServiceConnection {
		//服务启动完成后会进入到这个方法
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			//获得service中的MyBinder
			musicControl = (MusicService.MyBinder) service;
			Log.e("MainActivity","MUSIC服务与CallActivity成功绑定");
			LogClient.generate("【MUSIC服务与CallActivity成功绑定】");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
		}
	}

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
		instance = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_call);


		Intent intent_music = new Intent(this, MusicService.class);
		conn = new MyConnection();
		startService(intent_music);
		bindService(intent_music, conn, BIND_AUTO_CREATE);

		SurfaceView surfaceInVideo = (SurfaceView)
					  findViewById(R.id.surfaceIncomingVideo);
		SurfaceView surfacePreview = (SurfaceView)
					  findViewById(R.id.surfacePreviewCapture);
		Button buttonShowPreview = (Button)
					  findViewById(R.id.buttonShowPreview);

		if (MainActivity.currentCall == null ||
			MainActivity.currentCall.vidWin == null)
		{
			surfaceInVideo.setVisibility(View.GONE);
			buttonShowPreview.setVisibility(View.GONE);
		}
		setupVideoPreview(surfacePreview, buttonShowPreview);
		surfaceInVideo.getHolder().addCallback(this);
		surfacePreview.getHolder().addCallback(previewHandler);

		handler_ = handler;

		if (MainActivity.currentCall != null) {
			try {
			lastCallInfo = MainActivity.currentCall.getInfo();
			updateCallState(lastCallInfo);
			} catch (Exception e) {
			System.out.println(e);
			}
		} else {
			updateCallState(lastCallInfo);
		}

//		Button buttonRevert = (Button)
//					findViewById(R.id.buttonRevert);
//		buttonRevert.setVisibility(View.GONE);


		TextView nameView = findViewById(R.id.textViewPeer);
		TextView stateView = findViewById(R.id.textViewCallState);

		if(GlobalVariable.innerstart)//当前是内通发起方的机器，应该显示to的相关信息
		{
			nameView.setText(GlobalVariable.innerchannelto + GlobalVariable.innernameto);
			stateView.setText("等待对方接听");

			ImageButton buttonAccept = findViewById(R.id.buttonAccept);
			buttonAccept.setVisibility(View.GONE);
		}
		else{
			nameView.setText(GlobalVariable.innerchannelfrom + GlobalVariable.innernamefrom);
			stateView.setText("呼入中...");

			ImageButton buttonAccept = findViewById(R.id.buttonAccept);
			buttonAccept.setVisibility(View.VISIBLE);

			//响铃+闪烁
			CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
			flashManager.flash();

		}

//		Button buttonBackAccept = (Button)findViewById(R.id.buttonBackAccept);
//		buttonBackAccept.setVisibility(View.GONE);



    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        
        WindowManager wm;
        Display display;
        int rotation;
        int orient;

        wm = (WindowManager)this.getSystemService(Context.WINDOW_SERVICE);
        display = wm.getDefaultDisplay();
        rotation = display.getRotation();
        System.out.println("Device orientation changed: " + rotation);
        
        switch (rotation) {
        case Surface.ROTATION_0:   // Portrait
            orient = pjmedia_orient.PJMEDIA_ORIENT_ROTATE_270DEG;
            break;
        case Surface.ROTATION_90:  // Landscape, home button on the right
            orient = pjmedia_orient.PJMEDIA_ORIENT_NATURAL;
            break;
        case Surface.ROTATION_180:
            orient = pjmedia_orient.PJMEDIA_ORIENT_ROTATE_90DEG;
            break;
        case Surface.ROTATION_270: // Landscape, home button on the left
            orient = pjmedia_orient.PJMEDIA_ORIENT_ROTATE_180DEG;
            break;
        default:
            orient = pjmedia_orient.PJMEDIA_ORIENT_UNKNOWN;
        }

        if (MyApp.ep != null && MainActivity.account != null) {
            try {
        	AccountConfig cfg = MainActivity.account.cfg;
        	int cap_dev = cfg.getVideoConfig().getDefaultCaptureDevice();
        	MyApp.ep.vidDevManager().setCaptureOrient(cap_dev, orient,
        						  true);
            } catch (Exception e) {
        	System.out.println(e);
            }
        }
    }    

    @Override
    protected void onDestroy()
    {
		unbindService(conn);
		super.onDestroy();
		handler_ = null;
    }
    
    private void updateVideoWindow(boolean show)
    { 
	if (MainActivity.currentCall != null &&
	    MainActivity.currentCall.vidWin != null &&
	    MainActivity.currentCall.vidPrev != null)
	{
	    SurfaceView surfaceInVideo = (SurfaceView) 
		       		  findViewById(R.id.surfaceIncomingVideo);
	    
	    VideoWindowHandle vidWH = new VideoWindowHandle();	    
	    if (show) {
		vidWH.getHandle().setWindow(
				       surfaceInVideo.getHolder().getSurface());
	    } else {
		vidWH.getHandle().setWindow(null);
	    }
	    try {
		MainActivity.currentCall.vidWin.setWindow(vidWH);
	    } catch (Exception e) {
		System.out.println(e);
	    }	    
	}
    }
     
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
	updateVideoWindow(true);
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
	updateVideoWindow(false);
    }


	public void LED_Media_Notify(){
		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
		if(musicControl!=null&&!musicControl.isPlaying()){

			if(GlobalVariable.callring){
				musicControl.play();
				flashManager.flash();
			}
			else{
				flashManager.flash();
			}
		}
		//每次暂停播放音乐也就是接听后将切换到话筒

	}
	public void LED_Media_Pause(){
		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
		if(musicControl!=null&&musicControl.isPlaying()){
			musicControl.pause();
		}
		flashManager.stopflash();
		//每次暂停播放音乐也就是接听后将切换到话筒
	}

    public void switchCall(View view)
	{
//		String msg_to_send = "{ \"msgType\": \"DAOBO TO ZHUBO\", \"DaoBoCallUri\": \""+MainActivity.daoboUri+"\", \"ZhuBoCallUri\": \""+MainActivity.zhuboUri+"\", \"OutCallUri\": \""+MainActivity.callId+"\" }";
//		Intent intent_ack = new Intent(this, WebSocketService.class);
//		intent_ack.putExtra("msg_to_send", msg_to_send);
//		startService(intent_ack);
	}

	public void revertCall(View view)
	{
		//主播通知SIP服务器，应答主播返回电话
//		String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""+MainActivity.callId+"\", \"DaoBoCallUri\": \""+MainActivity.daoboUri+"\", \"ZhuBoCallUri\": \""+MainActivity.zhuboUri+"\" }";
//		Intent intent_ack = new Intent(this, WebSocketService.class);
//		intent_ack.putExtra("msg_to_send", msg_to_send);
//		startService(intent_ack);
	}

	public void backAcceptCall(View view)
	{
//		//导播通知SIP服务器，应答主播返回电话
//		String msg_to_send = "{ \"msgType\": \"ACK ZHUBO TO DAOBO\", \"OutCallUri\": \""+MainActivity.callId+"\", \"DaoBoCallUri\": \""+MainActivity.daoboUri+"\", \"ZhuBoCallUri\": \""+MainActivity.zhuboUri+"\" }";
//		Intent intent_ack = new Intent(this, WebSocketService.class);
//		intent_ack.putExtra("msg_to_send", msg_to_send);
//		startService(intent_ack);
	}

    public void acceptCall(View view)
    {
    	//20210522改成协议发送形式
//		CallOpParam prm = new CallOpParam();
//		prm.setStatusCode(pjsip_status_code.PJSIP_SC_OK);
//		try {
//			MainActivity.currentCall.answer(prm);
//		} catch (Exception e) {
//			System.out.println(e);
//		}

		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
		flashManager.stopflash();

		String msg_to_send =
				"{ \"msgType\": \"INNER ANSWER CALL\"" +
						", \"fromsipid\": \""+ GlobalVariable.innercallfrom+ "\""
						+", \"tosipid\": \""+ GlobalVariable.innercallto+ "\""
						+ ", \"frompgm\": \""+ GlobalVariable.innerchannelfrom + "\""
						+ ", \"topgm\": \""+ GlobalVariable.innerchannelto + "\""
						+ ", \"fromname\": \""+ GlobalVariable.innernamefrom + "\""
						+ ", \"toname\": \""+ GlobalVariable.innernameto + "\""
						+ ", \"fromrole\": \""+ GlobalVariable.innerrolefrom + "\""
						+ ", \"torole\": \""+ GlobalVariable.innerroleto + "\""
						+ ", \"msg\": \"" + "\""
						+" }";
		if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);
		view.setVisibility(View.GONE);
    }

    public void hangupCall(View view)
    {
		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
		flashManager.stopflash();

		TextView stateView = findViewById(R.id.textViewCallState);
		if(stateView.getText().toString().equals("外线呼入")){
			//跳转到主界面
			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
		}
		else {
			String msg_to_send =
					"{ \"msgType\": \"INNER HANGUP\""
							+ ", \"fromsipid\": \"" + GlobalVariable.innercallfrom + "\""
							+ ", \"tosipid\": \"" + GlobalVariable.innercallto + "\""
							+ ", \"frompgm\": \"" + GlobalVariable.innerchannelfrom + "\""
							+ ", \"topgm\": \"" + GlobalVariable.innerchannelto + "\""
							+ ", \"fromname\": \"" + GlobalVariable.innernamefrom + "\""
							+ ", \"toname\": \"" + GlobalVariable.innernameto + "\""
							+ ", \"fromrole\": \"" + GlobalVariable.innerrolefrom + "\""
							+ ", \"torole\": \"" + GlobalVariable.innerroleto + "\""
							+ ", \"msg\": \"" + "\""
							+ " }";
			if (MainActivity.wsControl != null) MainActivity.wsControl.sendMessage(msg_to_send);
		}
		handler_ = null;
		finish();
		//20210522修改为发送消息
//		if (MainActivity.currentCall != null) {
//			CallOpParam prm = new CallOpParam();
//			prm.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
//			try {
//				MainActivity.currentCall.hangup(prm);
//			} catch (Exception e) {
//				System.out.println(e);
//			}
//		}
    }
    
    public void setupVideoPreview(SurfaceView surfacePreview, 
	    			  Button buttonShowPreview)
    {
	surfacePreview.setVisibility(previewHandler.videoPreviewActive?
		     		     View.VISIBLE:View.GONE);
	
	buttonShowPreview.setText(previewHandler.videoPreviewActive?
		  		  getString(R.string.hide_preview):
		  		  getString(R.string.show_preview));		
    }
    
    public void showPreview(View view)
    {
	SurfaceView surfacePreview = (SurfaceView)
		  	          findViewById(R.id.surfacePreviewCapture);	

	Button buttonShowPreview = (Button) 
		  		  findViewById(R.id.buttonShowPreview);
	
	
	previewHandler.videoPreviewActive = !previewHandler.videoPreviewActive;
	
	setupVideoPreview(surfacePreview, buttonShowPreview);
	
	previewHandler.updateVideoPreview(surfacePreview.getHolder());
    }

    private void setupVideoSurface()
    {
	SurfaceView surfaceInVideo = (SurfaceView)
				  findViewById(R.id.surfaceIncomingVideo);
	SurfaceView surfacePreview = (SurfaceView)
		  		  findViewById(R.id.surfacePreviewCapture);
	Button buttonShowPreview = (Button)
		  		  findViewById(R.id.buttonShowPreview);	
	surfaceInVideo.setVisibility(View.VISIBLE);
	buttonShowPreview.setVisibility(View.VISIBLE);
	surfacePreview.setVisibility(View.GONE);	
    }

    @Override
    public boolean handleMessage(Message m)
    {
		if (m.what == 1001) {
			GifImageView gifView = (GifImageView) findViewById(R.id.callPanelGif);
			gifView.setVisibility(View.VISIBLE);
			ImageButton buttonAccept = findViewById(R.id.buttonAccept);
			buttonAccept.setVisibility(View.GONE);
//			ImageButton buttonAccept = findViewById(R.id.buttonAccept);
//			buttonAccept.setVisibility(View.GONE);
			//TextView nameView = findViewById(R.id.textViewPeer);
			TextView stateView = findViewById(R.id.textViewCallState);
			stateView.setText(GlobalVariable.innerstate);
		}
		if (m.what == 1002) {

			Log.e("CALLActivity收到挂断刷新消息","是");
			GifImageView gifView = (GifImageView) findViewById(R.id.callPanelGif);
			gifView.setVisibility(View.GONE);
			ImageButton buttonAccept = findViewById(R.id.buttonAccept);
			buttonAccept.setVisibility(View.GONE);
			//TextView nameView = findViewById(R.id.textViewPeer);
			TextView stateView = findViewById(R.id.textViewCallState);
			stateView.setText(GlobalVariable.innerstate);
		}
		if (m.what == 1003) {
			TextView nameView = findViewById(R.id.textViewPeer);
			TextView stateView = findViewById(R.id.textViewCallState);

			if(GlobalVariable.innerstart)//当前是内通发起方的机器，应该显示to的相关信息
			{
				nameView.setText(GlobalVariable.innerchannelto + GlobalVariable.innernameto);
				stateView.setText("等待对方接听");

				ImageButton buttonAccept = findViewById(R.id.buttonAccept);
				buttonAccept.setVisibility(View.GONE);
			}
			else{
				nameView.setText(GlobalVariable.innerchannelfrom + GlobalVariable.innernamefrom);
				stateView.setText("呼入中...");

				ImageButton buttonAccept = findViewById(R.id.buttonAccept);
				buttonAccept.setVisibility(View.VISIBLE);
			}
		}
		//20210522去除原消息处理
//		if (m.what == MainActivity.MSG_TYPE.CALL_STATE) {
//
//			lastCallInfo = (CallInfo) m.obj;
//			updateCallState(lastCallInfo);
//
//		}
//		else if (m.what == MainActivity.MSG_TYPE.CALL_MEDIA_STATE) {
//
//			if (MainActivity.currentCall.vidWin != null) {
//			/* Set capture orientation according to current
//			 * device orientation.
//			 */
//			onConfigurationChanged(getResources().getConfiguration());
//			/* If there's incoming video, display it. */
//			setupVideoSurface();
//			}
//
//		}
//		else if(m.what == MainActivity.MSG_TYPE.BACK_CALL_STATE) {
//			Button buttonBackAccept = (Button)
//					findViewById(R.id.buttonBackAccept);
//			buttonBackAccept.setVisibility(View.VISIBLE);
//		}
		else {

			/* Message not handled */
			return false;

		}
		return true;
    }

    private void updateCallState(CallInfo ci) {
		GifImageView gifView = (GifImageView) findViewById(R.id.callPanelGif);
		TextView tvPeer  = (TextView) findViewById(R.id.textViewPeer);
		TextView tvState = (TextView) findViewById(R.id.textViewCallState);
		ImageButton buttonHangup = (ImageButton) findViewById(R.id.buttonHangup);
		ImageButton buttonAccept = (ImageButton) findViewById(R.id.buttonAccept);
		String call_state = "";

		if (ci == null) {
			gifView.setVisibility(View.GONE);
			buttonAccept.setVisibility(View.GONE);
			//buttonHangup.setText("OK");
			tvState.setText("电话挂断");
			return;
		}

		if (ci.getRole() == pjsip_role_e.PJSIP_ROLE_UAC) {
			buttonAccept.setVisibility(View.GONE);
		}

		if (ci.getState() <
			pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED)
		{
			if (ci.getRole() == pjsip_role_e.PJSIP_ROLE_UAS) {
				gifView.setVisibility(View.GONE);
				call_state = "来电";
			/* Default button texts are already 'Accept' & 'Reject' */
			} else {
				gifView.setVisibility(View.GONE);
				//buttonHangup.setText("取消");
				call_state = ci.getStateText();
				//自己拨出电话播出成功后
			}
		}
		else if (ci.getState() >=
			 pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED)
		{
			buttonAccept.setVisibility(View.GONE);
			call_state = ci.getStateText();
			if(call_state!=null && call_state.equals("CONFIRMED"))
				call_state = "已接通";
			if (ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_CONFIRMED) {
				gifView.setVisibility(View.VISIBLE);
				//buttonHangup.setText("挂断");
			} else if (ci.getState() ==
				   pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED)
			{
				gifView.setVisibility(View.GONE);
				//buttonHangup.setText("确认");
				call_state = "电话挂断: " + ci.getLastReason();

				int id = ci.getId();
				Log.e("测试CALLID--CALLINFO",id+"");
				Call call = Call.lookup(id);
				CallOpParam prm = new CallOpParam();
				prm.setStatusCode(pjsip_status_code.PJSIP_SC_DECLINE);
				try {
					if(call!=null) {
						//call.hangup(prm);
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
				if(call!=null){
					call.delete();
				}
			}
		}
		//20210514不再显示remoteuri显示频道和用户名

		String showname = "";
		String rm_uri = ci.getRemoteUri();
		showname = rm_uri;
		String[] rm_uri_split = rm_uri.split("@");
		String sipid = rm_uri_split[0].replace("<sip:","");


		for(UserInfo info : GlobalVariable.userinfoList)
		{
			if(info.getSipid()!=null && info.getSipid().equals(sipid));
				showname = info.getChannelname()+" "+info.getUsername();
		}
		tvPeer.setText(showname);
		//tvPeer.setText(ci.getRemoteUri());
		tvState.setText(call_state);
    }
}

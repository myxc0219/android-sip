package org.pjsip.pjsua2.app;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import org.java_websocket.handshake.ServerHandshake;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.JWebSocketClient;

import java.net.URI;

public class JWebSocketClientService extends Service {

    //public static final String ws = "ws://"+ GlobalVariable.EUSERVER +":1333";
    public JWebSocketClient client;
    private JWebSocketClientBinder mBinder = new JWebSocketClientBinder();
    private final static int GRAY_SERVICE_ID = 1001;
    public static final long HEART_BEAT_RATE = 10 * 1000;//十秒心跳


    //灰色保活
    public static class GrayInnerService extends Service {

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
            return super.onStartCommand(intent, flags, startId);
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }


    //用于Activity和service通讯
    public class JWebSocketClientBinder extends Binder {
        public JWebSocketClientService getService() {
            return JWebSocketClientService.this;
        }
        public void sendMessage(String msg_to_send){
            try {
                Log.e("发出消息", msg_to_send);
                client.send(msg_to_send);
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
        //return new MyBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //初始化websocket
        initSocketClient();
        mHandler.postDelayed(heartBeatRunnable, HEART_BEAT_RATE);//开启心跳检测
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        closeConnect();
        super.onDestroy();
    }

    public JWebSocketClientService() {
    }


    /**
     * 初始化websocket连接
     */
    private void initSocketClient() {

        //URI uri = URI.create(ws);
        Log.e("JWebSocketClientService", "初始化client"+"ws://"+ GlobalVariable.EUSERVER +":1333");
        URI uri = URI.create("ws://"+ GlobalVariable.EUSERVER +":1333");
        client = new JWebSocketClient(uri) {
            @Override
            public void onMessage(String message) {
                //EventBus.getDefault().post(new SocketMessage(1, message));
                handleMessage(message);
            }

            @Override
            public void onOpen(ServerHandshake handshakedata) {
                super.onOpen(handshakedata);
                //EventBus.getDefault().post(new SocketMessage(0, ""));

            }


        };
        connect();
    }


    void handleMessage(String msg) {
        Intent intent = new Intent("cn.fitcan.action.HANDLE_WS_MESSAGE");
        intent.putExtra("msg", msg);
        intent.setPackage(getPackageName());
        sendBroadcast(intent);
    }

    /**
     * 连接websocket
     */
    private void connect() {
        new Thread() {
            @Override
            public void run() {
                try {
                    //connectBlocking多出一个等待操作，会先连接再发送，否则未连接发送会报错
                    client.connectBlocking();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }

    /**
     * 发送消息
     *
     * @param msg
     */
    public void sendMsg(String msg) {
        if (null != client && client.isOpen()) {
            Log.e("发送的消息：" , msg);
            LogClient.generate("【发送的消息】"+msg);
            client.send(msg);
        }
    }

    /**
     * 断开连接
     */
    private void closeConnect() {
        try {
            if (null != client) {
                client.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            client = null;
            mHandler.removeCallbacksAndMessages(null);
        }
    }

    private Handler mHandler = new Handler();
    private Runnable heartBeatRunnable = new Runnable() {
        @Override
        public void run() {
            Log.e("JWebSocketClientService", "心跳包检测websocket连接状态");
            LogClient.generate("【心跳包检测websocket连接状态】");
            if (client != null) {
                Log.e("JWebSocketClientService", "获得client连接对象"+client.getURI());
                if (client.isClosed()) {

                    reconnectWs();
                }
            } else {
                //如果client已为空，重新初始化连接
                client = null;
                initSocketClient();
            }
            //每隔一定的时间，对长连接进行一次心跳检测
            mHandler.postDelayed(this, HEART_BEAT_RATE);
        }
    };

    /**
     * 开启重连
     */
    private void reconnectWs() {
        mHandler.removeCallbacks(heartBeatRunnable);
        new Thread() {
            @Override
            public void run() {
                try {
                    Log.e("JWebSocketClientService", "开启重连");
                    LogClient.generate("【JWebSocketClientService开启重连】");
                    //20200608重连有问题
                    client.reconnectBlocking();
                    //client.connectBlocking();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public class MyBinder extends Binder {
        public void sendMessage(String msg_to_send){
            client.send(msg_to_send);
        }
    }


}

//import android.app.Service;
//import android.content.Intent;
//import android.os.Binder;
//import android.os.IBinder;
//import android.util.Log;
//
//import org.java_websocket.WebSocket;
//import org.java_websocket.framing.Framedata;
//import org.pjsip.pjsua2.app.global.GlobalVariable;
//import org.pjsip.pjsua2.app.model.JWebSocketClient;
//
//import java.net.URI;
//
//public class JWebSocketClientService  extends Service {
//    private URI uri;
//    public JWebSocketClient client;
//    private JWebSocketClientBinder mBinder = new JWebSocketClientBinder();
//
//    @Override
//    public void onCreate(){
//        uri = URI.create("ws://"+ GlobalVariable.EUSERVER +":1333");
//        JWebSocketClient client = new JWebSocketClient(uri) {
//            @Override
//            public void onMessage(String message) {
//                //message就是接收到的消息
//                //Log.e("JWebSClientService", message);
//                handleMessage(message);
//
//            }
//            @Override
//            public void onError(Exception ex) {
//                ex.printStackTrace();
//                reConnectWebSocket();
//            }
//            @Override
//            public void onClose(int code, String reason, boolean remote) {
//                System.out.println(
//                        "Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
//                                + reason);
//                reConnectWebSocket();
//            }
//
//            @Override
//            public void onWebsocketPong(WebSocket conn, Framedata f) {
//                reConnectWebSocket();
//                super.onWebsocketPong(conn, f);
//            }
//        };
//
//        try {
//            client.connectBlocking();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        //20秒发送一个心跳包
//        new Thread() {
//            @Override
//            public void run() {
//                try {
//                    if(null != client) {
//                        client.sendPing();
//                    }
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }.start();
//
//    }
//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        String msg_to_send = String.valueOf(intent.getExtras().get("msg_to_send"));
//        Log.d("消息内容", msg_to_send);
//
//        client.send(msg_to_send);
//
//        return super.onStartCommand(intent, flags, startId);
//    }
//    @Override
//    public void onDestroy() {
//        closeConnect();
//        super.onDestroy();
//    }
//
//    private void reConnectWebSocket() {
//        if (null != client && !client.isOpen()) {
//            //LogUtils.showLog("socket onStartConnect");
//            new Thread() {
//                @Override
//                public void run() {
//                    client.reconnect();
//                }
//            }.start();
//
//        }
//    }
//
//    private void closeConnect() {
//        try {
//            if (null != client) {
//                client.close();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            client = null;
//        }
//    }
//
//    void handleMessage(String msg) {
//        Intent intent = new Intent("cn.fitcan.action.HANDLE_WS_MESSAGE");
//        intent.putExtra("msg", msg);
//        intent.setPackage(getPackageName());
//        sendBroadcast(intent);
//    }
//
//    //用于Activity和service通讯
//    class JWebSocketClientBinder extends Binder {
//        public JWebSocketClientService getService() {
//            return JWebSocketClientService.this;
//        }
//    }
//
//    @Override
//    public IBinder onBind(Intent intent) {
//        return new MyBinder();
//    }
//
//    public class MyBinder extends Binder {
//        public void sendMessage(String msg_to_send){
//            client.send(msg_to_send);
//        }
//    }
//}




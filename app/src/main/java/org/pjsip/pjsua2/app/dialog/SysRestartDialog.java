


package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;

public class SysRestartDialog extends Dialog {

    private SysRestartDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private SysRestartDialog mDialog;

        private LinearLayout mLineBkg;
        private TextView mTitle;
        private TextView mContent;
        private Button mConfirmButton;


        public Builder(Context context,String content) {
            mContext = context;
            mDialog = new SysRestartDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_sys_restart, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mContent = mLayout.findViewById(R.id.msgContent);
            mContent.setText(content);
            mConfirmButton = mLayout.findViewById(R.id.msgConfirmButton);

            if(false){//if(GlobalVariable.viewmodel == 2){
                mLineBkg = mLayout.findViewById(R.id.sysRestartLineBkg);
                mLineBkg.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_thin_ctgray));
                mTitle = mLayout.findViewById(R.id.sysRestartTitle);
                mTitle.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mContent.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mConfirmButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector));
                mConfirmButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
            }
        }
        public SysRestartDialog create() {
            mConfirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //发个消息给MainActivity通知关闭当前软件

                    Message m2 = Message.obtain(MainActivity.handler_,1017,null);
                    m2.sendToTarget();
                    mDialog.dismiss();
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog

            return mDialog;
        }
    }

}
package org.pjsip.pjsua2.app.util;

import android.util.Log;

import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Collections;
import java.util.List;

public class IpUtils {
    public static String getLocalIpAddress() {

        try {
            String ipv4;
            List<NetworkInterface> nilist = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface ni: nilist)
            {
                List<InetAddress>  ialist = Collections.list(ni.getInetAddresses());
                for (InetAddress address: ialist)
                {
                    String xip = address.getHostAddress();
                    if (!address.isLoopbackAddress() && isIPv4Address(ipv4=address.getHostAddress()))
                    {
                        return ipv4;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("IPCONF", ex.toString());
        }
        return null;
    }
    public static boolean isIPv4Address(@NotNull String ipAddress){
        if((! ipAddress.contains(":"))&& ipAddress.contains(".")){
            String[] arry=ipAddress.split("\\.");
            return arry.length==4;
        }
        return false;
    }

}

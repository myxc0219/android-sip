package org.pjsip.pjsua2.app.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PlatformManagerUtils {
    /**
     * LED设备
     * 默认的LED设备，话机上的LED
     */
    public static final int LED_DEVICE_DEFAULT = 0;
    /**
     * USB耳机的的LED
     */
    public static final int LED_DEVICE_USB_HEADSET = 1;
    /**
     * LED类型
     * 电源灯
     */
    public static final int LED_TYPE_POWER = 0;
    /**
     * LED类型
     * 静音灯
     */
    public static final int LED_TYPE_MUTE = 1;
    /**
     * LED类型
     * 耳机键灯
     */
    public static final int LED_TYPE_HEADSET = 2;
    /**
     * LED类型
     * 免提键灯
     */
    public static final int LED_TYPE_SPEAKER = 3;
    /**
     * LED类型
     * USB耳机来电灯
     */
    public static final int LED_TYPE_RING = 4;
    /**
     * LED状态
     * 关闭
     */
    public static final int LED_STATE_OFF = 0;
    /**
     * LED状态
     * 打开
     */
    public static final int LED_STATE_ON = 1;
    /**
     * LED状态
     * 闪烁
     */
    public static final int LED_STATE_BLINK = 2;
    /**
     * USB连接模式
     * HOST
     */
    public static final int USB_MODE_HOST = 0;
    /**
     * USB连接模式
     * DEVICE
     */
    public static final int USB_MODE_DEVICE = 1;



    private static Method getLedState;
    private static Method setLedState;
    private static Method getUsbMode;
    private static Method setUsbMode;
    private static Method systemUpgrade;
    private static Method forceTimeRefresh;
    private static Method notifyCallState;

    static {
        try {
            Class<?> cls = Class.forName("android.os.PlatformManager");
            Method M[] = cls.getMethods();
            for (Method m : M) {
                String n = m.getName();
                if (n.equals("getLedState")) {
                    getLedState = m;
                } else if (n.equals("setLedState")) {
                    setLedState = m;
                } else if (n.equals("getUsbMode")) {
                    getUsbMode = m;
                } else if (n.equals("setUsbMode")) {
                    setUsbMode = m;
                } else if (n.equals("systemUpgrade")) {
                    systemUpgrade = m;
                } else if (n.equals("forceTimeRefresh")) {
                    forceTimeRefresh = m;
                } else if (n.equals("notifyCallState")) {
                    notifyCallState = m;
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }





    public static int getLedState(Object owner,int device, int type) {
        try {
            return (int)getLedState.invoke(owner, device,type);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return -1;
    }
    /**
     * 设置LED的状态
     * X7A暂不支持设置颜色和闪烁，闪烁可通过循环设置开关实现
     * @param device LED设备
     * @param type LED类型
     * @param state LED状态
     * @param on 闪烁时 LED亮的时间(单位毫秒)
     * @param off 闪烁时 LED灭的时间(单位毫秒)
     * @param color LED颜色 默认值为0
     * @return
     */
    public static boolean setLedState(Object owner,int device, int type, int state, int on, int off,
                               int color) {

        try {
            return (boolean)setLedState.invoke(owner, device,type,state,on,off,color);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return false;

    }
    /**
     * 获取USB连接模式
     * @return USB连接模式 {@link #USB_MODE_HOST} or {@link #USB_MODE_DEVICE}
     */
    public static int getUsbMode(Object owner) {
        try {
            return (int)getUsbMode.invoke(owner);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     *
     * @param mode {@link #USB_MODE_HOST} or {@link #USB_MODE_DEVICE}
     * @return 返回值忽略
     */
    public static int setUsbMode(Object owner,int mode) {
        try {
            return (int)setUsbMode.invoke(owner,mode);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return -1;
    }
    /**
     * 系统版本升级
     * @param otaPath 升级文件的路径
     * 升级进度获取 android.intent.action.SYSTEM_UPGRADE_STATE_CHANGED
     * 升级完成结果获取 android.intent.action.SYSTEM_UPGRADE_COMPLETED
     */
    public static void systemUpgrade(Object owner,String otaPath) {
        try {
            systemUpgrade.invoke(owner,otaPath);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    /**
     * SNTP时间更新
     * @param server 时间服务器地址
     * @param timeout 超时时间
     * @return 刷新后的当前时间的毫秒数 -1表示时间更新失败
     */
    public static long forceTimeRefresh(Object owner,String server, int timeout) {
        try {
            return (long)forceTimeRefresh.invoke(owner,server,timeout);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return -1L;
    }
//    /**
//     * 同步自定义的通话状态到系统
//     * @param state 通话状态 @link TelephonyManager#CALL_STATE_IDLE} {@link TelephonyManager#CALL_STATE_RINGING} {@link TelephonyManager#CALL_STATE_OFFHOOK}
//     * @param incomingNumber 来电号码
//     */
    public static void notifyCallState(Object owner,int state, String incomingNumber) {
        try {
            notifyCallState.invoke(owner,state,incomingNumber);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }




}

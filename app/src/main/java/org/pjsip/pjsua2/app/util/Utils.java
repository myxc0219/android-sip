package org.pjsip.pjsua2.app.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utils {
    public static boolean debug = false;
    private static String TAG = "Utils";

    public static File createFile(String name) {
        File file = new File(Constant.TMP_DIRPATH);
        if (!file.exists()) {
            boolean suc = file.mkdirs();
            Log.i(TAG,"mkdirs suc =" + suc);
        }

        String filePath = Constant.TMP_DIRPATH + name;
        File objFile = new File(filePath);

        if (objFile.exists()) {
            objFile.delete();
        }

        try {
            objFile.createNewFile();
            return objFile;
        } catch (Exception e) {
            Log.e(TAG,"createFile error =" + e);
        }

        return null;
    }

    public static File getFile(String name) {
        File file = new File(Constant.TMP_DIRPATH);
        if (!file.exists()) {
            boolean suc = file.mkdirs();
            Log.i(TAG,"mkdirs suc =" + suc);
        }
        String filePath = Constant.TMP_DIRPATH + name;
        return new File(filePath);
    }

    public static String processCmd(String cmd) {
        Log.i(TAG,"processCmd =");
        InputStream is = null;
        try {
            String[] runCmd = new String[]{"/bin/sh", "-c", cmd};
            Process process = Runtime.getRuntime().exec(runCmd);
            is = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String data = reader.readLine();
            Log.i(TAG,"write into RK==" + data);
            is.close();
            return data;
        } catch (Exception e) {
            Log.e(TAG,"processCMD error == " + e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                is = null;
            }
        }
        return null;
    }

    public static String processSystemCmd(String cmd) {
        Log.i(TAG,"processCmd =");
        InputStream is = null;
        try {
            String[] runCmd = new String[]{"/system/bin/sh", "-c", cmd};
            Process process = Runtime.getRuntime().exec(runCmd);
            is = process.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String data = reader.readLine();
            Log.i(TAG,"write into RK==" + data);
            is.close();
            return data;
        } catch (Exception e) {
            Log.e(TAG,"processCMD error == " + e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                is = null;
            }
        }
        return null;
    }

    public static int justProcessCmd(String cmd) {
        try {
            String[] runCmd = new String[]{"/bin/sh", "-c", cmd};
            Process process = Runtime.getRuntime().exec(runCmd);
            return process.waitFor();//0--suc 1--fail
        } catch (Exception e) {
            Log.e(TAG,"processCMD error == " + e);
        }
        return 1;
    }
}

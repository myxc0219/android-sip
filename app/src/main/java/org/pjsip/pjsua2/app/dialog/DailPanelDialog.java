package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;

import java.util.ArrayList;
import java.util.List;

public class DailPanelDialog extends Dialog {

    private DailPanelDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        public static View mLayout;
        private Context mContext;
        private DailPanelDialog mDialog;
        private List<Button> clickButtonList = new ArrayList<Button>();

        public static MainActivity.MyCallWithState mCallWithState;


        public Builder(Context context,MainActivity.MyCallWithState callWithState) {
            mContext = context;

            mCallWithState = callWithState;
            mDialog = new DailPanelDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_dail_panel, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            clickButtonList.add(mLayout.findViewById(R.id.dailNumber1));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber2));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber3));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber4));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber5));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber6));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber7));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber8));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber9));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber0));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumberX));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumberY));


        }


        public DailPanelDialog create() {

            for(Button btn : clickButtonList) {
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //根据Button的Text决定输入值
                        Button clickbutton = (Button)v;
                        String number = (String) clickbutton.getText();
                        EditText dailtext = (EditText)mLayout.findViewById(R.id.dialNumberText);
                        dailtext.setText(dailtext.getText().toString()+number);

                        if(mCallWithState!=null){
                            String msg_to_send =
                                    "{ \"msgType\": \"DAIL OUT CALL MSG\", \"DaoBoCallUri\": \""
                                            + mCallWithState.daobocalluri
                                            + "\", \"ZhuBoCallUri\": \""
                                            + MainActivity.zhuboUri
                                            + "\", \"Pstn\": \""
                                            + mCallWithState.pstn
                                            + "\", \"OutCallUri\": \""
                                            + mCallWithState.outcalluri
                                            + "\", \"Msg\": \""
                                            + number
                                            + "\" }";
                            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);
//                            Intent intent_ack = new Intent(mContext, JWebSocketClientService.class);
//                            intent_ack.putExtra("msg_to_send", msg_to_send);
//                            mContext.startService(intent_ack);
                        }
                    }
                });
            }

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {//主界面特有的按键映射
        int keycode = event.getKeyCode();
        if(event.getAction()== KeyEvent.ACTION_DOWN)
        {
            switch(keycode)
            {
                case KeyEvent.KEYCODE_0:
                    sendClickMessage("0");
                    break;
                case KeyEvent.KEYCODE_1:
                    sendClickMessage("1");
                    break;
                case KeyEvent.KEYCODE_2:
                    sendClickMessage("2");
                    break;
                case KeyEvent.KEYCODE_3:
                    sendClickMessage("3");
                    break;
                case KeyEvent.KEYCODE_4:
                    sendClickMessage("4");
                    break;
                case KeyEvent.KEYCODE_5:
                    sendClickMessage("5");
                    break;
                case KeyEvent.KEYCODE_6:
                    sendClickMessage("6");
                    break;
                case KeyEvent.KEYCODE_7:
                    sendClickMessage("7");
                    break;
                case KeyEvent.KEYCODE_8:
                    sendClickMessage("8");
                    break;
                case KeyEvent.KEYCODE_9:
                    sendClickMessage("9");
                    break;
                case KeyEvent.KEYCODE_STAR:
                    sendClickMessage("*");
                    break;
                case KeyEvent.KEYCODE_POUND:
                    sendClickMessage("#");
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void sendClickMessage(String number){
        EditText dailtext = (EditText)Builder.mLayout.findViewById(R.id.dialNumberText);
        dailtext.setText(dailtext.getText().toString()+number);
        if(Builder.mCallWithState!=null){
            String msg_to_send =
                    "{ \"msgType\": \"DAIL OUT CALL MSG\", \"DaoBoCallUri\": \""
                            + Builder.mCallWithState.daobocalluri
                            + "\", \"ZhuBoCallUri\": \""
                            + MainActivity.zhuboUri
                            + "\", \"Pstn\": \""
                            + Builder.mCallWithState.pstn
                            + "\", \"OutCallUri\": \""
                            + Builder.mCallWithState.outcalluri
                            + "\", \"Msg\": \""
                            + number
                            + "\" }";
            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);
        }
    }
}
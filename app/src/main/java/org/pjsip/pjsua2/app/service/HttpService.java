package org.pjsip.pjsua2.app.service;

import org.pjsip.pjsua2.app.model.Pstn;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HttpService {
    @GET("top250")
    Call<Pstn> getTopMovie(@Query("start") int start, @Query("count") int count);
}

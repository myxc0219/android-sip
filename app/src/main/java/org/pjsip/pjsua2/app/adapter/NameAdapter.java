package org.pjsip.pjsua2.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.model.PersonBean;

import java.util.List;

public class NameAdapter extends BaseAdapter {
    private Context context;
    private List<PersonBean> persons;
    private LayoutInflater inflater;

    public NameAdapter(Context context, List<PersonBean> persons) {
        this.context = context;
        this.persons = persons;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return persons.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewholder = null;
        PersonBean person = persons.get(position);
        if (convertView == null) {
            viewholder = new ViewHolder();
            convertView = inflater.inflate(R.layout.contacts_list_item_test, null);
            viewholder.tv_tag = (TextView) convertView
                    .findViewById(R.id.tv_lv_item_tag);
            viewholder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_lv_item_name);

            viewholder.bt_detail = (Button) convertView
                    .findViewById(R.id.button_ContactDetail);
            //点击按钮时查找对象需要
            viewholder.bt_detail.setTag(position);

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }
        // 获取首字母的assii值
        int selection = person.getSortLetters().charAt(0);
        // 通过首字母的assii值来判断是否显示字母
        int positionForSelection = getPositionForSelection(selection);
        if (position == positionForSelection) {// 相等说明需要显示字母
            viewholder.tv_tag.setVisibility(View.VISIBLE);
            viewholder.tv_tag.setText(person.getSortLetters());
        } else {
            viewholder.tv_tag.setVisibility(View.GONE);

        }
        viewholder.tv_name.setText(person.getName());
        return convertView;
    }

    public int getPositionForSelection(int selection) {
        for (int i = 0; i < persons.size(); i++) {
            String Fpinyin = persons.get(i).getSortLetters();
            char first = Fpinyin.toUpperCase().charAt(0);
            if (first == selection) {
                return i;
            }
        }
        return -1;

    }

    class ViewHolder {
        TextView tv_tag;
        TextView tv_name;
        Button bt_detail;
    }

}

package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.util.CommonMediaManager;
import org.pjsip.pjsua2.app.util.CommonPlatformManager;

import java.util.ArrayList;
import java.util.List;

public class CtDailPanelDialog extends Dialog {

    private Context imContext;

    private Button imbtnLine1;
    private Button imbtnLine2;
    private Button imbtnLine3;
    private Button imbtnLine4;
    private Button imbtnLine5;
    private Button imbtnLine6;

    private CtDailPanelDialog(Context context, int themeResId) {
        super(context, themeResId);
        imContext = context;
    }

    @Override
    protected void onStart() {
        imbtnLine1 = findViewById(R.id.btnLine1);
        imbtnLine2 = findViewById(R.id.btnLine2);
        imbtnLine3 = findViewById(R.id.btnLine3);
        imbtnLine4 = findViewById(R.id.btnLine4);
        imbtnLine5 = findViewById(R.id.btnLine5);
        imbtnLine6 = findViewById(R.id.btnLine6);

        for(int i = 1;i < 7;i++){
            LineBkgDraw(i);
        }

    }

    private void LineBkgDraw(int i){
        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        int line = -1;
        //当前线是否是注册线路
        boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);

        Log.e("ctNumberMap",GlobalVariable.ctNumberMap.toString());
        Log.e("line_exist",i+(line_exist?"--true":"--false"));
        //当前线是否被已有电话占用
        boolean line_occupy = false;
//        try {
//            int id_index = GlobalVariable.ctIdMap.get(i);
//            MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//            line_occupy = true;
//        }catch (Exception e){
//            e.printStackTrace();
//            line_occupy = false;
//        }
        if(GlobalVariable.isDaLian){
            //判断外线号码和当前号码相同判断线路被占用
            for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                    line_occupy = true;
                }
            }
        }
        if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
            line_occupy = true;
        }
        if(false)//if(GlobalVariable.viewmodel == 2)
        {
            if(i == 1)
                imbtnLine1.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            else if(i == 2)
                imbtnLine2.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            else if(i == 3)
                imbtnLine3.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            else if(i == 4)
                imbtnLine4.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            else if(i == 5)
                imbtnLine5.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            else if(i == 6)
                imbtnLine6.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
        }
        else if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3)
        {
            if(i == 1)
                imbtnLine1.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
            else if(i == 2)
                imbtnLine2.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
            else if(i == 3)
                imbtnLine3.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
            else if(i == 4)
                imbtnLine4.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
            else if(i == 5)
                imbtnLine5.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
            else if(i == 6)
                imbtnLine6.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
        }



        if(line_occupy){
            if(i == 1)
                imbtnLine1.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_red));
            else if(i == 2)
                imbtnLine2.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_red));
            else if(i == 3)
                imbtnLine3.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_red));
            else if(i == 4)
                imbtnLine4.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_red));
            else if(i == 5)
                imbtnLine5.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_red));
            else if(i == 6)
                imbtnLine6.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_red));
        }
        if(!line_exist){
            if(i == 1)
                imbtnLine1.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_gray));
            else if(i == 2)
                imbtnLine2.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_gray));
            else if(i == 3)
                imbtnLine3.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_gray));
            else if(i == 4)
                imbtnLine4.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_gray));
            else if(i == 5)
                imbtnLine5.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_gray));
            else if(i == 6)
                imbtnLine6.setBackground(imContext.getResources().getDrawable(R.drawable.button_selector_gray));
        }
    }

    public static class Builder {

        public static View mLayout;
        private Context mContext;
        private CtDailPanelDialog mDialog;
        private List<Button> clickButtonList = new ArrayList<Button>();

        private Button mbtnLine1;
        private Button mbtnLine2;
        private Button mbtnLine3;
        private Button mbtnLine4;
        private Button mbtnLine5;
        private Button mbtnLine6;

        public static MainActivity.MyCallWithState mCallWithState;


        public Builder(Context context,MainActivity.MyCallWithState callWithState,String keycode) {
            mContext = context;
            mCallWithState = callWithState;
            mDialog = new CtDailPanelDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            if(GlobalVariable.viewmodel == 2)
                mLayout = inflater.inflate(R.layout.dlg_ct_dail_panel, null, false);
            else if(GlobalVariable.viewmodel == 3)
                mLayout = inflater.inflate(R.layout.dlg_gs_dail_panel, null, false);


            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            clickButtonList.add(mLayout.findViewById(R.id.dailNumber1));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber2));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber3));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber4));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber5));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber6));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber7));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber8));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber9));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumber0));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumberX));
            clickButtonList.add(mLayout.findViewById(R.id.dailNumberY));

            mbtnLine1 = mLayout.findViewById(R.id.btnLine1);
            mbtnLine2 = mLayout.findViewById(R.id.btnLine2);
            mbtnLine3 = mLayout.findViewById(R.id.btnLine3);
            mbtnLine4 = mLayout.findViewById(R.id.btnLine4);
            mbtnLine5 = mLayout.findViewById(R.id.btnLine5);
            mbtnLine6 = mLayout.findViewById(R.id.btnLine6);

            for(int i = 1;i < 7;i++){
                LineBkgDraw(i);
            }

            for(int i = 1; i<7; i++) {
                String line_num = "";
                String tail_num = "";

                if (GlobalVariable.ctNumberMapRevert.get(i) == null) {
                    tail_num = "";
                } else {
                    tail_num = "\n"+GlobalVariable.ctNumberMapRevert.get(i);
                }
                if(i==1){
                    mbtnLine1.setText("线路1"+tail_num);
                }
                else if(i==2){
                    mbtnLine2.setText("线路2"+tail_num);
                }
                else if(i==3){
                    mbtnLine3.setText("线路3"+tail_num);
                }
                else if(i==4){
                    mbtnLine4.setText("线路4"+tail_num);
                }
                else if(i==5){
                    mbtnLine5.setText("线路5"+tail_num);
                }
                else if(i==6){
                    mbtnLine6.setText("线路6"+tail_num);
                }
            }


            EditText dailtext = (EditText)mLayout.findViewById(R.id.dialNumberText);
            dailtext.setText(dailtext.getText().toString()+keycode);
        }

        private void LineBkgDraw(int i){
            EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
            int line = -1;
            //当前线是否是注册线路
            boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);
            //当前线是否被已有电话占用
            boolean line_occupy = false;
//            try {
//                int id_index = GlobalVariable.ctIdMap.get(i);
//                MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//                line_occupy = true;
//            }catch (Exception e){
//                e.printStackTrace();
//                line_occupy = false;
//            }
            if(GlobalVariable.isDaLian){
                //判断外线号码和当前号码相同判断线路被占用
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                        line_occupy = true;
                    }
                }
            }
            if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
                line_occupy = true;
            }
            if(false){//if(GlobalVariable.viewmodel == 2)
                if(i == 1)
                    mbtnLine1.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
                else if(i == 2)
                    mbtnLine2.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
                else if(i == 3)
                    mbtnLine3.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
                else if(i == 4)
                    mbtnLine4.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
                else if(i == 5)
                    mbtnLine5.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
                else if(i == 6)
                    mbtnLine6.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray));
            }
            else if(GlobalVariable.viewmodel == 3 || GlobalVariable.viewmodel == 2){
                if(i == 1)
                    mbtnLine1.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
                else if(i == 2)
                    mbtnLine2.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
                else if(i == 3)
                    mbtnLine3.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
                else if(i == 4)
                    mbtnLine4.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
                else if(i == 5)
                    mbtnLine5.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
                else if(i == 6)
                    mbtnLine6.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_radius_half));
            }



            if(line_occupy){
                if(i == 1)
                    mbtnLine1.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_red));
                else if(i == 2)
                    mbtnLine2.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_red));
                else if(i == 3)
                    mbtnLine3.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_red));
                else if(i == 4)
                    mbtnLine4.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_red));
                else if(i == 5)
                    mbtnLine5.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_red));
                else if(i == 6)
                    mbtnLine6.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_red));
            }
            if(!line_exist){
                if(i == 1)
                    mbtnLine1.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_gray));
                else if(i == 2)
                    mbtnLine2.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_gray));
                else if(i == 3)
                    mbtnLine3.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_gray));
                else if(i == 4)
                    mbtnLine4.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_gray));
                else if(i == 5)
                    mbtnLine5.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_gray));
                else if(i == 6)
                    mbtnLine6.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_gray));
            }
        }

        private void LineMakeCall(int i){
            EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
            int line = -1;
            //当前线是否是注册线路
            boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);
            //当前线是否被已有电话占用
            boolean line_occupy = false;
//            try {
//                int id_index = GlobalVariable.ctIdMap.get(i);
//                MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//                line_occupy = true;
//            }catch (Exception e){
//                e.printStackTrace();
//                line_occupy = false;
//            }
            if(GlobalVariable.isDaLian){
                //判断外线号码和当前号码相同判断线路被占用
                for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                    if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                        line_occupy = true;
                    }
                }
            }
            if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
                line_occupy = true;
            }
            if(line_exist && !line_occupy){
                line = i;
                //当前线路未被占用，直接呼出，否则提示当前线路不可用
                String contacturi = GlobalVariable.ctNumberMapRevert.get(line);
                Integer prefix_id = GlobalVariable.ctNumberMap.get(contacturi);
                String prefix = "0"+prefix_id;
                //EditText dailtext = mDialog.findViewById(R.id.dialNumberText);
                String phonenumber = "";
                phonenumber = prefix+ dailtext.getText().toString();

                String daoboUri = "";
                String zhuboUri = "";
                if(GlobalVariable.charactername.equals("director"))
                {
                    daoboUri = MainActivity.localUri;
                }
                else
                {
                    zhuboUri = MainActivity.zhuboUri;
                }
                phonenumber = phonenumber.replace("#","A");
                String msg_to_send =
                        "{ \"msgType\": \"DAIL OUT CALL\", " +
                                "\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
                                "\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
                                "\"Pstn\": \""+ phonenumber+ "\", " +
                                "\"contacturi\": \""+ contacturi+ "\"" +
                                " }";
                if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                List<String> numberlist = new ArrayList<>();
                numberlist.add("<sip:"+phonenumber+"@"+GlobalVariable.EUSERVER+">");
                numberlist.add(contacturi);
                Message m2 = Message.obtain(MainActivity.handler_,1008,numberlist);
                m2.sendToTarget();

                Intent intent = new Intent(mContext, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                mContext.startActivity(intent);

                mDialog.dismiss();
            }
            else{
                //提示线路不可用
                MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(mContext,"当前线路不可用").create();
                msgNotifyDialog.show();
                if(GlobalVariable.viewmodel == 1)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                else if(GlobalVariable.viewmodel == 2)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
                else if(GlobalVariable.viewmodel == 3)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
                msgNotifyDialog.getWindow().setLayout(500, 400);
            }
        }


        public CtDailPanelDialog create() {

            for(Button btn : clickButtonList) {
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        //根据Button的Text决定输入值
                        Button clickbutton = (Button)v;
                        String number = (String) clickbutton.getText();
                        EditText dailtext = (EditText)mLayout.findViewById(R.id.dialNumberText);
                        dailtext.setText(dailtext.getText().toString()+number);

                        if(mCallWithState!=null){
                            String msg_to_send =
                                    "{ \"msgType\": \"DAIL OUT CALL MSG\", \"DaoBoCallUri\": \""
                                            + mCallWithState.daobocalluri
                                            + "\", \"ZhuBoCallUri\": \""
                                            + MainActivity.zhuboUri
                                            + "\", \"Pstn\": \""
                                            + mCallWithState.pstn
                                            + "\", \"OutCallUri\": \""
                                            + mCallWithState.outcalluri
                                            + "\", \"Msg\": \""
                                            + number
                                            + "\" }";
                            if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);
//                            Intent intent_ack = new Intent(mContext, JWebSocketClientService.class);
//                            intent_ack.putExtra("msg_to_send", msg_to_send);
//                            mContext.startService(intent_ack);
                        }
                    }
                });
            }
            mbtnLine1.setOnClickListener(v ->{
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String temp = String.valueOf(dailtext.getText());
                if(temp.equals("")||temp.equals("null")){
                    return;
                }
                if(GlobalVariable.diallinemodel == 1){
                    int i = 1;
                    LineMakeCall(i);
                }
            });
            mbtnLine2.setOnClickListener(v ->{
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String temp = String.valueOf(dailtext.getText());
                if(temp.equals("")||temp.equals("null")){
                    return;
                }
                if(GlobalVariable.diallinemodel == 1){
                    int i = 2;
                    LineMakeCall(i);
                }
            });
            mbtnLine3.setOnClickListener(v ->{
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String temp = String.valueOf(dailtext.getText());
                if(temp.equals("")||temp.equals("null")){
                    return;
                }
                if(GlobalVariable.diallinemodel == 1){
                    int i = 3;
                    LineMakeCall(i);
                }
            });
            mbtnLine4.setOnClickListener(v ->{
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String temp = String.valueOf(dailtext.getText());
                if(temp.equals("")||temp.equals("null")){
                    return;
                }
                if(GlobalVariable.diallinemodel == 1){
                    int i = 4;
                    LineMakeCall(i);
                }
            });
            mbtnLine5.setOnClickListener(v ->{
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String temp = String.valueOf(dailtext.getText());
                if(temp.equals("")||temp.equals("null")){
                    return;
                }
                if(GlobalVariable.diallinemodel == 1){
                    int i = 5;
                    LineMakeCall(i);
                }
            });
            mbtnLine6.setOnClickListener(v ->{
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String temp = String.valueOf(dailtext.getText());
                if(temp.equals("")||temp.equals("null")){
                    return;
                }
                if(GlobalVariable.diallinemodel == 1){
                    int i = 6;
                    LineMakeCall(i);
                }
            });

            ImageButton backSpacebutton =  mDialog.findViewById(R.id.dailBackSpace);
            backSpacebutton.setOnClickListener(v -> {
                EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                String old_value = dailtext.getText().toString();
                if(old_value.length()>0)
                {
                    dailtext.setText(old_value.substring(0,old_value.length() - 1));
                }
            });
            backSpacebutton.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    EditText dailtext = (EditText)mDialog.findViewById(R.id.dialNumberText);
                    dailtext.setText("");
                    return true;
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {//主界面特有的按键映射
        int keycode = event.getKeyCode();
        if(event.getAction()== KeyEvent.ACTION_DOWN)
        {
            switch(keycode)
            {
                case KeyEvent.KEYCODE_0:
                    sendClickMessage("0");
                    break;
                case KeyEvent.KEYCODE_1:
                    sendClickMessage("1");
                    break;
                case KeyEvent.KEYCODE_2:
                    sendClickMessage("2");
                    break;
                case KeyEvent.KEYCODE_3:
                    sendClickMessage("3");
                    break;
                case KeyEvent.KEYCODE_4:
                    sendClickMessage("4");
                    break;
                case KeyEvent.KEYCODE_5:
                    sendClickMessage("5");
                    break;
                case KeyEvent.KEYCODE_6:
                    sendClickMessage("6");
                    break;
                case KeyEvent.KEYCODE_7:
                    sendClickMessage("7");
                    break;
                case KeyEvent.KEYCODE_8:
                    sendClickMessage("8");
                    break;
                case KeyEvent.KEYCODE_9:
                    sendClickMessage("9");
                    break;
                case KeyEvent.KEYCODE_STAR:
                    sendClickMessage("*");
                    break;
                case KeyEvent.KEYCODE_POUND:
                    sendClickMessage("#");
                    break;
            }
        }
        return super.dispatchKeyEvent(event);
    }

    public void sendClickMessage(String number){
        EditText dailtext = (EditText)Builder.mLayout.findViewById(R.id.dialNumberText);
        dailtext.setText(dailtext.getText().toString()+number);
    }

    public void subNumber(View view)
    {
        EditText dailtext = (EditText)findViewById(R.id.dialNumberText);
        String old_value = dailtext.getText().toString();
        if(old_value.length()>0)
        {
            dailtext.setText(old_value.substring(0,old_value.length() - 1));
        }
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        CommonMediaManager mediaManager = CommonMediaManager.getInstance(imContext);
        CommonPlatformManager flashManager = CommonPlatformManager.getInstance(imContext);
        if(keyCode == 401){
            mediaManager.setVoiceAudioDevice(1);
            flashManager.flashSpeaker();

            Message m2 = Message.obtain(MainActivity.handler_, 2000, null);
            m2.sendToTarget();
        }
        return super.onKeyUp(keyCode, event);
    }
}
package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;


public class PhoneRTXChangeDialog extends Dialog {

    private PhoneRTXChangeDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private PhoneRTXChangeDialog mDialog;
        private LinearLayout mLineBkg;
        private TextView mTitle;
        private TextView mRXValue;
        private TextView mTXValue;
        private Button mRXdescButton;
        private Button mRXascButton;
        private Button mTXdescButton;
        private Button mTXascButton;

        private Thread getrtxvalue_thread;


        public Builder(Context context,String content) {
            mContext = context;
            mDialog = new PhoneRTXChangeDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_phonertxchange, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mRXValue = mLayout.findViewById(R.id.msgRXValue);
            mTXValue = mLayout.findViewById(R.id.msgTXValue);

            mRXdescButton = mLayout.findViewById(R.id.msgRXdescButton);
            mRXascButton = mLayout.findViewById(R.id.msgRXascButton);
            mTXdescButton = mLayout.findViewById(R.id.msgTXdescButton);
            mTXascButton = mLayout.findViewById(R.id.msgTXascButton);

            if(false){//if(GlobalVariable.viewmodel == 2){
                mLineBkg = mLayout.findViewById(R.id.msgNotifyLineBkg);
                mLineBkg.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_thin_ctgray));
                mTitle = mLayout.findViewById(R.id.msgNotifyTitle);
                mTitle.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mRXValue.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mTXValue.setTextColor(mContext.getResources().getColor(R.color.ct_gray));

                mRXdescButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mRXdescButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mRXascButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mRXascButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));

                mTXdescButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mTXdescButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mTXascButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mTXascButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
            }



        }
        public PhoneRTXChangeDialog create() {
            mRXdescButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(GlobalVariable.rxLevelValue.equals("8.0")){GlobalVariable.rxLevelValue = "5.6";}
                    else if(GlobalVariable.rxLevelValue.equals("5.6")){GlobalVariable.rxLevelValue = "4.0";}
                    else if(GlobalVariable.rxLevelValue.equals("4.0")){GlobalVariable.rxLevelValue = "2.8";}
                    else if(GlobalVariable.rxLevelValue.equals("2.8")){GlobalVariable.rxLevelValue = "2.0";}
                    else if(GlobalVariable.rxLevelValue.equals("2.0")){GlobalVariable.rxLevelValue = "1.4";}
                    else if(GlobalVariable.rxLevelValue.equals("1.4")){GlobalVariable.rxLevelValue = "1.0";}
                    else if(GlobalVariable.rxLevelValue.equals("1.0")){GlobalVariable.rxLevelValue = "0.7";}
                    else if(GlobalVariable.rxLevelValue.equals("0.7")){GlobalVariable.rxLevelValue = "0.5";}
                    else if(GlobalVariable.rxLevelValue.equals("0.5")){GlobalVariable.rxLevelValue = "0.5";}
                    String msg_to_send = "{ \"msgType\": \"SET PHONE VOLUME\", " +
                            "\"model\": \"rx\", " +
                            "\"volume\": \""+GlobalVariable.rxLevelValue+"\" }";
                    if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                    if(GlobalVariable.rxLevelValue.equals("8.0")){mRXValue.setText("输入 +12dB");}
                    else if(GlobalVariable.rxLevelValue.equals("5.6")){mRXValue.setText("输入 +9dB");}
                    else if(GlobalVariable.rxLevelValue.equals("4.0")){mRXValue.setText("输入 +6dB");}
                    else if(GlobalVariable.rxLevelValue.equals("2.8")){mRXValue.setText("输入 +3dB");}
                    else if(GlobalVariable.rxLevelValue.equals("2.0")){mRXValue.setText("输入 0dB");}
                    else if(GlobalVariable.rxLevelValue.equals("1.4")){mRXValue.setText("输入 -3dB");}
                    else if(GlobalVariable.rxLevelValue.equals("1.0")){mRXValue.setText("输入 -6dB");}
                    else if(GlobalVariable.rxLevelValue.equals("0.7")){mRXValue.setText("输入 -9dB");}
                    else if(GlobalVariable.rxLevelValue.equals("0.5")){mRXValue.setText("输入 -12dB");}
                }
            });

            mRXascButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(GlobalVariable.rxLevelValue.equals("8.0")){GlobalVariable.rxLevelValue = "8.0";}
                    else if(GlobalVariable.rxLevelValue.equals("5.6")){GlobalVariable.rxLevelValue = "8.0";}
                    else if(GlobalVariable.rxLevelValue.equals("4.0")){GlobalVariable.rxLevelValue = "5.6";}
                    else if(GlobalVariable.rxLevelValue.equals("2.8")){GlobalVariable.rxLevelValue = "4.0";}
                    else if(GlobalVariable.rxLevelValue.equals("2.0")){GlobalVariable.rxLevelValue = "2.8";}
                    else if(GlobalVariable.rxLevelValue.equals("1.4")){GlobalVariable.rxLevelValue = "2.0";}
                    else if(GlobalVariable.rxLevelValue.equals("1.0")){GlobalVariable.rxLevelValue = "1.4";}
                    else if(GlobalVariable.rxLevelValue.equals("0.7")){GlobalVariable.rxLevelValue = "1.0";}
                    else if(GlobalVariable.rxLevelValue.equals("0.5")){GlobalVariable.rxLevelValue = "0.7";}
                    String msg_to_send = "{ \"msgType\": \"SET PHONE VOLUME\", " +
                            "\"model\": \"rx\", " +
                            "\"volume\": \""+GlobalVariable.rxLevelValue+"\" }";
                    if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                    if(GlobalVariable.rxLevelValue.equals("8.0")){mRXValue.setText("输入 +12dB");}
                    else if(GlobalVariable.rxLevelValue.equals("5.6")){mRXValue.setText("输入 +9dB");}
                    else if(GlobalVariable.rxLevelValue.equals("4.0")){mRXValue.setText("输入 +6dB");}
                    else if(GlobalVariable.rxLevelValue.equals("2.8")){mRXValue.setText("输入 +3dB");}
                    else if(GlobalVariable.rxLevelValue.equals("2.0")){mRXValue.setText("输入 0dB");}
                    else if(GlobalVariable.rxLevelValue.equals("1.4")){mRXValue.setText("输入 -3dB");}
                    else if(GlobalVariable.rxLevelValue.equals("1.0")){mRXValue.setText("输入 -6dB");}
                    else if(GlobalVariable.rxLevelValue.equals("0.7")){mRXValue.setText("输入 -9dB");}
                    else if(GlobalVariable.rxLevelValue.equals("0.5")){mRXValue.setText("输入 -12dB");}

                }
            });

            mTXdescButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(GlobalVariable.txLevelValue.equals("8.0")){GlobalVariable.txLevelValue = "5.6";}
                    else if(GlobalVariable.txLevelValue.equals("5.6")){GlobalVariable.txLevelValue = "4.0";}
                    else if(GlobalVariable.txLevelValue.equals("4.0")){GlobalVariable.txLevelValue = "2.8";}
                    else if(GlobalVariable.txLevelValue.equals("2.8")){GlobalVariable.txLevelValue = "2.0";}
                    else if(GlobalVariable.txLevelValue.equals("2.0")){GlobalVariable.txLevelValue = "1.4";}
                    else if(GlobalVariable.txLevelValue.equals("1.4")){GlobalVariable.txLevelValue = "1.0";}
                    else if(GlobalVariable.txLevelValue.equals("1.0")){GlobalVariable.txLevelValue = "0.7";}
                    else if(GlobalVariable.txLevelValue.equals("0.7")){GlobalVariable.txLevelValue = "0.5";}
                    else if(GlobalVariable.txLevelValue.equals("0.5")){GlobalVariable.txLevelValue = "0.5";}
                    String msg_to_send = "{ \"msgType\": \"SET PHONE VOLUME\", " +
                            "\"model\": \"tx\", " +
                            "\"volume\": \""+GlobalVariable.txLevelValue+"\" }";
                    if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                    if(GlobalVariable.txLevelValue.equals("8.0")){mTXValue.setText("输出 +12dB");}
                    else if(GlobalVariable.txLevelValue.equals("5.6")){mTXValue.setText("输出 +9dB");}
                    else if(GlobalVariable.txLevelValue.equals("4.0")){mTXValue.setText("输出 +6dB");}
                    else if(GlobalVariable.txLevelValue.equals("2.8")){mTXValue.setText("输出 +3dB");}
                    else if(GlobalVariable.txLevelValue.equals("2.0")){mTXValue.setText("输出 0dB");}
                    else if(GlobalVariable.txLevelValue.equals("1.4")){mTXValue.setText("输出 -3dB");}
                    else if(GlobalVariable.txLevelValue.equals("1.0")){mTXValue.setText("输出 -6dB");}
                    else if(GlobalVariable.txLevelValue.equals("0.7")){mTXValue.setText("输出 -9dB");}
                    else if(GlobalVariable.txLevelValue.equals("0.5")){mTXValue.setText("输出 -12dB");}
                }
            });

            mTXascButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(GlobalVariable.txLevelValue.equals("8.0")){GlobalVariable.txLevelValue = "8.0";}
                    else if(GlobalVariable.txLevelValue.equals("5.6")){GlobalVariable.txLevelValue = "8.0";}
                    else if(GlobalVariable.txLevelValue.equals("4.0")){GlobalVariable.txLevelValue = "5.6";}
                    else if(GlobalVariable.txLevelValue.equals("2.8")){GlobalVariable.txLevelValue = "4.0";}
                    else if(GlobalVariable.txLevelValue.equals("2.0")){GlobalVariable.txLevelValue = "2.8";}
                    else if(GlobalVariable.txLevelValue.equals("1.4")){GlobalVariable.txLevelValue = "2.0";}
                    else if(GlobalVariable.txLevelValue.equals("1.0")){GlobalVariable.txLevelValue = "1.4";}
                    else if(GlobalVariable.txLevelValue.equals("0.7")){GlobalVariable.txLevelValue = "1.0";}
                    else if(GlobalVariable.txLevelValue.equals("0.5")){GlobalVariable.txLevelValue = "0.7";}
                    String msg_to_send = "{ \"msgType\": \"SET PHONE VOLUME\", " +
                            "\"model\": \"tx\", " +
                            "\"volume\": \""+GlobalVariable.txLevelValue+"\" }";
                    if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

                    if(GlobalVariable.txLevelValue.equals("8.0")){mTXValue.setText("输出 +12dB");}
                    else if(GlobalVariable.txLevelValue.equals("5.6")){mTXValue.setText("输出 +9dB");}
                    else if(GlobalVariable.txLevelValue.equals("4.0")){mTXValue.setText("输出 +6dB");}
                    else if(GlobalVariable.txLevelValue.equals("2.8")){mTXValue.setText("输出 +3dB");}
                    else if(GlobalVariable.txLevelValue.equals("2.0")){mTXValue.setText("输出 0dB");}
                    else if(GlobalVariable.txLevelValue.equals("1.4")){mTXValue.setText("输出 -3dB");}
                    else if(GlobalVariable.txLevelValue.equals("1.0")){mTXValue.setText("输出 -6dB");}
                    else if(GlobalVariable.txLevelValue.equals("0.7")){mTXValue.setText("输出 -9dB");}
                    else if(GlobalVariable.txLevelValue.equals("0.5")){mTXValue.setText("输出 -12dB");}
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            //mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }

    }

}
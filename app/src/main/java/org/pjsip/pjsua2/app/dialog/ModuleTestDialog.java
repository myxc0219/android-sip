package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;

public class ModuleTestDialog extends Dialog {


    public void showKeyboard() {
        TextView editText = findViewById(R.id.moduleEt1);
        editText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //editText.requestFocus();
                InputMethodManager keyboard = (InputMethodManager)editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                boolean rsOpenSoft = keyboard.showSoftInput(editText, 0);
                Log.e("打开软键盘结果",rsOpenSoft?"成功":"失败");
            }
        });
        editText.postDelayed(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                editText.requestFocus();
                InputMethodManager keyboard = (InputMethodManager)editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                boolean rsOpenSoft = keyboard.showSoftInput(editText, 0);
                Log.e("打开软键盘结果",rsOpenSoft?"成功":"失败");
            }
        },200);
    }

    private ModuleTestDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;
        private ModuleTestDialog mDialog;

        public Builder(Context context) {
            mContext = context;
            mDialog = new ModuleTestDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_module_test, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        public ModuleTestDialog create() {

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog

            return mDialog;
        }
    }

}

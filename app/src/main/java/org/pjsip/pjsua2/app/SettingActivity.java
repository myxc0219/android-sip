package org.pjsip.pjsua2.app;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.alibaba.fastjson.JSON;

import org.json.JSONArray;
import org.json.JSONObject;
import org.pjsip.pjsua2.app.dialog.ClearAllCallDialog;
import org.pjsip.pjsua2.app.dialog.ImNotifyDialog;
import org.pjsip.pjsua2.app.dialog.LineBusyDialog;
import org.pjsip.pjsua2.app.dialog.MsgNotifyDialog;
import org.pjsip.pjsua2.app.dialog.PhoneRTXChangeDialog;
import org.pjsip.pjsua2.app.dialog.SysRestartDialog;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.model.CallRecord;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SettingActivity extends BaseActivity implements Handler.Callback {
    public static Handler handler_;
    private final Handler handler = new Handler(this);


//    public static Integer callnum;
//    public static Integer waitnum;

    public static Integer viewmodel = 3;
    public static Integer sipmodel = 1;
    public static boolean online = true;
    public static boolean callring = true;
    public static boolean communicatering = true;
    public static boolean busystate = false;
    //全局状态
    public static boolean allowblacklist = false;
    public static boolean whitelistmodel = false;
    //public static boolean busyall = false;//20210630，添加分路置忙功能此项变成多选项
    public static Integer busyall = 0;
    public static boolean autoswitch = false;

    EditText mforceExitText;


    Button button_t_online;
    Button button_f_online;

    Button button_t_callring;
    Button button_f_callring;

    Button button_t_communicatering;
    Button button_f_communicatering;

    Button button_t_allowblacklist;
    Button button_f_allowblacklist;

    Button button_t_whitelistmodel;
    Button button_f_whitelistmodel;

    Button button_t_busyall;
    Button button_f_busyall;
    Button button_2_busyall;

    Button button_t_autoswitch;
    Button button_f_autoswitch;

    Button button_sipmodel_1;
    Button button_sipmodel_2;
    Button button_sipmodel_3;
    Button button_sipmodel_4;

    Button button_1_viewmodel;
    Button button_2_viewmodel;
    Button button_3_viewmodel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        handler_ = handler;
        if(GlobalVariable.viewmodel == 1){
            setContentView(R.layout.activity_setting);
        }
        else if(GlobalVariable.viewmodel == 2){
            setContentView(R.layout.activity_setting_ct);
        }
        else if(GlobalVariable.viewmodel == 3){
            setContentView(R.layout.activity_setting_gs);
        }

        //获取相关本机配置，也就是用户配置，每个用户对应唯一分机号
        //从数据库获得本机状态并进行相应设置
        new Thread(){
            @Override
            public void run() {
                try {
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getSysConfig",
                            "{\"userid\":\""+GlobalVariable.userid+"\"," +
                                    "\"pgmid\":\""+GlobalVariable.pgmid+"\" }"
                    );
                    JSONObject rs_json = new JSONObject(rs);
                    JSONObject rs_json_userstate = rs_json.getJSONObject("data").getJSONObject("userstate");
                    JSONObject rs_json_config = rs_json.getJSONObject("data").getJSONObject("config");

                    online = rs_json_userstate.get("online").toString().equals("0")? false:true;
                    callring = rs_json_userstate.get("callring").toString().equals("0")? false:true;
                    communicatering = rs_json_userstate.get("communicatering").toString().equals("0")? false:true;
                    busystate = rs_json_userstate.get("communicatering").toString().equals("0")? false:true;
                    GlobalVariable.callnum = Integer.parseInt(rs_json_userstate.get("callnum").toString());
                    GlobalVariable.waitnum = Integer.parseInt(rs_json_userstate.get("waitnum").toString());

                    allowblacklist = rs_json_config.get("allowblacklist").toString().equals("0")? false:true;
                    whitelistmodel = rs_json_config.get("whitelistmodel").toString().equals("0")? false:true;
                    busyall = Integer.parseInt(rs_json_config.get("busyall").toString());
                    autoswitch = rs_json_config.get("autoswitch").toString().equals("0")? false:true;
                    sipmodel = Integer.parseInt(rs_json_config.get("sipmodel").toString());
                    viewmodel = Integer.parseInt(rs_json_config.get("viewmodel").toString());

                    GlobalVariable.viewmodel=viewmodel;
                    GlobalVariable.sipmodel=sipmodel;
                    GlobalVariable.online=online;
                    GlobalVariable.callring=callring;
                    GlobalVariable.communicatering=communicatering;
                    GlobalVariable.busystate=busystate;

                    GlobalVariable.allowblacklist=allowblacklist;
                    GlobalVariable.whitelistmodel=whitelistmodel;
                    GlobalVariable.busyall=busyall;
                    //获得解析数据后更新页面
                    Message m2 = Message.obtain(SettingActivity.handler_,1001,null);
    				m2.sendToTarget();

    				//20210701  添加获得线路置忙配置
                    HttpRequest requestX = new HttpRequest();
                    String rsX = requestX.postJson(
                            "http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/getLineMapping",
                            "{}"
                    );
                    JSONObject rs_jsonX = new JSONObject(rsX);
                    Log.e("EUSERVER",GlobalVariable.EUSERVER);
                    Log.e("getLineMapping",rsX);

                    if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy1"))) == 1)
                    {GlobalVariable.linebusy1 = true;} else {GlobalVariable.linebusy1 = false;}
                    if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy2"))) == 1)
                    {GlobalVariable.linebusy2 = true;} else {GlobalVariable.linebusy2 = false;}
                    if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy3"))) == 1)
                    {GlobalVariable.linebusy3 = true;} else {GlobalVariable.linebusy3 = false;}
                    if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy4"))) == 1)
                    {GlobalVariable.linebusy4 = true;} else {GlobalVariable.linebusy4 = false;}
                    if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy5"))) == 1)
                    {GlobalVariable.linebusy5 = true;} else {GlobalVariable.linebusy5 = false;}
                    if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy6"))) == 1)
                    {GlobalVariable.linebusy6 = true;} else {GlobalVariable.linebusy6 = false;}

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        button_t_online = (Button)findViewById(R.id.button_t_online);
        button_f_online = (Button)findViewById(R.id.button_f_online);

        button_t_callring = (Button)findViewById(R.id.button_t_callring);
        button_f_callring = (Button)findViewById(R.id.button_f_callring);

        button_t_communicatering = (Button)findViewById(R.id.button_t_communicatering);
        button_f_communicatering = (Button)findViewById(R.id.button_f_communicatering);

        button_t_allowblacklist = (Button)findViewById(R.id.button_t_allowblacklist);
        button_f_allowblacklist = (Button)findViewById(R.id.button_f_allowblacklist);

        button_t_whitelistmodel = (Button)findViewById(R.id.button_t_whitelistmodel);
        button_f_whitelistmodel = (Button)findViewById(R.id.button_f_whitelistmodel);

        button_t_busyall = (Button)findViewById(R.id.button_t_busyall);
        button_f_busyall = (Button)findViewById(R.id.button_f_busyall);
        button_2_busyall = (Button)findViewById(R.id.button_2_busyall);

        button_t_autoswitch = (Button)findViewById(R.id.button_t_autoswitch);
        button_f_autoswitch = (Button)findViewById(R.id.button_f_autoswitch);

        button_sipmodel_1 = (Button)findViewById(R.id.button_sipmodel_1);
        button_sipmodel_2 = (Button)findViewById(R.id.button_sipmodel_2);
        button_sipmodel_3 = (Button)findViewById(R.id.button_sipmodel_3);
        button_sipmodel_4 = (Button)findViewById(R.id.button_sipmodel_4);

        button_1_viewmodel = (Button)findViewById(R.id.button_1_viewmodel);
        button_2_viewmodel = (Button)findViewById(R.id.button_2_viewmodel);
        button_3_viewmodel = (Button)findViewById(R.id.button_3_viewmodel);

        //设置用户信息
        TextView channelNameTv = (TextView)findViewById(R.id.settingChannelName);
        TextView sipNameTv = (TextView)findViewById(R.id.settingSipName);
        TextView characterNameTv = (TextView)findViewById(R.id.settingCharacterName);
        TextView userNameTv = (TextView)findViewById(R.id.settingUserName);

        channelNameTv.setText(GlobalVariable.channelname);
        sipNameTv.setText(GlobalVariable.sipid);
        characterNameTv.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
        userNameTv.setText(GlobalVariable.username);

        //全局信息需要从数据库获得，返回信息待拓展
        TextView callinNumTv = (TextView)findViewById(R.id.settingCallinNum);
        TextView zhuboWaitNumTv = (TextView)findViewById(R.id.settingZhuboWaitNum);
        TextView daoboWaitNumTv = (TextView)findViewById(R.id.settingDaoboWaitNum);

        //主播将自动转接项隐藏
        LinearLayout layoutAutoAccept = findViewById(R.id.autoAccept);
        LinearLayout layoutAutoAcceptRepalce = findViewById(R.id.autoAcceptReplace);
        if(GlobalVariable.charactername.equals("director")){
            layoutAutoAccept.setVisibility(View.VISIBLE);
            layoutAutoAcceptRepalce.setVisibility(View.GONE);
        }
        else{
            layoutAutoAccept.setVisibility(View.GONE);
            layoutAutoAcceptRepalce.setVisibility(View.VISIBLE);
        }

        //仅导播和无导播模式也要把自动转接项隐藏（目前先在这个模式下屏蔽了自动转接的功能）
        if( GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3 ){
            layoutAutoAccept.setVisibility(View.GONE);
            layoutAutoAcceptRepalce.setVisibility(View.VISIBLE);
        }

        if(GlobalVariable.viewmodel == 1){
            ImageButton imageButtonTitle = findViewById(R.id.buttonICON2);
            if(GlobalVariable.charactername.equals("broadcaster")) {
                //主播，仅导播模式屏蔽,
                if (GlobalVariable.sipmodel == 3){
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
                //主播，单/多导播模式屏蔽
                if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
            }
            else{
                if (GlobalVariable.sipmodel == 0){
                    imageButtonTitle.setBackgroundResource(R.color.transparent);
                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
                    imageButtonTitle.setEnabled(false);
                }
            }
        }
        else if(GlobalVariable.viewmodel == 2){
//            ImageButton imageButtonTitle = findViewById(R.id.ctTitleDail);
//            if(GlobalVariable.charactername.equals("broadcaster")) {
//                if (GlobalVariable.sipmodel == 3) {
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
//                }
//                if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) {
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
//                }
//            }
//            else{
//                if (GlobalVariable.sipmodel == 0) {
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
//                }
//            }
        }
        else if(GlobalVariable.viewmodel == 3){
//            ImageButton imageButtonTitle = findViewById(R.id.ctTitleDail);
//            if(GlobalVariable.charactername.equals("broadcaster")) {
//                if (GlobalVariable.sipmodel == 3) {
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
//                }
//                if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) {
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
//                }
//            }
//            else{
//                if (GlobalVariable.sipmodel == 0) {
//                    imageButtonTitle.setBackgroundResource(R.color.transparent);
//                    imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//                    imageButtonTitle.setEnabled(false);
//                }
//            }
        }


        TextView versionTv = findViewById(R.id.versionTv);
        versionTv.setText(GlobalVariable.version_id);

        mforceExitText = findViewById(R.id.forceExitText);



//        if(GlobalVariable.isDaLian){
//            LinearLayout sipModel = findViewById(R.id.sipModel);
//            sipModel.setVisibility(View.GONE);
//        }
    }

    public void forceExitApp(View view){
        if(mforceExitText!=null){
            if(GlobalVariable.isDL1018&&String.valueOf(mforceExitText.getText()).equals("jsbwx")){
                Message m2 = Message.obtain(MainActivity.handler_,1018,null);
                m2.sendToTarget();
            }
            else if(String.valueOf(mforceExitText.getText()).equals("admin")){
                Message m2 = Message.obtain(MainActivity.handler_,1018,null);
                m2.sendToTarget();
            }
        }
    }

    @Override
    public boolean handleMessage(@NonNull Message msg) {

        if(msg.what == 1001)
        {
            updateViewByConfig();
        }
        else if(msg.what == 1010)
        {
            if(GlobalVariable.viewmodel == 1){
                TextView timeView = (TextView) findViewById(R.id.mainTime);
                if (timeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    timeView.setText(dateString.replace(" ", "         "));
                }
                //刷新状态图标
                ImageView mainState1 = (ImageView) findViewById(R.id.MainState1);
                ImageView mainState2 = (ImageView) findViewById(R.id.MainState2);
                ImageView mainState3 = (ImageView) findViewById(R.id.MainState3);

                Button ImButton = findViewById(R.id.buttonMA6);
                if(GlobalVariable.IMRedTitle)
                {
                    if(GlobalVariable.IMRedShowed)
                    {
                        ImButton.setTextColor(Color.WHITE);
                        GlobalVariable.IMRedShowed = false;
                    }
                    else
                    {
                        ImButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.IMRedShowed = true;
                    }
                }
                else{
                    ImButton.setTextColor(Color.WHITE);
                }

                /**有未接听电话首页图标闪烁**/
                Button PhoneButton = findViewById(R.id.buttonMA3);
                if(GlobalVariable.PhoneRedTitle){
                    if(GlobalVariable.PhoneRedShowed2){
                        PhoneButton.setTextColor(Color.WHITE);
                        GlobalVariable.PhoneRedShowed2 = false;
                    }else{
                        PhoneButton.setTextColor(getResources().getColor(R.color.notify_blue));
                        GlobalVariable.PhoneRedShowed2 = true;
                    }
                }else{
                    PhoneButton.setTextColor(Color.WHITE);
                }

                if(GlobalVariable.callring)
                {
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.allowring));
                }
                else{
                    mainState1.setImageDrawable(getResources().getDrawable(R.drawable.disablering));
                }

                if(GlobalVariable.whitelistmodel)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.whitelistmodel));
                }
                else if(GlobalVariable.allowblacklist)
                {
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.allowblack));
                }
                else{
                    mainState2.setImageDrawable(getResources().getDrawable(R.drawable.disableblack));
                }

                if(!GlobalVariable.online)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.pause));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall == 1)
                {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.busy));
                    mainState3.setColorFilter(Color.RED);
                }
                else if(GlobalVariable.busyall==2) {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.linebusy));
                    mainState3.setColorFilter(Color.TRANSPARENT);
                }
                else {
                    mainState3.setImageDrawable(getResources().getDrawable(R.drawable.allowin));
                    mainState3.setColorFilter(Color.WHITE);
                }

                TextView mainChannel = findViewById(R.id.mainChannel);
                TextView mainSipID = findViewById(R.id.mainSipID);
                mainChannel.setText(GlobalVariable.channelname);
                mainSipID.setText(GlobalVariable.user_sipid);
            }
            else if(GlobalVariable.viewmodel == 2) {

                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
                ct_title_name.setText(GlobalVariable.username);

                Typeface typeface_fitcan = Typeface.createFromAsset(this.getAssets(), "fonts/BGOTHM.TTF");
                ct_title_username.setTypeface(typeface_fitcan);
                ct_title_name.setTypeface(typeface_fitcan);
                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_busy));
                } else if(GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_pdr));
                }
            }
            else if(GlobalVariable.viewmodel == 3) {
                /**耦合器状态栏显示频道和sipid**/
                TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
                TextView ct_title_username = findViewById(R.id.ct_title_username);
                TextView ct_title_character = findViewById(R.id.ct_title_charactername);
                TextView ct_title_name = findViewById(R.id.ct_title_name);

                ct_title_channelname.setText(GlobalVariable.channelname);
                ct_title_username.setText(GlobalVariable.user_sipid);
                ct_title_character.setText(GlobalVariable.charactername.equals("director") ? "导播" : "主播");
                ct_title_name.setText(GlobalVariable.username);

                /**耦合器状态栏显示系统时间**/
                TextView ctTimeView = (TextView) findViewById(R.id.ct_title_systime);
                Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
                ctTimeView.setTypeface(typeface);
                if (ctTimeView != null) {
                    Date date = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = sdf.format(date);
                    ctTimeView.setText(dateString);
                }
                /**耦合器状态栏状态图标刷新**/
                /**状态栏三个状态图标刷新**/
                ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
                ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
                ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
                ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
                if (GlobalVariable.callring) {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowring));
                } else {
                    ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disablering));
                }
                if (GlobalVariable.whitelistmodel) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_whitelistmodel));
                } else if (GlobalVariable.allowblacklist) {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowblack));
                } else {
                    ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disableblack));
                }
                if (!GlobalVariable.online) {
                    //ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
                } else if (GlobalVariable.busyall == 1) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_busy));
                } else if (GlobalVariable.busyall == 2) {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_linebusy));
                } else {
                    ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowin));
                }

                if (GlobalVariable.sipmodel == 0) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_mbc));
                } else if (GlobalVariable.sipmodel == 1) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_single));
                } else if (GlobalVariable.sipmodel == 2) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_multi));
                } else if (GlobalVariable.sipmodel == 3) {
                    ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_pdr));
                }
            }

        }
        else if(msg.what == 1011) {
            if(GlobalVariable.viewmodel == 1) {
                ImageView titleImage = findViewById(R.id.titleImage);
                if (String.valueOf(msg.obj).equals("success")) {
                    titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo));
                } else {
                    titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo_red));
                }
            }
            else if(GlobalVariable.viewmodel == 2){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(msg.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }
            else if(GlobalVariable.viewmodel == 3){
                TextView tvchannel = findViewById(R.id.ct_title_channelname);
                if(String.valueOf(msg.obj).equals("success")) {
                    tvchannel.setTextColor(getResources().getColor(R.color.white));
                }
                else{
                    tvchannel.setTextColor(Color.RED);
                }
            }
        }
        else if(msg.what == 1005){
            if(msg.obj!=null && GlobalVariable.prm!=null){
                //2021-04-09提醒内容更改为自定义格式
                //OnInstantMessageParam prm = (OnInstantMessageParam) msg.obj;
//                OnInstantMessageParam prm = GlobalVariable.prm;
//                String msgContent = "";
//                String username = "";
//                String template = "";
//                int type = -1;
//                try {
//                    JSONObject msg_json = new JSONObject(prm.getMsgBody());
//                    msgContent = msg_json.get("msg").toString();
//                    username = msg_json.get("username").toString();
//                    template = msg_json.get("template").toString();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(this,msgContent,username,template).create();
                ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(
                        SettingActivity.this,
                        GlobalVariable.im_msg,
                        GlobalVariable.im_username,
                        GlobalVariable.im_template).create();
                imNotifyDialog.show();
                imNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                imNotifyDialog.getWindow().setLayout(500, 300);
            }
        }
        else if(msg.what == 1002){
            if(GlobalVariable.modelchange){
                //弹出提示框，提示系统模式已更改，请重新登录
                SysRestartDialog sysRestartDialog = new SysRestartDialog.Builder(SettingActivity.this,"保存成功需重新登陆").create();
                sysRestartDialog.show();
                if(GlobalVariable.viewmodel == 1)
                    sysRestartDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                else if(GlobalVariable.viewmodel == 2)
                    sysRestartDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
                else if(GlobalVariable.viewmodel == 3)
                    sysRestartDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
                sysRestartDialog.getWindow().setLayout(500, 400);

            }
            else{
                MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(SettingActivity.this,"保存成功").create();
                msgNotifyDialog.show();
                if(GlobalVariable.viewmodel == 1)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                else if(GlobalVariable.viewmodel == 2)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
                else if(GlobalVariable.viewmodel ==3)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
                msgNotifyDialog.getWindow().setLayout(500, 300);
                //还需要
                //msgNotifyDialog.dismiss();

                final Timer t = new Timer();
                t.schedule(new TimerTask() {
                    public void run() {
                        msgNotifyDialog.dismiss();
                        showMainActivity(null);
                        t.cancel();
                    }
                }, 2000);
            }
        }
        else if(msg.what == 1003) {
            TextView  settingCallinNumTv = findViewById(R.id.settingCallinNum);
            TextView  settingZhuboWaitNumTv = findViewById(R.id.settingZhuboWaitNum);
            settingCallinNumTv.setText(GlobalVariable.sip_totalnum);
            settingZhuboWaitNumTv.setText(GlobalVariable.sip_tozhubonum);
        }
        else if(msg.what == 1004) {
            busyallChange0(null);
        }
        else if(msg.what == 1014){
            String msg_content = String.valueOf(msg.obj);
            MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"系统状态已更改\n"+msg_content).create();
            msgNotifyDialog.show();
            if(GlobalVariable.viewmodel == 1)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
            else if(GlobalVariable.viewmodel == 2)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
            else if(GlobalVariable.viewmodel == 3)
                msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
            msgNotifyDialog.getWindow().setLayout(500, 400);
            final Timer t = new Timer();
            t.schedule(new TimerTask() {
                public void run() {
                    msgNotifyDialog.dismiss();
                    t.cancel();
                }
            }, 2000);
        }
        else if(msg.what == 1015){
            showMainActivity(null);
        }
        else if(msg.what == 1016){
            new Thread(){
                @Override
                public void run() {
                    try {
                        int currentpage= 1;
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getHistoryByUser",
                                "{\"userid\":\""+GlobalVariable.userid+"\", " +
                                        "\"state\": \"-1\", " +
                                        "\"operatetype\": \"-1\", " +
                                        "\"page\": \""+currentpage+"\", " +
                                        "\"pageSize\": \"8\", " +
                                        "\"pgmid\": \""+GlobalVariable.pgmid+"\", " +
                                        "\"character\": \""+GlobalVariable.charactername+"\"}"
                        );
                        JSONObject rs_json = new JSONObject(rs);
                        JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
                        List<CallRecord> callrecordlist = new ArrayList<>();
                        callrecordlist = JSON.parseArray(rs_json_callrecordlist.toString(), CallRecord.class);

                        for(CallRecord callRecord : callrecordlist){
                            if(callRecord.type != MainActivity.CALL_TYPE.ENDCALL) {
                                //updateCallRecordST(callRecord.callid, 1, MainActivity.CALL_TYPE.ENDCALL);
                                String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
                                        + "<sip:" + callRecord.callingnumber + "@" + GlobalVariable.SIPSERVER +">"
                                        + "\", \"DaoBoCallUri\": \""
                                        + MainActivity.localUri
                                        + "\", \"ZhuBoCallUri\": \""
                                        + MainActivity.zhuboUri
                                        + "\", \"VirtualUri\": \""
                                        + null
                                        + "\" }";
                                if (MainActivity.wsControl != null)
                                    MainActivity.wsControl.sendMessage(msg_to_send);

                                String msg_to_send1 = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
                                        + "<sip:" + callRecord.callingnumber + "@" + GlobalVariable.SIPSERVER +">"
                                        + "\", \"DaoBoCallUri\": \""
                                        + MainActivity.localUri
                                        + "\", \"ZhuBoCallUri\": \""
                                        + MainActivity.zhuboUri
                                        + "\", \"VirtualUri\": \""
                                        + null
                                        + "\" }";
                                if (MainActivity.wsControl != null)
                                    MainActivity.wsControl.sendMessage(msg_to_send1);

                                String msg_to_send2 = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
                                        + "<sip:" + callRecord.callingnumber + "@" + GlobalVariable.VOSEVER +">"
                                        + "\", \"DaoBoCallUri\": \""
                                        + MainActivity.localUri
                                        + "\", \"ZhuBoCallUri\": \""
                                        + MainActivity.zhuboUri
                                        + "\", \"VirtualUri\": \""
                                        + null
                                        + "\" }";
                                if (MainActivity.wsControl != null)
                                    MainActivity.wsControl.sendMessage(msg_to_send2);

                                String msg_to_send3 = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
                                        + "<sip:" + callRecord.callingnumber + "@" + GlobalVariable.VOSEVER +">"
                                        + "\", \"DaoBoCallUri\": \""
                                        + MainActivity.localUri
                                        + "\", \"ZhuBoCallUri\": \""
                                        + MainActivity.zhuboUri
                                        + "\", \"VirtualUri\": \""
                                        + null
                                        + "\" }";
                                if (MainActivity.wsControl != null)
                                    MainActivity.wsControl.sendMessage(msg_to_send3);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }.start();
        }
        return true;
    }

    @Override
    protected void onStart() {
        TextView phonertxchangeView = findViewById(R.id.phonertxchange);
        if(phonertxchangeView!=null){
            phonertxchangeView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //调整话机的输入输出音量
                    PhoneRTXChangeDialog myPhoneRTXChangeDialog = new PhoneRTXChangeDialog.Builder(SettingActivity.this,"调整话机音量").create();
                    myPhoneRTXChangeDialog.show();
                    if(GlobalVariable.viewmodel == 1){
                        myPhoneRTXChangeDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                    }
                    else if(GlobalVariable.viewmodel == 2){
                        myPhoneRTXChangeDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
                    }
                    else{
                        myPhoneRTXChangeDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
                    }
                    myPhoneRTXChangeDialog.getWindow().setLayout(500, 300);
                    TextView mRXValue = myPhoneRTXChangeDialog.getWindow().findViewById(R.id.msgRXValue);
                    TextView mTXValue = myPhoneRTXChangeDialog.getWindow().findViewById(R.id.msgTXValue);
                    if(GlobalVariable.rxLevelValue.equals("8.0")){mRXValue.setText("输入 +12dB");}
                    else if(GlobalVariable.rxLevelValue.equals("5.6")){mRXValue.setText("输入 +9dB");}
                    else if(GlobalVariable.rxLevelValue.equals("4.0")){mRXValue.setText("输入 +6dB");}
                    else if(GlobalVariable.rxLevelValue.equals("2.8")){mRXValue.setText("输入 +3dB");}
                    else if(GlobalVariable.rxLevelValue.equals("2.0")){mRXValue.setText("输入 0dB");}
                    else if(GlobalVariable.rxLevelValue.equals("1.4")){mRXValue.setText("输入 -3dB");}
                    else if(GlobalVariable.rxLevelValue.equals("1.0")){mRXValue.setText("输入 -6dB");}
                    else if(GlobalVariable.rxLevelValue.equals("0.7")){mRXValue.setText("输入 -9dB");}
                    else if(GlobalVariable.rxLevelValue.equals("0.5")){mRXValue.setText("输入 -12dB");}

                    if(GlobalVariable.txLevelValue.equals("8.0")){mTXValue.setText("输出 +12dB");}
                    else if(GlobalVariable.txLevelValue.equals("5.6")){mTXValue.setText("输出 +9dB");}
                    else if(GlobalVariable.txLevelValue.equals("4.0")){mTXValue.setText("输出 +6dB");}
                    else if(GlobalVariable.txLevelValue.equals("2.8")){mTXValue.setText("输出 +3dB");}
                    else if(GlobalVariable.txLevelValue.equals("2.0")){mTXValue.setText("输出 0dB");}
                    else if(GlobalVariable.txLevelValue.equals("1.4")){mTXValue.setText("输出 -3dB");}
                    else if(GlobalVariable.txLevelValue.equals("1.0")){mTXValue.setText("输出 -6dB");}
                    else if(GlobalVariable.txLevelValue.equals("0.7")){mTXValue.setText("输出 -9dB");}
                    else if(GlobalVariable.txLevelValue.equals("0.5")){mTXValue.setText("输出 -12dB");}
                    return true;
                }
            });
        }

        if(GlobalVariable.isDL0117){
            if(GlobalVariable.viewmodel == 1) {
                LinearLayout clearAllCallLayout = findViewById(R.id.clearAllCall);
                clearAllCallLayout.setVisibility(View.VISIBLE);
                LinearLayout settingTopFourLayout = findViewById(R.id.settingTopFour);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 60);
                settingTopFourLayout.setLayoutParams(params);
            }
        }
        if(GlobalVariable.isDL1129 && GlobalVariable.viewmodel == 1) {

            Button innercomBtn = findViewById(R.id.buttonMA6);
            innercomBtn.setVisibility(View.GONE);

            LinearLayout viewModelLayout = findViewById(R.id.viewModel);
            viewModelLayout.setVisibility(View.GONE);

            LinearLayout sipModelLayout = findViewById(R.id.sipModel);
            sipModelLayout.setVisibility(View.GONE);

            LinearLayout autoAcceptLayout = findViewById(R.id.autoAccept);
            autoAcceptLayout.setVisibility(View.GONE);

            LinearLayout ringModelLayout = findViewById(R.id.ringModel);
            ringModelLayout.setVisibility(View.GONE);

            LinearLayout blackListModelLayout = findViewById(R.id.blackListModel);
            blackListModelLayout.setVisibility(View.GONE);

            LinearLayout whiteListModelLayout = findViewById(R.id.whiteListModel);
            whiteListModelLayout.setVisibility(View.GONE);

            LinearLayout busyModelLayout = findViewById(R.id.busyModel);
            busyModelLayout.setMinimumHeight(80);
        }

        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.ct_title_home));
            imgBtn1.setImageDrawable(getDrawable(R.drawable.ct_title_history));
            imgBtn2.setImageDrawable(getDrawable(R.drawable.ct_title_contacts));
            //imgBtn3.setImageDrawable(getDrawable(R.drawable.ct_title_setting));
            imgBtn4.setImageDrawable(getDrawable(R.drawable.ct_title_dail));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
            ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
            ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
            ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
            ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

            imgBtn0.setImageDrawable(getDrawable(R.drawable.gs_title_home));
            imgBtn1.setImageDrawable(getDrawable(R.drawable.gs_title_history));
            imgBtn2.setImageDrawable(getDrawable(R.drawable.gs_title_contacts));
            //imgBtn3.setImageDrawable(getDrawable(R.drawable.ct_title_setting));
            imgBtn4.setImageDrawable(getDrawable(R.drawable.gs_title_dail));
        }
        super.onStart();
//        Button ImButton = findViewById(R.id.buttonMA6);
//        if(GlobalVariable.IMRedTitle)
//            ImButton.setTextColor(Color.RED);
//        else
//            ImButton.setTextColor(Color.WHITE);

        //发送消息在回调中处理
        String msg_to_send =
                "{ \"msgType\": \"GET STAT NUM\"}";
        if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

    }


    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        handler_ = null;
    }

    public void showDailActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleDail);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_dail_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleDail);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_dail_disable));
        }
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, DailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showMainActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_home_disabled));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleHome);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_home_disabled));
        }
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showHistoryActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_history_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_history_disable));
        }
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showContactsActivity(View view)
    {
        if(GlobalVariable.viewmodel == 2){
            ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
            imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_contacts_disable));
        }
        else if(GlobalVariable.viewmodel == 3){
            ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
            imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_contacts_disable));
        }
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showCommunicateActivity(View view)
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, CommunicateActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }
    public void showSettingActivity(View view)
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, SettingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public void showIntercomActivity(View view)
    {
        overridePendingTransition(0, 0);
        Intent intent = new Intent(this, IntercomActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        //setContentView(layoutIntercom);
    }



    public void saveConfig(View view){
        if(GlobalVariable.sipmodel!=sipmodel || GlobalVariable.viewmodel!=viewmodel){

            if(GlobalVariable.sip_totalnum!=null && !GlobalVariable.sip_totalnum.equals("0")){
                MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(SettingActivity.this,"当前有在线电话，不允许更改模式").create();
                msgNotifyDialog.show();
                if(GlobalVariable.viewmodel == 1)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
                else if(GlobalVariable.viewmodel == 2)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
                else if(GlobalVariable.viewmodel == 3)
                    msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
                msgNotifyDialog.getWindow().setLayout(500, 300);
                return;
            }
            else {
                //修改了模式并且当前没有在线电话
                GlobalVariable.modelchange = true;
                //通知其他的电话也需要重新登录
                String msg_to_send = "{ " +
                        "\"msgType\": \"CHANGE SYS CONFIG\"," +
                        "\"config\": \"[导播模式]\"," +
                        "\"state\": \"1\"," +
                        "\"local\": \""+MainActivity.localUri+"\"" +
                        "}";
                if (MainActivity.wsControl != null) MainActivity.wsControl.sendMessage(msg_to_send);

                new Thread() {
                    @Override
                    public void run() {
                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/modelchange",
                                    "{" +
                                            "\"sipmodel\":\"" + sipmodel + "\","+
                                            "\"code\":" + 200 + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }

        }

        GlobalVariable.send_to_other_config = false;
        GlobalVariable.send_to_other_content = "";
        if(GlobalVariable.allowblacklist!=allowblacklist
            || GlobalVariable.whitelistmodel!=whitelistmodel
            || GlobalVariable.busyall!=busyall
            || (GlobalVariable.busyall==busyall && GlobalVariable.linebusy_change == true)
            || GlobalVariable.autoswitch!=autoswitch
        ) {
            if(GlobalVariable.autoswitch!=autoswitch)
                GlobalVariable.send_to_other_content +="[自动转接]\r\n";
            if(GlobalVariable.allowblacklist!=allowblacklist)
                GlobalVariable.send_to_other_content +="[黑名单]\r\n";
            if(GlobalVariable.whitelistmodel!=whitelistmodel)
                GlobalVariable.send_to_other_content +="[白名单]\r\n";
            if(GlobalVariable.busyall!=busyall && busyall == 1) {
                GlobalVariable.send_to_other_content += "[置忙]\r\n";
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/linebusy",
                                    "{\"code\":" + 200 + ","
                                            +"\"linebusy\": \""
                                            +"true" + ","
                                            +"true" + ","
                                            +"true" + ","
                                            +"true" + ","
                                            +"true" + ","
                                            +"true" + "\","
                                            +"\"busymodel\": "+ GlobalVariable.busyall
                                            + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/setbusy",
                                    "{\"code\":" + 200 + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
            if(GlobalVariable.busyall!=busyall && busyall == 0){
                GlobalVariable.send_to_other_content +="[取消置忙]\r\n";
                new Thread() {
                    @Override
                    public void run() {

                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/linebusy",
                                    "{\"code\":" + 200 + ","
                                            +"\"linebusy\": \""
                                            + "false" + ","
                                            + "false" + ","
                                            + "false" + ","
                                            + "false" + ","
                                            + "false" + ","
                                            + "false" + "\","
                                            +"\"busymodel\": "+ GlobalVariable.busyall
                                            + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/unsetbusy",
                                    "{\"code\":" + 200 + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
            }
            if(
                    (GlobalVariable.busyall!=busyall || GlobalVariable.linebusy_change )&& busyall == 2
            ) {
                GlobalVariable.send_to_other_content += "[分路置忙]\r\n";

                new Thread() {
                    @Override
                    public void run() {

                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/linebusy",
                                    "{\"code\":" + 200 + ","
                                            +"\"linebusy\": \""
                                            +(GlobalVariable.linebusy1?"true":"false") + ","
                                            +(GlobalVariable.linebusy2?"true":"false") + ","
                                            +(GlobalVariable.linebusy3?"true":"false") + ","
                                            +(GlobalVariable.linebusy4?"true":"false") + ","
                                            +(GlobalVariable.linebusy5?"true":"false") + ","
                                            +(GlobalVariable.linebusy6?"true":"false") + "\","
                                            +"\"busymodel\": "+ GlobalVariable.busyall
                                            + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            HttpRequest request = new HttpRequest();
                            String rs = request.postJson(
                                    GlobalVariable.SEHTTPServer + "/setbusy",
                                    "{\"code\":" + 200 + "}"
                            );
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

            }

            GlobalVariable.send_to_other_config = true;

        }


        new Thread() {
            @Override
            public void run() {
                try {
                    //这个时候不能改viewmodel会引起主页面刷新崩溃
                    //GlobalVariable.viewmodel=viewmodel;
                    GlobalVariable.sipmodel=sipmodel;
                    GlobalVariable.online=online;
                    GlobalVariable.callring=callring;
                    GlobalVariable.communicatering=communicatering;
                    GlobalVariable.busystate=busystate;

                    GlobalVariable.allowblacklist=allowblacklist;
                    GlobalVariable.whitelistmodel=whitelistmodel;
                    GlobalVariable.busyall=busyall;
                    if(busyall == 1){
                        HttpRequest request = new HttpRequest();
                        String rs = request.postJson(
                                "http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/updateLineMapping",
                                "{\"busy1\":\"" + 1 + "\"," +
                                        "\"busy2\":\"" + 1 + "\"," +
                                        "\"busy3\":\"" + 1 + "\"," +
                                        "\"busy4\":\"" + 1 + "\"," +
                                        "\"busy5\":\"" + 1 + "\"," +
                                        "\"busy6\":\"" + 1 + "\"}"
                        );
                    }
                    GlobalVariable.autoswitch=autoswitch;
                    HttpRequest request = new HttpRequest();
                    String rs = request.postJson(
                            "http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/updateSysConfig",
                            "{\"userid\":\""+GlobalVariable.userid+"\"," +
                                    "\"pgmid\":\"" + GlobalVariable.pgmid+"\","+
                                    "\"busystate\": " + (GlobalVariable.busystate?'1':'0') +","+
                                    "\"callnum\": " + (GlobalVariable.callnum==null?0:GlobalVariable.callnum) +","+
                                    "\"waitnum\": " + (GlobalVariable.waitnum==null? 0:GlobalVariable.waitnum) +","+
                                    "\"online\": " + (GlobalVariable.online?'1':'0') +","+
                                    "\"callring\": " +(GlobalVariable.callring?'1':'0') +","+
                                    "\"communicatering\": " +(GlobalVariable.communicatering?'1':'0')+","+
                                    "\"allowblacklist\": " +(GlobalVariable.allowblacklist?'1':'0')+","+
                                    "\"whitelistmodel\": " +(GlobalVariable.whitelistmodel?'1':'0')+","+
                                    "\"busyall\": " +(GlobalVariable.busyall)+","+
                                    "\"viewmodel\": " +(viewmodel)+","+//特殊情况
                                    "\"sipmodel\": " +GlobalVariable.sipmodel+","+
                                    "\"autoswitch\": " +(GlobalVariable.autoswitch?'1':'0')+","+
                                    "}"
                    );
                    System.out.println(rs);

                    if(GlobalVariable.modelchange){
                        HttpRequest request_srvm = new HttpRequest();
                        String rs_srvm = request_srvm.postJson(
                         "http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/restartsipsubsrvm",
                        "{\"path1\":\"/usr/sipprj/bin/sipsrv.json\"," +
                                "\"path2\":\"\","+
                                "\"model\": \"" +GlobalVariable.sipmodel+"\""+
                                "}"
                        );
                    }

                    Message m2 = Message.obtain(SettingActivity.handler_,1002,null);
                    m2.sendToTarget();

                    if(GlobalVariable.send_to_other_config) {
                        String msg_to_send = "{ " +
                                "\"msgType\": \"CHANGE SYS CONFIG\"," +
                                "\"config\": \"" + GlobalVariable.send_to_other_content + "\"," +
                                "\"state\": \"1\"," +
                                "\"local\": \"" + MainActivity.localUri + "\"" +
                                "}";
                        if (MainActivity.wsControl != null)
                            MainActivity.wsControl.sendMessage(msg_to_send);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    public void updateViewByConfig()
    {
        //设置用户信息
        TextView channelNameTv = (TextView)findViewById(R.id.settingChannelName);
        TextView sipNameTv = (TextView)findViewById(R.id.settingSipName);
        TextView characterNameTv = (TextView)findViewById(R.id.settingCharacterName);
        TextView userNameTv = (TextView)findViewById(R.id.settingUserName);

        channelNameTv.setText(GlobalVariable.channelname);
        sipNameTv.setText(GlobalVariable.sipid);
        characterNameTv.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
        userNameTv.setText(GlobalVariable.username);

        //全局信息需要从数据库获得，返回信息待拓展
        TextView callinNumTv = (TextView)findViewById(R.id.settingCallinNum);
        TextView zhuboWaitNumTv = (TextView)findViewById(R.id.settingZhuboWaitNum);
        TextView daoboWaitNumTv = (TextView)findViewById(R.id.settingDaoboWaitNum);


        //设置背景颜色
        int colorblue = this.getResources().getColor(R.color.blue);
        int colorwhite = this.getResources().getColor(R.color.white);
        int colorgray = this.getResources().getColor(R.color.ct_gray);


        if(GlobalVariable.viewmodel == 1){
            //online蓝底白字
            button_t_online.setTextColor(online?colorwhite:colorblue);
            button_f_online.setTextColor(online?colorblue:colorwhite);
            button_t_online.setBackgroundResource(online?R.drawable.button_selector_thin:R.drawable.button_selected);
            button_f_online.setBackgroundResource(online?R.drawable.button_selected:R.drawable.button_selector_thin);
            //callring白底蓝字
            button_t_callring.setTextColor(callring?colorblue:colorwhite);
            button_f_callring.setTextColor(callring?colorwhite:colorblue);
            button_t_callring.setBackgroundResource(callring?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_f_callring.setBackgroundResource(callring?R.drawable.button_selector_thin:R.drawable.button_selected);
            //communicatering白底蓝字
            button_t_communicatering.setTextColor(communicatering?colorblue:colorwhite);
            button_f_communicatering.setTextColor(communicatering?colorwhite:colorblue);
            button_t_communicatering.setBackgroundResource(communicatering?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_f_communicatering.setBackgroundResource(communicatering?R.drawable.button_selector_thin:R.drawable.button_selected);

            //allowblacklist白底蓝字
            button_t_allowblacklist.setTextColor(allowblacklist?colorblue:colorwhite);
            button_f_allowblacklist.setTextColor(allowblacklist?colorwhite:colorblue);;
            button_t_allowblacklist.setBackgroundResource(allowblacklist?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_f_allowblacklist.setBackgroundResource(allowblacklist?R.drawable.button_selector_thin:R.drawable.button_selected);
            //whitelistmodel白底蓝字
            button_t_whitelistmodel.setTextColor(whitelistmodel?colorblue:colorwhite);
            button_f_whitelistmodel.setTextColor(whitelistmodel?colorwhite:colorblue);
            button_t_whitelistmodel.setBackgroundResource(whitelistmodel?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_f_whitelistmodel.setBackgroundResource(whitelistmodel?R.drawable.button_selector_thin:R.drawable.button_selected);
            //busyall白底蓝字
            button_2_busyall.setTextColor(busyall==2?colorblue:colorwhite);
            button_t_busyall.setTextColor(busyall==1?colorblue:colorwhite);
            button_f_busyall.setTextColor(busyall==0?colorblue:colorwhite);
            button_2_busyall.setBackgroundResource(busyall==2?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_t_busyall.setBackgroundResource(busyall==1?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_f_busyall.setBackgroundResource(busyall==0?R.drawable.button_selected:R.drawable.button_selector_thin);

            //busyall白底蓝字
            button_t_autoswitch.setTextColor(autoswitch?colorblue:colorwhite);
            button_f_autoswitch.setTextColor(autoswitch?colorwhite:colorblue);
            button_t_autoswitch.setBackgroundResource(autoswitch?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_f_autoswitch.setBackgroundResource(autoswitch?R.drawable.button_selector_thin:R.drawable.button_selected);

            button_sipmodel_1.setTextColor(sipmodel==0?colorblue:colorwhite);
            button_sipmodel_2.setTextColor(sipmodel==1?colorblue:colorwhite);
            button_sipmodel_3.setTextColor(sipmodel==2?colorblue:colorwhite);
            button_sipmodel_4.setTextColor(sipmodel==3?colorblue:colorwhite);
            button_sipmodel_1.setBackgroundResource(sipmodel==0?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_sipmodel_2.setBackgroundResource(sipmodel==1?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_sipmodel_3.setBackgroundResource(sipmodel==2?R.drawable.button_selected:R.drawable.button_selector_thin);
            button_sipmodel_4.setBackgroundResource(sipmodel==3?R.drawable.button_selected:R.drawable.button_selector_thin);

            button_1_viewmodel.setTextColor(viewmodel==1?colorblue:colorwhite);
            button_2_viewmodel.setTextColor(viewmodel==2?colorblue:colorwhite);
            button_3_viewmodel.setTextColor(viewmodel==3?colorblue:colorwhite);
            button_1_viewmodel.setBackgroundResource(viewmodel==1?R.drawable.button_selected:R.drawable.button_selector_ctgray);
            button_2_viewmodel.setBackgroundResource(viewmodel==2?R.drawable.button_selected:R.drawable.button_selector_ctgray);
            button_3_viewmodel.setBackgroundResource(viewmodel==3?R.drawable.button_selected:R.drawable.button_selector_ctgray);
        }
        else if(false){//else if(GlobalVariable.viewmodel == 2){
            //online蓝底白字
            button_t_online.setTextColor(online?colorgray:colorwhite);
            button_f_online.setTextColor(online?colorwhite:colorgray);
            button_t_online.setBackgroundResource(online?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);
            button_f_online.setBackgroundResource(online?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            //callring白底蓝字
            button_t_callring.setTextColor(callring?colorwhite:colorgray);
            button_f_callring.setTextColor(callring?colorgray:colorwhite);
            button_t_callring.setBackgroundResource(callring?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_f_callring.setBackgroundResource(callring?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);
            //communicatering白底蓝字
            button_t_communicatering.setTextColor(communicatering?colorwhite:colorgray);
            button_f_communicatering.setTextColor(communicatering?colorgray:colorwhite);
            button_t_communicatering.setBackgroundResource(communicatering?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_f_communicatering.setBackgroundResource(communicatering?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            //allowblacklist白底蓝字
            button_t_allowblacklist.setTextColor(allowblacklist?colorwhite:colorgray);
            button_f_allowblacklist.setTextColor(allowblacklist?colorgray:colorwhite);;
            button_t_allowblacklist.setBackgroundResource(allowblacklist?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_f_allowblacklist.setBackgroundResource(allowblacklist?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);
            //whitelistmodel白底蓝字
            button_t_whitelistmodel.setTextColor(whitelistmodel?colorwhite:colorgray);
            button_f_whitelistmodel.setTextColor(whitelistmodel?colorgray:colorwhite);
            button_t_whitelistmodel.setBackgroundResource(whitelistmodel?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_f_whitelistmodel.setBackgroundResource(whitelistmodel?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);
            //busyall白底蓝字
            button_2_busyall.setTextColor(busyall==2?colorwhite:colorgray);
            button_t_busyall.setTextColor(busyall==1?colorwhite:colorgray);
            button_f_busyall.setTextColor(busyall==0?colorwhite:colorgray);
            button_2_busyall.setBackgroundResource(busyall==2?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_t_busyall.setBackgroundResource(busyall==1?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_f_busyall.setBackgroundResource(busyall==0?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);

            //autoswitch白底蓝字
            button_t_autoswitch.setTextColor(autoswitch?colorwhite:colorgray);
            button_f_autoswitch.setTextColor(autoswitch?colorgray:colorwhite);
            button_t_autoswitch.setBackgroundResource(autoswitch?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_f_autoswitch.setBackgroundResource(autoswitch?R.drawable.button_selector_ctgray:R.drawable.button_selected_ctgray);

            button_sipmodel_1.setTextColor(sipmodel==0?colorwhite:colorgray);
            button_sipmodel_2.setTextColor(sipmodel==1?colorwhite:colorgray);
            button_sipmodel_3.setTextColor(sipmodel==2?colorwhite:colorgray);
            button_sipmodel_4.setTextColor(sipmodel==3?colorwhite:colorgray);
            button_sipmodel_1.setBackgroundResource(sipmodel==0?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_sipmodel_2.setBackgroundResource(sipmodel==1?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_sipmodel_3.setBackgroundResource(sipmodel==2?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_sipmodel_4.setBackgroundResource(sipmodel==3?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);

            button_1_viewmodel.setTextColor(viewmodel==1?colorwhite:colorgray);
            button_2_viewmodel.setTextColor(viewmodel==2?colorwhite:colorgray);
            button_3_viewmodel.setTextColor(viewmodel==3?colorwhite:colorgray);
            button_1_viewmodel.setBackgroundResource(viewmodel==1?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_2_viewmodel.setBackgroundResource(viewmodel==2?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);
            button_3_viewmodel.setBackgroundResource(viewmodel==3?R.drawable.button_selected_ctgray:R.drawable.button_selector_ctgray);

        }
        else if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3){
            button_t_online.setTextColor(online?colorwhite:colorgray);
            button_f_online.setTextColor(online?colorgray:colorwhite);
            button_t_online.setBackgroundResource(online?R.drawable.button_selector:R.drawable.button_selected);
            button_f_online.setBackgroundResource(online?R.drawable.button_selected:R.drawable.button_selector);
            //callring白底蓝字
            button_t_callring.setTextColor(callring?colorgray:colorwhite);
            button_f_callring.setTextColor(callring?colorwhite:colorgray);
            button_t_callring.setBackgroundResource(callring?R.drawable.button_selected:R.drawable.button_selector);
            button_f_callring.setBackgroundResource(callring?R.drawable.button_selector:R.drawable.button_selected);
            //communicatering白底蓝字
            button_t_communicatering.setTextColor(communicatering?colorgray:colorwhite);
            button_f_communicatering.setTextColor(communicatering?colorwhite:colorgray);
            button_t_communicatering.setBackgroundResource(communicatering?R.drawable.button_selected:R.drawable.button_selector);
            button_f_communicatering.setBackgroundResource(communicatering?R.drawable.button_selector:R.drawable.button_selected);

            //allowblacklist白底蓝字
            button_t_allowblacklist.setTextColor(allowblacklist?colorgray:colorwhite);
            button_f_allowblacklist.setTextColor(allowblacklist?colorwhite:colorgray);;
            button_t_allowblacklist.setBackgroundResource(allowblacklist?R.drawable.button_selected:R.drawable.button_selector);
            button_f_allowblacklist.setBackgroundResource(allowblacklist?R.drawable.button_selector:R.drawable.button_selected);
            //whitelistmodel白底蓝字
            button_t_whitelistmodel.setTextColor(whitelistmodel?colorgray:colorwhite);
            button_f_whitelistmodel.setTextColor(whitelistmodel?colorwhite:colorgray);
            button_t_whitelistmodel.setBackgroundResource(whitelistmodel?R.drawable.button_selected:R.drawable.button_selector);
            button_f_whitelistmodel.setBackgroundResource(whitelistmodel?R.drawable.button_selector:R.drawable.button_selected);
            //busyall白底蓝字
            button_2_busyall.setTextColor(busyall==2?colorgray:colorwhite);
            button_t_busyall.setTextColor(busyall==1?colorgray:colorwhite);
            button_f_busyall.setTextColor(busyall==0?colorgray:colorwhite);
            button_2_busyall.setBackgroundResource(busyall==2?R.drawable.button_selected:R.drawable.button_selector);
            button_t_busyall.setBackgroundResource(busyall==1?R.drawable.button_selected:R.drawable.button_selector);
            button_f_busyall.setBackgroundResource(busyall==0?R.drawable.button_selected:R.drawable.button_selector);

            //autoswitch白底蓝字
            button_t_autoswitch.setTextColor(autoswitch?colorgray:colorwhite);
            button_f_autoswitch.setTextColor(autoswitch?colorwhite:colorgray);
            button_t_autoswitch.setBackgroundResource(autoswitch?R.drawable.button_selected:R.drawable.button_selector);
            button_f_autoswitch.setBackgroundResource(autoswitch?R.drawable.button_selector:R.drawable.button_selected);

            button_sipmodel_1.setTextColor(sipmodel==0?colorgray:colorwhite);
            button_sipmodel_2.setTextColor(sipmodel==1?colorgray:colorwhite);
            button_sipmodel_3.setTextColor(sipmodel==2?colorgray:colorwhite);
            button_sipmodel_4.setTextColor(sipmodel==3?colorgray:colorwhite);
            button_sipmodel_1.setBackgroundResource(sipmodel==0?R.drawable.button_selected:R.drawable.button_selector);
            button_sipmodel_2.setBackgroundResource(sipmodel==1?R.drawable.button_selected:R.drawable.button_selector);
            button_sipmodel_3.setBackgroundResource(sipmodel==2?R.drawable.button_selected:R.drawable.button_selector);
            button_sipmodel_4.setBackgroundResource(sipmodel==3?R.drawable.button_selected:R.drawable.button_selector);

            button_1_viewmodel.setTextColor(viewmodel==1?colorgray:colorwhite);
            button_2_viewmodel.setTextColor(viewmodel==2?colorgray:colorwhite);
            button_3_viewmodel.setTextColor(viewmodel==3?colorgray:colorwhite);
            button_1_viewmodel.setBackgroundResource(viewmodel==1?R.drawable.button_selected:R.drawable.button_selector);
            button_2_viewmodel.setBackgroundResource(viewmodel==2?R.drawable.button_selected:R.drawable.button_selector);
            button_3_viewmodel.setBackgroundResource(viewmodel==3?R.drawable.button_selected:R.drawable.button_selector);
        }

    }
    public void autoswitchChange(View view){
        autoswitch = !autoswitch;
        updateViewByConfig();
    }
    public void onlineChange(View view){
        online = !online;
        updateViewByConfig();
    }
    public void callringChange(View view){
        callring = !callring;
        updateViewByConfig();
    }
    public void communicateringChange(View view){
        communicatering = !communicatering;
        updateViewByConfig();
    }
    public void busystateChange(View view){
        busystate = !busystate;
        updateViewByConfig();
    }

    public void viewModelChange1(View view){
        viewmodel = 1;
        updateViewByConfig();
    }

    public void viewModelChange2(View view){
        viewmodel = 2;
        updateViewByConfig();
    }

    public void viewModelChange3(View view){
        viewmodel = 3;
        updateViewByConfig();
    }

    public void sipmodelChangeS(View view) {
        sipmodel = 0;
        updateViewByConfig();
    }
    public void sipmodelChangeD(View view) {
        sipmodel = 1;
        updateViewByConfig();
    }
    public void sipmodelChangeM(View view) {
        sipmodel = 2;
        updateViewByConfig();
    }
    public void sipmodelChangeSD(View view) {
        sipmodel = 3;
        updateViewByConfig();
    }


    public void allowblacklistChange(View view){
        allowblacklist = !allowblacklist;
        updateViewByConfig();
    }
    public void whitelistmodelChange(View view){
        whitelistmodel = !whitelistmodel;
        updateViewByConfig();
    }
    public void busyallChange0(View view){
        busyall = 0;
        updateViewByConfig();
    }
    public void busyallChange1(View view){
        busyall = 1;
        updateViewByConfig();
    }
    public void busyallChange2(View view){
        busyall = 2;
        updateViewByConfig();
        LineBusyDialog lineBusyDialog = new LineBusyDialog.Builder(SettingActivity.this).create();
        lineBusyDialog.show();
        if(GlobalVariable.viewmodel == 1)
            lineBusyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
        else if(GlobalVariable.viewmodel == 2)
            lineBusyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
        else if(GlobalVariable.viewmodel == 3)
            lineBusyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
        lineBusyDialog.getWindow().setLayout(900, 500);
    }

    public void muteCall(View view){}

    public void settingHide(View view){

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);//可以跳过中间栈直接实现两个界面间的切换
        startActivity(intent);
    }

    public void clearAllCall(View view){
        //查找所有不为挂断状态的电话并拼接为挂断消息发送给服务器提示挂断
        ClearAllCallDialog myClearAllCallDialog = new ClearAllCallDialog.Builder(SettingActivity.this,"确认挂断?").create();
        myClearAllCallDialog.show();
        if(GlobalVariable.viewmodel == 1){
            myClearAllCallDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
        }
        else if(GlobalVariable.viewmodel == 2){
            myClearAllCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
        }
        else{
            myClearAllCallDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
        }
        myClearAllCallDialog.getWindow().setLayout(500, 300);
    }
    public void updateCallRecordST(String uid,Integer state,Integer type){
        new Thread() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void run() {
                HttpRequest request = new HttpRequest();
                String rs = request.postJson(
                        "http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordState",
                        "{\"type\":\"post pstn info\", " +
                                "\"callid\":\"" + uid + "\", " +
                                "\"state\":\"" + state + "\", " +
                                "\"type\":\"" + type + "\"}"
                );
                Message m2 = Message.obtain(MainActivity.handler_,1001,null);
                m2.sendToTarget();
            }
        }.start();

    }


}

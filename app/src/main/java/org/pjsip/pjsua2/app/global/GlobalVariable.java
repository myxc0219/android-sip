package org.pjsip.pjsua2.app.global;

import android.media.Ringtone;
import android.util.Log;

import org.pjsip.pjsua2.AudioMediaPlayer;
import org.pjsip.pjsua2.OnInstantMessageParam;
import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.model.Channel;
import org.pjsip.pjsua2.app.model.ChatMessage;
import org.pjsip.pjsua2.app.model.ProgramDirector;
import org.pjsip.pjsua2.app.model.TemplateType;
import org.pjsip.pjsua2.app.model.UserInfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalVariable {
    public static boolean isTest = true;
    public static boolean isDaLian = true;
    public static boolean isSuZhou = true;
    public static boolean isDL1018 = false;
    public static boolean isDL1129 = false;//处理大连的各界面图标隐藏问题
    public static boolean isDL0117 = false;//大连仅导播话机断电无法控制已切直播电话问题

    public static String txLevelValue = "2.0";
    public static String rxLevelValue = "2.0";

    public static boolean btnContrlModel_Open = true;//出厂版本先屏蔽此功能，完成后放开
    //切换实体按键映射话机接听/转接/直播/挂断 默认关闭，在点击back按钮后开启
    public static boolean btnContrlModel = false;
    //切换普通模式和多话机Lite模式
    public static boolean liteModel = false;

    public static String version_id = "ver.230627";

    public static boolean login_success = false;
    //判断用户是否修改了分路置忙状态
    public static boolean linebusy_change = false;

    //20220418判断当前是否已有系统状态弹框
    public static boolean sysconfig_notify = false;

    //处理保存配置成功之后才给其他话机发送消息的问题（提前发送导致未成功保存配置先刷新配置对不上）
    public static boolean send_to_other_config = false;
    public static String send_to_other_content = "";
    //耦合器模式处理多个同时点击置忙问题
    public static boolean current_busy_lock = false;
    //内通模式响铃
    public static boolean inner_ringcall = false;
    //2.0名字的问题
    public static boolean name_clicked = false;
    //耦合器模式选中的序号
    public static Integer ct_selected_index = -1;
    //导播系统2.0选中的序号
    public static Integer gs_selected_index = -1;
    //导播切入直播状态
    public static boolean daobo_living = false;
    //操作锁定(防止用户在同一时间点击多个按钮)
    public static boolean operatelock = false;
    //直播锁定
    public static boolean livelock = false;
    //处理内部消息请求ip
    public static String SEHTTPServer = "http://192.168.1.243:8080";

    public static Integer diallinemodel = 1;//1:自动分配线路 2:固定线路呼出

    //处理网络连接中断多次计数达到后退出登录
    public static int neterror_count;

    //处理摘机/挂机键
    public static long pressTime = 0;

    //处理点击信息按钮自动弹出线路选择窗口并呼出的功能
//    //点击挂断按钮时的锁定在下一秒解锁
//    public static boolean hangup_lock = false;
    //分路置忙当全否时自动将置忙设置切换回否
    public static boolean linebusyall = false;
    //分路置忙标志位
    public static boolean linebusy1 = false;
    public static boolean linebusy2 = false;
    public static boolean linebusy3 = false;
    public static boolean linebusy4 = false;
    public static boolean linebusy5 = false;
    public static boolean linebusy6 = false;

    //当前登录用户在用户表的id
    public static String user_tableid = "0";


    //判断当前用户是否更改了模式
    public static boolean modelchange = false;

    //public static Map<String,List<Integer>> ctNumberMap = new HashMap();
    public static Map<String,Integer> ctNumberMap = new HashMap();
    //public static List<NumberMap> ctNumberMap = new ArrayList<>();
    public static Map<Integer,String> ctNumberMapRevert = new HashMap();
    //处理发送线路置忙的问题
    public static Map<String,Integer> ctNumberMapAll = new HashMap();
    public static Map<Integer,String> ctNumberMapRevertAll = new HashMap();
    //处理禁止呼出线路不显示问题
    public static Map<Integer,String> getCtNumberMapRevertForbid = new HashMap();

    public static Map<Integer,Integer> ctIdMap = new HashMap<>();

    public static Integer viewmodel = 1;//1:电话导播模式  2:耦合器模式

    public static MainActivity.MyCallWithState autoswitchwaitcall;

    //消息传递prm会存在接收不到的问题，修改为全局变量存储，弹框时取出的方式
    public static OnInstantMessageParam prm = null;
    public static String im_msg = "";
    public static String im_username = "";
    public static String im_template = "";
    public static String im_type = "";

    //临时存储内通输入消息
    public static boolean firstcontentselect = false;
    public static String imMessage = "";

    public static boolean innerstart;//判断是否是内通发起方
    public static String innerrolefrom = "";
    public static String innerroleto = "";
    public static String innercallfrom = "";
    public static String innercallto = "";
    public static String innerchannelfrom = "";
    public static String innerchannelto = "";
    public static String innernamefrom = "";
    public static String innernameto = "";

    public static String innerstate = "";



    public static boolean headset = false;
    public static boolean macrophone = true;
    public static int audiodevice = 1;//默认扬声器

    public static String currentaccept_uid = "";

    public static long timeout_milliseconds = 300000;

    public static List<String> remoteCallUidList = new ArrayList<>();
    //public static int remoteCallNum;

    public static String localip;

    public static Long userid;//pdrid或者mbcid
    public static Long mbcid;//导播使用的对应主播的id

    public static Long channelid;
    public static Long columnid;
    public static String SIPSERVER = "";
    public static String EUSERVER = "";
    public static String VOSEVER = "192.168.1.118";

    public static String authToken = "12033b1a49bb452caffae2d2a9e4ab29";

    //用户登录信息
    public static String pgmid = "";
    public static String channelname = "";//频道名称
    public static String sipid = "";//分机号
    public static String charactername = "";//角色名称
    public static String username = "";//用户名

    //导播列表
    public static List<ProgramDirector> pdrList = new ArrayList<>();

    //控制是否进行录音(暂未提出业务需求)
    //public  boolean record = true;

    public static Integer callnum = 0;
    public static Integer waitnum = 0;

    public static Integer sipmodel = 1;//默认的模式是单导播模式，模式代码1：单导播模式，0：无导播模式 2：多导播模式,3:仅导播模式

    public static String ringtone = "http://"+EUSERVER+":8080/record/ringcall.wav";
    public static Ringtone r;


    public static AudioMediaPlayer rintoneplayer;

    public static float mplayerSpeed = 1F;

    //用于快捷跳转到拨号界面初始化号码
    public static boolean setdailvalue = false;
    public static String dailvalue = "";
    //用于大连信息键自动外呼历史记录
    public static boolean autocallout = false;


    //即时消息IM列表全局维护
    public static List<ChatMessage> messages = new ArrayList<ChatMessage>();

    public static boolean IMRedTitle = false;
    //dail setting main history contact communicate
    public static boolean IMRedShowed = false;

    public static boolean PhoneRedTitle = false;
    //dail setting main history contact communicate
    public static boolean PhoneRedShowed1 = false;
    public static boolean PhoneRedShowed2 = false;
    public static boolean PhoneRedShowed3 = false;
    public static boolean PhoneRedShowed4 = false;
    public static boolean PhoneRedShowed5 = false;
    public static boolean PhoneRedShowed6 = false;


    //全局维护Ringtone(电话响铃)

    //在话机和web界面修改后都即时保存
    public static Integer DebugIndex = 0;

    //设置界面需要的相关状态
    //本机状态
    public static boolean online = true;
    public static boolean callring = true;
    public static boolean communicatering = true;
    public static boolean busystate = false;
    //全局状态
    public static boolean allowblacklist = false;
    public static boolean whitelistmodel = false;
    //public static boolean busyall = false;//20210630，添加分路置忙功能此项变成多选项
    public static Integer busyall = 0;

    //标准时间转换格式
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static SimpleDateFormat sdfw = new SimpleDateFormat("yyyyMMddHHmmss");
    public static SimpleDateFormat hm_df = new SimpleDateFormat("HH:mm");
    public static SimpleDateFormat ms_df = new SimpleDateFormat("mm:ss");
    public static SimpleDateFormat hms_df = new SimpleDateFormat("HH:mm:ss");

    //姓名查询KV
    public static Map<String,String> pstn_name_KV = new HashMap<>();

    //配置切换界面后补充的属性
    public static String sipUri;
    public static String sipUriParam;
    public static String zhuboUri;
    public static String user_sipid;
    public static String localUri;
    public static String localUriParam;

    //202010514补充导播自动转接的功能
    public static boolean autoswitch = false;

    //将网络配置集中后CommunicateActivity需要的属性

    public static String[] typeArray = new String[1];
    public static Map<String, String[]> contentarrayMap = new HashMap<>();
    public static List<TemplateType> typeList = new ArrayList<>();

    //处理拨打内通显示用户名的列表
    public static List<UserInfo> userinfoList = new ArrayList<>();


    //由sipserver提供的统计数据
    public static String sip_totalnum;
    public static String sip_daobonum;
    public static String sip_zhubonum;
    public static String sip_switchnum;

    public static String sip_todaobonum;
    public static String sip_tozhubonum;

    //dail使用的channel列表
    public static List<Channel> channelList = new ArrayList<>();

    public static String getRingDuring(String mUri){
        String duration=null;
        android.media.MediaMetadataRetriever mmr = new android.media.MediaMetadataRetriever();
        try { if (mUri != null) { HashMap<String, String> headers=null;
            if (headers == null) { headers = new HashMap<String, String>();
                headers.put("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.4.2; zh-CN; MW-KW-001 Build/JRO03C) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 UCBrowser/1.0.0.001 U4/0.8.0 Mobile Safari/533.1"); }
            mmr.setDataSource(mUri, headers); }
            duration = mmr.extractMetadata(android.media.MediaMetadataRetriever.METADATA_KEY_DURATION); }
        catch (Exception ex) { }
        finally { mmr.release(); }
        Log.e("ryan","duration "+duration);
        return duration;
    }


}

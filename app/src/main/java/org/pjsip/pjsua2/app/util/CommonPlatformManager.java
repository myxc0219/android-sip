package org.pjsip.pjsua2.app.util;

import android.content.Context;
import android.util.Log;

public class CommonPlatformManager {

    private static String TAG = "CommonPlatformManager";

    private Context mContext = null;
    private Object mPlatformManager= null;
    private boolean mFlash = false;

    private CommonPlatformManager(Context context) {
        Log.i(TAG, "MediaManager");
        mContext = context;
        mPlatformManager = context.getSystemService("platform");
//        mAudioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//        mMediaThread = new HandlerThread("MediaHandler");
//        mMediaThread.start();
//        mMediaHandler = new MediaHandler(mMediaThread.getLooper());
//        volumeValue = SystemPropertiesUtils.getInt("ro.config.ring_vol_steps", 7);
    }
    private static CommonPlatformManager sInstance;

    public static CommonPlatformManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new CommonPlatformManager(context);
        }
        return sInstance;
    }


    public void flash() {
        mFlash = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                int state = PlatformManagerUtils.LED_STATE_ON;
                while (mFlash) {
                    PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                            .LED_TYPE_POWER, state, 0, 0, 1);
//                    PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
//                            .LED_TYPE_MUTE, state, 0, 0, 0);
//                    PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
//                            .LED_TYPE_SPEAKER, state, 0, 0, 0);
//                    PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
//                            .LED_TYPE_HEADSET, state, 0, 0, 0);
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (state == PlatformManagerUtils.LED_STATE_ON) {
                        state = PlatformManagerUtils.LED_STATE_OFF;
                    } else {
                        state = PlatformManagerUtils.LED_STATE_ON;
                    }
                }
                Thread.currentThread().interrupt();
            }
        }).start();
    }

    public void stopflash(){
        mFlash = false;
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_POWER, PlatformManagerUtils.LED_STATE_OFF, 0, 0, 0);
    }

    public void flashMute() {
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_MUTE, PlatformManagerUtils.LED_STATE_ON, 0, 0, 1);
    }
    public void stopflashMute() {
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_MUTE, PlatformManagerUtils.LED_STATE_OFF, 0, 0, 0);
    }

    public void flashSpeaker() {
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_SPEAKER, PlatformManagerUtils.LED_STATE_ON, 0, 0, 1);
    }
    public void stopflashSpeaker() {
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_SPEAKER, PlatformManagerUtils.LED_STATE_OFF, 0, 0, 0);
    }

    public void flashHeadset() {
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_HEADSET, PlatformManagerUtils.LED_STATE_ON, 0, 0, 1);
    }
    public void stopflashHeadset() {
        PlatformManagerUtils.setLedState(mPlatformManager,PlatformManagerUtils.LED_DEVICE_DEFAULT,PlatformManagerUtils
                .LED_TYPE_HEADSET, PlatformManagerUtils.LED_STATE_OFF, 0, 0, 0);
    }

    public void flashLedPower(){
        mFlash = true;
    }

}

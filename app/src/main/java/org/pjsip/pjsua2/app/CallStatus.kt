package org.pjsip.pjsua2.app

enum class CallStatus {
    空闲, 挂断, 呼入, 接听, 待播, 播出, 完成, 呼出
}
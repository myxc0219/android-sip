package org.pjsip.pjsua2.app.util;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collection;

public class GsonIgnoreStrategy implements ExclusionStrategy {

    /*
     * (non-Javadoc)
     *
     * @see com.google.gson.ExclusionStrategy#shouldSkipClass(java.lang.Class)
     */
    @Override
    public boolean shouldSkipClass(Class<?> clazz) {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.google.gson.ExclusionStrategy#shouldSkipField(com.google.gson.
     * FieldAttributes)
     */
    @Override
    public boolean shouldSkipField(FieldAttributes fieldAttributes) {
        Collection<Annotation> annotations = fieldAttributes.getAnnotations();
        for (Annotation annotation : annotations) {
            if (annotation.annotationType() == GsonIgnore.class) {
                return true;
            }
        }
        return false;
    }
}

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@interface GsonIgnore {
}
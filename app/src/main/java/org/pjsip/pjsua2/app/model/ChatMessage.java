package org.pjsip.pjsua2.app.model;

public class ChatMessage {

    public static final int MESSAGE_FROM = 0;
    public static final int MESSAGE_TO = 1;

    private int direction;
    private String content;
    private String time;
    private String templatename;
    private String username;

    public ChatMessage(int direction, String content,String time,String templatename,String username) {
        super();
        this.direction = direction;
        this.content = content;
        this.time = time;
        this.templatename = templatename;
        this.username = username;

    }

    public String getTemplatename() {
        return templatename;
    }

    public void setTemplatename(String templatename) {
        this.templatename = templatename;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public CharSequence getContent() {
        return content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

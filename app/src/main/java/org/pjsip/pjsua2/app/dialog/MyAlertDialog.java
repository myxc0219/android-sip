package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.pjsip.pjsua2.app.ContactsActivity;
import org.pjsip.pjsua2.app.R;

public class MyAlertDialog extends Dialog {

    private MyAlertDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    public static class Builder {

        private View mLayout;
        private Context mContext;

        private MyAlertDialog mDialog;
        private LinearLayout mLineBkg;
        private TextView mTitle;
        private TextView mContent;
        private Button mConfirmButton;
        private Button mCancelButton;


        public Builder(Context context,String content) {
            mContext = context;
            mDialog = new MyAlertDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_alert, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mContent = mLayout.findViewById(R.id.msgContent);
            mContent.setText(content);
            mConfirmButton = mLayout.findViewById(R.id.msgConfirmButton);
            mCancelButton = mLayout.findViewById(R.id.msgCancelButton);

            if(false){//if(GlobalVariable.viewmodel == 2)
                mLineBkg = mLayout.findViewById(R.id.msgNotifyLineBkg);
                mLineBkg.setBackground(mContext.getResources().getDrawable(R.drawable.layout_underline_thin_ctgray));
                mTitle = mLayout.findViewById(R.id.msgNotifyTitle);
                mTitle.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mContent.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mConfirmButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mConfirmButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
                mCancelButton.setBackground(mContext.getResources().getDrawable(R.drawable.button_selector_ctgray_radius));
                mCancelButton.setTextColor(mContext.getResources().getColor(R.color.ct_gray));
            }
        }
        public MyAlertDialog create() {
            mConfirmButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Message m2 = Message.obtain(ContactsActivity.handler_, 1016, null);
                    m2.sendToTarget();
                    mDialog.dismiss();
                }
            });

            mCancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });

            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(true);   //用户不可以点击外部来关闭 Dialog

            return mDialog;
        }
    }



}

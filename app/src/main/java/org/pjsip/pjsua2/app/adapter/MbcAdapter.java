package org.pjsip.pjsua2.app.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;

import java.util.ArrayList;
import java.util.List;

abstract public class MbcAdapter extends BaseAdapter {
    //abstract protected View getHeaderView(String caption, int index, View convertView, ViewGroup parent);

    private List<Section> sections=new ArrayList<Section>();
    private static int TYPE_SECTION_HEADER=0;

    private LayoutInflater mInflater;
    private Context mContext;
    private RecyclerView.ViewHolder mViewHolder;
    //private ArrayList<MainActivity.MyCallWithState> callWithStateList;
    private ArrayList<MainActivity.MyCallWithState> callWithStateList = new ArrayList<>();

    private ListView mListView;

    public void setDataList(ArrayList<MainActivity.MyCallWithState> list) {
        if (list != null) {
//            callWithStateList = (ArrayList<MainActivity.MyCallWithState>) list.clone();
//            notifyDataSetChanged();
            callWithStateList.clear();
            callWithStateList.addAll(list);
            notifyDataSetChanged();
        }
    }

    public void clearDataList() {
        if (callWithStateList != null) {
            callWithStateList.clear();
        }
        notifyDataSetChanged();
    }

    public MbcAdapter(Context pContext, List<MainActivity.MyCallWithState> allCallinList, ListView listView) {
        super();
        mContext = pContext;
        //callWithStateList = allCallinList;
        mInflater = LayoutInflater.from(pContext);
        mListView = listView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        ((SimpleAdapter)(sections.get(0).adapter)).notifyDataSetChanged();
        ((SimpleAdapter)(sections.get(1).adapter)).notifyDataSetChanged();
    }

    public void addSection(String caption, Adapter adapter) {
        sections.add(new Section(caption, adapter));
    }

//    public Object getItem(int position) {
//        for (Section section : this.sections) {
//            if (position==0) {
//                return(section);
//            }
//
//            int size=section.adapter.getCount()+1;
//
//            if (position<size) {
//                return(section.adapter.getItem(position-1));
//            }
//
//            position-=size;
//        }
//
//        return(null);
//    }

//    public int getCount() {
//        int total=0;
//
////        for (Section section : this.sections) {
////            //total+=section.adapter.getCount()+1; // add one for header
////            total+=section.adapter.getCount();
////        }
//        total = sections.get(0).adapter.getCount();
//        return(total);
//    }

//    @Override
//    public int getCount() {
//        // TODO Auto-generated method stub
//        return sections.get(0).adapter.getCount();
//    }
//
//    @Override
//    public Object getItem(int position) {
//        // TODO Auto-generated method stub
//        return null;
//    }
    /**
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return callWithStateList.size();
    }

    /**
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return callWithStateList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return(position);
    }

    public int getViewTypeCount() {
        int total=0;	// one for the header, plus those from sections

        for (Section section : this.sections) {
            total+=section.adapter.getViewTypeCount();
        }
        return(total);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

//    public int getItemViewType(int position) {
//        //int typeOffset=TYPE_SECTION_HEADER+1;	// start counting from here
//        int typeOffset=TYPE_SECTION_HEADER;
//        for (Section section : this.sections) {
//            if (position==0) {
//                return(TYPE_SECTION_HEADER);
//            }
//
//            int size=section.adapter.getCount()+1;
//
//            if (position<size) {
//                return(typeOffset+section.adapter.getItemViewType(position-1));
//            }
//
//            position-=size;
//            typeOffset+=section.adapter.getViewTypeCount();
//        }
//
//        return(-1);
//    }

    public boolean areAllItemsSelectable() {
        return(false);
    }

    public boolean isEnabled(int position) {
        //return(getItemViewType(position)!=TYPE_SECTION_HEADER);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView,
                        ViewGroup parent) {
        return createViewFromResource(mInflater, position, convertView, parent);
    }
    private View createViewFromResource(LayoutInflater inflater, int position, View convertView,
                                        ViewGroup parent ) {
        View v;
        if (convertView == null) {
            v = inflater.inflate(R.layout.callin_list_item_mbc, parent, false);
        } else {
            v = convertView;
        }

        boolean currentselected = false;
        int selectposition = mListView.getCheckedItemPosition();
        //Log.e("selectposition",selectposition+"");

        if(selectposition==position)
            currentselected = true;

        int size=sections.get(0).adapter.getCount();
        if (position<size) {
            convertView = mInflater.inflate(R.layout.callin_list_item_mbc,parent, false);
//            LinearLayout layout = convertView.findViewById(R.id.mbcCallTitle);
//            ImageView imageCallType = convertView.findViewById(R.id.imageCallType);

            //设置状态图标
            if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.NEWINIT
                    || callWithStateList.get(position).type==MainActivity.CALL_TYPE.DIALINIT
            )
            {
                if(currentselected)
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_accept_selected));
                else
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_accept));
            }
            if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.DAOBOACPT)
            {
                if(callWithStateList.get(position).dailout){
                    if(currentselected)
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_dail_select));
                    else
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_dail));
                }
                else {
                    if (currentselected)
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_accept_selected));
                    else
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_accept));
                }
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.DAOBOHOLD)
            {
                if(currentselected)
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_hold_selected));
                else
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_hold));
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.WAITZHUBO)
            {
                if(currentselected)
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_wait_selected_mbc));
                else
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_wait_mbc));
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.ZHUBOACPT)
            {
                if(callWithStateList.get(position).dailout){
                    if(currentselected)
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_dail_select));
                    else
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_dail));
                }
                else{
                    if (currentselected)
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_accept_selected));
                    else
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_accept));
                }
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.ZHUBOHOLD)
            {
                if(currentselected)
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_hold_selected));
                else
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_hold));
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.LIVEACPT)
            {
                if(currentselected)
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_live_selected_mbc));
                else
                    v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_live_mbc));
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.LIVEHOLD)
            {
                if(callWithStateList.get(position).first_livehold && GlobalVariable.sipmodel == 0 && !callWithStateList.get(position).syncshow) {
                    if (currentselected)
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_wait_selected_mbc));
                    else
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_wait_mbc));
                }
                else {
                    if (currentselected)
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_livehold_selected_mbc));
                    else
                        v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_livehold_mbc));
                }
            }
            else if(callWithStateList.get(position).type==MainActivity.CALL_TYPE.ENDCALL)
            {
                v.setBackground(mContext.getResources().getDrawable(R.drawable.bkg_normal_hangup));
            }

            v = sections.get(1).adapter.getView(position , v, parent);
        }
        return v;
    }



    class Section {
        String caption;
        Adapter adapter;
        Section(String caption, Adapter adapter) {
            this.caption=caption;
            this.adapter=adapter;
        }
    }
}

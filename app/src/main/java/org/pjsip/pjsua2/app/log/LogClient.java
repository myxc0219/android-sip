package org.pjsip.pjsua2.app.log;

import android.util.Log;

import org.graylog2.syslog4j.Syslog;
import org.graylog2.syslog4j.SyslogConstants;
import org.graylog2.syslog4j.SyslogIF;
import org.pjsip.pjsua2.app.global.GlobalVariable;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class LogClient {

    //private static final String HOST = GlobalVariable.EUSERVER;
    private static final int PORT = 514;

    public static void generate(String loginfo) {
        new Thread(new Runnable() {
            @Override
            public void run() {
//                SyslogIF syslog = Syslog.getInstance(SyslogConstants.UDP);
//                //syslog.getConfig().setHost(HOST);
//                syslog.getConfig().setHost(GlobalVariable.EUSERVER);
//                syslog.getConfig().setPort(PORT);
//                syslog.log(Syslog.LEVEL_ERROR, loginfo);
                if(GlobalVariable.EUSERVER!=null&&GlobalVariable.EUSERVER.equals("")) {
                    try {
                        SyslogIF syslog = Syslog.getInstance(SyslogConstants.UDP);
                        syslog.getConfig().setHost(GlobalVariable.EUSERVER);
                        syslog.getConfig().setPort(PORT);
                        //消息内容为Test
                        syslog.log(SyslogConstants.LEVEL_CRITICAL, URLDecoder.decode("【安卓话机发送消息】" + loginfo, "utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        Log.e("send syslog failed", e.getMessage());
                    }
                }
            }
        }).start();
    }


}

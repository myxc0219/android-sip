package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.pjsip.pjsua2.app.DailActivity;
import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.util.EditTextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LineChoseDialog extends Dialog {



    private LineChoseDialog(Context context, int themeResId) {
        super(context, themeResId);
    }



    public static class Builder {

        private View mLayout;
        private Context mContext;


//        private ImageView mIcon;
//        private TextView mTitle;
//        private TextView mMessage;
//        private Button mButton;

        private Button mbtnLineConfirm;
        private Button mbtnLineCancel;

        private View.OnClickListener mButtonClickListener;

        private LineChoseDialog mDialog;

        private ArrayList<Map<String, String>> lineList = new ArrayList<>();
        private ListView lineListView;

        private SimpleAdapter lineAdapter;

        private int lineSelectedIndex = -1;




        public Builder(Context context) {
            mContext = context;
            mDialog = new LineChoseDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_line_chose, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mbtnLineConfirm = mLayout.findViewById(R.id.btnLineConfirm);
            mbtnLineCancel = mLayout.findViewById(R.id.btnLineCancel);
            lineListView = mLayout.findViewById(R.id.lineList);

        }

//        /**
//         * 设置按钮文字和监听
//         */
//        public Builder setButton(@NonNull String text, View.OnClickListener listener) {
//            mButton.setText(text);
//            mButtonClickListener = listener;
//            return this;
//        }

        public LineChoseDialog create() {

            //填充ListView的内容

            //判别有效线路并添加到列表
            //拨号之前预选线路，确定当前有几路电话，确定预配置电话总路数
            //确定是否有空闲的可外呼线路
            //确定是固定呼出线路模式还是自动分配线路模式
            //如无空闲线路提示当前无空闲线路并返回
            int line = -1;
            for(int i = 1;i<7;i++){
                //当前线是否是注册线路
                boolean line_exist = GlobalVariable.ctNumberMap.containsValue(i);
                //当前线是否被已有电话占用
                boolean line_occupy = false;
//                try {
//                    int id_index = GlobalVariable.ctIdMap.get(i);
//                    MainActivity.MyCallWithState callws = MainActivity.allCallinList.get(id_index);
//                    line_occupy = true;
//                }catch (Exception e){
//                    e.printStackTrace();
//                    line_occupy = false;
//                }
                //
                if(GlobalVariable.isDaLian){
                    //判断外线号码和当前号码相同判断线路被占用
                    for(MainActivity.MyCallWithState callws : MainActivity.allCallinList){
                        if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
                            line_occupy = true;
                        }
                    }
                }
                if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
                    line_occupy = true;
                }

                if(line_exist && !line_occupy){
                    line = i;
                    HashMap<String, String> item = new HashMap<String, String>();
                    item.put("linename", "线路"+line);
                    item.put("linenumber", GlobalVariable.ctNumberMapRevert.get(line));
                    lineList.add(item);
                }
            }

            String[] from = { "linename", "linenumber" };
            int[] to = { R.id.textName, R.id.textNumber };
            lineAdapter = new SimpleAdapter(mContext, lineList,
                    R.layout.line_list_item,
                    from, to);
            lineListView.setAdapter(lineAdapter);

            lineListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,
                                        final View view,
                                        int position, long id)
                {
                    try {
                        view.setSelected(true);
                        lineSelectedIndex = position;

                        Map<String,String> item = null;
                        try{
                            item = lineList.get(lineSelectedIndex);
                        }
                        catch (Exception e){}
                        if(item!=null) {
                            String contacturi = item.get("linenumber");
                            Message m2 = Message.obtain(DailActivity.handler_, 1016, contacturi);
                            m2.sendToTarget();
                        }
                        mDialog.dismiss();


                    }catch (Exception e){
                        LogClient.generate("【操作错误】"+e.getMessage());
                        e.printStackTrace();
                    }
                }

            });

            mbtnLineConfirm.setOnClickListener(view -> {
//                EditText editText = ((Activity)mContext).findViewById(R.id.editTextName);
//                editText.setText(mAppellationText.getText());
                Map<String,String> item = null;
                try{
                    item = lineList.get(lineSelectedIndex);
                }
                catch (Exception e){}
                if(item!=null) {
                    String contacturi = item.get("linenumber");
                    Message m2 = Message.obtain(DailActivity.handler_, 1016, contacturi);
                    m2.sendToTarget();
                }
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });

            mbtnLineCancel.setOnClickListener(view -> {
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });
            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }

    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), this.getContext());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}


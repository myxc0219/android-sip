/* $Id: MainActivity.java 5022 2015-03-25 03:41:21Z nanang $ */
/*
 * Copyright (C) 2013 Teluu Inc. (http://www.teluu.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.pjsip.pjsua2.app;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import com.alibaba.fastjson.JSON;
import com.yanzhenjie.andserver.AndServer;
import com.yanzhenjie.andserver.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.pjsip.pjsua2.AccountConfig;
import org.pjsip.pjsua2.AuthCredInfo;
import org.pjsip.pjsua2.AuthCredInfoVector;
import org.pjsip.pjsua2.BuddyConfig;
import org.pjsip.pjsua2.CallInfo;
import org.pjsip.pjsua2.CallOpParam;
import org.pjsip.pjsua2.OnInstantMessageParam;
import org.pjsip.pjsua2.StringVector;
import org.pjsip.pjsua2.app.adapter.MbcAdapter;
import org.pjsip.pjsua2.app.adapter.MyAdapter;
import org.pjsip.pjsua2.app.controller.ActivityController;
import org.pjsip.pjsua2.app.dialog.AppellationDialog;
import org.pjsip.pjsua2.app.dialog.CtDailPanelDialog;
import org.pjsip.pjsua2.app.dialog.DailPanelDialog;
import org.pjsip.pjsua2.app.dialog.DaoboToLiveDialog;
import org.pjsip.pjsua2.app.dialog.HistoryDetialDialog;
import org.pjsip.pjsua2.app.dialog.HistoryDialog;
import org.pjsip.pjsua2.app.dialog.ImNotifyDialog;
import org.pjsip.pjsua2.app.dialog.ModuleTestDialog;
import org.pjsip.pjsua2.app.dialog.MsgNotifyDialog;
import org.pjsip.pjsua2.app.dialog.PdrChoseDialog;
import org.pjsip.pjsua2.app.dialog.PstnDetailDialog;
import org.pjsip.pjsua2.app.dialog.SysRestartDialog;
import org.pjsip.pjsua2.app.global.GlobalNotify;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.CallRecord;
import org.pjsip.pjsua2.app.model.JWebSocketClient;
import org.pjsip.pjsua2.app.model.OutCallUri;
import org.pjsip.pjsua2.app.model.ProgramDirector;
import org.pjsip.pjsua2.app.model.Pstn;
import org.pjsip.pjsua2.app.observer.VolumeChangeObserver;
import org.pjsip.pjsua2.app.service.HttpService;
import org.pjsip.pjsua2.app.service.MusicService;
import org.pjsip.pjsua2.app.util.CommonPlatformManager;
import org.pjsip.pjsua2.app.util.DataConvertUtil;
import org.pjsip.pjsua2.app.util.EditTextUtils;
import org.pjsip.pjsua2.app.util.IpUtils;
import org.pjsip.pjsua2.pjsip_inv_state;
import org.pjsip.pjsua2.pjsip_status_code;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity
			  implements Handler.Callback, MyAppObserver ,VolumeChangeObserver.VolumeChangeListener {

	//注意只有当服务绑定成功之后才能进行正确操作
	//进行网络请求
	private void getMovie(){
		String baseUrl = "https://api.douban.com/v2/movie/";
		Retrofit retrofit = new Retrofit.Builder().baseUrl(baseUrl).addConverterFactory(GsonConverterFactory.create()).build();
		HttpService httpService = retrofit.create(HttpService.class);
		Call<Pstn> call = httpService.getTopMovie(0, 10);
		call.enqueue(
			new Callback<Pstn>() {
				@Override
				public void onResponse(Call<Pstn> call, Response<Pstn> response)
				{
					System.out.println(response.body().toString());
				}
				@Override
				public void onFailure(Call<Pstn> call, Throwable t)
				{
					System.out.println(t.getMessage());
				}
			}
		);
	}

	private JWebSocketClient client;
	//private JWebSocketClientService.JWebSocketClientBinder binder;
	public static JWebSocketClientService.JWebSocketClientBinder wsControl;
	private JWebSocketClientService jWebSClientService;
	private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
			Log.e("MainActivity","SOCKET服务与活动成功绑定");
			LogClient.generate("【SOCKET服务与活动成功绑定】");
			wsControl = (JWebSocketClientService.JWebSocketClientBinder) iBinder;
			jWebSClientService = wsControl.getService();
			client = jWebSClientService.client;
			//服务上线
			String msg_to_send	= null;
			if(GlobalVariable.charactername.equals("director")){
				msg_to_send = "{ \"msgType\": \"DEVICE ONLINE\", \"DaoBoCallUri\": \""
						+ localUri + "\","
						+"\"ZhuBoCallUri\": \""+"\""
						+" }";
			}else{
				msg_to_send = "{ \"msgType\": \"DEVICE ONLINE\", \"DaoBoCallUri\": \""
						+"\","
						+"\"ZhuBoCallUri\": \""+ localUri + "\""
						+" }";
			}
			if(wsControl!=null&&client.isOpen())
				wsControl.sendMessage(msg_to_send);
			else
				Log.e("MainActivity","尚未初始化成功");
				LogClient.generate("【尚未初始化成功】");
		}

		@Override
		public void onServiceDisconnected(ComponentName componentName) {
			Log.e("MainActivity","服务与活动成功断开");
			LogClient.generate("【服务与活动成功断开】");
		}
	};

	//耦合器模式主界面拨号弹窗
    CtDailPanelDialog mctDailPanelDialog;
	//强制锁定模式
	private static final String KIOSK_PACKAGE = "org.pjsip.pjsua2.app";
	private static final String[] APP_PACKAGES = {KIOSK_PACKAGE};
	//调用音乐服务播放铃音
	private MyConnection conn;
	private MusicService.MyBinder musicControl;
	private class MyConnection implements ServiceConnection {
		//服务启动完成后会进入到这个方法
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			//获得service中的MyBinder
			musicControl = (MusicService.MyBinder) service;
			Log.e("MainActivity","MUSIC服务与活动成功绑定");
			LogClient.generate("【MUSIC服务与活动成功绑定】");
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
		}
	}

	private VolumeChangeObserver mVolumeChangeObserver;

	@Override
	public void onVolumeChanged(int volume) {
//		//系统媒体音量改变时的回调
//		Log.e("VOLUME_CHANGE", "onVolumeChanged()--->volume = " + volume);
//		//将音量调整值记录到缓存中
//		SharedPreferences sharedPreferencesx = getSharedPreferences("volumeconfig", Context.MODE_PRIVATE);
//		SharedPreferences.Editor editor = sharedPreferencesx.edit();//获取编辑器
//		editor.putString("volume", volume+"");
//		editor.commit();//提交修改
	}

	public Context mainActivityContext = this;
	public View layoutMain = null;
	public View layoutIntercom = null;

//	public static String sipUri = "<sip:8101@192.168.1.234>";
//	public static String sipUriParam = "sip:8101@192.168.1.234";
//	public static String zhuboUri = "<sip:9528@192.168.1.234>";
//	public static String localUri = "<sip:9528@192.168.1.234>";
//	public static String localUriParam = "sip:9528@192.168.1.234";
//	public static String user_sipid = "9528";

	public static String sipUri;
	public static String sipUriParam;
	public static String zhuboUri;
	public static String localUri;
	public static String localUriParam;
	public static String user_sipid;

	public static String localIP = "127.0.0.1";
	public static Boolean configcomplete = false;

	//只会在一台话机上后台保持一通电话
	public static MyCall onlycall;
	//处理多通电话呼入一通接听其他已接听的自动保持
	public MyCallWithState localacceptcall;
	//深拷贝防止对象被释放
	public static MyCallWithState tempCall = new MyCallWithState();

	private Boolean permissionToRecordAccepted = false;
	private String[] permissions = new String[]{Manifest.permission.RECORD_AUDIO, Manifest.permission.INTERNET, Manifest.permission.MODIFY_AUDIO_SETTINGS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WAKE_LOCK, Manifest.permission.VIBRATE, Manifest.permission.USE_SIP, Manifest.permission.CAMERA};

	//****************************************************************

	public static int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    public static MyApp app = null;
    public static MyCall currentCall = null;
	public static MyCallWithState currentCallWithState = null;

    public static MyAccount account = null;
    public static AccountConfig accCfg = null;
    public static MyBroadcastReceiver receiver = null;
    public static IntentFilter intentFilter = null;

	//标准时间格式
	private SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    //维持所有的Call
	public static ArrayList<MyCallWithState> allCallinList = new ArrayList();

    private Integer currentCallNum = 0;

	public class CALL_STATE_TYPE {
		//导播端状态
		public final static int DAOBO_INIT = 101;
		public final static int DAOBO_ACCEPT = 102;//导播接听了电话
		//public final static int DAOBO_HANGUP = 103;
		//public final static int DAOBO_MUTE = 104;
		public final static int DAOBO_HOLD = 105;//导播将该通电话保持
		public final static int DAOBO_SWITCH = 106;//导播将该通电话转给了主播
		public final static int DAOBO_REVERT = 107;//导播非第一次接听电话(此时未接听)
		//主播端状态
		public final static int ZHUBO_INIT = 201;
		public final static int ZHUBO_ACCEPT = 202;
		//public final static int ZHUBO_HANGUP = 203;
		//public final static int ZHUBO_MUTE = 204;
		public final static int ZHUBO_HOLD = 205;//主播将该通电话保持
		public final static int ZHUBO_SWITCH = 206;//主播将该通电话转给了导播
		public final static int ZHUBO_REVERT = 207;//主播非第一次接听电话

		public final static int HANGUP = 303;
		//public final static int HOLD = 305;
		public final static int MUTE = 306;

		public final static int LIVE_INIT = 401;
	}
	public class CALL_TYPE{
		public final static int NEWINIT = 0;
		public final static int DAOBOACPT = 1;
		public final static int DAOBOHOLD = 2;
		public final static int WAITZHUBO = 3;
		public final static int ZHUBOACPT = 4;
		public final static int ZHUBOHOLD = 5;
		public final static int LIVEACPT = 6;
		public final static int LIVEHOLD = 7;
		public final static int WAITDAOBO = 8;
		public final static int ENDCALL = 9;

		//2021/04/06添加拨打电话出去的虚拟状态
		public final static int DIALINIT = -2;
	}
    public static class MyCallWithState{
		public Integer type;
    	public Integer state;
    	public Integer virtualstate;
		public MyCall mycall;
		public String outcalluri;
		public String daobocalluri;
		//20210531添加关联的线路号码
		public String callednumber = "0000";
		//线路号码显示仅显示4位
		public String show_callednumber = "0000";

		public String callintime;
		public String accepttime;
		public String livetime;
		public String hanguptime;

		public String pstn;
		public String from;
		public String name;
		public Integer age;
		public Integer male;
		public Integer iswhitelist = 0;
		public Integer isblacklist = 0;
		public Integer iscontact = 0;
		public Integer isexpert = 0;
		public String level = "1";
		public String province;
		public String city;
		public String address;

		//20210616(处理待解决事项标志位)
		public String remark = "0";


		public String pstnmark;
		public String pdrmark;
		public String mbcmark;
		public String content;

		public String uid;//唯一通话编号

		public String timeshow;
		public int id;
		public boolean record = true;
		public boolean virtual = false;

		public HashMap<String, String> showitem;

		//设置当前call的hold状态以及在等待ack消息时的界面处理
		public boolean hold = false;
		public boolean wait_ackhold = false;
		//设置接听后等待动作
		public boolean wait_ackaccept = false;
		//在接听电话后立即进行转接动作
		public boolean autoswitch = false;

		//是否是未接听直接挂断
		public boolean forcehangup = false;

		//是否是由导/主播向外拨出
		public boolean dailout = false;

		//仅导播模式下主播同步显示状态
		public boolean syncshow = false;

		public String contacturi = "";

		//处理耦合器模式选中行按钮单独区分
		public boolean selected_line_btn = false;

		//点击播出按钮后直播锁定
		public boolean live_lock = false;

		//判断是不是第一次进入直播保持状态（苏州台第一次直播保持时显示预备播出）
		public boolean first_livehold = true;

	}
	//历史记录
	List<CallRecord> hisDetailList;
	ArrayList<Map<String, String>> hisList;
	//呼入电话列表
	private ListView callinListView;
	private SimpleAdapter callinListMbcAdapter;

	private int callinListSelectedIdx = -1;
	ArrayList<Map<String, String>> callinList;

	//电话列表适配器
	MyAdapter myAdapter = null;
	MbcAdapter mbcAdapter = null;

	private ListView buddyListView;
    private SimpleAdapter buddyListAdapter;
    private int buddyListSelectedIdx = -1;
    ArrayList<Map<String, String>> buddyList = new ArrayList<>();
    private String lastRegStatus = "";

	public static Handler handler_;
    private final Handler handler = new Handler(this);
    public class MSG_TYPE
    {
		public final static int INCOMING_CALL = 1;
		public final static int CALL_STATE = 2;
		public final static int REG_STATE = 3;
		public final static int BUDDY_STATE = 4;
		public final static int CALL_MEDIA_STATE = 5;
		public final static int CHANGE_NETWORK = 6;
		public final static int BACK_CALL_STATE = 7;
    }
	//1.姓名  2.年龄   3.等级  4.地址   5.分组   6.性别   7.黑名单  8.通讯录
    private Map<String,View> column_view_map = new HashMap<>();

    private class MyBroadcastReceiver extends BroadcastReceiver {
		private String conn_name = "";

		@Override
		public void onReceive(Context context, Intent intent) {
			if (isNetworkChange(context))
			notifyChangeNetwork();
		}

		private boolean isNetworkChange(Context context) {
			boolean network_changed = false;
			ConnectivityManager connectivity_mgr =
			((ConnectivityManager)context.getSystemService(
								 Context.CONNECTIVITY_SERVICE));

			NetworkInfo net_info = connectivity_mgr.getActiveNetworkInfo();
			if(net_info != null && net_info.isConnectedOrConnecting() &&
			   !conn_name.equalsIgnoreCase(""))
			{
			String new_con = net_info.getExtraInfo();
			if (new_con != null && !new_con.equalsIgnoreCase(conn_name))
				network_changed = true;

			conn_name = (new_con == null)?"":new_con;
			} else {
			if (conn_name.equalsIgnoreCase(""))
				conn_name = net_info.getExtraInfo();
			}
			return network_changed;
		}
    }

	/**界面切换按钮**/
    private void showCallActivity()
    {
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, CallActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//可以跳过中间栈直接实现两个界面间的切换
		startActivity(intent);
    }
	public void showIntercomActivity(View view)
	{
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, IntercomActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);
		//setContentView(layoutIntercom);
	}
	public void showDailActivity(View view)
	{
		if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn = findViewById(R.id.ctTitleDail);
			imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_dail_disable));
		}
		else if(GlobalVariable.viewmodel == 3){
			ImageButton imgBtn = findViewById(R.id.ctTitleDail);
			imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_dail_disable));
		}
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, DailActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);
	}
	public void showLoginActivity(View view)
	{
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, LoginActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//可以跳过中间栈直接实现两个界面间的切换
		startActivity(intent);
	}
	public void showHistoryActivity(View view)
	{
		if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
			imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_history_disable));
		}
		else if(GlobalVariable.viewmodel == 3){
			ImageButton imgBtn = findViewById(R.id.ctTitleHistory);
			imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_history_disable));
		}
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, HistoryActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);
	}
	public void showContactsActivity(View view)
	{
		if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
			imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_contacts_disable));
		}
		else if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn = findViewById(R.id.ctTitleContacts);
			imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_contacts_disable));
		}
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, ContactsActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);
	}
	public void showCommunicateActivity(View view)
	{
		//overridePendingTransition(0, 0);
		Intent intent = new Intent(this, CommunicateActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);
	}
	public void showSettingActivity(View view)
	{
		if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
			imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_setting_disable));
		}
		else if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn = findViewById(R.id.ctTitleSetting);
			imgBtn.setImageDrawable(getDrawable(R.drawable.gs_title_setting_disable));
		}
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, SettingActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);

	}
	public void showBroadcasterActivity(View view)
	{
		overridePendingTransition(0, 0);
		Intent intent = new Intent(this, BroadcasterActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		startActivity(intent);

	}


	/**首次加载页面时初始化控件**/
	private void initComponent(){
		//getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/**设置姓名为自定义输入框（弹出姓氏选择的AlertDialog）**/
		EditText editTextName = (EditText) findViewById(R.id.editTextName);
		EditText editTextInfo = (EditText) findViewById(R.id.textViewInfo);
		//		editTextName.setFocusableInTouchMode(true);
		//		editTextName.requestFocus();
		//		editTextName.requestFocusFromTouch();
		if(GlobalVariable.viewmodel == 1) {
			editTextName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				@Override
				public void onFocusChange(View arg0, boolean hasFocus) {
					if (hasFocus) {
						//					//被选中时直接弹出
						//					NameSelectDialog nameSelectDialog = new NameSelectDialog.Builder(MainActivity.this)
						//							.create();
						//					nameSelectDialog.show();
						AppellationDialog appellationDialog = new AppellationDialog.Builder(MainActivity.this).create();
						appellationDialog.show();
						appellationDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
						appellationDialog.getWindow().setLayout(1000, 600);
					} else {
					}
				}
			});
			editTextName.setOnClickListener(v -> {
				//已经被选中再次点击也可以弹出
//			NameSelectDialog nameSelectDialog = new NameSelectDialog.Builder(MainActivity.this)
//					.create();
//			nameSelectDialog.show();

				AppellationDialog appellationDialog = new AppellationDialog.Builder(MainActivity.this).create();
				appellationDialog.show();
				appellationDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
				appellationDialog.getWindow().setLayout(1000, 600);
			});
			editTextName.setInputType(InputType.TYPE_NULL);
		}
		else if(GlobalVariable.viewmodel == 3){
//			editTextName.setOnClickListener(v -> {
//				GlobalVariable.name_clicked = true;
//			});
			editTextName.addTextChangedListener(new TextWatcher() {
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					if(false){
						GlobalVariable.name_clicked = false;
						Log.e("参数1 -- s",s.toString());
						Log.e("参数2 -- start",""+start);
						Log.e("参数3 -- before",""+before);
						Log.e("参数4 -- count",""+count);
						if(count>0 && !s.toString().contains("先生") && !s.toString().contains("女士")){
							View view = getCurrentFocus();
							if(view!=null){
								IBinder token = view.getWindowToken();
								if (token != null) {
									InputMethodManager im = (InputMethodManager) mainActivityContext.getSystemService(Context.INPUT_METHOD_SERVICE);
									im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
								}
								View select_view = View.inflate(mainActivityContext, R.layout.dlg_character_select, null);

								TextView characterScMan = select_view.findViewById(R.id.characterScMan);
								TextView characterScWoMan = select_view.findViewById(R.id.characterScWoMan);
								PopupWindow pop = new PopupWindow(select_view , ViewGroup.LayoutParams.WRAP_CONTENT,
										ViewGroup.LayoutParams.WRAP_CONTENT);

								characterScMan.setOnClickListener(v->{
									editTextName.setText(editTextName.getText()+"先生");
									pop.dismiss();
									editTextInfo.requestFocus();
									editTextInfo.setSelection(editTextInfo.getText().length());
									Timer timer = new Timer();
									timer.schedule(new TimerTask(){
													   public void run()
													   {
														   InputMethodManager inputManager =
																   (InputMethodManager)editTextInfo.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
														   inputManager.showSoftInput(editTextInfo, 0);
													   }
												   },
											500);
								});
								characterScWoMan.setOnClickListener(v->{
									editTextName.setText(editTextName.getText()+"女士");
									pop.dismiss();
									editTextInfo.requestFocus();
									editTextInfo.setSelection(editTextInfo.getText().length());
		//							InputMethodManager manager = ((InputMethodManager)editTextInfo.getContext().getSystemService(Context.INPUT_METHOD_SERVICE));
		//							if (manager != null) manager.showSoftInput(editTextInfo, 0);
									Timer timer = new Timer();
									timer.schedule(new TimerTask(){
													   public void run()
													   {
														   InputMethodManager inputManager =
																   (InputMethodManager)editTextInfo.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
														   inputManager.showSoftInput(editTextInfo, 0);
													   }
												   },
											500);
								});
								//pop.setOutsideTouchable(true);
								pop.setFocusable(true);
								EditText et = findViewById(R.id.editTextName);
								pop.showAsDropDown(et);
							}

						}
					}


				}
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
				@Override
				public void afterTextChanged(Editable s) {}
			});

		}

		/**MyApp初始化（SIP底层服务）**/
		if (app == null) {
			app = new MyApp();
			//			// Wait for GDB to init, for native debugging only
			//		    if (false &&
			//			(getApplicationInfo().flags &
			//			ApplicationInfo.FLAG_DEBUGGABLE) != 0)
			//		    {
			//			try {
			//			    Thread.sleep(5000);
			//			} catch (InterruptedException e) {}
			//		    }

			app.init(this, getFilesDir().getAbsolutePath());

		}
		if (app.accList.size() == 0) {
			if(localUriParam!=null) {
				accCfg = new AccountConfig();
				accCfg.setIdUri(localUriParam);
				accCfg.getRegConfig().setRegistrarUri("sip:" + GlobalVariable.EUSERVER);
				AuthCredInfo cred = new AuthCredInfo("digest", "*", user_sipid, 0, user_sipid);
				accCfg.getSipConfig().getAuthCreds().add(cred);
				accCfg.getNatConfig().setIceEnabled(true);
				accCfg.getVideoConfig().setAutoTransmitOutgoing(true);
				accCfg.getVideoConfig().setAutoShowIncoming(true);
				account = app.addAcc(accCfg);
			}
		} else {
			account = app.accList.get(0);
			accCfg = account.cfg;
		}

		/**buddyList初始化（界面展示部分已弃用）**/
		//		buddyList = new ArrayList<Map<String, String>>();
		//		for (int i = 0; i < account.buddyList.size(); i++) {
		//			buddyList.add(DataConvertUtil.putData(account.buddyList.get(i).cfg.getUri(),
		//					account.buddyList.get(i).getStatusText()));
		//		}
		//		String[] from = { "uri", "status" };
		//		int[] to = { android.R.id.text1, android.R.id.text2 };
		//		buddyListAdapter = new SimpleAdapter(
		//				this, buddyList,
		//				android.R.layout.simple_list_item_2,
		//				from, to);

		//	buddyListView = (ListView) findViewById(R.id.listViewBuddy);
		//	buddyListView.setAdapter(buddyListAdapter);
		//	buddyListView.setOnItemClickListener(
		//	    new AdapterView.OnItemClickListener()
		//	    {
		//		@Override
		//		public void onItemClick(AdapterView<?> parent,
		//					final View view,
		//					int position, long id)
		//		{
		//		    view.setSelected(true);
		//		    buddyListSelectedIdx = position;
		//		}
		//	    }
		//	);
		//这边需要为显示单独创建一个类型：包括显示的各个字段

		/**Adapter初始化（与网络通信结果无关）**/
		String[] callin_from = { "name", "time","firsttimeshow","callednumber","id" };
		String[] callinmbc_from = { "name", "time","pstnmark","content","id" };//导播和主播添加的内容对应项不同
		String[] callin_from_s = { "name", "time" };
		int[] callin_to = { R.id.textCI1, R.id.textCI2, R.id.textCI3, R.id.textCI4, R.id.pdrCallId };
		int[] callinmbc_to = { R.id.textMbcCI1, R.id.textMbcCI2, R.id.textMbcCI3, R.id.textMbcCI4, R.id.mbcCallId };
		int[] callin_to_s = { R.id.clitCI1, R.id.clitCI2 };

		callinList = new ArrayList<Map<String, String>>();
		callinListView = (ListView) findViewById(R.id.listViewCall);
		callinListMbcAdapter = new SimpleAdapter(this, callinList,R.layout.callin_list_item_mbc,callinmbc_from, callinmbc_to);
		myAdapter=new MyAdapter(this,allCallinList,callinListView) {
			//		protected View getHeaderView(String caption, int index, View convertView, ViewGroup parent) {
			//			LinearLayout result=(LinearLayout)convertView;
			//			if (convertView==null) {
			//				result=(LinearLayout)getLayoutInflater().inflate(R.layout.callin_list_item, null);
			//			}
			//			return(result);
			//		}
		};
		myAdapter.addSection("Small",new SimpleAdapter(this, callinList,R.layout.callin_list_item_thin,callin_from_s, callin_to_s));
		myAdapter.addSection("Origin",new SimpleAdapter(this, callinList,R.layout.callin_list_item,callin_from, callin_to));
		mbcAdapter=new MbcAdapter(this,allCallinList,callinListView) {};
		mbcAdapter.addSection("Small",new SimpleAdapter(this, callinList,R.layout.callin_list_item_mbc,callinmbc_from, callinmbc_to));
		mbcAdapter.addSection("Origin",new SimpleAdapter(this, callinList,R.layout.callin_list_item_mbc,callinmbc_from, callinmbc_to));

		/**callinListView的点击事件初始化（与网络通信结果无关）**/
		callinListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
					@Override
					public void onItemClick(AdapterView<?> parent,
											final View view,
											int position, long id)
					{
						try {
							view.setSelected(true);
							callinListSelectedIdx = position;
							currentCallWithState = allCallinList.get(callinListSelectedIdx);
							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
							if(GlobalVariable.viewmodel==1){
								updateOperateButton();
								updateCurrentInfoView();
							}


						}catch (Exception e){
							LogClient.generate("【操作错误】"+e.getMessage());
							e.printStackTrace();
						}
					}

				});
		callinListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
					@RequiresApi(api = Build.VERSION_CODES.N)
					@Override
					public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
						view.setSelected(true);
						callinListSelectedIdx = position;
						currentCallWithState = allCallinList.get(callinListSelectedIdx);
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
							updateCurrentInfoView();
						}

						if(GlobalVariable.charactername.equals("director")){
							if(GlobalVariable.sipmodel == 2){
								PdrChoseDialog pdrChoseDialog = new PdrChoseDialog.Builder(MainActivity.this).create();
								pdrChoseDialog.show();
								pdrChoseDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
								pdrChoseDialog.getWindow().setLayout(500, 400);
							}
						}
						else if(GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){
							//当前是主播，主播呼出接听长按变保持，保持长按变接听
							MyCallWithState callWithState = allCallinList.get(callinListSelectedIdx);
							if(callWithState.dailout)
							{
								if(callWithState.type == CALL_TYPE.DAOBOACPT){
									//通知服务器变成主播保持状态，显示主播保持
									String msg_to_send =
											"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
													+ currentCallWithState.outcalluri
													+ "\", \"ZhuBoCallUri\": \""
													+ MainActivity.zhuboUri
													+ "\" }";

									if(wsControl!=null)wsControl.sendMessage(msg_to_send);
								}
								if(callWithState.type == CALL_TYPE.ZHUBOHOLD){

									updateCallRecordST(callWithState.uid,1,CALL_TYPE.DAOBOACPT);
									//主播取消保持
									GlobalVariable.currentaccept_uid = callWithState.uid;
									autoHoldCallAll();
									callWithState.wait_ackhold = true;
									if(GlobalVariable.viewmodel==1) {
										updateOperateButton();
									}

									//切换回主播接听但是显示导播接听
									String msg_to_send =
											"{ \"msgType\": \"ZHUBO UNHOLD\", \"OutCallUri\": \""
													+ currentCallWithState.outcalluri
													+ "\", \"ZhuBoCallUri\": \""
													+ MainActivity.zhuboUri
													+ "\" }";
									if(wsControl!=null)wsControl.sendMessage(msg_to_send);

								}
							}
						}

						if(false)//多导播模式下长按转接
						{
							try {
								new  AlertDialog.Builder(mainActivityContext)
										.setTitle("确认" )
										.setMessage("确定吗？" )
										.setPositiveButton("是" ,  new DialogInterface.OnClickListener() {
											@RequiresApi(api = Build.VERSION_CODES.N)
											@Override
											public void onClick(DialogInterface dialog, int which) {
												if(false){
													Log.e("which序号",""+which);
													//发送DAOBO TO DAOBO 消息
													JSONArray outcalluri_array = new JSONArray();
													//需要放置的对象类型
													MyCallWithState callWithState = allCallinList.get(callinListSelectedIdx);
													if(callWithState.type != CALL_TYPE.NEWINIT) {
														//先使用测试导播
														String msg_to_send = "{ \"msgType\": \"DAOBO TO DAOBO\", \"DaoBoCallUri\": \""
																+ "<sip:9529@192.168.1.244>"//这里是导播发消息,
																+ "\", \"ToOutCallUri\": [{"
																+ "\"outcalluri\": "+"\""+callWithState.outcalluri+"\","
																+ "\"uid\": "+"\""+callWithState.uid+"\","
																+ "\"type\": "+"\""+callWithState.type+"\","
																+ "\"state\": "+"\""+callWithState.state+"\""
																+ "}] }";
														if(wsControl!=null)wsControl.sendMessage(msg_to_send);
														Log.e("callWithState序号",""+callWithState.id);
														Log.e("callinListSize",""+callinList.size());
														//callinList.remove(callWithState.id);
														allCallinList.remove(callWithState.id);
														updateShowIndex();
														if(callinListSelectedIdx == callWithState.id){
															callinListSelectedIdx = -1;
														}
														currentCallNum--;
														resetMainView();
														updateInfoAndButton();
													}
												}


												//										OutCallUri outCallUri = new OutCallUri();
												//										outCallUri.setOutcalluri(callWithState.outcalluri);
												//										outCallUri.setUid(callWithState.uid);
												//										outCallUri.setType(callWithState.type);
												//										outCallUri.setState(callWithState.state);
												//										JSONObject outcalluri_json = (JSONObject) JSON.toJSON(outCallUri);
												//										outcalluri_array.put(outcalluri_json);


											}
										})
										.setNegativeButton("否" ,   new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {

											}
										})
										.show();
								//callinList.remove(position);
								//updateShowIndex();
								return true;
							}catch (Exception e){
								LogClient.generate("【操作错误】"+e.getMessage());
								e.printStackTrace();
							}
						}
						return false;
					}
				});
		/**长按接听键挂断（该逻辑已弃用）**/
		//		ImageButton acceptbutton = (ImageButton)findViewById(R.id.buttonCN1);
		//		acceptbutton.setOnLongClickListener(new View.OnLongClickListener() {
		//			@Override
		//			public boolean onLongClick(View v) {
		//				hangupCall(acceptbutton);
		//				return false;
		//			}
		//		});
		/**添加右侧相关输入框绑定数据监听器（与网络通信结果无关）**/
		EditText textName = (EditText)findViewById(R.id.editTextName);
		EditText textPstnMark = (EditText)findViewById(R.id.textViewDescribe);
		EditText textContent = (EditText)findViewById(R.id.textViewInfo);
		EditText textPdrMark = (EditText)findViewById(R.id.textViewPdrRemark);
		EditText textMbcMark = (EditText)findViewById(R.id.textViewMbcRemark);
		textName.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					}
					@Override
					public void onTextChanged(CharSequence text, int start, int before, int count) {
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(currentCallWithState!=null)
							currentCallWithState.name = textName.getText().toString();
					}
				});
		textPstnMark.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					}
					@Override
					public void onTextChanged(CharSequence text, int start, int before, int count) {
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(currentCallWithState!=null)
							currentCallWithState.pstnmark = textPstnMark.getText().toString();
					}
				});
		textContent.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					}
					@Override
					public void onTextChanged(CharSequence text, int start, int before, int count) {
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(currentCallWithState!=null)
							currentCallWithState.content = textContent.getText().toString();
					}
				});
		textPdrMark.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					}
					@Override
					public void onTextChanged(CharSequence text, int start, int before, int count) {
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(currentCallWithState!=null)
							currentCallWithState.pdrmark = textPdrMark.getText().toString();
					}
				});
		textMbcMark.addTextChangedListener(new TextWatcher() {
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					}
					@Override
					public void onTextChanged(CharSequence text, int start, int before, int count) {
					}
					@Override
					public void afterTextChanged(Editable s) {
						if(currentCallWithState!=null)
							currentCallWithState.mbcmark = textMbcMark.getText().toString();
					}
				});
		/**添加右侧相关输入框绑定数据监听器（与网络通信结果无关）**/
		new Thread(new Runnable() {
			@RequiresApi(api = Build.VERSION_CODES.N)
			@Override
			public void run() {
				while(true){
					try {
						Date nowdate = new Date();
						//处理左侧呼入电话的呼入/等待时间
						for(MyCallWithState callWithState : allCallinList){
							Map<String,String> map = callWithState.showitem;
							if(map!=null)
							{
								String newtime = null;
								SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
								Date dateTime = null;
								try {
									dateTime = simpleDateFormat.parse(map.get("firsttime"));
								} catch (ParseException e) {
									e.printStackTrace();
								}
								long diff = nowdate.getTime()-dateTime.getTime();
								diff = diff - TimeZone.getDefault().getRawOffset();
								SimpleDateFormat hmsDateFormat = new SimpleDateFormat("HH:mm:ss");
								String hms = hmsDateFormat.format(diff);
								map.replace("time",hms);

								//处理左侧当前选中项的通话时间
								//map还可以添加一个statetime项
								Date acceptTime = null;
								try {
									if(!map.get("accepttime").equals("00:00:00")) {
										acceptTime = simpleDateFormat.parse(map.get("accepttime"));
									}
									else{
										if(diff >= (-28800000+GlobalVariable.timeout_milliseconds) && !callWithState.syncshow && !callWithState.dailout)
										{
											String msg_to_send = "{ \"msgType\": \"TIMEOUT CALL\", \"OutCallUri\": \""
													+ callWithState.outcalluri+ "\" }";
//											Intent intent_ack = new Intent(MainActivity.this, JWebSocketClientService.class);
//											intent_ack.putExtra("msg_to_send", msg_to_send);
//											startService(intent_ack);
											if(wsControl!=null)wsControl.sendMessage(msg_to_send);
											map.put("accepttime","timeout");
										}

									}
								} catch (Exception e) {
									//e.printStackTrace();
								}
								if(acceptTime!=null){
									long statediff = nowdate.getTime()-acceptTime.getTime()-TimeZone.getDefault().getRawOffset();
									String statehms = hmsDateFormat.format(statediff);
									map.replace("statetime",statehms);
								}
							}

						}
						//处理右侧当前选中项的通话时间
						//					if(currentCallWithState!=null&&currentCallWithState.accepttime!=null)
						//					{
						//						Date dateTime = null;
						//						try {
						//							dateTime = standardDateFormat.parse(currentCallWithState.accepttime);
						//						} catch (ParseException e) {
						//							e.printStackTrace();
						//						}
						//						long diff = nowdate.getTime()-dateTime.getTime();
						//						diff = diff - TimeZone.getDefault().getRawOffset();
						//						SimpleDateFormat hmsDateFormat = new SimpleDateFormat("HH:mm:ss");
						//						String hms = hmsDateFormat.format(diff);
						//						currentCallWithState.timeshow = hms;
						//					}
						Message m2 = Message.obtain(MainActivity.handler_,1010,null);
						m2.sendToTarget();
						//一定是先进主页面的，所以其他页面的定时器也可以从主页面去刷新
						if(HistoryActivity.handler_!=null){
							Message m3 = Message.obtain(HistoryActivity.handler_,1010,null);
							m3.sendToTarget();
						}
						if(DailActivity.handler_!=null){
							Message m4 = Message.obtain(DailActivity.handler_,1010,null);
							m4.sendToTarget();
						}
						if(SettingActivity.handler_!=null){
							Message m5 = Message.obtain(SettingActivity.handler_,1010,null);
							m5.sendToTarget();
						}
						if(ContactsActivity.handler_!=null){
							Message m6 = Message.obtain(ContactsActivity.handler_,1010,null);
							m6.sendToTarget();
						}
						if(CommunicateActivity.handler_!=null){
							Message m7 = Message.obtain(CommunicateActivity.handler_,1010,null);
							m7.sendToTarget();
						}
						Thread.sleep(1000);
					}catch (Exception e){
						Log.i("aaa", "error:" + e.getMessage());
						LogClient.generate("【时间刷新器错误】"+e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}).start();
		/**进入页面之前刷新按钮**/
		if(GlobalVariable.viewmodel==1) {
			updateOperateButton();
		}
		else if(GlobalVariable.viewmodel==3) {
			updateGsOperateButton();
		}

		/**自动转接标记线程**/
		new Thread(new Runnable() {
			@Override
			public void run() {

				while(true){
					//给所有的标记

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		}).start();
	}

	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(requestCode==REQUEST_RECORD_AUDIO_PERMISSION){
			permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
		}
		if (!Settings.canDrawOverlays(this)) {
			//未授权限，请求权限
			Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
			intent.setData(Uri.parse("package:" + getPackageName()));
			startActivityForResult(intent,0);
		}

		if (!permissionToRecordAccepted) finish();

		//		//2021-04-01
		//		Intent audioIntent = new Intent();
		//		Log.d("SEND A BROAD CAST", "vdroid.intent.action.CONTACTS");
		//		audioIntent.setAction("vdroid.intent.action.CONTACTS");
		//		audioIntent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
		//		sendBroadcast(audioIntent);
	}
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
	}
	@Override
	public void onDestroy() {
    	super.onDestroy();
	}
	@RequiresApi(api = Build.VERSION_CODES.M)
	@Override
    protected void onCreate(Bundle savedInstanceState){
		GlobalVariable.login_success = true;
		super.onCreate(savedInstanceState);
		handler_ = handler;
		LayoutInflater inflater = this.getLayoutInflater();
		layoutMain = inflater.inflate(R.layout.activity_main, null);
		layoutIntercom = inflater.inflate(R.layout.activity_intercom, null);

		/**获取权限**/
		ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
		/**进入应用后停止闪光灯**/
		//		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
		//		flashManager.stopflash();
		/**获得设备本地IP**/
		localIP = IpUtils.getLocalIpAddress();
		/**注册接收websocket消息的监听器**/
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("cn.fitcan.action.HANDLE_WS_MESSAGE");
		wsReceiver = new WSReceiver();
		try{registerReceiver(wsReceiver, intentFilter);}catch (Exception e){}
		//20210506为了测试按钮功能先取消服务注册
		/**绑定音乐服务**/	//使用混合的方法开启服务
		Intent intent_music = new Intent(this, MusicService.class);
		conn = new MyConnection();
		startService(intent_music);
		bindService(intent_music, conn, BIND_AUTO_CREATE);

		/**绑定音量消息回调**/
		//实例化对象并设置监听器
		mVolumeChangeObserver = new VolumeChangeObserver(this);
		mVolumeChangeObserver.setVolumeChangeListener(this);
		int initVolume = mVolumeChangeObserver.getCurrentMusicVolume();
		Log.e(MainActivity.class.getSimpleName(), "initVolume = " + initVolume);

		/**绑定websocket服务**/
		Intent intent_websocket = new Intent(this, JWebSocketClientService.class);
		startService(intent_websocket);
		bindService(intent_websocket, serviceConnection, BIND_AUTO_CREATE);
		/**注册接收网络连接消息的监听器**/
		if (receiver == null) {
			receiver = new MyBroadcastReceiver();
			intentFilter = new IntentFilter(
					ConnectivityManager.CONNECTIVITY_ACTION);
			registerReceiver(receiver, intentFilter);
		}
		/**设置声音播放模式（待测试）**/
		//		//
		//		AudioManager audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
		////        audioManager.mode = AudioManager.MODE_IN_COMMUNICATION
		//		audioManager.setMode(AudioManager.MODE_NORMAL);
		//
		//		if(audioManager.isSpeakerphoneOn()) {
		//			Log.d("AUDIO", "isSpeakerphoneOn");
		//		}else if(audioManager.isWiredHeadsetOn()) {
		//			Log.d("AUDIO", "isWiredHeadsetOn");
		//		}else {
		//			AudioDeviceInfo[] x = audioManager.getDevices(AudioManager.GET_DEVICES_OUTPUTS);
		//
		//			Log.d("AUDIO", "other cases");
		////            Log.d("AUDIO", x[0].productName.toString() + x[1].productName.toString())
		//			Log.d("AUDIO", String.valueOf(x.length));
		//		}
		sipUri = GlobalVariable.sipUri;
		sipUriParam = GlobalVariable.sipUriParam;
		zhuboUri = GlobalVariable.zhuboUri;
		user_sipid = GlobalVariable.user_sipid;
		localUri = GlobalVariable.localUri;
		localUriParam = GlobalVariable.localUriParam;
		/**线程请求服务器配置信息(转移到LoginActivity)**/

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/**根据不同的角色来判定当前应该展示哪个页面**/

		if(GlobalVariable.viewmodel == 3){
			if (GlobalVariable.charactername != null && GlobalVariable.charactername.equals("director")) {
				setContentView(R.layout.activity_main_gs_pdr);
			}
			else{
				setContentView(R.layout.activity_main_gs_mbc);
			}
		}
		else if(GlobalVariable.viewmodel == 2){
			setContentView(R.layout.activity_main_ct);
		}
		else if(GlobalVariable.viewmodel == 1){
			if (GlobalVariable.charactername != null && GlobalVariable.charactername.equals("director")) {
				setContentView(R.layout.activity_main_new);
			} else {
				setContentView(R.layout.activity_main_mbc);
			}
		}
		//getWindow().setBackgroundDrawable(null);
		/**初始化各种网络无关控件**/
		initComponent();
		/**根据角色不同更改callinListView显示的Adapter（与网络通信结果有关）**/
		if(GlobalVariable.charactername.equals("director")){
			callinListView.setAdapter(myAdapter);
		}else {
			//callinListView.setAdapter(mbcAdapter);
			//20220316主播端和导播端同样显示
			callinListView.setAdapter(myAdapter);
		}


		if(GlobalVariable.viewmodel == 1){
			updateOperateButton();
		}
		//耦合器模式确定界面按钮
		else if(GlobalVariable.viewmodel == 2) {
			updateCtOperateButton();
			//确定控件自定义字体
			Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/Arial.ttf");

			TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
			TextView ct_title_username = findViewById(R.id.ct_title_username);
			TextView ct_title_systime = findViewById(R.id.ct_title_systime);

			TextView ct_state_id_1 = findViewById(R.id.ct_state_id_1);
			TextView ct_state_id_2 = findViewById(R.id.ct_state_id_2);
			TextView ct_state_id_3 = findViewById(R.id.ct_state_id_3);
			TextView ct_state_id_4 = findViewById(R.id.ct_state_id_4);
			TextView ct_state_id_5 = findViewById(R.id.ct_state_id_5);
			TextView ct_state_id_6 = findViewById(R.id.ct_state_id_6);

			TextView ctCallNum1 = (TextView)findViewById(R.id.ct_phone_num_1);
			TextView ctCallNum2 = (TextView)findViewById(R.id.ct_phone_num_2);
			TextView ctCallNum3 = (TextView)findViewById(R.id.ct_phone_num_3);
			TextView ctCallNum4 = (TextView)findViewById(R.id.ct_phone_num_4);
			TextView ctCallNum5 = (TextView)findViewById(R.id.ct_phone_num_5);
			TextView ctCallNum6 = (TextView)findViewById(R.id.ct_phone_num_6);

			TextView ctAcptTime1 = (TextView)findViewById(R.id.ct_acpt_time_1);
			TextView ctAcptTime2 = (TextView)findViewById(R.id.ct_acpt_time_2);
			TextView ctAcptTime3 = (TextView)findViewById(R.id.ct_acpt_time_3);
			TextView ctAcptTime4 = (TextView)findViewById(R.id.ct_acpt_time_4);
			TextView ctAcptTime5 = (TextView)findViewById(R.id.ct_acpt_time_5);
			TextView ctAcptTime6 = (TextView)findViewById(R.id.ct_acpt_time_6);

			TextView ctLiveTime1 = (TextView)findViewById(R.id.ct_live_time_1);
			TextView ctLiveTime2 = (TextView)findViewById(R.id.ct_live_time_2);
			TextView ctLiveTime3 = (TextView)findViewById(R.id.ct_live_time_3);
			TextView ctLiveTime4 = (TextView)findViewById(R.id.ct_live_time_4);
			TextView ctLiveTime5 = (TextView)findViewById(R.id.ct_live_time_5);
			TextView ctLiveTime6 = (TextView)findViewById(R.id.ct_live_time_6);

//			ct_title_channelname.setTypeface(typeface);
//			ct_title_username.setTypeface(typeface);
			//ct_title_systime.setTypeface(typeface);

			ct_state_id_1.setTypeface(typeface);
			ct_state_id_2.setTypeface(typeface);
			ct_state_id_3.setTypeface(typeface);
			ct_state_id_4.setTypeface(typeface);
			ct_state_id_5.setTypeface(typeface);
			ct_state_id_6.setTypeface(typeface);

			ctCallNum1.setTypeface(typeface);
			ctCallNum2.setTypeface(typeface);
			ctCallNum3.setTypeface(typeface);
			ctCallNum4.setTypeface(typeface);
			ctCallNum5.setTypeface(typeface);
			ctCallNum6.setTypeface(typeface);

			ctAcptTime1.setTypeface(typeface);
			ctAcptTime2.setTypeface(typeface);
			ctAcptTime3.setTypeface(typeface);
			ctAcptTime4.setTypeface(typeface);
			ctAcptTime5.setTypeface(typeface);
			ctAcptTime6.setTypeface(typeface);

			ctLiveTime1.setTypeface(typeface);
			ctLiveTime2.setTypeface(typeface);
			ctLiveTime3.setTypeface(typeface);
			ctLiveTime4.setTypeface(typeface);
			ctLiveTime5.setTypeface(typeface);
			ctLiveTime6.setTypeface(typeface);


			//耦合器模式首页线路长按置忙功能
			LinearLayout stateLayout1 = findViewById(R.id.ct_state_bkg_1);
			LinearLayout stateLayout2 = findViewById(R.id.ct_state_bkg_2);
			LinearLayout stateLayout3 = findViewById(R.id.ct_state_bkg_3);
			LinearLayout stateLayout4 = findViewById(R.id.ct_state_bkg_4);
			LinearLayout stateLayout5 = findViewById(R.id.ct_state_bkg_5);
			LinearLayout stateLayout6 = findViewById(R.id.ct_state_bkg_6);

			stateLayout1.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					lineBusyByClick(1);
					return true;
				}
			});
			stateLayout2.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					lineBusyByClick(2);
					return true;
				}
			});
			stateLayout3.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					lineBusyByClick(3);
					return true;
				}
			});
			stateLayout4.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					lineBusyByClick(4);
					return true;
				}
			});
			stateLayout5.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					lineBusyByClick(5);
					return true;
				}
			});
			stateLayout6.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					lineBusyByClick(6);
					return true;
				}
			});

		}
		else if(GlobalVariable.viewmodel == 3){
			updateGsOperateButton();

			LinearLayout stateLayout1 = findViewById(R.id.gs_state_bkg_1);
			LinearLayout stateLayout2 = findViewById(R.id.gs_state_bkg_2);
			LinearLayout stateLayout3 = findViewById(R.id.gs_state_bkg_3);
			LinearLayout stateLayout4 = findViewById(R.id.gs_state_bkg_4);
			LinearLayout stateLayout5 = findViewById(R.id.gs_state_bkg_5);
			LinearLayout stateLayout6 = findViewById(R.id.gs_state_bkg_6);

			stateLayout1.setOnClickListener(v -> {
				GlobalVariable.gs_selected_index = 1;
				int index = 1;
				Integer id_index = GlobalVariable.ctIdMap.get(index);
				if(id_index!=null&&id_index>=0&& id_index<allCallinList.size()){
					currentCallWithState = allCallinList.get(id_index);
				}
				else{
					currentCallWithState = null;
				}
				for(MyCallWithState callws : allCallinList){
					callws.selected_line_btn = false;
				}
				if(currentCallWithState!=null){
					currentCallWithState.selected_line_btn = true;
				}
				updateInfoAndButton();
			});
			stateLayout2.setOnClickListener(v -> {
				GlobalVariable.gs_selected_index = 2;
				int index = 2;
				Integer id_index = GlobalVariable.ctIdMap.get(index);
				if(id_index!=null&&id_index>=0&& id_index<allCallinList.size()){
					currentCallWithState = allCallinList.get(id_index);
				}
				else{
					currentCallWithState = null;
				}
				for(MyCallWithState callws : allCallinList){
					callws.selected_line_btn = false;
				}
				if(currentCallWithState!=null){
					currentCallWithState.selected_line_btn = true;
				}
				updateInfoAndButton();
			});
			stateLayout3.setOnClickListener(v -> {
				GlobalVariable.gs_selected_index = 3;
				int index = 3;
				Integer id_index = GlobalVariable.ctIdMap.get(index);
				if(id_index!=null&&id_index>=0&& id_index<allCallinList.size()){
					currentCallWithState = allCallinList.get(id_index);
				}
				else{
					currentCallWithState = null;
				}
				for(MyCallWithState callws : allCallinList){
					callws.selected_line_btn = false;
				}
				if(currentCallWithState!=null){
					currentCallWithState.selected_line_btn = true;
				}
				updateInfoAndButton();
			});
			stateLayout4.setOnClickListener(v -> {
				GlobalVariable.gs_selected_index = 4;
				int index = 4;
				Integer id_index = GlobalVariable.ctIdMap.get(index);
				if(id_index!=null&&id_index>=0&& id_index<allCallinList.size()){
					currentCallWithState = allCallinList.get(id_index);
				}
				else{
					currentCallWithState = null;
				}
				for(MyCallWithState callws : allCallinList){
					callws.selected_line_btn = false;
				}
				if(currentCallWithState!=null){
					currentCallWithState.selected_line_btn = true;
				}
				updateInfoAndButton();
			});
			stateLayout5.setOnClickListener(v -> {
				GlobalVariable.gs_selected_index = 5;
				int index = 5;
				Integer id_index = GlobalVariable.ctIdMap.get(index);
				if(id_index!=null&&id_index>=0&& id_index<allCallinList.size()){
					currentCallWithState = allCallinList.get(id_index);
				}
				else{
					currentCallWithState = null;
				}
				for(MyCallWithState callws : allCallinList){
					callws.selected_line_btn = false;
				}
				if(currentCallWithState!=null){
					currentCallWithState.selected_line_btn = true;
				}
				updateInfoAndButton();
			});
			stateLayout6.setOnClickListener(v -> {
				GlobalVariable.gs_selected_index = 6;
				int index = 6;
				Integer id_index = GlobalVariable.ctIdMap.get(index);
				if(id_index!=null&&id_index>=0&& id_index<allCallinList.size()){
					currentCallWithState = allCallinList.get(id_index);
				}
				else{
					currentCallWithState = null;
				}
				for(MyCallWithState callws : allCallinList){
					callws.selected_line_btn = false;
				}
				if(currentCallWithState!=null){
					currentCallWithState.selected_line_btn = true;
				}
				updateInfoAndButton();
			});


		}



		if(GlobalVariable.viewmodel == 1) {
			ImageButton imageButtonTitle = findViewById(R.id.buttonICON2);
			if (GlobalVariable.charactername.equals("broadcaster")) {
				//主播，仅导播模式屏蔽
				if (GlobalVariable.sipmodel == 3) {
					imageButtonTitle.setBackgroundResource(R.color.transparent);
					imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
					imageButtonTitle.setEnabled(false);
				}
				//主播，单/多导播模式屏蔽
				if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){
					imageButtonTitle.setBackgroundResource(R.color.transparent);
					imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
					imageButtonTitle.setEnabled(false);
				}
			} else {
				if (GlobalVariable.sipmodel == 0) {
					imageButtonTitle.setBackgroundResource(R.color.transparent);
					imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
					imageButtonTitle.setEnabled(false);
				}
			}
		}
		else if(GlobalVariable.viewmodel == 2) {
//			ImageButton imageButtonTitle = findViewById(R.id.ctTitleDail);
//			if(GlobalVariable.charactername.equals("broadcaster")) {
//				if (GlobalVariable.sipmodel == 3) {
//					imageButtonTitle.setBackgroundResource(R.color.transparent);
//					imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//					imageButtonTitle.setEnabled(false);
//				}
//				if (GlobalVariable.sipmodel == 1||GlobalVariable.sipmodel == 2) {
//					imageButtonTitle.setBackgroundResource(R.color.transparent);
//					imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//					imageButtonTitle.setEnabled(false);
//				}
//			}
//			else{
//				if (GlobalVariable.sipmodel == 0) {
//					imageButtonTitle.setBackgroundResource(R.color.transparent);
//					imageButtonTitle.setImageDrawable(getDrawable(R.color.transparent));
//					imageButtonTitle.setEnabled(false);
//				}
//			}
		}



		/**监听服务状态,在服务未启动时主图标显示红色**/
		//现在是每秒进一次线程，可以将这个过程再延长一点，然后在多次请求失败的情况下退回到登录界面
		new Thread() {
			@Override
			public void run() {
				while(true){
					//处理经常接听电话也能听到外线响铃的情况
					for(MyCallWithState callws : allCallinList){
						if(callws.type == CALL_TYPE.DAOBOACPT && callws.syncshow == false){
							LED_Media_Pause();
						}
					}
					String result = "";
					boolean net_error = false;
					try {
						HttpRequest request1 = new HttpRequest();
						String rs1 = request1.postJson("http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/chkprocandroid", "{\"path1\":\"sipsubsrv_m\",\"path2\":\"\"}");
						JSONObject rs_json1 = new JSONObject(rs1);
						if(String.valueOf(rs_json1.getJSONArray("data").get(0)).equals("0"))
						{
							result +="sip服务/kamalio服务/串口服务 未启动";
							net_error = true;
						}
//						HttpRequest request2 = new HttpRequest();
//						String rs2 = request2.postJson("http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/chkproc", "{\"path1\":\"kamailio\",\"path2\":\"\"}");
//						JSONObject rs_json2 = new JSONObject(rs2);
//						if(String.valueOf(rs_json2.getJSONArray("data").get(0)).equals("0"))
//						{
//							result +="kamalio服务未启动";
//							net_error = true;
//						}
//						HttpRequest request3 = new HttpRequest();
//						String rs3 = request3.postJson("http://"+ GlobalVariable.SIPSERVER +":8080/smart/shellExcute/chkproc", "{\"path1\":\"sipcomm\",\"path2\":\"\"}");
//						JSONObject rs_json3 = new JSONObject(rs3);
//						if(String.valueOf(rs_json3.getJSONArray("data").get(0)).equals("0"))
//						{
//							result +="串口服务未启动";
//							net_error = true;
//						}
					} catch (Exception e) {
						e.printStackTrace();
						result = "网络连接异常";
						net_error = true;
					}
					if(net_error)
					{
						if(GlobalVariable.neterror_count<5){
							GlobalVariable.neterror_count++;

							Message m2 = Message.obtain(MainActivity.handler_,1011,result);
							m2.sendToTarget();
							//一定是先进主页面的，所以其他页面的定时器也可以从主页面去刷新
							if(HistoryActivity.handler_!=null){
								Message m3 = Message.obtain(HistoryActivity.handler_,1011,result);
								m3.sendToTarget();
							}
							if(DailActivity.handler_!=null){
								Message m4 = Message.obtain(DailActivity.handler_,1011,result);
								m4.sendToTarget();
							}
							if(SettingActivity.handler_!=null){
								Message m5 = Message.obtain(SettingActivity.handler_,1011,result);
								m5.sendToTarget();
							}
							if(ContactsActivity.handler_!=null){
								Message m6 = Message.obtain(ContactsActivity.handler_,1011,result);
								m6.sendToTarget();
							}
							if(CommunicateActivity.handler_!=null){
								Message m7 = Message.obtain(CommunicateActivity.handler_,1011,result);
								m7.sendToTarget();
							}
						}
						else{
							if(!GlobalVariable.isTest){
								//处理退出软件逻辑
								GlobalVariable.neterror_count = 0;
								Message m = Message.obtain(MainActivity.handler_,1017,result);
								m.sendToTarget();
							}
						}
					}
					else{
						GlobalVariable.neterror_count = 0;

						Message m2 = Message.obtain(MainActivity.handler_,1011,"success");
						m2.sendToTarget();
						//一定是先进主页面的，所以其他页面的定时器也可以从主页面去刷新
						if(HistoryActivity.handler_!=null){
							Message m3 = Message.obtain(HistoryActivity.handler_,1011,"success");
							m3.sendToTarget();
						}
						if(DailActivity.handler_!=null){
							Message m4 = Message.obtain(DailActivity.handler_,1011,"success");
							m4.sendToTarget();
						}
						if(SettingActivity.handler_!=null){
							Message m5 = Message.obtain(SettingActivity.handler_,1011,"success");
							m5.sendToTarget();
						}
						if(ContactsActivity.handler_!=null){
							Message m6 = Message.obtain(ContactsActivity.handler_,1011,"success");
							m6.sendToTarget();
						}
						if(CommunicateActivity.handler_!=null){
							Message m7 = Message.obtain(CommunicateActivity.handler_,1011,"success");
							m7.sendToTarget();
						}
					}

					try {
						currentThread().sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

			}
		}.start();

		/**设置Http服务器用于远程控制话机**/
		Context context = this.getApplicationContext();
		Server server = AndServer.webServer(context)
				.port(8081)
				.timeout(10, TimeUnit.SECONDS)
				.build();
		server.startup();
    }

    private void lineBusyByClick(int i) {
		//先判断线路号码是否存在，线路号码存在且未被占用的情况下
		//且线路当前无正在通话，
		//判断当前全局的置忙状态，如果是全部置忙或者非置忙，切换到分路置忙
		//分为两种情况：未置忙切换到置忙，置忙取消
		if(GlobalVariable.current_busy_lock){
			return;
		}
		GlobalVariable.current_busy_lock = true;
		try {
			String line_number = GlobalVariable.ctNumberMapRevert.get(1);
			boolean line_exist = false;
			boolean line_occupy = false;

			line_exist = GlobalVariable.ctNumberMap.containsValue(i);
			//判断外线号码和当前号码相同判断线路被占用
			for(MyCallWithState callws : allCallinList){
				if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevert.get(i))){
					line_occupy = true;
				}
			}
			//这里不要考虑线路禁止呼出的问题
			//		if(GlobalVariable.getCtNumberMapRevertForbid.containsValue(GlobalVariable.ctNumberMapRevert.get(i))){
			//			line_occupy = true;
			//		}
			if(line_exist && !line_occupy){
				if(i == 1)
					GlobalVariable.linebusy1 = !GlobalVariable.linebusy1;
				else if(i == 2)
					GlobalVariable.linebusy2 = !GlobalVariable.linebusy2;
				else if(i == 3)
					GlobalVariable.linebusy3 = !GlobalVariable.linebusy3;
				else if(i == 4)
					GlobalVariable.linebusy4 = !GlobalVariable.linebusy4;
				else if(i == 5)
					GlobalVariable.linebusy5 = !GlobalVariable.linebusy5;
				else if(i == 6)
					GlobalVariable.linebusy6 = !GlobalVariable.linebusy6;

				new Thread(){
					@Override
					public void run() {
						try {
							HttpRequest request = new HttpRequest();
							String rs = request.postJson(
									"http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/updateLineMapping",
									"{\"busy1\":\"" + (GlobalVariable.linebusy1?1:0) + "\"," +
											"\"busy2\":\"" + (GlobalVariable.linebusy2?1:0) + "\"," +
											"\"busy3\":\"" + (GlobalVariable.linebusy3?1:0) + "\"," +
											"\"busy4\":\"" + (GlobalVariable.linebusy4?1:0) + "\"," +
											"\"busy5\":\"" + (GlobalVariable.linebusy5?1:0) + "\"," +
											"\"busy6\":\"" + (GlobalVariable.linebusy6?1:0) + "\"}"
							);
						}catch (Exception e){}
					}
				}.start();
				if(GlobalVariable.busyall == 2
						&&!GlobalVariable.linebusy1
						&&!GlobalVariable.linebusy2
						&&!GlobalVariable.linebusy3
						&&!GlobalVariable.linebusy4
						&&!GlobalVariable.linebusy5
						&&!GlobalVariable.linebusy6
				){
					GlobalVariable.busyall = 0;
					new Thread() {
						@Override
						public void run() {
							try {
								HttpRequest request = new HttpRequest();
								String rs = request.postJson(
										"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateSysConfig",
										"{\"userid\":\"" + GlobalVariable.userid + "\"," +
												"\"pgmid\":\"" + GlobalVariable.pgmid + "\"," +
												"\"busystate\": " + (GlobalVariable.busystate ? '1' : '0') + "," +
												"\"callnum\": " + (GlobalVariable.callnum == null ? 0 : GlobalVariable.callnum) + "," +
												"\"waitnum\": " + (GlobalVariable.waitnum == null ? 0 : GlobalVariable.waitnum) + "," +
												"\"online\": " + (GlobalVariable.online ? '1' : '0') + "," +
												"\"callring\": " + (GlobalVariable.callring ? '1' : '0') + "," +
												"\"communicatering\": " + (GlobalVariable.communicatering ? '1' : '0') + "," +
												"\"allowblacklist\": " + (GlobalVariable.allowblacklist ? '1' : '0') + "," +
												"\"whitelistmodel\": " + (GlobalVariable.whitelistmodel ? '1' : '0') + "," +
												"\"busyall\": " + (GlobalVariable.busyall) + "," +
												"\"viewmodel\": " + (GlobalVariable.viewmodel) + "," +//特殊情况
												"\"sipmodel\": " + GlobalVariable.sipmodel + "," +
												"\"autoswitch\": " + (GlobalVariable.autoswitch ? '1' : '0') + "," +
												"}");
							} catch (Exception e) {
							}
						}
					}.start();
				}
				else if(GlobalVariable.busyall == 1 || GlobalVariable.busyall == 0) {
					GlobalVariable.busyall = 2;
					new Thread() {
						@Override
						public void run() {
							try {
								HttpRequest request = new HttpRequest();
								String rs = request.postJson(
										"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateSysConfig",
										"{\"userid\":\"" + GlobalVariable.userid + "\"," +
												"\"pgmid\":\"" + GlobalVariable.pgmid + "\"," +
												"\"busystate\": " + (GlobalVariable.busystate ? '1' : '0') + "," +
												"\"callnum\": " + (GlobalVariable.callnum == null ? 0 : GlobalVariable.callnum) + "," +
												"\"waitnum\": " + (GlobalVariable.waitnum == null ? 0 : GlobalVariable.waitnum) + "," +
												"\"online\": " + (GlobalVariable.online ? '1' : '0') + "," +
												"\"callring\": " + (GlobalVariable.callring ? '1' : '0') + "," +
												"\"communicatering\": " + (GlobalVariable.communicatering ? '1' : '0') + "," +
												"\"allowblacklist\": " + (GlobalVariable.allowblacklist ? '1' : '0') + "," +
												"\"whitelistmodel\": " + (GlobalVariable.whitelistmodel ? '1' : '0') + "," +
												"\"busyall\": " + (GlobalVariable.busyall) + "," +
												"\"viewmodel\": " + (GlobalVariable.viewmodel) + "," +//特殊情况
												"\"sipmodel\": " + GlobalVariable.sipmodel + "," +
												"\"autoswitch\": " + (GlobalVariable.autoswitch ? '1' : '0') + "," +
												"}");
							} catch (Exception e) {
							}
						}
					}.start();
				}
			}

			Thread.sleep(300);
		}catch (Exception e){
			GlobalVariable.current_busy_lock = false;
		}
		GlobalVariable.current_busy_lock = false;
	}

	@Override
	protected void onStart() {
		if(GlobalVariable.isDL1129) {
			Button innercomBtn = findViewById(R.id.buttonMA6);
			innercomBtn.setVisibility(View.GONE);
		}

		//处理分路置忙占线发送消息的问题
//		String lineparam = "";
//		for(int i=1;i<7;i++){
//			//如果当路置忙的情况添加到
//			if(GlobalVariable.ctNumberMapRevertAll.get(i)!=null){
//				if(i==1&&GlobalVariable.linebusy1){
//					lineparam += ",{\"line\":\""+"01"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//				}
//				if(i==2&&GlobalVariable.linebusy2){
//					lineparam += ",{\"line\":\""+"02"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//				}
//				if(i==3&&GlobalVariable.linebusy3){
//					lineparam += ",{\"line\":\""+"03"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//				}
//				if(i==4&&GlobalVariable.linebusy4){
//					lineparam += ",{\"line\":\""+"04"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//				}
//				if(i==5&&GlobalVariable.linebusy5){
//					lineparam += ",{\"line\":\""+"05"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//				}
//				if(i==6&&GlobalVariable.linebusy6){
//					lineparam += ",{\"line\":\""+"06"+GlobalVariable.ctNumberMapRevertAll.get(i)+"\"}";
//				}
//			}
//		}
//		String msg_to_send = "{ \"msgType\": \"ADD BUSY LINE\", \"BusyLines\": ["+(lineparam.equals("")?"":lineparam.substring(1))+ "] }";
//		if(wsControl!=null)wsControl.sendMessage(msg_to_send);

		if(GlobalVariable.viewmodel == 1) {
			ImageView titleImage = findViewById(R.id.titleImage);
			if(titleImage!=null){
				titleImage.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
//					ForceExitDialog forceExitDialog = new ForceExitDialog.Builder(MainActivity.this).create();
//					forceExitDialog.show();
//					if(GlobalVariable.viewmodel == 1)
//						forceExitDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
//					else if(GlobalVariable.viewmodel == 2)
//						forceExitDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
//					forceExitDialog.getWindow().setLayout(500, 300);
						if(GlobalVariable.daobo_living){
							Message m2 = Message.obtain(MainActivity.handler_,1022,null);
							m2.sendToTarget();
						}
						else {
							DaoboToLiveDialog daoboToLiveDialog = new DaoboToLiveDialog.Builder(MainActivity.this).create();
							daoboToLiveDialog.show();
							daoboToLiveDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
							daoboToLiveDialog.getWindow().setLayout(500, 300);
						}
						return true;
					}
				});
			}

		}
		else if(GlobalVariable.viewmodel == 2){
			ImageView titleImage = findViewById(R.id.ctTitleImage);
			if(titleImage!=null){
				titleImage.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
//					ForceExitDialog forceExitDialog = new ForceExitDialog.Builder(MainActivity.this).create();
//					forceExitDialog.show();
//					if(GlobalVariable.viewmodel == 1)
//						forceExitDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
//					else if(GlobalVariable.viewmodel == 2)
//						forceExitDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
//					forceExitDialog.getWindow().setLayout(500, 300);
						if(GlobalVariable.daobo_living){
							Message m2 = Message.obtain(MainActivity.handler_,1022,null);
							m2.sendToTarget();
						}
						else {
							DaoboToLiveDialog daoboToLiveDialog = new DaoboToLiveDialog.Builder(MainActivity.this).create();
							daoboToLiveDialog.show();
							daoboToLiveDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
							daoboToLiveDialog.getWindow().setLayout(500, 300);
						}

						return true;
					}
				});
			}

		}
		else if(GlobalVariable.viewmodel == 3){
			ImageView titleImage = findViewById(R.id.ctTitleImage);
			if(titleImage!=null){
				titleImage.setOnLongClickListener(new View.OnLongClickListener() {
					@Override
					public boolean onLongClick(View v) {
						if(GlobalVariable.daobo_living){
							Message m2 = Message.obtain(MainActivity.handler_,1022,null);
							m2.sendToTarget();
						}
						else {
							DaoboToLiveDialog daoboToLiveDialog = new DaoboToLiveDialog.Builder(MainActivity.this).create();
							daoboToLiveDialog.show();
							daoboToLiveDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
							daoboToLiveDialog.getWindow().setLayout(500, 300);
						}

						return true;
					}
				});
			}

		}

//		new Thread(new Runnable() {
//			@RequiresApi(api = Build.VERSION_CODES.N)
//			@Override
//			public void run() {
//				while(true){
//					try {
//						Thread.sleep(1000);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					//Log.e("线程在响应",(new Date()).toString()+"");
//					ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
//					List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
//					if (!tasks.isEmpty()) {
//						ComponentName topActivity = tasks.get(0).topActivity;
//						if (!topActivity.getPackageName().equals(getPackageName())) {
//							Intent intent = new Intent(MainActivity.this, MainActivity.class);
//							intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
//							startActivity(intent);
//						}
//					}
//
//				}
//			}
//		}).start();


		if(GlobalVariable.viewmodel == 2){
			ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
			ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
			ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
			ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
			ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

			//imgBtn0.setImageDrawable(getDrawable(R.drawable.ct_title_home));
			imgBtn1.setImageDrawable(getDrawable(R.drawable.ct_title_history));
			imgBtn2.setImageDrawable(getDrawable(R.drawable.ct_title_contacts));
			imgBtn3.setImageDrawable(getDrawable(R.drawable.ct_title_setting));
			imgBtn4.setImageDrawable(getDrawable(R.drawable.ct_title_dail));
		}
		else if(GlobalVariable.viewmodel == 3){
			ImageButton imgBtn0 = findViewById(R.id.ctTitleHome);
			ImageButton imgBtn1 = findViewById(R.id.ctTitleHistory);
			ImageButton imgBtn2 = findViewById(R.id.ctTitleContacts);
			ImageButton imgBtn3 = findViewById(R.id.ctTitleSetting);
			ImageButton imgBtn4 = findViewById(R.id.ctTitleDail);

			//imgBtn0.setImageDrawable(getDrawable(R.drawable.gs_title_home));
			imgBtn1.setImageDrawable(getDrawable(R.drawable.gs_title_history));
			imgBtn2.setImageDrawable(getDrawable(R.drawable.gs_title_contacts));
			imgBtn3.setImageDrawable(getDrawable(R.drawable.gs_title_setting));
			imgBtn4.setImageDrawable(getDrawable(R.drawable.gs_title_dail));
		}
		super.onStart();

//		Intent intent = new Intent(MainActivity.this, MainActivity.class);
//		intent.addCategory(Intent.CATEGORY_LAUNCHER);
//		intent.setAction(Intent.ACTION_MAIN);
//		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
//		ActivityManager.RunningTaskInfo moveTaskToFront(rti.id, ActivityManager.MOVE_TASK_WITH_HOME);
	}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
		return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
//	switch (item.getItemId()) {
//	    case R.id.action_acc_config:
//		dlgAccountSetting();
//		break;
//
//	    case R.id.action_quit:
//		Message m = Message.obtain(handler, 0);
//		m.sendToTarget();
//		break;
//
//	    default:
//		break;
//	}

	return true;
    }
	/**点击软键盘外面的区域关闭软键盘**/
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// Finger touch screen event
		if (ev.getAction() == MotionEvent.ACTION_DOWN) {
			// get current focus,Generally it is EditText
			View view = getCurrentFocus();
			if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
				EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), MainActivity.this);

				if(GlobalVariable.viewmodel == 3 && GlobalVariable.charactername.equals("director")) {
					TextView editTextName = findViewById(R.id.editTextName);
					TextView textViewInfo = findViewById(R.id.textViewInfo);
					editTextName.clearFocus();
					textViewInfo.clearFocus();
				}


			}
		}
		return super.dispatchTouchEvent(ev);
	}
    @RequiresApi(api = Build.VERSION_CODES.P)
	@Override
    public boolean handleMessage(Message m){

		/**操作后刷新页面**/
    	if(m.what == 1001){
			//列表内容更新显示
			myAdapter.setDataList(allCallinList);
			mbcAdapter.setDataList(allCallinList);

			//保证currentCallNum和数组数量一致
			currentCallNum = allCallinList.size();
			if(currentCallNum == 0&& musicControl!=null){
				//LED_Media_Pause();
			}
			if(currentCallNum == 1){
				if(callinListSelectedIdx!=0)
				{
					callinListView.setItemChecked(0,true);
					currentCallWithState = allCallinList.get(0);
					callinListSelectedIdx = 0;
				}
				if(GlobalVariable.viewmodel == 3){
					//查找这路电话的线路号并置为选中状态
					if(currentCallWithState != null)
						currentCallWithState.selected_line_btn = true;
				}
			}
			updateInfoAndButton();

			MyCallWithState callWithState = (MyCallWithState) m.obj;
			if(GlobalVariable.autoswitch&&GlobalVariable.charactername.equals("director")){

				MyCallWithState tempCall = currentCallWithState;
				currentCallWithState = callWithState;
				if(currentCallWithState!=null) {
					if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
					{
						GlobalVariable.currentaccept_uid = "";
					}
					if (GlobalVariable.charactername.equals("director")) {
						setCallRecordInfo(currentCallWithState);
					} else {
						setCallRecordInfoMbc(currentCallWithState);
					}
					//单导播和多导播模式这么处理
					if(GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){
						if(currentCallWithState.type == CALL_TYPE.DAOBOACPT){
							updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITZHUBO);
							//导播通知SIP服务器，电话转给主播接听
							currentCallWithState.state = CALL_STATE_TYPE.DAOBO_SWITCH;
							broadcastCallType(currentCallWithState,CALL_TYPE.WAITZHUBO);
							String msg_to_send = "{ \"msgType\": \"DAOBO TO ZHUBO\", \"DaoBoCallUri\": \""
									+ localUri//这里是导播发消息
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri + "\" }";
							if(wsControl!=null)wsControl.sendMessage(msg_to_send);
						}
						else if(currentCallWithState.type == CALL_TYPE.NEWINIT){
//						if(GlobalVariable.sipmodel == 2){
//							//发送消息给SERVER之后，确定自己到底要不要继续
//							GlobalVariable.autoswitchwaitcall = currentCallWithState;
//							String msg_to_send =
//									"{ \"msgType\": \"AUTO SWITCH WAIT CALL\"," +
//											"\"DaoBoCallUri\": \""+localUri+"\","+
//											"\"OutCallUri\": \""+currentCallWithState.outcalluri+"\""+
//											"}";
//							if(wsControl!=null)wsControl.sendMessage(msg_to_send);
//						}
//						else {
							updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.WAITZHUBO);
							//先接电话再转电话
							acceptCall(null);
							currentCallWithState.autoswitch = true;
//						}
						}
						else if(currentCallWithState.type == CALL_TYPE.WAITDAOBO){
							updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITZHUBO);
							//先接电话再转电话
							acceptCall(null);
							currentCallWithState.autoswitch = true;
						}
						else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO){
							//20210510新需求使用接听按键处理主播待接听转回
							if(GlobalVariable.charactername.equals("director"))
							{

							}
							else{
								updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
								//先接电话再转电话
								acceptCall(null);
								currentCallWithState.autoswitch = true;
							}
						}
						else if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT){
							updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
							//主播通知SIP服务器，应答主播返回电话
							//currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_SWITCH;
							String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ currentCallWithState.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri + "\" }";
							if(wsControl!=null)wsControl.sendMessage(msg_to_send);
							//通知完毕后直接在这边将该电话挂断，并将界面中的电话移除
							try {

								broadcastCallType(currentCallWithState,CALL_TYPE.WAITDAOBO);
								allCallinList.remove(currentCallWithState.id);
								callinList.remove(currentCallWithState.id);
								updateShowIndex();
								if(callinListSelectedIdx == currentCallWithState.id)
								{
									callinListSelectedIdx = -1;
								}
								currentCallNum--;
								resetMainView();
								refreshCallList();
								myAdapter.setDataList(allCallinList);
								mbcAdapter.setDataList(allCallinList);
							} catch (Exception e) {
								System.out.println(e);
							}
						}
						else if(currentCallWithState.type == CALL_TYPE.LIVEACPT || currentCallWithState.type == CALL_TYPE.LIVEHOLD){
							updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
							currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_SWITCH;

							String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ currentCallWithState.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri + "\" }";
							if(wsControl!=null)wsControl.sendMessage(msg_to_send);

							//更新type为待导播接听
							broadcastCallType(currentCallWithState,CALL_TYPE.WAITDAOBO);
							try {
								allCallinList.remove(currentCallWithState.id);
								callinList.remove(currentCallWithState.id);
								updateShowIndex();
								if(callinListSelectedIdx == currentCallWithState.id)
								{
									callinListSelectedIdx = -1;
								}
								currentCallNum--;
								resetMainView();
								refreshCallList();
								if(GlobalVariable.viewmodel == 1) {
									updateOperateButton();
								}
								myAdapter.setDataList(allCallinList);
								mbcAdapter.setDataList(allCallinList);
							} catch (Exception e) {
								System.out.println(e);
							}
						}
					}
					else if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3){//无导播模式电话不会到导播所以也没有影响


					}

				}
				currentCallWithState = tempCall;
			}



		}
		/**根据配置修改界面**/
    	else if(m.what == 1002){
			updateViewByConfig((JSONObject) m.obj);
		}
		/**网络请求获得后更新界面**/
    	else if(m.what == 1003){

		}
		/**向buddylist添加buddy**/
    	else if(m.what == 1004){
			String uri = String.valueOf(m.obj);
			addBuddyByThread(uri);
		}
		/**主页面显示内通消息**/
    	else if(m.what == 1005){
			if(m.obj!=null && GlobalVariable.prm!=null){
				//2021-04-09提醒内容更改为自定义格式
				//2021-06-01修改为全局变量
				//OnInstantMessageParam prm = (OnInstantMessageParam) m.obj;
//				OnInstantMessageParam prm = GlobalVariable.prm;
//
//				String msgContent = "";
//				String username = "";
//				String template = "";
//				int type = -1;
//				try {
//					JSONObject msg_json = new JSONObject(prm.getMsgBody());
//					msgContent = msg_json.get("msg").toString();
//					username = msg_json.get("username").toString();
//					template = msg_json.get("template").toString();
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//
//				ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(MainActivity.this,msgContent,username,template).create();

					ImNotifyDialog imNotifyDialog = new ImNotifyDialog.Builder(
							MainActivity.this,
							GlobalVariable.im_msg,
							GlobalVariable.im_username,
							GlobalVariable.im_template).create();
					imNotifyDialog.show();
					imNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
					imNotifyDialog.getWindow().setLayout(500, 300);

			}

		}
		/**弹出当前选中电话的历史呼入记录**/
		else if(m.what == 1006){
			//初始化历史记录列表
			HistoryDetialDialog historyDetialDialog = new HistoryDetialDialog.Builder(MainActivity.this,hisList).create();
			historyDetialDialog.show();
			historyDetialDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
			historyDetialDialog.getWindow().setLayout(1000, 600);
		}
		/**刷新电话列表，按钮，重置主界面**/
		else if(m.what == 1007){
			String uid = String.valueOf(m.obj) ;
			try {
				int removeid  = -1;
				for(MyCallWithState callws : allCallinList)
				{
					if(callws.uid.equals(uid)){
						removeid = callws.id;
					}
				}
				if(removeid != -1){
					allCallinList.remove(removeid);
					callinList.remove(removeid);
				}

//				allCallinList.removeIf(myCallWithState -> myCallWithState.uid.equals(uid));
//				callinList.removeIf(map ->map.get("uid").equals(uid));
				currentCallNum = allCallinList.size();

				//自动移除后取消选中
				callinListView.setItemChecked(callinListView.getCheckedItemPosition(),false);
				callinListSelectedIdx = -1;
				updateShowIndex();
				resetMainView();
				refreshCallList();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
				updateInfoAndButton();
			}catch (Exception e){}
		}
		/**导播主动拨打外线电话加入到电话列表**/
		else if(m.what == 1008) {
			//20220315逻辑修改，即使是导播系统2.0和耦合器模式外呼电话也采用预显示刷新功能

//			if(GlobalVariable.viewmodel==2 || GlobalVariable.viewmodel==3){
//
//			}
//			else {
				//外呼之前有其他电话先自动保持
				autoHoldCallAll();
				List<String> numberlist = (ArrayList)m.obj;
				String OutCallUri = String.valueOf(numberlist.get(0));
				String callednumber = String.valueOf(numberlist.get(1));
				Log.e("进入1008","Yes");
				//当前是导播拨打电话（外线接听后对于导播来说也是自动接听）
				new Thread() {
					@Override
					public void run() {
						//MyCall call1 = new MyCall(account, -1);
						MyCallWithState callWithState = new MyCallWithState();
						callWithState.dailout = true;
						callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
						callWithState.type = CALL_TYPE.DIALINIT;
						callWithState.outcalluri = OutCallUri;
						callWithState.uid = "CO" + GlobalVariable.sdfw.format(new Date());
						//只有是导播呼出的时候才填导播位置
						if(GlobalVariable.charactername.equals("director"))
						{
							callWithState.daobocalluri = localUri;
						}
						SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
						callWithState.callintime = df.format(new Date());
						//20210618测试耦合器外呼用
						callWithState.callednumber = callednumber;

						try {
							currentCallNum = allCallinList.size();
							callWithState.id = currentCallNum;
							allCallinList.add(currentCallNum, callWithState);
							currentCallNum++;
						} catch (Exception e) {
							e.printStackTrace();
						}
						//查询电话信息
						String pstn = OutCallUri.replace("<sip:", "");
						String[] pstn_list = pstn.split("@");
						pstn = pstn_list[0];
						callWithState.pstn = pstn;
						//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
						//pstn = pstn.replaceFirst("00", "");
                        pstn = pstn.substring(2);
						getCallRecordInfo(callWithState, callWithState.pstn, callWithState.uid);

						String tailnumber = "";
						if (pstn.length() >= 4) {
							tailnumber = pstn.substring(pstn.length() - 4);
						} else {
							tailnumber = pstn;
						}

						if(callWithState.name == null || callWithState.name.equals("") || callWithState.name.equals("null")){
							callWithState.name = tailnumber;
						}

						callWithState.showitem = DataConvertUtil.putData(
								(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
								standardDateFormat.format(new Date()),
								callWithState.from,
								pstn,
								tailnumber,
								callWithState.pstnmark,
								callWithState.content,
								String.valueOf(callWithState.id + 1),
								callWithState.uid,
								callWithState.callednumber
						);
						callinList.add(callWithState.showitem);
						Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
						m2.sendToTarget();
					}
				}.start();
//			}

		}
		/**每秒收到定时器消息刷新界面内容**/
		else if(m.what == 1010) {

//			/**每次主动点击挂断后都会出现ListView内容数量改变需要先刷新界面才能进行下一步操作**/
//			String unlock_key = String.valueOf(m.obj);
//			if(unlock_key.equals("true")&&GlobalVariable.hangup_lock){
//				GlobalVariable.hangup_lock =false;
//				if(GlobalVariable.viewmodel == 1){
//					ImageButton btCN4 = findViewById(R.id.buttonCN4);
//					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
//					btCN4.setEnabled(true);
//				}
//			}
			/**电话列表内容更新显示**/
			myAdapter.setDataList(allCallinList);
			mbcAdapter.setDataList(allCallinList);

			/**无电话时停止响铃和指示灯闪烁**/
			if(currentCallNum == 0&& musicControl!=null){
				//LED_Media_Pause();
			}
			boolean havewaitcall = false;
			for(MyCallWithState callWithState : allCallinList){
				if(GlobalVariable.charactername.equals("director")){//导播
					if(callWithState.type == CALL_TYPE.WAITDAOBO || callWithState.type == CALL_TYPE.NEWINIT)
					{
						havewaitcall = true;
					}
				}
				else{//主播
					if(callWithState.type == CALL_TYPE.WAITZHUBO || callWithState.type == CALL_TYPE.NEWINIT)
					{
						havewaitcall = true;
					}
				}
			}

			if(!havewaitcall && musicControl!=null && musicControl.isPlaying()){
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
			}
			else if(havewaitcall && musicControl!=null && !musicControl.isPlaying()){
				LED_Media_Notify();
				GlobalVariable.PhoneRedTitle = true;
			}

			//当前不允许响铃
			if(!havewaitcall && !GlobalVariable.callring){
				GlobalVariable.PhoneRedTitle = false;
			}


			/**仅一通电话时自动选中**/
			if(currentCallNum == 1){
				if(callinListSelectedIdx!=0)
				{
					callinListView.setItemChecked(0,true);
					currentCallWithState = allCallinList.get(0);
					callinListSelectedIdx = 0;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
						updateCurrentInfoView();
					}
				}
			}
			/**显示系统时间**/
			TextView timeView = (TextView)findViewById(R.id.mainTime);
			if(timeView!=null) {
				Date date = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateString = sdf.format(date);
				timeView.setText(dateString.replace(" ","         "));
			}
			/**有未读消息内通图标闪烁**/
			Button ImButton = findViewById(R.id.buttonMA6);
			if(GlobalVariable.IMRedTitle){
				if(GlobalVariable.IMRedShowed){
					ImButton.setTextColor(Color.WHITE);
					GlobalVariable.IMRedShowed = false;
				}else{
					ImButton.setTextColor(getResources().getColor(R.color.notify_blue));
					GlobalVariable.IMRedShowed = true;
				}
			}else{
				ImButton.setTextColor(Color.WHITE);
			}

			/**有未接听电话首页图标闪烁**/
			Button PhoneButton = findViewById(R.id.buttonMA3);
			if(GlobalVariable.PhoneRedTitle){
				if(GlobalVariable.PhoneRedShowed3){
					PhoneButton.setTextColor(Color.WHITE);
					GlobalVariable.PhoneRedShowed3 = false;
				}else{
					PhoneButton.setTextColor(getResources().getColor(R.color.notify_blue));
					GlobalVariable.PhoneRedShowed3 = true;
				}
			}else{
				PhoneButton.setTextColor(Color.WHITE);
			}

			/**状态栏三个状态图标刷新**/
			ImageView mainState1 = (ImageView) findViewById(R.id.MainState1);
			ImageView mainState2 = (ImageView) findViewById(R.id.MainState2);
			ImageView mainState3 = (ImageView) findViewById(R.id.MainState3);
			if(GlobalVariable.callring){
				mainState1.setImageDrawable(getResources().getDrawable(R.drawable.allowring));
			}else{
				mainState1.setImageDrawable(getResources().getDrawable(R.drawable.disablering));
			}
			if(GlobalVariable.whitelistmodel) {
				mainState2.setImageDrawable(getResources().getDrawable(R.drawable.whitelistmodel));
			}else if(GlobalVariable.allowblacklist) {
				mainState2.setImageDrawable(getResources().getDrawable(R.drawable.allowblack));
			} else{
				mainState2.setImageDrawable(getResources().getDrawable(R.drawable.disableblack));
			}
			if(!GlobalVariable.online) {
				mainState3.setImageDrawable(getResources().getDrawable(R.drawable.pause));
				mainState3.setColorFilter(Color.RED);
			}
			else if(GlobalVariable.busyall==1) {
				mainState3.setImageDrawable(getResources().getDrawable(R.drawable.busy));
				mainState3.setColorFilter(Color.RED);
			}
            else if(GlobalVariable.busyall==2) {
                mainState3.setImageDrawable(getResources().getDrawable(R.drawable.linebusy));
				mainState3.setColorFilter(Color.TRANSPARENT);
            }
            else {
				mainState3.setImageDrawable(getResources().getDrawable(R.drawable.allowin));
				mainState3.setColorFilter(Color.WHITE);
			}

			TextView mainChannel = findViewById(R.id.mainChannel);
			TextView mainSipID = findViewById(R.id.mainSipID);
			mainChannel.setText(GlobalVariable.channelname);
			mainSipID.setText(GlobalVariable.user_sipid);

			if(GlobalVariable.viewmodel == 2){
				/**耦合器状态栏显示频道和sipid**/
				TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
				TextView ct_title_username = findViewById(R.id.ct_title_username);
				TextView ct_title_character = findViewById(R.id.ct_title_charactername);
				TextView ct_title_name = findViewById(R.id.ct_title_name);

				ct_title_channelname.setText(GlobalVariable.channelname);
				ct_title_username.setText(GlobalVariable.user_sipid);
				ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
				ct_title_name.setText(GlobalVariable.username);

				Typeface typeface_fitcan = Typeface.createFromAsset(this.getAssets(), "fonts/BGOTHM.TTF");
				ct_title_username.setTypeface(typeface_fitcan);
				ct_title_name.setTypeface(typeface_fitcan);

				/**耦合器状态栏显示系统时间**/
				TextView ctTimeView = (TextView)findViewById(R.id.ct_title_systime);
				Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
				ctTimeView.setTypeface(typeface);
				if(ctTimeView!=null) {
					Date date = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String dateString = sdf.format(date);
					ctTimeView.setText(dateString);
				}
				/**耦合器状态栏状态图标刷新**/
				/**状态栏三个状态图标刷新**/
				ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
				ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
				ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
				ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
				if(GlobalVariable.callring){
					ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowring));
				}else{
					ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disablering));
				}
				if(GlobalVariable.whitelistmodel) {
					ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_whitelistmodel));
				}else if(GlobalVariable.allowblacklist) {
					ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowblack));
				} else{
					ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_disableblack));
				}
				if(!GlobalVariable.online) {
					//ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
				} else if (GlobalVariable.busyall == 1) {
					ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_busy));
				} else if(GlobalVariable.busyall == 2) {
					ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_linebusy));
				} else {
					ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_allowin));
				}

				if(GlobalVariable.sipmodel == 0){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_mbc));
				}
				else if(GlobalVariable.sipmodel == 1){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_single));
				}
				else if(GlobalVariable.sipmodel == 2){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_multi));
				}
				else if(GlobalVariable.sipmodel == 3){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_state_pdr));
				}
				/**耦合器电话列表中线路号码显示刷新**/
				TextView ctLineNum1 = (TextView)findViewById(R.id.ct_line_number_1);
				TextView ctLineNum2 = (TextView)findViewById(R.id.ct_line_number_2);
				TextView ctLineNum3 = (TextView)findViewById(R.id.ct_line_number_3);
				TextView ctLineNum4 = (TextView)findViewById(R.id.ct_line_number_4);
				TextView ctLineNum5 = (TextView)findViewById(R.id.ct_line_number_5);
				TextView ctLineNum6 = (TextView)findViewById(R.id.ct_line_number_6);

				for(int i = 1; i<7; i++){
					String line_num = "";
					String tail_num = "";
					if(GlobalVariable.ctNumberMapRevert.get(i)==null){
						tail_num = "";
					}
					else{
						line_num = GlobalVariable.ctNumberMapRevert.get(i);
						if (line_num.length() >= 4) {
							tail_num = line_num.substring(line_num.length() - 4);
						} else {
							tail_num = line_num;
						}
					}
					if(i==1){
						ctLineNum1.setText(tail_num);
					}
					else if(i==2){
						ctLineNum2.setText(tail_num);
					}
					else if(i==3){
						ctLineNum3.setText(tail_num);
					}
					else if(i==4){
						ctLineNum4.setText(tail_num);
					}
					else if(i==5){
						ctLineNum5.setText(tail_num);
					}
					else if(i==6){
						ctLineNum6.setText(tail_num);
					}
				}

				/**耦合器电话列表中电话号码显示刷新**/
				TextView ctCallNum1 = (TextView)findViewById(R.id.ct_phone_num_1);
				TextView ctCallNum2 = (TextView)findViewById(R.id.ct_phone_num_2);
				TextView ctCallNum3 = (TextView)findViewById(R.id.ct_phone_num_3);
				TextView ctCallNum4 = (TextView)findViewById(R.id.ct_phone_num_4);
				TextView ctCallNum5 = (TextView)findViewById(R.id.ct_phone_num_5);
				TextView ctCallNum6 = (TextView)findViewById(R.id.ct_phone_num_6);
				ctCallNum1.setText("");
				ctCallNum2.setText("");
				ctCallNum3.setText("");
				ctCallNum4.setText("");
				ctCallNum5.setText("");
				ctCallNum6.setText("");

				/**耦合器电话列表中时间显示刷新**/
				TextView ctAcptTime1 = (TextView)findViewById(R.id.ct_acpt_time_1);
				TextView ctAcptTime2 = (TextView)findViewById(R.id.ct_acpt_time_2);
				TextView ctAcptTime3 = (TextView)findViewById(R.id.ct_acpt_time_3);
				TextView ctAcptTime4 = (TextView)findViewById(R.id.ct_acpt_time_4);
				TextView ctAcptTime5 = (TextView)findViewById(R.id.ct_acpt_time_5);
				TextView ctAcptTime6 = (TextView)findViewById(R.id.ct_acpt_time_6);

				TextView ctLiveTime1 = (TextView)findViewById(R.id.ct_live_time_1);
				TextView ctLiveTime2 = (TextView)findViewById(R.id.ct_live_time_2);
				TextView ctLiveTime3 = (TextView)findViewById(R.id.ct_live_time_3);
				TextView ctLiveTime4 = (TextView)findViewById(R.id.ct_live_time_4);
				TextView ctLiveTime5 = (TextView)findViewById(R.id.ct_live_time_5);
				TextView ctLiveTime6 = (TextView)findViewById(R.id.ct_live_time_6);

				ctAcptTime1.setText("");
				ctAcptTime2.setText("");
				ctAcptTime3.setText("");
				ctAcptTime4.setText("");
				ctAcptTime5.setText("");
				ctAcptTime6.setText("");

				ctLiveTime1.setText("");
				ctLiveTime2.setText("");
				ctLiveTime3.setText("");
				ctLiveTime4.setText("");
				ctLiveTime5.setText("");
				ctLiveTime6.setText("");

				Date now_date = new Date();
				try {
					now_date = GlobalVariable.hms_df.parse(GlobalVariable.hms_df.format(new Date()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				for(MyCallWithState callws : allCallinList){
					//注意顺序不是按照id排列,而是按照号码字典排列
					int id = GlobalVariable.ctNumberMap.get(callws.callednumber)==null?0:GlobalVariable.ctNumberMap.get(callws.callednumber);
					String showtime1 = "00:00";//呼入时间或接听时间
					String showtime2 = "00:00";//播出时间
					String showcallnum = callws.showitem==null?callws.pstn:callws.showitem.get("pstn");

					//注意三个时间节点，电话呼入的时间，电话接听的时间，电话播出的时间
					if(callws.type == CALL_TYPE.NEWINIT){
						try {
							Date date = GlobalVariable.hms_df.parse(callws.callintime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if(callws.type == CALL_TYPE.WAITZHUBO){
						try {
							Date date = GlobalVariable.hms_df.parse(callws.callintime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);

							Date date2 = GlobalVariable.hms_df.parse(callws.livetime);
							diff = now_date.getTime()-date2.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime2 = GlobalVariable.ms_df.format(diff);

						} catch (Exception e) {
							//e.printStackTrace();
						}
					}
					else if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
						try {
							Date date = GlobalVariable.hms_df.parse(callws.accepttime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);

							Date date2 = GlobalVariable.hms_df.parse(callws.livetime);
							diff = now_date.getTime()-date2.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime2 = GlobalVariable.ms_df.format(diff);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else{
						try {
							Date date = GlobalVariable.hms_df.parse(callws.accepttime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);
						} catch (Exception e) {
							//e.printStackTrace();
						}
					}

					if(id == 1){
						ctAcptTime1.setText(showtime1);
						ctLiveTime1.setText(showtime2);
						ctCallNum1.setText(showcallnum);
						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
							if(GlobalVariable.charactername.equals("director")){
								ctLiveTime1.setVisibility(View.GONE);
							}
							else{
								ctLiveTime1.setVisibility(View.VISIBLE);
							}
						}
						else{
							ctLiveTime1.setVisibility(View.GONE);
						}
					}
					else if(id == 2){
						ctAcptTime2.setText(showtime1);
						ctLiveTime2.setText(showtime2);
						ctCallNum2.setText(showcallnum);
						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
							if(GlobalVariable.charactername.equals("director")){
								ctLiveTime2.setVisibility(View.GONE);
							}
							else{
								ctLiveTime2.setVisibility(View.VISIBLE);
							}
						}
						else{
							ctLiveTime2.setVisibility(View.GONE);
						}
					}
					else if(id == 3){
						ctAcptTime3.setText(showtime1);
						ctLiveTime3.setText(showtime2);
						ctCallNum3.setText(showcallnum);
						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
							if(GlobalVariable.charactername.equals("director")){
								ctLiveTime3.setVisibility(View.GONE);
							}
							else{
								ctLiveTime3.setVisibility(View.VISIBLE);
							}
						}
						else{
							ctLiveTime3.setVisibility(View.GONE);
						}
					}
					else if(id == 4){
						ctAcptTime4.setText(showtime1);
						ctLiveTime4.setText(showtime2);
						ctCallNum4.setText(showcallnum);
						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
							if(GlobalVariable.charactername.equals("director")){
								ctLiveTime4.setVisibility(View.GONE);
							}
							else{
								ctLiveTime4.setVisibility(View.VISIBLE);
							}
						}
						else{
							ctLiveTime4.setVisibility(View.GONE);
						}
					}
					else if(id == 5){
						ctAcptTime5.setText(showtime1);
						ctLiveTime5.setText(showtime2);
						ctCallNum5.setText(showcallnum);
						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
							if(GlobalVariable.charactername.equals("director")){
								ctLiveTime5.setVisibility(View.GONE);
							}
							else{
								ctLiveTime5.setVisibility(View.VISIBLE);
							}
						}
						else{
							ctLiveTime5.setVisibility(View.GONE);
						}
					}
					else if(id == 6){
						ctAcptTime6.setText(showtime1);
						ctLiveTime6.setText(showtime2);
						ctCallNum6.setText(showcallnum);
						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
							if(GlobalVariable.charactername.equals("director")){
								ctLiveTime6.setVisibility(View.GONE);
							}
							else{
								ctLiveTime6.setVisibility(View.VISIBLE);
							}
						}
						else{
							ctLiveTime6.setVisibility(View.GONE);
						}
					}
				}
				//完成后整体刷新一次
				updateCtOperateButton();
				updateCtInfoView();
			}
			else if(GlobalVariable.viewmodel == 3){
				/**耦合器状态栏显示频道和sipid**/
				TextView ct_title_channelname = findViewById(R.id.ct_title_channelname);
				TextView ct_title_username = findViewById(R.id.ct_title_username);
				TextView ct_title_character = findViewById(R.id.ct_title_charactername);
				TextView ct_title_name = findViewById(R.id.ct_title_name);

				ct_title_channelname.setText(GlobalVariable.channelname);
				ct_title_username.setText(GlobalVariable.user_sipid);
				ct_title_character.setText(GlobalVariable.charactername.equals("director")?"导播":"主播");
				ct_title_name.setText(GlobalVariable.username);

				/**耦合器状态栏显示系统时间**/
				TextView ctTimeView = (TextView)findViewById(R.id.ct_title_systime);
				Typeface typeface = Typeface.createFromAsset(this.getAssets(), "fonts/lcdD.TTF");
				ctTimeView.setTypeface(typeface);
				if(ctTimeView!=null) {
					Date date = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String dateString = sdf.format(date);
					ctTimeView.setText(dateString);
				}
				/**耦合器状态栏状态图标刷新**/
				/**状态栏三个状态图标刷新**/
				ImageView ctMainState1 = (ImageView) findViewById(R.id.ct_MainState1);
				ImageView ctMainState2 = (ImageView) findViewById(R.id.ct_MainState2);
				ImageView ctMainState3 = (ImageView) findViewById(R.id.ct_MainState3);
				ImageView ctMainState4 = (ImageView) findViewById(R.id.ct_MainState4);
				if(GlobalVariable.callring){
					ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowring));
				}else{
					ctMainState1.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disablering));
				}
				if(GlobalVariable.whitelistmodel) {
					ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_whitelistmodel));
				}else if(GlobalVariable.allowblacklist) {
					ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowblack));
				} else{
					ctMainState2.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_disableblack));
				}
				if(!GlobalVariable.online) {
					//ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.ct_title_pause));
				} else if (GlobalVariable.busyall == 1) {
					ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_busy));
				} else if(GlobalVariable.busyall == 2) {
					ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_linebusy));
				} else {
					ctMainState3.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_allowin));
				}

				if(GlobalVariable.sipmodel == 0){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_mbc));
				}
				else if(GlobalVariable.sipmodel == 1){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_single));
				}
				else if(GlobalVariable.sipmodel == 2){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_multi));
				}
				else if(GlobalVariable.sipmodel == 3){
					ctMainState4.setImageDrawable(getResources().getDrawable(R.drawable.gs_title_state_pdr));
				}
				/**耦合器电话列表中线路号码显示刷新**/
				TextView ctLineNum1 = (TextView)findViewById(R.id.gs_line_number_1);
				TextView ctLineNum2 = (TextView)findViewById(R.id.gs_line_number_2);
				TextView ctLineNum3 = (TextView)findViewById(R.id.gs_line_number_3);
				TextView ctLineNum4 = (TextView)findViewById(R.id.gs_line_number_4);
				TextView ctLineNum5 = (TextView)findViewById(R.id.gs_line_number_5);
				TextView ctLineNum6 = (TextView)findViewById(R.id.gs_line_number_6);

				for(int i = 1; i<7; i++){
					String line_num = "";
					String tail_num = "";

					if(GlobalVariable.ctNumberMapRevert.get(i)==null){
						tail_num = "";
					}
					else{
						line_num = GlobalVariable.ctNumberMapRevert.get(i);
						if (line_num.length() >= 4) {
							//tail_num = line_num.substring(line_num.length() - 4);
							//20220310导播2.0不再显示尾号显示完整号码
							tail_num = line_num;
						} else {
							tail_num = line_num;
						}
					}
					if(i==1){
						ctLineNum1.setText(tail_num);
					}
					else if(i==2){
						ctLineNum2.setText(tail_num);
					}
					else if(i==3){
						ctLineNum3.setText(tail_num);
					}
					else if(i==4){
						ctLineNum4.setText(tail_num);
					}
					else if(i==5){
						ctLineNum5.setText(tail_num);
					}
					else if(i==6){
						ctLineNum6.setText(tail_num);
					}
				}

				/**耦合器电话列表中电话号码显示刷新**/
				TextView ctCallNum1 = (TextView)findViewById(R.id.gs_phone_num_1);
				TextView ctCallNum2 = (TextView)findViewById(R.id.gs_phone_num_2);
				TextView ctCallNum3 = (TextView)findViewById(R.id.gs_phone_num_3);
				TextView ctCallNum4 = (TextView)findViewById(R.id.gs_phone_num_4);
				TextView ctCallNum5 = (TextView)findViewById(R.id.gs_phone_num_5);
				TextView ctCallNum6 = (TextView)findViewById(R.id.gs_phone_num_6);
				ctCallNum1.setText("");
				ctCallNum2.setText("");
				ctCallNum3.setText("");
				ctCallNum4.setText("");
				ctCallNum5.setText("");
				ctCallNum6.setText("");

				/**耦合器电话列表中时间显示刷新**/
				TextView ctAcptTime1 = (TextView)findViewById(R.id.gs_acpt_time_1);
				TextView ctAcptTime2 = (TextView)findViewById(R.id.gs_acpt_time_2);
				TextView ctAcptTime3 = (TextView)findViewById(R.id.gs_acpt_time_3);
				TextView ctAcptTime4 = (TextView)findViewById(R.id.gs_acpt_time_4);
				TextView ctAcptTime5 = (TextView)findViewById(R.id.gs_acpt_time_5);
				TextView ctAcptTime6 = (TextView)findViewById(R.id.gs_acpt_time_6);

//				TextView ctLiveTime1 = (TextView)findViewById(R.id.ct_live_time_1);
//				TextView ctLiveTime2 = (TextView)findViewById(R.id.ct_live_time_2);
//				TextView ctLiveTime3 = (TextView)findViewById(R.id.ct_live_time_3);
//				TextView ctLiveTime4 = (TextView)findViewById(R.id.ct_live_time_4);
//				TextView ctLiveTime5 = (TextView)findViewById(R.id.ct_live_time_5);
//				TextView ctLiveTime6 = (TextView)findViewById(R.id.ct_live_time_6);

				ctAcptTime1.setText("");
				ctAcptTime2.setText("");
				ctAcptTime3.setText("");
				ctAcptTime4.setText("");
				ctAcptTime5.setText("");
				ctAcptTime6.setText("");

//				ctLiveTime1.setText("");
//				ctLiveTime2.setText("");
//				ctLiveTime3.setText("");
//				ctLiveTime4.setText("");
//				ctLiveTime5.setText("");
//				ctLiveTime6.setText("");

				Date now_date = new Date();
				try {
					now_date = GlobalVariable.hms_df.parse(GlobalVariable.hms_df.format(new Date()));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				for(MyCallWithState callws : allCallinList){
					//注意顺序不是按照id排列,而是按照号码字典排列
					int id = GlobalVariable.ctNumberMap.get(callws.callednumber)==null?0:GlobalVariable.ctNumberMap.get(callws.callednumber);
					String showtime1 = "00:00";//呼入时间或接听时间
					String showtime2 = "00:00";//播出时间
					String showcallnum = callws.showitem==null?callws.pstn:callws.showitem.get("pstn");

					//注意三个时间节点，电话呼入的时间，电话接听的时间，电话播出的时间
					if(callws.type == CALL_TYPE.NEWINIT){
						try {
							Date date = GlobalVariable.hms_df.parse(callws.callintime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					if(callws.type == CALL_TYPE.WAITZHUBO){
						try {
							Date date = GlobalVariable.hms_df.parse(callws.callintime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);

							Date date2 = GlobalVariable.hms_df.parse(callws.livetime);
							diff = now_date.getTime()-date2.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime2 = GlobalVariable.ms_df.format(diff);

						} catch (Exception e) {
							//e.printStackTrace();
						}
					}
					else if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
						try {
							Date date = GlobalVariable.hms_df.parse(callws.accepttime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);

							Date date2 = GlobalVariable.hms_df.parse(callws.livetime);
							diff = now_date.getTime()-date2.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime2 = GlobalVariable.ms_df.format(diff);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else{
						try {
							Date date = GlobalVariable.hms_df.parse(callws.accepttime);
							long diff = now_date.getTime()-date.getTime();
							diff = diff - TimeZone.getDefault().getRawOffset();
							showtime1 = GlobalVariable.ms_df.format(diff);
						} catch (Exception e) {
							//e.printStackTrace();
						}
					}

					if(id == 1){
						ctAcptTime1.setText(showtime1);
//						ctLiveTime1.setText(showtime2);
						ctCallNum1.setText(showcallnum);
//						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
//							if(GlobalVariable.charactername.equals("director")){
//								ctLiveTime1.setVisibility(View.GONE);
//							}
//							else{
//								ctLiveTime1.setVisibility(View.VISIBLE);
//							}
//						}
//						else{
//							ctLiveTime1.setVisibility(View.GONE);
//						}
					}
					else if(id == 2){
						ctAcptTime2.setText(showtime1);
//						ctLiveTime2.setText(showtime2);
						ctCallNum2.setText(showcallnum);
//						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
//							if(GlobalVariable.charactername.equals("director")){
//								ctLiveTime2.setVisibility(View.GONE);
//							}
//							else{
//								ctLiveTime2.setVisibility(View.VISIBLE);
//							}
//						}
//						else{
//							ctLiveTime2.setVisibility(View.GONE);
//						}
					}
					else if(id == 3){
						ctAcptTime3.setText(showtime1);
//						ctLiveTime3.setText(showtime2);
						ctCallNum3.setText(showcallnum);
//						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
//							if(GlobalVariable.charactername.equals("director")){
//								ctLiveTime3.setVisibility(View.VISIBLE);
//							}
//							else{
//								ctLiveTime3.setVisibility(View.GONE);
//							}
//						}
//						else{
//							ctLiveTime3.setVisibility(View.GONE);
//						}
					}
					else if(id == 4){
						ctAcptTime4.setText(showtime1);
//						ctLiveTime4.setText(showtime2);
						ctCallNum4.setText(showcallnum);
//						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
//							if(GlobalVariable.charactername.equals("director")){
//								ctLiveTime4.setVisibility(View.GONE);
//							}
//							else{
//								ctLiveTime4.setVisibility(View.VISIBLE);
//							}
//						}
//						else{
//							ctLiveTime4.setVisibility(View.GONE);
//						}
					}
					else if(id == 5){
						ctAcptTime5.setText(showtime1);
//						ctLiveTime5.setText(showtime2);
						ctCallNum5.setText(showcallnum);
//						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
//							if(GlobalVariable.charactername.equals("director")){
//								ctLiveTime5.setVisibility(View.GONE);
//							}
//							else{
//								ctLiveTime5.setVisibility(View.VISIBLE);
//							}
//						}
//						else{
//							ctLiveTime5.setVisibility(View.GONE);
//						}
					}
					else if(id == 6){
						ctAcptTime6.setText(showtime1);
//						ctLiveTime6.setText(showtime2);
						ctCallNum6.setText(showcallnum);
//						if(callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
//							if(GlobalVariable.charactername.equals("director")){
//								ctLiveTime6.setVisibility(View.GONE);
//							}
//							else{
//								ctLiveTime6.setVisibility(View.VISIBLE);
//							}
//						}
//						else{
//							ctLiveTime6.setVisibility(View.GONE);
//						}
					}
				}
				//完成后整体刷新一次
				updateGsOperateButton();
				updateGsInfoView();
			}

		}
		else if(m.what == 1011) {
			if(GlobalVariable.viewmodel == 1){
				ImageView titleImage = findViewById(R.id.titleImage);
				if(String.valueOf(m.obj).equals("success")){
					titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo));
					//titleImage.setColorFilter(Color.WHITE);

					//				Drawable newColor = titleImage.getDrawable();
					//				newColor.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
					//				titleImage.setImageDrawable(newColor);
				}
				else{
					titleImage.setImageDrawable(getDrawable(R.drawable.mian_logo_red));
					//titleImage.setColorFilter(getResources().getColor(R.color.im_red));

					//				Drawable newColor = titleImage.getDrawable();
					//				newColor.setColorFilter(getResources().getColor(R.color.im_red), PorterDuff.Mode.SRC_ATOP);
					//				titleImage.setImageDrawable(newColor);
				}
			}
			else if(GlobalVariable.viewmodel == 2){
				TextView tvchannel = findViewById(R.id.ct_title_channelname);
				if(String.valueOf(m.obj).equals("success")) {
					tvchannel.setTextColor(Color.WHITE);
				}
				else{
					tvchannel.setTextColor(Color.RED);
				}
			}
			else if(GlobalVariable.viewmodel == 3){
				TextView tvchannel = findViewById(R.id.ct_title_channelname);
				if(String.valueOf(m.obj).equals("success")) {
					tvchannel.setTextColor(Color.WHITE);
				}
				else{
					tvchannel.setTextColor(Color.RED);
				}
			}

		}
		else if(m.what == 1012){
			if(GlobalVariable.charactername.equals("director"))
			{
				setCallRecordInfo(currentCallWithState);
				new Thread() {
					@Override
					public void run() {
						try {
							HttpRequest request = new HttpRequest();
							String rs = request.postJson(
									GlobalVariable.SEHTTPServer + "/listchange",
									"{\"code\":" + 200 + ","
											+"\"operate\": \""+"edit"+ "\","
											+"\"phonenumber\": \""
											+ (currentCallWithState.dailout?currentCallWithState.pstn.substring(2):currentCallWithState.pstn) + "\","
											+"\"type\": \"all\","
											+"\"iswhitelist\": "+ (currentCallWithState.iswhitelist==null?0:currentCallWithState.iswhitelist)+ ","
											+"\"isblacklist\": "+ (currentCallWithState.iswhitelist==null?0:currentCallWithState.iswhitelist)
											+ "}"
							);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}.start();

			}
			else{
				setCallRecordInfoMbc(currentCallWithState);
			}
		}
		else if(m.what == 1013){
			autoHoldCallPdr();
		}
		else if(m.what == 1014){
			if(!GlobalVariable.sysconfig_notify) {
				String msg_content = String.valueOf(m.obj);
				MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this, "系统状态已更改\n" + msg_content).create();
				msgNotifyDialog.show();
				GlobalVariable.sysconfig_notify = true;
				if (GlobalVariable.viewmodel == 1)
					msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
				else if (GlobalVariable.viewmodel == 2)
					msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
				else if (GlobalVariable.viewmodel == 3)
					msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
				msgNotifyDialog.getWindow().setLayout(500, 400);

				final Timer t = new Timer();
				t.schedule(new TimerTask() {
					public void run() {
						msgNotifyDialog.dismiss();
						t.cancel();
					}
				}, 2000);
			}


		}
		//处理导播转接的选择
		else if(m.what == 1016){
			String to_sipid  = String.valueOf(m.obj);
			if(!to_sipid.equals("null")){
				//发送DAOBO TO DAOBO 消息
			JSONArray outcalluri_array = new JSONArray();
			//需要放置的对象类型
			MyCallWithState callWithState = allCallinList.get(callinListSelectedIdx);
			if(callWithState.type != CALL_TYPE.NEWINIT) {
				//先使用测试导播
				String msg_to_send = "{ \"msgType\": \"DAOBO TO DAOBO\", \"DaoBoCallUri\": \""
						+ "<sip:"+to_sipid+"@"+GlobalVariable.EUSERVER+">"//这里是导播发消息,
						+ "\", \"ToOutCallUri\": [{"
						+ "\"outcalluri\": "+"\""+callWithState.outcalluri+"\","
						+ "\"uid\": "+"\""+callWithState.uid+"\","
						+ "\"type\": "+"\""+callWithState.type+"\","
						+ "\"callednumber\": "+"\""+callWithState.callednumber+"\","
						+ "\"state\": "+"\""+callWithState.state+"\""
						+ "}] }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				callinList.remove(callWithState.id);
				allCallinList.remove(callWithState.id);
				updateShowIndex();
				if(callinListSelectedIdx == callWithState.id){
					callinListSelectedIdx = -1;
				}
				currentCallNum--;
				resetMainView();
				updateInfoAndButton();
			}
			}

		}
		/**处理修改模式后重启**/
		else if(m.what == 1017){
			String to_sipid = "";
			for(ProgramDirector pdr :GlobalVariable.pdrList){
				if(to_sipid.equals("")&&pdr.sipid.compareTo(GlobalVariable.user_sipid)!=0){
					to_sipid = String.valueOf(pdr.sipid);
				}
			}
			if(allCallinList.size()>0 && GlobalVariable.sipmodel == 2 && !to_sipid.equals(""))
			{
				//有未处理电话,弹出转接窗口
				//自动转接，手动选择转接
				//发送DAOBO TO DAOBO 消息
				//需要放置的对象类型

				String msg_to_send = "{ \"msgType\": \"DAOBO TO DAOBO\", \"DaoBoCallUri\": \""
						+ "<sip:"+to_sipid+"@"+GlobalVariable.EUSERVER+">\", " +
						"\"ToOutCallUri\": [" ;
				for(MyCallWithState callWithState : allCallinList){
					if(callWithState.type != CALL_TYPE.NEWINIT && callWithState.type != CALL_TYPE.ENDCALL) {
						msg_to_send += "{\"outcalluri\": "+"\""+callWithState.outcalluri+"\","
								+ "\"uid\": "+"\""+callWithState.uid+"\","
								+ "\"type\": "+"\""+callWithState.type+"\","
								+ "\"callednumber\": "+"\""+callWithState.callednumber+"\","
								+ "\"state\": "+"\""+callWithState.state+"\""
								+ "}" ;
						msg_to_send += ",";
					}
				}
				msg_to_send = msg_to_send.substring(0,msg_to_send.length()-1);
				msg_to_send += "]}";

				Log.e("msg_to_send",msg_to_send);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			}
			if(onlycall!=null)
			{
				CallOpParam prm = new CallOpParam();
				prm.setStatusCode(pjsip_status_code.PJSIP_SC_BUSY_HERE);
				try {
					onlycall.hangup(prm);
					Log.e("主动挂断电话","SOSOS");
				} catch (Exception e) {}
			}
			String msg_to_send	= null;
			if(GlobalVariable.charactername.equals("director"))
			{
				msg_to_send = "{ \"msgType\": \"DEVICE OFFLINE\", \"DaoBoCallUri\": \""
						+ localUri + "\","
						+"\"ZhuBoCallUri\": \""+"\""
						+" }";
			}
			else{
				msg_to_send = "{ \"msgType\": \"DEVICE OFFLINE\", \"DaoBoCallUri\": \""
						+"\","
						+"\"ZhuBoCallUri\": \""+ localUri + "\""
						+" }";
			}
			if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			unbindService(conn);
			unbindService(serviceConnection);
			try {
				if (receiver != null) {
					unregisterReceiver(receiver);
				}
				if (wsReceiver != null) {
					unregisterReceiver(wsReceiver);
				}
			}catch (Exception e){
				e.printStackTrace();
			}

			//退出前清空所有电话
			callinList.clear();
			allCallinList.clear();
			app.deinit();

			finish();

			final Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			//杀掉以前进程
			android.os.Process.killProcess(android.os.Process.myPid());
		}
		/**处理输入管理员密码后强制退出**/
		else if(m.what == 1018){
			//解除锁定状态
			Context context = this.getApplicationContext();//.getContext();
			DevicePolicyManager dpm =
					(DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
			//ComponentName adminName = getComponentName();
			ComponentName adminName = new ComponentName(this, AdminReceiver.class);

			DevicePolicyManager devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
			boolean isAdminActive = devicePolicyManager.isAdminActive(adminName);
			if(!isAdminActive){//这一句一定要有...
				Intent intent = new Intent();
				//指定动作
				intent.setAction(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
				//指定给那个组件授权
				intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, adminName);
				startActivity(intent);
			}
			dpm.setLockTaskPackages(adminName, APP_PACKAGES);
			// Set an option to turn on lock task mode when starting the activity.
			ActivityOptions options = ActivityOptions.makeBasic();
			options.setLockTaskEnabled(true);

			// Start our kiosk app's main activity with our lock task mode option.
//        PackageManager packageManager = context.getPackageManager();
//        Intent launchIntent = packageManager.getLaunchIntentForPackage(KIOSK_PACKAGE);
//        if (launchIntent != null) {
//            context.startActivity(launchIntent, options.toBundle());
//        }
			if (dpm.isLockTaskPermitted(this.getPackageName())) {
				stopLockTask();
			}
			else{
				Log.e("取消锁屏失败","没有权限");
			}

//			String to_sipid = "";
//			for(ProgramDirector pdr :GlobalVariable.pdrList){
//				if(to_sipid.equals("")&&pdr.sipid.compareTo(Long.parseLong(GlobalVariable.user_sipid))!=0){
//					to_sipid = String.valueOf(pdr.sipid);
//				}
//			}
//			if(allCallinList.size()>0 && GlobalVariable.sipmodel == 2 && !to_sipid.equals(""))
//			{
//				//有未处理电话,弹出转接窗口
//				//自动转接，手动选择转接
//				//发送DAOBO TO DAOBO 消息
//				//需要放置的对象类型
//
//				String msg_to_send = "{ \"msgType\": \"DAOBO TO DAOBO\", \"DaoBoCallUri\": \""
//						+ "<sip:"+to_sipid+"@"+GlobalVariable.EUSERVER+">\", " +
//						"\"ToOutCallUri\": [" ;
//				for(MyCallWithState callWithState : allCallinList){
//					if(callWithState.type != CALL_TYPE.NEWINIT && callWithState.type != CALL_TYPE.ENDCALL) {
//						msg_to_send += "{\"outcalluri\": "+"\""+callWithState.outcalluri+"\","
//								+ "\"uid\": "+"\""+callWithState.uid+"\","
//								+ "\"type\": "+"\""+callWithState.type+"\","
//								+ "\"callednumber\": "+"\""+callWithState.callednumber+"\","
//								+ "\"state\": "+"\""+callWithState.state+"\""
//								+ "}" ;
//						msg_to_send += ",";
//					}
//				}
//				msg_to_send = msg_to_send.substring(0,msg_to_send.length()-1);
//				msg_to_send += "]}";
//
//				Log.e("msg_to_send",msg_to_send);
//				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
//			}
//			if(onlycall!=null)
//			{
//				CallOpParam prm = new CallOpParam();
//				prm.setStatusCode(pjsip_status_code.PJSIP_SC_BUSY_HERE);
//				try {
//					onlycall.hangup(prm);
//					Log.e("主动挂断电话","SOSOS");
//				} catch (Exception e) {}
//			}
//			String msg_to_send	= null;
//			if(GlobalVariable.charactername.equals("director"))
//			{
//				msg_to_send = "{ \"msgType\": \"DEVICE OFFLINE\", \"DaoBoCallUri\": \""
//						+ localUri + "\","
//						+"\"ZhuBoCallUri\": \""+"\""
//						+" }";
//			}
//			else{
//				msg_to_send = "{ \"msgType\": \"DEVICE OFFLINE\", \"DaoBoCallUri\": \""
//						+"\","
//						+"\"ZhuBoCallUri\": \""+ localUri + "\""
//						+" }";
//			}
//			if(wsControl!=null)wsControl.sendMessage(msg_to_send);
//
//			unbindService(conn);
////				Intent intent_music = new Intent(this, MusicService.class);
////				stopService(intent_music);
//
//			unbindService(serviceConnection);
////				Intent intent = new Intent(this, JWebSocketClientService.class);
////				stopService(intent);
//
//			//2021-03-17
//			try {
//				if (receiver != null) {
//					unregisterReceiver(receiver);
//				}
//				if (wsReceiver != null) {
//					unregisterReceiver(wsReceiver);
//				}
//			}catch (Exception e){
//				e.printStackTrace();
//			}
//
//			//退出前清空所有电话
//			callinList.clear();
//			allCallinList.clear();
//
//			app.deinit();
//			finish();
//			Runtime.getRuntime().gc();
//			android.os.Process.killProcess(android.os.Process.myPid());
//			finish();
		}
		/**处理信息按钮点击后自动重拨逻辑**/
		else if(m.what == 1019){
			Intent intent = new Intent(this, DailActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
		}
		/**处理拨号弹窗弹出**/
		else if(m.what == 1020){
			String keycode = String.valueOf(m.obj);
			showCtDailPanelDialog(keycode);
		}
		/**处理导播声音直接切入直播间**/
		else if(m.what == 1021){
			String msg_to_send = "{ \"msgType\": \"START DAOBO LIVING\", " +
					"\"DaoBoCallUri\": \""+localUri+"\"}";
			if(wsControl!=null)wsControl.sendMessage(msg_to_send);
		}
		/**处理导播声音直接切出直播间**/
		else if(m.what == 1022){
			String msg_to_send = "{ \"msgType\": \"STOP DAOBO LIVING\", " +
					"\"DaoBoCallUri\": \""+localUri+"\"}";
			if(wsControl!=null)wsControl.sendMessage(msg_to_send);
		}

		/**话筒摘机/挂机逻辑处理**/
		else if(m.what == 2000){
			//挂机逻辑处理
			//仅有一通电话，判断电话状态是否是导播接听或者是主播接听就好了，如果是，直接挂断

			if(allCallinList.size() == 1){
				//currentCallWithState = allCallinList.get(0);
				if(currentCallWithState!=null&&(currentCallWithState.type == CALL_TYPE.DAOBOACPT || currentCallWithState.type == CALL_TYPE.ZHUBOACPT)){
					if(!currentCallWithState.syncshow) {
						hangupCall(null);
					}
				}
			}
			//20230602处理syncshowcall问题，syncshowcall应该排除在处理电话列表外
			else if(allCallinList.size() > 1){
				for(MyCallWithState callws : allCallinList){
					//if(currentCallWithState.type == CALL_TYPE.DAOBOACPT || currentCallWithState.type == CALL_TYPE.ZHUBOACPT){
					if(callws.id == localacceptcall.id && callws.syncshow == false){
						callinListSelectedIdx = -1;
						currentCallWithState = callws;
					}
				}
				if(currentCallWithState!=null
						&&(currentCallWithState.type == CALL_TYPE.DAOBOACPT || currentCallWithState.type == CALL_TYPE.ZHUBOACPT)
						&&currentCallWithState.syncshow==false)
				{
					hangupCall(null);
				}
			}
			else{
                closeCtDailPanelDialog();
            }
		}
		else if(m.what == 2001 && GlobalVariable.charactername.equals("director")){
			//摘机处理逻辑
			if(allCallinList.size() == 0){
				boolean btn_enable = true;
				if(GlobalVariable.viewmodel == 1){
					ImageButton imgBtn = findViewById(R.id.buttonICON2);
					if(!imgBtn.isEnabled()){
						btn_enable = false;
					}

					if(btn_enable) {
						overridePendingTransition(0, 0);
						Intent intent = new Intent(this, DailActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
						startActivity(intent);
					}
				}
				else if(GlobalVariable.viewmodel == 2){
					//20210818不再跳转到拨号界面而是选择调出拨号弹窗
					//					ImageButton imgBtn = findViewById(R.id.ctTitleDail);
					//					if(!imgBtn.isEnabled()){
					//						imgBtn.setImageDrawable(getDrawable(R.drawable.ct_title_dail_disable));
					//					}
					//					else{
					//						btn_enable = false;
					//					}
					showCtDailPanelDialog("");
				}
				else if(GlobalVariable.viewmodel == 3){
					showCtDailPanelDialog("");
				}

			}
			else if(allCallinList.size() == 1) {
				if(currentCallWithState!=null&&(currentCallWithState.type == CALL_TYPE.NEWINIT || currentCallWithState.type == CALL_TYPE.WAITDAOBO)){
					acceptCall(null);
				}
			}
			//20230602处理syncshowcall问题，syncshowcall应该排除在处理电话列表外
			else if(allCallinList.size() > 1){
				for(MyCallWithState callws : allCallinList){
					if(callws!=null&&callws.type == CALL_TYPE.NEWINIT&&callws.syncshow==false){
						if(callinListSelectedIdx == callws.id){
							callinListSelectedIdx = -1;
							currentCallWithState = callws;
							acceptCall(null);
						}
					}
//					if(currentCallWithState!=null&&(currentCallWithState.type == CALL_TYPE.NEWINIT)){
//						Log.e("电话id",currentCallWithState.id+"");
//						if(callinListSelectedIdx == currentCallWithState.id){
//							callinListSelectedIdx = -1;
//							currentCallWithState = callws;
//							acceptCall(null);
//						}
//					}
				}
			}
		}
		else if(m.what == 2002){
			LED_Media_Notify();
		}
		else if(m.what == 2003){
			LED_Media_Pause();
		}
		else if(m.what == 2004){
			Log.e("调试","准备刷新界面"
					+GlobalVariable.linebusy1
					+GlobalVariable.linebusy2
					+GlobalVariable.linebusy3
					+GlobalVariable.linebusy4
					+GlobalVariable.linebusy5
					+GlobalVariable.linebusy6
			);
			updateInfoAndButton();
		}

		/**远程导播接听**/
		else if(m.what == 3000){
			String uid = String.valueOf(m.obj);
			LED_Media_Pause();
			GlobalVariable.PhoneRedTitle = false;
			for(MyCallWithState callws : allCallinList){
				if(uid.equals(callws.uid)){

					if (GlobalVariable.charactername.equals("director")) {
						setCallRecordInfo(callws);
					}
					if (callws.type == CALL_TYPE.NEWINIT) {
						updateCallRecordST(callws.uid,1,CALL_TYPE.DAOBOACPT);
						//外线接进来的
						GlobalVariable.currentaccept_uid = callws.uid;
						autoHoldCallPdr();
						callws.wait_ackaccept = true;
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						String msg_to_send = "{ \"msgType\": \"DAOBO MAKE CALL\", " +
								(GlobalVariable.autoswitch?"\"autoswitch\": \"1\",":" ")+
								"\"callUri_id\": \""+0+"\", " +
								"\"OutCallUri\": \""+callws.outcalluri+"\", " +
								"\"DaoBoCallUri\": \""+localUri+"\", " +
								"\"Record\":"+callws.record+" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);

						localacceptcall = callws;
					}
					else if (callws.type == CALL_TYPE.WAITDAOBO) {
						updateCallRecordST(callws.uid,1,CALL_TYPE.DAOBOACPT);
						//这是从主播返回的
						//导播通知SIP服务器，应答主播返回电话
						GlobalVariable.currentaccept_uid = callws.uid;

						autoHoldCallPdr();
						callws.wait_ackaccept = true;
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						String msg_to_send = "{ \"msgType\": \"ACK ZHUBO TO DAOBO\", \"OutCallUri\": \""
								+ callws.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ localUri//这里是导播发消息
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);

						//状态转为接听状态
						callws.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
						callws.type = CALL_TYPE.DAOBOACPT;

						localacceptcall = callws;
						//broadcastCallType(callws, CALL_TYPE.DAOBOACPT);
					}
					else if (callws.type == CALL_TYPE.DAOBOHOLD) {
						updateCallRecordST(callws.uid,1,CALL_TYPE.DAOBOACPT);
						//导播取消保持
						GlobalVariable.currentaccept_uid = callws.uid;
						autoHoldCallPdr();
						callws.wait_ackhold = true;
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						String msg_to_send =
								"{ \"msgType\": \"DAOBO UNHOLD\", \"OutCallUri\": \""
										+ callws.outcalluri
										+ "\", \"DaoBoCallUri\": \""
										+ localUri
										+ "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					}
					else if (callws.type == CALL_TYPE.LIVEACPT || callws.type == CALL_TYPE.LIVEHOLD){
						if(GlobalVariable.charactername.equals("director")){
							updateCallRecordST(callws.uid,1,CALL_TYPE.DAOBOACPT);
							//外线接进来的
							GlobalVariable.currentaccept_uid = callws.uid;
							autoHoldCallPdr();
							callws.wait_ackaccept = true;
							if(GlobalVariable.viewmodel==1) {
								updateOperateButton();
							}
							else if(GlobalVariable.viewmodel==3){
								updateGsOperateButton();
							}
							String msg_to_send = "{ \"msgType\": \"DAOBO MAKE CALL\", " +
									"\"callUri_id\": \""+0+"\", " +
									"\"OutCallUri\": \""+callws.outcalluri+"\", " +
									"\"DaoBoCallUri\": \""+localUri+"\", " +
									"\"Record\":"+callws.record+" }";
							if(wsControl!=null)wsControl.sendMessage(msg_to_send);
						}
					}
					updateInfoAndButton();
					myAdapter.setDataList(allCallinList);
					mbcAdapter.setDataList(allCallinList);
				}
			}
		}
		/**远程导播挂断**/
		else if(m.what == 3001){
			String uid = String.valueOf(m.obj);
			MyCallWithState delete_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					delete_call = callws;
				}
			}
			if(delete_call !=null){
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if(GlobalVariable.currentaccept_uid.equals(delete_call.uid))
				{
					GlobalVariable.currentaccept_uid = "";
				}
				//存在线程处理时对象已被释放问题
				tempCall.pstn = delete_call.pstn;
				tempCall.uid = delete_call.uid;
				tempCall.name = delete_call.name;
				tempCall.male = delete_call.male;
				tempCall.age = delete_call.age;
				tempCall.from = delete_call.from;
				tempCall.iswhitelist = delete_call.iswhitelist;
				tempCall.isblacklist = delete_call.isblacklist;
				tempCall.iscontact = delete_call.iscontact;
				tempCall.isexpert = delete_call.isexpert;
				tempCall.level = delete_call.level;
				tempCall.province = delete_call.province;
				tempCall.city = delete_call.city;
				tempCall.address = delete_call.address;
				tempCall.content = delete_call.content;
				tempCall.pstnmark = delete_call.pstnmark;
				tempCall.pdrmark = delete_call.pdrmark;
				tempCall.mbcmark = delete_call.mbcmark;
				tempCall.remark = delete_call.remark;

				if (GlobalVariable.charactername.equals("director")) {
					setCallRecordInfo(tempCall);
				}
				//也是通过type来处理
				if(delete_call.type == CALL_TYPE.NEWINIT
						|| delete_call.type == CALL_TYPE.DIALINIT
				) {

					updateCallRecordST(delete_call.uid,2,CALL_TYPE.ENDCALL);
					Log.e("步骤4","4"+delete_call.pstn);
					String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					//2021 06 04去除
					if(wsControl!=null && delete_call.type != CALL_TYPE.WAITDAOBO)wsControl.sendMessage(msg_to_send);
					//未接听直接挂断
					delete_call.forcehangup =true;
					delete_call.type = CALL_TYPE.ENDCALL;
					broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
					try {
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
					}catch(Exception e){};
					updateShowIndex();
					if(callinListSelectedIdx == delete_call.id)
					{
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					resetMainView();
				}
				else if(delete_call.type == CALL_TYPE.WAITZHUBO)
				{
					//主播挂断不算拒绝
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					delete_call.type = CALL_TYPE.ENDCALL;
					broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
					try {
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
					}catch(Exception e){};
					updateShowIndex();
					if(callinListSelectedIdx == delete_call.id)
					{
						callinListView.clearChoices();
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					delete_call = null;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					resetMainView();
				}
				else if(delete_call.type == CALL_TYPE.DAOBOACPT
						||delete_call.type == CALL_TYPE.DAOBOHOLD
						||delete_call.type == CALL_TYPE.WAITDAOBO
				){
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					try {
						delete_call.type = CALL_TYPE.ENDCALL;
						broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
						updateShowIndex();
						if(callinListSelectedIdx == delete_call.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}

				}
				else if(delete_call.type == CALL_TYPE.ZHUBOACPT
						||delete_call.type == CALL_TYPE.ZHUBOHOLD
						||delete_call.type == CALL_TYPE.LIVEACPT
						||delete_call.type == CALL_TYPE.LIVEHOLD
				)
				{
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					try {
						delete_call.type = CALL_TYPE.ENDCALL;
						broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
						updateShowIndex();
						if(callinListSelectedIdx == delete_call.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				else
				{
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					try {
						delete_call.type = CALL_TYPE.ENDCALL;
						broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
						updateShowIndex();
						if(callinListSelectedIdx == delete_call.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				//处理挂断后挂断按钮状态不能正确刷新问题
				delete_call = null;
			}
		}
		/**远程导播保持**/
		else if(m.what == 3002){
			String uid = String.valueOf(m.obj);
			for(MyCallWithState callws : allCallinList) {
				if(uid.equals(callws.uid)) {
					LED_Media_Pause();
					GlobalVariable.PhoneRedTitle = false;
					if (GlobalVariable.charactername.equals("director")) {
						setCallRecordInfo(callws);
					}
					if (callws.type == CALL_TYPE.DAOBOACPT) {
						if(GlobalVariable.charactername.equals("director")){
							//导播保持
							updateCallRecordST(callws.uid,1,CALL_TYPE.DAOBOHOLD);
							if(GlobalVariable.currentaccept_uid.equals(callws.uid))
							{
								GlobalVariable.currentaccept_uid = "";
							}
							callws.wait_ackhold = true;
							if(GlobalVariable.viewmodel==1) {
								updateOperateButton();
							}
							else if(GlobalVariable.viewmodel==3){
								updateGsOperateButton();
							}
							String msg_to_send =
									"{ \"msgType\": \"DAOBO HOLD\", \"OutCallUri\": \""
											+ callws.outcalluri
											+ "\", \"DaoBoCallUri\": \""
											+ localUri//本机是导播直接使用localUri
											+ "\" }";
							if(wsControl!=null)wsControl.sendMessage(msg_to_send);
						}
					}

				}
			}
		}
		/**远程导播转主播**/
		else if(m.what == 3003){
			String uid = String.valueOf(m.obj);
			MyCallWithState switch_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					switch_call = callws;
				}
			}

			if(switch_call!=null) {
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if(GlobalVariable.currentaccept_uid.equals(switch_call.uid))
				{
					GlobalVariable.currentaccept_uid = "";
				}
				//存在线程处理时对象已被释放问题
				tempCall.pstn = switch_call.pstn;
				tempCall.uid = switch_call.uid;
				tempCall.name = switch_call.name;
				tempCall.male = switch_call.male;
				tempCall.age = switch_call.age;
				tempCall.from = switch_call.from;
				tempCall.iswhitelist = switch_call.iswhitelist;
				tempCall.isblacklist = switch_call.isblacklist;
				tempCall.iscontact = switch_call.iscontact;
				tempCall.isexpert = switch_call.isexpert;
				tempCall.level = switch_call.level;
				tempCall.province = switch_call.province;
				tempCall.city = switch_call.city;
				tempCall.address = switch_call.address;
				tempCall.content = switch_call.content;
				tempCall.pstnmark = switch_call.pstnmark;
				tempCall.pdrmark = switch_call.pdrmark;
				tempCall.mbcmark = switch_call.mbcmark;
				tempCall.remark = switch_call.remark;

				if (GlobalVariable.charactername.equals("director")) {
					setCallRecordInfo(tempCall);
				}
				if(switch_call.type == CALL_TYPE.DAOBOACPT)
				{
					if(GlobalVariable.charactername.equals("director")){
						updateCallRecordST(switch_call.uid,1,CALL_TYPE.WAITZHUBO);
						//导播通知SIP服务器，电话转给主播接听
						switch_call.state = CALL_STATE_TYPE.DAOBO_SWITCH;
						broadcastCallType(switch_call,CALL_TYPE.WAITZHUBO);
						String msg_to_send = "{ \"msgType\": \"DAOBO TO ZHUBO\", \"DaoBoCallUri\": \""
								+ localUri//这里是导播发消息
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\", \"OutCallUri\": \""
								+ switch_call.outcalluri + "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					}
					else{
						//当前是在主播端，出现导播接听状态代表电话呼出成功
						updateCallRecordST(switch_call.uid,1,CALL_TYPE.WAITDAOBO);
						switch_call.state = CALL_STATE_TYPE.ZHUBO_SWITCH;

						String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
								+ switch_call.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ switch_call.daobocalluri
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri + "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);

						//更新type为待导播接听
						broadcastCallType(switch_call,CALL_TYPE.WAITDAOBO);
						try {
							allCallinList.remove(switch_call.id);
							callinList.remove(switch_call.id);
							updateShowIndex();
							if(callinListSelectedIdx == switch_call.id)
							{
								callinListSelectedIdx = -1;
							}
							currentCallNum--;
							resetMainView();
							refreshCallList();
							if(GlobalVariable.viewmodel==1) {
								updateOperateButton();
							}
							else if(GlobalVariable.viewmodel==3){
								updateGsOperateButton();
							}
							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
						} catch (Exception e) {
							System.out.println(e);
						}
					}

				}
				else if(switch_call.type == CALL_TYPE.NEWINIT
						||switch_call.type == CALL_TYPE.WAITDAOBO)
				{
					updateCallRecordST(switch_call.uid,1,CALL_TYPE.WAITZHUBO);
					//先接电话再转电话
					Message m2 = Message.obtain(MainActivity.handler_, 3000, switch_call.uid);
					m2.sendToTarget();
					switch_call.autoswitch = true;
				}
			}
			myAdapter.setDataList(allCallinList);
			mbcAdapter.setDataList(allCallinList);
			updateInfoAndButton();
		}
		/**远程导播转接拉回**/
		else if(m.what == 3004){
			String uid = String.valueOf(m.obj);
			MyCallWithState revert_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					revert_call = callws;
				}
			}
			if(revert_call != null){
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if (revert_call.type == CALL_TYPE.WAITZHUBO) {
					if(GlobalVariable.charactername.equals("director"))
					{
						updateCallRecordST(revert_call.uid,1,CALL_TYPE.DAOBOACPT);
						//当前是导播客户端，需要取消转接操作
						String msg_to_send = "{ \"msgType\": \"DAOBO CANCEL SWITCH\", \"DaoBoCallUri\": \""
								+ localUri//这里是导播发消息
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\", \"OutCallUri\": \""
								+ revert_call.outcalluri + "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					}
				}
				updateInfoAndButton();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
			}
		}
		/**远程导播呼出电话**/
		else if(m.what == 3005){
			List<String> CallParam = (List<String>) m.obj;
			String dailtext = CallParam.get(0);
			String contacturi = CallParam.get(1);
			Integer prefix_id = GlobalVariable.ctNumberMap.get(contacturi);
			String prefix = "0"+prefix_id;
			String phonenumber = "";
			phonenumber = prefix+ dailtext;

			String daoboUri = "";
			String zhuboUri = "";
			if(GlobalVariable.charactername.equals("director"))
			{
				daoboUri = MainActivity.localUri;
			}
			else
			{
				zhuboUri = MainActivity.zhuboUri;
			}
			phonenumber = phonenumber.replace("#","A");
			String msg_to_send =
					"{ \"msgType\": \"DAIL OUT CALL\", " +
							"\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
							"\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
							"\"Pstn\": \""+ phonenumber+ "\", " +
							"\"contacturi\": \""+ contacturi+ "\"" +
							" }";
			if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

			List<String> numberlist = new ArrayList<>();
			numberlist.add("<sip:"+phonenumber+"@"+GlobalVariable.EUSERVER+">");
			numberlist.add(contacturi);
			Message m2 = Message.obtain(MainActivity.handler_,1008,numberlist);
			m2.sendToTarget();

			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
		}
		/**远程主播接听**/
		else if(m.what == 3006){
			String uid = String.valueOf(m.obj);
			MyCallWithState accept_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					accept_call = callws;
				}
			}
			if(accept_call != null){
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if (GlobalVariable.charactername.equals("director")) {} else {
					setCallRecordInfoMbc(accept_call);
				}
				if (accept_call.type == CALL_TYPE.NEWINIT) {
					updateCallRecordST(accept_call.uid,1,CALL_TYPE.DAOBOACPT);
					//外线接进来的
					GlobalVariable.currentaccept_uid = accept_call.uid;
					autoHoldCallPdr();
					accept_call.wait_ackaccept = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send = "{ \"msgType\": \"DAOBO MAKE CALL\", " +
							(GlobalVariable.autoswitch?"\"autoswitch\": \"1\",":" ")+
							"\"callUri_id\": \""+0+"\", " +
							"\"OutCallUri\": \""+accept_call.outcalluri+"\", " +
							"\"DaoBoCallUri\": \""+localUri+"\", " +
							"\"Record\":"+accept_call.record+" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else if (accept_call.type == CALL_TYPE.WAITZHUBO) {
					if(GlobalVariable.charactername.equals("director")){}
					else {
						updateCallRecordST(accept_call.uid, 1, CALL_TYPE.ZHUBOACPT);
						//外线或者导播接进来的
						GlobalVariable.currentaccept_uid = accept_call.uid;
						autoHoldCallAll();
						accept_call.wait_ackaccept = true;
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						String msg_to_send = "{ \"msgType\": \"ZHUBO MAKE CALL\", " +
								"\"callUri_id\": \"" + 0 + "\", " +
								"\"OutCallUri\": \"" + accept_call.outcalluri + "\", " +
								"\"ZhuBoCallUri\": \"" + localUri + "\", " +
								"\"Record\":" + accept_call.record + " }";
						if (wsControl != null) wsControl.sendMessage(msg_to_send);
					}
				}
				else if (accept_call.type == CALL_TYPE.ZHUBOHOLD) {
					if(accept_call.dailout){
						updateCallRecordST(accept_call.uid,1,CALL_TYPE.DAOBOACPT);
					}
					else {
						updateCallRecordST(accept_call.uid, 1, CALL_TYPE.ZHUBOACPT);
					}
					//主播取消保持
					GlobalVariable.currentaccept_uid = accept_call.uid;
					autoHoldCallAll();
					accept_call.wait_ackhold = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO UNHOLD\", \"OutCallUri\": \""
									+ accept_call.outcalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);


				}
				else if (accept_call.type == CALL_TYPE.LIVEACPT || accept_call.type == CALL_TYPE.LIVEHOLD){
					if(GlobalVariable.charactername.equals("director")){}
					else{
						updateCallRecordST(accept_call.uid, 1, CALL_TYPE.ZHUBOACPT);
						//外线或者导播接进来的
						GlobalVariable.currentaccept_uid = accept_call.uid;
						autoHoldCallAll();
						accept_call.wait_ackaccept = true;
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}

						String msg_to_send = "{ \"msgType\": \"ZHUBO MAKE CALL\", " +
								"\"callUri_id\": \"" + 0 + "\", " +
								"\"OutCallUri\": \"" + accept_call.outcalluri + "\", " +
								"\"ZhuBoCallUri\": \"" + localUri + "\", " +
								"\"Record\":" + accept_call.record + " }";
						if (wsControl != null) wsControl.sendMessage(msg_to_send);
					}
				}
				updateInfoAndButton();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
			}
		}
		/**远程主播保持**/
		else if(m.what == 3007){
			String uid = String.valueOf(m.obj);
			MyCallWithState hold_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					hold_call = callws;
				}
			}
			if(hold_call != null) {
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if (GlobalVariable.charactername.equals("director")) {} else {
					setCallRecordInfoMbc(currentCallWithState);
				}
				if (currentCallWithState.type == CALL_TYPE.ZHUBOACPT) {
					//主播保持
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ZHUBOHOLD);
					if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
					{
						GlobalVariable.currentaccept_uid = "";
					}
					currentCallWithState.wait_ackhold = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				updateInfoAndButton();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
			}
		}
		/**远程主播转回导播**/
		else if(m.what == 3008){
			String uid = String.valueOf(m.obj);
			MyCallWithState switch_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					switch_call = callws;
				}
			}

			if(switch_call!=null) {
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if(GlobalVariable.currentaccept_uid.equals(switch_call.uid))
				{
					GlobalVariable.currentaccept_uid = "";
				}
				//存在线程处理时对象已被释放问题
				tempCall.pstn = switch_call.pstn;
				tempCall.uid = switch_call.uid;
				tempCall.name = switch_call.name;
				tempCall.male = switch_call.male;
				tempCall.age = switch_call.age;
				tempCall.from = switch_call.from;
				tempCall.iswhitelist = switch_call.iswhitelist;
				tempCall.isblacklist = switch_call.isblacklist;
				tempCall.iscontact = switch_call.iscontact;
				tempCall.isexpert = switch_call.isexpert;
				tempCall.level = switch_call.level;
				tempCall.province = switch_call.province;
				tempCall.city = switch_call.city;
				tempCall.address = switch_call.address;
				tempCall.content = switch_call.content;
				tempCall.pstnmark = switch_call.pstnmark;
				tempCall.pdrmark = switch_call.pdrmark;
				tempCall.mbcmark = switch_call.mbcmark;
				tempCall.remark = switch_call.remark;

				if (GlobalVariable.charactername.equals("director")) {} else {
					setCallRecordInfoMbc(tempCall);
				}
				if(switch_call.type == CALL_TYPE.WAITZHUBO)
				{
					//20210510新需求使用接听按键处理主播待接听转回
					if(GlobalVariable.charactername.equals("director")){
						//导播控制主播盒（20220901）
						updateCallRecordST(switch_call.uid,1,CALL_TYPE.WAITDAOBO);
						//主播通知SIP服务器，应答主播返回电话
						String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
								+ switch_call.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ switch_call.daobocalluri
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri + "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
						broadcastCallType(switch_call,CALL_TYPE.WAITDAOBO);
					}
					else{
						updateCallRecordST(switch_call.uid,1,CALL_TYPE.WAITDAOBO);
						//先接电话再转电话
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						acceptCall(null);
						switch_call.autoswitch = true;
					}

				}
				else if(switch_call.type == CALL_TYPE.ZHUBOACPT){
					updateCallRecordST(switch_call.uid,1,CALL_TYPE.WAITDAOBO);
					//主播通知SIP服务器，应答主播返回电话
					String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
							+ switch_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ switch_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri + "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					//通知完毕后直接在这边将该电话挂断，并将界面中的电话移除
					try {

						broadcastCallType(switch_call,CALL_TYPE.WAITDAOBO);
						if (GlobalVariable.charactername.equals("broadcaster")) {
							allCallinList.remove(switch_call.id);
							callinList.remove(switch_call.id);
							updateShowIndex();
							if (callinListSelectedIdx == switch_call.id) {
								callinListSelectedIdx = -1;
							}
							currentCallNum--;
							resetMainView();
							refreshCallList();
							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
						}
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				else if(switch_call.type == CALL_TYPE.LIVEACPT || switch_call.type == CALL_TYPE.LIVEHOLD) {
					updateCallRecordST(switch_call.uid, 1, CALL_TYPE.WAITDAOBO);
					switch_call.state = CALL_STATE_TYPE.ZHUBO_SWITCH;
					String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
							+ switch_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ switch_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri + "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);

					//更新type为待导播接听
					broadcastCallType(switch_call, CALL_TYPE.WAITDAOBO);
					if (GlobalVariable.charactername.equals("broadcaster")) {
						try {
							allCallinList.remove(switch_call.id);
							callinList.remove(switch_call.id);
							updateShowIndex();
							if (callinListSelectedIdx == switch_call.id) {
								callinListSelectedIdx = -1;
							}
							currentCallNum--;
							//处理多通电话转出时按钮和后台电话不对应的问题，按钮状态不能正确刷新
							switch_call = null;
							resetMainView();
							refreshCallList();
							if(GlobalVariable.viewmodel==1) {
								updateOperateButton();
							}
							else if(GlobalVariable.viewmodel==3){
								updateGsOperateButton();
							}
							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
						} catch (Exception e) {
							System.out.println(e);
						}
					}
				}
				updateInfoAndButton();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
			}
		}
		/**远程直播**/
		else if(m.what == 3009){
			String uid = String.valueOf(m.obj);
			MyCallWithState live_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					live_call = callws;
				}
			}
			if(live_call!=null) {
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if (
						live_call.type == CALL_TYPE.NEWINIT
								|| live_call.type == CALL_TYPE.WAITDAOBO
								|| live_call.type == CALL_TYPE.DAOBOACPT
				) {
					//设置直播锁定
					live_call.live_lock = true;
					//导播直接转直播的情况，待处理
					updateCallRecordST(live_call.uid, 1, CALL_TYPE.LIVEACPT);
					if (!live_call.virtual) {
						live_call.virtual = true;
					}
					live_call.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ live_call.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ live_call.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ live_call.record
									+ " }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				else if (live_call.type == CALL_TYPE.WAITZHUBO
						|| live_call.type == CALL_TYPE.ZHUBOACPT
				) {
					//设置直播锁定
					live_call.live_lock = true;
					updateCallRecordST(live_call.uid, 1, CALL_TYPE.LIVEACPT);
					if (!live_call.virtual) {
						live_call.virtual = true;
					}
					live_call.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ live_call.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ live_call.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ live_call.record
									+ " }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				else if (live_call.type == CALL_TYPE.LIVEHOLD) {
					//设置直播锁定
					live_call.live_lock = true;
					updateCallRecordST(live_call.uid, 1, CALL_TYPE.LIVEACPT);
					live_call.wait_ackhold = true;
					String msg_to_send =
							"{ \"msgType\": \"VIRTUAL UNHOLD\", \"OutCallUri\": \""
									+ live_call.outcalluri
									+ "\", \"VirtualCallUri\": \""
									+ null
									+ "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				else if (live_call.type == CALL_TYPE.DAOBOHOLD
						|| live_call.type == CALL_TYPE.ZHUBOHOLD
				) {
					//设置直播锁定
					live_call.live_lock = true;
					updateCallRecordST(live_call.uid, 1, CALL_TYPE.LIVEACPT);
					if (!live_call.virtual) {
						live_call.virtual = true;
					}
					live_call.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ live_call.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ live_call.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ live_call.record
									+ " }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				updateInfoAndButton();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
			}
		}
		/**远程直播保持**/
		else if(m.what == 3010){
			String uid = String.valueOf(m.obj);
			MyCallWithState livehold_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					livehold_call = callws;
				}
			}
			if(livehold_call!=null) {
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if (livehold_call.type == CALL_TYPE.LIVEACPT) {
					if (!livehold_call.virtual) {
						livehold_call.virtual = true;
					}
					updateCallRecordST(livehold_call.uid, 1, CALL_TYPE.LIVEHOLD);
					livehold_call.wait_ackhold = true;
					String msg_to_send =
							"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
									+ livehold_call.outcalluri
									+ "\", \"VirtualCallUri\": \""
									+ null
									+ "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				updateInfoAndButton();
				myAdapter.setDataList(allCallinList);
				mbcAdapter.setDataList(allCallinList);
			}
		}
		/**远程主播呼出电话**/
		else if(m.what == 3011){
			List<String> CallParam = (List<String>) m.obj;
			String dailtext = CallParam.get(0);
			String contacturi = CallParam.get(1);
			Integer prefix_id = GlobalVariable.ctNumberMap.get(contacturi);
			String prefix = "0"+prefix_id;
			String phonenumber = "";
			phonenumber = prefix+ dailtext;

			String daoboUri = "";
			String zhuboUri = "";
			if(GlobalVariable.charactername.equals("director"))
			{
				daoboUri = MainActivity.localUri;
			}
			else
			{
				zhuboUri = MainActivity.zhuboUri;
			}
			phonenumber = phonenumber.replace("#","A");
			String msg_to_send =
					"{ \"msgType\": \"DAIL OUT CALL\", " +
							"\"DaoBoCallUri\": \""+ daoboUri+ "\", " +
							"\"ZhuBoCallUri\": \""+ zhuboUri+ "\", " +
							"\"Pstn\": \""+ phonenumber+ "\", " +
							"\"contacturi\": \""+ contacturi+ "\"" +
							" }";
			if(MainActivity.wsControl!=null)MainActivity.wsControl.sendMessage(msg_to_send);

			List<String> numberlist = new ArrayList<>();
			numberlist.add("<sip:"+phonenumber+"@"+GlobalVariable.EUSERVER+">");
			numberlist.add(contacturi);
			Message m2 = Message.obtain(MainActivity.handler_,1008,numberlist);
			m2.sendToTarget();

			Intent intent = new Intent(this, MainActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			startActivity(intent);
		}
		/**远程主播挂断电话**/
		else if(m.what == 3012){
			String uid = String.valueOf(m.obj);
			MyCallWithState delete_call = null;
			for(MyCallWithState callws : allCallinList) {
				if (uid.equals(callws.uid)) {
					delete_call = callws;
				}
			}
			if(delete_call !=null){
				LED_Media_Pause();
				GlobalVariable.PhoneRedTitle = false;
				if(GlobalVariable.currentaccept_uid.equals(delete_call.uid))
				{
					GlobalVariable.currentaccept_uid = "";
				}
				//存在线程处理时对象已被释放问题
				tempCall.pstn = delete_call.pstn;
				tempCall.uid = delete_call.uid;
				tempCall.name = delete_call.name;
				tempCall.male = delete_call.male;
				tempCall.age = delete_call.age;
				tempCall.from = delete_call.from;
				tempCall.iswhitelist = delete_call.iswhitelist;
				tempCall.isblacklist = delete_call.isblacklist;
				tempCall.iscontact = delete_call.iscontact;
				tempCall.isexpert = delete_call.isexpert;
				tempCall.level = delete_call.level;
				tempCall.province = delete_call.province;
				tempCall.city = delete_call.city;
				tempCall.address = delete_call.address;
				tempCall.content = delete_call.content;
				tempCall.pstnmark = delete_call.pstnmark;
				tempCall.pdrmark = delete_call.pdrmark;
				tempCall.mbcmark = delete_call.mbcmark;
				tempCall.remark = delete_call.remark;

				if (GlobalVariable.charactername.equals("director")) {
					setCallRecordInfo(tempCall);
				}
				//也是通过type来处理
				if(delete_call.type == CALL_TYPE.NEWINIT
						|| delete_call.type == CALL_TYPE.DIALINIT
				) {

					updateCallRecordST(delete_call.uid,2,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					//2021 06 04去除
					if(wsControl!=null && delete_call.type != CALL_TYPE.WAITDAOBO)wsControl.sendMessage(msg_to_send);
					//未接听直接挂断
					delete_call.forcehangup =true;
					delete_call.type = CALL_TYPE.ENDCALL;
					broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
					try {
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
					}catch(Exception e){};
					updateShowIndex();
					if(callinListSelectedIdx == delete_call.id)
					{
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					resetMainView();
				}
				else if(delete_call.type == CALL_TYPE.WAITZHUBO)
				{
					//主播挂断不算拒绝
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					delete_call.type = CALL_TYPE.ENDCALL;
					broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
					try {
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
					}catch(Exception e){};
					updateShowIndex();
					if(callinListSelectedIdx == delete_call.id)
					{
						callinListView.clearChoices();
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					delete_call = null;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					resetMainView();
				}
				else if(delete_call.type == CALL_TYPE.DAOBOACPT
						||delete_call.type == CALL_TYPE.DAOBOHOLD
						||delete_call.type == CALL_TYPE.WAITDAOBO
				){
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					try {
						delete_call.type = CALL_TYPE.ENDCALL;
						broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
						updateShowIndex();
						if(callinListSelectedIdx == delete_call.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}

				}
				else if(delete_call.type == CALL_TYPE.ZHUBOACPT
						||delete_call.type == CALL_TYPE.ZHUBOHOLD
						||delete_call.type == CALL_TYPE.LIVEACPT
						||delete_call.type == CALL_TYPE.LIVEHOLD
				)
				{
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
							+ delete_call.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ delete_call.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					try {
						delete_call.type = CALL_TYPE.ENDCALL;
						broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
						updateShowIndex();
						if(callinListSelectedIdx == delete_call.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				else
				{
					updateCallRecordST(delete_call.uid,1,CALL_TYPE.ENDCALL);
					try {
						delete_call.type = CALL_TYPE.ENDCALL;
						broadcastCallType(delete_call,CALL_TYPE.ENDCALL);
						allCallinList.remove(delete_call.id);
						callinList.remove(delete_call.id);
						updateShowIndex();
						if(callinListSelectedIdx == delete_call.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				//处理挂断后挂断按钮状态不能正确刷新问题
				delete_call = null;
			}
		}

		/**SIP电话相关消息处理**/
		if (m.what == 0) {

			app.deinit();
			finish();
			Runtime.getRuntime().gc();
			android.os.Process.killProcess(android.os.Process.myPid());

		}
		else if (m.what == MSG_TYPE.CALL_STATE) {

			CallInfo ci = (CallInfo) m.obj;

			if (currentCall == null || ci == null || ci.getId() != currentCall.getId()) {
			System.out.println("Call state event received, but call info is invalid");
			return true;
			}

			/* Forward the call info to CallActivity */
			if (CallActivity.handler_ != null) {
				Message m2 = Message.obtain(CallActivity.handler_, MSG_TYPE.CALL_STATE, ci);
				m2.sendToTarget();
			}
			/* 把CallInfo同样传递给DailActivity */
			if(DailActivity.handler_ != null) {
				Message m2 = Message.obtain(DailActivity.handler_, MSG_TYPE.CALL_STATE, ci);
				m2.sendToTarget();
			}
			if (CallSkipActivity.handler_ != null) {
				Message m2 = Message.obtain(CallSkipActivity.handler_, MSG_TYPE.CALL_STATE, ci);
				m2.sendToTarget();
			}

			if (ci.getState() == pjsip_inv_state.PJSIP_INV_STATE_DISCONNECTED)
			{
			currentCall.delete();
			currentCall = null;
			}

		}
		else if (m.what == MSG_TYPE.CALL_MEDIA_STATE) {

			/* Forward the message to CallActivity */
			if (CallActivity.handler_ != null) {
				Message m2 = Message.obtain(CallActivity.handler_,
					MSG_TYPE.CALL_MEDIA_STATE,
					null);
				m2.sendToTarget();
			}
			if(DailActivity.handler_ != null) {
				Message m2 = Message.obtain(DailActivity.handler_,
						MSG_TYPE.CALL_MEDIA_STATE,
						null);
				m2.sendToTarget();
			}

		}
		else if (m.what == MSG_TYPE.BUDDY_STATE) {

			MyBuddy buddy = (MyBuddy) m.obj;
			int idx = account.buddyList.indexOf(buddy);

			/* Update buddy status text, if buddy is valid and
			* the buddy lists in account and UI are sync-ed.
			*/
			if (idx >= 0 && account.buddyList.size() == buddyList.size())
			{
			buddyList.get(idx).put("status", buddy.getStatusText());
			buddyListAdapter.notifyDataSetChanged();
			// TODO: selection color/mark is gone after this,
			//       dont know how to return it back.
			//buddyListView.setSelection(buddyListSelectedIdx);
			//buddyListView.performItemClick(buddyListView,
			//				     buddyListSelectedIdx,
			//				     buddyListView.
			//		    getItemIdAtPosition(buddyListSelectedIdx));

			/* Return back Call activity */
			notifyCallState(currentCall);
			}

		}
		else if (m.what == MSG_TYPE.REG_STATE) {

			String msg_str = (String) m.obj;
			lastRegStatus = msg_str;

		}
		else if (m.what == MSG_TYPE.INCOMING_CALL) {

			/* Incoming call */
			final MyCall call = (MyCall) m.obj;
			CallOpParam prm = new CallOpParam();

			/* Only one call at anytime */
			if (currentCall != null) {
			/*
			prm.setStatusCode(pjsip_status_code.PJSIP_SC_BUSY_HERE);
			try {
			call.hangup(prm);
			} catch (Exception e) {}
			*/
			// TODO: set status code
			call.delete();
			return true;
			}

			/* Answer with ringing */
			prm.setStatusCode(pjsip_status_code.PJSIP_SC_RINGING);
			try {
			call.answer(prm);
			} catch (Exception e) {}

			currentCall = call;
			//20210715改为显示测试页面
			//showCallActivity();
			Intent intent = new Intent(this, CallSkipActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//可以跳过中间栈直接实现两个界面间的切换
			startActivity(intent);



		}
		else if (m.what == MSG_TYPE.CHANGE_NETWORK) {
			app.handleNetworkChange();
		}
		else {
			/* Message not handled */
			return false;
		}
		return true;
    }

    private void dlgAccountSetting(){
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.dlg_account_config, null);

		if (lastRegStatus.length()!=0) {
			TextView tvInfo = (TextView)view.findViewById(R.id.textViewInfo);
			tvInfo.setText("Last status: " + lastRegStatus);
		}

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setView(view);
		adb.setTitle("Account Settings");

		final EditText etId    = (EditText)view.findViewById(R.id.editTextId);
		final EditText etReg   = (EditText)view.findViewById(R.id.editTextRegistrar);
		final EditText etProxy = (EditText)view.findViewById(R.id.editTextProxy);
		final EditText etUser  = (EditText)view.findViewById(R.id.editTextUsername);
		final EditText etPass  = (EditText)view.findViewById(R.id.editTextPassword);

		etId.   setText(accCfg.getIdUri());
		etReg.  setText(accCfg.getRegConfig().getRegistrarUri());
		StringVector proxies = accCfg.getSipConfig().getProxies();
		if (proxies.size() > 0)
			etProxy.setText(proxies.get(0));
		else
			etProxy.setText("");
		AuthCredInfoVector creds = accCfg.getSipConfig().getAuthCreds();
		if (creds.size() > 0) {
			etUser. setText(creds.get(0).getUsername());
			etPass. setText(creds.get(0).getData());
		} else {
			etUser. setText("");
			etPass. setText("");
		}

		adb.setCancelable(false);
		adb.setPositiveButton("OK",
			new DialogInterface.OnClickListener()
			{
			public void onClick(DialogInterface dialog,int id)
			{
				String acc_id 	 = etId.getText().toString();
				String registrar = etReg.getText().toString();
				String proxy 	 = etProxy.getText().toString();
				String username  = etUser.getText().toString();
				String password  = etPass.getText().toString();

				accCfg.setIdUri(acc_id);
				accCfg.getRegConfig().setRegistrarUri(registrar);
				AuthCredInfoVector creds = accCfg.getSipConfig().
									getAuthCreds();
				creds.clear();
				if (username.length() != 0) {
				creds.add(new AuthCredInfo("Digest", "*", username, 0,
							   password));
				}
				StringVector proxies = accCfg.getSipConfig().getProxies();
				proxies.clear();
				if (proxy.length() != 0) {
				proxies.add(proxy);
				}

				/* Enable ICE */
				accCfg.getNatConfig().setIceEnabled(true);

				/* Finally */
				lastRegStatus = "";
				try {
				account.modify(accCfg);
				} catch (Exception e) {}
			}
			}
		);
		adb.setNegativeButton("Cancel",
			new DialogInterface.OnClickListener()
			{
			public void onClick(DialogInterface dialog,int id)
			{
				dialog.cancel();
			}
			}
		);

		AlertDialog ad = adb.create();
		ad.show();
    }
    public void makeCall(View view){
		if (buddyListSelectedIdx == -1)
			return;

		/* Only one call at anytime */
		if (currentCall != null) {
			return;
		}

		HashMap<String, String> item = (HashMap<String, String>) buddyListView.
						   getItemAtPosition(buddyListSelectedIdx);
		String buddy_uri = item.get("uri");

		MyCall call = new MyCall(account, -1);
		CallOpParam prm = new CallOpParam(true);

		try {
			call.makeCall(buddy_uri, prm);
		} catch (Exception e) {
			call.delete();
			return;
		}

		currentCall = call;
		showCallActivity();
    }
    //供跨线程调用的addBuddy
	private void addBuddyByThread(String uri){
		final BuddyConfig cfg = new BuddyConfig();
		cfg.setUri(uri);
		cfg.setSubscribe(true);
		account.addBuddy(cfg);
	}
	private void removeBuddyByThread(){
		int length = account.buddyList.size();
		for(int i=length-1;i>=0;i--)
		{
			account.delBuddy(i);
		}
	}
    private void dlgAddEditBuddy(BuddyConfig initial){
		final BuddyConfig cfg = new BuddyConfig();
		final BuddyConfig old_cfg = initial;
		final boolean is_add = initial == null;

		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.dlg_add_buddy, null);

		AlertDialog.Builder adb = new AlertDialog.Builder(this);
		adb.setView(view);

		final EditText etUri  = (EditText)view.findViewById(R.id.editTextUri);
		final CheckBox cbSubs = (CheckBox)view.findViewById(R.id.checkBoxSubscribe);

		if (is_add) {
			adb.setTitle("Add Buddy");
		} else {
			adb.setTitle("Edit Buddy");
			etUri. setText(initial.getUri());
			cbSubs.setChecked(initial.getSubscribe());
		}

		adb.setCancelable(false);
		adb.setPositiveButton("OK",
			new DialogInterface.OnClickListener()
			{
			public void onClick(DialogInterface dialog,int id)
			{
				cfg.setUri(etUri.getText().toString());
				cfg.setSubscribe(cbSubs.isChecked());

				if (is_add) {
				account.addBuddy(cfg);
				buddyList.add(DataConvertUtil.putData(cfg.getUri(), ""));
				buddyListAdapter.notifyDataSetChanged();
				buddyListSelectedIdx = -1;
				} else {
				if (!old_cfg.getUri().equals(cfg.getUri())) {
					account.delBuddy(buddyListSelectedIdx);
					account.addBuddy(cfg);
					buddyList.remove(buddyListSelectedIdx);
					buddyList.add(DataConvertUtil.putData(cfg.getUri(), ""));
					buddyListAdapter.notifyDataSetChanged();
					buddyListSelectedIdx = -1;
				} else if (old_cfg.getSubscribe() !=
					   cfg.getSubscribe())
				{
					MyBuddy bud = account.buddyList.get(
								buddyListSelectedIdx);
					try {
					bud.subscribePresence(cfg.getSubscribe());
					} catch (Exception e) {}
				}
				}
			}
			}
		);
		adb.setNegativeButton("Cancel",
			new DialogInterface.OnClickListener()
			{
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
			}
		);

		AlertDialog ad = adb.create();
		ad.show();
    }
    public void addBuddy(View view) {
		dlgAddEditBuddy(null);
    }
    public void editBuddy(View view){
	if (buddyListSelectedIdx == -1)
	    return;

	BuddyConfig old_cfg = account.buddyList.get(buddyListSelectedIdx).cfg;
	dlgAddEditBuddy(old_cfg);
    }
    public void delBuddy(View view) {
	if (buddyListSelectedIdx == -1)
	    return;

	final HashMap<String, String> item = (HashMap<String, String>)
			buddyListView.getItemAtPosition(buddyListSelectedIdx);
	String buddy_uri = item.get("uri");

	DialogInterface.OnClickListener ocl =
					new DialogInterface.OnClickListener()
	{
	    @Override
	    public void onClick(DialogInterface dialog, int which)
	    {
		switch (which) {
		    case DialogInterface.BUTTON_POSITIVE:
			account.delBuddy(buddyListSelectedIdx);
			buddyList.remove(item);
			buddyListAdapter.notifyDataSetChanged();
			buddyListSelectedIdx = -1;
			break;
		    case DialogInterface.BUTTON_NEGATIVE:
			break;
		}
	    }
	};

	AlertDialog.Builder adb = new AlertDialog.Builder(this);
	adb.setTitle(buddy_uri);
	adb.setMessage("\nDelete this buddy?\n");
	adb.setPositiveButton("Yes", ocl);
	adb.setNegativeButton("No", ocl);
	adb.show();
    }

    /*
    * === MyAppObserver ===
    * 
    * As we cannot do UI from worker thread, the callbacks mostly just send
    * a message to UI/main thread.
    */
    public void notifyIncomingCall(MyCall call){
		Message m = Message.obtain(handler, MSG_TYPE.INCOMING_CALL, call);
		m.sendToTarget();
    }
	public void notifyInstantMessage(OnInstantMessageParam prm){
		GlobalNotify.showSysnotify(getApplicationContext());
	}
    public void notifyRegState(int code, String reason,long expiration) {
		String msg_str = "";
		if (expiration == 0)
			msg_str += "Unregistration";
		else
			msg_str += "Registration";

		if (code/100 == 2)
			msg_str += " successful";
		else
			msg_str += " failed: " + reason;

		Message m = Message.obtain(handler, MSG_TYPE.REG_STATE, msg_str);
		m.sendToTarget();
    }
    public void notifyCallState(MyCall call){
		if (currentCall == null || call.getId() != currentCall.getId())
			return;

		CallInfo ci = null;
		try {
			ci = call.getInfo();
		} catch (Exception e) {}

		if (ci == null)
			return;

		Message m = Message.obtain(handler, MSG_TYPE.CALL_STATE, ci);
		m.sendToTarget();
    }
    public void notifyCallMediaState(MyCall call){
		Message m = Message.obtain(handler, MSG_TYPE.CALL_MEDIA_STATE, null);
		m.sendToTarget();
    }
    public void notifyBuddyState(MyBuddy buddy){
		Message m = Message.obtain(handler, MSG_TYPE.BUDDY_STATE, buddy);
		m.sendToTarget();
    }
    public void notifyChangeNetwork(){
		Message m = Message.obtain(handler, MSG_TYPE.CHANGE_NETWORK, null);
		m.sendToTarget();
    }

    /* === end of MyAppObserver ==== */

	/*
		//state 类型
		//  未接        //0   默认状态
		//  已接        //1   导播或者主播接听电话会置这个状态
		//  挂断        //2   导播或者主播将电话挂断会置这个状态
		//  语音留言     //3
		// 黑名单被拒绝   //4
		//  拨出        //10  初始状态也是结束状态
		//在这个位置还不知道导播和主播是谁，到时候多导播和多主播还要再确定是谁接的可以在安卓端发请求修改记录

		//		//使用type字段进行更新
		//		0           新来电
		//		1           导播接听
		//		2           导播接听保持
		//		3           待主播接听
		//		4           主持人接听
		//		5           主持人接听保持
		//		6           正在直播
		//		7           电话直播保持
		//		8           待导播接听
		//		9           电话结束
	*/
	public void updateCallRecordST(String uid,Integer state,Integer type){
		new Thread() {
			@RequiresApi(api = Build.VERSION_CODES.N)
			@Override
			public void run() {
				HttpRequest request = new HttpRequest();
				String rs = request.postJson(
						"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordState",
						"{\"type\":\"post pstn info\", " +
								"\"callid\":\"" + uid + "\", " +
								"\"state\":\"" + state + "\", " +
								"\"type\":\"" + type + "\"}"
				);
				Message m2 = Message.obtain(MainActivity.handler_,1001,null);
				m2.sendToTarget();

			}
		}.start();

	}
	public void setCallRecordInfo(MyCallWithState callws){
		try {
		if(callws!=null) {

				if (callws.type == CALL_TYPE.DIALINIT)
					return;
				new Thread() {
					@RequiresApi(api = Build.VERSION_CODES.N)
					@Override
					public void run() {
						Log.e("20211203", "" + callws.type);
						HttpRequest request = new HttpRequest();
						String rs = request.postJson(
								"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfo",
								"{\"type\":\"post pstn info\", " +
										//callws.type == CALL_TYPE.
										"\"pstn\":\"" + callws.pstn + "\", " +
										"\"callid\":\"" + callws.uid + "\", " +
										"\"callednumber\":\"" + callws.callednumber + "\", " +
										"\"name\":\"" + ((callws.name == null || callws.name.equals("")) ? "" : callws.name) + "\", " +
										"\"male\":\"" + (callws.male == null ? 1 : callws.male) + "\", " +
										"\"age\":\"" + (callws.age == null ? 0 : callws.age) + "\", " +
										"\"frm\":\"" + callws.from + "\", " +
										"\"iswhitelist\":\"" + (callws.iswhitelist == null ? 0 : callws.iswhitelist) + "\", " +
										"\"isblacklist\":\"" + (callws.isblacklist == null ? 0 : callws.isblacklist) + "\", " +
										"\"iscontact\":\"" + (callws.iscontact == null ? 0 : callws.iscontact) + "\", " +
										//2021-03-22待服务器更新后添加以下字段
										"\"isexpert\":\"" + (callws.isexpert == null ? 0 : callws.isexpert) + "\", " +
										"\"level\":\"" + callws.level + "\", " +
										"\"province\":\"" + callws.province + "\", " +
										"\"city\":\"" + callws.city + "\", " +
										"\"address\":\"" + callws.address + "\", " +
										"\"director\":\"" + GlobalVariable.userid + "\", " +
										"\"broadcaster\":\"" + GlobalVariable.mbcid + "\", " +
										"\"content\":\"" + callws.content + "\", " +
										"\"pstnmark\":\"" + callws.pstnmark + "\", " +
										"\"pdrmark\":\"" + callws.pdrmark + "\", " +
										"\"mbcmark\":\"" + callws.mbcmark + "\"," +
										//2021-06-16
										"\"remark\":\"" + callws.remark + "\"" +
										"}"

						);
						//只有导播修改后需要实时更新，主播不会修改用户数据
						if (currentCallWithState != null) {
							try {
								updateListUserInfo(currentCallWithState);
								String msg_to_send =
										"{ \"msgType\": \"CHANGE CALL INFO\", \"uid\": \""
												+ currentCallWithState.uid
												+ "\" }";
								if (wsControl != null) wsControl.sendMessage(msg_to_send);
							} catch (Exception e) {
							}

						}
						//告诉主播更新界面
						Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
						m2.sendToTarget();

					}
				}.start();
			}
		}catch(Exception e){
			e.printStackTrace();
			return;
		}
	}
	public void setCallRecordInfoMbc(MyCallWithState callws){
		if(callws!=null){
			new Thread() {
				@Override
				public void run() {
					HttpRequest request = new HttpRequest();
					String rs = request.postJson(
							"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoMbc",
							"{\"type\":\"post pstn info\", " +
									"\"pstn\":\"" + callws.pstn + "\", " +
									"\"callid\":\"" + callws.uid + "\", " +
									"\"mbcmark\":\"" + callws.mbcmark + "\"}"
					);
					Message m2 = Message.obtain(MainActivity.handler_,1001,null);
					m2.sendToTarget();
				}
			}.start();
		}
	}
	public void broadcastCallType(MyCallWithState callWithState,int type){
				String msg_to_send =
				"{ \"msgType\": \"CHANGE CALL TYPE\", \"uid\": \""
						+ callWithState.uid
						+ "\", \"type\": \""
						+ type
						+ "\", \"DaoBoCallUri\": \""
						+ callWithState.daobocalluri
						+ "\", \"ZhuBoCallUri\": \""
						+ MainActivity.zhuboUri
						+ "\" }";
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);

				new Thread() {
					@Override
					public void run() {
						try {
							String call_status = "";
							if(type == CALL_TYPE.NEWINIT){
								call_status = "CALLING";
							}
							else if(type == CALL_TYPE.DAOBOACPT){
								call_status = "DAOBO_ANSWER";
								if(GlobalVariable.charactername.equals("broadcaster") && callWithState.dailout)
								{
									call_status = "ZHUBO_ANSWER";
								}
							}
							else if(type == CALL_TYPE.DAOBOHOLD){
								call_status = "DAOBO_HOLD";
							}
							else if(type == CALL_TYPE.WAITZHUBO){
								call_status = "DAOBO_TO_ZHUBO";
							}
							else if(type == CALL_TYPE.ZHUBOACPT){
								call_status = "ZHUBO_ANSWER";
							}
							else if(type == CALL_TYPE.ZHUBOHOLD){
								call_status = "ZHUBO_HOLD";
							}
							else if(type == CALL_TYPE.WAITDAOBO){
								call_status = "ZHUBO_TO_DAOBO";
							}
							else if(type == CALL_TYPE.LIVEACPT){
								call_status = "LIVING";
							}
							else if(type == CALL_TYPE.LIVEHOLD){
								call_status = "LIVING_HOLD";
							}
							else if(type == CALL_TYPE.ENDCALL){
								call_status = "ENDCALL";
							}

							HttpRequest request = new HttpRequest();
							String rs = request.postJson(
									GlobalVariable.SEHTTPServer + "/calltypechange",
									"{" +
											"\"recordpath\":\"http://"+GlobalVariable.SIPSERVER+":8080/record/"+callWithState.uid+".wav"+ "\"," +
											"\"outcall\":\"" + (callWithState.dailout?callWithState.pstn.substring(2):callWithState.pstn) + "\"," +
											"\"contact\":\"" + callWithState.callednumber + "\"," +
											"\"status\":\"" + call_status + "\"," +
											"\"callintime\":\"" + callWithState.callintime + "\"," +
											"\"code\":" + 200 + "}"
							);
						} catch (Exception e) {
							//e.printStackTrace();
							Log.e("SEHTTPServer","指令异常");
						}
					}
				}.start();

	}
	public void setCallRecordState(String uid,Integer state){

		if(currentCallWithState!=null){
			new Thread() {
				@Override
				public void run() {
					HttpRequest request = new HttpRequest();
					String rs = request.postJson(
							"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordState",
							"{\"type\":\"post pstn info\", " +
									"\"callid\":\"" + uid + "\", " +
									"\"state\":\"" + state + "\"}"
					);
				}
			}.start();
		}
	}
	public void getCallRecordInfo(MyCallWithState callWithState,String finalPstn,String uid){
		//注意先留存上一次的dailout状态，如果状态发生更改，需要重新查询结果
		String comein = "default";
		if(callWithState.dailout)
			comein = "dailout";

		if(callWithState.dailout){
			//finalPstn = finalPstn.replaceFirst("00","");
            finalPstn = finalPstn.substring(2,finalPstn.length());
		}

		HttpRequest request = new HttpRequest();
		String rs = request.postJson(
				"Http://"+GlobalVariable.SIPSERVER+":8080/smart/contactWithSipServer/getPstnByPstn",
				"{\"type\":\"post pstn info\", " +
						"\"pstn\":\""+ finalPstn +"\", " +
						"\"callid\":\""+ uid +"\"}"
		);

		try {
			JSONObject rs_json = new JSONObject(rs);
			JSONArray rs_jsonarray = rs_json.getJSONObject("data").getJSONArray("pstns");
			if(rs_jsonarray.length()>0)
			{
				JSONObject rs_json_pstn = rs_jsonarray.getJSONObject(0);
				//callWithState.pstn = rs_json_pstn.get("pstn")==null?"":rs_json_pstn.get("pstn").toString();
				callWithState.name = rs_json_pstn.get("name")==null?"":rs_json_pstn.get("name").toString();
				if(callWithState.name.length()>6){
					int length = callWithState.name.length();
					callWithState.name = callWithState.name.substring(length-4,length);
				}


				//callWithState.age =  rs_json_pstn.get("age")==null? 0:(Integer)rs_json_pstn.get("age");
				//callWithState.male = rs_json_pstn.get("male")==null? 0:(Integer) rs_json_pstn.get("male");
				//callWithState.from = rs_json_pstn.get("frm")==null?"":rs_json_pstn.get("frm").toString();
				//"isblacklist": 0,
				//"iscontact": 1,
				//"isexpert": 0,
				callWithState.iswhitelist = String.valueOf(rs_json_pstn.get("iswhitelist")).equals("1")?1:0;
				callWithState.isblacklist = String.valueOf(rs_json_pstn.get("isblacklist")).equals("1")?1:0;
				callWithState.iscontact = String.valueOf(rs_json_pstn.get("iscontact")).equals("1")?1:0;
				callWithState.isexpert = String.valueOf(rs_json_pstn.get("isexpert")).equals("1")?1:0;
			}


			JSONObject rs_json_callrecord = rs_json.getJSONObject("data").getJSONObject("callrecord");
			if(rs_json_callrecord!=null)
			{
				callWithState.content = rs_json_callrecord.get("content")==null?"":rs_json_callrecord.get("content").toString();
				callWithState.pstnmark = rs_json_callrecord.get("pstnmark")==null?"":rs_json_callrecord.get("pstnmark").toString();
				callWithState.pdrmark = rs_json_callrecord.get("pdrmark")==null?"":rs_json_callrecord.get("pdrmark").toString();
				callWithState.mbcmark = rs_json_callrecord.get("mbcmark")==null?"":rs_json_callrecord.get("mbcmark").toString();
				callWithState.dailout = String.valueOf(rs_json_callrecord.get("operatetype")).equals("1")?true:false;

				if(comein.equals("defult")&&callWithState.dailout){
					//进来的时候是非拨出，但是查询到数据库是拨出，需要重新查询电话信息

					//finalPstn = finalPstn.replaceFirst("00","");
                    finalPstn = finalPstn.substring(2,finalPstn.length());
					HttpRequest request1 = new HttpRequest();
					String rs1 = request1.postJson(
							"Http://"+GlobalVariable.SIPSERVER+":8080/smart/contactWithSipServer/getPstnByPstn",
							"{\"type\":\"post pstn info\", " +
									"\"pstn\":\""+ finalPstn +"\", " +
									"\"callid\":\""+ uid +"\"}"
					);

					JSONObject rs_json1 = new JSONObject(rs1);
					JSONArray rs_jsonarray1 = rs_json1.getJSONObject("data").getJSONArray("pstns");
					if(rs_jsonarray1.length()>0)
					{
						JSONObject rs_json_pstn = rs_jsonarray1.getJSONObject(0);
						//callWithState.pstn = rs_json_pstn.get("pstn")==null?"":rs_json_pstn.get("pstn").toString();
						callWithState.name = rs_json_pstn.get("name")==null?"":rs_json_pstn.get("name").toString();
						//callWithState.age =  rs_json_pstn.get("age")==null? 0:(Integer)rs_json_pstn.get("age");
						//callWithState.male = rs_json_pstn.get("male")==null? 0:(Integer) rs_json_pstn.get("male");
						//callWithState.from = rs_json_pstn.get("frm")==null?"":rs_json_pstn.get("frm").toString();
						//"isblacklist": 0,
						//"iscontact": 1,
						//"isexpert": 0,
						callWithState.iswhitelist = String.valueOf(rs_json_pstn.get("iswhitelist")).equals("1")?1:0;
						callWithState.isblacklist = String.valueOf(rs_json_pstn.get("isblacklist")).equals("1")?1:0;
						callWithState.iscontact = String.valueOf(rs_json_pstn.get("iscontact")).equals("1")?1:0;
						callWithState.isexpert = String.valueOf(rs_json_pstn.get("isexpert")).equals("1")?1:0;
					}

				}
				else if(comein.equals("dailout")&&!callWithState.dailout){
					//数据库查询是非拨出，但进来的时候是拨出，不可能
				}
				else if(comein.equals("dailout")&&callWithState.dailout){
					//状态相同不处理
				}
				else if(comein.equals("defult")&&!callWithState.dailout){
					//不处理
				}
			}
		} catch (Exception e) {
            LogClient.generate("【CallRecord获取转换错误】"+e.getMessage());
			e.printStackTrace();
		}
		//这边暂时测试用，待定

//		String tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
//
//		//右上角时间代表的呼入时间，左下角代表当前状态时间，默认值用00:00:00表示，在后面状态更新了再更新
//		callinList.add(DataConvertUtil.putData(callWithState.name == null?tailnumber:callWithState.name,standardDateFormat.format(new Date()),"00:00:00","0"));
//		Message m2 = Message.obtain(MainActivity.handler_,1001,null);
//		m2.sendToTarget();
	}
	public void showDailPanel(View view){
		DailPanelDialog dailPanelDialog = new DailPanelDialog.Builder(MainActivity.this,currentCallWithState).create();
		dailPanelDialog.show();
		dailPanelDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
		dailPanelDialog.getWindow().setLayout(655, 525);
		if(GlobalVariable.charactername.equals("director"))
			dailPanelDialog.getWindow().setGravity(Gravity.RIGHT|Gravity.BOTTOM);
		else
			dailPanelDialog.getWindow().setGravity(Gravity.RIGHT);
	}
	public void changeCallInfo(View view){
		if (GlobalVariable.charactername.equals("director")) {
			setCallRecordInfo(currentCallWithState);
		} else {
			setCallRecordInfoMbc(currentCallWithState);
		}
		//Toast.makeText(MainActivity.this, "信息保存成功", Toast.LENGTH_SHORT).show();
		MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"同步成功").create();
		msgNotifyDialog.show();
		if(GlobalVariable.viewmodel == 1)
			msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
		else if(GlobalVariable.viewmodel == 2)
			msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
		else if(GlobalVariable.viewmodel == 3)
			msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
		msgNotifyDialog.getWindow().setLayout(500, 300);
		//还需要
		//msgNotifyDialog.dismiss();

		final Timer t = new Timer();
		t.schedule(new TimerTask() {
			public void run() {
				msgNotifyDialog.dismiss();
				t.cancel();
			}
		}, 2000);
	}
	/**显示历史记录列表**/
	public void showHisDlg(View view){
		if(currentCallWithState!=null) {
			hisList = new ArrayList<>();
			new Thread() {
				@Override
				public void run() {
					try {
						//处理姓名键值对
						HttpRequest requestKV = new HttpRequest();
						String rsKV = requestKV.postJson(
								"http://"+ GlobalVariable.SIPSERVER +":8080/smart/contactWithSipServer/getPstnList",
								"{\"userid\":\""+GlobalVariable.userid+"\", " +
										"\"character\": \""+GlobalVariable.charactername+"\"," +
										"\"isexpert\": \"-1\"," +
										"\"iscontact\": \"0\"" +
										"}"
						);
						JSONObject rs_jsonKV = new JSONObject(rsKV);
						JSONArray rs_json_contactslist = rs_jsonKV.getJSONArray("data");
						List<Pstn> pstnlist = JSON.parseArray(rs_json_contactslist.toString(), Pstn.class);

						for(Pstn pstn :pstnlist){
							if(pstn.name!=null&&pstn.name!="")
							{
								GlobalVariable.pstn_name_KV.put(pstn.pstn,pstn.name);
							}
						}

						HttpRequest request = new HttpRequest();
						String rs = request.postJson(
								"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getHistoryByPstn",
								"{\"pstn\":\"" + currentCallWithState.pstn + "\", " +
										"\"state\": \"-1\"}"
						);
						JSONObject rs_json = new JSONObject(rs);
						JSONArray rs_json_callrecordlist = rs_json.getJSONArray("data");
						hisDetailList = JSON.parseArray(rs_json_callrecordlist.toString(), CallRecord.class);
						for (CallRecord record : hisDetailList) {
							//"state", "callingnumber","starttime","type"
							String duration = GlobalVariable.getRingDuring(record.recordpath);
							if(duration == null)
							{
								duration = "00:00";
							}
							else{
								duration = GlobalVariable.ms_df.format(new Date(Long.parseLong(duration)));
							}
							hisList.add(DataConvertUtil.putDataHISDlg(record.starttime,duration,record.content));
						}
						//获得解析数据后更新页面
						Message m2 = Message.obtain(MainActivity.handler_, 1006, null);
						m2.sendToTarget();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}.start();
		}
		else{ alertCallNotSelected(); }
	}
	public void showHistoryDetail(View view){
		CallRecord record = null;
		Integer position = (Integer) view.getTag();
		if(hisDetailList!=null)
		{
			try {
				record = hisDetailList.get(position);
			}catch (Exception e){

			}
		}
		if(record!=null)
		{
			HistoryDialog historyDialog = new HistoryDialog.Builder(this,record).create();
			historyDialog.show();
			if(GlobalVariable.viewmodel == 1)
				historyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
			else if(GlobalVariable.viewmodel == 2)
				historyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
			else if(GlobalVariable.viewmodel == 3)
				historyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
			historyDialog.getWindow().setLayout(1000, 600);
		}

	}
	public void showPstnDetail(View view){
		if(currentCallWithState!=null){
			PstnDetailDialog pstnDetailDialog = new PstnDetailDialog.Builder(MainActivity.this,currentCallWithState).create();
			pstnDetailDialog.show();
			pstnDetailDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
			pstnDetailDialog.getWindow().setLayout(700, 500);
		}
		else{ alertCallNotSelected(); }
	}
	/**处理电话挂断后界面信息清除问题**/
	public void resetMainView(){
		if(GlobalVariable.viewmodel == 1) {
			if (currentCallNum == 0) {
				TextView textPstn = findViewById(R.id.textPstn);
				EditText textName = findViewById(R.id.editTextName);
				EditText textViewDescribe = findViewById(R.id.textViewDescribe);
				EditText textViewInfo = findViewById(R.id.textViewInfo);
				EditText textViewPdrRemark = findViewById(R.id.textViewPdrRemark);
				EditText textViewMbcRemark = findViewById(R.id.textViewMbcRemark);
				//ImageButton buttonCN1 = findViewById(R.id.buttonCN1);

				textPstn.setText("");
				textName.setText("");
				textViewDescribe.setText("");
				textViewInfo.setText("");
				textViewPdrRemark.setText("");
				textViewMbcRemark.setText("");
				currentCallWithState = null;
			}
		}
		else if(GlobalVariable.viewmodel == 2){
			if (currentCallNum == 0) {
				currentCallWithState = null;
			}
		}
		else if(GlobalVariable.viewmodel == 3){
			if (currentCallNum == 0) {
				TextView textPstn = findViewById(R.id.textPstn);
				EditText textName = findViewById(R.id.editTextName);
				EditText textViewInfo = findViewById(R.id.textViewInfo);

				textPstn.setText("");
				textName.setText("");
				textViewInfo.setText("");
				currentCallWithState = null;
			}
		}
	}
	/**自动保持（将localacceptcall更改为保持状态）**/
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void autoHoldCallPdr(){

		Log.e("localacceptcall",localacceptcall==null?"null":localacceptcall.type+"");
		if(localacceptcall!=null&&!localacceptcall.syncshow) {
			//因为影响远程控制去掉一个判断前提 &&!localacceptcall.equals(currentCallWithState)
			if (localacceptcall.type == CALL_TYPE.DAOBOACPT) {
				//导播保持
				localacceptcall.wait_ackhold = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send =
						"{ \"msgType\": \"DAOBO HOLD\", \"OutCallUri\": \""
								+ localacceptcall.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ localUri//本机是导播直接使用localUri
								+ "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);

			}
		}
	}
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void autoHoldCallAll(){
		if(localacceptcall!=null&&!localacceptcall.syncshow) {
			if (localacceptcall.type == CALL_TYPE.DAOBOACPT) {
				if(localacceptcall.dailout && GlobalVariable.charactername.equals("broadcaster")){
					//主播保持
					localacceptcall.wait_ackhold = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
									+ localacceptcall.outcalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				else{
					//导播保持
					localacceptcall.wait_ackhold = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send =
							"{ \"msgType\": \"DAOBO HOLD\", \"OutCallUri\": \""
									+ localacceptcall.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ localUri//本机是导播直接使用localUri
									+ "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}

			}
			else if (localacceptcall.type == CALL_TYPE.ZHUBOACPT) {
					//主播接听在单导播和多导播情况下接听状态自动保持在主播端是不存在的
					//接听状态仅用于转回导播的操作
					if(GlobalVariable.sipmodel ==0 || GlobalVariable.sipmodel ==2){
						//主播保持
						localacceptcall.wait_ackhold = true;
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						String msg_to_send =
								"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
										+ localacceptcall.outcalluri
										+ "\", \"ZhuBoCallUri\": \""
										+ MainActivity.zhuboUri
										+ "\" }";
						if (wsControl != null) wsControl.sendMessage(msg_to_send);
					}
			}
		}
	}
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void autoHoldCallPdrByInner(){

		if(localacceptcall!=null&&!localacceptcall.syncshow) {
			if (localacceptcall.type == CALL_TYPE.DAOBOACPT) {
				//导播保持
				localacceptcall.wait_ackhold = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send =
						"{ \"msgType\": \"DAOBO HOLD\", \"OutCallUri\": \""
								+ localacceptcall.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ localUri//本机是导播直接使用localUri
								+ "\" }";
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);

			}
		}
	}
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void autoHoldCallMbc(){
		if(localacceptcall!=null&&!localacceptcall.syncshow&&!localacceptcall.equals(currentCallWithState)) {
			if (localacceptcall.type == CALL_TYPE.ZHUBOACPT) {
				//主播保持
				localacceptcall.wait_ackhold = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send =
						"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
								+ localacceptcall.outcalluri
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			}
		}
	}

	/**接听电话等一系列按钮的绑定事件**/
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void acceptCall(View view) {
		if(GlobalVariable.operatelock) return;//额外判断允许在锁定按钮情况下也执行自动转接动作
		Log.e("进来接听电话","1111");
		GlobalVariable.operatelock = true;
		updateInfoAndButton();

		if(wsControl==null)
		{
			Toast.makeText(MainActivity.this, "请等待服务初始化成功后尝试", Toast.LENGTH_SHORT).show();
			GlobalVariable.operatelock = false;
			updateInfoAndButton();

			return;
		}
		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
			//处理耦合器按钮显示
			for(MyCallWithState callws : allCallinList){
				callws.selected_line_btn = false;
			}
			currentCallWithState.selected_line_btn = true;
		}
		LED_Media_Pause();
		GlobalVariable.PhoneRedTitle = false;
		if(currentCallWithState!=null) {
			if(currentCallWithState.type == CALL_TYPE.LIVEHOLD)
				//20230506 处理仅导播模式希望每次进直播前都先经过预备播出的过程
				if(GlobalVariable.sipmodel != 3) {
					currentCallWithState.first_livehold = false;
				}
//			if(currentCallWithState.mycall == null)
//			{
//				try {
//					MyCall mycall  = new MyCall(account, -1);
//					CallOpParam prm1 = new CallOpParam(true);
//					currentCallWithState.mycall.makeCall(sipUriParam, prm1);
//				} catch (Exception e) {
//					e.printStackTrace();
//					currentCallWithState.mycall.delete();
//					return;
//				}
//			}

			if (GlobalVariable.charactername.equals("director")) {
				setCallRecordInfo(currentCallWithState);
			} else {
				setCallRecordInfoMbc(currentCallWithState);
			}
			if (currentCallWithState.type == CALL_TYPE.NEWINIT) {
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
				//外线接进来的
				GlobalVariable.currentaccept_uid = currentCallWithState.uid;
				autoHoldCallPdr();
				currentCallWithState.wait_ackaccept = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}

				String msg_to_send = "{ \"msgType\": \"DAOBO MAKE CALL\", " +
						(GlobalVariable.autoswitch?"\"autoswitch\": \"1\",":" ")+
						"\"callUri_id\": \""+0+"\", " +
						"\"OutCallUri\": \""+currentCallWithState.outcalluri+"\", " +
						"\"DaoBoCallUri\": \""+localUri+"\", " +
						"\"Record\":"+currentCallWithState.record+" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
//				CallOpParam prm1 = new CallOpParam(true);
//				try {
//					currentCallWithState.mycall.makeCall(sipUriParam, prm1);
//					currentCallWithState.accepttime = standardDateFormat.format(new Date());
//					currentCallWithState.showitem.put("accepttime", currentCallWithState.accepttime);
//
//				} catch (Exception e) {
//					currentCallWithState.mycall.delete();
//				}
			}
			else if (currentCallWithState.type == CALL_TYPE.WAITZHUBO) {

				if(GlobalVariable.charactername.equals("director"))
				{
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
					//当前是导播客户端，需要取消转接操作
					String msg_to_send = "{ \"msgType\": \"DAOBO CANCEL SWITCH\", \"DaoBoCallUri\": \""
							+ localUri//这里是导播发消息
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri + "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else {
					updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.ZHUBOACPT);
					//外线或者导播接进来的
					GlobalVariable.currentaccept_uid = currentCallWithState.uid;
					autoHoldCallAll();
					currentCallWithState.wait_ackaccept = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}

					String msg_to_send = "{ \"msgType\": \"ZHUBO MAKE CALL\", \"callUri_id\": \"" + 0 + "\", \"OutCallUri\": \"" + currentCallWithState.outcalluri + "\", \"ZhuBoCallUri\": \"" + localUri + "\", \"Record\":" + currentCallWithState.record + " }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
//				CallOpParam prm1 = new CallOpParam(true);
//				try {
//					currentCallWithState.mycall.makeCall(sipUriParam, prm1);
//					currentCallWithState.accepttime = standardDateFormat.format(new Date());
//					currentCallWithState.showitem.put("accepttime", currentCallWithState.accepttime);
//				} catch (Exception e) {
//					e.printStackTrace();
//					currentCallWithState.mycall.delete();
//					return;
//				}
			}
			else if (currentCallWithState.type == CALL_TYPE.WAITDAOBO) {
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
				//这是从主播返回的
				//导播通知SIP服务器，应答主播返回电话
				GlobalVariable.currentaccept_uid = currentCallWithState.uid;

				autoHoldCallPdr();
				currentCallWithState.wait_ackaccept = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send = "{ \"msgType\": \"ACK ZHUBO TO DAOBO\", \"OutCallUri\": \""
						+ currentCallWithState.outcalluri
						+ "\", \"DaoBoCallUri\": \""
						+ localUri//这里是导播发消息
						+ "\", \"ZhuBoCallUri\": \""
						+ MainActivity.zhuboUri
						+ "\" }";
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);

				//状态转为接听状态
				currentCallWithState.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
				currentCallWithState.type = CALL_TYPE.DAOBOACPT;

				localacceptcall = currentCallWithState;
				//broadcastCallType(currentCallWithState, CALL_TYPE.DAOBOACPT);
			}
			else if (currentCallWithState.type == CALL_TYPE.DAOBOACPT) {
				if(GlobalVariable.charactername.equals("director")){
					//导播保持
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOHOLD);
					if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
					{
						GlobalVariable.currentaccept_uid = "";
					}
					currentCallWithState.wait_ackhold = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send =
							"{ \"msgType\": \"DAOBO HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ localUri//本机是导播直接使用localUri
									+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else{
					//主播保持
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ZHUBOHOLD);
					if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
					{
						GlobalVariable.currentaccept_uid = "";
					}
					currentCallWithState.wait_ackhold = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}


			}
			else if (currentCallWithState.type == CALL_TYPE.ZHUBOACPT) {
				//主播保持
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ZHUBOHOLD);
				if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
				{
					GlobalVariable.currentaccept_uid = "";
				}
				currentCallWithState.wait_ackhold = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send =
						"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
								+ currentCallWithState.outcalluri
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			}
			else if (currentCallWithState.type == CALL_TYPE.DAOBOHOLD) {
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
				//导播取消保持
				GlobalVariable.currentaccept_uid = currentCallWithState.uid;
				autoHoldCallPdr();
				currentCallWithState.wait_ackhold = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send =
						"{ \"msgType\": \"DAOBO UNHOLD\", \"OutCallUri\": \""
								+ currentCallWithState.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ localUri
								+ "\" }";
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);

			}
			else if (currentCallWithState.type == CALL_TYPE.ZHUBOHOLD) {
				if(currentCallWithState.dailout){
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
				}
				else {
					updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.ZHUBOACPT);
				}
				//主播取消保持
				GlobalVariable.currentaccept_uid = currentCallWithState.uid;
				autoHoldCallAll();
				currentCallWithState.wait_ackhold = true;
				if(GlobalVariable.viewmodel==1) {
					updateOperateButton();
				}
				else if(GlobalVariable.viewmodel==3){
					updateGsOperateButton();
				}
				String msg_to_send =
						"{ \"msgType\": \"ZHUBO UNHOLD\", \"OutCallUri\": \""
								+ currentCallWithState.outcalluri
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);


			}
			else if (currentCallWithState.type == CALL_TYPE.LIVEACPT || currentCallWithState.type == CALL_TYPE.LIVEHOLD){
				if(GlobalVariable.charactername.equals("director")){
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
					//外线接进来的
					GlobalVariable.currentaccept_uid = currentCallWithState.uid;
					autoHoldCallPdr();
					currentCallWithState.wait_ackaccept = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}

					String msg_to_send = "{ \"msgType\": \"DAOBO MAKE CALL\", \"callUri_id\": \""+0+"\", \"OutCallUri\": \""+currentCallWithState.outcalluri+"\", \"DaoBoCallUri\": \""+localUri+"\", \"Record\":"+currentCallWithState.record+" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else{
					updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.ZHUBOACPT);
					//外线或者导播接进来的
					GlobalVariable.currentaccept_uid = currentCallWithState.uid;
					autoHoldCallAll();
					currentCallWithState.wait_ackaccept = true;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}

					String msg_to_send = "{ \"msgType\": \"ZHUBO MAKE CALL\", \"callUri_id\": \"" + 0 + "\", \"OutCallUri\": \"" + currentCallWithState.outcalluri + "\", \"ZhuBoCallUri\": \"" + localUri + "\", \"Record\":" + currentCallWithState.record + " }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
			}
			else {

			}
		}
		else{ alertCallNotSelected(); }
		//在每次点击按钮后刷新List

		GlobalVariable.operatelock = false;
		updateInfoAndButton();
		myAdapter.setDataList(allCallinList);
		mbcAdapter.setDataList(allCallinList);

	}
	public void muteCall(View view) {
		//暂不处理，存在多路通话静音一路是否影响其他路的问题
		if(currentCallWithState!=null) {
			if(currentCallWithState.virtual && localUri.equals(zhuboUri)){
				String msg_to_send =
						"{ \"msgType\": \"ZHUBO TO VIRTUAL MUTE CALL\", \"OutCallUri\": \""
								+ currentCallWithState.outcalluri
								+ "\", \"DaoBoCallUri\": \""
								+ currentCallWithState.daobocalluri
								+ "\", \"ZhuBoCallUri\": \""
								+ MainActivity.zhuboUri
								+ "\", \"VirtualUri\": \""
								+ null
								+ "\" }";
				//2021-03-23 屏蔽虚拟主播
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			}
		}
		else{ alertCallNotSelected(); }
		//在每次点击按钮后刷新List
		myAdapter.setDataList(allCallinList);
		mbcAdapter.setDataList(allCallinList);
		//callinListmbcAdapter.setDataList(allCallinList);
	}
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void holdCall(View view) {
		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
			//处理耦合器按钮显示
			for(MyCallWithState callws : allCallinList){
				callws.selected_line_btn = false;
			}
			currentCallWithState.selected_line_btn = true;
		}
		//此方法是否可行待定(更改为发消息由服务器处理)
		if(currentCallWithState!=null) {
			//如果当前电话状态不是正在接听状态或者主播转回接听状态是不能进行hold的
			if(
					(
						currentCallWithState.state == CALL_STATE_TYPE.ZHUBO_ACCEPT
						||currentCallWithState.state == CALL_STATE_TYPE.DAOBO_ACCEPT
//						||currentCallWithState.state == CALL_STATE_TYPE.DAOBO_REVERT
//						||currentCallWithState.state == CALL_STATE_TYPE.ZHUBO_REVERT
					)
					&&
					(!currentCallWithState.hold)
			)
			{
				currentCallWithState.wait_ackhold = true;
				if(currentCallWithState.virtual && localUri.equals(zhuboUri))//本机操作虚拟导播
				{
					String msg_to_send =
							"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"VirtualCallUri\": \""
									+ null
									+ "\" }";
//					Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//					intent_ack.putExtra("msg_to_send", msg_to_send);
//					startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else if(localUri.equals(zhuboUri))//本机操作主播话机
				{
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\" }";
//					Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//					intent_ack.putExtra("msg_to_send", msg_to_send);
//					startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else //本机操作导播话机
				{
					String msg_to_send =
							"{ \"msgType\": \"DAOBO HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ localUri//本机是导播直接使用localUri
									+ "\" }";
//					Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//					intent_ack.putExtra("msg_to_send", msg_to_send);
//					startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
			}
			else if(currentCallWithState.hold) {//取消保持
				currentCallWithState.wait_ackhold = true;
				if (currentCallWithState.virtual && localUri.equals(zhuboUri))//虚拟主播
				{
					String msg_to_send =
							"{ \"msgType\": \"VIRTUAL UNHOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"VirtualCallUri\": \""
									+ null
									+ "\" }";
//					Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//					intent_ack.putExtra("msg_to_send", msg_to_send);
//					startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else if (localUri.equals(zhuboUri))//主播
				{
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO UNHOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\" }";
//					Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//					intent_ack.putExtra("msg_to_send", msg_to_send);
//					startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else //导播
				{
					String msg_to_send =
							"{ \"msgType\": \"DAOBO UNHOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ localUri
									+ "\" }";
//					Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//					intent_ack.putExtra("msg_to_send", msg_to_send);
//					startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
			}
			else
			{
				//既不符合保持电话的条件也不符合取消保持的条件，不做任何操作
			}
		} else {
			alertCallNotSelected();
		}
		//在每次点击按钮后刷新List
		myAdapter.setDataList(allCallinList);
		mbcAdapter.setDataList(allCallinList);
		updateInfoAndButton();
		//callinListmbcAdapter.setDataList(allCallinList);
	}
	@RequiresApi(api = Build.VERSION_CODES.N)
	public void hangupCall(View view){
//		Log.e("步骤1","1"+currentCallWithState.pstn);
//		GlobalVariable.hangup_lock = true;
//		ImageButton btCN4 = findViewById(R.id.buttonCN4);
//		btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_disable));
//		btCN4.setEnabled(false);

		GlobalVariable.operatelock = true;
		updateInfoAndButton();


		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
		}
		LED_Media_Pause();
		GlobalVariable.PhoneRedTitle = false;
		//待处理情况，如果用户呼入导播未接听，挂断应该怎样处理(界面移除但是call不进行挂断操作)
		if(currentCallWithState!=null)
		{
			Log.e("步骤2","2"+currentCallWithState.pstn);
			if(currentCallWithState.live_lock && !GlobalVariable.livelock)//20220905解决即使配置取消直播锁定仍需要两次点击挂断的问题
			{
				currentCallWithState.live_lock = false;
				GlobalVariable.operatelock = false;
				updateInfoAndButton();
				return;
			}
			else if(currentCallWithState.live_lock && GlobalVariable.livelock){
				currentCallWithState.live_lock = false;
				GlobalVariable.operatelock = false;
				updateInfoAndButton();
				return;
			}
			else{
				Log.e("步骤3","3"+currentCallWithState.pstn);
				if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
				{
					GlobalVariable.currentaccept_uid = "";
				}
				//存在线程处理时对象已被释放问题
				tempCall.type = CALL_TYPE.ENDCALL;
				tempCall.pstn = currentCallWithState.pstn;
				tempCall.callednumber = currentCallWithState.callednumber;
				tempCall.uid = currentCallWithState.uid;
				tempCall.name = currentCallWithState.name;
				tempCall.male = currentCallWithState.male;
				tempCall.age = currentCallWithState.age;
				tempCall.from = currentCallWithState.from;
				tempCall.iswhitelist = currentCallWithState.iswhitelist;
				tempCall.isblacklist = currentCallWithState.isblacklist;
				tempCall.iscontact = currentCallWithState.iscontact;
				tempCall.isexpert = currentCallWithState.isexpert;
				tempCall.level = currentCallWithState.level;
				tempCall.province = currentCallWithState.province;
				tempCall.city = currentCallWithState.city;
				tempCall.address = currentCallWithState.address;
				tempCall.content = currentCallWithState.content;
				tempCall.pstnmark = currentCallWithState.pstnmark;
				tempCall.pdrmark = currentCallWithState.pdrmark;
				tempCall.mbcmark = currentCallWithState.mbcmark;
				tempCall.remark = currentCallWithState.remark;

				if (GlobalVariable.charactername.equals("director")) {
					setCallRecordInfo(tempCall);
				} else {
					setCallRecordInfoMbc(tempCall);
				}
				//也是通过type来处理
				if(currentCallWithState.type == CALL_TYPE.NEWINIT
						|| currentCallWithState.type == CALL_TYPE.DIALINIT
				) {
	//				if(!GlobalVariable.charactername.equals("director")&&GlobalVariable.sipmodel !=1||GlobalVariable.sipmodel !=2)//非单多导播模式
	//				{
	//					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ENDCALL);
	//				}
	//				else{
						updateCallRecordST(currentCallWithState.uid,2,CALL_TYPE.ENDCALL);
	//				}
					Log.e("步骤4","4"+currentCallWithState.pstn);
					String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ currentCallWithState.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
	//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
	//				intent_ack.putExtra("msg_to_send", msg_to_send);
	//				startService(intent_ack);
					//2021 06 04去除
					if(wsControl!=null && currentCallWithState.type != CALL_TYPE.WAITDAOBO)wsControl.sendMessage(msg_to_send);
					//if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					//未接听直接挂断
					currentCallWithState.forcehangup =true;
	//				if (GlobalVariable.charactername.equals("director")) {
	//					setCallRecordInfo(currentCallWithState);
	//				} else {
	//					setCallRecordInfoMbc(currentCallWithState);
	//				}
					currentCallWithState.type = CALL_TYPE.ENDCALL;
					broadcastCallType(currentCallWithState,CALL_TYPE.ENDCALL);
					try {
						allCallinList.remove(currentCallWithState.id);
						callinList.remove(currentCallWithState.id);
					}catch(Exception e){};
					updateShowIndex();
					if(callinListSelectedIdx == currentCallWithState.id)
					{
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					resetMainView();
				}
				else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO)
				{
					//主播挂断不算拒绝
					//updateCallRecordST(currentCallWithState.uid,2,CALL_TYPE.ENDCALL);
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ currentCallWithState.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
	//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
	//				intent_ack.putExtra("msg_to_send", msg_to_send);
	//				startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					currentCallWithState.type = CALL_TYPE.ENDCALL;
					broadcastCallType(currentCallWithState,CALL_TYPE.ENDCALL);
					try {
						allCallinList.remove(currentCallWithState.id);
						callinList.remove(currentCallWithState.id);
					}catch(Exception e){};
					updateShowIndex();
					if(callinListSelectedIdx == currentCallWithState.id)
					{
						callinListView.clearChoices();
						callinListSelectedIdx = -1;
					}
					currentCallNum--;

					//这个时候应该来一个1001？
	//				Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
	//				m2.sendToTarget();

					currentCallWithState = null;
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					resetMainView();

				}
	//			else if(currentCallWithState.type == CALL_TYPE.LIVEACPT||currentCallWithState.type == CALL_TYPE.LIVEHOLD)
	//			{
	//				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ENDCALL);
	//				String msg_to_send =
	//						"{ \"msgType\": \"ZHUBO TO VIRTUAL HANGUP CALL\", \"OutCallUri\": \""
	//								+ currentCallWithState.outcalluri
	//								+ "\", \"DaoBoCallUri\": \""
	//								+ currentCallWithState.daobocalluri
	//								+ "\", \"ZhuBoCallUri\": \""
	//								+ MainActivity.zhuboUri
	//								+ "\", \"VirtualUri\": \""
	//								+ MainActivity.virtualUri
	//								+ "\" }";
	//				//2021-03-23 屏蔽虚拟主播
	//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
	//				intent_ack.putExtra("msg_to_send", msg_to_send);
	//				startService(intent_ack);
	//			}
				else if(currentCallWithState.type == CALL_TYPE.DAOBOACPT
					||currentCallWithState.type == CALL_TYPE.DAOBOHOLD
					||currentCallWithState.type == CALL_TYPE.WAITDAOBO
				){
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"DAOBO HANGUP\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ currentCallWithState.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
	//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
	//				intent_ack.putExtra("msg_to_send", msg_to_send);
	//				startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					try {
						currentCallWithState.type = CALL_TYPE.ENDCALL;
						broadcastCallType(currentCallWithState,CALL_TYPE.ENDCALL);
						allCallinList.remove(currentCallWithState.id);
						callinList.remove(currentCallWithState.id);
						updateShowIndex();
						if(callinListSelectedIdx == currentCallWithState.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}

				}
				else if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT
						||currentCallWithState.type == CALL_TYPE.ZHUBOHOLD
						||currentCallWithState.type == CALL_TYPE.LIVEACPT
						||currentCallWithState.type == CALL_TYPE.LIVEHOLD
				)
				{
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ENDCALL);
					String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ currentCallWithState.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"VirtualUri\": \""
							+ null
							+ "\" }";
	//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
	//				intent_ack.putExtra("msg_to_send", msg_to_send);
	//				startService(intent_ack);
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					try {
						currentCallWithState.type = CALL_TYPE.ENDCALL;
						broadcastCallType(currentCallWithState,CALL_TYPE.ENDCALL);
						allCallinList.remove(currentCallWithState.id);
						callinList.remove(currentCallWithState.id);
						updateShowIndex();
						if(callinListSelectedIdx == currentCallWithState.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}

				}
				else
				{
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.ENDCALL);
					try {
						currentCallWithState.type = CALL_TYPE.ENDCALL;
						broadcastCallType(currentCallWithState,CALL_TYPE.ENDCALL);
						allCallinList.remove(currentCallWithState.id);
						callinList.remove(currentCallWithState.id);
						updateShowIndex();
						if(callinListSelectedIdx == currentCallWithState.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
					} catch (Exception e) {
						System.out.println(e);
					}
				}
				//处理挂断后挂断按钮状态不能正确刷新问题
				currentCallWithState = null;
			}

		}
		else{ alertCallNotSelected(); }


		//在每次点击按钮后刷新List
		refreshCallList();
		myAdapter.setDataList(allCallinList);
		mbcAdapter.setDataList(allCallinList);

		GlobalVariable.operatelock = false;
		updateInfoAndButton();


		if(callinListSelectedIdx!=0&&allCallinList.size()>0)
		{
			callinListView.setItemChecked(0,true);
			currentCallWithState = allCallinList.get(0);
			callinListSelectedIdx = 0;
			updateInfoAndButton();
		}
	}
	@RequiresApi(api = Build.VERSION_CODES.N)
	public void switchCall(View view)  {
		if(GlobalVariable.operatelock) return;
		GlobalVariable.operatelock = true;
		updateInfoAndButton();

		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
			//处理耦合器按钮显示
			for(MyCallWithState callws : allCallinList){
				callws.selected_line_btn = false;
			}
			currentCallWithState.selected_line_btn = true;
		}
		LED_Media_Pause();
		GlobalVariable.PhoneRedTitle = false;
		if(currentCallWithState!=null) {
			if (currentCallWithState.type == CALL_TYPE.LIVEHOLD){
				if (GlobalVariable.sipmodel != 3) {
					currentCallWithState.first_livehold = false;
				}
			}
			if(GlobalVariable.currentaccept_uid.equals(currentCallWithState.uid))
			{
				GlobalVariable.currentaccept_uid = "";
			}
			//存在线程处理时对象已被释放问题
			tempCall.type = currentCallWithState.type;
			tempCall.pstn = currentCallWithState.pstn;
			tempCall.uid = currentCallWithState.uid;
			tempCall.name = currentCallWithState.name;
			tempCall.male = currentCallWithState.male;
			tempCall.age = currentCallWithState.age;
			tempCall.from = currentCallWithState.from;
			tempCall.iswhitelist = currentCallWithState.iswhitelist;
			tempCall.isblacklist = currentCallWithState.isblacklist;
			tempCall.iscontact = currentCallWithState.iscontact;
			tempCall.isexpert = currentCallWithState.isexpert;
			tempCall.level = currentCallWithState.level;
			tempCall.province = currentCallWithState.province;
			tempCall.city = currentCallWithState.city;
			tempCall.address = currentCallWithState.address;
			tempCall.content = currentCallWithState.content;
			tempCall.pstnmark = currentCallWithState.pstnmark;
			tempCall.pdrmark = currentCallWithState.pdrmark;
			tempCall.mbcmark = currentCallWithState.mbcmark;
			tempCall.remark = currentCallWithState.remark;

			if (GlobalVariable.charactername.equals("director")) {
				setCallRecordInfo(tempCall);
			} else {
				setCallRecordInfoMbc(tempCall);
			}
			if(currentCallWithState.type == CALL_TYPE.DAOBOACPT)
			{
				if(GlobalVariable.charactername.equals("director")){
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITZHUBO);
					//导播通知SIP服务器，电话转给主播接听
					currentCallWithState.state = CALL_STATE_TYPE.DAOBO_SWITCH;
					broadcastCallType(currentCallWithState,CALL_TYPE.WAITZHUBO);
					String msg_to_send = "{ \"msgType\": \"DAOBO TO ZHUBO\", \"DaoBoCallUri\": \""
							+ localUri//这里是导播发消息
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri
							+ "\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri + "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else{
					//当前是在主播端，出现导播接听状态代表电话呼出成功

					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
					currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_SWITCH;

					String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
							+ currentCallWithState.outcalluri
							+ "\", \"DaoBoCallUri\": \""
							+ currentCallWithState.daobocalluri
							+ "\", \"ZhuBoCallUri\": \""
							+ MainActivity.zhuboUri + "\" }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);

					//更新type为待导播接听
					broadcastCallType(currentCallWithState,CALL_TYPE.WAITDAOBO);
					try {
						allCallinList.remove(currentCallWithState.id);
						callinList.remove(currentCallWithState.id);
						updateShowIndex();
						if(callinListSelectedIdx == currentCallWithState.id)
						{
							callinListSelectedIdx = -1;
						}
						currentCallNum--;
						resetMainView();
						refreshCallList();
						if(GlobalVariable.viewmodel==1) {
							updateOperateButton();
						}
						else if(GlobalVariable.viewmodel==3){
							updateGsOperateButton();
						}
						myAdapter.setDataList(allCallinList);
						mbcAdapter.setDataList(allCallinList);
					} catch (Exception e) {
						System.out.println(e);
					}
				}

			}
			else if(currentCallWithState.type == CALL_TYPE.NEWINIT
				||currentCallWithState.type == CALL_TYPE.WAITDAOBO)
			{
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITZHUBO);
				//先接电话再转电话
				GlobalVariable.operatelock = false;
				acceptCall(null);
				currentCallWithState.autoswitch = true;


			}
			else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO)
			{

				//20210510新需求使用接听按键处理主播待接听转回
				if(GlobalVariable.charactername.equals("director"))
				{
					//2021-04-09处理导播之间转接的问题
//					if(currentCallWithState.mycall == null)
//					{
//						try {
//							MyCall mycall  = new MyCall(account, -1);
//							CallOpParam prm1 = new CallOpParam(true);
//							currentCallWithState.mycall.makeCall(sipUriParam, prm1);
//						} catch (Exception e) {
//							e.printStackTrace();
//							currentCallWithState.mycall.delete();
//							return;
//						}
//					}

//					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.DAOBOACPT);
//					//当前是导播客户端，需要取消转接操作
//					String msg_to_send = "{ \"msgType\": \"DAOBO CANCEL SWITCH\", \"DaoBoCallUri\": \""
//							+ localUri//这里是导播发消息
//							+ "\", \"ZhuBoCallUri\": \""
//							+ MainActivity.zhuboUri
//							+ "\", \"OutCallUri\": \""
//							+ currentCallWithState.outcalluri + "\" }";
//					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else{
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
					//先接电话再转电话
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}

					GlobalVariable.operatelock = false;
					acceptCall(null);
					currentCallWithState.autoswitch = true;

				}

			}
			else if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT){
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
				//主播通知SIP服务器，应答主播返回电话
				//currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_SWITCH;
				String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
						+ currentCallWithState.outcalluri
						+ "\", \"DaoBoCallUri\": \""
						+ currentCallWithState.daobocalluri
						+ "\", \"ZhuBoCallUri\": \""
						+ MainActivity.zhuboUri + "\" }";
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				//通知完毕后直接在这边将该电话挂断，并将界面中的电话移除
				try {

					broadcastCallType(currentCallWithState,CALL_TYPE.WAITDAOBO);
					allCallinList.remove(currentCallWithState.id);
					callinList.remove(currentCallWithState.id);
					updateShowIndex();
					if(callinListSelectedIdx == currentCallWithState.id)
					{
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					resetMainView();
					refreshCallList();
					myAdapter.setDataList(allCallinList);
					mbcAdapter.setDataList(allCallinList);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			else if(currentCallWithState.type == CALL_TYPE.LIVEACPT || currentCallWithState.type == CALL_TYPE.LIVEHOLD)
			{
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.WAITDAOBO);
				currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_SWITCH;
//				String msg_to_send =
//						"{ \"msgType\": \"ZHUBO TO VIRTUAL HANGUP CALL\", \"OutCallUri\": \""
//								+ currentCallWithState.outcalluri
//								+ "\", \"DaoBoCallUri\": \""
//								+ currentCallWithState.daobocalluri
//								+ "\", \"ZhuBoCallUri\": \""
//								+ MainActivity.zhuboUri
//								+ "\", \"VirtualUri\": \""
//								+ MainActivity.virtualUri
//								+ "\" }";
//				//2021-03-23 屏蔽虚拟主播
//				Intent intent_ack = new Intent(this, JWebSocketClientService.class);
//				intent_ack.putExtra("msg_to_send", msg_to_send);
//				startService(intent_ack);
				String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
						+ currentCallWithState.outcalluri
						+ "\", \"DaoBoCallUri\": \""
						+ currentCallWithState.daobocalluri
						+ "\", \"ZhuBoCallUri\": \""
						+ MainActivity.zhuboUri + "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);

				//更新type为待导播接听
				broadcastCallType(currentCallWithState,CALL_TYPE.WAITDAOBO);
				try {
					allCallinList.remove(currentCallWithState.id);
					callinList.remove(currentCallWithState.id);
					updateShowIndex();
					if(callinListSelectedIdx == currentCallWithState.id)
					{
						callinListSelectedIdx = -1;
					}
					currentCallNum--;
					//处理多通电话转出时按钮和后台电话不对应的问题，按钮状态不能正确刷新
					currentCallWithState = null;
					resetMainView();
					refreshCallList();
					if(GlobalVariable.viewmodel==1) {
						updateOperateButton();
					}
					else if(GlobalVariable.viewmodel==3){
						updateGsOperateButton();
					}
					myAdapter.setDataList(allCallinList);
					mbcAdapter.setDataList(allCallinList);
				} catch (Exception e) {
					System.out.println(e);
				}

			}
		}
		else{ alertCallNotSelected(); }
		//在每次点击按钮后刷新List
		myAdapter.setDataList(allCallinList);
		mbcAdapter.setDataList(allCallinList);


		GlobalVariable.operatelock = false;
		updateInfoAndButton();


	}
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void liveCall(View view){
		if(GlobalVariable.operatelock) return;
		GlobalVariable.operatelock = true;
		updateInfoAndButton();

		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
			//处理耦合器按钮显示
			for(MyCallWithState callws : allCallinList){
				callws.selected_line_btn = false;
			}
			currentCallWithState.selected_line_btn = true;
		}

		LED_Media_Pause();
		GlobalVariable.PhoneRedTitle = false;
		ImageButton livecallbutton = (ImageButton)findViewById(R.id.buttonCN6);
		//livecallbutton.setTextColor(android.graphics.Color.RED);
		//如果当前没有切到虚拟主播，将状态切换到虚拟主播状态，但是不允许虚拟主播状态转回
		if(currentCallWithState!=null) {
			if(currentCallWithState.type == CALL_TYPE.LIVEHOLD) {
				if(GlobalVariable.sipmodel != 3) {
					currentCallWithState.first_livehold = false;
				}
			}
			//优先处理无导播时主播将电话切入直播前要先进入预备播出（播出保持）状态的问题
			if((currentCallWithState.type == CALL_TYPE.WAITZHUBO
					|| currentCallWithState.type == CALL_TYPE.ZHUBOACPT
					|| currentCallWithState.type == CALL_TYPE.ZHUBOHOLD
					|| currentCallWithState.type == CALL_TYPE.DAOBOACPT) && GlobalVariable.sipmodel == 0
			) {
				if(currentCallWithState.first_livehold) {
					if (!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.LIVEHOLD);
					currentCallWithState.wait_ackhold = true;
					String msg_to_send =
							"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"VirtualCallUri\": \""
									+ null
									+ "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				else {
					//设置直播锁定
					currentCallWithState.live_lock = true;
					//导播直接转直播的情况，待处理
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEACPT);
					if(!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ currentCallWithState.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ currentCallWithState.record
									+ " }";
					//2021-03-23 屏蔽虚拟主播
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
			}
			else if(
				currentCallWithState.type == CALL_TYPE.NEWINIT
				||currentCallWithState.type == CALL_TYPE.WAITDAOBO
				||currentCallWithState.type == CALL_TYPE.DAOBOACPT
			)
			{
				if(GlobalVariable.isDL1018){
					//设置直播锁定
					currentCallWithState.live_lock = true;
					//导播直接转直播的情况，待处理
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEACPT);
					if(!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ currentCallWithState.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ currentCallWithState.record
									+ " }";
					//2021-03-23 屏蔽虚拟主播
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else {
					if(!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					//if(currentCallWithState.dailout)//20220216状态操作统一，在仅导播模式下也先进行预备播出再进入播出状态
					if(!currentCallWithState.first_livehold)
					{
						//设置直播锁定
						currentCallWithState.live_lock = true;
						updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEACPT);
						currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
						String msg_to_send =
								"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
										+ currentCallWithState.outcalluri
										+ "\", \"DaoBoCallUri\": \""
										+ currentCallWithState.daobocalluri
										+ "\", \"ZhuBoCallUri\": \""
										+ MainActivity.zhuboUri
										+ "\", \"VirtualUri\": \""
										+ null
										+ "\", \"record\": "
										+ currentCallWithState.record
										+ " }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					}
					else {
						updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.LIVEHOLD);
						currentCallWithState.wait_ackhold = true;
						String msg_to_send =
								"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
										+ currentCallWithState.outcalluri
										+ "\", \"VirtualCallUri\": \""
										+ null
										+ "\" }";
						if (wsControl != null) wsControl.sendMessage(msg_to_send);
					}
				}
			}
			else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO
				||currentCallWithState.type == CALL_TYPE.ZHUBOACPT
			)
			{
				if(!currentCallWithState.first_livehold || GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) {
					//设置直播锁定
					currentCallWithState.live_lock = true;
					updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.LIVEACPT);
					if (!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ currentCallWithState.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ currentCallWithState.record
									+ " }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
				else {
					updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.LIVEHOLD);
					currentCallWithState.wait_ackhold = true;
					String msg_to_send =
							"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"VirtualCallUri\": \""
									+ null
									+ "\" }";
					if (wsControl != null) wsControl.sendMessage(msg_to_send);
				}
			}
			else if(currentCallWithState.type == CALL_TYPE.LIVEACPT)
			{
				if(!currentCallWithState.virtual) {
					currentCallWithState.virtual = true;
				}
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEHOLD);
				currentCallWithState.wait_ackhold = true;
				String msg_to_send =
						"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
								+ currentCallWithState.outcalluri
								+ "\", \"VirtualCallUri\": \""
								+ null
								+ "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			}
			else if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
				//设置直播锁定
				currentCallWithState.live_lock = true;
				updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEACPT);
				currentCallWithState.wait_ackhold = true;
				String msg_to_send =
						"{ \"msgType\": \"VIRTUAL UNHOLD\", \"OutCallUri\": \""
								+ currentCallWithState.outcalluri
								+ "\", \"VirtualCallUri\": \""
								+ null
								+ "\" }";
				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
			}
			else if(currentCallWithState.type == CALL_TYPE.DAOBOHOLD
					||currentCallWithState.type == CALL_TYPE.ZHUBOHOLD
			){
				if(GlobalVariable.isDL1018){
					//设置直播锁定
					currentCallWithState.live_lock = true;
					updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEACPT);
					if(!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
					String msg_to_send =
							"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
									+ currentCallWithState.outcalluri
									+ "\", \"DaoBoCallUri\": \""
									+ currentCallWithState.daobocalluri
									+ "\", \"ZhuBoCallUri\": \""
									+ MainActivity.zhuboUri
									+ "\", \"VirtualUri\": \""
									+ null
									+ "\", \"record\": "
									+ currentCallWithState.record
									+ " }";
					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
				}
				else{
					if(!currentCallWithState.virtual) {
						currentCallWithState.virtual = true;
					}
					//20221018已经修改后台逻辑，可以直接保持切直播保持或者直播
					if(!currentCallWithState.first_livehold || GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2){

						//20211228调整保持状态都可以直接切直播而不是直播保持（似乎后台不能直接切到直播保持状态，状态也不对）
						//设置直播锁定
						currentCallWithState.live_lock = true;
						updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEACPT);

						currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
						String msg_to_send =
								"{ \"msgType\": \"ZHUBO TO VIRTUAL ACCEPT CALL\", \"OutCallUri\": \""
										+ currentCallWithState.outcalluri
										+ "\", \"DaoBoCallUri\": \""
										+ currentCallWithState.daobocalluri
										+ "\", \"ZhuBoCallUri\": \""
										+ MainActivity.zhuboUri
										+ "\", \"VirtualUri\": \""
										+ null
										+ "\", \"record\": "
										+ currentCallWithState.record
										+ " }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					}
					else {
						if(!currentCallWithState.virtual) {
							currentCallWithState.virtual = true;
						}
						updateCallRecordST(currentCallWithState.uid,1,CALL_TYPE.LIVEHOLD);
						currentCallWithState.wait_ackhold = true;
						String msg_to_send =
								"{ \"msgType\": \"VIRTUAL HOLD\", \"OutCallUri\": \""
										+ currentCallWithState.outcalluri
										+ "\", \"VirtualCallUri\": \""
										+ null
										+ "\" }";
						if(wsControl!=null)wsControl.sendMessage(msg_to_send);
					}

				}



			}

		}
		else{ alertCallNotSelected(); }
		//在每次点击按钮后刷新List
		myAdapter.setDataList(allCallinList);
		mbcAdapter.setDataList(allCallinList);

		GlobalVariable.operatelock = false;
		updateInfoAndButton();
	}
	public void liteToLiveCall(View view){
		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
			//处理耦合器按钮显示
			for(MyCallWithState callws : allCallinList){
				callws.selected_line_btn = false;
			}
			currentCallWithState.selected_line_btn = true;
		}
		String msg_to_send = "{ \"msgType\": \"LITE DAOBO TO LIVING\", \"OutCallUri\": \""
				+ currentCallWithState.outcalluri
				+ "\", \"DaoBoCallUri\": \""
				+ currentCallWithState.daobocalluri
				+ "\"}";
		if(wsControl!=null)wsControl.sendMessage(msg_to_send);
	}
	public void liteEndLiveCall(View view){
		if(GlobalVariable.viewmodel == 2 && view!=null){
			int index = Integer.parseInt(String.valueOf(view.getTag()));
			int id_index = GlobalVariable.ctIdMap.get(index);
			if(id_index>=0&& id_index<allCallinList.size()){
				currentCallWithState = allCallinList.get(id_index);
			}
			//处理耦合器按钮显示
			for(MyCallWithState callws : allCallinList){
				callws.selected_line_btn = false;
			}
			currentCallWithState.selected_line_btn = true;
		}
		String msg_to_send = "{ \"msgType\": \"LITE LIVING TO DAOBO\", \"OutCallUri\": \""
				+ currentCallWithState.outcalluri
				+ "\", \"DaoBoCallUri\": \""
				+ currentCallWithState.daobocalluri
				+ "\"}";
		if(wsControl!=null)wsControl.sendMessage(msg_to_send);
	}
	public void savePstn(View view){
		new Thread() {
			@Override
			public void run() {
				HttpRequest request = new HttpRequest();
				String rs = request.postJson(
						"Http://"+GlobalVariable.SIPSERVER+":8080/smart/contactWithSipServer/getPstnByPstn",
						"{\"type\":\"post pstn info\", \"pstn\":\"18325539967\"}"
				);
			}
		}.start();
	}
	/**右侧详细信息显示更新**/
	public void updateCurrentInfoView()  {
		if(currentCallWithState!=null) {
			EditText editTextName = (EditText)findViewById(R.id.editTextName);
			EditText textViewDescribe = (EditText)findViewById(R.id.textViewDescribe);
			EditText textViewInfo = (EditText)findViewById(R.id.textViewInfo);
			EditText textViewPdrRemark = (EditText)findViewById(R.id.textViewPdrRemark);
			EditText textViewMbcRemark = (EditText)findViewById(R.id.textViewMbcRemark);
			TextView textPstn = (TextView)findViewById(R.id.textPstn);

			ImageView editImgType = (ImageView)findViewById(R.id.editImgType);
			TextView textCallStartTime = (TextView)findViewById(R.id.textCallStartTime);
			if(currentCallWithState.dailout){
				//textPstn.setText(currentCallWithState.pstn.replaceFirst("00",""));
                textPstn.setText(currentCallWithState.pstn.substring(2));
			}
			else{
				textPstn.setText(currentCallWithState.pstn);
			}

			editTextName.setText(currentCallWithState.name);
			textViewDescribe.setText(currentCallWithState.pstnmark);
			textViewInfo.setText(currentCallWithState.content);
			textViewPdrRemark.setText(currentCallWithState.pdrmark);
			textViewMbcRemark.setText(currentCallWithState.mbcmark);

			if(currentCallWithState.type == CALL_TYPE.NEWINIT){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_init));
			}
			else if(currentCallWithState.type == CALL_TYPE.DAOBOACPT){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_acpt));
			}
			else if(currentCallWithState.type == CALL_TYPE.DAOBOHOLD){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_hold));
			}
			else if(currentCallWithState.type == CALL_TYPE.WAITDAOBO){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_switch_green));
			}
			else if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_acpt_green));
			}
			else if(currentCallWithState.type == CALL_TYPE.ZHUBOHOLD){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_hold_green));
			}
			else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_init_green));
			}
			else if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_live_green));
			}
			else if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.phonestate_livehold_green));
			}
			else if(currentCallWithState.type == CALL_TYPE.ENDCALL){
				editImgType.setImageDrawable(getResources().getDrawable(R.drawable.hangup));
			}


			try {
				SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
				textCallStartTime.setText(df.format(df.parse(currentCallWithState.callintime)));
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	public void updateGsInfoView(){
		Map<Integer,String> locationMap = new HashMap<>();

		LinearLayout ct_bkg_1 = findViewById(R.id.gs_state_bkg_1);
		LinearLayout ct_bkg_2 = findViewById(R.id.gs_state_bkg_2);
		LinearLayout ct_bkg_3 = findViewById(R.id.gs_state_bkg_3);
		LinearLayout ct_bkg_4 = findViewById(R.id.gs_state_bkg_4);
		LinearLayout ct_bkg_5 = findViewById(R.id.gs_state_bkg_5);
		LinearLayout ct_bkg_6 = findViewById(R.id.gs_state_bkg_6);

		TextView ctCallNum1 = (TextView)findViewById(R.id.gs_phone_num_1);
		TextView ctCallNum2 = (TextView)findViewById(R.id.gs_phone_num_2);
		TextView ctCallNum3 = (TextView)findViewById(R.id.gs_phone_num_3);
		TextView ctCallNum4 = (TextView)findViewById(R.id.gs_phone_num_4);
		TextView ctCallNum5 = (TextView)findViewById(R.id.gs_phone_num_5);
		TextView ctCallNum6 = (TextView)findViewById(R.id.gs_phone_num_6);

		TextView ctAcptTime1 = (TextView)findViewById(R.id.gs_acpt_time_1);
		TextView ctAcptTime2 = (TextView)findViewById(R.id.gs_acpt_time_2);
		TextView ctAcptTime3 = (TextView)findViewById(R.id.gs_acpt_time_3);
		TextView ctAcptTime4 = (TextView)findViewById(R.id.gs_acpt_time_4);
		TextView ctAcptTime5 = (TextView)findViewById(R.id.gs_acpt_time_5);
		TextView ctAcptTime6 = (TextView)findViewById(R.id.gs_acpt_time_6);

//		TextView ctLiveTime1 = (TextView)findViewById(R.id.ct_live_time_1);
//		TextView ctLiveTime2 = (TextView)findViewById(R.id.ct_live_time_2);
//		TextView ctLiveTime3 = (TextView)findViewById(R.id.ct_live_time_3);
//		TextView ctLiveTime4 = (TextView)findViewById(R.id.ct_live_time_4);
//		TextView ctLiveTime5 = (TextView)findViewById(R.id.ct_live_time_5);
//		TextView ctLiveTime6 = (TextView)findViewById(R.id.ct_live_time_6);

//		GifImageView ctInitFlash1 = (GifImageView)findViewById(R.id.ct_init_flash_1);
//		GifImageView ctInitFlash2 = (GifImageView)findViewById(R.id.ct_init_flash_2);
//		GifImageView ctInitFlash3 = (GifImageView)findViewById(R.id.ct_init_flash_3);
//		GifImageView ctInitFlash4 = (GifImageView)findViewById(R.id.ct_init_flash_4);
//		GifImageView ctInitFlash5 = (GifImageView)findViewById(R.id.ct_init_flash_5);
//		GifImageView ctInitFlash6 = (GifImageView)findViewById(R.id.ct_init_flash_6);

		TextView ctLineNumber1 = (TextView)findViewById(R.id.gs_line_number_1);
		TextView ctLineNumber2 = (TextView)findViewById(R.id.gs_line_number_2);
		TextView ctLineNumber3 = (TextView)findViewById(R.id.gs_line_number_3);
		TextView ctLineNumber4 = (TextView)findViewById(R.id.gs_line_number_4);
		TextView ctLineNumber5 = (TextView)findViewById(R.id.gs_line_number_5);
		TextView ctLineNumber6 = (TextView)findViewById(R.id.gs_line_number_6);

		LinearLayout ct_bkg = ct_bkg_1;
		TextView ct_acpttime = ctAcptTime1;
//		TextView ct_livetime = ctLiveTime1;
		TextView ct_callnum = ctCallNum1;
//		GifImageView ct_init_flash = ctInitFlash1;
		TextView ct_line_number = ctLineNumber1;
		String line_number = GlobalVariable.ctNumberMapRevert.get(1) == null?"":GlobalVariable.ctNumberMapRevert.get(1);
		boolean line_busy = GlobalVariable.linebusy1;

		for(MyCallWithState callws : allCallinList) {
			//int id = allCallinList.indexOf(callws)+1;
			int id = GlobalVariable.ctNumberMap.get(callws.callednumber) == null?0:GlobalVariable.ctNumberMap.get(callws.callednumber);

			//标记该列已被占用
			locationMap.put(id,"1");
			//标记该列id对应List中的id
			GlobalVariable.ctIdMap.put(id,callws.id);

			if (id == 1) {
				ct_bkg = ct_bkg_1;
				ct_acpttime = ctAcptTime1;
//				ct_livetime = ctLiveTime1;
				ct_callnum = ctCallNum1;
//				ct_init_flash = ctInitFlash1;
				ct_line_number = ctLineNumber1;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(1)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(1);
					tail_num = line_num;
//					if (line_num.length() >= 4) {
//						tail_num = line_num.substring(line_num.length() - 4);
//					} else {
//						tail_num = line_num;
//					}
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 2) {
				ct_bkg = ct_bkg_2;
				ct_acpttime = ctAcptTime2;
//				ct_livetime = ctLiveTime2;
				ct_callnum = ctCallNum2;
//				ct_init_flash = ctInitFlash2;
				ct_line_number = ctLineNumber2;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(2)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(2);
					tail_num = line_num;
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 3) {
				ct_bkg = ct_bkg_3;
				ct_acpttime = ctAcptTime3;
//				ct_livetime = ctLiveTime3;
				ct_callnum = ctCallNum3;
//				ct_init_flash = ctInitFlash3;
				ct_line_number = ctLineNumber3;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(3)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(3);
					tail_num = line_num;
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 4) {
				ct_bkg = ct_bkg_4;
				ct_acpttime = ctAcptTime4;
//				ct_livetime = ctLiveTime4;
				ct_callnum = ctCallNum4;
//				ct_init_flash = ctInitFlash4;
				ct_line_number = ctLineNumber4;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(4)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(4);
					tail_num = line_num;
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 5) {
				ct_bkg = ct_bkg_5;
				ct_acpttime = ctAcptTime5;
//				ct_livetime = ctLiveTime5;
				ct_callnum = ctCallNum5;
//				ct_init_flash = ctInitFlash5;
				ct_line_number = ctLineNumber5;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(5)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(5);
					tail_num = line_num;
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 6) {
				ct_bkg = ct_bkg_6;
				ct_acpttime = ctAcptTime6;
//				ct_livetime = ctLiveTime6;
				ct_callnum = ctCallNum6;
//				ct_init_flash = ctInitFlash6;
				ct_line_number = ctLineNumber6;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(6)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(6);
					tail_num = line_num;
				}
				ct_line_number.setText(tail_num);
			}
			//在每次显示更新前先屏蔽闪烁GIF
//			ct_init_flash.setVisibility(View.GONE);
			if(callws.type == CALL_TYPE.DIALINIT)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_dailout_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green3_selected));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green3_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_dailout));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green3));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green3));
				}
			}
			else if(callws.type == CALL_TYPE.NEWINIT)
			{
				if(callws.selected_line_btn){
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_init_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green1_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green1_selected));
//					ct_init_flash.setVisibility(View.VISIBLE);
				}else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_init));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green1));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green1));
//					ct_init_flash.setVisibility(View.VISIBLE);
				}

				if(callws.dailout){
					if(callws.selected_line_btn) {
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_dailout_selected));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green3_selected));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green3_selected));
					}
					else{
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_dailout));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green3));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green3));
					}

				}
			}
			else if(callws.type == CALL_TYPE.WAITDAOBO)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_revert_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green4_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green4_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_revert));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green4));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green4));
				}

			}
			else if(callws.type == CALL_TYPE.DAOBOACPT)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_accept_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green2_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green2));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green2_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_accept));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green2));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green2));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green2));
				}

				if(callws.dailout){
					if(callws.selected_line_btn) {
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_dailout_selected));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green3_selected));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green3_selected));
					}
					else{
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_dailout));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green3));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green3));
					}

				}
			}
			else if(callws.type == CALL_TYPE.DAOBOHOLD)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_hold_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_yellow_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_yellow));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_yellow_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_hold));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_yellow));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_yellow));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_yellow));
				}

			}
			else if(callws.type == CALL_TYPE.WAITZHUBO)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_waitzhubo_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_blue_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_blue));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_blue_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_waitzhubo));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_blue));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_blue));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_blue));
				}

				if(GlobalVariable.charactername.equals("broadcaster") && GlobalVariable.sipmodel == 0){
					//无导播模式下主播不显示等待主播而显示新呼入
					if(callws.selected_line_btn) {
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_init_selected));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green1_selected));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green1_selected));
//						ct_init_flash.setVisibility(View.VISIBLE);
					}
					else{
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_init));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green1));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green1));
//						ct_init_flash.setVisibility(View.VISIBLE);
					}

				}
			}
			else if(callws.type == CALL_TYPE.ZHUBOACPT)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_accept_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green2_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green2));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green2_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_accept));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green2));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green2));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_green2));
				}

				if(GlobalVariable.charactername.equals("broadcaster") && (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2)){
					//单导播和多导播在转接回导播的时候
					if(callws.selected_line_btn) {
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_revert_selected));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green4_selected));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green4_selected));
					}
					else{
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_revert));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_green4));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_green4));
					}

				}
			}
			else if(callws.type == CALL_TYPE.ZHUBOHOLD)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_hold_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_yellow_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_yellow));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_yellow_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_hold));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_yellow));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_yellow));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_yellow));
				}

			}
			else if(callws.type == CALL_TYPE.LIVEACPT)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_live_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_red_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_red));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_red_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_live));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_red));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_red));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_red));
				}

			}
			else if(callws.type == CALL_TYPE.LIVEHOLD)
			{
				if(callws.selected_line_btn) {
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_livehold_selected));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_purple_selected));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_purple));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_purple_selected));
				}
				else{
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_livehold));
					ct_acpttime.setTextColor(getResources().getColor(R.color.gs_purple));
//					ct_livetime.setTextColor(getResources().getColor(R.color.ct_purple));
					ct_callnum.setTextColor(getResources().getColor(R.color.gs_purple));
				}

				if(callws.first_livehold && (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3) ){
					if(callws.selected_line_btn) {
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_waitzhubo_selected));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_blue_selected));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_blue));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_blue_selected));
					}
					else{
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_waitzhubo));
						ct_acpttime.setTextColor(getResources().getColor(R.color.gs_blue));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_blue));
						ct_callnum.setTextColor(getResources().getColor(R.color.gs_blue));
					}

				}
			}
			else if(callws.type == CALL_TYPE.ENDCALL)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_hangup));
				ct_acpttime.setTextColor(getResources().getColor(R.color.gs_gray));
				ct_callnum.setTextColor(getResources().getColor(R.color.gs_gray));
			}
		}
		for(int j=1;j<=6;j++){
			//所有未被处理的电话
			boolean is_gs_selected = false;
			if(locationMap.get(j)==null) {
				int id = j;
				if(id == 1){
					ct_bkg = ct_bkg_1;
					ct_acpttime = ctAcptTime1;
//					ct_livetime = ctLiveTime1;
					ct_callnum = ctCallNum1;
//					ct_init_flash = ctInitFlash1;
					line_number = GlobalVariable.ctNumberMapRevert.get(1) == null?"":GlobalVariable.ctNumberMapRevert.get(1);
					line_busy = GlobalVariable.linebusy1;
					if(GlobalVariable.gs_selected_index == 1)is_gs_selected = true;
				}
				else if(id == 2){
					ct_bkg = ct_bkg_2;
					ct_acpttime = ctAcptTime2;
//					ct_livetime = ctLiveTime2;
					ct_callnum = ctCallNum2;
//					ct_init_flash = ctInitFlash2;
					line_number = GlobalVariable.ctNumberMapRevert.get(2) == null?"":GlobalVariable.ctNumberMapRevert.get(2);
					line_busy = GlobalVariable.linebusy2;
					if(GlobalVariable.gs_selected_index == 2)is_gs_selected = true;
				}
				else if(id == 3){
					ct_bkg = ct_bkg_3;
					ct_acpttime = ctAcptTime3;
//					ct_livetime = ctLiveTime3;
					ct_callnum = ctCallNum3;
//					ct_init_flash = ctInitFlash3;
					line_number = GlobalVariable.ctNumberMapRevert.get(3) == null?"":GlobalVariable.ctNumberMapRevert.get(3);
					line_busy = GlobalVariable.linebusy3;
					if(GlobalVariable.gs_selected_index == 3)is_gs_selected = true;
				}
				else if(id == 4){
					ct_bkg = ct_bkg_4;
					ct_acpttime = ctAcptTime4;
//					ct_livetime = ctLiveTime4;
					ct_callnum = ctCallNum4;
//					ct_init_flash = ctInitFlash4;
					line_number = GlobalVariable.ctNumberMapRevert.get(4) == null?"":GlobalVariable.ctNumberMapRevert.get(4);
					line_busy = GlobalVariable.linebusy4;
					if(GlobalVariable.gs_selected_index == 4)is_gs_selected = true;
				}
				else if(id == 5){
					ct_bkg = ct_bkg_5;
					ct_acpttime = ctAcptTime5;
//					ct_livetime = ctLiveTime5;
					ct_callnum = ctCallNum5;
//					ct_init_flash = ctInitFlash5;
					line_number = GlobalVariable.ctNumberMapRevert.get(5) == null?"":GlobalVariable.ctNumberMapRevert.get(5);
					line_busy = GlobalVariable.linebusy5;
					if(GlobalVariable.gs_selected_index == 5)is_gs_selected = true;
				}
				else if(id == 6){
					ct_bkg = ct_bkg_6;
					ct_acpttime = ctAcptTime6;
//					ct_livetime = ctLiveTime6;
					ct_callnum = ctCallNum6;
//					ct_init_flash = ctInitFlash6;
					line_number = GlobalVariable.ctNumberMapRevert.get(6) == null?"":GlobalVariable.ctNumberMapRevert.get(6);
					line_busy = GlobalVariable.linebusy6;
					if(GlobalVariable.gs_selected_index == 6)is_gs_selected = true;
				}
//				ct_init_flash.setVisibility(View.GONE);

				if(line_number.equals("")){
					if(is_gs_selected){
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_idle_selected));
					}
					else{
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_idle));
					}

					ct_acpttime.setTextColor(getResources().getColor(R.color.black));
//					ct_livetime.setTextColor(getResources().getColor(R.color.black));
					ct_callnum.setTextColor(getResources().getColor(R.color.black));
				}
				else {
					if(GlobalVariable.busyall == 1) {
						//全部置忙
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_busy));
						ct_acpttime.setTextColor(getResources().getColor(R.color.ct_gray));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_gray));
						ct_callnum.setTextColor(getResources().getColor(R.color.ct_gray));
					}
					else if(GlobalVariable.busyall == 0){
						if(is_gs_selected){
							ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_idle_selected));
						}
						else{
							ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_idle));
						}
						ct_acpttime.setTextColor(getResources().getColor(R.color.black));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.black));
					}
					else if(line_busy){
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_busy));
						ct_acpttime.setTextColor(getResources().getColor(R.color.ct_gray));
//						ct_livetime.setTextColor(getResources().getColor(R.color.ct_gray));
						ct_callnum.setTextColor(getResources().getColor(R.color.ct_gray));
					}
					else {
						if(is_gs_selected){
							ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_idle_selected));
						}
						else{
							ct_bkg.setBackground(getResources().getDrawable(R.drawable.gs_state_bkg_idle));
						}
						ct_acpttime.setTextColor(getResources().getColor(R.color.black));
//						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.black));
					}

				}
			}
		}

		//以上为处理左侧界面刷新
		//以下为处理右侧界面的刷新
		if(currentCallWithState!=null) {
			EditText editTextName = (EditText)findViewById(R.id.editTextName);
			EditText textViewInfo = (EditText)findViewById(R.id.textViewInfo);
			TextView textPstn = (TextView)findViewById(R.id.textPstn);
			if(currentCallWithState.dailout){
				textPstn.setText(currentCallWithState.pstn.substring(2));
			}
			else{
				textPstn.setText(currentCallWithState.pstn);
			}
			if(!editTextName.getText().toString().equals(currentCallWithState.name)){
				editTextName.setText(currentCallWithState.name);
			}
			if(!textViewInfo.getText().toString().equals(currentCallWithState.content)){
				textViewInfo.setText(currentCallWithState.content);
			}


		}

	}
	public void updateGsOperateButton(){
		ImageButton gs_btn_acptorhold =  findViewById(R.id.gs_btn_acptorhold);
		ImageButton gs_btn_liveorhold =  findViewById(R.id.gs_btn_liveorhold);
		ImageButton gs_btn_switch =  findViewById(R.id.gs_btn_switch);
		ImageButton gs_btn_hangup = findViewById(R.id.gs_btn_hangup);

		ImageButton gs_btn_lite_liveorhold = findViewById(R.id.gs_btn_lite_liveorhold);
		ImageButton gs_btn_lite_tolive = findViewById(R.id.gs_btn_lite_tolive);
		ImageButton gs_btn_lite_endlive = findViewById(R.id.gs_btn_lite_endlive);

		if(GlobalVariable.liteModel){
			gs_btn_acptorhold.setVisibility(View.GONE);
			gs_btn_liveorhold.setVisibility(View.GONE);
			gs_btn_switch.setVisibility(View.GONE);
			gs_btn_hangup.setVisibility(View.GONE);

			gs_btn_lite_liveorhold.setVisibility(View.VISIBLE);
			gs_btn_lite_tolive.setVisibility(View.VISIBLE);
			gs_btn_lite_endlive.setVisibility(View.VISIBLE);

			if(GlobalVariable.charactername.equals("broadcaster")){
				gs_btn_hangup.setVisibility(View.VISIBLE);
				gs_btn_lite_tolive.setVisibility(View.GONE);
			}
		}

		//模式代码1：单导播模式，0：无导播模式 2：多导播模式,3:仅导播模式
		if (GlobalVariable.charactername.equals("director")) {
			if (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)//无导播/仅导播
			{
				gs_btn_switch.setVisibility(View.GONE);
			} else if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2)//单导播/多导播
			{
				gs_btn_liveorhold.setVisibility(View.GONE);
			}
		} else {
			if (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)//无导播/仅导播
			{
				gs_btn_switch.setVisibility(View.GONE);
			} else if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2)//单导播/多导播
			{
				gs_btn_acptorhold.setVisibility(View.GONE);
			}
		}

		if(currentCallNum == 0 || currentCallWithState==null || currentCallWithState.syncshow || GlobalVariable.operatelock)
		{
			gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			if(GlobalVariable.charactername.equals("director"))
			{
				gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			}
			else
			{
				gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			}
			gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			gs_btn_acptorhold.setEnabled(false);
			gs_btn_liveorhold.setEnabled(false);
			gs_btn_switch.setEnabled(false);
			gs_btn_hangup.setEnabled(false);

			gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_idle));
			gs_btn_lite_liveorhold.setEnabled(false);
			gs_btn_lite_tolive.setEnabled(false);
			gs_btn_lite_endlive.setEnabled(false);
		}
		else if(currentCallWithState!=null) {
			//用TYPE非STATE进行判断
			if(GlobalVariable.charactername.equals("director"))
			{
				if(currentCallWithState.type == CALL_TYPE.DIALINIT)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));

					gs_btn_acptorhold.setEnabled(false);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(false);

					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));
					gs_btn_liveorhold.setEnabled(false);
					if(currentCallWithState.first_livehold && (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(false);

				}
				if(currentCallWithState.type == CALL_TYPE.NEWINIT)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);

					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_liveorhold.setEnabled(true);
					if(currentCallWithState.first_livehold && (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(false);
				}
				else if(currentCallWithState.type == CALL_TYPE.DAOBOACPT)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hold));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);

					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_liveorhold.setEnabled(true);
					if(currentCallWithState.first_livehold && (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(true);
					gs_btn_lite_endlive.setEnabled(false);
				}
				else if(currentCallWithState.type == CALL_TYPE.DAOBOHOLD)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(false);
					//导播保持的时候不能能直接直播
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					gs_btn_liveorhold.setEnabled(false);
					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3) {
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
						gs_btn_liveorhold.setEnabled(true);
						if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0 ){
							gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
							gs_btn_liveorhold.setEnabled(true);
						}
					}

					//lite no
				}
				else if(currentCallWithState.type == CALL_TYPE.WAITDAOBO)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));

					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);

					//lite no
				}
				else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(false);
					//等待主播接听可以直接直播
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_liveorhold.setEnabled(true);

					//lite no
				}
				else if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT
						||currentCallWithState.type == CALL_TYPE.ZHUBOHOLD
						||currentCallWithState.type == CALL_TYPE.LIVEACPT
						||currentCallWithState.type == CALL_TYPE.LIVEHOLD
				)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_acptorhold.setEnabled(false);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(false);

					//主播接听的时候可以直播
					if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
						gs_btn_liveorhold.setEnabled(true);
					}
					//主播保持的时候不能直播
					if(currentCallWithState.type == CALL_TYPE.ZHUBOHOLD){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));
						gs_btn_liveorhold.setEnabled(false);
					}
					if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold));
						gs_btn_liveorhold.setEnabled(true);
					}
					if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
						gs_btn_liveorhold.setEnabled(true);
					}
					//直播的时候可以接听，直播保持的时候可以接听(仅导播模式下)
					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)
					{
						gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
						gs_btn_acptorhold.setEnabled(true);

						if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
							gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold));
							gs_btn_liveorhold.setEnabled(true);
						}
						if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
							gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
							gs_btn_liveorhold.setEnabled(true);
						}
					}

					if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
						gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold));
						gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
						gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
						gs_btn_lite_liveorhold.setEnabled(true);
						gs_btn_lite_tolive.setEnabled(false);
						gs_btn_lite_endlive.setEnabled(true);
					}
					if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
						gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
						gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
						gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
						gs_btn_lite_liveorhold.setEnabled(true);
						gs_btn_lite_tolive.setEnabled(false);
						gs_btn_lite_endlive.setEnabled(true);
					}
				}
				else if(currentCallWithState.type == CALL_TYPE.ENDCALL)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_acptorhold.setEnabled(false);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(false);

					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));
					gs_btn_liveorhold.setEnabled(false);

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(false);
				}
			}
			else if(GlobalVariable.charactername.equals("broadcaster"))
			{
				if(currentCallWithState.type == CALL_TYPE.DIALINIT)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_acptorhold.setEnabled(false);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(false);
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));
					gs_btn_liveorhold.setEnabled(false);

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(false);
				}
				if(currentCallWithState.type == CALL_TYPE.WAITZHUBO)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));

					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);
					gs_btn_liveorhold.setEnabled(true);

					//20221018处理
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}
				}
				//处理呼出外线的情况
				else if(currentCallWithState.type == CALL_TYPE.DAOBOACPT)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hold));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);

					if(currentCallWithState.dailout){
						gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					}
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_liveorhold.setEnabled(true);
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}


					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(false);
				}
				if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hold));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);
					gs_btn_liveorhold.setEnabled(true);
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}
				}
				if(currentCallWithState.type == CALL_TYPE.ZHUBOHOLD)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);
					gs_btn_liveorhold.setEnabled(true);
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
						gs_btn_liveorhold.setEnabled(true);
					}
					if(currentCallWithState.dailout){
						gs_btn_switch.setEnabled(false);
						gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					}

					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3){
						gs_btn_switch.setEnabled(true);
						gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
						gs_btn_liveorhold.setEnabled(true);
						gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					}
                    if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
                        gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in));
                        gs_btn_liveorhold.setEnabled(true);
                    }
				}
				if(currentCallWithState.type == CALL_TYPE.LIVEACPT)
				{
					//直播的时候允许直接切回接听
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold));
					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);
					gs_btn_liveorhold.setEnabled(true);


					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_lite_liveorhold.setEnabled(true);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(true);
				}
				if(currentCallWithState.type == CALL_TYPE.LIVEHOLD)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));

					gs_btn_acptorhold.setEnabled(true);
					gs_btn_hangup.setEnabled(true);
					gs_btn_switch.setEnabled(true);
					gs_btn_liveorhold.setEnabled(true);

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out));
					gs_btn_lite_liveorhold.setEnabled(true);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(true);
				}
				else if(currentCallWithState.type == CALL_TYPE.ENDCALL)
				{
					gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
					gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_hangup_disable));
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));
					gs_btn_acptorhold.setEnabled(false);
					gs_btn_hangup.setEnabled(false);
					gs_btn_switch.setEnabled(false);
					gs_btn_liveorhold.setEnabled(false);

					gs_btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
					gs_btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
					gs_btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
					gs_btn_lite_liveorhold.setEnabled(false);
					gs_btn_lite_tolive.setEnabled(false);
					gs_btn_lite_endlive.setEnabled(false);
				}
			}
			if(currentCallWithState.wait_ackhold)
			{
				gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
				gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));

				gs_btn_acptorhold.setEnabled(false);//保持
				gs_btn_liveorhold.setEnabled(false);

			}
			if(currentCallWithState.wait_ackaccept)
			{
				Log.e("等待接听","未允许转接");
				if(GlobalVariable.charactername.equals("director")){
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
				}
				else{
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
				}
				gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_live_disable));

				gs_btn_switch.setEnabled(false);
				gs_btn_liveorhold.setEnabled(false);
			}
			if(currentCallWithState.live_lock && GlobalVariable.livelock){
				gs_btn_acptorhold.setEnabled(false);
				gs_btn_switch.setEnabled(false);
				gs_btn_liveorhold.setEnabled(false);
				gs_btn_hangup.setEnabled(true);

				gs_btn_acptorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_accept_disable));
				if(GlobalVariable.charactername.equals("director")){
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_in_disable));
				}
				else{
					gs_btn_switch.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_switch_out_disable));
				}
				gs_btn_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_livehold_disable));
				gs_btn_hangup.setImageDrawable(getResources().getDrawable(R.drawable.gs_btn_lock));
			}
		}
	}
	/**耦合器模式更新状态和号码**/
	public void updateCtInfoView(){
		Map<Integer,String> locationMap = new HashMap<>();

		LinearLayout ct_bkg_1 = findViewById(R.id.ct_state_bkg_1);
		LinearLayout ct_bkg_2 = findViewById(R.id.ct_state_bkg_2);
		LinearLayout ct_bkg_3 = findViewById(R.id.ct_state_bkg_3);
		LinearLayout ct_bkg_4 = findViewById(R.id.ct_state_bkg_4);
		LinearLayout ct_bkg_5 = findViewById(R.id.ct_state_bkg_5);
		LinearLayout ct_bkg_6 = findViewById(R.id.ct_state_bkg_6);

		TextView ctStateId1 = (TextView)findViewById(R.id.ct_state_id_1);
		TextView ctStateId2 = (TextView)findViewById(R.id.ct_state_id_2);
		TextView ctStateId3 = (TextView)findViewById(R.id.ct_state_id_3);
		TextView ctStateId4 = (TextView)findViewById(R.id.ct_state_id_4);
		TextView ctStateId5 = (TextView)findViewById(R.id.ct_state_id_5);
		TextView ctStateId6 = (TextView)findViewById(R.id.ct_state_id_6);

		TextView ctCallNum1 = (TextView)findViewById(R.id.ct_phone_num_1);
		TextView ctCallNum2 = (TextView)findViewById(R.id.ct_phone_num_2);
		TextView ctCallNum3 = (TextView)findViewById(R.id.ct_phone_num_3);
		TextView ctCallNum4 = (TextView)findViewById(R.id.ct_phone_num_4);
		TextView ctCallNum5 = (TextView)findViewById(R.id.ct_phone_num_5);
		TextView ctCallNum6 = (TextView)findViewById(R.id.ct_phone_num_6);

		TextView ctAcptTime1 = (TextView)findViewById(R.id.ct_acpt_time_1);
		TextView ctAcptTime2 = (TextView)findViewById(R.id.ct_acpt_time_2);
		TextView ctAcptTime3 = (TextView)findViewById(R.id.ct_acpt_time_3);
		TextView ctAcptTime4 = (TextView)findViewById(R.id.ct_acpt_time_4);
		TextView ctAcptTime5 = (TextView)findViewById(R.id.ct_acpt_time_5);
		TextView ctAcptTime6 = (TextView)findViewById(R.id.ct_acpt_time_6);

		TextView ctLiveTime1 = (TextView)findViewById(R.id.ct_live_time_1);
		TextView ctLiveTime2 = (TextView)findViewById(R.id.ct_live_time_2);
		TextView ctLiveTime3 = (TextView)findViewById(R.id.ct_live_time_3);
		TextView ctLiveTime4 = (TextView)findViewById(R.id.ct_live_time_4);
		TextView ctLiveTime5 = (TextView)findViewById(R.id.ct_live_time_5);
		TextView ctLiveTime6 = (TextView)findViewById(R.id.ct_live_time_6);

		GifImageView ctInitFlash1 = (GifImageView)findViewById(R.id.ct_init_flash_1);
		GifImageView ctInitFlash2 = (GifImageView)findViewById(R.id.ct_init_flash_2);
		GifImageView ctInitFlash3 = (GifImageView)findViewById(R.id.ct_init_flash_3);
		GifImageView ctInitFlash4 = (GifImageView)findViewById(R.id.ct_init_flash_4);
		GifImageView ctInitFlash5 = (GifImageView)findViewById(R.id.ct_init_flash_5);
		GifImageView ctInitFlash6 = (GifImageView)findViewById(R.id.ct_init_flash_6);

		TextView ctLineNumber1 = (TextView)findViewById(R.id.ct_line_number_1);
		TextView ctLineNumber2 = (TextView)findViewById(R.id.ct_line_number_2);
		TextView ctLineNumber3 = (TextView)findViewById(R.id.ct_line_number_3);
		TextView ctLineNumber4 = (TextView)findViewById(R.id.ct_line_number_4);
		TextView ctLineNumber5 = (TextView)findViewById(R.id.ct_line_number_5);
		TextView ctLineNumber6 = (TextView)findViewById(R.id.ct_line_number_6);

		LinearLayout ct_bkg = ct_bkg_1;
		TextView ct_stateid = ctStateId1;
		TextView ct_acpttime = ctAcptTime1;
		TextView ct_livetime = ctLiveTime1;
		TextView ct_callnum = ctCallNum1;
		GifImageView ct_init_flash = ctInitFlash1;
		TextView ct_line_number = ctLineNumber1;
		String line_number = GlobalVariable.ctNumberMapRevert.get(1) == null?"":GlobalVariable.ctNumberMapRevert.get(1);
		boolean line_busy = GlobalVariable.linebusy1;

		if(allCallinList.size() == 1)
		{
			int id = GlobalVariable.ctNumberMapAll.get(allCallinList.get(0).callednumber) == null?0:GlobalVariable.ctNumberMapAll.get(allCallinList.get(0).callednumber);
			GlobalVariable.ct_selected_index = id;
		}

		for(MyCallWithState callws : allCallinList) {
			//int id = allCallinList.indexOf(callws)+1;
			int id = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null?0:GlobalVariable.ctNumberMapAll.get(callws.callednumber);

			//标记该列已被占用
			locationMap.put(id,"1");
			//标记该列id对应List中的id
			GlobalVariable.ctIdMap.put(id,callws.id);

			if (id == 1) {
				ct_bkg = ct_bkg_1;
				ct_acpttime = ctAcptTime1;
				ct_livetime = ctLiveTime1;
				ct_callnum = ctCallNum1;
				ct_init_flash = ctInitFlash1;
				ct_line_number = ctLineNumber1;
				ct_stateid = ctStateId1;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(1)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(1);
					if (line_num.length() >= 4) {
						tail_num = line_num.substring(line_num.length() - 4);
					} else {
						tail_num = line_num;
					}
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 2) {
				ct_bkg = ct_bkg_2;
				ct_acpttime = ctAcptTime2;
				ct_livetime = ctLiveTime2;
				ct_callnum = ctCallNum2;
				ct_init_flash = ctInitFlash2;
				ct_line_number = ctLineNumber2;
				ct_stateid = ctStateId2;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(2)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(2);
					if (line_num.length() >= 4) {
						tail_num = line_num.substring(line_num.length() - 4);
					} else {
						tail_num = line_num;
					}
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 3) {
				ct_bkg = ct_bkg_3;
				ct_acpttime = ctAcptTime3;
				ct_livetime = ctLiveTime3;
				ct_callnum = ctCallNum3;
				ct_init_flash = ctInitFlash3;
				ct_line_number = ctLineNumber3;
				ct_stateid = ctStateId3;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(3)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(3);
					if (line_num.length() >= 4) {
						tail_num = line_num.substring(line_num.length() - 4);
					} else {
						tail_num = line_num;
					}
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 4) {
				ct_bkg = ct_bkg_4;
				ct_acpttime = ctAcptTime4;
				ct_livetime = ctLiveTime4;
				ct_callnum = ctCallNum4;
				ct_init_flash = ctInitFlash4;
				ct_line_number = ctLineNumber4;
				ct_stateid = ctStateId4;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(4)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(4);
					if (line_num.length() >= 4) {
						tail_num = line_num.substring(line_num.length() - 4);
					} else {
						tail_num = line_num;
					}
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 5) {
				ct_bkg = ct_bkg_5;
				ct_acpttime = ctAcptTime5;
				ct_livetime = ctLiveTime5;
				ct_callnum = ctCallNum5;
				ct_init_flash = ctInitFlash5;
				ct_line_number = ctLineNumber5;
				ct_stateid = ctStateId5;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(5)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(5);
					if (line_num.length() >= 4) {
						tail_num = line_num.substring(line_num.length() - 4);
					} else {
						tail_num = line_num;
					}
				}
				ct_line_number.setText(tail_num);
			}
			else if (id == 6) {
				ct_bkg = ct_bkg_6;
				ct_acpttime = ctAcptTime6;
				ct_livetime = ctLiveTime6;
				ct_callnum = ctCallNum6;
				ct_init_flash = ctInitFlash6;
				ct_line_number = ctLineNumber6;
				ct_stateid = ctStateId6;

				String line_num = "";
				String tail_num = "";
				if(GlobalVariable.ctNumberMapRevert.get(6)==null){
					tail_num = "";
				}
				else{
					line_num = GlobalVariable.ctNumberMapRevert.get(6);
					if (line_num.length() >= 4) {
						tail_num = line_num.substring(line_num.length() - 4);
					} else {
						tail_num = line_num;
					}
				}
				ct_line_number.setText(tail_num);
			}

			//选中列的数字标号红色显示
			if(GlobalVariable.ct_selected_index == id)
				ct_stateid.setTextColor(Color.RED);
			else
				ct_stateid.setTextColor(Color.WHITE);
			//在每次显示更新前先屏蔽闪烁GIF
			ct_init_flash.setVisibility(View.GONE);

			if(callws.type == CALL_TYPE.DIALINIT)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_dailout));
				ct_acpttime.setTextColor(getResources().getColor(R.color.black));
				ct_livetime.setTextColor(getResources().getColor(R.color.black));
				ct_callnum.setTextColor(getResources().getColor(R.color.black));
			}
			else if(callws.type == CALL_TYPE.NEWINIT)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_init));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_green1));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_green1));
				ct_init_flash.setVisibility(View.VISIBLE);
				if(callws.dailout){
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_dailout));
					ct_acpttime.setTextColor(getResources().getColor(R.color.black));
					ct_livetime.setTextColor(getResources().getColor(R.color.black));
					ct_callnum.setTextColor(getResources().getColor(R.color.black));
				}
			}
			else if(callws.type == CALL_TYPE.WAITDAOBO)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_revert));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_green1));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_green1));
			}
			else if(callws.type == CALL_TYPE.DAOBOACPT)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_accept));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_green2));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_green2));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_green2));
				if(callws.dailout){
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_dailout));
					ct_acpttime.setTextColor(getResources().getColor(R.color.black));
					ct_livetime.setTextColor(getResources().getColor(R.color.black));
					ct_callnum.setTextColor(getResources().getColor(R.color.black));
				}
			}
			else if(callws.type == CALL_TYPE.DAOBOHOLD)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_hold));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_yellow));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_yellow));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_yellow));
			}
			else if(callws.type == CALL_TYPE.WAITZHUBO)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_zhubowait));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_blue));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_blue));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_blue));
				ct_init_flash.setVisibility(View.VISIBLE);
				if(GlobalVariable.charactername.equals("broadcaster") && GlobalVariable.sipmodel == 0){
					//无导播模式下主播不显示等待主播而显示新呼入
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_init));
					ct_acpttime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_callnum.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_init_flash.setVisibility(View.VISIBLE);
				}
			}
			else if(callws.type == CALL_TYPE.ZHUBOACPT)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_accept));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_green2));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_green2));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_green2));
				if(GlobalVariable.charactername.equals("broadcaster") && (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2)){
					//单导播和多导播在转接回导播的时候
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_revert));
					ct_acpttime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_livetime.setTextColor(getResources().getColor(R.color.ct_green1));
					ct_callnum.setTextColor(getResources().getColor(R.color.ct_green1));
				}
			}
			else if(callws.type == CALL_TYPE.ZHUBOHOLD)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_hold));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_yellow));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_yellow));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_yellow));
			}
			else if(callws.type == CALL_TYPE.LIVEACPT)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_live));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_red));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_red));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_red));
			}
			else if(callws.type == CALL_TYPE.LIVEHOLD)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_livehold));
				ct_acpttime.setTextColor(getResources().getColor(R.color.ct_purple));
				ct_livetime.setTextColor(getResources().getColor(R.color.ct_purple));
				ct_callnum.setTextColor(getResources().getColor(R.color.ct_purple));
				if(callws.first_livehold && (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_zhubowait));
					ct_acpttime.setTextColor(getResources().getColor(R.color.ct_blue));
					ct_livetime.setTextColor(getResources().getColor(R.color.ct_blue));
					ct_callnum.setTextColor(getResources().getColor(R.color.ct_blue));
				}
			}
			else if(callws.type == CALL_TYPE.ENDCALL)
			{
				ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_hangup));
			}
		}
		for(int j=1;j<=6;j++){
			//所有未被处理的电话
			if(locationMap.get(j)==null) {
				int id = j;
				if(id == 1){
					ct_bkg = ct_bkg_1;
					ct_acpttime = ctAcptTime1;
					ct_livetime = ctLiveTime1;
					ct_callnum = ctCallNum1;
					ct_init_flash = ctInitFlash1;
					line_number = GlobalVariable.ctNumberMapRevert.get(1) == null?"":GlobalVariable.ctNumberMapRevert.get(1);
					line_busy = GlobalVariable.linebusy1;
					ct_stateid = ctStateId1;
				}
				else if(id == 2){
					ct_bkg = ct_bkg_2;
					ct_acpttime = ctAcptTime2;
					ct_livetime = ctLiveTime2;
					ct_callnum = ctCallNum2;
					ct_init_flash = ctInitFlash2;
					line_number = GlobalVariable.ctNumberMapRevert.get(2) == null?"":GlobalVariable.ctNumberMapRevert.get(2);
					line_busy = GlobalVariable.linebusy2;
					ct_stateid = ctStateId2;
				}
				else if(id == 3){
					ct_bkg = ct_bkg_3;
					ct_acpttime = ctAcptTime3;
					ct_livetime = ctLiveTime3;
					ct_callnum = ctCallNum3;
					ct_init_flash = ctInitFlash3;
					line_number = GlobalVariable.ctNumberMapRevert.get(3) == null?"":GlobalVariable.ctNumberMapRevert.get(3);
					line_busy = GlobalVariable.linebusy3;
					ct_stateid = ctStateId3;
				}
				else if(id == 4){
					ct_bkg = ct_bkg_4;
					ct_acpttime = ctAcptTime4;
					ct_livetime = ctLiveTime4;
					ct_callnum = ctCallNum4;
					ct_init_flash = ctInitFlash4;
					line_number = GlobalVariable.ctNumberMapRevert.get(4) == null?"":GlobalVariable.ctNumberMapRevert.get(4);
					line_busy = GlobalVariable.linebusy4;
					ct_stateid = ctStateId4;
				}
				else if(id == 5){
					ct_bkg = ct_bkg_5;
					ct_acpttime = ctAcptTime5;
					ct_livetime = ctLiveTime5;
					ct_callnum = ctCallNum5;
					ct_init_flash = ctInitFlash5;
					line_number = GlobalVariable.ctNumberMapRevert.get(5) == null?"":GlobalVariable.ctNumberMapRevert.get(5);
					line_busy = GlobalVariable.linebusy5;
					ct_stateid = ctStateId5;
				}
				else if(id == 6){
					ct_bkg = ct_bkg_6;
					ct_acpttime = ctAcptTime6;
					ct_livetime = ctLiveTime6;
					ct_callnum = ctCallNum6;
					ct_init_flash = ctInitFlash6;
					line_number = GlobalVariable.ctNumberMapRevert.get(6) == null?"":GlobalVariable.ctNumberMapRevert.get(6);
					line_busy = GlobalVariable.linebusy6;
					ct_stateid = ctStateId6;
				}
				ct_init_flash.setVisibility(View.GONE);

				//选中列的数字标号红色显示
				if(GlobalVariable.ct_selected_index == id)
					ct_stateid.setTextColor(Color.RED);
				else
					ct_stateid.setTextColor(Color.WHITE);

				if(line_number.equals("")){
					ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_idle));
					ct_acpttime.setTextColor(getResources().getColor(R.color.black));
					ct_livetime.setTextColor(getResources().getColor(R.color.black));
					ct_callnum.setTextColor(getResources().getColor(R.color.black));
				}
				else {
					if(GlobalVariable.busyall == 1) {
						//全部置忙
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_busy));
						ct_acpttime.setTextColor(getResources().getColor(R.color.ct_gray));
						ct_livetime.setTextColor(getResources().getColor(R.color.ct_gray));
						ct_callnum.setTextColor(getResources().getColor(R.color.ct_gray));
					}
					else if(GlobalVariable.busyall == 0){
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_idle));
						ct_acpttime.setTextColor(getResources().getColor(R.color.black));
						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.black));
					}
					else if(line_busy){
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_busy));
						ct_acpttime.setTextColor(getResources().getColor(R.color.ct_gray));
						ct_livetime.setTextColor(getResources().getColor(R.color.ct_gray));
						ct_callnum.setTextColor(getResources().getColor(R.color.ct_gray));
					}
					else {
						ct_bkg.setBackground(getResources().getDrawable(R.drawable.ct_state_idle));
						ct_acpttime.setTextColor(getResources().getColor(R.color.black));
						ct_livetime.setTextColor(getResources().getColor(R.color.black));
						ct_callnum.setTextColor(getResources().getColor(R.color.black));
					}

				}
			}

		}
	}
	/**耦合器模式更新按钮**/
	public void updateCtOperateButton(){

		Map<Integer,String> locationMap = new HashMap<>();
		ImageButton btn_ct_1_1 = findViewById(R.id.ct_1_1);
		ImageButton btn_ct_1_2 = findViewById(R.id.ct_1_2);
		ImageButton btn_ct_1_3 = findViewById(R.id.ct_1_3);
		ImageButton btn_ct_1_4 = findViewById(R.id.ct_1_4);

		ImageButton btn_ct_2_1 = findViewById(R.id.ct_2_1);
		ImageButton btn_ct_2_2 = findViewById(R.id.ct_2_2);
		ImageButton btn_ct_2_3 = findViewById(R.id.ct_2_3);
		ImageButton btn_ct_2_4 = findViewById(R.id.ct_2_4);

		ImageButton btn_ct_3_1 = findViewById(R.id.ct_3_1);
		ImageButton btn_ct_3_2 = findViewById(R.id.ct_3_2);
		ImageButton btn_ct_3_3 = findViewById(R.id.ct_3_3);
		ImageButton btn_ct_3_4 = findViewById(R.id.ct_3_4);

		ImageButton btn_ct_4_1 = findViewById(R.id.ct_4_1);
		ImageButton btn_ct_4_2 = findViewById(R.id.ct_4_2);
		ImageButton btn_ct_4_3 = findViewById(R.id.ct_4_3);
		ImageButton btn_ct_4_4 = findViewById(R.id.ct_4_4);

		ImageButton btn_ct_5_1 = findViewById(R.id.ct_5_1);
		ImageButton btn_ct_5_2 = findViewById(R.id.ct_5_2);
		ImageButton btn_ct_5_3 = findViewById(R.id.ct_5_3);
		ImageButton btn_ct_5_4 = findViewById(R.id.ct_5_4);

		ImageButton btn_ct_6_1 = findViewById(R.id.ct_6_1);
		ImageButton btn_ct_6_2 = findViewById(R.id.ct_6_2);
		ImageButton btn_ct_6_3 = findViewById(R.id.ct_6_3);
		ImageButton btn_ct_6_4 = findViewById(R.id.ct_6_4);


		ImageButton btn_ct_1_5 = findViewById(R.id.ct_1_5);
		ImageButton btn_ct_1_6 = findViewById(R.id.ct_1_6);
		ImageButton btn_ct_1_7 = findViewById(R.id.ct_1_7);
		ImageButton btn_ct_2_5 = findViewById(R.id.ct_2_5);
		ImageButton btn_ct_2_6 = findViewById(R.id.ct_2_6);
		ImageButton btn_ct_2_7 = findViewById(R.id.ct_2_7);
		ImageButton btn_ct_3_5 = findViewById(R.id.ct_3_5);
		ImageButton btn_ct_3_6 = findViewById(R.id.ct_3_6);
		ImageButton btn_ct_3_7 = findViewById(R.id.ct_3_7);
		ImageButton btn_ct_4_5 = findViewById(R.id.ct_4_5);
		ImageButton btn_ct_4_6 = findViewById(R.id.ct_4_6);
		ImageButton btn_ct_4_7 = findViewById(R.id.ct_4_7);
		ImageButton btn_ct_5_5 = findViewById(R.id.ct_5_5);
		ImageButton btn_ct_5_6 = findViewById(R.id.ct_5_6);
		ImageButton btn_ct_5_7 = findViewById(R.id.ct_5_7);
		ImageButton btn_ct_6_5 = findViewById(R.id.ct_6_5);
		ImageButton btn_ct_6_6 = findViewById(R.id.ct_6_6);
		ImageButton btn_ct_6_7 = findViewById(R.id.ct_6_7);

		//模式代码1：单导播模式，0：无导播模式 2：多导播模式,3:仅导播模式
		if(GlobalVariable.liteModel){
			btn_ct_1_1.setVisibility(View.GONE);
			btn_ct_1_2.setVisibility(View.GONE);
			btn_ct_1_3.setVisibility(View.GONE);
			btn_ct_1_4.setVisibility(View.GONE);
			btn_ct_2_1.setVisibility(View.GONE);
			btn_ct_2_2.setVisibility(View.GONE);
			btn_ct_2_3.setVisibility(View.GONE);
			btn_ct_2_4.setVisibility(View.GONE);
			btn_ct_3_1.setVisibility(View.GONE);
			btn_ct_3_2.setVisibility(View.GONE);
			btn_ct_3_3.setVisibility(View.GONE);
			btn_ct_3_4.setVisibility(View.GONE);
			btn_ct_4_1.setVisibility(View.GONE);
			btn_ct_4_2.setVisibility(View.GONE);
			btn_ct_4_3.setVisibility(View.GONE);
			btn_ct_4_4.setVisibility(View.GONE);
			btn_ct_5_1.setVisibility(View.GONE);
			btn_ct_5_2.setVisibility(View.GONE);
			btn_ct_5_3.setVisibility(View.GONE);
			btn_ct_5_4.setVisibility(View.GONE);
			btn_ct_6_1.setVisibility(View.GONE);
			btn_ct_6_2.setVisibility(View.GONE);
			btn_ct_6_3.setVisibility(View.GONE);
			btn_ct_6_4.setVisibility(View.GONE);

			btn_ct_1_5.setVisibility(View.VISIBLE);
			btn_ct_1_6.setVisibility(View.VISIBLE);
			btn_ct_1_7.setVisibility(View.VISIBLE);
			btn_ct_2_5.setVisibility(View.VISIBLE);
			btn_ct_2_6.setVisibility(View.VISIBLE);
			btn_ct_2_7.setVisibility(View.VISIBLE);
			btn_ct_3_5.setVisibility(View.VISIBLE);
			btn_ct_3_6.setVisibility(View.VISIBLE);
			btn_ct_3_7.setVisibility(View.VISIBLE);
			btn_ct_4_5.setVisibility(View.VISIBLE);
			btn_ct_4_6.setVisibility(View.VISIBLE);
			btn_ct_4_7.setVisibility(View.VISIBLE);
			btn_ct_5_5.setVisibility(View.VISIBLE);
			btn_ct_5_6.setVisibility(View.VISIBLE);
			btn_ct_5_7.setVisibility(View.VISIBLE);
			btn_ct_6_5.setVisibility(View.VISIBLE);
			btn_ct_6_6.setVisibility(View.VISIBLE);
			btn_ct_6_7.setVisibility(View.VISIBLE);


			if(GlobalVariable.charactername.equals("broadcaster")){
				btn_ct_1_4.setVisibility(View.VISIBLE);
				btn_ct_2_4.setVisibility(View.VISIBLE);
				btn_ct_3_4.setVisibility(View.VISIBLE);
				btn_ct_4_4.setVisibility(View.VISIBLE);
				btn_ct_5_4.setVisibility(View.VISIBLE);
				btn_ct_6_4.setVisibility(View.VISIBLE);

				btn_ct_1_6.setVisibility(View.GONE);
				btn_ct_2_6.setVisibility(View.GONE);
				btn_ct_3_6.setVisibility(View.GONE);
				btn_ct_4_6.setVisibility(View.GONE);
				btn_ct_5_6.setVisibility(View.GONE);
				btn_ct_6_6.setVisibility(View.GONE);
			}
		}
		else if(GlobalVariable.charactername.equals("director")){
			if(GlobalVariable.sipmodel==0||GlobalVariable.sipmodel==3)//无导播/仅导播
			{
				btn_ct_1_2.setVisibility(View.GONE);
				btn_ct_2_2.setVisibility(View.GONE);
				btn_ct_3_2.setVisibility(View.GONE);
				btn_ct_4_2.setVisibility(View.GONE);
				btn_ct_5_2.setVisibility(View.GONE);
				btn_ct_6_2.setVisibility(View.GONE);
			}
			else if(GlobalVariable.sipmodel==1||GlobalVariable.sipmodel==2)//单导播/多导播
			{
				btn_ct_1_3.setVisibility(View.GONE);
				btn_ct_2_3.setVisibility(View.GONE);
				btn_ct_3_3.setVisibility(View.GONE);
				btn_ct_4_3.setVisibility(View.GONE);
				btn_ct_5_3.setVisibility(View.GONE);
				btn_ct_6_3.setVisibility(View.GONE);
			}
		}
		else{
			if(GlobalVariable.sipmodel==0||GlobalVariable.sipmodel==3)//无导播/仅导播
			{
				btn_ct_1_2.setVisibility(View.GONE);
				btn_ct_2_2.setVisibility(View.GONE);
				btn_ct_3_2.setVisibility(View.GONE);
				btn_ct_4_2.setVisibility(View.GONE);
				btn_ct_5_2.setVisibility(View.GONE);
				btn_ct_6_2.setVisibility(View.GONE);
			}
			else if(GlobalVariable.sipmodel==1||GlobalVariable.sipmodel==2)//单导播/多导播
			{
				btn_ct_1_1.setVisibility(View.GONE);
				btn_ct_2_1.setVisibility(View.GONE);
				btn_ct_3_1.setVisibility(View.GONE);
				btn_ct_4_1.setVisibility(View.GONE);
				btn_ct_5_1.setVisibility(View.GONE);
				btn_ct_6_1.setVisibility(View.GONE);

			}
		}

		ImageButton btn1 = btn_ct_1_1;;
		ImageButton btn2 = btn_ct_1_2;
		ImageButton btn3 = btn_ct_1_3;
		ImageButton btn4 = btn_ct_1_4;
		ImageButton btn5 = btn_ct_1_5;
		ImageButton btn6 = btn_ct_1_6;
		ImageButton btn7 = btn_ct_1_7;


		for(MyCallWithState callws : allCallinList){

			//int id = allCallinList.indexOf(callws)+1;
			int id = GlobalVariable.ctNumberMapAll.get(callws.callednumber)==null?0:GlobalVariable.ctNumberMapAll.get(callws.callednumber);
			//标记该列已被占用
			locationMap.put(id,"1");
			//标记该列id对应List中的id
			GlobalVariable.ctIdMap.put(id,callws.id);

			if(id == 1){
				btn1 = btn_ct_1_1;
				btn2 = btn_ct_1_2;
				btn3 = btn_ct_1_3;
				btn4 = btn_ct_1_4;

				btn5 = btn_ct_1_5;
				btn6 = btn_ct_1_6;
				btn7 = btn_ct_1_7;
			}
			else if(id == 2){
				btn1 = btn_ct_2_1;
				btn2 = btn_ct_2_2;
				btn3 = btn_ct_2_3;
				btn4 = btn_ct_2_4;

				btn5 = btn_ct_2_5;
				btn6 = btn_ct_2_6;
				btn7 = btn_ct_2_7;
			}
			else if(id == 3){
				btn1 = btn_ct_3_1;
				btn2 = btn_ct_3_2;
				btn3 = btn_ct_3_3;
				btn4 = btn_ct_3_4;

				btn5 = btn_ct_3_5;
				btn6 = btn_ct_3_6;
				btn7 = btn_ct_3_7;
			}
			else if(id == 4){
				btn1 = btn_ct_4_1;
				btn2 = btn_ct_4_2;
				btn3 = btn_ct_4_3;
				btn4 = btn_ct_4_4;

				btn5 = btn_ct_4_5;
				btn6 = btn_ct_4_6;
				btn7 = btn_ct_4_7;
			}
			else if(id == 5){
				btn1 = btn_ct_5_1;
				btn2 = btn_ct_5_2;
				btn3 = btn_ct_5_3;
				btn4 = btn_ct_5_4;

				btn5 = btn_ct_5_5;
				btn6 = btn_ct_5_6;
				btn7 = btn_ct_5_7;
			}
			else if(id == 6){
				btn1 = btn_ct_6_1;
				btn2 = btn_ct_6_2;
				btn3 = btn_ct_6_3;
				btn4 = btn_ct_6_4;

				btn5 = btn_ct_6_5;
				btn6 = btn_ct_6_6;
				btn7 = btn_ct_6_7;
			}

			if(GlobalVariable.charactername.equals("director"))
			{
				if(GlobalVariable.operatelock){
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_disable));

					btn1.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
					btn4.setEnabled(false);
				}
				else if(callws.syncshow){
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_disable));

					btn1.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
					btn4.setEnabled(false);
				}
				else if(callws.type == CALL_TYPE.DIALINIT)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in));

					btn1.setEnabled(false);
					btn4.setEnabled(true);
					btn2.setEnabled(false);

					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn3.setEnabled(false);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_us));
					}

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));

				}
				else if(callws.type == CALL_TYPE.NEWINIT)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);


					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in));

					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);
					//
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}

					//先进播出保持，再进播出状态
					if(callws.first_livehold && GlobalVariable.sipmodel==3)
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));

				}
				else if(callws.type == CALL_TYPE.DAOBOACPT)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_us));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(true);
					btn7.setEnabled(false);


					btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in));

					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);

					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}
					//先进播出保持，再进播出状态
					if(callws.first_livehold && GlobalVariable.sipmodel==3)
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));

				}
				else if(callws.type == CALL_TYPE.DAOBOHOLD)
				{
					//LITE模式下无此状态，不处理btn567

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));

					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(false);
					//导播保持的时候不能能直接直播
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn3.setEnabled(false);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
					}
					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3) {
						if(callws.selected_line_btn) {
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
						}else{
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
						}
						btn3.setEnabled(true);

						//先进播出保持，再进播出状态
						if(callws.first_livehold && GlobalVariable.sipmodel==3)
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}


				}
				else if(callws.type == CALL_TYPE.WAITDAOBO)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);


					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in));

					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_us));
					}
				}
				else if(callws.type == CALL_TYPE.WAITZHUBO)
				{
					//LITE模式下无此状态，不处理btn567

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(false);

					//等待主播接听可以直接直播
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));

					}
				}
				else if(callws.type == CALL_TYPE.ZHUBOACPT
						||callws.type == CALL_TYPE.ZHUBOHOLD
						||callws.type == CALL_TYPE.LIVEACPT
						||callws.type == CALL_TYPE.LIVEHOLD
				)
				{
					if(callws.type == CALL_TYPE.LIVEACPT){
						btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_us));
						btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
						btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						btn5.setEnabled(true);
						btn6.setEnabled(false);
						btn7.setEnabled(true);
					}
					else if(callws.type == CALL_TYPE.LIVEHOLD){
						btn5.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
						btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
						btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
						btn5.setEnabled(true);
						btn6.setEnabled(false);
						btn7.setEnabled(true);
					}

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn1.setEnabled(false);
					btn4.setEnabled(true);
					btn2.setEnabled(false);


					if(callws.selected_line_btn){
						//选中列深色
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
					}
					else{
						//非选中列浅色
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
					}

					//主播接听的时候可以直播
					if(callws.type == CALL_TYPE.ZHUBOACPT){
						if(callws.selected_line_btn) {
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
						}else{
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
						}
						btn3.setEnabled(true);
					}
					//主播保持的时候不能直播
					if(callws.type == CALL_TYPE.ZHUBOHOLD){
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
						btn3.setEnabled(false);
					}
					if(callws.type == CALL_TYPE.LIVEACPT){
						if(callws.selected_line_btn) {
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold_sc));
						}else{
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold_us));
						}
						btn3.setEnabled(true);
					}
					if(callws.type == CALL_TYPE.LIVEHOLD){
						if(callws.selected_line_btn) {
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
						}else {
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
						}
						btn3.setEnabled(true);
					}
					//直播的时候可以接听，直播保持的时候可以接听(仅导播模式下)
					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)
					{
						if(callws.selected_line_btn) {
							btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						}else{
							btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						}
						btn1.setEnabled(true);

						if(callws.type == CALL_TYPE.LIVEACPT){
							if(callws.selected_line_btn) {
								btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold_sc));
							}
							else{
								btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold_us));
							}
							btn3.setEnabled(true);
						}
						if(callws.type == CALL_TYPE.LIVEHOLD){
							if(callws.selected_line_btn) {
								btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
							}
							else{
								btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
							}
							btn3.setEnabled(true);
						}
					}
				}

				else if(callws.type == CALL_TYPE.ENDCALL)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);


					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn1.setEnabled(false);
					btn4.setEnabled(true);
					btn2.setEnabled(false);

					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn3.setEnabled(false);
				}

				if(callws.live_lock && GlobalVariable.livelock){
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_lock));

					btn1.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
					btn4.setEnabled(true);
				}
			}
			else if(GlobalVariable.charactername.equals("broadcaster"))
			{
				if(GlobalVariable.operatelock){
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_disable));

					btn1.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
					btn4.setEnabled(false);
				}
				else if(callws.syncshow){
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_disable));

					btn1.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
					btn4.setEnabled(false);
				}

				else if(callws.type == CALL_TYPE.DIALINIT)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));

					btn1.setEnabled(false);
					btn4.setEnabled(true);
					btn2.setEnabled(false);

					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn3.setEnabled(false);

					if(callws.selected_line_btn){
						//选中列深色
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
					}
					else{
						//非选中列浅色
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
					}
				}
				else if(callws.type == CALL_TYPE.WAITZHUBO)
				{
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}

					if(callws.first_livehold && GlobalVariable.sipmodel == 0)//仅在无导播模式下，需要先进播出保持再进直播
					{
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}

				}
				//处理呼出外线的情况
				else if(callws.type == CALL_TYPE.DAOBOACPT)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in));
					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);

					if(callws.dailout){
						if(callws.selected_line_btn) {
							btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_sc));
						}
						else{
							btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						}
					}
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}
					if(callws.first_livehold && GlobalVariable.sipmodel == 0)//仅在无导播模式下，需要先进播出保持再进直播
					{
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}
				}
				else if(callws.type == CALL_TYPE.ZHUBOACPT)
				{
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_hold_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}

					if(callws.first_livehold && GlobalVariable.sipmodel == 0)//仅在无导播模式下，需要先进播出保持再进直播
					{
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}

				}
				else if(callws.type == CALL_TYPE.ZHUBOHOLD)
				{
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}

					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3){
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));
						if(callws.selected_line_btn){
							//选中列深色
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));
						}
						else{
							//非选中列浅色
							btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
						}

						btn3.setEnabled(true);
					}
					if(callws.first_livehold && GlobalVariable.sipmodel == 0)//仅在无导播模式下，需要先进播出保持再进直播
					{
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_sc));
					}
				}
				else if(callws.type == CALL_TYPE.LIVEACPT)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_us));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
					btn5.setEnabled(true);
					btn6.setEnabled(false);
					btn7.setEnabled(true);

					//直播的时候允许直接切回接听
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold));
					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold_sc));
					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_livehold_us));
					}
				}
				else if(callws.type == CALL_TYPE.LIVEHOLD)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
					btn5.setEnabled(true);
					btn6.setEnabled(false);
					btn7.setEnabled(true);

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live));

					btn1.setEnabled(true);
					btn4.setEnabled(true);
					btn2.setEnabled(true);
					btn3.setEnabled(true);

					if(callws.selected_line_btn){
						//选中列深色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_sc));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_sc));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_sc));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_sc));

					}
					else{
						//非选中列浅色
						btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_us));
						btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_us));
						btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_us));
						btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_us));
					}
				}
				else if(callws.type == CALL_TYPE.ENDCALL)
				{
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_disable));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_disable));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn5.setEnabled(false);
					btn6.setEnabled(false);
					btn7.setEnabled(false);

					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn1.setEnabled(false);
					btn4.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
				}

				if(callws.live_lock && GlobalVariable.livelock){
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_disable));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_disable));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_disable));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_lock));

					btn1.setEnabled(false);
					btn2.setEnabled(false);
					btn3.setEnabled(false);
					btn4.setEnabled(true);
				}
			}
			if(callws.wait_ackhold)
			{
				btn1.setEnabled(false);//保持
			}
			if(callws.wait_ackaccept)
			{
				Log.e("等待接听","未允许转接");
				btn2.setEnabled(false);
				btn3.setEnabled(false);
			}
		}

		for(int j=1;j<=6;j++){
			//所有未被处理的电话
			if(locationMap.get(j)==null) {
				int id = j;
				if(id == 1){
					btn1 = btn_ct_1_1;
					btn2 = btn_ct_1_2;
					btn3 = btn_ct_1_3;
					btn4 = btn_ct_1_4;
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_idle_1));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_1));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_idle_1));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_idle_1));

					btn5 = btn_ct_1_5;
					btn6 = btn_ct_1_6;
					btn7 = btn_ct_1_7;
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_idle_1));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_1));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_idle_1));
				}
				else if(id == 2){
					btn1 = btn_ct_2_1;
					btn2 = btn_ct_2_2;
					btn3 = btn_ct_2_3;
					btn4 = btn_ct_2_4;
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_idle_2));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_2));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_idle_2));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_idle_2));

					btn5 = btn_ct_2_5;
					btn6 = btn_ct_2_6;
					btn7 = btn_ct_2_7;
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_idle_2));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_2));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_idle_2));
				}
				else if(id == 3){
					btn1 = btn_ct_3_1;
					btn2 = btn_ct_3_2;
					btn3 = btn_ct_3_3;
					btn4 = btn_ct_3_4;
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_idle_3));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_3));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_idle_3));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_idle_3));

					btn5 = btn_ct_3_5;
					btn6 = btn_ct_3_6;
					btn7 = btn_ct_3_7;
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_idle_3));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_3));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_idle_3));
				}
				else if(id == 4){
					btn1 = btn_ct_4_1;
					btn2 = btn_ct_4_2;
					btn3 = btn_ct_4_3;
					btn4 = btn_ct_4_4;
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_idle_4));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_4));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_idle_4));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_idle_4));

					btn5 = btn_ct_4_5;
					btn6 = btn_ct_4_6;
					btn7 = btn_ct_4_7;
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_idle_4));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_4));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_idle_4));
				}
				else if(id == 5){
					btn1 = btn_ct_5_1;
					btn2 = btn_ct_5_2;
					btn3 = btn_ct_5_3;
					btn4 = btn_ct_5_4;
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_idle_5));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_5));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_idle_5));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_idle_5));

					btn5 = btn_ct_5_5;
					btn6 = btn_ct_5_6;
					btn7 = btn_ct_5_7;
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_idle_5));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_5));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_idle_5));
				}
				else if(id == 6){
					btn1 = btn_ct_6_1;
					btn2 = btn_ct_6_2;
					btn3 = btn_ct_6_3;
					btn4 = btn_ct_6_4;
					btn1.setBackground(getResources().getDrawable(R.drawable.ct_accept_idle_6));
					btn2.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_6));
					btn3.setBackground(getResources().getDrawable(R.drawable.ct_live_idle_6));
					btn4.setBackground(getResources().getDrawable(R.drawable.ct_hangup_idle_6));

					btn5 = btn_ct_6_5;
					btn6 = btn_ct_6_6;
					btn7 = btn_ct_6_7;
					btn5.setBackground(getResources().getDrawable(R.drawable.ct_livehold_idle_6));
					btn6.setBackground(getResources().getDrawable(R.drawable.ct_switch_in_idle_6));
					btn7.setBackground(getResources().getDrawable(R.drawable.ct_switch_out_idle_6));
				}

				btn1.setEnabled(false);
				btn2.setEnabled(false);
				btn3.setEnabled(false);
				btn4.setEnabled(false);
				btn5.setEnabled(false);
				btn6.setEnabled(false);
				btn7.setEnabled(false);
			}
		}

//		btn1.setBackground(getResources().getDrawable(R.drawable.ct_test_idle));
//		btn2.setBackground(getResources().getDrawable(R.drawable.ct_test_idle));
//		btn3.setBackground(getResources().getDrawable(R.drawable.ct_test_idle));
//		btn4.setBackground(getResources().getDrawable(R.drawable.ct_test_idle));
	}
	/**右侧操作按钮根据权限改变刷新可操作状态**/
	@RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
	public void updateOperateButton(){
		ImageButton btCN1 = findViewById(R.id.buttonCN1);
		Button btCN2 = findViewById(R.id.buttonCN2);
		ImageButton btCN3 = findViewById(R.id.buttonCN3);
		ImageButton btCN4 = findViewById(R.id.buttonCN4);
		ImageButton btCN5 = findViewById(R.id.buttonCN5);
		ImageButton btCN6 = findViewById(R.id.buttonCN6);

		ImageButton btn_lite_liveorhold = findViewById(R.id.btn_lite_liveorhold);
		ImageButton btn_lite_tolive = findViewById(R.id.btn_lite_tolive);
		ImageButton btn_lite_endlive = findViewById(R.id.btn_lite_endlive);

		if(GlobalVariable.liteModel){
			btCN1.setVisibility(View.GONE);
			btCN4.setVisibility(View.GONE);
			btCN5.setVisibility(View.GONE);
			btCN6.setVisibility(View.GONE);

			btn_lite_liveorhold.setVisibility(View.VISIBLE);
			btn_lite_tolive.setVisibility(View.VISIBLE);
			btn_lite_endlive.setVisibility(View.VISIBLE);

			if(GlobalVariable.charactername.equals("broadcaster")){
				btCN4.setVisibility(View.VISIBLE);
				btn_lite_tolive.setVisibility(View.GONE);
			}
		}

		//模式代码1：单导播模式，0：无导播模式 2：多导播模式,3:仅导播模式
		if (GlobalVariable.charactername.equals("director")) {
			if (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)//无导播/仅导播
			{
				btCN5.setVisibility(View.GONE);
			} else if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2)//单导播/多导播
			{
				btCN6.setVisibility(View.GONE);
			}
		} else {
			if (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)//无导播/仅导播
			{
				btCN5.setVisibility(View.GONE);
			} else if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2)//单导播/多导播
			{
				btCN1.setVisibility(View.GONE);
			}
		}


		if(currentCallNum == 0 || currentCallWithState==null || currentCallWithState.syncshow || GlobalVariable.operatelock)
		{
			btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
			btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_disable));
			if(GlobalVariable.charactername.equals("director"))
			{
				btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
			}
			else
			{
				btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
			}

			btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
			btCN1.setEnabled(false);
			btCN4.setEnabled(false);
			btCN5.setEnabled(false);
			btCN6.setEnabled(false);

			btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));
			btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
			btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
			btn_lite_liveorhold.setEnabled(false);
			btn_lite_tolive.setEnabled(false);
			btn_lite_endlive.setEnabled(false);

		}
		else if(currentCallWithState!=null) {
			//用TYPE非STATE进行判断
			if(GlobalVariable.charactername.equals("director"))
			{
				if(currentCallWithState.type == CALL_TYPE.DIALINIT)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));

					btCN1.setEnabled(false);
					btCN4.setEnabled(true);
					btCN5.setEnabled(false);

					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btCN6.setEnabled(false);
					if(currentCallWithState.first_livehold&& (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						btCN6.setEnabled(true);
						if(GlobalVariable.isDL1018){

							btCN6.setEnabled(false);
							btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
						}
					}

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(false);
				}
				if(currentCallWithState.type == CALL_TYPE.NEWINIT)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));

					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);
					//
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN6.setEnabled(true);
					if(currentCallWithState.first_livehold&& (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						if(GlobalVariable.isDL1018){
							btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
						}

						btCN6.setEnabled(true);
					}

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(false);
				}
				else if(currentCallWithState.type == CALL_TYPE.DAOBOACPT)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_hold_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));

					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);

					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN6.setEnabled(true);
					if(currentCallWithState.first_livehold&& (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						btCN6.setEnabled(true);
						if(GlobalVariable.isDL1018){
							btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
						}
					}

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(true);
					btn_lite_endlive.setEnabled(false);
				}
				else if(currentCallWithState.type == CALL_TYPE.DAOBOHOLD)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));

					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(false);
					//导播保持的时候不能能直接直播
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btCN6.setEnabled(false);

					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3) {
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
						btCN6.setEnabled(true);
						if(currentCallWithState.first_livehold&& (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)){
							btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
							btCN6.setEnabled(true);
							if(GlobalVariable.isDL1018){
								btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
							}
						}
					}

					//Lite模式无此状态不处理
				}
				else if(currentCallWithState.type == CALL_TYPE.WAITDAOBO)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));

					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);

					//lite no
				}
				else if(currentCallWithState.type == CALL_TYPE.WAITZHUBO)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(false);

					//等待主播接听可以直接直播
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN6.setEnabled(true);

					//lite no
				}
				else if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT
						||currentCallWithState.type == CALL_TYPE.ZHUBOHOLD
						||currentCallWithState.type == CALL_TYPE.LIVEACPT
						||currentCallWithState.type == CALL_TYPE.LIVEHOLD
				)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btCN1.setEnabled(false);
					btCN4.setEnabled(true);
					btCN5.setEnabled(false);


					//主播接听的时候可以直播
					if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
						btCN6.setEnabled(true);
					}
					//主播保持的时候不能直播
					if(currentCallWithState.type == CALL_TYPE.ZHUBOHOLD){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
						btCN6.setEnabled(false);
					}
					if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_enable));
						btCN6.setEnabled(true);
					}
					if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
						btCN6.setEnabled(true);
					}
					//直播的时候可以接听，直播保持的时候可以接听(仅导播模式下)
					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3)
					{
						btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
						btCN1.setEnabled(true);

						if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
							btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_enable));
							btCN6.setEnabled(true);
						}
						if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
							btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
							btCN6.setEnabled(true);
						}
					}


					if(currentCallWithState.type == CALL_TYPE.LIVEACPT){
						btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));
						btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
						btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
						btn_lite_liveorhold.setEnabled(false);
						btn_lite_tolive.setEnabled(false);
						btn_lite_endlive.setEnabled(false);

					}
					if(currentCallWithState.type == CALL_TYPE.LIVEHOLD){
						btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
						btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
						btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
						btn_lite_liveorhold.setEnabled(false);
						btn_lite_tolive.setEnabled(false);
						btn_lite_endlive.setEnabled(false);
					}
				}


				else if(currentCallWithState.type == CALL_TYPE.ENDCALL)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btCN1.setEnabled(false);
					btCN4.setEnabled(true);
					btCN5.setEnabled(false);

					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btCN6.setEnabled(false);

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(false);
				}
			}
			else if(GlobalVariable.charactername.equals("broadcaster"))
			{
				if(currentCallWithState.type == CALL_TYPE.DIALINIT)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));

					btCN1.setEnabled(false);
					btCN4.setEnabled(true);
					btCN5.setEnabled(false);

					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btCN6.setEnabled(false);

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(false);
				}
				if(currentCallWithState.type == CALL_TYPE.WAITZHUBO)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);
					btCN6.setEnabled(true);

					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						btCN6.setEnabled(true);
					}
				}
				//处理呼出外线的情况
				else if(currentCallWithState.type == CALL_TYPE.DAOBOACPT)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_hold_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);

					if(currentCallWithState.dailout){
						btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					}

					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN6.setEnabled(true);
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						btCN6.setEnabled(true);
					}


					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(false);
				}
				if(currentCallWithState.type == CALL_TYPE.ZHUBOACPT)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_hold_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);
					btCN6.setEnabled(true);
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						btCN6.setEnabled(true);
					}
				}
				if(currentCallWithState.type == CALL_TYPE.ZHUBOHOLD)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);
					btCN6.setEnabled(true);
					if(currentCallWithState.dailout){
						btCN5.setEnabled(false);
						btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					}

					if(GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3){
						btCN5.setEnabled(true);
						btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));

						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
						btCN6.setEnabled(true);
					}
					if(currentCallWithState.first_livehold && GlobalVariable.sipmodel == 0){
						btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_enable));
						btCN6.setEnabled(true);
					}
				}
				if(currentCallWithState.type == CALL_TYPE.LIVEACPT)
				{
					//直播的时候允许直接切回接听
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_enable));
					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);
					btCN6.setEnabled(true);

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_enable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btn_lite_liveorhold.setEnabled(true);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(true);
				}
				if(currentCallWithState.type == CALL_TYPE.LIVEHOLD)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_enable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_enable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));

					btCN1.setEnabled(true);
					btCN4.setEnabled(true);
					btCN5.setEnabled(true);
					btCN6.setEnabled(true);

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_enable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_enable));
					btn_lite_liveorhold.setEnabled(true);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(true);
				}
				else if(currentCallWithState.type == CALL_TYPE.ENDCALL)
				{
					btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
					btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_hangup_disable));
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
					btCN1.setEnabled(false);
					btCN4.setEnabled(false);
					btCN5.setEnabled(false);
					btCN6.setEnabled(false);

					btn_lite_liveorhold.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));
					btn_lite_tolive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
					btn_lite_endlive.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
					btn_lite_liveorhold.setEnabled(false);
					btn_lite_tolive.setEnabled(false);
					btn_lite_endlive.setEnabled(false);
				}
			}
			if(currentCallWithState.wait_ackhold)
			{
				btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
				btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
				btCN1.setEnabled(false);//保持
				btCN6.setEnabled(false);
			}
			if(currentCallWithState.wait_ackaccept)
			{
				Log.e("等待接听","未允许转接");
				if(GlobalVariable.charactername.equals("director")){
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
				}
				else{
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
				}

				btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_live_disable));
				btCN5.setEnabled(false);
				btCN6.setEnabled(false);

			}
			if(currentCallWithState.live_lock && GlobalVariable.livelock){
				btCN1.setEnabled(false);
				btCN5.setEnabled(false);
				btCN6.setEnabled(false);
				btCN1.setImageDrawable(getResources().getDrawable(R.drawable.btn_accept_disable));
				if(GlobalVariable.charactername.equals("director")){
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_pdr_disable));
				}
				else{
					btCN5.setImageDrawable(getResources().getDrawable(R.drawable.btn_switch_mbc_disable));
				}
				btCN6.setImageDrawable(getResources().getDrawable(R.drawable.btn_livehold_disable));

				btCN4.setImageDrawable(getResources().getDrawable(R.drawable.btn_lock));
			}
		}
	}
	/**每次移除电话后对剩余电话进行更新显示重排序**/
	@RequiresApi(api = Build.VERSION_CODES.N)
	public void updateShowIndex(){
		refreshCallList();
		for(MyCallWithState callws : allCallinList)
		{
			callws.showitem.replace("id",String.valueOf(callws.id+1));
		}
	}
	/**更新ListID**/
	@RequiresApi(api = Build.VERSION_CODES.N)
	private void refreshCallList(){
		int tempid = 0;
		for(MyCallWithState callws :allCallinList)
		{
			callws.id = tempid;
			callws.showitem.replace("id",""+(tempid+1));
			tempid++;
		}
	}
	/**获取ip地址**/
	/**每次有新电话呼入的时候自动跳转到MainActivity**/
	public void autoSkipToMainActivity(){
		if(HistoryActivity.handler_!=null){
			Message m3 = Message.obtain(HistoryActivity.handler_,1015,null);
			m3.sendToTarget();
		}
		if(DailActivity.handler_!=null){
			Message m4 = Message.obtain(DailActivity.handler_,1015,null);
			m4.sendToTarget();
		}
		if(SettingActivity.handler_!=null){
			Message m5 = Message.obtain(SettingActivity.handler_,1015,null);
			m5.sendToTarget();
		}
		if(ContactsActivity.handler_!=null){
			Message m6 = Message.obtain(ContactsActivity.handler_,1015,null);
			m6.sendToTarget();
		}
		if(CommunicateActivity.handler_!=null){
			Message m7 = Message.obtain(CommunicateActivity.handler_,1015,null);
			m7.sendToTarget();
		}
	}
	public void alertCallNotSelected(){
//		AlertDialog.Builder builder  = new AlertDialog.Builder(MainActivity.this);
//		builder.setTitle("确认" ) ;
//		builder.setMessage("未选择处理电话" ) ;
//		builder.setPositiveButton("是" ,  null );
//		builder.show();

		MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"未选择处理电话").create();
		msgNotifyDialog.show();
		if(GlobalVariable.viewmodel == 1)
			msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
		else if(GlobalVariable.viewmodel == 2)
			msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
		else if(GlobalVariable.viewmodel == 3)
			msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
		msgNotifyDialog.getWindow().setLayout(500, 300);
	}
	/**根据读取配置项配置界面（搁置未实现）**/
	public void updateViewByConfig(JSONObject configJson) {
		//先确定各个位置是否有内容，如果一行都没有内容，该行都隐藏
		try {
			String text1 = configJson.get("text1").toString();
			String text2 = configJson.get("text2").toString();
			String text3 = configJson.get("text3").toString();
			String select1 = configJson.get("select1").toString();
			String select2 = configJson.get("select2").toString();
			String select3 = configJson.get("select3").toString();

			//确定text的数量
			int textnum = 0;
			if(!text1.equals("0")) textnum++;
			if(!text2.equals("0")) textnum++;
			if(!text3.equals("0")) textnum++;

			//重新排序将不显示的放在后面
			if(textnum == 1)
			{
				if(text1.equals("0")) {
					text1 = (text2.equals("0") ? text2 : text3);
				}
			}
			if(textnum == 2)
			{
				if(text1.equals("0")) {
					text1 = text3;
					text3 = "0";
				}
				if(text2.equals("0")){
					text2 = text3;
					text3 = "0";
				}
			}

			//确定select的数量
			int selectnum = 0;
			if(!select1.equals("0")) selectnum++;
			if(!select2.equals("0")) selectnum++;
			if(!select3.equals("0")) selectnum++;

			//重新排序将不显示的放在后面
			if(selectnum == 1)
			{
				if(select1.equals("0")) {
					select1 = (select2.equals("0") ? select3 : select2);
					select2 = select3 = "0";
				}
			}
			if(selectnum == 2)
			{
				if(select1.equals("0")) {
					select1 = select3;
					select3 = "0";
				}
				if(select2.equals("0")){
					select2 = select3;
					select3 = "0";
				}
			}

			//确定显示行数
			int linenum = Math.max(textnum,selectnum);
			TableRow row1 = (TableRow)findViewById(R.id.TableRow1);
			TableRow row2 = (TableRow)findViewById(R.id.TableRow2);
			TableRow row3 = (TableRow)findViewById(R.id.TableRow3);
			TableLayout mytable = (TableLayout) findViewById(R.id.TableForm);
			if(linenum == 0) {
				mytable.removeView(findViewById(R.id.TableRow1));
				mytable.removeView(findViewById(R.id.TableRow2));
				mytable.removeView(findViewById(R.id.TableRow3));

			}
			if(linenum == 1)
			{
				mytable.removeView(findViewById(R.id.TableRow2));
				mytable.removeView(findViewById(R.id.TableRow3));

				if(!text1.equals("0")) {
					TextView textTitle1 = (TextView)findViewById(R.id.textTitle1);
					textTitle1.setText(DataConvertUtil.getConfigString(text1));
				}
				if(!select1.equals("0")) {
					TextView selectTitle1 = (TextView)findViewById(R.id.selectTitle1);
					selectTitle1.setText(DataConvertUtil.getConfigString(select1));
				}
			}
			if(linenum == 2)
			{
				mytable.removeView(findViewById(R.id.TableRow3));

				TextView textTitle1 = (TextView)findViewById(R.id.textTitle1);
				textTitle1.setText(DataConvertUtil.getConfigString(text1));
				TextView selectTitle1 = (TextView)findViewById(R.id.selectTitle1);
				selectTitle1.setText(DataConvertUtil.getConfigString(select1));

				if(!text2.equals("0")) {
					TextView textTitle2 = (TextView)findViewById(R.id.textTitle2);
					textTitle2.setText(DataConvertUtil.getConfigString(text2));
				}
				if(!select2.equals("0")) {
					TextView selectTitle2 = (TextView)findViewById(R.id.selectTitle2);
					selectTitle2.setText(DataConvertUtil.getConfigString(select2));
				}
			}
			if(linenum == 3)
			{
				mytable.setColumnCollapsed(0,false);
				mytable.setColumnCollapsed(0,false);
				mytable.setColumnCollapsed(0,false);

				TextView textTitle1 = (TextView)findViewById(R.id.textTitle1);
				textTitle1.setText(DataConvertUtil.getConfigString(text1));
				TextView selectTitle1 = (TextView)findViewById(R.id.selectTitle1);
				selectTitle1.setText(DataConvertUtil.getConfigString(select1));

				TextView textTitle2 = (TextView)findViewById(R.id.textTitle2);
				textTitle2.setText(DataConvertUtil.getConfigString(text2));
				TextView selectTitle2 = (TextView)findViewById(R.id.selectTitle2);
				selectTitle2.setText(DataConvertUtil.getConfigString(select2));

				TextView textTitle3 = (TextView)findViewById(R.id.textTitle3);
				textTitle3.setText(DataConvertUtil.getConfigString(text3));
				TextView selectTitle3 = (TextView)findViewById(R.id.selectTitle3);
				selectTitle3.setText(DataConvertUtil.getConfigString(select3));
			}

			//确定每行显示内容


		} catch (JSONException e) {
			LogClient.generate("【界面配置错误】"+e.getMessage());
			e.printStackTrace();
		}
	}
	/**更新展示列表内用户的详细信息**/
	@RequiresApi(api = Build.VERSION_CODES.N)
	public void updateListUserInfo(MyCallWithState callWithState){
		try {
			getCallRecordInfo(callWithState, callWithState.pstn, callWithState.uid);
			String tailnumber = "";
			if (callWithState.pstn.length() >= 4) {
				tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
			} else {
				tailnumber = callWithState.pstn;
			}

			callWithState.showitem.replace("name", (callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name);
			callWithState.showitem.replace("pstnmark", callWithState.pstnmark);
			callWithState.showitem.replace("content", callWithState.content);
			Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
			m2.sendToTarget();
		}catch (Exception e)
		{

		}
	}
	public void updateInfoAndButton(){
		if(GlobalVariable.viewmodel == 1){
			updateCurrentInfoView();
			updateOperateButton();
		}
		else if(GlobalVariable.viewmodel == 2){
			updateCtInfoView();
			updateCtOperateButton();
		}
		else if(GlobalVariable.viewmodel == 3){
			updateGsInfoView();
			updateGsOperateButton();
		}
	}

	//************************自定义协议部分****************************
	WSReceiver wsReceiver;
	//private Boolean online = false;
	class WSReceiver extends BroadcastReceiver {

		@RequiresApi(api = Build.VERSION_CODES.N)
		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				String x = intent.getExtras().get("msg").toString();
				Log.d("HANDLE MESSAGE PRE", x.toString());
				if (intent.getExtras().get("msg").toString().equals("connected") ) {
					if (!GlobalVariable.online) {
						GlobalVariable.online = true;
						Log.d("HANDLE MESSAGE", "connected...............");
					}
				}
				else if (intent.getExtras().get("msg").toString().equals("disconnected")) {
					if (GlobalVariable.online) {
						GlobalVariable.online = false;
						Log.d("HANDLE MESSAGE", "disconnected...............");
					}
				}
				else {
					try {
						JSONObject msg_json = new JSONObject(intent.getExtras().get("msg").toString());
						String msgType = msg_json.get("msgType").toString();
						if(msgType.equals("OUT MAKE CALL")){
							String callednumber = "0000";
							String show_callednumber = "0000";
							try {
								String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
								String callednumber_all = contacturi.replace("<sip:", "");
								String[] callednumber_list = callednumber_all.split("@");
								String tailcallednumber = "";
								if (callednumber_list[0].length() >= 4) {
									tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
								} else {
									tailcallednumber = callednumber_list[0];
								}
								callednumber = callednumber_list[0];
								show_callednumber = tailcallednumber;
							} catch (Exception e) {
								e.printStackTrace();
							}
							if(GlobalVariable.ctNumberMap.get(callednumber)!=null) {
								String sta_callednumber = callednumber;
								String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
								String outCallUri = msg_json.get("OutCallUri").toString();
								String uid = msg_json.get("uid").toString();
								//20211220在已经获取到callednumber并确认callednumber归属映射表时，更新电话记录的callednumber
								new Thread() {
									@RequiresApi(api = Build.VERSION_CODES.N)
									@Override
									public void run() {
										HttpRequest request = new HttpRequest();
										String rs = request.postJson(
												"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoCalledNum",
												"{\"type\":\"post pstn info\", " +
														"\"callid\":\"" + uid + "\", " +
														"\"callednumber\":\"" + sta_callednumber + "\", " +
														"}"
										);
									}
								}.start();

								final String callednumber_final = callednumber;
								final String show_callednumber_final = show_callednumber;
								//final String callednumber = callednumber_list[0];
								if (GlobalVariable.charactername.equals("director")) {
									if (CallActivity.instance != null) {
										//当前正在打电话
										String msg_to_send =
												"{ \"msgType\": \"INNER HANGUP\""
														+ ", \"fromsipid\": \"" + GlobalVariable.innercallfrom + "\""
														+ ", \"tosipid\": \"" + GlobalVariable.innercallto + "\""
														+ ", \"frompgm\": \"" + GlobalVariable.innerchannelfrom + "\""
														+ ", \"topgm\": \"" + GlobalVariable.innerchannelto + "\""
														+ ", \"fromname\": \"" + GlobalVariable.innernamefrom + "\""
														+ ", \"toname\": \"" + GlobalVariable.innernameto + "\""
														+ ", \"fromrole\": \"" + GlobalVariable.innerrolefrom + "\""
														+ ", \"torole\": \"" + GlobalVariable.innerroleto + "\""
														+ ", \"msg\": \"" + "外线呼入" + "\""
														+ " }";
										if (MainActivity.wsControl != null)
											MainActivity.wsControl.sendMessage(msg_to_send);
									}
									//响铃和闪灯逻辑放到handleMessage中处理
									LED_Media_Notify();
									GlobalVariable.PhoneRedTitle = true;
									new Thread() {
										@Override
										public void run() {
											//MyCall call1 = new MyCall(account, -1);

											MyCallWithState callWithState = new MyCallWithState();
											//callWithState.mycall = call1;
											callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
											callWithState.type = CALL_TYPE.NEWINIT;
											callWithState.outcalluri = outCallUri;
											callWithState.uid = uid;
											callWithState.daobocalluri = localUri;
											//callWithState.callednumber = "0000";//callednumber;
											callWithState.callednumber = callednumber_final;
											callWithState.show_callednumber = show_callednumber_final;
											SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
											callWithState.callintime = df.format(new Date());

											try {
												currentCallNum = allCallinList.size();
												callWithState.id = currentCallNum;
												allCallinList.add(currentCallNum, callWithState);
												currentCallNum++;
											} catch (Exception e) {
												e.printStackTrace();
											}

											//查询电话信息
											String pstn = outCallUri.replace("<sip:", "");
											String[] pstn_list = pstn.split("@");
											pstn = pstn_list[0];
											String finalPstn = pstn;
											callWithState.pstn = pstn;

											getCallRecordInfo(callWithState, finalPstn, callWithState.uid);
											String tailnumber = "";
											if (callWithState.pstn.length() >= 4) {
												tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
											} else {
												tailnumber = callWithState.pstn;
											}
											callWithState.showitem = DataConvertUtil.putData(
													(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
													standardDateFormat.format(new Date()),
													callWithState.from,
													callWithState.pstn,
													tailnumber,
													callWithState.pstnmark,
													callWithState.content,
													String.valueOf(callWithState.id + 1),
													callWithState.uid,
													callWithState.callednumber
											);
											callinList.add(callWithState.showitem);


											try {
												HttpRequest request = new HttpRequest();

												String daobo = daoBoCallUri.replace("<sip:", "");
												String[] daobo_list = daobo.split("@");
												daobo = daobo_list[0];

												String rs = request.postJson(
														GlobalVariable.SEHTTPServer + "/incoming-call",
														"{" +
																"\"outcall\":\"" + callWithState.pstn + "\"," +
																"\"contact\":\"" + callWithState.callednumber + "\"," +
																"\"daobo\":\"" + daobo + "\"," +
																"\"zhubo\":\"\"," +
																"\"callintime\":\""+callWithState.callintime+"\"," +
																"\"code\":" + 200 + "}"
												);
											} catch (Exception e) {
												//e.printStackTrace();
												Log.e("SEHTTPServer","指令异常");
											}

											Message m2 = Message.obtain(MainActivity.handler_, 1001, callWithState);
											m2.sendToTarget();
											autoSkipToMainActivity();
										}
									}.start();
								}
							}
						}
						else if(msgType.equals("OUT REMAKE CALL")){
							//在导播转给主播的情况下导播下线应该怎么处理
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String outCallUri = msg_json.get("OutCallUri").toString();
							String uid = msg_json.get("uid").toString();
							//if(daoBoCallUri.equals(localUri) ) {
							if(GlobalVariable.charactername.equals("director") ) {
								LED_Media_Notify();
								GlobalVariable.PhoneRedTitle = true;

								String callednumber = "0000";
								String show_callednumber = "0000";
								try {
									String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
									String callednumber_all = contacturi.replace("<sip:", "");
									String[] callednumber_list = callednumber_all.split("@");
									String tailcallednumber = "";
									if (callednumber_list[0].length() >= 4) {
										tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
									} else {
										tailcallednumber = callednumber_list[0];
									}
									callednumber = callednumber_list[0];
									show_callednumber = tailcallednumber;
								}catch (Exception e){
									e.printStackTrace();
								}
								final String callednumber_final = callednumber;
								final String show_callednumber_final = show_callednumber;

								new Thread() {
									@Override
									public void run() {
										//MyCall call1 = new MyCall(account, -1);

										MyCallWithState callWithState = new MyCallWithState();
										//callWithState.mycall = call1;
										callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
										callWithState.type = CALL_TYPE.WAITDAOBO;//待定
										callWithState.outcalluri = outCallUri;
										callWithState.uid = uid;
										callWithState.daobocalluri = localUri;
										SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
										callWithState.callintime = df.format(new Date());
										callWithState.callednumber = callednumber_final;
										callWithState.show_callednumber = show_callednumber_final;

										try{
											currentCallNum = allCallinList.size();
											callWithState.id = currentCallNum;
											allCallinList.add(currentCallNum,callWithState);
											currentCallNum++;
										}
										catch (Exception e){
											e.printStackTrace();
										}


										//查询电话信息
										String pstn = outCallUri.replace("<sip:","");
										String[] pstn_list = pstn.split("@");
										pstn = pstn_list[0];
										String finalPstn = pstn;
										callWithState.pstn = pstn;

										getCallRecordInfo(callWithState,finalPstn,callWithState.uid);

										if(callWithState.dailout){
											//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
											//pstn = pstn.replaceFirst("00","");
                                            pstn = pstn.substring(2);
										}

										String tailnumber = "";
										if(callWithState.pstn.length()>=4) {
											tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
										}
										else{
											tailnumber = callWithState.pstn;
										}

										callWithState.showitem = DataConvertUtil.putData(
												(callWithState.name==null || callWithState.name.equals("null") || callWithState.name.equals(""))?tailnumber:callWithState.name,
												standardDateFormat.format(new Date()),
												callWithState.from,
												pstn,
												tailnumber,
												callWithState.pstnmark,
												callWithState.content,
												String.valueOf(callWithState.id+1),
												callWithState.uid,
												callWithState.callednumber);
										callinList.add(callWithState.showitem);
										Message m2 = Message.obtain(MainActivity.handler_,1001,null);
										m2.sendToTarget();
										autoSkipToMainActivity();
									}
								}.start();
							}
						}
						else if(msgType.equals("ACK DAOBO MAKE CALL")) {
							String callid = msg_json.get("callid").toString();
							String callUri = msg_json.get("callUri").toString();
							if(callUri.equals(localUri)){
								String msg_to_send = "{ \"msgType\": \"DAOBO MAKE CALL\", \"callUri_id\": \""+callid+"\", \"OutCallUri\": \""+currentCallWithState.outcalluri+"\", \"DaoBoCallUri\": \""+localUri+"\", \"Record\":"+currentCallWithState.record+" }";
//								Intent intent_ack = new Intent(context, JWebSocketClientService.class);
//								intent_ack.putExtra("msg_to_send", msg_to_send);
//								startService(intent_ack);
								if(wsControl!=null)wsControl.sendMessage(msg_to_send);
							}
						}
						else if(msgType.equals("ACK ZHUBO MAKE CALL")){

							String callid = msg_json.get("callid").toString();
							String callUri = msg_json.get("callUri").toString();
							if(callUri.equals(localUri)) {
								String msg_to_send = "{ \"msgType\": \"ZHUBO MAKE CALL\", \"callUri_id\": \"" + callid + "\", \"OutCallUri\": \"" + currentCallWithState.outcalluri + "\", \"ZhuBoCallUri\": \"" + localUri + "\", \"Record\":"+currentCallWithState.record+" }";
//								Intent intent_ack = new Intent(context, JWebSocketClientService.class);
//								intent_ack.putExtra("msg_to_send", msg_to_send);
//								startService(intent_ack);
								if(wsControl!=null)wsControl.sendMessage(msg_to_send);
							}
						}
						else if(msgType.equals("ACK VIRTUAL ZHUBO MAKE CALL")){
							String callid = msg_json.get("callid").toString();
							String callUri = msg_json.get("callUri").toString();
							if(callUri.equals(localUri)) {
								String msg_to_send = "{ \"msgType\": \"VIRTUAL ZHUBO MAKE CALL\", \"callUri_id\": \"" + callid + "\", \"OutCallUri\": \"" + currentCallWithState.outcalluri + "\", \"ZhuBoCallUri\": \"" + localUri + "\", \"Record\":"+currentCallWithState.record+" }";
								if(wsControl!=null)wsControl.sendMessage(msg_to_send);
							}
						}
						else if(msgType.equals("DAOBO TO ZHUBO")){
							String zhuBoCallUri = msg_json.get("ZhuBoCallUri").toString();
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String outCallUri = msg_json.get("OutCallUri").toString();
							String uid = msg_json.get("uid").toString();
							if(zhuBoCallUri.equals(localUri))
							{
								LED_Media_Notify();
								GlobalVariable.PhoneRedTitle = true;

								String callednumber = "0000";
								String show_callednumber = "0000";
								try {
									String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
									String callednumber_all = contacturi.replace("<sip:", "");
									String[] callednumber_list = callednumber_all.split("@");
									String tailcallednumber = "";
									if (callednumber_list[0].length() >= 4) {
										tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
									} else {
										tailcallednumber = callednumber_list[0];
									}
									callednumber = callednumber_list[0];
									show_callednumber = tailcallednumber;
								}catch (Exception e){
									e.printStackTrace();
								}
								final String callednumber_final = callednumber;
								final String show_callednumber_final = show_callednumber;

								new Thread() {
									@Override
									public void run() {

										if(currentCallNum != allCallinList.size())
										{
											currentCallNum = allCallinList.size();
										}

										MyCall zhuboCall = new MyCall(account, -1);
										MyCallWithState callWithState = new MyCallWithState();
										callWithState.mycall = zhuboCall;
										callWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
										callWithState.type = CALL_TYPE.WAITZHUBO;
										callWithState.outcalluri = outCallUri;
										callWithState.id = currentCallNum;
										callWithState.uid = uid;
										callWithState.daobocalluri = daoBoCallUri;
										SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
										callWithState.callintime = df.format(new Date());
										callWithState.callednumber = callednumber_final;
										callWithState.show_callednumber = show_callednumber_final;

										allCallinList.add(currentCallNum,callWithState);
										currentCallNum++;

										//查询电话信息
										String pstn = outCallUri.replace("<sip:","");
										String[] pstn_list = pstn.split("@");
										pstn = pstn_list[0];
										String finalPstn = pstn;
										callWithState.pstn = pstn;

										getCallRecordInfo(callWithState,finalPstn,callWithState.uid);

										if(callWithState.dailout){
											//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
											//pstn = pstn.replaceFirst("00","");
											pstn = pstn.substring(2);
										}

										String tailnumber = "";
										if(callWithState.pstn.length()>=4) {
											tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
										}
										else{
											tailnumber = callWithState.pstn;
										}

										callWithState.showitem = DataConvertUtil.putData(
												(callWithState.name==null || callWithState.name.equals("null") || callWithState.name.equals(""))?tailnumber:callWithState.name,
												standardDateFormat.format(new Date()),
												callWithState.from,
												pstn,
												tailnumber,
												callWithState.pstnmark,
												callWithState.content,
												String.valueOf(callWithState.id+1),
												callWithState.uid,
												callWithState.callednumber);
										callinList.add(callWithState.showitem);
										Message m2 = Message.obtain(MainActivity.handler_,1001,null);
										m2.sendToTarget();
										autoSkipToMainActivity();
									}
								}.start();
							}
							if(daoBoCallUri.equals(localUri)){
								for(MyCallWithState callws :allCallinList) {
									if( callws.outcalluri.equals(outCallUri) ) {
										broadcastCallType(callws,CALL_TYPE.WAITZHUBO);
									}
								}

							}
						}
						else if(msgType.equals("ZHUBO TO DAOBO")){
							//如果导播ID和本地ID一致，显示BackAccept
							String outCallUri = msg_json.get("OutCallUri").toString();
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String zhuBoCallUri = msg_json.get("ZhuBoCallUri").toString();
							if(daoBoCallUri.equals(localUri))
							{
								LED_Media_Notify();
								GlobalVariable.PhoneRedTitle = true;
								for(MyCallWithState callws :allCallinList) {
									if( callws.outcalluri.equals(outCallUri) ) {
										callws.state = CALL_STATE_TYPE.DAOBO_REVERT;

										//20210630添加type?是否影响流程需测试
										callws.type = CALL_TYPE.WAITDAOBO;
										broadcastCallType(callws,CALL_TYPE.WAITDAOBO);
									}
								}

								updateInfoAndButton();
							}
						}
						else if(msgType.equals("HANGUP OUT CALL")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							//String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							int remoteCallNum = -1;
							for(MyCallWithState callws :allCallinList) {
								if( callws.outcalluri.equals(outCallUri) ) {
									new Thread() {
										@Override
										public void run() {
											try {
												String character_param = "";

												String daobo = localUri.replace("<sip:", "");
												String[] daobo_list = daobo.split("@");
												daobo = daobo_list[0];

												if(GlobalVariable.charactername.equals("director")){
													character_param = "\"daobo\":\"" + daobo + "\"," ;
												}
												else{
													character_param = "\"zhubo\":\"" + daobo + "\"," ;
												}

												HttpRequest request = new HttpRequest();
												String rs = request.postJson(
														GlobalVariable.SEHTTPServer + "/hangup-call",
														"{" +
																"\"recordpath\":\"http://"+GlobalVariable.SIPSERVER+":8080/record/"+callws.uid+".wav"+ "\"," +
																"\"outcall\":\"" + (callws.dailout?callws.pstn.substring(2):callws.pstn) + "\"," +
																"\"contact\":\"" + callws.callednumber + "\"," +
																character_param +
																"\"code\":" + 200 + "}"
												);
											} catch (Exception e) {
												//e.printStackTrace();
												Log.e("SEHTTPServer","指令异常");
											}
										}
									}.start();

									remoteCallNum = callws.id;
									GlobalVariable.remoteCallUidList.add(callws.uid);
									callws.type = CALL_TYPE.ENDCALL;
									broadcastCallType(callws,CALL_TYPE.ENDCALL);
								}
							}
							resetMainView();
							refreshCallList();
							updateInfoAndButton();
							//if(remoteCallNum!=-1) {
								new Thread() {
									@Override
									public void run() {
										try {
											sleep(3000);
											List<String> numList = new ArrayList<>();
											numList.addAll(GlobalVariable.remoteCallUidList);
											for (String uid : numList) {
												GlobalVariable.remoteCallUidList.clear();
												Message m2 = Message.obtain(MainActivity.handler_,1007,uid);
												m2.sendToTarget();
											}
											GlobalVariable.remoteCallUidList.clear();
										} catch (Exception e) {
											System.out.println(e);
										}
									}
								}.start();
							//}

							//外线主动挂断后（话机端未操作），也需要检测当前电话列表中是否有待接电话，如果没有的情况下取消闪烁
							boolean stop_title_flash = true;
							for(MyCallWithState callws : allCallinList){
								if(callws.type == CALL_TYPE.WAITDAOBO || callws.type == CALL_TYPE.WAITZHUBO || callws.type == CALL_TYPE.NEWINIT)
									stop_title_flash = false;
							}
							if(stop_title_flash){
								GlobalVariable.PhoneRedTitle = false;
							}

						}
						else if(msgType.equals("VIRTUAL TO ZHUBO ACCEPT CALL")){
							//虚拟主播接听电话成功
							//需要将对应的CallWithState和CallList显示内容进行更新
							//注意这里不能使用currentCall,因为有可能进行了线路切换
							//需要根据返回信息中的外线URI来确定是哪一通并更新其状态
							String outCallUri = msg_json.get("OutCallUri").toString();
							String zhuboUri = msg_json.get("ZhuBoCallUri").toString();
							String daoboUri = msg_json.get("DaoBoCallUri").toString();
							String code = msg_json.get("Code").toString();
							if(code.equals("200")||code.equals("201")) {
								for (MyCallWithState callws : allCallinList) {
									if (callws.outcalluri.equals(outCallUri)) {
										if(daoboUri.equals(localUri)||zhuboUri.equals(localUri)) {
											String msg_to_send_sync = "{ \"msgType\": \"DAOBO SYNC SHOW CALL\"" +
													", \"DaoBoCallUri\": \""+callws.daobocalluri+"\"" +
													", \"OutCallUri\": \""+callws.outcalluri+"\"" +
													", \"uid\": \""+callws.uid+"\"" +
													", \"contacturi\":"+callws.callednumber+" }";
											if(wsControl!=null)wsControl.sendMessage(msg_to_send_sync);

											//if (localUri.equals(zhuboUri)) {
											callws.state = CALL_STATE_TYPE.ZHUBO_ACCEPT;
											//此时已经是Live状态
											callws.type = CALL_TYPE.LIVEACPT;
											if (callws.accepttime == null) {
												callws.accepttime = GlobalVariable.hms_df.format(new Date());
											}
											callws.livetime = GlobalVariable.hms_df.format(new Date());
											broadcastCallType(callws, CALL_TYPE.LIVEACPT);
											//}

											//不移除的一方发SYNC SHOW CALL

										}
										else{
											//判断不是自己接听的电话移除
											if(GlobalVariable.charactername.equals("director") && !callws.syncshow){
												int remoteCallNum = callws.id;
												if (remoteCallNum != -1) {
													try {
														allCallinList.remove(remoteCallNum);
														callinList.remove(remoteCallNum);
														updateShowIndex();
														if(callinListSelectedIdx == remoteCallNum)
														{
															callinListSelectedIdx = -1;
														}
														currentCallNum--;
														resetMainView();
														refreshCallList();
														if(GlobalVariable.viewmodel==1) {
															updateOperateButton();
														}
														else if(GlobalVariable.viewmodel==3){
															updateGsOperateButton();
														}
														myAdapter.setDataList(allCallinList);
														mbcAdapter.setDataList(allCallinList);
														//callinListmbcAdapter.setDataList(allCallinList);
													} catch (Exception e) {
														System.out.println(e);
													}
												}
											}
										}
//									//这个是虚拟主播给主播发送的消息理论上不会给导播进行处理
									}
								}
							}
							updateInfoAndButton();
						}
						else if(msgType.equals("VIRTUAL TO ZHUBO HANGUP CALL")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							String zhuboUri = msg_json.get("ZhuBoCallUri").toString();
							String daoboUri = msg_json.get("DaoBoCallUri").toString();

							int removeid = -1;
							for(MyCallWithState callws :allCallinList) {
								if( callws.outcalluri.equals(outCallUri) ) {
									removeid = callws.id;
									//主播转导播
									if(callws.state == CALL_STATE_TYPE.ZHUBO_SWITCH)
									{
										String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
												+ currentCallWithState.outcalluri
												+ "\", \"DaoBoCallUri\": \""
												+ currentCallWithState.daobocalluri
												+ "\", \"ZhuBoCallUri\": \""
												+ MainActivity.zhuboUri + "\" }";
//										Intent intent_ack = new Intent(context, JWebSocketClientService.class);
//										intent_ack.putExtra("msg_to_send", msg_to_send);
//										startService(intent_ack);
										if(wsControl!=null)wsControl.sendMessage(msg_to_send);
										//更新type为待导播接听
										broadcastCallType(callws,CALL_TYPE.WAITDAOBO);

									}
									//主播自己挂断
									else{
										String msg_to_send = "{ \"msgType\": \"ZHUBO HANGUP\", \"OutCallUri\": \""
												+ currentCallWithState.outcalluri
												+ "\", \"DaoBoCallUri\": \""
												+ currentCallWithState.daobocalluri
												+ "\", \"ZhuBoCallUri\": \""
												+ MainActivity.zhuboUri
												+ "\", \"VirtualUri\": \""
												+ null
												+ "\" }";
//										Intent intent_ack = new Intent(context, JWebSocketClientService.class);
//										intent_ack.putExtra("msg_to_send", msg_to_send);
//										startService(intent_ack);
										if(wsControl!=null)wsControl.sendMessage(msg_to_send);

										//更新type为电话结束
										broadcastCallType(callws,CALL_TYPE.ENDCALL);
									}
								}
							}
							if(removeid != -1)
							{
								try {
									//currentCallWithState.state = CALL_STATE_TYPE.HANGUP;
									allCallinList.remove(removeid);
									callinList.remove(removeid);
									updateShowIndex();
									if(callinListSelectedIdx == removeid)
									{
										callinListSelectedIdx = -1;
									}
									currentCallNum--;
									resetMainView();
									refreshCallList();
									if(GlobalVariable.viewmodel==1) {
										updateOperateButton();
									}
									else if(GlobalVariable.viewmodel==3){
										updateGsOperateButton();
									}
									myAdapter.setDataList(allCallinList);
									mbcAdapter.setDataList(allCallinList);
									//callinListmbcAdapter.setDataList(allCallinList);

								} catch (Exception e) {
									System.out.println(e);
								}
							}
						}
						else if(msgType.equals("VIRTUAL TO ZHUBO MUTE CALL")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							String zhuboUri = msg_json.get("ZhuBoCallUri").toString();
							String daoboUri = msg_json.get("DaoBoCallUri").toString();

							int removeid = -1;
							for(MyCallWithState callws :allCallinList) {
								if( callws.outcalluri.equals(outCallUri) ) {
									callws.state = CALL_STATE_TYPE.MUTE;
								}
							}
							updateInfoAndButton();

						}
						else if(msgType.equals("OUT MAKE CALL TO ZHUBO")) {
							String outCallUri = msg_json.get("OutCallUri").toString();
							String zhuboUri = msg_json.get("ZhuBoCallUri").toString();
							String uid = msg_json.get("uid").toString();
							String callednumber = "0000";
							String show_callednumber = "0000";
							try {
								String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
								String callednumber_all = contacturi.replace("<sip:", "");
								String[] callednumber_list = callednumber_all.split("@");
								String tailcallednumber = "";
								if (callednumber_list[0].length() >= 4) {
									tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
								} else {
									tailcallednumber = callednumber_list[0];
								}
								callednumber = callednumber_list[0];
								show_callednumber = tailcallednumber;
							}catch (Exception e){
								e.printStackTrace();
							}

							if(GlobalVariable.ctNumberMap.get(callednumber)!=null) {
								String sta_callednumber = callednumber;
								final String callednumber_final = callednumber;
								final String show_callednumber_final = show_callednumber;

								//20211220在已经获取到callednumber并确认callednumber归属映射表时，更新电话记录的callednumber
								new Thread() {
									@RequiresApi(api = Build.VERSION_CODES.N)
									@Override
									public void run() {
										HttpRequest request = new HttpRequest();
										String rs = request.postJson(
												"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoCalledNum",
												"{\"type\":\"post pstn info\", " +
														"\"callid\":\"" + uid + "\", " +
														"\"callednumber\":\"" + sta_callednumber + "\", " +
														"}"
										);
									}
								}.start();

								if (zhuboUri.equals(localUri)) {

									LED_Media_Notify();
									GlobalVariable.PhoneRedTitle = true;
									new Thread() {
										@Override
										public void run() {
											//MyCall call1 = new MyCall(account, -1);
											if (currentCallNum != allCallinList.size()) {
												currentCallNum = allCallinList.size();
											}
											MyCallWithState callWithState = new MyCallWithState();
											//callWithState.mycall = call1;
											callWithState.state = CALL_STATE_TYPE.ZHUBO_INIT;
											callWithState.type = CALL_TYPE.WAITZHUBO;
											callWithState.outcalluri = outCallUri;
											//callWithState.callednumber = "0000";//callednumber;

											callWithState.callednumber = callednumber_final;
											callWithState.show_callednumber = show_callednumber_final;

											callWithState.uid = uid;
											callWithState.id = currentCallNum;
											SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
											callWithState.callintime = df.format(new Date());

											broadcastCallType(callWithState, CALL_TYPE.WAITZHUBO);

											//此处的id可能需要做单例控制，因为是跨线程增减
											allCallinList.add(currentCallNum, callWithState);
											currentCallNum++;
											//callinList.add(DataConvertUtil.putData(currentCallNum.toString(),standardDateFormat.format(new Date()),"来自导播",outCallUri));
											//callinListAdapter.notifyDataSetChanged();

											//查询电话信息
											String pstn = outCallUri.replace("<sip:", "");
											String[] pstn_list = pstn.split("@");
											pstn = pstn_list[0];
											String finalPstn = pstn;
											callWithState.pstn = pstn;


											getCallRecordInfo(callWithState, finalPstn, callWithState.uid);
											String tailnumber = "";
											if (callWithState.pstn.length() >= 4) {
												tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
											} else {
												tailnumber = callWithState.pstn;
											}

											callWithState.showitem = DataConvertUtil.putData(
													(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
													standardDateFormat.format(new Date()),
													callWithState.from,
													callWithState.pstn,
													tailnumber,
													callWithState.pstnmark,
													callWithState.content,
													String.valueOf(callWithState.id + 1),
													callWithState.uid,
													callWithState.callednumber);
											callinList.add(callWithState.showitem);


											try {

												String zhubo = zhuboUri.replace("<sip:", "");
												String[] zhubo_list = zhubo.split("@");
												zhubo = zhubo_list[0];
												HttpRequest request = new HttpRequest();
												String rs = request.postJson(
														GlobalVariable.SEHTTPServer + "/incoming-call",
														"{" +
																"\"outcall\":\"" + callWithState.pstn + "\"," +
																"\"contact\":\"" + callWithState.callednumber + "\"," +
																"\"daobo\":\"\"," +
																"\"zhubo\":\"" + zhubo + "\"," +
																"\"callintime\":\""+callWithState.callintime+"\"," +
																"\"code\":" + 200 + "}"
												);
											} catch (Exception e) {
												//e.printStackTrace();
												Log.e("SEHTTPServer", "指令异常");
											}


											Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
											m2.sendToTarget();
										}
									}.start();
								}
							}
						}
						else if(msgType.equals("ACK ZHUBO LIVE MAKE CALL")){
							String callid = msg_json.get("callid").toString();
							String callUri = msg_json.get("callUri").toString();
							if(callUri.equals(localUri)) {
								String msg_to_send = "{ \"msgType\": \"ZHUBO LIVE MAKE CALL\", \"callUri_id\": \"" + callid + "\", \"OutCallUri\": \"" + currentCallWithState.outcalluri + "\", \"ZhuBoCallUri\": \"" + localUri + "\", \"Record\":"+currentCallWithState.record+" }";
//								Intent intent_ack = new Intent(context, JWebSocketClientService.class);
//								intent_ack.putExtra("msg_to_send", msg_to_send);
//								startService(intent_ack);
								if(wsControl!=null)wsControl.sendMessage(msg_to_send);
							}
						}
						//用于确定电话由哪一个导播已经接听，其他电话界面上移除该通电话
						else if(msgType.equals("DAOBO ANSWER CALL")){
							//暂不处理=>2020-03-04修改方案,
							String status = String.valueOf(msg_json.get("status"));
							if(status.equals("200")){
								String outCallUri = msg_json.get("OutCallUri").toString();
								String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();//这边不放被挂断的导播而是接听的那路导播
								if(daoBoCallUri.equals(localUri)){
									//处理当接通时有正在接听电话的话，其他电话

									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											//20210715处理导播同步显示
											String msg_to_send_sync = "{ \"msgType\": \"DAOBO SYNC SHOW CALL\"" +
													", \"DaoBoCallUri\": \""+callws.daobocalluri+"\"" +
													", \"OutCallUri\": \""+callws.outcalluri+"\"" +
													", \"uid\": \""+callws.uid+"\"" +
													", \"contacturi\":"+callws.callednumber+" }";
											if(wsControl!=null)wsControl.sendMessage(msg_to_send_sync);

											callws.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
											callws.type = CALL_TYPE.DAOBOACPT;
											callws.accepttime = GlobalVariable.hms_df.format(new Date());
											localacceptcall = callws;
											broadcastCallType(callws, CALL_TYPE.DAOBOACPT);
											if(callws.autoswitch)
											{
												//导播通知SIP服务器，电话转给主播接听
												callws.state = CALL_STATE_TYPE.DAOBO_SWITCH;
												//broadcastCallType(callws,CALL_TYPE.WAITZHUBO);
												String msg_to_send = "{ \"msgType\": \"DAOBO TO ZHUBO\", \"DaoBoCallUri\": \""
														+ localUri//这里是导播发消息
														+ "\", \"ZhuBoCallUri\": \""
														+ MainActivity.zhuboUri
														+ "\", \"OutCallUri\": \""
														+ callws.outcalluri + "\" }";
												if(wsControl!=null)wsControl.sendMessage(msg_to_send);
												callws.autoswitch = false;
												callws.wait_ackaccept = false;
												Message m2 = Message.obtain(MainActivity.handler_,1001,null);
												m2.sendToTarget();
											}
											else{

												callws.wait_ackaccept = false;
												updateInfoAndButton();
											}
										}
									}
								}
								else if(GlobalVariable.charactername.equals("director")){//判断不是自己接听的电话移除
									int remoteCallNum = -1;
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri) && !callws.syncshow) {
											remoteCallNum = callws.id;
										}
									}
									if (remoteCallNum != -1) {
										try {
											allCallinList.remove(remoteCallNum);
											callinList.remove(remoteCallNum);
											updateShowIndex();
											if(callinListSelectedIdx == remoteCallNum)
											{
												callinListSelectedIdx = -1;
											}
											currentCallNum--;
											resetMainView();
											refreshCallList();
											if(GlobalVariable.viewmodel==1) {
												updateOperateButton();
											}
											else if(GlobalVariable.viewmodel==3){
												updateGsOperateButton();
											}
											myAdapter.setDataList(allCallinList);
											mbcAdapter.setDataList(allCallinList);
											//callinListmbcAdapter.setDataList(allCallinList);
										} catch (Exception e) {
											System.out.println(e);
										}
									}
								}
							}
						}
						else if(msgType.equals("ZHUBO ANSWER CALL")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							String zhuBoCallUri = msg_json.get("ZhuBoCallUri").toString();//接听的那路主播
							if(localUri.equals(zhuBoCallUri))
							{
								for (MyCallWithState callws : allCallinList) {
									if (callws.outcalluri.equals(outCallUri)) {
										callws.state = CALL_STATE_TYPE.ZHUBO_ACCEPT;
										callws.type = CALL_TYPE.ZHUBOACPT;
										callws.accepttime = GlobalVariable.hms_df.format(new Date());
										localacceptcall = callws;
										broadcastCallType(callws, CALL_TYPE.ZHUBOACPT);
										if(callws.autoswitch && !callws.syncshow) {
											//主播通知SIP服务器，应答主播返回电话
											//currentCallWithState.state = CALL_STATE_TYPE.ZHUBO_SWITCH;
											String msg_to_send = "{ \"msgType\": \"ZHUBO TO DAOBO\", \"OutCallUri\": \""
													+ callws.outcalluri
													+ "\", \"DaoBoCallUri\": \""
													+ callws.daobocalluri
													+ "\", \"ZhuBoCallUri\": \""
													+ MainActivity.zhuboUri + "\" }";
//											Intent intent_ack = new Intent(MainActivity.this, JWebSocketClientService.class);
//											intent_ack.putExtra("msg_to_send", msg_to_send);
//											startService(intent_ack);
											if(wsControl!=null)wsControl.sendMessage(msg_to_send);

											try {
												//broadcastCallType(callws,CALL_TYPE.WAITDAOBO);
												allCallinList.remove(callws.id);
												callinList.remove(callws.id);
												updateShowIndex();
												if(callinListSelectedIdx == callws.id)
												{
													callinListSelectedIdx = -1;
												}
												currentCallNum--;
												if(GlobalVariable.viewmodel==1) {
													updateOperateButton();
												}
												else if(GlobalVariable.viewmodel==3){
													updateGsOperateButton();
												}
												resetMainView();
												refreshCallList();
												myAdapter.setDataList(allCallinList);
												mbcAdapter.setDataList(allCallinList);

												//主动移除掉当前电话后，将下一通电话自动选中
												if(allCallinList.size()>0){
													currentCallWithState = allCallinList.get(0);
												}

											} catch (Exception e) {
												System.out.println(e);
											}
											callws.autoswitch = false;
											callws.wait_ackaccept = false;
											Message m2 = Message.obtain(MainActivity.handler_,1001,null);
											m2.sendToTarget();
										}
										else{
											callws.wait_ackaccept = false;
											updateInfoAndButton();
										}

									}
								}
							}
						}
						//当导播将自己的电话转接给其他导播时，应该发送相关信息
						else if(msgType.equals("DAOBO TO DAOBO")){
							System.out.println("THIS STATE NO EXIST");
						}
						//其他导播收到一条被转发过来的外线电话时，有两种情况，已转和未转给主播
						else if(msgType.equals("OUT SWITCH MAKE CALL")){
							msg_json.put("ToOutCallUri",new JSONArray(String.valueOf(msg_json.get("ToOutCallUri")).replaceAll("\n","")));

							//只有当前是导播的时候才会接收此消息，发现是从另一个导播转过来的电话（因下线或者主动转接）
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							JSONArray outCallUriArray = msg_json.getJSONArray("ToOutCallUri");
							List<OutCallUri> outCallUriList = JSON.parseArray(outCallUriArray.toString(), OutCallUri.class);

							if(localUri.equals(daoBoCallUri))//只有当前是导播的时候才能接受电话
							{
								//需要给中心服务器拨打电话，需要维持的有哪些状态
								//如果是导播还没有接的，WAITDAOBO或者NEWINIT,应该到新导播还是还原这个状态
								//如果是在主播端还没返回的状态，同步数据库状态即可，就是当前状态显示即可
								//如果是在导播端的接听或者保持状态，在这边移除之后，在另一边变成新呼入电话NEWINIT
								//其他消息应该就从数据库中直接读取

								for(OutCallUri outcall : outCallUriList)
								{
									String outCallUri = outcall.getOutcalluri();
									String uid = outcall.getUid();
									Integer calltype = outcall.getType();
									Integer state = outcall.getState();

									String callednumber = "0000";
									String show_callednumber = "0000";
									try {

										Log.e("OUT SWITCH MAKE CALL",""+outcall.getCallednumber());
										String contacturi = outcall.getCallednumber();
										String callednumber_all = contacturi.replace("<sip:", "");
										String[] callednumber_list = callednumber_all.split("@");
										String tailcallednumber = "";
										if (callednumber_list[0].length() >= 4) {
											tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
										} else {
											tailcallednumber = callednumber_list[0];
										}
										callednumber = callednumber_list[0];
										show_callednumber = tailcallednumber;
									}catch (Exception e){
										e.printStackTrace();
									}
									final String callednumber_final = callednumber;
									final String show_callednumber_final = show_callednumber;

									//导播接听或保持状态都转化为新接入电话状态
									if(calltype==CALL_TYPE.DAOBOACPT
											||calltype==CALL_TYPE.DAOBOHOLD
											||calltype==CALL_TYPE.NEWINIT)
									{
										new Thread() {
											@Override
											public void run() {
												//MyCall call1 = new MyCall(account, -1);

												MyCallWithState callWithState = new MyCallWithState();
												//callWithState.mycall = call1;
												callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
												callWithState.type = CALL_TYPE.NEWINIT;
												callWithState.outcalluri = outCallUri;
												callWithState.uid = uid;
												callWithState.daobocalluri = localUri;
												SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
												callWithState.callintime = df.format(new Date());

												callWithState.callednumber = callednumber_final;
												callWithState.show_callednumber = show_callednumber_final;

												try {
													currentCallNum = allCallinList.size();
													callWithState.id = currentCallNum;
													allCallinList.add(currentCallNum, callWithState);
													currentCallNum++;
												}catch (Exception e){
													e.printStackTrace();
												}

												//查询电话信息
												String pstn = outCallUri.replace("<sip:","");
												String[] pstn_list = pstn.split("@");
												pstn = pstn_list[0];
												String finalPstn = pstn;
												callWithState.pstn = pstn;

												getCallRecordInfo(callWithState,finalPstn,callWithState.uid);

												if(callWithState.dailout){
													//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
													//pstn = pstn.replaceFirst("00","");
                                                    pstn = pstn.substring(2);
												}

												String tailnumber = "";
												if(callWithState.pstn.length()>=4) {
													tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
												}
												else{
													tailnumber = callWithState.pstn;
												}

												callWithState.showitem = DataConvertUtil.putData(
														(callWithState.name==null|| callWithState.name.equals("null") || callWithState.name.equals(""))?tailnumber:callWithState.name,
														standardDateFormat.format(new Date()),
														callWithState.from,
														pstn,
														tailnumber,
														callWithState.pstnmark,
														callWithState.content,
														String.valueOf(callWithState.id+1),
														callWithState.uid,
														callWithState.callednumber);
												callinList.add(callWithState.showitem);
												Message m2 = Message.obtain(MainActivity.handler_,1001,null);
												m2.sendToTarget();
												autoSkipToMainActivity();
											}
										}.start();
									}
									//已经转给主播的状态维持原状态不变,
									else if(calltype==CALL_TYPE.WAITDAOBO
											||calltype==CALL_TYPE.ZHUBOHOLD
											||calltype==CALL_TYPE.ZHUBOACPT
											||calltype==CALL_TYPE.LIVEACPT
											||calltype==CALL_TYPE.LIVEHOLD
											||calltype==CALL_TYPE.WAITZHUBO)
									{
										//先不初始化MyCall，等到需要拨打电话的时候再添加对象
										new Thread() {
											@Override
											public void run() {
												//MyCall call1 = new MyCall(account, -1);

												MyCallWithState callWithState = new MyCallWithState();
												callWithState.mycall = null;
												callWithState.state = state;
												callWithState.type = calltype;
												callWithState.outcalluri = outCallUri;
												callWithState.uid = uid;
												callWithState.daobocalluri = localUri;
												SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
												callWithState.callintime = df.format(new Date());
												callWithState.callednumber = callednumber_final;
												callWithState.show_callednumber = show_callednumber_final;

												try{
													currentCallNum = allCallinList.size();
													callWithState.id = currentCallNum;
													allCallinList.add(currentCallNum,callWithState);
													currentCallNum++;
												}catch (Exception e)
												{
													e.printStackTrace();
												}


												//查询电话信息
												String pstn = outCallUri.replace("<sip:","");
												String[] pstn_list = pstn.split("@");
												pstn = pstn_list[0];
												String finalPstn = pstn;
												callWithState.pstn = pstn;

												getCallRecordInfo(callWithState,finalPstn,callWithState.uid);
												if(callWithState.dailout){
													//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
													//pstn = pstn.replaceFirst("00","");
													pstn = pstn.substring(2);
												}

												String tailnumber = "";
												if(callWithState.pstn.length()>=4) {
													tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
												}
												else{
													tailnumber = callWithState.pstn;
												}

												callWithState.showitem = DataConvertUtil.putData(
														(callWithState.name==null|| callWithState.name.equals("null") || callWithState.name.equals(""))?tailnumber:callWithState.name,
														standardDateFormat.format(new Date()),
														callWithState.from,
														pstn,
														tailnumber,
														callWithState.pstnmark,
														callWithState.content,
														String.valueOf(callWithState.id+1),
														callWithState.uid,
														callWithState.callednumber);
												callinList.add(callWithState.showitem);
												Message m2 = Message.obtain(MainActivity.handler_,1001,null);
												m2.sendToTarget();
												autoSkipToMainActivity();
											}
										}.start();

									}
								}
							}
							//主播需要更新节目中的callWithState的daobocalluri
							if(!GlobalVariable.charactername.equals("director")){
								for(OutCallUri outcall : outCallUriList)
								{
									for(MyCallWithState callws : allCallinList){
										if(callws.outcalluri.equals(outcall.getOutcalluri())){
											callws.daobocalluri = daoBoCallUri;
										}
									}
								}
							}
						}
						else if(msgType.equals("ACK DAOBO HOLD")){
							if(GlobalVariable.charactername.equals("director")) {
								String status = msg_json.get("status").toString();
								String outCallUri = msg_json.get("OutCallUri").toString();
								if (status.equals("200")) {
									//根据外线电话确定是哪一通需要取消wait状态并且切换
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
											callws.hold = true;
											broadcastCallType(callws,CALL_TYPE.DAOBOHOLD);
											//callws.state = CALL_STATE_TYPE.DAOBO_HOLD;
										}
									}
									updateInfoAndButton();
								}
								else{
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
										}
									}
									updateInfoAndButton();
								}
							}
						}
						else if(msgType.equals("ACK DAOBO UNHOLD")) {
							if (GlobalVariable.charactername.equals("director"))
							{
								String status = msg_json.get("status").toString();
								String outCallUri = msg_json.get("OutCallUri").toString();
								if (status.equals("200")) {
									//根据外线电话确定是哪一通需要取消wait状态并且切换
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
											callws.hold = false;
											callws.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
											callws.type = CALL_TYPE.DAOBOACPT;
											localacceptcall = callws;
											broadcastCallType(callws,CALL_TYPE.DAOBOACPT);
										}
									}
									updateInfoAndButton();
								}
								else{
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
										}
									}
									updateInfoAndButton();
								}
							}
						}
						else if(msgType.equals("ACK ZHUBO HOLD")){
							if(GlobalVariable.charactername.equals("broadcaster")) {
								String status = msg_json.get("status").toString();
								String outCallUri = msg_json.get("OutCallUri").toString();
								if (status.equals("200")) {
									//根据外线电话确定是哪一通需要取消wait状态并且切换
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
											callws.hold = true;
											broadcastCallType(callws,CALL_TYPE.ZHUBOHOLD);
											//callws.state = CALL_STATE_TYPE.ZHUBO_ACCEPT;
										}
									}
									updateInfoAndButton();
								}
								else{
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
										}
									}
									updateInfoAndButton();
								}
							}
						}
						else if(msgType.equals("ACK ZHUBO UNHOLD")){//针对主播的消息
							if(GlobalVariable.charactername.equals("broadcaster"))
							{
								String status = msg_json.get("status").toString();
								String outCallUri = msg_json.get("OutCallUri").toString();
								if(status.equals("200"))
								{
									//根据外线电话确定是哪一通需要取消wait状态并且切换
									for(MyCallWithState callws :allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
											callws.hold = false;
											localacceptcall = callws;
											if(callws.dailout){
												callws.type = CALL_TYPE.DAOBOACPT;
												broadcastCallType(callws, CALL_TYPE.DAOBOACPT);
											}
											else {
												callws.type = CALL_TYPE.ZHUBOACPT;
												broadcastCallType(callws, CALL_TYPE.ZHUBOACPT);
											}
											//callws.state = CALL_STATE_TYPE.ZHUBO_ACCEPT;
										}
									}
									updateInfoAndButton();
								}
								else{
									for(MyCallWithState callws :allCallinList) {
										if (callws.outcalluri.equals(outCallUri)) {
											callws.wait_ackhold = false;
										}
									}
									updateInfoAndButton();
								}
							}
						}
						else if(msgType.equals("ACK VIRTUAL HOLD")){
							//if(GlobalVariable.charactername.equals("broadcaster")) {
								String status = String.valueOf(msg_json.get("status"));
								String outCallUri = String.valueOf(msg_json.get("OutCallUri"));
								if (status.equals("200")) {
									//根据外线电话确定是哪一通需要取消wait状态并且切换
									for (MyCallWithState callws : allCallinList) {
										//if (callws.outcalluri.equals(outCallUri) && callws.virtual) {
										if (callws.outcalluri.equals(outCallUri)) {

												if (callws.accepttime == null) {
													callws.accepttime = GlobalVariable.hms_df.format(new Date());
												}

											callws.wait_ackhold = false;
											callws.hold = true;
											broadcastCallType(callws,CALL_TYPE.LIVEHOLD);
											//callws.state = CALL_STATE_TYPE.ZHUBO_HOLD;
										}
									}
									updateInfoAndButton();
								}
							//}
						}
						else if(msgType.equals("ACK VIRTUAL UNHOLD")){
							//if(GlobalVariable.charactername.equals("broadcaster")) {
								String status = msg_json.get("status").toString();
								String outCallUri = msg_json.get("OutCallUri").toString();
								if (status.equals("200")) {
									//根据外线电话确定是哪一通需要取消wait状态并且切换
									for (MyCallWithState callws : allCallinList) {
										if (callws.outcalluri.equals(outCallUri) && callws.virtual) {
											callws.wait_ackhold = false;
											callws.hold = false;
											broadcastCallType(callws,CALL_TYPE.LIVEACPT);
											//callws.state = CALL_STATE_TYPE.ZHUBO_ACCEPT;
										}
									}
									updateInfoAndButton();
								}
							//}
						}
						else if(msgType.equals("ACK DAOBO CANCEL SWITCH")){
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String zhuBoCallUri = msg_json.get("ZhuBoCallUri").toString();
							String outCallUri = msg_json.get("OutCallUri").toString();
							if(daoBoCallUri.equals(localUri))
							{
								//根据外线电话确定是哪一通需要取消转出转回了接听状态
								for (MyCallWithState callws : allCallinList) {
									if (callws.outcalluri.equals(outCallUri)) {
										autoHoldCallPdr();
										callws.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
										callws.type = CALL_TYPE.DAOBOACPT;
										broadcastCallType(callws,CALL_TYPE.DAOBOACPT);
									}
								}
							}
							if(zhuBoCallUri.equals(localUri))
							{
								//根据外线电话确定是哪一通需要取消转出转回了接听状态
								for (MyCallWithState callws : allCallinList) {
									if (callws.outcalluri.equals(outCallUri)) {
										try {
											allCallinList.remove(callws.id);
											callinList.remove(callws.id);
										}catch(Exception e){};
										updateShowIndex();
										if(callinListSelectedIdx == callws.id)
										{
											callinListSelectedIdx = -1;
										}
										currentCallNum--;
									}
								}
							}
							updateInfoAndButton();
							resetMainView();
						}
						else if(msgType.equals("CHANGE CALL TYPE")) {

							String uid = msg_json.get("uid").toString();
							String type = msg_json.get("type").toString();
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String zhuBoCallUri = msg_json.get("ZhuBoCallUri").toString();

							//if(daoBoCallUri.equals(localUri)||zhuBoCallUri.equals(localUri)) {
								for (MyCallWithState callws : allCallinList) {
									if (callws.uid.equals(uid)) {
//										if(callws.type != Integer.parseInt(type)) {
//											callws.live_lock = false;
//										}
										callws.type = Integer.parseInt(type);
										if(callws.type == CALL_TYPE.LIVEACPT && GlobalVariable.charactername.equals("director")){
											if(!callws.syncshow && GlobalVariable.sipmodel == 3) {
												callws.live_lock = true;
											}
											else{
												callws.live_lock = false;
											}

										}
										if(GlobalVariable.charactername.equals("director")){
											if(callws.type == CALL_TYPE.DAOBOACPT)
											{
												localacceptcall = callws;
											}
										}
										else{
											if(callws.type == CALL_TYPE.DAOBOACPT||callws.type == CALL_TYPE.ZHUBOACPT )
											{
												localacceptcall = callws;
											}
										}

									}
								}
							//}

							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
							updateInfoAndButton();
						}
						else if(msgType.equals("CHANGE CALL INFO")){
							String uid = msg_json.get("uid").toString();
							for (MyCallWithState callws : allCallinList) {
								if (callws.uid.equals(uid)) {
									//从数据库重新读取其相关属性
									new Thread() {
										@Override
										public void run() {
											updateListUserInfo(callws);
										}
									}.start();

								}
							}
						}
						else if(msgType.equals("VOICE MAIL")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							for (MyCallWithState callws : allCallinList) {
								if (callws.outcalluri.equals(outCallUri)) {
									callws.type = CALL_TYPE.ENDCALL;
								}
							}
							updateInfoAndButton();
						}
						//用于重连
						else if(msgType.equals("DAOBO REMAKE A CALL")&&GlobalVariable.charactername.equals("director")){
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							if(daoBoCallUri.equals(localUri)){
								//请注意此时本地维持的所有Call都是无效Call
								if(onlycall!=null)
								{
									CallOpParam prm = new CallOpParam();
									prm.setStatusCode(pjsip_status_code.PJSIP_SC_BUSY_HERE);
									try {
										onlycall.hangup(prm);
										Log.e("主动挂断电话","SOSOS");
									} catch (Exception e) {}
									onlycall.delete();
								}

								onlycall = new MyCall(account, -1);
								try{
									CallOpParam prm1 = new CallOpParam(true);
									onlycall.makeCall(sipUriParam, prm1);

									//接通电话后发送准备完毕请求
									new Thread() {
										@Override
										public void run() {
											try {
												HttpRequest request = new HttpRequest();
												String rs = request.postJson(
														GlobalVariable.SEHTTPServer + "/ready",
														"{\"code\":" + 200 + "}"
												);
											} catch (Exception e) {
												//e.printStackTrace();
												Log.e("SEHTTPServer","指令异常");
											}
										}
									}.start();

								} catch (Exception e) {
									e.printStackTrace();
									onlycall.delete();
									HttpRequest request = new HttpRequest();
									new Thread() {
										@Override
										public void run() {
											try {
												HttpRequest request = new HttpRequest();
												String rs = request.postJson(
														GlobalVariable.SEHTTPServer + "/ready-failure",
														"{\"code\":" + 200 + "}"
												);
											} catch (Exception e) {
												//e.printStackTrace();
												Log.e("SEHTTPServer","指令异常");
											}
										}
									}.start();
								}
							}
						}
						else if(msgType.equals("ZHUBO REMAKE A CALL")&&GlobalVariable.charactername.equals("broadcaster")){
							if(onlycall!=null)
							{
								onlycall.delete();
							}
							onlycall = new MyCall(account, -1);
							try{
								CallOpParam prm1 = new CallOpParam(true);
								onlycall.makeCall(sipUriParam, prm1);

								new Thread() {
									@Override
									public void run() {
										try {
											HttpRequest request = new HttpRequest();
											String rs = request.postJson(
													GlobalVariable.SEHTTPServer + "/ready",
													"{\"code\":" + 200 + "}"
											);
										} catch (Exception e) {
											//e.printStackTrace();
											Log.e("SEHTTPServer","指令异常");
										}
									}
								}.start();
							} catch (Exception e) {
								e.printStackTrace();
								onlycall.delete();

								new Thread() {
									@Override
									public void run() {
										try {
											HttpRequest request = new HttpRequest();
											String rs = request.postJson(
													GlobalVariable.SEHTTPServer + "/ready-failure",
													"{\"code\":" + 200 + "}"
											);
										} catch (Exception e) {
											//e.printStackTrace();
											Log.e("SEHTTPServer","指令异常");
										}
									}
								}.start();
							}
						}
						//用于外拨
						else if(msgType.equals("DAIL OUT MAKE CALL")) {
							//							"DaoboCallUri":"导播URI"
							//							"ZhuboCallUri":"主播URI"
							//							"OutCallUri":"外线URI"
							String state = String.valueOf(msg_json.get("status"));
							if(state.equals("200")||state.equals("0")){
//								if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3){
//									//耦合器模式此时才进行添加
//									String uid = msg_json.get("uid").toString();
//									String outCallUri = msg_json.get("OutCallUri").toString();
//
//									String callednumber = "0000";
//									String show_callednumber = "0000";
//									try {
//										String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
//										Log.e("contacturi",contacturi);
//										String callednumber_all = contacturi.replace("<sip:", "");
//										String[] callednumber_list = callednumber_all.split("@");
//										String tailcallednumber = "";
//										if (callednumber_list[0].length() >= 4) {
//											tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
//										} else {
//											tailcallednumber = callednumber_list[0];
//										}
//										callednumber = callednumber_list[0];
//										show_callednumber = tailcallednumber;
//									} catch (Exception e) {
//										e.printStackTrace();
//									}
//
//
//									final String callednumber_final = callednumber;
//									final String show_callednumber_final = show_callednumber;
//									String sta_callednumber = callednumber;
//									//20211220在已经获取到callednumber并确认callednumber归属映射表时，更新电话记录的callednumber
//									new Thread() {
//										@RequiresApi(api = Build.VERSION_CODES.N)
//										@Override
//										public void run() {
//											HttpRequest request = new HttpRequest();
//											String rs = request.postJson(
//													"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoCalledNum",
//													"{\"type\":\"post pstn info\", " +
//															"\"callid\":\"" + uid + "\", " +
//															"\"callednumber\":\"" + sta_callednumber + "\", " +
//															"}"
//											);
//										}
//									}.start();
//
//									String daoboCallUri = "";
//									String zhuboCallUri = "";
//									if (GlobalVariable.charactername.equals("director"))
//										daoboCallUri = String.valueOf(msg_json.get("DaoBoCallUri"));
//									else
//										zhuboCallUri = String.valueOf(msg_json.get("ZhuBoCallUri"));
//									if (daoboCallUri.equals(localUri) || zhuboCallUri.equals(localUri)) {
//										//添加一个接听的电话之前先保持之前的所有电话
//										autoHoldCallAll();
//
//										new Thread() {
//											@Override
//											public void run() {
//												MyCallWithState callWithState = new MyCallWithState();
//
//												callWithState.dailout = true;
//												callWithState.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
//												callWithState.type = CALL_TYPE.DAOBOACPT;
//												callWithState.outcalluri = outCallUri;
//												callWithState.uid = uid;
//												callWithState.daobocalluri = localUri;
//												SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
//												callWithState.callintime = df.format(new Date());
//												callWithState.callednumber = callednumber_final;
//												callWithState.show_callednumber = show_callednumber_final;
//												callWithState.accepttime = GlobalVariable.hms_df.format(new Date());
//
//												try {
//													currentCallNum = allCallinList.size();
//													callWithState.id = currentCallNum;
//													localacceptcall = callWithState;
//													allCallinList.add(currentCallNum, callWithState);
//													currentCallNum++;
//												} catch (Exception e) {
//													e.printStackTrace();
//												}
//
//												//查询电话信息
//												String pstn = outCallUri.replace("<sip:", "");
//												String[] pstn_list = pstn.split("@");
//												pstn = pstn_list[0];
//												callWithState.pstn = pstn;
//												//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
//												//pstn = pstn.replaceFirst("00", "");
//                                                pstn = pstn.substring(2);
//
//                                                Log.e("查数据库前电话号码/是否是通讯录",callWithState.pstn+callWithState.iscontact);
//												getCallRecordInfo(callWithState, callWithState.pstn, callWithState.uid);
//												Log.e("查数据库后电话号码/是否是通讯录",callWithState.pstn+callWithState.iscontact);
//												String tailnumber = "";
//												if (callWithState.pstn.length() >= 4) {
//													tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
//												} else {
//													tailnumber = callWithState.pstn;
//												}
//
//												callWithState.showitem = DataConvertUtil.putData(
//														(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
//														standardDateFormat.format(new Date()),
//														callWithState.from,
//														pstn,
//														tailnumber,
//														callWithState.pstnmark,
//														callWithState.content,
//														String.valueOf(callWithState.id + 1),
//														callWithState.uid,
//														callWithState.show_callednumber
//												);
//												callinList.add(callWithState.showitem);
//												localacceptcall = callWithState;
//												broadcastCallType(callWithState, CALL_TYPE.DAOBOACPT);
//
//												//20211227添加完呼出电话后自动选中呼出电话(测处理过程过早状态未改变)
////												int index = GlobalVariable.ctNumberMap.get(sta_callednumber)==null?0:GlobalVariable.ctNumberMap.get(sta_callednumber);
////												if(index!=0) {
////													GlobalVariable.gs_selected_index = index;
////													Integer id_index = GlobalVariable.ctIdMap.get(index);
////													if (id_index != null && id_index >= 0 && id_index < allCallinList.size()) {
////														currentCallWithState = allCallinList.get(id_index);
////													} else {
////														currentCallWithState = null;
////													}
////													for (MyCallWithState callws : allCallinList) {
////														callws.selected_line_btn = false;
////													}
////													if (currentCallWithState != null) {
////														currentCallWithState.selected_line_btn = true;
////													}
////													updateInfoAndButton();
////												}
//
//												Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
//												m2.sendToTarget();
//											}
//										}.start();
//									}
//								}
								if(false){}
								else {
									String uid = msg_json.get("uid").toString();
									String outCallUri = msg_json.get("OutCallUri").toString();

									String callednumber = "0000";
									String show_callednumber = "0000";
									try {
										String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
										String callednumber_all = contacturi.replace("<sip:", "");
										String[] callednumber_list = callednumber_all.split("@");
										String tailcallednumber = "";
										if (callednumber_list[0].length() >= 4) {
											tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
										} else {
											tailcallednumber = callednumber_list[0];
										}
										callednumber = callednumber_list[0];
										show_callednumber = tailcallednumber;
									} catch (Exception e) {
										e.printStackTrace();
									}
									final String callednumber_final = callednumber;
									final String show_callednumber_final = show_callednumber;
									String sta_callednumber = callednumber;
									//20211220在已经获取到callednumber并确认callednumber归属映射表时，更新电话记录的callednumber
									new Thread() {
										@RequiresApi(api = Build.VERSION_CODES.N)
										@Override
										public void run() {
											HttpRequest request = new HttpRequest();
											String rs = request.postJson(
													"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/updateCallRecordInfoCalledNum",
													"{\"type\":\"post pstn info\", " +
															"\"callid\":\"" + uid + "\", " +
															"\"callednumber\":\"" + sta_callednumber + "\", " +
															"}"
											);
										}
									}.start();

									String daoboCallUri = "";
									String zhuboCallUri = "";
									if (GlobalVariable.charactername.equals("director"))
										daoboCallUri = String.valueOf(msg_json.get("DaoBoCallUri"));
									else
										zhuboCallUri = String.valueOf(msg_json.get("ZhuBoCallUri"));
									if (daoboCallUri.equals(localUri) || zhuboCallUri.equals(localUri)) {
										for (MyCallWithState callws : allCallinList) {
											if (callws.outcalluri.equals(outCallUri)) {
												//呼出接通以后
												LED_Media_Pause();
												callws.type = CALL_TYPE.DAOBOACPT;
												callws.uid = uid;
												//callws.callednumber = "0000";//callednumber;
												callws.callednumber = callednumber_final;
												callws.show_callednumber = callednumber_final;
												callws.showitem.replace("callednumber",callws.callednumber);

												SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
												callws.callintime = df.format(new Date());
												callws.accepttime = GlobalVariable.hms_df.format(new Date());

												localacceptcall = callws;
												broadcastCallType(callws, CALL_TYPE.DAOBOACPT);
												Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
												m2.sendToTarget();
											}
										}
									}
								}
							}
							else{
								MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(MainActivity.this,"呼出错误,请挂断").create();
								msgNotifyDialog.show();
								if(GlobalVariable.viewmodel == 1)
									msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
								else if(GlobalVariable.viewmodel == 2)
									msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
								else if(GlobalVariable.viewmodel == 3)
									msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
								msgNotifyDialog.getWindow().setLayout(500, 300);
							}

							//
							//							if(daoboCallUri.equals(localUri))
							//							{
							//								//当前是导播拨打电话（外线接听后对于导播来说也是自动接听）
							//								new Thread() {
							//									@Override
							//									public void run() {
							//										//MyCall call1 = new MyCall(account, -1);
							//										MyCallWithState callWithState = new MyCallWithState();
							//										callWithState.dailout = true;
							//										callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
							//										callWithState.type = CALL_TYPE.DAOBOACPT;
							//										callWithState.outcalluri = outCallUri;
							//										callWithState.uid = uid;
							//										callWithState.daobocalluri = localUri;
							//										SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
							//										callWithState.callintime = df.format(new Date());
							//
							//										callWithState.id = currentCallNum;
							//										allCallinList.add(currentCallNum,callWithState);
							//										currentCallNum++;
							//
							//										//查询电话信息
							//										String pstn = outCallUri.replace("<sip:","");
							//										String[] pstn_list = pstn.split("@");
							//										pstn = pstn_list[0];
							//										String finalPstn = pstn;
							//										callWithState.pstn = pstn;
							//
							//										getCallRecordInfo(callWithState,finalPstn,callWithState.uid);
							//										String tailnumber = "";
							//										if(callWithState.pstn.length()>=4) {
							//											tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
							//										}
							//										else{
							//											tailnumber = callWithState.pstn;
							//										}
							//
							//										callWithState.showitem = DataConvertUtil.putData(
							//												(callWithState.name==null||callWithState.name=="null"||callWithState.name=="")?tailnumber:callWithState.name,
							//												standardDateFormat.format(new Date()),
							//												callWithState.from,
							//												callWithState.pstn,
							//												tailnumber,
							//												callWithState.pstnmark,
							//												callWithState.content,
							//												String.valueOf(callWithState.id+1));
							//										callinList.add(callWithState.showitem);
							//										Message m2 = Message.obtain(MainActivity.handler_,1001,null);
							//										m2.sendToTarget();
							//									}
							//								}.start();
							//
							//							}
							//							else if(zhuboCallUri.equals(localUri))
							//							{
							//								//主播先不处理（不知道是不是直接处理成直播状态）
							//								//暂定在仅主播模式下可操作
							//							}
						}
						else if(msgType.equals("GET STAT NUM")){
							String totalnum = String.valueOf(msg_json.get("totalnum"));
							String daobonum = String.valueOf(msg_json.get("daobonum"));
							String zhubonum = String.valueOf(msg_json.get("zhubonum"));
							String switchnum = String.valueOf(msg_json.get("switchnum"));
							String todaobonum = String.valueOf(msg_json.get("todaobonum"));
							String tozhubonum = String.valueOf(msg_json.get("tozhubonum"));
							GlobalVariable.sip_totalnum = totalnum;
							GlobalVariable.sip_daobonum = daobonum;
							GlobalVariable.sip_zhubonum = zhubonum;
							GlobalVariable.sip_switchnum = switchnum;
							GlobalVariable.sip_todaobonum = todaobonum;
							GlobalVariable.sip_tozhubonum = tozhubonum;

							//有空指针异常待修改
							if(SettingActivity.handler_!=null) {
								Message m2 = Message.obtain(SettingActivity.handler_, 1003, null);
								m2.sendToTarget();
							}

							new Thread() {
								@Override
								public void run() {
									try {
										HttpRequest request = new HttpRequest();
										String rs = request.postJson(
												GlobalVariable.SEHTTPServer + "/stats",
												"{" +
														"\"total\":\"" + totalnum + "\"," +
														"\"daobo\":\"" + daobonum + "\"," +
														"\"zhubo\":\"" + zhubonum + "\"," +
														"\"todaobo\":\"" + tozhubonum + "\"," +
														"\"tozhubo\":\"" + tozhubonum + "\"," +
														"\"code\":" + 200 + "}"
										);
									} catch (Exception e) {
										//e.printStackTrace();
										Log.e("SEHTTPServer","指令异常");
									}
								}
							}.start();


						}
						else if(msgType.equals("INNER MAKE CALL")){
							String fromsipid = String.valueOf(msg_json.get("fromsipid"));
							String tosipid = String.valueOf(msg_json.get("tosipid"));
							if(tosipid.equals(localUri)){

								GlobalVariable.innerstart = false;
								GlobalVariable.innernamefrom = String.valueOf(msg_json.get("fromname"));
								GlobalVariable.innernameto = String.valueOf(msg_json.get("toname"));
								GlobalVariable.innerchannelfrom = String.valueOf(msg_json.get("frompgm"));
								GlobalVariable.innerchannelto = String.valueOf(msg_json.get("topgm"));
								GlobalVariable.innercallfrom = String.valueOf(msg_json.get("fromsipid"));
								GlobalVariable.innercallto = String.valueOf(msg_json.get("tosipid"));
								GlobalVariable.innerrolefrom = String.valueOf(msg_json.get("fromrole"));
								GlobalVariable.innerroleto = String.valueOf(msg_json.get("torole"));
								if (CallActivity.instance != null && CallActivity.handler_!=null) {
									//CallActivity.instance.finish();
									Message m2 = Message.obtain(CallActivity.handler_,1003,null);
									m2.sendToTarget();
								}
								showCallActivity();
							}
							if(fromsipid.equals(localUri)){
								showCallActivity();
							}
						}
						else if(msgType.equals("INNER ANSWER CALL")){
							String fromsipid = String.valueOf(msg_json.get("fromsipid"));
							String tosipid = String.valueOf(msg_json.get("tosipid"));
							if(fromsipid.equals(localUri) || tosipid.equals(localUri)){
								//更新电话信息为接通
								if(GlobalVariable.charactername.equals("director"))
								{
									autoHoldCallPdrByInner();
								}
								GlobalVariable.innerstate = "通话中";
								if(CallActivity.handler_!=null){
									Message m2 = Message.obtain(CallActivity.handler_,1001,null);
									m2.sendToTarget();
								}
							}
						}
						else if(msgType.equals("INNER HANGUP")){
							String fromsipid = String.valueOf(msg_json.get("fromsipid"));
							String tosipid = String.valueOf(msg_json.get("tosipid"));
							String msg = String.valueOf(msg_json.get("msg"));
							if(fromsipid.equals(localUri) || tosipid.equals(localUri)){
								if(msg.equals("外线呼入"))
								{
									GlobalVariable.innerstate = "外线呼入";
									if(CallActivity.handler_!=null){
										Message m2 = Message.obtain(CallActivity.handler_,1002,null);
										m2.sendToTarget();
									}
								}
								else{
									//更新电话信息为挂断
									String  canlinkactivity =  CallActivity.handler_==null?"否":"是";
									Log.e("准备更新界面为挂断",canlinkactivity);
									GlobalVariable.innerstart = false;
									GlobalVariable.innerstate = "对方挂断";
									if(CallActivity.handler_!=null){
										Message m2 = Message.obtain(CallActivity.handler_,1002,null);
										m2.sendToTarget();
									}
								}
							}
						}
						//20210715处理跨栏目内通协议
						else if(msgType.equals("ACK INNER2 MAKE CALL")){
							String fromsipid = String.valueOf(msg_json.get("fromsipid"));
							String tosipid = String.valueOf(msg_json.get("tosipid"));
							if(fromsipid.equals(localUri)){
								//
								//更新电话信息为接通
								if(GlobalVariable.charactername.equals("director"))
								{
									autoHoldCallPdrByInner();
								}
								GlobalVariable.innerstate = "通话中";
								if(CallActivity.handler_!=null){
									Message m2 = Message.obtain(CallSkipActivity.handler_,1001,null);
									m2.sendToTarget();
								}
							}
						}
						else if(msgType.equals("ACK INNER2 ANSWER CALL")){
							String fromsipid = String.valueOf(msg_json.get("fromsipid"));
							String tosipid = String.valueOf(msg_json.get("tosipid"));
							if(tosipid.equals(localUri)){
								//
								//更新电话信息为接通
								if(GlobalVariable.charactername.equals("director"))
								{
									autoHoldCallPdrByInner();
								}
								GlobalVariable.innerstate = "通话中";
								if(CallActivity.handler_!=null){
									Message m2 = Message.obtain(CallSkipActivity.handler_,1001,null);
									m2.sendToTarget();
								}
							}
						}
						else if(msgType.equals("ACK INNER2 HANGUP")){
							//INNER2 HANGUP消息的响应消息，说明电话已经确实挂断
							String fromsipid = String.valueOf(msg_json.get("fromsipid"));
							String tosipid = String.valueOf(msg_json.get("tosipid"));
							String msg = String.valueOf(msg_json.get("msg"));

							GlobalVariable.innerstart = false;
							GlobalVariable.innerstate = "对方挂断";
							if(CallActivity.handler_!=null){
								Message m2 = Message.obtain(CallActivity.handler_,1002,null);
								m2.sendToTarget();
							}
						}
						else if(msgType.equals("CHANGE SYS CONFIG")){

								String config = String.valueOf(msg_json.get("config"));
								String state = String.valueOf(msg_json.get("state"));
								String local = String.valueOf(msg_json.get("local"));
								if (!local.equals(localUri) && config.equals("[导播模式]")) {
									autoSkipToMainActivity();
									//弹出提示框，提示系统模式已更改，请重新登录

									SysRestartDialog sysRestartDialog = new SysRestartDialog.Builder(MainActivity.this, "模式更改需重新登陆").create();
									sysRestartDialog.show();
									if(GlobalVariable.viewmodel == 1)
										sysRestartDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
									else if(GlobalVariable.viewmodel == 2)
										sysRestartDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
									else if(GlobalVariable.viewmodel == 3)
										sysRestartDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
									sysRestartDialog.getWindow().setLayout(500, 400);
								} else if (!local.equals(localUri)) {
									String notify_string = config;
//							if(config.equals("autoswitch")) notify_string = "自动转接";
//							if(config.equals("allowblacklist")) notify_string = "允许黑名单";
//							if(config.equals("whitelistmodel")) notify_string = "白名单模式";
//							if(config.equals("autoswitch")) notify_string = "置忙";
									//收到之后立即进行状态更新,以及提示当前用户状态更改
									new Thread() {
										@Override
										public void run() {
											try {
												HttpRequest request = new HttpRequest();
												String rs = request.postJson(
														"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getSysConfig",
														"{\"userid\":\"" + GlobalVariable.userid + "\"," +
																"\"pgmid\":\"" + GlobalVariable.pgmid + "\" }"
												);
												JSONObject rs_json = new JSONObject(rs);
												JSONObject rs_json_userstate = rs_json.getJSONObject("data").getJSONObject("userstate");
												JSONObject rs_json_config = rs_json.getJSONObject("data").getJSONObject("config");

												GlobalVariable.allowblacklist = rs_json_config.get("allowblacklist").toString().equals("0") ? false : true;
												GlobalVariable.whitelistmodel = rs_json_config.get("whitelistmodel").toString().equals("0") ? false : true;
												GlobalVariable.busyall = Integer.parseInt(rs_json_config.get("busyall").toString());
												GlobalVariable.autoswitch = rs_json_config.get("autoswitch").toString().equals("0") ? false : true;
												GlobalVariable.sipmodel = Integer.parseInt(rs_json_config.get("sipmodel").toString());
												GlobalVariable.viewmodel = Integer.parseInt(rs_json_config.get("viewmodel").toString());

												//20210701  添加获得线路置忙配置
												HttpRequest requestX = new HttpRequest();
												String rsX = requestX.postJson(
														"http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/getLineMapping",
														"{}"
												);
												JSONObject rs_jsonX = new JSONObject(rsX);
												if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy1"))) == 1)
												{GlobalVariable.linebusy1 = true;} else {GlobalVariable.linebusy1 = false;}
												if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy2"))) == 1)
												{GlobalVariable.linebusy2 = true;} else {GlobalVariable.linebusy2 = false;}
												if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy3"))) == 1)
												{GlobalVariable.linebusy3 = true;} else {GlobalVariable.linebusy3 = false;}
												if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy4"))) == 1)
												{GlobalVariable.linebusy4 = true;} else {GlobalVariable.linebusy4 = false;}
												if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy5"))) == 1)
												{GlobalVariable.linebusy5 = true;} else {GlobalVariable.linebusy5 = false;}
												if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy6"))) == 1)
												{GlobalVariable.linebusy6 = true;} else {GlobalVariable.linebusy6 = false;}

												//获得解析数据后更新页面
												Message m2 = Message.obtain(SettingActivity.handler_, 1001, null);
												m2.sendToTarget();
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}.start();


									final Activity activity = ActivityController.getCurrentActivity();
									ActivityManager am = (ActivityManager) activity.getApplicationContext().getSystemService(activity.ACTIVITY_SERVICE);
									ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
									if (cn.getClassName().equals("org.pjsip.pjsua2.app.MainActivity")) {

										Message m2 = Message.obtain(MainActivity.handler_, 1014, notify_string);
										m2.sendToTarget();
									}

									//一定是先进主页面的，所以其他页面的定时器也可以从主页面去刷新
									if (cn.getClassName().equals("org.pjsip.pjsua2.app.HistoryActivity")) {
										Message m3 = Message.obtain(HistoryActivity.handler_, 1014, notify_string);
										m3.sendToTarget();
									}
									if (cn.getClassName().equals("org.pjsip.pjsua2.app.DailActivity")) {
										Message m4 = Message.obtain(DailActivity.handler_, 1014, notify_string);
										m4.sendToTarget();
									}
									if (cn.getClassName().equals("org.pjsip.pjsua2.app.SettingActivity")) {
										Message m5 = Message.obtain(SettingActivity.handler_, 1014, notify_string);
										m5.sendToTarget();
									}
									if (cn.getClassName().equals("org.pjsip.pjsua2.app.ContactsActivity")) {
										Message m6 = Message.obtain(ContactsActivity.handler_, 1014, notify_string);
										m6.sendToTarget();
									}
									if (cn.getClassName().equals("org.pjsip.pjsua2.app.CommunicateActivity")) {
										Message m7 = Message.obtain(CommunicateActivity.handler_, 1014, notify_string);
										m7.sendToTarget();
									}
									//获取相关本机配置，也就是用户配置，每个用户对应唯一分机号
									//从数据库获得本机状态并进行相应设置
									new Thread() {
										@Override
										public void run() {
											try {
												HttpRequest request = new HttpRequest();
												String rs = request.postJson(
														"http://" + GlobalVariable.SIPSERVER + ":8080/smart/contactWithSipServer/getSysConfig",
														"{\"userid\":\"" + GlobalVariable.userid + "\"," +
																"\"pgmid\":\"" + GlobalVariable.pgmid + "\" }"
												);
												JSONObject rs_json = new JSONObject(rs);
												JSONObject rs_json_userstate = rs_json.getJSONObject("data").getJSONObject("userstate");
												JSONObject rs_json_config = rs_json.getJSONObject("data").getJSONObject("config");

												GlobalVariable.online = rs_json_userstate.get("online").toString().equals("0") ? false : true;
												GlobalVariable.callring = rs_json_userstate.get("callring").toString().equals("0") ? false : true;
												GlobalVariable.communicatering = rs_json_userstate.get("communicatering").toString().equals("0") ? false : true;
												GlobalVariable.busystate = rs_json_userstate.get("communicatering").toString().equals("0") ? false : true;
												GlobalVariable.callnum = Integer.parseInt(rs_json_userstate.get("callnum").toString());
												GlobalVariable.waitnum = Integer.parseInt(rs_json_userstate.get("waitnum").toString());

												GlobalVariable.allowblacklist = rs_json_config.get("allowblacklist").toString().equals("0") ? false : true;
												GlobalVariable.whitelistmodel = rs_json_config.get("whitelistmodel").toString().equals("0") ? false : true;
												GlobalVariable.busyall = Integer.parseInt(rs_json_config.get("busyall").toString());
												GlobalVariable.autoswitch = rs_json_config.get("autoswitch").toString().equals("0") ? false : true;
												GlobalVariable.sipmodel = Integer.parseInt(rs_json_config.get("sipmodel").toString());
												GlobalVariable.viewmodel = Integer.parseInt(rs_json_config.get("viewmodel").toString());
											} catch (Exception e) {
												e.printStackTrace();
											}
										}
									}.start();
								}

						}
						else if(msgType.equals("ACK AUTO SWITCH WAIT CALL")){
							String daoBoCallUri = String.valueOf(msg_json.get("DaoBoCallUri"));
							String outCallUri = String.valueOf(msg_json.get("OutCallUri"));
							if(GlobalVariable.autoswitchwaitcall!=null
									&&daoBoCallUri.equals(localUri)
									&& outCallUri.equals(GlobalVariable.autoswitchwaitcall.outcalluri)){

								MyCallWithState tempcall = currentCallWithState;
								currentCallWithState = GlobalVariable.autoswitchwaitcall;
								updateCallRecordST(currentCallWithState.uid, 1, CALL_TYPE.WAITZHUBO);
								//先接电话再转电话
								acceptCall(null);
								currentCallWithState.autoswitch = true;
								currentCallWithState = tempcall;
							}
						}
						else if(msgType.equals("ZHUBO SYNC SHOW CALL")){
							//只有在仅导播的模式下，主播和导播端同步显示，但是主播端不允许操作
							if(GlobalVariable.charactername.equals("broadcaster") && GlobalVariable.sipmodel == 3){
								String frommsg = String.valueOf(msg_json.get("frommsg"));
								if(frommsg.equals("OUT MAKE CALL") ){
									String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
									String outCallUri = msg_json.get("OutCallUri").toString();
									String uid = msg_json.get("uid").toString();
									String callednumber = "0000";
									String show_callednumber = "0000";
									try {
										String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
										String callednumber_all = contacturi.replace("<sip:", "");
										String[] callednumber_list = callednumber_all.split("@");
										String tailcallednumber = "";
										if (callednumber_list[0].length() >= 4) {
											tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
										} else {
											tailcallednumber = callednumber_list[0];
										}
										callednumber = callednumber_list[0];
										show_callednumber = tailcallednumber;
									}catch (Exception e){
										e.printStackTrace();
									}
									final String callednumber_final = callednumber;
									final String show_callednumber_final = show_callednumber;
									//final String callednumber = callednumber_list[0];
										GlobalVariable.PhoneRedTitle = true;
										new Thread() {
											@Override
											public void run() {
												//MyCall call1 = new MyCall(account, -1);
												MyCallWithState callWithState = new MyCallWithState();
												//callWithState.mycall = call1;
												callWithState.syncshow = true;
												callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
												callWithState.type = CALL_TYPE.NEWINIT;
												callWithState.outcalluri = outCallUri;
												callWithState.uid = uid;
												callWithState.daobocalluri = localUri;
												//callWithState.callednumber = "0000";//callednumber;
												callWithState.callednumber = callednumber_final;
												callWithState.show_callednumber = show_callednumber_final;
												SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
												callWithState.callintime = df.format(new Date());

												try {
													currentCallNum = allCallinList.size();
													callWithState.id = currentCallNum;
													allCallinList.add(currentCallNum, callWithState);
													currentCallNum++;
												}catch (Exception e)
												{
													e.printStackTrace();
												}

												//查询电话信息
												String pstn = outCallUri.replace("<sip:","");
												String[] pstn_list = pstn.split("@");
												pstn = pstn_list[0];
												String finalPstn = pstn;
												callWithState.pstn = pstn;

												getCallRecordInfo(callWithState,finalPstn,callWithState.uid);
												String tailnumber = "";
												if(callWithState.pstn.length()>=4) {
													tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
												}
												else{
													tailnumber = callWithState.pstn;
												}
												callWithState.showitem = DataConvertUtil.putData(
														(callWithState.name==null || callWithState.name.equals("null") || callWithState.name.equals(""))?tailnumber:callWithState.name,
														standardDateFormat.format(new Date()),
														callWithState.from,
														callWithState.pstn,
														tailnumber,
														callWithState.pstnmark,
														callWithState.content,
														String.valueOf(callWithState.id+1),
														callWithState.uid,
														callWithState.callednumber
												);
												callinList.add(callWithState.showitem);
												Message m2 = Message.obtain(MainActivity.handler_,1001,callWithState);
												m2.sendToTarget();
												autoSkipToMainActivity();
											}
										}.start();

								}
								else if(frommsg.equals("DAIL OUT MAKE CALL")){
									String state = String.valueOf(msg_json.get("status"));
									if(state.equals("200")||state.equals("0")){

										//这个时候必定没有显示都以耦合器模式现添加处理,且注意屏蔽所有的电话操作
										//if(GlobalVariable.viewmodel == 2){
										if(true){
											//耦合器模式此时才进行添加
											String uid = msg_json.get("uid").toString();
											String outCallUri = msg_json.get("OutCallUri").toString();

											String callednumber = "0000";
											String show_callednumber = "0000";
											try {
												String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
												String callednumber_all = contacturi.replace("<sip:", "");
												String[] callednumber_list = callednumber_all.split("@");
												String tailcallednumber = "";
												if (callednumber_list[0].length() >= 4) {
													tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
												} else {
													tailcallednumber = callednumber_list[0];
												}
												callednumber = callednumber_list[0];
												show_callednumber = tailcallednumber;
											} catch (Exception e) {
												e.printStackTrace();
											}
											final String callednumber_final = callednumber;
											final String show_callednumber_final = show_callednumber;

											//不再判断这通电话的归属者，仅做显示
//											String daoboCallUri = "";
//											String zhuboCallUri = "";
//											if (GlobalVariable.charactername.equals("director"))
//												daoboCallUri = String.valueOf(msg_json.get("DaoBoCallUri"));
//											else
//												zhuboCallUri = String.valueOf(msg_json.get("ZhuBoCallUri"));
//											if (daoboCallUri.equals(localUri) || zhuboCallUri.equals(localUri)) {
												//添加一个接听的电话之前先保持之前的所有电话
												//autoHoldCallAll();

												new Thread() {
													@Override
													public void run() {
														MyCallWithState callWithState = new MyCallWithState();

														callWithState.syncshow = true;
														callWithState.dailout = true;
														callWithState.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
														callWithState.type = CALL_TYPE.DAOBOACPT;
														callWithState.outcalluri = outCallUri;
														callWithState.uid = uid;
														callWithState.daobocalluri = localUri;
														SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
														callWithState.callintime = df.format(new Date());
														callWithState.callednumber = callednumber_final;
														callWithState.show_callednumber = show_callednumber_final;
														callWithState.accepttime = GlobalVariable.hms_df.format(new Date());

														try {
															currentCallNum = allCallinList.size();
															callWithState.id = currentCallNum;
															localacceptcall = callWithState;
															allCallinList.add(currentCallNum, callWithState);
															currentCallNum++;
														} catch (Exception e) {
															e.printStackTrace();
														}

														//查询电话信息
														String pstn = outCallUri.replace("<sip:", "");
														String[] pstn_list = pstn.split("@");
														pstn = pstn_list[0];
														callWithState.pstn = pstn;
														//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
														//pstn = pstn.replaceFirst("00", "");
                                                        pstn = pstn.substring(2);
														getCallRecordInfo(callWithState, callWithState.pstn, callWithState.uid);

														String tailnumber = "";
														if (callWithState.pstn.length() >= 4) {
															tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
														} else {
															tailnumber = callWithState.pstn;
														}

														callWithState.showitem = DataConvertUtil.putData(
																(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
																standardDateFormat.format(new Date()),
																callWithState.from,
																pstn,
																tailnumber,
																callWithState.pstnmark,
																callWithState.content,
																String.valueOf(callWithState.id + 1),
																callWithState.uid,
																callWithState.callednumber
														);
														callinList.add(callWithState.showitem);
														localacceptcall = callWithState;
														broadcastCallType(callWithState, CALL_TYPE.DAOBOACPT);

														Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
														m2.sendToTarget();
													}
												}.start();
											//}
										}
									}
									else{
										MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(MainActivity.this,"呼出错误,请挂断").create();
										msgNotifyDialog.show();
										if(GlobalVariable.viewmodel == 1)
											msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
										else if(GlobalVariable.viewmodel == 2)
											msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
										else if(GlobalVariable.viewmodel == 3)
											msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.gs_bkg_notify_dialog);
										msgNotifyDialog.getWindow().setLayout(500, 300);
									}
								}
							}
						}
						else if(msgType.equals("DAOBO SYNC SHOW CALL")){
							//仅在耦合器模式下，多导播或仅导播，导播互相之间同步显示
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							if(GlobalVariable.charactername.equals("director")
									&& GlobalVariable.sipmodel != 0
									&& GlobalVariable.viewmodel == 2
									&& !daoBoCallUri.equals(localUri)){

								String outCallUri = msg_json.get("OutCallUri").toString();
								for(MyCallWithState callws : allCallinList){
									if(callws.outcalluri.equals(outCallUri)){
										return;
									}
								}
								//如果本地无相关线路不予显示（直接添加会造成冲突）
								String contacturi_judge = msg_json.get("contacturi").toString();
								if(!GlobalVariable.ctNumberMapRevert.containsValue(contacturi_judge)){
									return;
								}

								String uid = msg_json.get("uid").toString();
								String callednumber = "0000";
								String show_callednumber = "0000";
								try {
									String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
									String callednumber_all = contacturi.replace("<sip:", "");
									String[] callednumber_list = callednumber_all.split("@");
									String tailcallednumber = "";
									if (callednumber_list[0].length() >= 4) {
										tailcallednumber = callednumber_list[0].substring(callednumber_list[0].length() - 4);
									} else {
										tailcallednumber = callednumber_list[0];
									}
									callednumber = callednumber_list[0];
									show_callednumber = tailcallednumber;
								}catch (Exception e){
									e.printStackTrace();
								}
								final String callednumber_final = callednumber;
								final String show_callednumber_final = show_callednumber;
								//final String callednumber = callednumber_list[0];
								//GlobalVariable.PhoneRedTitle = true;
								new Thread() {
									@Override
									public void run() {
										//MyCall call1 = new MyCall(account, -1);
										MyCallWithState callWithState = new MyCallWithState();
										//callWithState.mycall = call1;
										callWithState.syncshow = true;
										callWithState.state = CALL_STATE_TYPE.DAOBO_INIT;
										callWithState.type = CALL_TYPE.DAOBOACPT;
										callWithState.outcalluri = outCallUri;
										callWithState.uid = uid;
										callWithState.daobocalluri = localUri;
										//callWithState.callednumber = "0000";//callednumber;
										callWithState.callednumber = callednumber_final;
										callWithState.show_callednumber = show_callednumber_final;
										SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
										callWithState.callintime = df.format(new Date());

										try {
											currentCallNum = allCallinList.size();
											callWithState.id = currentCallNum;
											allCallinList.add(currentCallNum, callWithState);
											currentCallNum++;
										}catch (Exception e)
										{
											e.printStackTrace();
										}

										//查询电话信息
										String pstn = outCallUri.replace("<sip:","");
										String[] pstn_list = pstn.split("@");
										pstn = pstn_list[0];
										String finalPstn = pstn;
										callWithState.pstn = pstn;

										getCallRecordInfo(callWithState,finalPstn,callWithState.uid);
										String tailnumber = "";
										if(callWithState.pstn.length()>=4) {
											tailnumber = callWithState.pstn.substring(callWithState.pstn.length()-4);
										}
										else{
											tailnumber = callWithState.pstn;
										}
										callWithState.showitem = DataConvertUtil.putData(
												(callWithState.name==null || callWithState.name.equals("null") || callWithState.name.equals(""))?tailnumber:callWithState.name,
												standardDateFormat.format(new Date()),
												callWithState.from,
												callWithState.pstn,
												tailnumber,
												callWithState.pstnmark,
												callWithState.content,
												String.valueOf(callWithState.id+1),
												callWithState.uid,
												callWithState.callednumber
										);
										callinList.add(callWithState.showitem);
										Message m2 = Message.obtain(MainActivity.handler_,1001,callWithState);
										m2.sendToTarget();
										autoSkipToMainActivity();
									}
								}.start();
							}
						}
						else if(msgType.equals("LITE OUT MAKE CALL")){
							String callednumber = "0000";
							String show_callednumber = "0000";
							//CONNECTING 和 CONFIRMED状态有时候会发两条消息出来，屏蔽多余的消息处理
							String outCallUri = msg_json.get("OutCallUri").toString();
							String uid = msg_json.get("uid").toString();
							for(MyCallWithState callws : allCallinList){
								if(callws.outcalluri.equals(outCallUri))	return;
								if(callws.uid.equals(uid))	return;
							}
							try {
								String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
								String callednumber_all = contacturi.replace("<sip:", "");
								String[] callednumber_list = callednumber_all.split("@");
								String tailcallednumber = "";
								callednumber = callednumber_list[0];
								show_callednumber = callednumber;
							}
							catch (Exception e){}

							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();


							final String callednumber_final = callednumber;
							final String show_callednumber_final = show_callednumber;
							//final String callednumber = callednumber_list[0];
							//20210924 保证主播端也会同步显示，但是主播端在DAOBOACPT状态不能操作
							//GlobalVariable.charactername.equals("director")
							if (true) {

								//当前正在内部通话
								if (CallActivity.instance != null) {

									String msg_to_send =
											"{ \"msgType\": \"INNER HANGUP\""
													+ ", \"fromsipid\": \"" + GlobalVariable.innercallfrom + "\""
													+ ", \"tosipid\": \"" + GlobalVariable.innercallto + "\""
													+ ", \"frompgm\": \"" + GlobalVariable.innerchannelfrom + "\""
													+ ", \"topgm\": \"" + GlobalVariable.innerchannelto + "\""
													+ ", \"fromname\": \"" + GlobalVariable.innernamefrom + "\""
													+ ", \"toname\": \"" + GlobalVariable.innernameto + "\""
													+ ", \"fromrole\": \"" + GlobalVariable.innerrolefrom + "\""
													+ ", \"torole\": \"" + GlobalVariable.innerroleto + "\""
													+ ", \"msg\": \"" + "外线呼入" + "\""
													+ " }";
									if (MainActivity.wsControl != null)
										MainActivity.wsControl.sendMessage(msg_to_send);
								}
								//响铃和闪灯逻辑放到handleMessage中处理
								LED_Media_Notify();
								GlobalVariable.PhoneRedTitle = true;
								new Thread() {
									@Override
									public void run() {
										//MyCall call1 = new MyCall(account, -1);

										MyCallWithState callWithState = new MyCallWithState();
										//callWithState.mycall = call1;
										callWithState.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
										callWithState.type = CALL_TYPE.DAOBOACPT;
										callWithState.outcalluri = outCallUri;
										callWithState.uid = uid;
										callWithState.daobocalluri = daoBoCallUri;
										//callWithState.callednumber = "0000";//callednumber;
										callWithState.callednumber = callednumber_final;
										callWithState.show_callednumber = show_callednumber_final;
										SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
										callWithState.callintime = df.format(new Date());

										try {
											currentCallNum = allCallinList.size();
											callWithState.id = currentCallNum;
											allCallinList.add(currentCallNum, callWithState);
											currentCallNum++;
										} catch (Exception e) {
											e.printStackTrace();
										}

										//查询电话信息
										String pstn = outCallUri.replace("<sip:", "");
										String[] pstn_list = pstn.split("@");
										pstn = pstn_list[0];
										String finalPstn = pstn;
										callWithState.pstn = pstn;

										getCallRecordInfo(callWithState, finalPstn, callWithState.uid);
										String tailnumber = "";
										if (callWithState.pstn.length() >= 4) {
											tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
										} else {
											tailnumber = callWithState.pstn;
										}
										callWithState.showitem = DataConvertUtil.putData(
												(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
												standardDateFormat.format(new Date()),
												callWithState.from,
												callWithState.pstn,
												tailnumber,
												callWithState.pstnmark,
												callWithState.content,
												String.valueOf(callWithState.id + 1),
												callWithState.uid,
												callWithState.callednumber
										);
										callinList.add(callWithState.showitem);


										try {
											String daobo = daoBoCallUri.replace("<sip:", "");
											String[] daobo_list = daobo.split("@");
											daobo = daobo_list[0];

											HttpRequest request = new HttpRequest();
											String rs = request.postJson(
													GlobalVariable.SEHTTPServer + "/incoming-call",
													"{" +
															"\"outcall\":\"" + callWithState.pstn + "\"," +
															"\"contact\":\"" + callWithState.callednumber + "\"," +
															"\"daobo\":\"" + daobo + "\"," +
															"\"zhubo\":\"\"," +
															"\"code\":" + 200 + "}"
											);
										} catch (Exception e) {
											//e.printStackTrace();
											Log.e("SEHTTPServer","指令异常");
										}

										Message m2 = Message.obtain(MainActivity.handler_, 1001, callWithState);
										m2.sendToTarget();
										autoSkipToMainActivity();
									}
								}.start();
							}
						}
						else if(msgType.equals("LITE DAIL OUT MAKE CALL")){
							String uid = msg_json.get("uid").toString();
							String outCallUri = msg_json.get("OutCallUri").toString();
							for(MyCallWithState callws : allCallinList){
								if(callws.outcalluri.equals(outCallUri))	return;
								if(callws.uid.equals(uid))	return;
							}

							String callednumber = "0000";
							String show_callednumber = "0000";
							try {
								String contacturi = String.valueOf(msg_json.get("contacturi")).equals("null") ? "0000" : String.valueOf(msg_json.get("contacturi"));
								Log.e("contacturi",contacturi);
								String callednumber_all = contacturi.replace("<sip:", "");
								String[] callednumber_list = callednumber_all.split("@");
								callednumber = callednumber_list[0];
								show_callednumber = callednumber;
							} catch (Exception e) {
								e.printStackTrace();
							}
							final String callednumber_final = callednumber;
							final String show_callednumber_final = show_callednumber;


							String  tempDaoboCallUri = String.valueOf(msg_json.get("DaoBoCallUri")).replace("\"","");
							int temp_index = tempDaoboCallUri.indexOf("<");
							if(temp_index!=-1)
							{
								tempDaoboCallUri = tempDaoboCallUri.substring(temp_index);
							}
							String daoboCallUri = tempDaoboCallUri;

								new Thread() {
									@Override
									public void run() {
										MyCallWithState callWithState = new MyCallWithState();

										callWithState.dailout = true;
										callWithState.state = CALL_STATE_TYPE.DAOBO_ACCEPT;
										callWithState.type = CALL_TYPE.DAOBOACPT;
										callWithState.outcalluri = outCallUri;
										callWithState.uid = uid;
										callWithState.daobocalluri = daoboCallUri;
										SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
										callWithState.callintime = df.format(new Date());
										callWithState.callednumber = callednumber_final;
										callWithState.show_callednumber = show_callednumber_final;
										callWithState.accepttime = GlobalVariable.hms_df.format(new Date());

										try {
											currentCallNum = allCallinList.size();
											callWithState.id = currentCallNum;
											localacceptcall = callWithState;
											allCallinList.add(currentCallNum, callWithState);
											currentCallNum++;
										} catch (Exception e) {
											e.printStackTrace();
										}

										//查询电话信息
										String pstn = outCallUri.replace("<sip:", "");
										pstn = pstn.replace("sip:","");
										String[] pstn_list = pstn.split("@");
										pstn = pstn_list[0];
										callWithState.pstn = pstn;
										//呼出电话去掉开头的00（注意在数据库中仍然带0，只能修改显示项）
										//pstn = pstn.replaceFirst("00", "");
										pstn = pstn.substring(2);

										getCallRecordInfo(callWithState, callWithState.pstn, callWithState.uid);
										String tailnumber = "";
										if (callWithState.pstn.length() >= 4) {
											tailnumber = callWithState.pstn.substring(callWithState.pstn.length() - 4);
										} else {
											tailnumber = callWithState.pstn;
										}

										callWithState.showitem = DataConvertUtil.putData(
												(callWithState.name == null || callWithState.name.equals("null") || callWithState.name.equals("")) ? tailnumber : callWithState.name,
												standardDateFormat.format(new Date()),
												callWithState.from,
												pstn,
												tailnumber,
												callWithState.pstnmark,
												callWithState.content,
												String.valueOf(callWithState.id + 1),
												callWithState.uid,
												callWithState.callednumber
										);
										callinList.add(callWithState.showitem);
										localacceptcall = callWithState;
										broadcastCallType(callWithState, CALL_TYPE.DAOBOACPT);

										Message m2 = Message.obtain(MainActivity.handler_, 1001, null);
										m2.sendToTarget();
									}
								}.start();

						}
						else if(msgType.equals("LITE ACK DAOBO TO LIVING")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							String code = msg_json.get("Code").toString();
							if(code.equals("200")){
								//当前是导播话机，移除call
								if(GlobalVariable.charactername.equals("director")){
									int removeid = -1;
									for(MyCallWithState callws : allCallinList){
										if(callws.outcalluri.equals(outCallUri)){
											removeid = callws.id;
										}
									}
									if(removeid != -1){
										if(currentCallWithState==allCallinList.get(removeid)){
											currentCallWithState = null;
										}
										try {
											allCallinList.remove(removeid);
											callinList.remove(removeid);
										}catch(Exception e){};
										updateShowIndex();
										if(callinListSelectedIdx == removeid)
										{
											callinListView.clearChoices();
											callinListSelectedIdx = -1;
										}
										currentCallNum--;

										if(GlobalVariable.viewmodel==1) {
											updateOperateButton();
										}
										else if(GlobalVariable.viewmodel==3){
											updateGsOperateButton();
										}
										resetMainView();
									}
								}
								//当前是主播话机，添加call，并置call状态为直播保持
								else if(GlobalVariable.charactername.equals("broadcaster")){
									for(MyCallWithState callws : allCallinList){
										if(callws.outcalluri.equals(outCallUri)){
											callws.type = CALL_TYPE.LIVEHOLD;
											callws.virtual = true;
											updateInfoAndButton();
										}
									}
								}

							}
						}
						else if(msgType.equals("LITE ACK LIVING TO DAOBO")){
							String outCallUri = msg_json.get("OutCallUri").toString();
							String code = msg_json.get("Code").toString();
							if(code.equals("200")){
								int remove_id = -1;
								for(MyCallWithState callws : allCallinList) {
									if (callws.outcalluri.equals(outCallUri)) {
										callws.type = CALL_TYPE.ENDCALL;
										broadcastCallType(callws,CALL_TYPE.ENDCALL);
										remove_id = callws.id;
									}
								}
								if(remove_id!=-1){
									allCallinList.remove(remove_id);
									callinList.remove(remove_id);
									updateInfoAndButton();
								}
							}
						}
						else if(msgType.equals("ACK START DAOBO LIVING")){
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String code = msg_json.get("code").toString();
							if(daoBoCallUri.equals(localUri) && code.equals("200")){
								GlobalVariable.daobo_living = true;
								if(GlobalVariable.viewmodel == 1) {
									ImageView titleLogo = findViewById(R.id.titleImage);
									titleLogo.setColorFilter(Color.RED);
								}
								else if(GlobalVariable.viewmodel == 2) {
									ImageView titleLogo = findViewById(R.id.ctTitleImage);
									titleLogo.setColorFilter(Color.RED);
								}
								else if(GlobalVariable.viewmodel == 3) {
									ImageView titleLogo = findViewById(R.id.ctTitleImage);
									titleLogo.setColorFilter(Color.RED);
								}
							}
						}
						else if(msgType.equals("ACK STOP DAOBO LIVING")){
							String daoBoCallUri = msg_json.get("DaoBoCallUri").toString();
							String code = msg_json.get("code").toString();
							if(daoBoCallUri.equals(localUri) && code.equals("200")){
								GlobalVariable.daobo_living = false;
								if(GlobalVariable.viewmodel == 1){
									ImageView titleLogo = findViewById(R.id.titleImage);
									titleLogo.setColorFilter(Color.TRANSPARENT);
								}
								else if(GlobalVariable.viewmodel == 2){
									ImageView titleLogo = findViewById(R.id.ctTitleImage);
									titleLogo.setColorFilter(Color.TRANSPARENT);
								}
								else if(GlobalVariable.viewmodel == 3){
									ImageView titleLogo = findViewById(R.id.ctTitleImage);
									titleLogo.setColorFilter(Color.TRANSPARENT);
								}
							}
						}
						else if(msgType.equals("ACK SET LINEBUSY")){
							//处理串口返回的置忙处理消息
							String commStr = msg_json.get("result").toString();
							//重新获取一遍置忙状态
							new Thread(){
								@Override
								public void run() {
									try{
										HttpRequest requestX = new HttpRequest();
										String rsX = requestX.postJson(
												"http://" + GlobalVariable.EUSERVER + ":8080/smart/contactWithSipServer/getLineMapping",
												"{}"
										);
										JSONObject rs_jsonX = new JSONObject(rsX);
										if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy1"))) == 1) {
											GlobalVariable.linebusy1 = true;
										} else {
											GlobalVariable.linebusy1 = false;
										}
										if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy2"))) == 1) {
											GlobalVariable.linebusy2 = true;
										} else {
											GlobalVariable.linebusy2 = false;
										}
										if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy3"))) == 1) {
											GlobalVariable.linebusy3 = true;
										} else {
											GlobalVariable.linebusy3 = false;
										}
										if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy4"))) == 1) {
											GlobalVariable.linebusy4 = true;
										} else {
											GlobalVariable.linebusy4 = false;
										}
										if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy5"))) == 1) {
											GlobalVariable.linebusy5 = true;
										} else {
											GlobalVariable.linebusy5 = false;
										}
										if (Integer.parseInt(String.valueOf(rs_jsonX.getJSONObject("data").get("busy6"))) == 1) {
											GlobalVariable.linebusy6 = true;
										} else {
											GlobalVariable.linebusy6 = false;
										}
										Message m2 = Message.obtain(MainActivity.handler_, 2004, null);
										m2.sendToTarget();
									}catch(Exception e){}
								}
							}.start();
						}
						else if(msgType.equals("ACK GET POWER")){
							String powerstate = msg_json.get("result").toString();
							new Thread() {
								@Override
								public void run() {
									try {
										HttpRequest request = new HttpRequest();
										String rs = request.postJson(
												GlobalVariable.SEHTTPServer + "/powerstate",
												"{\"code\":" + 200 + ","
														+"\"powerstate\": \""+powerstate+ "\""
														+ "}"
										);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}.start();
						}
						else if(msgType.equals("GET PHONE VOLUME")){
							String rxvalue = msg_json.get("rxvalue").toString();
							String txvalue = msg_json.get("txvalue").toString();
							Log.e("rxvalue",rxvalue);
							Log.e("txvalue",txvalue);
							GlobalVariable.rxLevelValue = rxvalue;
							GlobalVariable.txLevelValue = txvalue;
						}
						else{}
					} catch (JSONException e) {
						LogClient.generate("【JSON转换错误】"+e.getMessage());
						e.printStackTrace();
					}
					catch (Exception e) {
						LogClient.generate("【消息处理错误】"+e.getMessage());
						e.printStackTrace();
					}
				}
			}
		}
	}
	//****************************************************************

	/**响铃和灯的处理**/
	public void LED_Media_Notify(){
		if(GlobalVariable.liteModel)
			return;

//		CommonMediaManager mediaManager = CommonMediaManager.getInstance(getBaseContext());
		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
//		mediaManager.setVoiceAudioDevice(1);
//		flashManager.flashSpeaker();
		//&& GlobalVariable.currentaccept_uid.equals("")
		if(musicControl!=null&&!musicControl.isPlaying()){

			boolean haveacceptcall =false;
			for(MyCallWithState callws : allCallinList)
			{
				if(callws.type == CALL_TYPE.DAOBOACPT && GlobalVariable.charactername.equals("director") && callws.syncshow == false){
					haveacceptcall = true;
				}
				if(callws.type == CALL_TYPE.ZHUBOACPT && !GlobalVariable.charactername.equals("director") && callws.syncshow == false){
					haveacceptcall = true;
				}
			}

			if(GlobalVariable.callring){
				if((!haveacceptcall) && (!GlobalVariable.daobo_living)) {
					musicControl.play();
					flashManager.flash();
				}
			}
			else{
//				if(!haveacceptcall) {
//					flashManager.stopflash();
//					flashManager.flash();
//				}
			}
		}
		//每次暂停播放音乐也就是接听后将切换到话筒

	}
	public void LED_Media_Pause(){
//		CommonMediaManager mediaManager = CommonMediaManager.getInstance(getBaseContext());
		CommonPlatformManager flashManager = CommonPlatformManager.getInstance(getBaseContext());
//		mediaManager.setVoiceAudioDevice(2);
//		flashManager.stopflashSpeaker();
		if(musicControl!=null&&musicControl.isPlaying()){
			musicControl.pause();
		}
		flashManager.stopflash();
		//每次暂停播放音乐也就是接听后将切换到话筒
	}

	public void showCtDailPanelDialog(String keycode){
		if(GlobalVariable.viewmodel == 2){
			CtDailPanelDialog ctDailPanelDialog = new CtDailPanelDialog.Builder(MainActivity.this,currentCallWithState,keycode).create();
			mctDailPanelDialog = ctDailPanelDialog;
			ctDailPanelDialog.show();
			ctDailPanelDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
			ctDailPanelDialog.getWindow().setLayout(700, 500);
			ctDailPanelDialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM);
		}
		else if(GlobalVariable.viewmodel == 3){
			CtDailPanelDialog ctDailPanelDialog = new CtDailPanelDialog.Builder(MainActivity.this,currentCallWithState,keycode).create();
			mctDailPanelDialog = ctDailPanelDialog;
			ctDailPanelDialog.show();
			ctDailPanelDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
			ctDailPanelDialog.getWindow().setLayout(700, 500);
			ctDailPanelDialog.getWindow().setGravity(Gravity.CENTER_HORIZONTAL|Gravity.BOTTOM);
		}
	}
	public void closeCtDailPanelDialog(){
        if(mctDailPanelDialog!=null){
            mctDailPanelDialog.dismiss();
        }
    }

	//用于处理软件退出的问题
	private long exitTime = 0;
	/**
	 * 捕捉返回事件按钮
	 *
	 * 因为此 Activity 继承 TabActivity 用 onKeyDown 无响应，所以改用 dispatchKeyEvent
	 * 一般的 Activity 用 onKeyDown 就可以了
	 */
	@RequiresApi(api = Build.VERSION_CODES.N)
	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		Log.e("按键设备ID",event.getDeviceId()+"");
		int keycode = event.getKeyCode();

        TextView editTextName = null;
        TextView textViewDescribe = null;
        TextView textViewInfo = null;
        TextView textViewPdrRemark = null;
        TextView textViewMbcRemark = null;

        if(GlobalVariable.viewmodel == 1)
        {
            editTextName = findViewById(R.id.editTextName);
            textViewInfo = findViewById(R.id.textViewInfo);
            textViewDescribe = findViewById(R.id.textViewDescribe);
            if(GlobalVariable.charactername.equals("director"))
            {
                textViewPdrRemark = findViewById(R.id.textViewPdrRemark);
            }
            if(GlobalVariable.charactername.equals("broadcaster"))
			{
				textViewMbcRemark = findViewById(R.id.textViewMbcRemark);
			}
        }
        if(GlobalVariable.viewmodel == 2 || GlobalVariable.viewmodel == 3) {//20230221处理2模式未初始化控件捕获异常
            editTextName = findViewById(R.id.editTextName);
            textViewInfo = findViewById(R.id.textViewInfo);
        }

		ImageButton btnAccept = null;
		ImageButton btnLive = null;
		ImageButton btnSwitch = null;
		ImageButton btnHangup = null;
		if(GlobalVariable.viewmodel == 1) {
			btnAccept = findViewById(R.id.buttonCN1);//接听
			btnLive = findViewById(R.id.buttonCN6);//直播
			btnSwitch = findViewById(R.id.buttonCN5);//转接
			btnHangup = findViewById(R.id.buttonCN4);//挂断
		}
		if(GlobalVariable.viewmodel == 2)
		{
			int id = GlobalVariable.ct_selected_index;
			//找到当前被选中的电话线路
			if(id == 1) {
				btnAccept = findViewById(R.id.ct_1_1);//接听
				btnSwitch = findViewById(R.id.ct_1_2);//转接
				btnLive = findViewById(R.id.ct_1_3);//直播
				btnHangup = findViewById(R.id.ct_1_4);//挂断
			}
			if(id == 2) {
				btnAccept = findViewById(R.id.ct_2_1);//接听
				btnSwitch = findViewById(R.id.ct_2_2);//转接
				btnLive = findViewById(R.id.ct_2_3);//直播
				btnHangup = findViewById(R.id.ct_2_4);//挂断
			}
			if(id == 3) {
				btnAccept = findViewById(R.id.ct_3_1);//接听
				btnSwitch = findViewById(R.id.ct_3_2);//转接
				btnLive = findViewById(R.id.ct_3_3);//直播
				btnHangup = findViewById(R.id.ct_3_4);//挂断
			}
			if(id == 4) {
				btnAccept = findViewById(R.id.ct_4_1);//接听
				btnSwitch = findViewById(R.id.ct_4_2);//转接
				btnLive = findViewById(R.id.ct_4_3);//直播
				btnHangup = findViewById(R.id.ct_4_4);//挂断
			}
			if(id == 5) {
				btnAccept = findViewById(R.id.ct_5_1);//接听
				btnSwitch = findViewById(R.id.ct_5_2);//转接
				btnLive = findViewById(R.id.ct_5_3);//直播
				btnHangup = findViewById(R.id.ct_5_4);//挂断
			}
			if(id == 6) {
				btnAccept = findViewById(R.id.ct_6_1);//接听
				btnSwitch = findViewById(R.id.ct_6_2);//转接
				btnLive = findViewById(R.id.ct_6_3);//直播
				btnHangup = findViewById(R.id.ct_6_4);//挂断
			}
		}
		if(GlobalVariable.viewmodel == 3)
		{
			btnAccept = findViewById(R.id.gs_btn_acptorhold);//接听
			btnSwitch = findViewById(R.id.gs_btn_switch);//转接
			btnLive = findViewById(R.id.gs_btn_liveorhold);//直播
			btnHangup = findViewById(R.id.gs_btn_hangup);//挂断
		}

		//主界面特有的按键映射
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			switch (keycode) {
				case KeyEvent.KEYCODE_0:
					//如果是虚拟键盘则不截获输入事件
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("0");
					}
					break;
				case KeyEvent.KEYCODE_1:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						if(GlobalVariable.btnContrlModel == true)
						{
							//0：无导播模式 1:单导播模式 2：多导播模式,3:仅导播模式
							//无导播模式 导播无 主播1接听2直播3挂断
							//多导播模式 导播1接听2转接3挂断 主播1直播2转接3挂断
							//仅导播模式 导播1接听2直播3挂断 主播无
							if(GlobalVariable.charactername.equals("director"))
							{
								if(GlobalVariable.sipmodel != 0 && btnAccept != null && btnAccept.isEnabled()){
									acceptCall(btnAccept);
								}
							}
							else if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if(GlobalVariable.sipmodel == 0  && btnAccept != null && btnAccept.isEnabled()){
									acceptCall(btnAccept);
								}
								else if(GlobalVariable.viewmodel == 1 && (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) && btnLive != null && btnLive.isEnabled())
								{
									liveCall(btnLive);
								}
								else if(GlobalVariable.viewmodel == 2 && (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) && btnSwitch != null && btnSwitch.isEnabled())
                                {
                                    switchCall(btnSwitch);
                                }
								else if(GlobalVariable.viewmodel == 3 && (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) && btnLive != null && btnLive.isEnabled())
								{
									liveCall(btnLive);
								}
							}
						}
						else{
							showCtDailPanelDialog("1");
						}
					}
					break;
				case KeyEvent.KEYCODE_2:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {

						if(GlobalVariable.btnContrlModel == true && GlobalVariable.viewmodel != 2)
						{
							//0：无导播模式 1:单导播模式 2：多导播模式,3:仅导播模式
							//无导播模式 导播无 主播1接听2直播3挂断
							//多导播模式 导播1接听2转接3挂断 主播1直播2转接3挂断
							//仅导播模式 导播1接听2直播3挂断 主播无
							if(GlobalVariable.charactername.equals("director"))
							{
								if(GlobalVariable.sipmodel == 1  && btnSwitch != null && btnSwitch.isEnabled()){
									switchCall(btnSwitch);
								}
								else if(GlobalVariable.sipmodel == 2  && btnSwitch != null && btnSwitch.isEnabled()){
									switchCall(btnSwitch);
								}
								else if(GlobalVariable.sipmodel == 3  && btnLive != null && btnLive.isEnabled()){
									liveCall(btnLive);
								}
							}
							else if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if(GlobalVariable.sipmodel == 0 && btnLive != null && btnLive.isEnabled()){
									liveCall(btnLive);
								}
								else if(GlobalVariable.sipmodel == 1 && btnSwitch != null && btnSwitch.isEnabled()){
									switchCall(btnSwitch);
								}
								else if(GlobalVariable.sipmodel == 2 && btnSwitch != null && btnSwitch.isEnabled()){
									switchCall(btnSwitch);
								}

							}
						}
						else{
							showCtDailPanelDialog("2");
						}
					}
					break;
				case KeyEvent.KEYCODE_3:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {

						if(GlobalVariable.btnContrlModel == true && GlobalVariable.viewmodel != 2)
						{
							//0：无导播模式 1:单导播模式 2：多导播模式,3:仅导播模式
							//无导播模式 导播无 主播1接听2直播3挂断
							//多导播模式 导播1接听2转接3挂断 主播1直播2转接3挂断
							//仅导播模式 导播1接听2直播3挂断 主播无
							if(GlobalVariable.charactername.equals("director"))
							{
								if(GlobalVariable.sipmodel != 0 && btnHangup != null && btnHangup.isEnabled()){
									hangupCall(btnHangup);
								}
							}
							else if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if(GlobalVariable.sipmodel != 3 && btnHangup != null && btnHangup.isEnabled()){
									hangupCall(btnHangup);
								}
							}
						}
						else{
							showCtDailPanelDialog("3");
						}
					}
					break;
				case KeyEvent.KEYCODE_4:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						if(GlobalVariable.btnContrlModel == true && GlobalVariable.viewmodel == 2)
						{
							//0：无导播模式 1:单导播模式 2：多导播模式,3:仅导播模式
							//无导播模式 导播无 主播1接听2直播3挂断
							//多导播模式 导播1接听2转接3挂断 主播1转接2直播3挂断
							//仅导播模式 导播1接听2直播3挂断 主播无
							if(GlobalVariable.charactername.equals("director"))
							{
								if(GlobalVariable.sipmodel == 1  && btnSwitch != null && btnSwitch.isEnabled()){
									switchCall(btnSwitch);
								}
								else if(GlobalVariable.sipmodel == 2  && btnSwitch != null && btnSwitch.isEnabled()){
									switchCall(btnSwitch);
								}
								else if(GlobalVariable.sipmodel == 3  && btnLive != null && btnLive.isEnabled()){
									liveCall(btnLive);
								}
							}
							else if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if(GlobalVariable.sipmodel == 0 && btnLive != null && btnLive.isEnabled()){
									liveCall(btnLive);
								}
								else if(GlobalVariable.sipmodel == 1 && btnLive != null && btnLive.isEnabled()){
									liveCall(btnLive);
								}
								else if(GlobalVariable.sipmodel == 2 && btnSwitch != null && btnSwitch.isEnabled()){
									liveCall(btnLive);
								}

							}
						}
						else {
							showCtDailPanelDialog("4");
						}
					}
					break;
				case KeyEvent.KEYCODE_5:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("5");
					}
					break;
				case KeyEvent.KEYCODE_6:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("6");
					}
					break;
				case KeyEvent.KEYCODE_7:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						if(GlobalVariable.btnContrlModel == true && GlobalVariable.viewmodel == 2)
						{
							//0：无导播模式 1:单导播模式 2：多导播模式,3:仅导播模式
							//无导播模式 导播无 主播1接听2直播3挂断
							//多导播模式 导播1接听2转接3挂断 主播1直播2转接3挂断
							//仅导播模式 导播1接听2直播3挂断 主播无
							if(GlobalVariable.charactername.equals("director"))
							{
								if(GlobalVariable.sipmodel != 0 && btnHangup != null && btnHangup.isEnabled()){
									hangupCall(btnHangup);
								}
							}
							else if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if(GlobalVariable.sipmodel != 3 && btnHangup != null && btnHangup.isEnabled()){
									hangupCall(btnHangup);
								}
							}
						}
						else {
							showCtDailPanelDialog("7");
						}
					}
					break;
				case KeyEvent.KEYCODE_8:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("8");
					}
					break;
				case KeyEvent.KEYCODE_9:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("9");
					}
					break;
				case KeyEvent.KEYCODE_STAR:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("*");
					}
					break;
				case KeyEvent.KEYCODE_POUND:
					if (!editTextName.isFocused() && !textViewInfo.isFocused()) {
						showCtDailPanelDialog("#");
					}
					break;
				case 19://上
					//判断当前ListView是否有选中项，且有多个选中项，
					//如果是，切换到上一个，如果已经选中项是第一项，不作处理
					if(GlobalVariable.viewmodel == 1 && allCallinList.size()>1) {
						if (callinListSelectedIdx != 0) {
							callinListView.setItemChecked(callinListSelectedIdx - 1, true);
							currentCallWithState = allCallinList.get(callinListSelectedIdx - 1);
							callinListSelectedIdx--;
							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
							updateInfoAndButton();
						}
					}
					else if(GlobalVariable.viewmodel == 2){
						int id = GlobalVariable.ct_selected_index;
						if(id == -1)
						{
							//处于未选中状态，查找当前选中电话并更新
							for(MyCallWithState callws : allCallinList)
							{
								if(callws.selected_line_btn)
								{
									id = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
								}
							}
							if(id == -1)
							{
								//查找无结果当前有电话选中第一通
//								if(!allCallinList.isEmpty())
//								{
//									MyCallWithState callws = allCallinList.get(0);
//									callws.selected_line_btn = true;
//									GlobalVariable.ct_selected_index = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
//								}
								GlobalVariable.ct_selected_index = 1;
							}
						}
						if( id == 2 ||id == 3 ||id == 4 ||id == 5 ||id == 6 )
						{
							//已经有一个了而且是选中状态，更改为选中前一个，如果选中的前一个是没有电话的，那么就把当前所有的callws.selected_line_btn = false
							GlobalVariable.ct_selected_index = id - 1;
							for(MyCallWithState callws : allCallinList)
							{
								callws.selected_line_btn = false;
								if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevertAll.get(id-1))) {
									callws.selected_line_btn = true;
								}
							}
						}
						updateInfoAndButton();
					}
					else if(GlobalVariable.viewmodel == 3){
						int id = GlobalVariable.gs_selected_index;
						//找到了当前选中的电话，选中前一通电话
						if(id == -1)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_1);
							stateLayout.callOnClick();
						}
						if(id == 2)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_1);
							stateLayout.callOnClick();
						}
						if(id == 3)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_2);
							stateLayout.callOnClick();
						}
						if(id == 4)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_3);
							stateLayout.callOnClick();
						}
						if(id == 5)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_4);
							stateLayout.callOnClick();
						}
						if(id == 6)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_5);
							stateLayout.callOnClick();
						}

					}

					break;
				case 20://
					if(GlobalVariable.viewmodel == 1 && allCallinList.size()>1)
					{
						if(callinListSelectedIdx != allCallinList.size()-1)
						{
							callinListView.setItemChecked(callinListSelectedIdx+1,true);
							currentCallWithState = allCallinList.get(callinListSelectedIdx+1);
							callinListSelectedIdx++;
							//立即通知ListView更新结果
							myAdapter.setDataList(allCallinList);
							mbcAdapter.setDataList(allCallinList);
							updateInfoAndButton();
						}
					}
					else if(GlobalVariable.viewmodel == 2)
					{
						int id = GlobalVariable.ct_selected_index;
						if(id == -1)
						{
							//处于未选中状态，查找当前选中电话并更新
							for(MyCallWithState callws : allCallinList)
							{
								if(callws.selected_line_btn)
								{
									id = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
								}
							}
							if(id == -1)
							{
								//查找无结果当前有电话选中第一通
//								if(!allCallinList.isEmpty())
//								{
//									MyCallWithState callws = allCallinList.get(0);
//									callws.selected_line_btn = true;
//									GlobalVariable.ct_selected_index = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
//								}
								GlobalVariable.ct_selected_index = 1;
							}
						}
						if( id == 1 ||id == 2 ||id == 3 ||id == 4 ||id == 5 )
						{
							//已经有一个了而且是选中状态，更改为选中前一个，如果选中的前一个是没有电话的，那么就把当前所有的callws.selected_line_btn = false
							GlobalVariable.ct_selected_index = id + 1;
							for(MyCallWithState callws : allCallinList)
							{
								callws.selected_line_btn = false;
								if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevertAll.get(id+1))) {
									callws.selected_line_btn = true;
								}
							}
						}
						updateInfoAndButton();
					}
					else if(GlobalVariable.viewmodel == 3){
						int id = GlobalVariable.gs_selected_index;
						//找到了当前选中的电话，选中后一通电话
						if(id == -1)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_1);
							stateLayout.callOnClick();
						}
						if(id == 1)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_2);
							stateLayout.callOnClick();
						}
						if(id == 2)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_3);
							stateLayout.callOnClick();
						}
						if(id == 3)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_4);
							stateLayout.callOnClick();
						}
						if(id == 4)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_5);
							stateLayout.callOnClick();
						}
						if(id == 5)
						{
							LinearLayout stateLayout = findViewById(R.id.gs_state_bkg_6);
							stateLayout.callOnClick();
						}

					}

					break;
					//break;
				case 21://左
					if(GlobalVariable.viewmodel == 2){
						int id = GlobalVariable.ct_selected_index;
						if(id == -1)
						{
							//处于未选中状态，查找当前选中电话并更新
							for(MyCallWithState callws : allCallinList)
							{
								if(callws.selected_line_btn)
								{
									id = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
								}
							}
							if(id == -1)
							{
								//查找无结果当前有电话选中第一通
//								if(!allCallinList.isEmpty())
//								{
//									MyCallWithState callws = allCallinList.get(0);
//									callws.selected_line_btn = true;
//									GlobalVariable.ct_selected_index = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
//								}
								GlobalVariable.ct_selected_index = 1;
							}
						}
						if( id == 2 ||id == 3 ||id == 4 ||id == 5 ||id == 6 )
						{
							//已经有一个了而且是选中状态，更改为选中前一个，如果选中的前一个是没有电话的，那么就把当前所有的callws.selected_line_btn = false
							GlobalVariable.ct_selected_index = id - 1;
							for(MyCallWithState callws : allCallinList)
							{
								callws.selected_line_btn = false;
								if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevertAll.get(id-1))) {
									callws.selected_line_btn = true;
								}
							}
						}
						updateInfoAndButton();
					}
					break;
				case 22://右
					if(GlobalVariable.viewmodel == 2)
					{
						int id = GlobalVariable.ct_selected_index;
						if(id == -1)
						{
							//处于未选中状态，查找当前选中电话并更新
							for(MyCallWithState callws : allCallinList)
							{
								if(callws.selected_line_btn)
								{
									id = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
								}
							}
							if(id == -1)
							{
								//查找无结果当前有电话选中第一通
//								if(!allCallinList.isEmpty())
//								{
//									MyCallWithState callws = allCallinList.get(0);
//									callws.selected_line_btn = true;
//									GlobalVariable.ct_selected_index = GlobalVariable.ctNumberMapAll.get(callws.callednumber) == null ? 0 : GlobalVariable.ctNumberMapAll.get(callws.callednumber);
//								}
								GlobalVariable.ct_selected_index = 1;
							}
						}
						if( id == 1 ||id == 2 ||id == 3 ||id == 4 ||id == 5 )
						{
							//已经有一个了而且是选中状态，更改为选中前一个，如果选中的前一个是没有电话的，那么就把当前所有的callws.selected_line_btn = false
							GlobalVariable.ct_selected_index = id + 1;
							for(MyCallWithState callws : allCallinList)
							{
								callws.selected_line_btn = false;
								if(callws.callednumber.equals(GlobalVariable.ctNumberMapRevertAll.get(id+1))) {
									callws.selected_line_btn = true;
								}
							}
						}
						updateInfoAndButton();
					}
					break;
				case 403://刷新信息，相当于点击刷新按钮
					if(GlobalVariable.charactername.equals("director"))
					{
						changeCallInfo(null);
					}
					return true;
				case 404://实体保持键，20221011更新为映射实体键操作开启/关闭
					if(GlobalVariable.btnContrlModel_Open)
					{
                        GlobalVariable.btnContrlModel = !GlobalVariable.btnContrlModel;

                        if(GlobalVariable.btnContrlModel == true)
                        {
                            //开启实体按键操作就屏蔽输入框操作
							if(GlobalVariable.charactername.equals("director")) {
								if (editTextName != null) {
									editTextName.setEnabled(false);
								}
								if (textViewDescribe != null) {
									textViewDescribe.setEnabled(false);
								}
								if (textViewInfo != null) {
									textViewInfo.setEnabled(false);
								}
								if (textViewPdrRemark != null) {
									textViewPdrRemark.setEnabled(false);
								}
							}
							if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if (textViewMbcRemark != null) {
									textViewMbcRemark.setEnabled(false);
								}
							}
                        }
                        else{
							if(GlobalVariable.charactername.equals("director")) {
								if (editTextName != null) {
									editTextName.setEnabled(true);
								}
								if (textViewDescribe != null) {
									textViewDescribe.setEnabled(true);
								}
								if (textViewInfo != null) {
									textViewInfo.setEnabled(true);
								}
								if (textViewPdrRemark != null) {
									textViewPdrRemark.setEnabled(true);
								}
							}
							if(GlobalVariable.charactername.equals("broadcaster"))
							{
								if (textViewMbcRemark != null) {
									textViewMbcRemark.setEnabled(true);
								}
							}
                        }

						if (GlobalVariable.viewmodel == 1 || GlobalVariable.viewmodel == 3) {
							TextView tvBtn1 = (TextView) findViewById(R.id.textBtn1);
							TextView tvBtn2_1 = (TextView) findViewById(R.id.textBtn2_1);
							TextView tvBtn2_2 = (TextView) findViewById(R.id.textBtn2_2);
							TextView tvBtn3 = (TextView) findViewById(R.id.textBtn3);
							if (GlobalVariable.btnContrlModel == true) {

								tvBtn1.setVisibility(View.VISIBLE);
								tvBtn3.setVisibility(View.VISIBLE);
								//无导播， 导播端 接听 直播 挂断，主播端 接听 直播 挂断
								//单多导播 导播端 接听 转接 挂断，主播端 直播 转接 挂断
								//仅导播， 导播端 接听 直播 挂断，主播端 接听 直播 挂断
								if (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3) {
									tvBtn2_1.setVisibility(View.VISIBLE);//留直播
								} else if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) {
									tvBtn2_2.setVisibility(View.VISIBLE);//留转接
								}

							} else {
								tvBtn1.setVisibility(View.GONE);
								tvBtn2_1.setVisibility(View.GONE);
								tvBtn2_2.setVisibility(View.GONE);
								tvBtn3.setVisibility(View.GONE);
							}
						} else if (GlobalVariable.viewmodel == 2) {
							TextView tvBtn1_1 = findViewById(R.id.textBtn1_1);
							TextView tvBtn1_2_1 = findViewById(R.id.textBtn1_2_1);
							TextView tvBtn1_2_2 = findViewById(R.id.textBtn1_2_2);
							TextView tvBtn1_3 = findViewById(R.id.textBtn1_3);
							TextView tvBtn2_1 = findViewById(R.id.textBtn2_1);
							TextView tvBtn2_2_1 = findViewById(R.id.textBtn2_2_1);
							TextView tvBtn2_2_2 = findViewById(R.id.textBtn2_2_2);
							TextView tvBtn2_3 = findViewById(R.id.textBtn2_3);
							TextView tvBtn3_1 = findViewById(R.id.textBtn3_1);
							TextView tvBtn3_2_1 = findViewById(R.id.textBtn3_2_1);
							TextView tvBtn3_2_2 = findViewById(R.id.textBtn3_2_2);
							TextView tvBtn3_3 = findViewById(R.id.textBtn3_3);
							TextView tvBtn4_1 = findViewById(R.id.textBtn4_1);
							TextView tvBtn4_2_1 = findViewById(R.id.textBtn4_2_1);
							TextView tvBtn4_2_2 = findViewById(R.id.textBtn4_2_2);
							TextView tvBtn4_3 = findViewById(R.id.textBtn4_3);
							TextView tvBtn5_1 = findViewById(R.id.textBtn5_1);
							TextView tvBtn5_2_1 = findViewById(R.id.textBtn5_2_1);
							TextView tvBtn5_2_2 = findViewById(R.id.textBtn5_2_2);
							TextView tvBtn5_3 = findViewById(R.id.textBtn5_3);
							TextView tvBtn6_1 = findViewById(R.id.textBtn6_1);
							TextView tvBtn6_2_1 = findViewById(R.id.textBtn6_2_1);
							TextView tvBtn6_2_2 = findViewById(R.id.textBtn6_2_2);
							TextView tvBtn6_3 = findViewById(R.id.textBtn6_3);

							if (GlobalVariable.btnContrlModel == true) {
								if (GlobalVariable.sipmodel == 1 || GlobalVariable.sipmodel == 2) {
									//三个按键
									//导播 接听 预备播出 挂断
									//主播 预备播出 播出 挂断
									if (GlobalVariable.charactername.equals("director")) {
										tvBtn1_1.setVisibility(View.VISIBLE);//接听
										tvBtn2_1.setVisibility(View.VISIBLE);
										tvBtn3_1.setVisibility(View.VISIBLE);
										tvBtn4_1.setVisibility(View.VISIBLE);
										tvBtn5_1.setVisibility(View.VISIBLE);
										tvBtn6_1.setVisibility(View.VISIBLE);

										tvBtn1_2_1.setVisibility(View.VISIBLE);//留转接
										tvBtn2_2_1.setVisibility(View.VISIBLE);
										tvBtn3_2_1.setVisibility(View.VISIBLE);
										tvBtn4_2_1.setVisibility(View.VISIBLE);
										tvBtn5_2_1.setVisibility(View.VISIBLE);
										tvBtn6_2_1.setVisibility(View.VISIBLE);

										tvBtn1_2_1.setText("4");//转接在第2位
										tvBtn2_2_1.setText("4");
										tvBtn3_2_1.setText("4");
										tvBtn4_2_1.setText("4");
										tvBtn5_2_1.setText("4");
										tvBtn6_2_1.setText("4");

										tvBtn1_3.setVisibility(View.VISIBLE);//挂断
										tvBtn2_3.setVisibility(View.VISIBLE);
										tvBtn3_3.setVisibility(View.VISIBLE);
										tvBtn4_3.setVisibility(View.VISIBLE);
										tvBtn5_3.setVisibility(View.VISIBLE);
										tvBtn6_3.setVisibility(View.VISIBLE);
									}
									else if (GlobalVariable.charactername.equals("broadcaster")) {

										tvBtn1_2_1.setVisibility(View.VISIBLE);//留转接
										tvBtn2_2_1.setVisibility(View.VISIBLE);
										tvBtn3_2_1.setVisibility(View.VISIBLE);
										tvBtn4_2_1.setVisibility(View.VISIBLE);
										tvBtn5_2_1.setVisibility(View.VISIBLE);
										tvBtn6_2_1.setVisibility(View.VISIBLE);

										tvBtn1_2_1.setText("1");//转接在第一位
										tvBtn2_2_1.setText("1");
										tvBtn3_2_1.setText("1");
										tvBtn4_2_1.setText("1");
										tvBtn5_2_1.setText("1");
										tvBtn6_2_1.setText("1");

										tvBtn1_2_2.setVisibility(View.VISIBLE);//留直播
										tvBtn2_2_2.setVisibility(View.VISIBLE);
										tvBtn3_2_2.setVisibility(View.VISIBLE);
										tvBtn4_2_2.setVisibility(View.VISIBLE);
										tvBtn5_2_2.setVisibility(View.VISIBLE);
										tvBtn6_2_2.setVisibility(View.VISIBLE);

										tvBtn1_3.setVisibility(View.VISIBLE);//挂断
										tvBtn2_3.setVisibility(View.VISIBLE);
										tvBtn3_3.setVisibility(View.VISIBLE);
										tvBtn4_3.setVisibility(View.VISIBLE);
										tvBtn5_3.setVisibility(View.VISIBLE);
										tvBtn6_3.setVisibility(View.VISIBLE);
									}
								}
								else if (GlobalVariable.sipmodel == 0 || GlobalVariable.sipmodel == 3) {
									//三个按键
									//导播 接听 播出 挂断
									//主播 接听 播出 挂断
									tvBtn1_1.setVisibility(View.VISIBLE);//接听
									tvBtn2_1.setVisibility(View.VISIBLE);
									tvBtn3_1.setVisibility(View.VISIBLE);
									tvBtn4_1.setVisibility(View.VISIBLE);
									tvBtn5_1.setVisibility(View.VISIBLE);
									tvBtn6_1.setVisibility(View.VISIBLE);

									tvBtn1_2_2.setVisibility(View.VISIBLE);//留直播
									tvBtn2_2_2.setVisibility(View.VISIBLE);
									tvBtn3_2_2.setVisibility(View.VISIBLE);
									tvBtn4_2_2.setVisibility(View.VISIBLE);
									tvBtn5_2_2.setVisibility(View.VISIBLE);
									tvBtn6_2_2.setVisibility(View.VISIBLE);

									tvBtn1_3.setVisibility(View.VISIBLE);//挂断
									tvBtn2_3.setVisibility(View.VISIBLE);
									tvBtn3_3.setVisibility(View.VISIBLE);
									tvBtn4_3.setVisibility(View.VISIBLE);
									tvBtn5_3.setVisibility(View.VISIBLE);
									tvBtn6_3.setVisibility(View.VISIBLE);

								}

							} else {
								tvBtn1_1.setVisibility(View.GONE);
								tvBtn1_2_1.setVisibility(View.GONE);
								tvBtn1_2_2.setVisibility(View.GONE);
								tvBtn1_3.setVisibility(View.GONE);
								tvBtn2_1.setVisibility(View.GONE);
								tvBtn2_2_1.setVisibility(View.GONE);
								tvBtn2_2_2.setVisibility(View.GONE);
								tvBtn2_3.setVisibility(View.GONE);
								tvBtn3_1.setVisibility(View.GONE);
								tvBtn3_2_1.setVisibility(View.GONE);
								tvBtn3_2_2.setVisibility(View.GONE);
								tvBtn3_3.setVisibility(View.GONE);
								tvBtn4_1.setVisibility(View.GONE);
								tvBtn4_2_1.setVisibility(View.GONE);
								tvBtn4_2_2.setVisibility(View.GONE);
								tvBtn4_3.setVisibility(View.GONE);
								tvBtn5_1.setVisibility(View.GONE);
								tvBtn5_2_1.setVisibility(View.GONE);
								tvBtn5_2_2.setVisibility(View.GONE);
								tvBtn5_3.setVisibility(View.GONE);
								tvBtn6_1.setVisibility(View.GONE);
								tvBtn6_2_1.setVisibility(View.GONE);
								tvBtn6_2_2.setVisibility(View.GONE);
								tvBtn6_3.setVisibility(View.GONE);
							}
						}
						if (false) {
							if (GlobalVariable.viewmodel == 1) {
								final Activity activity = ActivityController.getCurrentActivity();
								ActivityManager am = (ActivityManager) activity.getApplicationContext().getSystemService(activity.ACTIVITY_SERVICE);
								ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
								if (cn.getClassName().equals("org.pjsip.pjsua2.app.MainActivity")) {
									//判断主界面上当前按钮的状态是否允许操作
									//允许的话对当前的电话进行保持和取消保持
									if (GlobalVariable.charactername.equals("director")) {
										ImageButton btCN1 = findViewById(R.id.buttonCN1);
										if (btCN1.isEnabled()) {
											acceptCall(btCN1);
										}
									} else {
										ImageButton btCN6 = findViewById(R.id.buttonCN6);
										if (btCN6.isEnabled()) {
											liveCall(btCN6);
										}
									}
								}
							} else if (GlobalVariable.viewmodel == 2) {
								showCtDailPanelDialog("");
							} else if (GlobalVariable.viewmodel == 3) {
								showCtDailPanelDialog("");
							}
						}
					}
					return true;
			}
		}
		if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
			if (event.getAction() == KeyEvent.ACTION_DOWN && event.getRepeatCount() == 0) {
				if(GlobalVariable.isDaLian){

				}
				else{
					this.exitApp();
				}
			}
			return true;
		}
		else {
			return super.dispatchKeyEvent(event);
		}
	}

//	@Override
//	public void onBackPressed() {
//		if (System.currentTimeMillis() - exitTime < 2000) {
//			super.onBackPressed();
//		} else {
//			Toast.makeText(MainActivity.this, "再按一次退出到登录界面", Toast.LENGTH_SHORT).show();
//			exitTime = System.currentTimeMillis();
//		}
//	}
	/**退出程序**/
	private void exitApp(){
		// 判断2次点击事件时间
		if ((System.currentTimeMillis() - exitTime) > 2000) {
			Toast.makeText(MainActivity.this, "再按一次重新登录", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			Message m2 = Message.obtain(MainActivity.handler_,1017,null);
			m2.sendToTarget();

//				String to_sipid = "";
//				for(ProgramDirector pdr :GlobalVariable.pdrList){
//					if(to_sipid.equals("")&&pdr.sipid.compareTo(Long.parseLong(GlobalVariable.user_sipid))!=0){
//						to_sipid = String.valueOf(pdr.sipid);
//					}
//				}
//				if(allCallinList.size()>0 && GlobalVariable.sipmodel == 2 && !to_sipid.equals(""))
//				{
//					//有未处理电话,弹出转接窗口
//					//自动转接，手动选择转接
//					//发送DAOBO TO DAOBO 消息
//					//需要放置的对象类型
//
//					String msg_to_send = "{ \"msgType\": \"DAOBO TO DAOBO\", \"DaoBoCallUri\": \""
//							+ "<sip:"+to_sipid+"@"+GlobalVariable.EUSERVER+">\", " +
//							"\"ToOutCallUri\": [" ;
//					for(MyCallWithState callWithState : allCallinList){
//						if(callWithState.type != CALL_TYPE.NEWINIT && callWithState.type != CALL_TYPE.ENDCALL) {
//							msg_to_send += "{\"outcalluri\": "+"\""+callWithState.outcalluri+"\","
//									+ "\"uid\": "+"\""+callWithState.uid+"\","
//									+ "\"type\": "+"\""+callWithState.type+"\","
//									+ "\"callednumber\": "+"\""+callWithState.callednumber+"\","
//									+ "\"state\": "+"\""+callWithState.state+"\""
//									+ "}" ;
//							msg_to_send += ",";
//						}
//					}
//					msg_to_send = msg_to_send.substring(0,msg_to_send.length()-1);
//					msg_to_send += "]}";
//
//					Log.e("msg_to_send",msg_to_send);
//					if(wsControl!=null)wsControl.sendMessage(msg_to_send);
//				}
//
//				if(onlycall!=null)
//				{
//					CallOpParam prm = new CallOpParam();
//					prm.setStatusCode(pjsip_status_code.PJSIP_SC_BUSY_HERE);
//					try {
//						onlycall.hangup(prm);
//						Log.e("主动挂断电话","SOSOS");
//					} catch (Exception e) {}
//				}
//				String msg_to_send	= null;
//				if(GlobalVariable.charactername.equals("director"))
//				{
//					msg_to_send = "{ \"msgType\": \"DEVICE OFFLINE\", \"DaoBoCallUri\": \""
//							+ localUri + "\","
//							+"\"ZhuBoCallUri\": \""+"\""
//							+" }";
//				}
//				else{
//					msg_to_send = "{ \"msgType\": \"DEVICE OFFLINE\", \"DaoBoCallUri\": \""
//							+"\","
//							+"\"ZhuBoCallUri\": \""+ localUri + "\""
//							+" }";
//				}
//				if(wsControl!=null)wsControl.sendMessage(msg_to_send);
//
//				unbindService(conn);
////				Intent intent_music = new Intent(this, MusicService.class);
////				stopService(intent_music);
//
//				unbindService(serviceConnection);
////				Intent intent = new Intent(this, JWebSocketClientService.class);
////				stopService(intent);
//
//				//2021-03-17
//				try {
//					if (receiver != null) {
//						unregisterReceiver(receiver);
//					}
//					if (wsReceiver != null) {
//						unregisterReceiver(wsReceiver);
//					}
//				}catch (Exception e){
//					e.printStackTrace();
//				}
//
//				//退出前清空所有电话
//				callinList.clear();
//				allCallinList.clear();
//
//				app.deinit();
//				finish();
//				Runtime.getRuntime().gc();
//				android.os.Process.killProcess(android.os.Process.myPid());
//			finish();
		}
	}

	public void ShowModuleTest(View view){
//		MsgNotifyDialog msgNotifyDialog = new MsgNotifyDialog.Builder(this,"未选择处理电话").create();
//		msgNotifyDialog.show();
//		msgNotifyDialog.getWindow().setBackgroundDrawableResource(R.drawable.ct_bkg_notify_dialog);
//		msgNotifyDialog.getWindow().setLayout(500, 300);
		ModuleTestDialog moduleTestDialog = new ModuleTestDialog.Builder(this).create();
		moduleTestDialog.show();
		Window window = moduleTestDialog.getWindow();
		if (window != null) {
			WindowManager.LayoutParams attr = window.getAttributes();
			if (attr != null) {
				attr.height = ViewGroup.LayoutParams.MATCH_PARENT;
				attr.width = ViewGroup.LayoutParams.MATCH_PARENT;
				attr.gravity = Gravity.BOTTOM;
				window.setAttributes(attr);
			}
		}
		moduleTestDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);;
		moduleTestDialog.getWindow().setBackgroundDrawableResource(R.color.translucentblack);
		moduleTestDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		moduleTestDialog.showKeyboard();

//		TextView et = moduleTestDialog.findViewById(R.id.moduleEt1);
//		//设置可获得焦点
//		et.setFocusable(true);
//		et.setFocusableInTouchMode(true);http://www.tokorozawa-stm.ed.jp/D_base/nihongo/html/chinese.html
//		//请求获得焦点
//		et.requestFocus();
//		//调用系统输入法
//		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//		//imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
//		boolean rsOpenSoft = imm.showSoftInput(et, InputMethodManager.SHOW_FORCED);
//		Log.e("打开软键盘结果",rsOpenSoft?"成功":"失败");

//		moduleTestDialog.getWindow().setGravity(Gravity.TOP);
//		moduleTestDialog.getWindow().setLayout(1024, 600);

	}
}


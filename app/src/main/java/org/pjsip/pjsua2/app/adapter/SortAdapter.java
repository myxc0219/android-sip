package org.pjsip.pjsua2.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.model.PersonBean;

import java.util.List;

public class SortAdapter extends BaseAdapter {
    private Context context;
    public List<PersonBean> persons;
    private LayoutInflater inflater;

    private ListView mListView;

    public SortAdapter(Context context, List<PersonBean> persons,ListView listview) {
        this.context = context;
        this.persons = persons;
        this.inflater = LayoutInflater.from(context);
        this.mListView = listview;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return persons.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return persons.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewholder = null;
        PersonBean person = persons.get(position);
        if (convertView == null) {
            viewholder = new ViewHolder();
            convertView = inflater.inflate(R.layout.contacts_list_item_test, null);

            boolean currentselected = false;
//            int selectposition = mListView.getCheckedItemPosition();
//            if(selectposition==position)
//                currentselected = true;
            if(person.isSelected())
                currentselected = true;

            TextView  nameView = (TextView) convertView.findViewById(R.id.tv_lv_item_name);//用于处理选择的项的颜色处理
            TextView  phoneView = (TextView) convertView.findViewById(R.id.tv_lv_item_phone);

            TextView tagView = convertView.findViewById(R.id.tv_lv_item_tag);
            ImageView headView = convertView.findViewById(R.id.iv_lv_item_head);
            if(GlobalVariable.viewmodel == 1){
                tagView.setTextColor(Color.WHITE);
                headView.setColorFilter(convertView.getResources().getColor(R.color.transparent));
            }
            else if(GlobalVariable.viewmodel == 2) {
                tagView.setTextSize(24);
                nameView.setTextSize(24);
                phoneView.setTextSize(24);
                tagView.setTypeface(null, Typeface.NORMAL);
                nameView.setTypeface(null, Typeface.NORMAL);
                phoneView.setTypeface(null, Typeface.NORMAL);

//                tagView.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
//                headView.setColorFilter(convertView.getResources().getColor(R.color.ct_gray));
                tagView.setTextColor(convertView.getResources().getColor(R.color.white));
                headView.setColorFilter(convertView.getResources().getColor(R.color.white));
            }



            if(currentselected){
                nameView.setTextColor(context.getResources().getColor(R.color.select_blue));
                nameView.setTextSize(26);
                phoneView.setTextColor(context.getResources().getColor(R.color.select_blue));
                phoneView.setTextSize(26);
            }
            else{
                if(GlobalVariable.viewmodel == 1){
                    nameView.setTextColor(Color.WHITE);
                    phoneView.setTextColor(Color.WHITE);
                }
                else if(GlobalVariable.viewmodel == 2){
//                    nameView.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
//                    phoneView.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
                    nameView.setTextColor(Color.WHITE);
                    phoneView.setTextColor(Color.WHITE);
                }
                else if(GlobalVariable.viewmodel == 3){
                    nameView.setTextColor(Color.WHITE);
                    phoneView.setTextColor(Color.WHITE);
                }

                nameView.setTextSize(24);
                phoneView.setTextSize(24);
            }

            viewholder.tv_tag = (TextView) convertView
                    .findViewById(R.id.tv_lv_item_tag);
            viewholder.tv_name = (TextView) convertView
                    .findViewById(R.id.tv_lv_item_name);
            viewholder.tv_phone = (TextView) convertView
                    .findViewById(R.id.tv_lv_item_phone);

            viewholder.bt_detail = (Button) convertView
                    .findViewById(R.id.button_ContactDetail);
            //点击按钮时查找对象需要
            viewholder.bt_detail.setTag(position);

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
            boolean currentselected = false;
//            int selectposition = mListView.getCheckedItemPosition();
//            if(selectposition==position)
//                currentselected = true;
            if(person.isSelected())
                currentselected = true;

            TextView  nameView = (TextView) convertView.findViewById(R.id.tv_lv_item_name);//用于处理选择的项的颜色处理
            TextView  phoneView = (TextView) convertView.findViewById(R.id.tv_lv_item_phone);

            TextView tagView = convertView.findViewById(R.id.tv_lv_item_tag);
            ImageView headView = convertView.findViewById(R.id.iv_lv_item_head);
            if(GlobalVariable.viewmodel == 1){
                tagView.setTextColor(Color.WHITE);
                headView.setColorFilter(convertView.getResources().getColor(R.color.transparent));
            }
            else if(GlobalVariable.viewmodel == 2) {
                tagView.setTextSize(24);
                nameView.setTextSize(24);
                phoneView.setTextSize(24);
                tagView.setTypeface(null, Typeface.NORMAL);
                nameView.setTypeface(null, Typeface.NORMAL);
                phoneView.setTypeface(null, Typeface.NORMAL);

//                tagView.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
//                headView.setColorFilter(convertView.getResources().getColor(R.color.ct_gray));
                tagView.setTextColor(convertView.getResources().getColor(R.color.white));
                headView.setColorFilter(convertView.getResources().getColor(R.color.white));
            }

            if(currentselected){
                nameView.setTextColor(context.getResources().getColor(R.color.select_blue));
                nameView.setTextSize(26);
                phoneView.setTextColor(context.getResources().getColor(R.color.select_blue));
                phoneView.setTextSize(26);
            }
            else{
                if(GlobalVariable.viewmodel == 1){
                    nameView.setTextColor(Color.WHITE);
                    phoneView.setTextColor(Color.WHITE);
                }
                else if(GlobalVariable.viewmodel == 2){
//                    nameView.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
//                    phoneView.setTextColor(convertView.getResources().getColor(R.color.ct_gray));
                    nameView.setTextColor(Color.WHITE);
                    phoneView.setTextColor(Color.WHITE);
                }
                else if(GlobalVariable.viewmodel == 3){
                    nameView.setTextColor(Color.WHITE);
                    phoneView.setTextColor(Color.WHITE);
                }

                nameView.setTextSize(24);
                phoneView.setTextSize(24);
            }

        }
        // 获取首字母的assii值
        int selection = person.getSortLetters().charAt(0);
        // 通过首字母的assii值来判断是否显示字母
        int positionForSelection = getPositionForSelection(selection);
        if (position == positionForSelection) {// 相等说明需要显示字母
            viewholder.tv_tag.setVisibility(View.VISIBLE);
            viewholder.tv_tag.setText(person.getSortLetters());
        } else {
            viewholder.tv_tag.setVisibility(View.GONE);

        }
        viewholder.tv_name.setText(person.getName());
        viewholder.tv_phone.setText(person.getPhone());
        return convertView;
    }

    public int getPositionForSelection(int selection) {
        for (int i = 0; i < persons.size(); i++) {
            String Fpinyin = persons.get(i).getSortLetters();
            char first = Fpinyin.toUpperCase().charAt(0);
            if (first == selection) {
                return i;
            }
        }
        return -1;

    }

    class ViewHolder {
        TextView tv_tag;
        TextView tv_name;
        TextView tv_phone;
        Button bt_detail;
    }

}
package org.pjsip.pjsua2.app.model;

public class PersonBean {

    private String name;

    private String pinyin;

    private String sortLetters;

    private String phone;

    private Integer age;

    private String frm;

    private Integer isblacklist;

    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getFrm() {
        return frm;
    }

    public void setFrm(String frm) {
        this.frm = frm;
    }

    public Integer getIsblacklist() {
        return isblacklist;
    }

    public void setIsblacklist(Integer isblacklist) {
        this.isblacklist = isblacklist;
    }

    public String getPinYin() {
        return pinyin;
    }

    public void setPinYin(String pinyin) {
        this.pinyin = pinyin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSortLetters() {
        return sortLetters;
    }

    public void setSortLetters(String sortLetters) {
        this.sortLetters = sortLetters;
    }

    public PersonBean(){

    }

}

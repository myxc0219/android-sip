package org.pjsip.pjsua2.app.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.pjsip.pjsua2.app.MainActivity;
import org.pjsip.pjsua2.app.R;
import org.pjsip.pjsua2.app.global.GlobalVariable;
import org.pjsip.pjsua2.app.log.LogClient;
import org.pjsip.pjsua2.app.model.ProgramDirector;
import org.pjsip.pjsua2.app.util.EditTextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PdrChoseDialog extends Dialog {



    private PdrChoseDialog(Context context, int themeResId) {
        super(context, themeResId);
    }



    public static class Builder {

        private View mLayout;
        private Context mContext;


//        private ImageView mIcon;
//        private TextView mTitle;
//        private TextView mMessage;
//        private Button mButton;

        private Button mbtnPdrConfirm;
        private Button mbtnPdrCancel;

        private View.OnClickListener mButtonClickListener;

        private PdrChoseDialog mDialog;

        private ArrayList<Map<String, String>> pdrList = new ArrayList<>();
        private ListView pdrListView;

        private SimpleAdapter pdrAdapter;

        private int pdrSelectedIndex = -1;




        public Builder(Context context) {
            mContext = context;
            mDialog = new PdrChoseDialog(context, R.style.Theme_AppCompat_Dialog);
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //加载布局文件
            mLayout = inflater.inflate(R.layout.dlg_pdr_chose, null, false);
            //添加布局文件到 Dialog
            mDialog.addContentView(mLayout, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));

            mbtnPdrConfirm = mLayout.findViewById(R.id.btnPdrConfirm);
            mbtnPdrCancel = mLayout.findViewById(R.id.btnPdrCancel);
            pdrListView = mLayout.findViewById(R.id.pdrList);

        }

//        /**
//         * 设置按钮文字和监听
//         */
//        public Builder setButton(@NonNull String text, View.OnClickListener listener) {
//            mButton.setText(text);
//            mButtonClickListener = listener;
//            return this;
//        }

        public PdrChoseDialog create() {

            //填充ListView的内容
            //导播名称和分机号
            for(ProgramDirector pdr : GlobalVariable.pdrList){
                Log.e("SIPID",""+pdr.sipid);
                Log.e("userid",""+GlobalVariable.userid);
                if(pdr.sipid.compareTo(GlobalVariable.user_sipid)!=0){
                    HashMap<String, String> item = new HashMap<String, String>();
                    item.put("username", pdr.name);
                    item.put("sipid", String.valueOf(pdr.sipid));
                    pdrList.add(item);
                }
            }


            String[] from = { "username", "sipid" };
            int[] to = { R.id.textName, R.id.textSipid };
            pdrAdapter = new SimpleAdapter(mContext, pdrList,
                    R.layout.pdr_list_item,
                    from, to);
            pdrListView.setAdapter(pdrAdapter);

            pdrListView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent,
                                        final View view,
                                        int position, long id)
                {
                    try {
                        view.setSelected(true);
                        pdrSelectedIndex = position;
                    }catch (Exception e){
                        LogClient.generate("【操作错误】"+e.getMessage());
                        e.printStackTrace();
                    }
                }

            });

            mbtnPdrConfirm.setOnClickListener(view -> {
//                EditText editText = ((Activity)mContext).findViewById(R.id.editTextName);
//                editText.setText(mAppellationText.getText());

                Map<String,String> item = null;
                try{
                    item = pdrList.get(pdrSelectedIndex);
                }
                catch (Exception e){}
                if(item!=null) {
                    String to_sipid = item.get("sipid");
                    Message m2 = Message.obtain(MainActivity.handler_, 1016, to_sipid);
                    m2.sendToTarget();
                }
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });

            mbtnPdrCancel.setOnClickListener(view -> {
                mDialog.dismiss();
                //mButtonClickListener.onClick(view);
            });
            mDialog.setContentView(mLayout);
            mDialog.setCancelable(true);                //用户可以点击后退键关闭 Dialog
            mDialog.setCanceledOnTouchOutside(false);   //用户不可以点击外部来关闭 Dialog
            return mDialog;
        }
    }

    /**
     * 点击软键盘外面的区域关闭软键盘
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Finger touch screen event
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            // get current focus,Generally it is EditText
            View view = getCurrentFocus();
            if (EditTextUtils.isShouldHideSoftKeyBoard(view, ev)) {
                EditTextUtils.hideSoftKeyBoard(view.getWindowToken(), this.getContext());
            }
        }
        return super.dispatchTouchEvent(ev);
    }

}

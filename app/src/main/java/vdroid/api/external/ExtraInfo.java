package vdroid.api.external;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Patterns;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class ExtraInfo implements Parcelable {
    private String number;
    private String name;
    private String company;
    private String jobTitle;
    private String location;
    private String iconUrl;
    private String group;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUrl() {
        return iconUrl;
    }

    public void setUrl(String iconUrl) {
        try {
            URL url = new URL(iconUrl);
            URI uri = new URI(url.getProtocol(), url.getHost(), url.getPath(), url.getQuery(), null);
            if (!TextUtils.isEmpty(iconUrl) && (iconUrl.startsWith("http") || iconUrl.startsWith("rtsp"))) {
                if (Patterns.WEB_URL.matcher(iconUrl).matches()) {
                    this.iconUrl = iconUrl;
                }
            } else {
                this.iconUrl = iconUrl;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public ExtraInfo() {

    }

    public ExtraInfo(Parcel in) {
        number = in.readString();
        name = in.readString();
        company = in.readString();
        jobTitle = in.readString();
        location = in.readString();
        iconUrl = in.readString();
        group = in.readString();
    }

    public String getFormatDisplayString() {
        String company = this.company;
        String jobTitle = this.jobTitle;
        String group = this.group;
        String location = this.location;
        String format = "";
        if (TextUtils.isEmpty(company)) {
            company = "";
        }
        format = company;
        if (!TextUtils.isEmpty(jobTitle)) {
            if (!TextUtils.isEmpty(format)) {
                format += "/" + jobTitle;
            }
        }
        if (!TextUtils.isEmpty(group)) {
            if (!TextUtils.isEmpty(format)) {
                format += "-" + group;
            }
        }
        if (!TextUtils.isEmpty(location)) {
            if (!TextUtils.isEmpty(format)) {
                format += " " + location;
            }
        }
        return format;
    }

    public int compareTo(vdroid.api.external.ExtraInfo that) {
        int result = 1;
        if (that == null) return -1;

        if (this.number == null ? that.number != null : !this.number.equals(that.number)) {
            result = 0;
        }

        if (this.name == null ? that.name != null : !this.name.equals(that.name)) {
            result = 0;
        }

        if (this.company == null ? that.company != null : !this.company.equals(that.company)) {
            result = 0;
        }
        if (this.jobTitle == null ? that.jobTitle != null : !this.jobTitle.equals(that.jobTitle)) {
            result = 0;
        }
        if (this.location == null ? that.location != null : !this.location.equals(that.location)) {
            result = 0;
        }
        if (this.iconUrl == null ? that.iconUrl != null : !this.iconUrl.equals(that.iconUrl)) {
            result = 0;
        }
        if (this.group == null ? that.group != null : !this.group.equals(that.group)) {
            result = 0;
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("\nExtraInfo {\n");
        builder.append("Number=").append(number).append(", ");
        builder.append("Name=").append(name).append(", ");
        builder.append("Company=").append(company).append(", ");
        builder.append("JobTitle=").append(jobTitle).append(", ");
        builder.append("Url=").append(iconUrl).append(", ");
        builder.append("Group=").append(group).append("\n");
        builder.append("}");
        return builder.toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(number);
        dest.writeString(name);
        dest.writeString(company);
        dest.writeString(jobTitle);
        dest.writeString(location);
        dest.writeString(iconUrl);
        dest.writeString(group);
    }

    public static final Creator<ExtraInfo> CREATOR = new Creator<ExtraInfo>() {
        @Override
        public vdroid.api.external.ExtraInfo createFromParcel(Parcel in) {
            return new vdroid.api.external.ExtraInfo(in);
        }

        @Override
        public vdroid.api.external.ExtraInfo[] newArray(int size) {
            return new vdroid.api.external.ExtraInfo[size];
        }
    };
}